# WFM.com
***

[ ![Codeship Status for wfmdigitalweb/wfmcom](https://codeship.com/projects/2e960300-a4e0-0132-bacd-62f6ae253d65/status?branch=develop)](https://codeship.com/projects/66528)

## Project Guidelines and Development Standards

_QUICK PROJECT OVERVIEW_

### Source Control

Git Documentation and Resources:

    [Atlassian git tutorial](https://www.atlassian.com/git/tutorials "Atlassian git tutorial")
    [Pro Git Book](http://git-scm.com/book/en/v2 "Pro Git")
    [GitHub Documentaion](https://help.github.com/ "GitHub Help")

### Git Development Methodology

Development follows the [Gitflow workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow "Gitflow Workflow") and the [Forking workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow "Forking Workflow"). Please make sure to read and understand both of these workflows.

## Structure

### General

- **docroot** - The Drupal root.
- **README.md** - Project documentation written in markdown.
- **build** - Project specific files for building and testing WFM CORE.
- **build/composer.json** - Project specific vendor packages and repositories.
- **build/composer.lock** - Locked in version of vendor packages. To ensure consistency across the project.
- **.gitignore** - A list of files to be ignored by git. This is typically used for excluding local development modules and may create files to ignore that an IDE creates.

### Behat

- **build/tests/behat/behat.yml** - Provides all project specific behat configuration. Including regions and context configuration.
- **build/tests/behat/behat.local.yml** - Local configuration to override *behat.yml*. Typically this will only be the url of the current environment.
- **build/tests/behat/tests/behat** - The directory where behat .feature files are stored.

The *behat.local.yml* file is provided empty and ignored from the repository so changes can be made to run behat in the local environment. The structure of the file follows that of *behat.yml*, to set the local target URL to *http://wfmwfmcom.local/* for behat, the following may be placed in *behat.local.yml*:

#### Local behat settings.

```
default:
  extensions:
    Behat\MinkExtension:
      base_url: http://wfmwfmcom.local/
```

Behat parameters may also be added by altering the BEHAT_PARAMS variable. This will only affect direct behat runs, rather than those run through Phing.

```
export BEHAT_PARAMS='{"extensions" : {"Behat\\MinkExtension" : {"base_url" : "http://wfmwfmcom.local/"}}}'
```

## Build and Test Targets

### Phing

  [Phing User Guide](https://www.phing.info/docs/guide/stable/ "Phing User Guide")

- **build/phing/build.xml** - Contains project specific configuration and tasks that can be executed across this projects team.
- **build/phing/build.properties** - Environment specific configuration. Just like *behat.local.yml*, typically this will assign the url of the current environment.

The variables that Phing uses are configured at the top of build.xml. If there are alterations to these parameters to allow Phing to run locally, these may be placed in the *build.properties* file. This file is ignored from git so local modifications will not be committed. To alter the base URL for the Drupal site the following may be added to *build.properties*.

```
; local build properties

; The uri of the site.
drupal.base_url='http://local.wholefoodsmarket.com/'

; The local database credentials.
db.username = "root"
db.password = ""
db.name = local_wholefoods
db.host = localhost
db.port = 3306
```

All tasks in this project can be listed via the command:

```
phing -f build/phing/build.xml -l
```

## Deployment and Testing Workflows

[The PHP Quality Assurance Toolchain](http://phpqatools.org/) provides more information on many of the tests used on this project.

### Automated Testing

The ability to test a WFM CORE build is built into the repository with all tests run by [Codeship](https://codeship.com/) able to be run locally. Any changes made should be added and committed to your local repository and the following commands run:

```
phing -f build/phing/build.xml build
phing -f build/phing/build.xml validate-test:all
```

Individual tests may be run by specifying the target for Phing. If just the behat tests need to be run, the target can be changed:

```
phing -f build/phing/build.xml test:behat
```

#### Bitbucket and Codeship


#### Bitbucket and Bamboo



### Developer workflow

**Dependencies**

- [git](http://git-scm.com/)
- [composer](https://getcomposer.org/)
- [bundler](http://bundler.io/)

#### Project Setup

1. Ensure you're in the working GIT directory root

```
cd <project_directory>
```

1. Install bundler

```
gem install bundler
```

If you get a permission error, you may need to use `sudo` with the above command.


1. Install build system dependencies with composer

```
composer install --prefer-dist --working-dir=build
```

1. Create your local files

```
// Local settings for Drupal, docroot/sites/default/settings.local.php
$databases = array(
  'default' =>
    array (
      'default' =>
        array (
          'database' => '<localdbname>',
          'username' => '<localdbuser>',
          'password' => '<localdbpass>',
          'host' => '<localdbhost>',
          'port' => '<localdbport>',
          'driver' => 'mysql',
          'prefix' => '',
        ),
      ),
);

// Provides variables for the composer manager module
$conf['composer_manager_vendor_dir'] = '../vendor';
$conf['composer_manager_file_dir'] = '../';
```

```
// Local settings for Behat, build/tests/behat/behat.local.yml
default:
  extensions:
    Behat\MinkExtension:
      base_url: "<localbaseurl>"
```

```
; Local settings for Phing, build/phing/build.properties
; The uri of the site.
drupal.base_url='<localbaseurl>'

; The local database credentials.
db.username = "<localdbuser>"
db.password = "<localdbpass>"
db.name = "<localdbname>"
db.host = "<localdbhost>"
db.port = "<localdbport>"
```

1. Run the local build install

```
build/bin/phing -f build/phing/build.xml build:local
```

Or, you can choose not to download the WF database and install Drupal

```
build/bin/phing -f build/phing/build.xml build:local:no-install
```

If you are making changes to the make file, you can tell the build process to build from your local make file, instead of the one in the profile repository.

...
