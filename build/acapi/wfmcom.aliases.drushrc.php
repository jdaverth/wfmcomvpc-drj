<?php

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site wfmcom, environment dev
$aliases['dev'] = array(
  'root' => '/var/www/html/wfmcom.dev/docroot',
  'ac-site' => 'wfmcom',
  'ac-env' => 'dev',
  'ac-realm' => 'prod',
  'uri' => 'wfmcomdev.prod.acquia-sites.com',
  'remote-host' => 'staging-19108.prod.hosting.acquia.com',
  'remote-user' => 'wfmcom.dev',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['dev.livedev'] = array(
  'parent' => '@wfmcom.dev',
  'root' => '/mnt/gfs/wfmcom.dev/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site wfmcom, environment dev2
$aliases['dev2'] = array(
  'root' => '/var/www/html/wfmcom.dev2/docroot',
  'ac-site' => 'wfmcom',
  'ac-env' => 'dev2',
  'ac-realm' => 'prod',
  'uri' => 'wfmcomdev2.prod.acquia-sites.com',
  'remote-host' => 'staging-19108.prod.hosting.acquia.com',
  'remote-user' => 'wfmcom.dev2',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['dev2.livedev'] = array(
  'parent' => '@wfmcom.dev2',
  'root' => '/mnt/gfs/wfmcom.dev2/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site wfmcom, environment dev3
$aliases['dev3'] = array(
  'root' => '/var/www/html/wfmcom.dev3/docroot',
  'ac-site' => 'wfmcom',
  'ac-env' => 'dev3',
  'ac-realm' => 'prod',
  'uri' => 'wfmcomdev3.prod.acquia-sites.com',
  'remote-host' => 'staging-19108.prod.hosting.acquia.com',
  'remote-user' => 'wfmcom.dev3',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['dev3.livedev'] = array(
  'parent' => '@wfmcom.dev3',
  'root' => '/mnt/gfs/wfmcom.dev3/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site wfmcom, environment dev4
$aliases['dev4'] = array(
  'root' => '/var/www/html/wfmcom.dev4/docroot',
  'ac-site' => 'wfmcom',
  'ac-env' => 'dev4',
  'ac-realm' => 'prod',
  'uri' => 'wfmcomdev4.prod.acquia-sites.com',
  'remote-host' => 'staging-19108.prod.hosting.acquia.com',
  'remote-user' => 'wfmcom.dev4',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['dev4.livedev'] = array(
  'parent' => '@wfmcom.dev4',
  'root' => '/mnt/gfs/wfmcom.dev4/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site wfmcom, environment dev5
$aliases['dev5'] = array(
  'root' => '/var/www/html/wfmcom.dev5/docroot',
  'ac-site' => 'wfmcom',
  'ac-env' => 'dev5',
  'ac-realm' => 'prod',
  'uri' => 'wfmcomdev5.prod.acquia-sites.com',
  'remote-host' => 'staging-19108.prod.hosting.acquia.com',
  'remote-user' => 'wfmcom.dev5',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['dev5.livedev'] = array(
  'parent' => '@wfmcom.dev5',
  'root' => '/mnt/gfs/wfmcom.dev5/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site wfmcom, environment mdev
$aliases['mdev'] = array(
  'root' => '/var/www/html/wfmcom.mdev/docroot',
  'ac-site' => 'wfmcom',
  'ac-env' => 'mdev',
  'ac-realm' => 'prod',
  'uri' => 'wfmcommdev.prod.acquia-sites.com',
  'remote-host' => 'staging-19109.prod.hosting.acquia.com',
  'remote-user' => 'wfmcom.mdev',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['mdev.livedev'] = array(
  'parent' => '@wfmcom.mdev',
  'root' => '/mnt/gfs/wfmcom.mdev/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site wfmcom, environment prod
$aliases['prod'] = array(
  'root' => '/var/www/html/wfmcom.prod/docroot',
  'ac-site' => 'wfmcom',
  'ac-env' => 'prod',
  'ac-realm' => 'prod',
  'uri' => 'wfmcom.prod.acquia-sites.com',
  'remote-host' => 'web-19097.prod.hosting.acquia.com',
  'remote-user' => 'wfmcom.prod',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['prod.livedev'] = array(
  'parent' => '@wfmcom.prod',
  'root' => '/mnt/gfs/wfmcom.prod/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site wfmcom, environment ra
$aliases['ra'] = array(
  'root' => '/var/www/html/wfmcom.ra/docroot',
  'ac-site' => 'wfmcom',
  'ac-env' => 'ra',
  'ac-realm' => 'prod',
  'uri' => 'wfmcomra.prod.acquia-sites.com',
  'remote-host' => 'staging-19179.prod.hosting.acquia.com',
  'remote-user' => 'wfmcom.ra',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['ra.livedev'] = array(
  'parent' => '@wfmcom.ra',
  'root' => '/mnt/gfs/wfmcom.ra/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site wfmcom, environment test
$aliases['test'] = array(
  'root' => '/var/www/html/wfmcom.test/docroot',
  'ac-site' => 'wfmcom',
  'ac-env' => 'test',
  'ac-realm' => 'prod',
  'uri' => 'wfmcomstg.prod.acquia-sites.com',
  'remote-host' => 'staging-19109.prod.hosting.acquia.com',
  'remote-user' => 'wfmcom.test',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['test.livedev'] = array(
  'parent' => '@wfmcom.test',
  'root' => '/mnt/gfs/wfmcom.test/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site wfmcom, environment test2
$aliases['test2'] = array(
  'root' => '/var/www/html/wfmcom.test2/docroot',
  'ac-site' => 'wfmcom',
  'ac-env' => 'test2',
  'ac-realm' => 'prod',
  'uri' => 'wfmcomstg2.prod.acquia-sites.com',
  'remote-host' => 'staging-19109.prod.hosting.acquia.com',
  'remote-user' => 'wfmcom.test2',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['test2.livedev'] = array(
  'parent' => '@wfmcom.test2',
  'root' => '/mnt/gfs/wfmcom.test2/livedev/docroot',
);
