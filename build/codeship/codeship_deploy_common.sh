#!/bin/bash
#
# Automate Jekins deployment with CodeShip deploy piplines
#
# Jenkins Plugin :: https://wiki.jenkins-ci.org/display/JENKINS/Build+Token+Root+Plugin
# Codeship Environment Variables :: https://codeship.com/documentation/continuous-integration/set-environment-variables/
#
# JENKINS_URL=myserver.ci.cloudbees.com                 # Custom Codeship Environment Variable for ULR of the Jenkins server
# JENKINS_DEPLOY_JOB=deploy-job-name                    # Custom Codeship Environment Variable for the Jenkins job to be called
# JENKINS_DEPLOY_TOKEN=1234567890                       # Custom Codeship Environment Variable for access token for Jenkins job
# CI_BRANCH                                             # Default Codeship Environment Variable for the branch being tested
# BRANCH                                                # Jenkins Build Parameter for the branch to build passed to the Jenkins job

curl https://$JENKINS_URL/buildByToken/buildWithParameters/build?job=$JENKINS_DEPLOY_JOB\&token=$JENKINS_DEPLOY_TOKEN\&BRANCH=$CI_BRANCH
