<?php

/**
 * For a single database configuration, the following is sufficient:
**/

$databases = array (
  'default' =>
  array (
    'default' =>
    array (
      'database' => 'test',
      'username' => $_ENV["MYSQL_USER"],
      'password' => $_ENV["MYSQL_PASSWORD"],
      'host' => 'localhost',
      'port' => '',
      'driver' => 'mysql',
      'prefix' => '',
    ),
  ),
);

/**
 * Provides variables for the composer manager module
 */
$conf['composer_manager_vendor_dir'] = '../vendor';
$conf['composer_manager_file_dir'] = '../';
$conf['composer_manager_autobuild_file'] = 0;
$conf['composer_manager_autobuild_packages'] = 0;

// SAGE API Keys
$conf['sage_api_endpoint'] = $_ENV['SAGE_API_ENDPOINT'];
$conf['sage_api_key'] = $_ENV['SAGE_API_KEY'];
$conf['sage_api_secret'] = $_ENV['SAGE_API_SECRET'];
