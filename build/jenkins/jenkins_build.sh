#!/bin/bash
#
# This script is not called by Jenkins in favor of the Phing Plugin
# see: https://wiki.jenkins-ci.org/display/JENKINS/Phing+Plugin
#

# Runs the phing build target to prepare docroot for Acquia Cloud
build/bin/phing -f build/phing/build.xml build:deploy:no-install
