#!/bin/bash
#
# Configure Jenkins and install the dependencies for Phing
# Note: Workspace is deleted before the start of each build

# Tells bash to kill the script if any one command fails
set -e

# Install PHP
# wget -q https://repository-cloudbees.forge.cloudbees.com/distributions/ci-addons/php/use-php
# PHP_VERSION=5.4.11 . ./use-php

# Configure PHP
# mkdir -p /tmp/php
# echo "date.timezone = 'America/Chicago'" > /tmp/php/php.ini
# export PHPRC=/tmp/php

# Install Drush and Composer
# curl -s -o use-drush https://repository-cloudbees.forge.cloudbees.com/distributions/ci-addons/drush/use-drush
# DRUSH_VERSION=dev-master
# . ./use-drush

# Install Composer (bundled with Drush)
# ln -sf /scratch/jenkins/addons/composer/composer.phar $WORKSPACE/bin/composer

# Install Ruby
curl -s -o use-ruby https://repository-cloudbees.forge.cloudbees.com/distributions/ci-addons/ruby/use-ruby
RUBY_VERSION=2.1.2 . ./use-ruby

# Install compass gem
gem install bundler

# Install Node and NPM
# curl -s -o use-node https://repository-cloudbees.forge.cloudbees.com/distributions/ci-addons/node/use-node
# NODE_VERSION=0.10.25 . ./use-node

# Creates $WORKSPACE bin directoiry and adds it the $PATH
mkdir -p $WORKSPACE/bin
export PATH="$WORKSPACE/bin:$PATH"

# Fail the build if we don't have a $BRANCH to build or other parameters are empty
# @TODO :: Check if all the above parameters are set
if [ -z "${BRANCH}" ]; then
    echo "BRANCH parameter is not set"
    exit 1
fi

# Fail the build if $BRANCH == master, but we dont have a $PROD_RELEASE_VERSION
if [ ${BRANCH} == "master" ] && [ -z "${PROD_RELEASE_VERSION}" ]; then
  echo "PROD_RELEASE_VERSION parameter is not set and is required for building the master branch"
  exit 1
else
  echo "Building ${BRANCH} and adding version ${PROD_RELEASE_VERSION}"
fi

# Make sure we have a remote deploy endpoint and checkout our upstream branch
if (git remote | grep -q $DEPLOYMENT_REMOTE); then
  echo "$DEPLOYMENT_REMOTE is listed as a remote.";
else
  echo "$DEPLOYMENT_REMOTE is not a remote, adding.";
  git remote add $DEPLOYMENT_REMOTE $DEPLOYMENT_GIT_URL
fi

# Switch to the proper branch
git checkout $BRANCH

# Installs Composer in $WORKSPACE bin directory
curl -sS https://getcomposer.org/installer | php -- --install-dir=$WORKSPACE/bin --filename=composer  --version=1.0.0-alpha11

# Installs Phing and dependencies other required packages to build the project
composer install --prefer-dist --working-dir=build
