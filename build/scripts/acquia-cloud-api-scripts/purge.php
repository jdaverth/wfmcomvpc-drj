<?php

/**
 * @file
 * Varnish purge script using Acquia Cloud PHP SDK.
 *
 * The following environment variables must be set:
 *   - ACAPI_USERNAME / ACAPI_PASSWORD
 *   - ACQUIA_CLOUD_ENVIRONMENT (e.g. 'dev', 'test')
 */

// @TODO: Catch exceptions and handle task timeouts

require_once dirname(__FILE__) . '/../../vendor/autoload.php';
require_once 'config.php';

use Acquia\Cloud\Api\CloudApiClient;

// Build Cloud API client connection.
$cloudapi = CloudApiClient::factory(array(
  'username' => getenv('ACAPI_USERNAME'),
  'password' => getenv('ACAPI_PASSWORD'),
));

// Set up other required variables.
$environment = getenv('ACQUIA_CLOUD_ENVIRONMENT');

// Get a list of all an environment's domains.
$domains = $cloudapi->domains($site, $environment);
foreach ($domains as $domain) {
  $cloudapi->purgeVarnishCache($site, $environment, $domain);
  printf("Purged Varnish cache for %s in %s environment.\n", $domain, $environment);
}
