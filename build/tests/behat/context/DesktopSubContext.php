<?php
//require_once 'FeatureContext.php';

use Behat\Behat\Context\BehatContext;
use Drupal\DrupalExtension\Context\DrupalSubContextInterface;

class DesktopSubContext extends BehatContext implements DrupalSubContextInterface
{
  
  const REGION_CONTENT = 'content';
     
  public function __construct(array $parameters = null) {
    // do subcontext initialization
  }
  
  public static function getAlias() {
    return 'desktop';
  }
  
  public function getSession() {
    return $this->getMainContext()->getSession();
  }
  
  public function getRegion($region) {
    return $this->getMainContext()->getRegion($region);
  }

  /**
   * Find elements by CSS selector.
   * @param string $selector
   *   Valid CSS selector
   * @return object
   *   Results of findAll on selector.
   */
  public function getElements($selector) {
    $page = $this->getSession()->getPage();
    return $page->findAll('css', $selector);
  }

  /**
   * @Given /^That the Window is Full Screen$/
   */
  public function thatTheWindowIsFullScreen() {
    $this->getSession()->resizeWindow(1280, 800);
  }
  
  /**
   * @Given /^I should see many search results$/
   */
  public function iShouldSeeManySearchResults() {
    $element = $this->getSession()->getPage();
    $find = $element->find('css', '#block-system-main .search_description');
    if (empty($find)) {
      throw new Exception("Search result text selector not found.");
    }
    $resultsText = $find->getText();
    $begin = strpos($resultsText, 'of') + 3;
    $end = strpos($resultsText, 'for') - 1;
    $length = $end - $begin;
    $resultsNumber = substr($resultsText, $begin, $length);
    if ($resultsNumber <= 10) {
      throw new Exception("There were fewer than 10 results.");
    }
  }

    /**
     * @Then /^I should see the Global Marquees$/
     */
    public function iShouldSeeTheGlobalMarquees()
    {
        $element = $this->getSession()->getPage();
        $find = $element->findall('css', '#block-views-marquee-scroller-block .marquee-container');
        if (empty($find)) {
            throw new Exception("No Marquees were found on the home page.");
        }
        if (count($find) <= 1) {
            throw new Exception("There is only one marquee on the home page.");
        }
    }

    /**
     * @Given /^I should see the Go Local block$/
     */
    public function iShouldSeeTheGoLocalBlock()
    {
        $element = $this->getSession()->getPage();
        $find = $element->find('css', '.view-store-lookup-by-address.view-display-id-front_page');
        if (empty($find)) {
            throw new Exception('Go Local pod was not found');
        }
    }

    /**
     * @Then /^I should see the Pluck forum$/
     */
    public function iShouldSeeThePluckForum()
    {
        $element = $this->getSession()->getPage();
        $find = $element->find('css', '.Forums_MainContainer');
        if (empty($find)) {
            throw new Exception('Forum does not display');
        }
    }

    /**
     * @Given /^I should see suggested stores$/
     */
    public function iShouldSeeSuggestedStores()
    {
        $this->getSession()->wait(5000, '(0 === jQuery.active)');
        $element = $this->getSession()->getPage();
        $find = $element->findall('css', '.view-store-lookup-by-address.jquery-once-1-processed .views-row');
        if (empty($find)) {
            throw new Exception("No suggested stores.");
        }
        if (count($find) <= 3) {
            throw new Exception("There are three or fewer suggested stores.");
        }
    }

    /**
     * @Given /^I should see tabs to filter recipes$/
     */
    public function iShouldSeeTabsToFilterRecipes()
    {
        $element = $this->getSession()->getPage();
        $element->findById('quicktabs-recipes');
        if (empty ($element)) {
            throw new Exception("Recipe search form wasn\'t found.");
        }
        //check for a list of tabs
        $quickTabs = $element->find('css', '#quicktabs-recipes .quicktabs-tabs');
        if (empty($quickTabs)) {
            throw new Exception("Unable to find container: #quicktabs-recipes .quicktabs-tabs");
        }
        $html = $quickTabs->getHtml();
        //count the number of li children
        $num_items = substr_count($html, "<li");
        if (!$num_items) {
            throw new Exception("Didn\'t find all the recipe tabs/");
        }
        //look through each tab and check that it has content
        for ($i = 0; $i < $num_items; $i++) {
            if ($element->findById('quicktabs-tabpage-recipes-' . $i) == null) {
                throw new Exception("Recipe tab $i is missing content");
            }
        }
    }

    /**
     * @Then /^I should see the Facebook Like Buttom$/
     */
    public function iShouldSeeTheFacebookLikeButtom()
    {
        $sc = $this->getSocialContainer();
        if (!$sc->has('css', '#fb-root')) {
            throw new Exception('Social: Facebook #fb-root not loaded');
        }
        if (!$sc->has('css', '.recipe-fb')) {
            throw new Exception('Social: Facebook .recipe-fb not loaded');
        }
    }

    /**
     * @Then /^I should see the Twitter Tweet Button$/
     */
    public function iShouldSeeTheTwitterTweetButton()
    {
        $sc = $this->getSocialContainer();
        if (!$sc->has('css', '.recipe-twitter')) {
            throw new Exception('Social: Twitter .recipe-twitter not loaded');
        }
    }

    /**
     * @Then /^I should see the Pin It Button$/
     */
    public function iShouldSeeThePinItButton()
    {
        $sc = $this->getSocialContainer();
        if (!$sc->has('css', '.pinit')) {
            throw new Exception('Social: Pinterest .pinit not loaded');
        }
    }

    /**
     * Returns the div container that WFM's social links reside in
     */
    private function getSocialContainer()
    {
        $element = $this->getSession()->getPage();
        $social_node = $element->find('css', '.social-links');
        if (!$social_node) {
            throw new Exception('Social links container not loaded');
        }
        return $social_node;
    }

    /**
     * @Given /^I check the first checkbox under occasions$/
     */
    public function iCheckTheFirstCheckboxUnderOccasions()
    {
        $element = $this->getSession()->getPage()->find('css', '#edit-field-recipe-occasions');
        $checkbox = $element->find('css', '.form-checkbox');
        $checkbox->check();
    }

    /**
     * @Then /^I should be on a recipe search result page matching "([^"]*)"$/
     */
    public function iShouldBeOnARecipeSearchResultPageMatching($regex)
    {
        $url = $this->getSession()->getCurrentUrl();
        if (!preg_match($regex, $url)) {
            throw new Exception("Page url ($url) doesn't match the pattern $regex");
        }
    }

    /**
     * @Given /^I should see relevant search results for "([^"]*)" identified by "([^"]*)" in the "([^"]*)" region$/
     */
    public function iShouldSeeRelevantSearchResultsForIdentifiedByInTheRegion($search_term, $selector, $region)
    {
        $region_element = $this->getRegion($region);

        $results = $region_element->findAll('css', $selector);
        if (empty($results)) {
            throw new Exception("Unable to find '" . $selector . "'");
        }
        foreach ($results as $result) {
            if (stripos($result->getText(), $search_term) !== FALSE) {
                return;
            }
        }
        throw new Exception("Didn't find a search result title that contained the query.");
    }

    //@here
    /**
     * @When /^I click on some of the paginated links in "([^"]*)" there should be content for each link in "([^"]*)" elements$/
     */
    public function iClickOnSomeOfThePaginatedLinksInThereShouldBeContentForEachLinkInElements($contentContainer, $elementContainerSelector)
    {
        $page = $this->getSession()->getPage();
        $links = $page->findAll('css', $contentContainer . " .pager li a");
        $container = $page->find('css', $contentContainer);


        $last_page_num_container = $container->find('css', '.pager-last');
        if (empty($last_page_num_container)) {
            throw new Exception("Unable to find css selector .pager-last in order to find last page's number value.");
        }
        $last_page_num = $last_page_num_container->getText();
        //print "\n*.*.*.*.*.*.*.*.*.*.*.*\n" . $last_page_num . "\n*.*.*.*.*.*.*.*.*.*.*.*\n";


        //go through some of the links and check the last link
        for ($i = 2; $i < count($links); $i++) {
            $link = $container->findLink("Go to page $i");
            if (is_numeric($link->getText()) || $link->getAttribute('title') == ('Go to page ' . $last_page_num)) {
                $link->click();
                sleep(3); //wait for the new content to be loaded
                //check for content
                if (!count($page->findAll('css', $elementContainerSelector))) {
                    throw new Exception("Didn't find content for paginated page number " . $link->getText());
                }
            }
        }
        //check the very last page
        $lastLink = $container->findLink("Go to page " . $last_page_num);
        if (empty($lastLink)) {
            throw new Exception("Unable to find 'Go to last page' link.");
        }

        $lastLink->click();
        if (!count($page->findAll('css', $elementContainerSelector))) {
            throw new Exception("Didn't find content for last paginated page.");
        }
    }

    /**
     * @Then /^I should see the link "([^"]*)" with href "([^"]*)" under the "([^"]*)" heading link with href "([^"]*)" in the "([^"]*)" region$/
     */
    public function iShouldSeeTheLinkWithHrefUnderTheHeadingLinkWithHrefInTheRegion($menu_item_text, $menu_item_href, $heading_text, $heading_href, $menu_region)
    {
        $menu_region_node = $this->getRegion($menu_region);
        $menu_heading_nodes = $menu_region_node->findAll('css', 'ul.menu li a.secondary-link');
        $found_heading_text = FALSE;
        foreach ($menu_heading_nodes as $menu_heading_node) {
            $menu_heading_text = $menu_heading_node->getText();
            if (empty($menu_heading_text)) {
                $menu_heading_text = $menu_heading_node->getHtml();
            }
            if ($menu_heading_text == $heading_text) {
                $found_heading_text = TRUE;
                break;
            }
        }
        if (!$found_heading_text) {
            throw new Exception("Menu heading " . $heading_text . " was not found in the " . $menu_region . " region");
        }
        if (strpos($menu_heading_node->getAttribute('href'), $heading_href) === FALSE) {
            throw new Exception("Menu heading " . $heading_text . " found but link " . $heading_href . " is incorrect in the " . $menu_region . " region");
        }
        $heading_container_selector = $this->textToSelector($heading_text);
        //$sub_menu_items = $menu_region_node->findAll('css', 'ul.menu ul.menu li.leaf a');
        $sub_menu_items = $menu_region_node->findAll('css', 'div#monster-sub-nav-' . $heading_container_selector . ' a');
        if (!(is_array($sub_menu_items) && count($sub_menu_items) > 0)) {
            throw new Exception("Menu items under parent heading: " . $heading_text . " not found in the " . $menu_region . " region.");
        }
        $found = FALSE;
        foreach ($sub_menu_items as $href_node) {
            $link_text = $href_node->getText();
            if (empty($link_text)) {
                $link_text = $href_node->getHtml();
            }
            if ($menu_item_text == $link_text) {
                $found = TRUE;
                break;
            }
        }
        if (!$found) {
            throw new Exception("Menu item " . $menu_item_text . " under parent heading: " . $heading_text . " not found in the " . $menu_region . " region.");
        }
        if (strpos($href_node->getAttribute('href'), $menu_item_href) === FALSE) {
            throw new Exception("Menu item " . $menu_item_text . " under parent heading: " . $heading_text . " was found but its link " . $menu_item_href . " is incorrect, in the " . $menu_region . " region.");
        }
    }

    /**
     * For matching monster nav container identifiers
     */
    private function textToSelector($text)
    {
        $text = preg_replace('/[^\da-z\s]/i', '', $text);
        return strtolower(str_replace(' ', '-', $text));
    }

    /**
     * @Then /^I should see the disabled continue button in the "([^"]*)" container$/
     */
    public function iShouldSeeTheDisabledContinueButtonInTheContainer($container)
    {
        $container_node = $this->getSession()->getPage()->find('css', $container);
        if (empty($container_node)) {
            throw new Exception("Unable to find container " . $container);
        }
        $continue_button = $container_node->find('css', $container . " .form-submit.disabled");
        if (empty($continue_button)) {
            throw new Exception("Unable to find 'Continue' button in the " . $container . " region.");
        }
    }

    /**
     * @Then /^I should see the continue button in the "([^"]*)" container$/
     */
    public function iShouldSeeTheContinueButtonInTheContainer($container)
    {
        $container_node = $this->getSession()->getPage()->find('css', $container);
        if (empty($container_node)) {
            throw new Exception("Unable to find container " . $container);
        }
        /*$disabled = $contianer_node->find('css', $container ." .form-submit.disabled");
        if ($disabled) {
          throw new Exception("Continue Button found is Disabled.");
        }*/
        $continue_button = $container_node->find('css', $container . " .form-submit");
        if (empty($continue_button)) {
            throw new Exception("Unable to find 'Continue' button in the " . $container . " region.");
        }
    }

    /**
     * @Then /^I should see the select button in the "([^"]*)" region$/
     */
    public function iShouldSeeTheSelectButtonInTheRegion($region)
    {
        $region_node = $this->getRegion($region);
        $select_store_link = $region_node->findLink('Select');
        if (empty($select_store_link)) {
            throw new Exception("Unable to find 'Select' button in the " . $region . " region.");
        }
    }

    /**
     * @Given /^I have a store selected$/
     */
    public function iHaveAStoreSelected()
    {
        $store_list_path = 'stores/list';
        $url = $this->getSession()->getCurrentUrl();
        //this code block works around an IE feature that puts ?reloaded=true in the url
        $querystring_position = strpos($url, '?');
        if ($querystring_position !== false) {
            $url = substr($url, 0, $querystring_position);
            $querystring = substr($url, $querystring_position, strlen($url));
            $url .= $store_list_path . $querystring;
        } else {
            $url .= $store_list_path;
        }
        $this->getSession()->visit($url);
        $this->getMainContext()->iWaitForMilliseconds('5000');
        $inputNode = $this->getSession()->getPage()->find("css", 'input#edit-field-geo-data-latlon-address');
        if (empty($inputNode)) {
            throw new Exception('Unable to find input field: #edit-field-geo-data-latlon-address on page /stores/list');
        }
        $state = $this->getMainContext()->getRandomState();
        $inputNode->setValue($state);
        $submitNode = $this->getSession()->getPage()->find("css", 'input#edit-submit-store-lookup-by-address');
        if (empty($submitNode)) {
            throw new Exception('Unable to find submit button: #edit-submit-store-lookup-by-address on page /stores/list');
        }
        $submitNode->click();
        $this->getMainContext()->iWaitForMilliseconds('5000');
    }

    /**
     * @Given /^That my desktop recipe box is empty$/
     */
    public function thatMyDesktopRecipeBoxIsEmpty()
    {
        //explicitly adding domain to the visit call because this
        //test was going to some external domain and I couldn't figure out why
        $url = parse_url($this->getSession()->getCurrentUrl());
        //if already on myrecipebox - don't redirect there again
        if (strpos($url['path'], 'user') === FALSE) {
            $url = $url['scheme'] . '://' . $url['host'] . '/user?qt-user_profile_self=2';
            $this->getSession()->visit($url);
        }
        $region_element = $this->getRegion(self::REGION_CONTENT);

        $remove_recipe_links = $region_element->findAll('css', 'li .remove_link a');

        if (is_array($remove_recipe_links) && count($remove_recipe_links) > 0) {
            //recursive call until all stored recipes are removed from the recipe box
            foreach ($remove_recipe_links as $remove_recipe_link) {
                $remove_recipe_link->click();
                $this->thatMyDesktopRecipeBoxIsEmpty();
            }
        }
    }

    /**
     * @Given /^I select a random store$/
     */
    public function iSelectARandomStore()
    {
        $page_element = $this->getSession()->getPage();
        $make_this_my_store_buttons = $page_element->findAll('css', '.set_store input[type="submit"]');
        $clickable_mtmsbs = array();
        foreach ($make_this_my_store_buttons as $mtmsb) {
            if ($mtmsb->getValue() == 'Make This My Store') {
                $clickable_mtmsbs[] = $mtmsb;
            }
        }

        $random_index = array_rand($clickable_mtmsbs);
        $mtmsb = $clickable_mtmsbs[$random_index];

        if (empty($mtmsb)) {
            throw new Exception('Unable to find button with .set_store input[type="submit"]" and value of "Make This My Store" selector');
        }

        $torn_pod_content_node = $mtmsb->getParent()->getParent()->getParent()->getParent();
        $store_name_link = $torn_pod_content_node->find('css', '.views-field-title .field-content a');
        $store_name = $store_name_link->getText();
        $this->getMainContext()->selected_store_name = $store_name_link->getText();

        $mtmsb->click();
    }

    /**
     * @Then /^I should see the selected store name in the "([^"]*)" region$/
     */
    public function iShouldSeeTheSelectedStoreNameInTheRegion($region)
    {
        $this->getMainContext()->spin(function ($context) use ($region) {
            $region_node = $context->getRegion($region);
            $store_in_header = $region_node->find('css', '#block-wfm-welcome-block-wfm-welcome-block .content .welcome-box .store-box a');

            if (empty($store_in_header)) {
                throw new Exception('Unable to locate store name container in the . "header" . region identified by "#block-wfm-welcome-block-wfm-welcome-block .content .welcome-box .store-box a"');
            }

            $store_name = $store_in_header->getText();
            if (stripos($store_name, $context->selected_store_name) === FALSE) {
                throw new Exception('Found store name container in the ' . $region . ' region but store names do not match. Found: ' . $store_name . ', expected ' . $context->selected_store_name);
            }
            return TRUE;
        });
    }

    /**
     * @Given /^there should be at least "([^"]*)" "([^"]*)" elements with display "([^"]*)" in the "([^"]*)" region$/
     */
    public function thereShouldBeAtLeastElementsWithDisplayInTheRegion($min, $selector, $block_state, $region) {
        // Check if the region exists
        $element = $this->getRegion($region);

        // Do any elements called $selector exist in our region
        $find = $element->findall('css', $selector);
        if (empty($find)) {
            throw new Exception("No elements of " . $selector . " were found on the page.");
        }

        // use jQuery to count display:$block_state elements
        $counter = $this->getSession()->getDriver()->evaluateScript(
            "return jQuery('.views-row').css('display','$block_state').size();"
        );

        if ($counter < $min){
            throw new Exception('Expected ' . $count . 'elements but found' . $counter );
        }
    }

  /**
   * @Then /^I should see coupons in the "([^"]*)" region$/
   */
  public function iShouldSeeCouponsInTheRegion($region) {
    $region = $this->getRegion($region);
    $coupons = $region->findAll('css', '.view-id-coupons .views-row');
    if (count($coupons) <= 3) {
      throw new Exception('There are three or fewer coupons.');
    }
  }

  /**
   * @Given /^I should see sales items in the "([^"]*)" region$/
   */
  public function iShouldSeeSalesItemsInTheRegion($region) {
    $region = $this->getRegion($region);
    $sales = $region->findAll('css', '.view-id-sales_items .views-row');
    if (count($sales) <= 3) {
      throw new Exception('There are three or fewer sales items.');
    }
  }

}
