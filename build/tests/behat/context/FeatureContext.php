<?php
use  Behat\Behat\Context\ClosuredContextInterface;
use  Behat\Behat\Context\TranslatedContextInterface;
use  Behat\Behat\Context\BehatContext;
use  Behat\Gherkin\Node\PyStringNode;
use  Behat\Gherkin\Node\TableNode;
use  Drupal\Component\Utility\Random;
use  Behat\Mink\Element\NodeElement;
use  Behat\Behat\Context\Step\Then;

// Require 3rd-party libraries here:
// require_once 'PHPUnit/Autoload.php';

/**
 * Features context.
 */
class FeatureContext extends Drupal\DrupalExtension\Context\DrupalContext    
{
  //associated with email:
  //wfmjanraintest@hotmail.com
  //PW: EATy0urVEGGIE$
  //Janrain user ID
  const TEST_JANRAIN_UUID = '7c152da3-8e51-4b31-84db-9512f2751d40';
  const TEST_USER_NAME = 'WFM JanrainTest';
  const TEST_USER_PW= 'EATy0urVEGGIE$';
  const TEST_USER_EMAIL = 'wfmjanraintest@hotmail.com';
  const STATES = 'Alabama,Alaska,Arizona,Arkansas,California,Colorado,Connecticut,Delaware,District of Columbia,Florida,Georgia,Hawaii,Idaho,Illinois,Indiana,Iowa,Kansas,Kentucky,Louisiana,Maine,Maryland,Massachusetts,Michigan,Minnesota,Mississippi,Missouri,Montana,Nebraska,Nevada,New Hampshire,New Jersey,New Mexico,New York,North Carolina,North Dakota,Ohio,Oklahoma,Oregon,Pennsylvania,Rhode Island,South Carolina,South Dakota,Tennessee,Texas,Utah,Vermont,Virginia,Washington,West Virginia,Wisconsin';
  const CHECKED = "checked";
  const UNCHECKED = "unchecked";
  const LOGIN_URL = 'https://users.wholefoodsmarket.com/oauth/signin';
  
  //Used for maintaining state between lines of Gherkin tests
  public $selected_url;
 
 //Used for maintaining state between lines of Gherkin tests
  public $selected_text;
  
  public $found_link_text = FALSE;
  
  public $return = FALSE;
  
  public $selected_store_name = FALSE;
  
  public $selected_region = FALSE;
  /**
   * Initializes context.
   * Every scenario gets its own context object.
   *
   * @param array $parameters context parameters (set them up through behat.yml)
   */
  public function __construct(array $parameters) {
    // Initialize your context here
    $this->useContext('mobile', new MobileSubContext(array()));
    $this->useContext('desktop', new DesktopSubContext(array()));    
  }

  public function spinGetRegion($region) {
    $this->spin(function($context) use ($region) {
      $context->selected_region = $context->getRegion($region);
      return TRUE;
    });
    return $this->selected_region;
  }
  
  /**
   * @Given /^That the WFM API is Dev$/
   */
  public function thatTheWfmApiIsDev() {
    $this->getDriver()->drush('vset wfm_variable__wfmapi_host api.wholefoodsmarket.com:1800 --yes');
  }

  /**
   * @Given /^That the WFM API is Prod$/
   */
  public function thatTheWfmApiIsProd() {
    $this->getDriver()->drush('vset wfm_variable__wfmapi_host api.wholefoodsmarket.com --yes');
  }


  /**
   * @Given /^I wait for "([^"]*)" milliseconds$/
   */
  public function iWaitForMilliseconds($millis) {
    $this->getSession()->wait($millis);
  } 

  /**
   * @Given /^via Javascript I enter "([^"]*)" for "([^"]*)"$/
   * This one is to get around "invisible" form elements that are indeed visible
   */
  public function viaJavascriptIEnterFor($arg1, $arg2) {
    $script = 'jQuery("' . $arg2 . '").attr("value", "' . $arg1 . '");';
    $this->getSession()->executeScript($script);
  }  

  /**
   * @When /^I submit the "([^"]*)" form via Javascript$/
   * This one is to get around "invisible" form elements that are indeed visible
   */
  public function iSubmitTheFormViaJavascript($jquery_selector) {
    $script = 'jQuery("' . $jquery_selector . '").submit();';
    $this->getSession()->executeScript($script);
    $this->getSession()->wait(5000, '(0 === jQuery.active)');
  } 

  /**
   * @When /^I click "([^"]*)" via Javascript$/
   */
  public function iClickViaJavascript($jquery_selector) {
    $script = 'jQuery("' . $jquery_selector . '").click();';
    $this->getSession()->executeScript($script);
    $this->getSession()->wait(5000, '(0 === jQuery.active)');
  }

  /**
   * @Then /^I should see a "([^"]*)" element "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeAnEnabledOrDisabledElementInTheRegion($state, $selector, $region_name) {
    $region_node = $this->getRegion($region_name);
    if (empty($region_node)){
      throw new Exception("Unable to find region " . $region_name);
    }
    $button_node = $region_node->find('css', $selector);
    if (empty($button_node)) {
      throw new Exception("Unable to find element with selector " . $selector);
    }

    $result = $this->getSession()->getDriver()->evaluateScript(
      "return jQuery('" . $selector . ":" . $state . "') ? true : false;"
    );

    if (!$result) {
      throw new Exception('Element ' . $selector . ' with state ' . $state . ' not found.');
    }
  }
  
  /**
   * @Then /^"([^"]*)" should not be visible$/
   */
  public function shouldNotBeVisible($selector) {
    $this->spin(function($context) use ($selector) {
      $element = $context->getSession()->getPage()->find('css', $selector);
      if (empty($element)) {
        throw new Exception("Element ({$selector}) not found.");
      } 
      $style = preg_replace('/\s/', '', $element->getAttribute('style'));
      if (strpos($style, 'display:none') === FALSE) {
        throw new Exception("Element ({$selector}) is visible when it should not be.");
      }
      return TRUE;
    });
  }

  /**
   * @Then /^I should see an image in the "([^"]*)" container$/
   * @Then /^I should see an image in container "([^"]*)"$/
   * @Then /^I should see an image "([^"]*)"$/
   */
  public function iShouldSeeAnImageInContainer($container_node_or_name) {

    //if a container node is passed in, use it directly 
    $container = $container_node_or_name;
    
    //if a container selector string is supplied, try to find the container node
    if (is_string($container_node_or_name)) {
      $container = $this->getSession()->getPage()->find('css', $container_node_or_name);
      if (empty($container)) {
        throw new Exception("Unable to find container: " . $container_node_or_name);
      }
    }

    $img_node = $container->find('css', 'img');
    if (!$img_node->hasAttribute('src')) {
      throw new Exception("No image tag found in container: " . $container_node_or_name);
    }
    $img_src = $img_node->getAttribute('src');
    if (empty($img_src)) {
      throw new Exception("Unable to find src for image in: " . $container_node_or_name);
    }
  }

  /**
   * @Then /^I should see an image in container "([^"]*)" in the "([^"]*)" region$/
   * @Then /^I should see an image in the "([^"]*)" container in the "([^"]*)" region$/
   */
  public function iShouldSeeAnImageInContainerInTheRegion($selector, $region) {
    $container_node = $this->containerInRegion($selector, $region);
    $this->iShouldSeeAnImageInContainer($container_node);
  }
  
  /**
   * @Then /^I should see search results for "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeSearchResultsForInTheRegion($search_query, $region) {
    $region_node = $this->getRegion($region);
    $search_result_rows = $region_node->findAll('css', 'li.search-result');
    if (count($search_result_rows) == 0) {
      throw new Exception('Search results expected. No search results returned for search "' . $search_query . '"');
    }
  }
  
  /**
   * @Then /^I should see no search results for "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeNoSearchResultsForInTheRegion($search_query, $region) {
    $region_node = $this->getRegion($region);
    $search_result_rows = $region_node->findAll('css', '.search_result_row');
    $how_many_results = count($search_result_rows);
    if ($how_many_results > 0) {
      throw new Exception('No search results expected. ' . $how_many_results . ' search results returned for search "' . $search_query . '"');
    }
  }
  
  /**
   * @Then /^I should see Pluck Ratings$/
   */
  public function iShouldSeePluckRatings() {
    $element = $this->getSession()->getPage();
    if (!$element->find('css', '.plck-app-container-loaded')) {
      throw new Exception('Pluck recipe ratings not loaded');
    }
  }
  
  /**
   * @Then /^I should see list items with parent "([^"]*)" "([^"]*)"$/
   */
  public function iShouldSeeListItemsWithParent($id_or_class, $id_or_class_value) {
    $element = $this->getSession()->getPage();
    if (!($ul_node = $element->find('css', 'ul' . $this->idOrClassSeparator($id_or_class) . $id_or_class_value))) {
      throw new Exception('Unable to find ul with ' . $id_or_class . ': ' . $id_or_class_value);
    }
    $list_item_nodes = $ul_node->findAll('css', 'li');
    if (count($list_item_nodes) == 0) {
      throw new Exception('Zero ingredients loaded');  
    }
  }
  
  /**
   * @Then /^I should see a link with href "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeALinkWithHrefInTheRegion($href, $region) {
    $region_node = $this->getRegion($region);
    $link_nodes = $region_node->findAll('css', 'a');
    $found_link_text = FALSE;
    foreach ($link_nodes as $link_node) {
      if (strpos($link_node->getAttribute('href'), $href) !== FALSE) { //doing strpos instead of == to ignore the potential presence of a querystring in the href
        return;
      }
    }

    throw new Exception('Unable to locate link with href: ' . $href . ' in the ' . $region . ' region');
  }
  
   /**
   * @Then /^I should see the "([^"]*)" link with href "([^"]*)" in the "([^"]*)" container in the "([^"]*)" region$/
   */
  public function iShouldSeeTheLinkWithHrefInTheContainerInTheRegion($link_text, $href, $container_selector, $region) {
    $container = $this->containerInRegion($container_selector, $region);
    
    $this->iShouldSeeTheLinkWithHrefInTheRegion($link_text, $href, $container);
  }
  
  /**
   * @Then /^I should see the link "([^"]*)" with href "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeTheLinkWithHrefInTheRegion($link_text, $href, $region_name_or_container) {
    $parent_node = $region_name_or_container;
    if (is_string($region_name_or_container)) {
      $parent_node = $this->getRegion($region_name_or_container);      
    }

    $link_nodes = $parent_node->findAll('css', 'a');
    $found_link_text = FALSE;
    foreach ($link_nodes as $link_node) {
      $text = $link_node->getText();
      if (empty($text)) {
        $text = trim(strip_tags($link_node->getHtml()));  
      }

      //Some spaces (c2a0) were not being interpreted as standard spaces - likely a cut and poste issue in content in Drupal
      //for details see: http://stackoverflow.com/questions/12837682/non-breaking-utf-8-0xc2a0-space-and-preg-replace-strange-behaviour
      //A non breaking space is U+00A0 (Unicode) but encoded as C2A0 in UTF-8
      $cleaned_link_text = preg_replace('/[\s\x{00a0}]+/u', '\s', $link_text);

      //If our link has a / in it, we're searching for, we need to escape it with \/
      $cleaned_link_text = preg_replace('~/~', '\\/', $cleaned_link_text);

      //Build regex used cleaned link.
      $regex = '/^' . $cleaned_link_text . '$/iu';

      //case insensitive string comparison      
      if (strlen($text) > 0 && preg_match($regex, $text)) { 
        $found_link_text = TRUE;
        break;
      } elseif (strpos($text, $link_text) !== FALSE) {
        $found_link_text = TRUE;
        break;
      }
    }
    if (!$found_link_text) {
      throw new Exception('Unable to locate link with text: ' . $link_text);
    } 
    if (strpos($link_node->getAttribute('href'), $href) === FALSE) { //doing strpos instead of == to ignore the potential presence of a querystring in the href
      throw new Exception('Unable to locate link with href: ' . $href);
    }
  }
  
  /**
   * @Then /^I should see the image link with src "([^"]*)" with href "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeTheImageLinkWithSrcWithHrefInTheRegion($image_src, $link_href, $region) {
    $region_element = $this->getRegion($region);
    
    $link_elements = $region_element->findAll('css', 'a');
    $found_link_href = FALSE;
    $found_img_src = FALSE;
    foreach ($link_elements as $link_element) {
      if ($link_element->hasAttribute('href')) {
        $current_href = $link_element->getAttribute('href');
        if (strpos($current_href, $link_href) !== FALSE) {
          $found_link_href = TRUE;
        }
        
        $img_tag = $link_element->getHtml();
        if (strpos($img_tag, $image_src) !== FALSE) {
            $found_img_src = TRUE;
        }
      } 
    }
    
    if (!$found_link_href) {
      throw new Exception('Unable to locate link with href fragment: ' . $link_href . ' in the ' . $region . ' region');
    }

    if (!$found_img_src) {
      throw new Exception('Unable to locate image with src fragment: ' . $image_src . ' in the ' . $region . ' region');
    }
  }
  
  /**
   * @Then /^the page should contain "([^"]*)" "([^"]*)" tag with ID or class "([^"]*)" "([^"]*)" in the "([^"]*)" region$/
   */
  public function thePageShouldContainTagWithIdOrClassInTheRegion($required_count, $tag, $id_or_class, $id_or_class_value, $region) {
    $selector = $tag . $this->idOrClassSeparator($id_or_class) . $id_or_class_value; 
    $region_node = $this->getRegion($region);
    $div_nodes = $region_node->findAll('css', $selector);
    $div_node_count = count($div_nodes);
    if ($div_node_count == 0) {
      throw new Exception('Unable to locate any "' . $tag . '" tags with class name "' . $id_or_class_value . '" in the "' . $region . '" region ');
    } 
    if ($div_node_count > $required_count) {
      throw new Exception('Found too many ' . $tag . ' tags with class name ' . $id_or_class_value . ' in the ' . $region . ' region. Found ' . $div_node_count . ', expected ' . $required_count . '.');
    }
  }
  
  /**
   * @When /^I select radio button with value "([^"]*)"$/
   */
  public function iSelectRadioButtonWithValue($radio_button_value) {
    $script = 'jQuery(":radio[value=' . $radio_button_value . ']").attr("checked", true)';
    $this->getSession()->executeScript($script);
  }
  
  /**
   * @When /^I ammend radio button with value "([^"]*)" to be "([^"]*)" and then select it$/
   */
  public function iAmmendRadioButtonWithValueToBeAndThenSelectIt($radio_button_value, $new_radio_button_value) {
    $script = 'jQuery(":radio[value=' . $radio_button_value . ']").attr("value", "' . $new_radio_button_value . '").attr("checked", true)';
    $this->getSession()->executeScript($script);
  }
    
  /**
   * @Then /^the url and querystring should match "([^"]*)"$/
   */
  public function theUrlAndQuerystringShouldMatch($url) {
    $browser_url = $this->getSession()->getCurrentUrl();
    if (strpos($browser_url, $url) === FALSE) {
      throw new Exception('Browser URL "' . $browser_url . '" does not match test URL "' . $url . '"');
    }
  }
  
  /**
   * @Then /^I switch to window "([^"]*)"$/
   */
  public function iSwitchToWindow($window_name) {
    $this->getSession()->wait(3000);
    $this->getSession()->switchToWindow($window_name);
  }
  
  /**
   * @Then /^I close window$/
   */
  public function iCloseWindow() {
    $this->getSession()->executeScript('window.close();');
  }

  /**
   * @Then /^restart session$/
   */
  public function restartSession() {
    $this->getSession()->restart();
  }
  
  /**
   * @Then /^stop session$/
   */
  public function stopSession() {
    $this->getSession()->stop();
  }
  
  /**
   * @When /^I click the first link in the container "([^"]*)" "([^"]*)" in the "([^"]*)" region$/
   */
  public function iClickTheFirstLinkInTheContainerInTheRegion($id_or_class, $id_or_class_value, $region) {
    $region_node = $this->getRegion($region);
    if (!($container = $region_node->find('css', $this->idOrClassSeparator($id_or_class) . $id_or_class_value))) {
      throw new Exception('Unable to find container with ' . $id_or_class . ' ' . $id_or_class_value);
    }
    $first_link = $container->find('css', 'a');
    $url = $first_link->getAttribute("href");
    $this->getSession()->executeScript('window.open("' . $url . '", "testWindow")');
    $this->getSession()->switchToWindow('testWindow');
  }
    
  /**
   * Authenticates a test user.
   *
   * @Given /^I am logged in as a test user$/
   */
  public function iAmLoggedInAsATestUser() {
    $this->getSession()->visit('user/janrain_user');
  }
  
  /**
   * @Then /^I login as a test user$/
   */
  public function iLoginAsATestUser() {
    // Check if a user is already logged in.
    if ($this->loggedIn()) {
      return TRUE;
    }
    // Create user (and project)
    $user = (object) array(
      'name' => self::TEST_USER_EMAIL,
      'mail' => self::TEST_USER_EMAIL,
      'pass' => self::TEST_USER_PW,
      'role' => 'authenticated user'
    );
    $this->users[$user->name] = $this->user = $user;
    $this->spin(function($context) {    
      $context->login();
      return TRUE;      
    });
  }
  
  /**
   * Overrides DrupalContext's login method. Uses a janrain login page rather than Drupal's default.
   */
  public function login() {
    $current_url = $this->getSession()->getCurrentUrl();
    
    //if (strpos($current_url, self::LOGIN_URL) === FALSE) {
    //  throw new Exception('Not on the Janrain login page, unable to login');
    //}
    
    $element = $this->getSession()->getPage();
    
    if ($element->has('css', 'iframe')) {
      $iframes = $element->findAll('css', 'iframe');
      foreach($iframes as $iframe) {
        if (strpos($iframe->getAttribute('src'), '/user/janrain_register') !== FALSE) {
          $iframe_name = $iframe->getAttribute('name');
          $this->getSession()->switchToIFrame($iframe_name);
          break;
        }
      }
    }

    //$element->fillField('email_input', $this->user->name);
    $this->getSession()->executeScript('jQuery("#email_input").val("' . $this->user->name . '")');
    $this->getSession()->executeScript('jQuery("#password_input").show().val("' . $this->user->pass . '")');
    $submit = $element->findButton('signin');      
    
    if (empty($submit)) {
      throw new \Exception(sprintf("No submit button at %s", $this->getSession()->getCurrentUrl()));
    }
    
    // Log in.
    $submit->click();
  }
  
  /**
   * @Then /^I should see test username in the "([^"]*)" region$/
   */
  public function iShouldSeeTestUsernameInTheRegion($region) {
    $this->loggedIn($region);
  }
  
  /**
   * Determine if the a user is already logged in.
   */
  public function loggedIn($region = 'header') {

    $is_logged_in = FALSE;
        
    if (!$this->getSession()->getPage()->find('region', $region)) {
      return $is_logged_in;  
    }
    
    $region_element = $this->getRegion($region);
    $header_text = $region_element->getText();
   

    if (stripos($header_text, "Welcome, " . self::TEST_USER_NAME) !== FALSE) {
      $is_logged_in = TRUE;
    }

    return $is_logged_in;
  }
  
 /**
  * @Then /^the "([^"]*)" element should contain at least "([^"]*)" "([^"]*)" elements$/
  */
  public function theElementShouldContainAtLeastElements($parent_element_or_selector, $min_number_children, $child_elements_selector) {
    
    $container = $parent_element_or_selector;
      
    if (is_string($parent_element_or_selector)) {
      $element = $this->getSession()->getPage();
      $parent_containers = $element->findAll('css', $parent_element_or_selector);
      if (is_array($parent_containers) && count($parent_containers) > 1) {
        throw new Exception("More than one parent container found. Please supply an element selector that will return a single result.");
      }
      if (!($container = $element->find('css', $parent_element_or_selector))) {
        throw new Exception("Unable to find parent element " . $parent_element_or_selector);
      }      
    }

    if (!($children = $container->findAll('css', $child_elements_selector))) {
      throw new Exception("Unable to find " . $child_elements_selector . " child elements");
    }
    $how_many_children = count($children);
    if ($how_many_children < $min_number_children) {
      throw new Exception("Insufficient child nodes found for parent element " . $parent_element . ". " . $how_many_children . " were found, and " . $min_number_children . " are required.");
    }
  }
  
  /**
   * @Then /^the "([^"]*)" element should contain at least "([^"]*)" "([^"]*)" elements in the "([^"]*)" region$/
   */
  public function theElementShouldContainAtLeastElementsInTheRegion($parent_element, $min_number_children, $child_elements_selector, $region) {
    $this->getRegion($region);
    $this->theElementShouldContainAtLeastElements($parent_element, $min_number_children, $child_elements_selector);
  }
  
  /**
   * @Then /^I should see the text "([^"]*)" or at least "([^"]*)" elements identified by "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeTheTextOrAtLeastElementsIdentifiedByInTheRegion($text, $min_number_children, $child_elements_selector, $region) {
    $region_element = $this->getRegion($region);
    
    $region_text = $region_element->getText();
    
    if (stripos($region_text, $text) !== FALSE) {
      return;
    }
    
    $this->theElementShouldContainAtLeastElements($region_element, $min_number_children, $child_elements_selector);
  }
  
  /**
   * Only works if using the Drupal API driver which requires  
   * that tests on the same server as the code base is on.  
   * 
   * @Given /^That My Recipe Box is Empty$/
   */
  public function thatMyRecipeBoxIsEmpty() {
    global $user;
    if ($lists = $this->getUserRecipes()) {
      foreach($lists as $list) {
        WFMApiRecipeList::deleteItem($user->uid, $list['node']->nid);
      }      
    }
  }
  
  /**
   *Only works if using the Drupal API driver which requires  
   * that tests on the same server as the code base is on. 
   *
   * @Then /^my recipe box should be empty$/
   */
  public function myRecipeBoxShouldBeEmpty() {
    if (!$this->getUserRecipes()) {
      throw new Exception("Recipe box is not empty");
    }
  }
  
  /**
   * Get all recipes for a given user account
   * Only works if using the Drupal API driver which requires  
   * that tests on the same server as the code base is on. 
   */
  private function getUserRecipes() {
    global $user;
    if (!($user = user_load_by_name($this->user->name))) {
      throw new Exception("Unable to load user with username: " . $this->user->name);
    }
    $lists = WFMApiRecipeList::getList($user->uid);
    if (is_array($lists) && count($lists) > 0) {       
      return $lists;
    }
    return FALSE;
  }
     
  /**
   * Returns '.' or "#"
   */
  private function idOrClassSeparator($id_or_class) {
    $separator = '#';
    if ($id_or_class == 'class') {
      $separator = '.';
    }
    return $separator;
  }
  
  /**
   * Validates that an image exists.
   */
  private function curl_image($img_src) {
    $handle = curl_init($img_src);
    curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
    /* Get the HTML or whatever is linked in $url. */
    $response = curl_exec($handle);
    /* Check for 404 (file not found). */
    $http_code = curl_getinfo($handle, CURLINFO_HTTP_CODE);
    if ($http_code != 200) {
      throw new Exception("image not found, http response " . $http_code);
    }
    curl_close($handle);
    return;
  } #This function breaks while pointing at prod. Permissions denied?
  
  /**
   * @Then /^"([^"]*)" should be visible$/
   */
  public function shouldBeVisible($selector) {
    $this->spin(function($context) use ($selector) {
      $element = $context->getSession()->getPage()->find('css', $selector);
      $style = '';
      if (empty($element)) {
        throw new WfmBehatException("Element ({$selector}) not found"); 
      }
      $style = preg_replace('/\s/', '', $element->getAttribute('style'));
      if (strpos($style, 'display:none') !== FALSE) {
        throw new WfmBehatException("Element ({$selector}) is not visible when it should be.");
      }
      return TRUE;
    });
  }
    
  /**
   * @Given /^I should see at least "([^"]*)" "([^"]*)"$/
   */
  public function iShouldSeeAtLeast($count, $selector) {
    $this->spin(function($context) use ($count, $selector) {
      $actual = count($context->getSession()->getPage()->findAll('css', $selector)); 
      if ($actual < $count) {
        throw new Exception("Saw " . $actual . " elements and was expecting at least " . $count . " elements with selector : \'" . $selector . "\'");
      }
      return TRUE;
    }); 
  }
  
  /**
   * Click on the element with the provided xpath query
   *
   * @When /^I click on the element with css selector "([^"]*)"$/
   */
  public function iClickOnTheElementWithCssSeletor($css) {
    $this->spin(function($context) use ($css) {
      $session = $context->getSession();
      $element = $session->getPage()->find('xpath', $session->getSelectorsHandler()->selectorToXpath('css', $css));
      if (null === $element) {
        throw new Exception('Could not find element '.$css);
      }
      $element->click();
      return TRUE;  
    });
  }

  /**
   * @Given /^each article should have a title, author, comment link and read more link$/
   */
  public function eachArticleShouldHaveATitleAuthorCommentLinkAndReadMoreLink() {
    //find the articles for this page and check they have all the right parts 
    $articles = $this->getSession()->getPage()->findAll('css', ".view-id-blogs .views-row .torn-pod-content");
    $selectors = array(".blog-title a", ".blog-author", ".more-posts-by-link", ".views-more-link", ".post-comment-count");
    foreach($articles as $article) {
      foreach ($selectors as $selector) {
        $title = $article->find('css', $selector)->getText();
        if(!$title){
          throw new Exception("Didn't find " . $selector . " element for " . $article);
        }   
      }
    }
  }

  /**
   * @Given /^I wait for ajax calls to finish$/
   */
  public function iWaitForAjaxCallsToFinish() {
    //$this->ajaxClickHandlerBefore();
    $this->getSession()->wait(5000, '(0 === jQuery.active)');
  }
      
  /**
   * @Then /^I should see a heading in the "([^"]*)" region$/
   */
  public function iShouldSeeAHeadingInTheRegion($region) {
    $region_node = $this->getRegion($region);
    $heading_node = $region_node->find('css', 'h2') || 
    $heading_node = $region_node->find('css', 'h3') ||
    $heading_node = $region_node->find('css', 'h4');
    if (empty($heading_node)) {
      throw new Exception("Unable to locate a heading (h2, h3, h4) in the " . $region . " region.");
    }
  }

  /**
   * @Then /^I should see the link "([^"]*)" in the "([^"]*)" container$/
   */
  public function iShouldSeeTheLinkInTheContainer($link_text, $container_selector) {
    $container = $this->getSession()->getPage()->find('css', $container_selector); 
    if (empty($container)) {
      throw new Exception("Unable to find container: " . $container_selector);
    }
    $link_node = $container->findLink($link_text);
    if (empty($link_node)) {
      throw new Exception("Unable to find link with text: " . $link_text . " in container: " . $container_selector);
    }
  }

  /**
   * @Then /^I should see the link "([^"]*)" in the "([^"]*)" container with href "([^"]*)"$/
   */
  public function iShouldSeeTheLinkInTheContainerWithHref($link_text, $container_selector, $href) {
    $container = $this->getSession()->getPage()->find('css', $container_selector); 
    if (empty($container)) {
      throw new Exception("Unable to find container: " . $container_selector);
    }
    $link_node = $container->findLink($link_text);
    if (empty($link_node)) {
      throw new Exception("Unable to find link with text: " . $link_text . " in container: " . $container_selector);
    }
    $address = $link_node->getAttribute('href');
    if (strpos($address, $href) === false) {
      throw new Exception("Link address does not match href: " . $href);
    }
  }

  /**
   * @When /^I hover over the element "([^"]*)"$/
   */
  public function iHoverOverTheElement($locator) {
    $element = $this->getSession()->getPage()->find('css', $locator);
    if (null === $element) {
      throw new Exception("Could not find container: " . $locator);
    }
    $element->mouseOver();
  }

  /**
   * @Then /^I show the "([^"]*)" container$/
   */
  public function iShowInTheContainer($container) {
    $session = $this->getSession();
    $element = $session->getPage()->find('css', $container); 
    if (empty($element)) {
      throw new Exception("Unable to find container: " . $container);
    }
    $script = "jQuery('$container').css('height', '120px');";
    $this->getSession()->executeScript($script);
  }

  /**
   * @Then /^I should see "([^"]*)" "([^"]*)" elements in the "([^"]*)" container$/
   */
  public function iShouldSeeElementsInTheContainer($countOfListElementInstances, $listElement, $listContainer) {
    if (count($this->getSession()->getPage()->findAll('css', $listContainer)) > 1) {
      throw new Exception("Found more than one container named: " . $listContainer . ". Function should only receive id with no possible duplicates/copies on page.");
    }
    $container = $this->getSession()->getPage()->find('css', $listContainer); 
    if (empty($container)) {
      throw new Exception("Unable to find container: " . $listContainer);
    }
    $list_nodes = $container->findAll('css', $listElement);
    if (count($list_nodes) == 0) {
      throw new Exception("Zero elements named: " . $listElement . " found in " . $listContainer);
    }
    if (count($list_nodes) != $countOfListElementInstances) {
      throw new Exception("Container " . $listContainer . " does not hold " . $countOfListElementInstances . " instances of " . $listElement . ".");
    }
  }

  /**
   * @Then /^I should see an image in each "([^"]*)" element in the "([^"]*)" container$/
   */
  public function iShouldSeeAnImageInEachElementInTheContainer($listElement, $listContainer) {
    if (count($this->getSession()->getPage()->findAll('css', $listContainer)) > 1) {
      throw new Exception("Found more than one container named: " . $listContainer . ". Function should only receive id with no possible duplicates/copies on page.");
    }
    $container = $this->getSession()->getPage()->find('css', $listContainer); 
    if (empty($container)) {
      throw new Exception("Unable to find container: " . $listContainer);
    }
    $list_nodes = $container->findAll('css', $listElement);
    if (count($list_nodes) == 0) {
      throw new Exception("Zero elements named: " . $listElement . " found in " . $listContainer);
    }
    for ($index = 0; $index < count($list_nodes); $index++) {
      $this->iShouldSeeAnImageInContainer($list_nodes[$index]);
    }
  }

  /**
   * @Then /^I should see the text "([^"]*)" in count "([^"]*)" of "([^"]*)" element in the "([^"]*)" container$/
   */
  public function iShouldSeeTheTextInCountOfElementInTheContainer($text, $elementCountInList, $listElement, $listContainer) {
    if (count($this->getSession()->getPage()->findAll('css', $listContainer)) > 1) {
      throw new Exception("Found more than one container named: " . $listContainer . ". Function should only receive id with no possible duplicates/copies on page.");
    }
    //@here
    $container = $this->getSession()->getPage()->find('css', $listContainer); 
    if (empty($container)) {
      throw new Exception("Unable to find container: " . $listContainer);
    }
    $list_nodes = $container->findAll('css', $listElement);
    if (count($list_nodes) == 0) {
      throw new Exception("Zero elements named: " . $listElement . " found in " . $listContainer);
    }
    $retrieved_text = $list_nodes[($elementCountInList - 1)]->getText();
    if (empty($retrieved_text)) {
      throw new Exception("Unable to find text: " . $text . " in count " . $elementCountInList . " of " . $listElement . " in the " . $listContainer . " container.");
    }
    $positive = strpos($retrieved_text, $text);
    if ($positive === false) {
      throw new Exception("Text " . $text . " does not match text in container.");
    }
  }
 
  /**
   * @Then /^I should see the link "([^"]*)" with href "([^"]*)" in count "([^"]*)" of "([^"]*)" element in the "([^"]*)" container$/
   */
  public function iShouldSeeTheLinkWithHrefInCountOfElementInTheContainer($link_text, $href, $elementCountInList, $listElement, $listContainer) {
    if (count($this->getSession()->getPage()->findAll('css', $listContainer)) > 1) {
      throw new Exception("Found more than one container named: " . $listContainer . ". Function should only receive id with no possible duplicates/copies on page.");
    }
    $container = $this->getSession()->getPage()->find('css', $listContainer); 
    if (empty($container)) {
      throw new Exception("Unable to find container: " . $listContainer);
    }
    $list_nodes = $container->findAll('css', $listElement);
    if (count($list_nodes) == 0) {
      throw new Exception("Zero elements named: " . $listElement . " found in " . $listContainer);
    }
    $link_node = $container->findLink($link_text);
    if (empty($link_node)) {
      throw new Exception("Unable to find link with text: " . $link_text . " in container: " . $listContainer);
    }
    $address = $link_node->getAttribute('href');
    $positive = strpos($address, $href);
    if ($positive === false) {
      throw new Exception("Link address does not match href: " . $href);
    }
  }

  /**
   * @Then /^I should see the href "([^"]*)" in count "([^"]*)" of "([^"]*)" element in the "([^"]*)" container$/
   */
  public function iShouldSeeTheHrefInCountOfElementInTheContainer($href, $elementCountInList, $listElement, $listContainer) {
    if (count($this->getSession()->getPage()->findAll('css', $listContainer)) > 1) {
      throw new Exception("Found more than one container named: " . $listContainer . ". Function should only receive id with no possible duplicates/copies on page.");
    }
    $container = $this->getSession()->getPage()->find('css', $listContainer); 
    if (empty($container)) {
      throw new Exception("Unable to find container: " . $listContainer);
    }
    $list_nodes = $container->findAll('css', $listElement);
    if (count($list_nodes) == 0) {
      throw new Exception("Zero elements named: " . $listElement . " found in " . $listContainer);
    }
    $count = $elementCountInList - 1;
    $element = $list_nodes[$count]->findAll('css', 'a');
    $index = 0;
    foreach ($element as $key => $item) {
      if ($item->getAttribute('href') == $href) {
        return;
      }
    }
    throw new Exception("Unable to find href " . $href);
  }

  /**
   * @Then /^I should see the "([^"]*)" link with href "([^"]*)" in the "([^"]*)" container in "([^"]*)"$/
   */
  public function iShouldSeeTheLinkWithHrefInTheContainerIn($link_text, $link_href, $podContainer, $blockRowContainer) {
    $region = $this->getSession()->getPage()->find('css', $blockRowContainer); 
    if (empty($region)) {
      throw new Exception("Unable to find container: " . $blockRowContainer);
    }
    $block = $region->find('css', $podContainer);
    if (empty($block)) {
      throw new Exception("Unable to find container: " . $podContainer . " in container " . $blockRowContainer);
    }
    $link_node = $block->findLink($link_text);
    if (empty($link_node)) {
      throw new Exception("Unable to find link with text: " . $link_text . " in container " . $podContainer . " in container " . $blockRowContainer);
    }
    $link_address = $link_node->getAttribute('href');
    if (strpos($link_address, $link_href) === false) {
      throw new Exception("Link address with text: " . $link_text . " does not match href: " . $link_href);
    }
  }
  
  /**
   * @Then /^I should see a submit button in the "([^"]*)" container with value "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeASubmitButtonInTheContainerWithValueInTheRegion($container_selector, $value, $region) {
    $container = $this->containerInRegion($container_selector, $region);
    $button = $container->findAll('css', 'input[type="submit"]');
    foreach ($button as &$submitbutton) {
      if (stripos($submitbutton->getAttribute('value'), $value) !== FALSE) {
        return;
      }
    }
    throw new Exception('Unable to find submit button with value ' . $value . ' in container: ' . $container_selector . ' in the ' . $region . ' region.');
  }
  
  /**
   * @Given /^I press the submit button with value "([^"]*)" in the "([^"]*)" container in the "([^"]*)" region$/
   */
  public function iPressTheSubmitButtonWithValueInTheContainerInTheRegion($submit_button_text, $selector, $region) {
    $container = $this->containerInRegion($selector, $region);
    
    $submit_buttons = $container->findAll('css', 'input[type="submit"]');
    foreach ($submit_buttons as $submit_button) {
      if ($submit_button->getAttribute('value') === $submit_button_text) {
        $submit_button->click();
        return;
      }
    }
    throw new Exception("Unable to find submit button with value '" . $submit_button_text . "' in container '" . $selector . "' in the region '" . $region . "'");
  }

  /**
   * @Then /^I should see a link in the "([^"]*)" container in the "([^"]*)" region$/
   */
  public function iShouldSeeALinkInTheContainerInTheRegion($container_selector, $region) {
    $container = $this->containerInRegion($container_selector, $region);
    $link_node = $container->findLink('');
    if (empty($link_node)) {
      throw new Exception("Unable to find a link in container: " . $container_selector . ' in the ' . $region . ' region.');
    }
  }

  /**
   * @Then /^I should see the promo content in "([^"]*)" in "([^"]*)"$/
   */
  public function iShouldSeeThePromoContentInIn($promoContentContainerName, $blockName) {
    $block = $this->getSession()->getPage()->find('css', $blockName);
    if (empty($block)) {
      throw new Exception("Unable to find container: " . $blockName);
    }
    $container = $block->find('css', $promoContentContainerName);
    if (empty($container)) {
      throw new Exception("Unable to find promo content in : " . $promoContentContainerName . " in " . $blockName . "!!!");
    }
  }

  /**
   * @Then /^I should see the text "([^"]*)" in the "([^"]*)" container$/
   */
  public function iShouldSeeTheTextInTheContainer($text_to_find, $container_node_or_array_or_name) {
    $this->spin(function($context) use ($text_to_find, $container_node_or_array_or_name) { 
      if (is_array($container_node_or_array_or_name)) {
        $found_text = FALSE;
        foreach($container_node_or_array_or_name as $element) {
          $element_text = $element->getText();
          if (stripos($element_text, $text_to_find) !== FALSE) {
            $found_text = TRUE;
            break;
          }
        }
        if (!$found_text) {
          throw new Exception("Unable to find text in array: " . $text_to_find);
        }
        return TRUE;
      }
      
      $container = $container_node_or_array_or_name;
      
      //string selector to select a single element
      if (is_string(($container_node_or_array_or_name))) {
        $container = $context->getSession()->getPage()->find('css', $container);
        if (empty($container)) {
          throw new Exception("Unable to find container: " . $container);
        }      
      }
      
      $get_text = $container->getText();
      if (stripos($get_text, $text_to_find) === FALSE) {
        throw new Exception("Unable to find text: " . $text_to_find);
      }
      
      return TRUE;
    });
  }
  
  /**
   * @Then /^I should see the text "([^"]*)" in the "([^"]*)" container in the "([^"]*)" region$/
   */
  public function iShouldSeeTheTextInTheContainerInTheRegion($text, $container_selector, $region) {
    $container = $this->containerInRegion($container_selector, $region);
    $this->iShouldSeeTheTextInTheContainer($text, $container);
  }

  /**
   * @Given /^I have no store selected$/
   */
  public function iHaveNoStoreSelected() {
    // delete cookie:
    $this->getSession()->setCookie('local_store', null);
    $this->getSession()->setCookie('local_store_id', null);
    $this->getSession()->setCookie('local_store_name', null);
    $this->getSession()->setCookie('local_store_url', null);        
  }

  /**
   * Helper function to load a random state to fill in forms.
   */
  public function getRandomState() {
    $states = explode(',', self::STATES);
    $index = array_rand($states);
    return $states[$index];
  }
  
  /**
   * @Then /^I should see the regex "([^"]*)" identified by "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeTheRegexIdentifiedByInTheRegion($regex, $selector, $region) {
    $region_element = $this->getRegion($region);
    $element = $region_element->find('css', $selector);
    
    if (empty($element)) {
      throw new Exception('Unable to find element with selector ' . $selector);  
    }
    
    $find = $element->getText();
    
    if (!preg_match($regex, $find)) {
      throw new Exception('Unable to find text with regex ' . $regex . ' in the ' . $region . ' region.');
    }
  }
  
  /**
   * @Then /^I should see "([^"]*)" search results identified by "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeSearchResultsIdentifiedByInTheRegion($number_visible_search_results, $selector, $region) {
    $region_element = $this->getRegion($region);
    $results = $region_element->findAll('css', $selector);
    
    if (count($results) == 0) {
      throw new Exception('Search results expected. No search results returned.');
    }
    
    if (count($results) > $number_visible_search_results) {
      throw new Exception($number_visible_search_results . ' search results expected. ' . count($results) . ' search results returned.');
    }
    
    if (count($results) > $number_visible_search_results) {
      throw new Exception($number_visible_search_results . ' search results expected. ' . count($results) . ' search results returned.');
    }
  }

  protected function containerInRegion($selector, $region) {
    $this->spin(function($context) use ($selector, $region) {
      $region_element = $context->getRegion($region);
      $container_elements = $region_element->findAll('css', $selector);
      
      if (empty($container_elements)) {
        throw new Exception('Unable to find container element with selector: "' . $selector . '" in the "' . $region . '" region.');
      }
      
      if (sizeof($container_elements) == 1) {
        $context->return = $container_elements[0];
        return TRUE;
      }
      $context->return = $container_elements;
      return TRUE;
    });
    return $this->return;
  }
   
  /**
   * @Then /^there should be at least "([^"]*)" words in the "([^"]*)" container in the "([^"]*)" region$/
   */
  public function thereShouldBeAtLeastWordsInTheContainerInTheRegion($min_num_of_words, $selector, $region) {
  
    $container_element = $this->containerInRegion($selector, $region);
    $paragraph_text = $container_element->getText();
    
    $word_count = str_word_count($paragraph_text);
    
    if ($word_count < $min_num_of_words) {
      throw new Exception('Insuffient word count. At least ' . $min_num_of_words . ' expected. ' . $word_count . ' found. Current URL: ' . $this->getSession->getCurrentUrl());
    }
  }
  
  /**
   * @Then /^all checkboxes in container "([^"]*)" should be "([^"]*)" in the "([^"]*)" region$/
   */
  public function allCheckboxesInContainerShouldBeInTheRegion($selector, $checked_or_unchecked, $region) {
    $container_element = $this->containerInRegion($selector, $region);
    
    $checkboxes = $container_element->findAll('css', 'input[type=checkbox]');
    
    foreach ($checkboxes as $checkbox) {
      if ($checked_or_unchecked == self::CHECKED && !$checkbox->isChecked()) {
        throw new Exception('All checkboxes expected to be checked. At least one was not checked.');
      }
      
      if ($checked_or_unchecked == self::UNCHECKED && $checkbox->isChecked()) {
        throw new Exception('All checkboxes expected to be unchecked. At least one was checked.');
      }
    }
  }
  
  /**
   * @When /^I click "([^"]*)" in the "([^"]*)" region to open a print window$/
   */
  public function iClickInTheRegionToOpenAPrintWindow($link_text, $region) {
    $region_element = $this->getRegion($region);
    
    $links = $region_element->findAll('css', 'a.print');
    
    $link_found = false;
    $link_element = null;
    
    foreach($links as $link_element) {
      if (strtolower($link_element->getText()) == strtolower($link_text)) {
        $link_found = true;
        break;
      }
    }
    
    if (!$link_found) {
      throw new Exception('Unable to find link with text "' . $link_text . '" in the "' . $region . '" region.');
    }
    
    $suppress_print_dialog_function = '
      jQuery("a.print").click(
        function() {
          if (couponArray == null || (couponArray.length > 0)) {
            var cookieData = ( document.domain.indexOf("wholefoodsmarket.com") == -1 ) ? "&data="+couponArray.toString() : "";
            var features = "left=0,top=0,location=0,scrollbars=1,toolbar=0,status=0,menubar=0,resizable=0,width=750,height=550";
            window.open( "http://www2.wholefoodsmarket.com/coupons/print.php?suppressPrint=true&country=USA" + cookieData, "CouponPrint", features );
          }
          else{
            alert("Please select the coupons you would like to print, then click \'Print Selected\' again.");
          }
        }
      );
    ';
    
    $this->getSession()->executeScript($suppress_print_dialog_function);
    
    $link_element->click();
  }

  /**
   * @Then /^the page element identified by "([^"]*)" has content of minimum length "([^"]*)" in the "([^"]*)" region$/
   */
  public function thePageElementIdentifiedByHasContentOfMinimumLengthInTheRegion($selector, $minimum_length, $region) {
    $container_element = $this->containerInRegion($selector, $region);
    $text = $container_element->getText();
    $text_length = strlen($text);
    if ($text_length < $minimum_length) {
      throw new Exception('Page element with selector "' . $selector . '" was found but element content is too short. Expected minimum "' . $minimum_length . '" . characters. Found "' . $text_length . '" characters.');
    }
  }
  
  /**
   * @Then /^the page element identified by "([^"]*)" has at least "([^"]*)" words in the "([^"]*)" region$/
   */
  public function thePageElementIdentifiedByHasAtLeastWordsInTheRegion($selector, $minimum_word_count, $region) {
    $container_element = $this->containerInRegion($selector, $region);
    $text = $container_element->getText();
    $word_count = str_word_count($text);
    if ($word_count < $minimum_word_count) {
      throw new Exception('Page element with selector "' . $selector . '" was found but element content is too short. Expected minimum "' . $minimum_word_count . '" . words. Found "' . $word_count . '" words.');
    }
  }
  
  /**
   * @Then /^I click a random element identified by "([^"]*)" in the "([^"]*)" region$/
   */
  public function iClickARandomElementIdentifiedByInTheRegion($selector, $region) {
    $region_element = $this->spinGetRegion($region);
        
    $links = $region_element->findAll('css', $selector);
    
    if (empty($links)) {
      throw new Exception('Unable to find links with selector "' . $selector . '"');
    }
    
    $random_link_element = $this->getRandomLinkElement($links);
    $link_text = $random_link_element->getText();
    $matches = array();
    if (preg_match('/(\w+|\s|-|,)+/', $link_text, $matches) == 1) {
      $this->found_link_text = TRUE;
      $this->selected_text = trim($matches[0]);
    }
    
    $this->selected_url = $random_link_element->getAttribute('href');
    $random_link_element->click(); 
  }
  
  /**
   * Sometimes the links founds aren't visible. Try up to 10 random ones before failing.
   */
  private function getRandomLinkElement(&$links, $count = 0) {
    $index = array_rand($links);
    $random_link_element = $links[$index];
    if ($random_link_element->isVisible()) {
      return $random_link_element;
    }
    else if ($count < 10) { //10 is an arbitrary number - hopefully a visible link is return by that many tries
      //remove hidden links so they are selected next time around
      unset($links[$index]);
      return $this->getRandomLinkElement($links, $count++);
    } 
  }
  
  /**
   * @Then /^I should be on the selected page$/
   */
  public function iShouldBeOnTheSelectedPage() {
    $current_url = $this->getSession()->getCurrentUrl();
    
    if (strpos($current_url, $this->selected_url) === FALSE) {
      throw new Exception('Not on correct page. Expected: ' . $this->selected_url . ' but page is: ' . $current_url);
    }
  }
  
  /**
   * @Then /^I should be on the selected page with title identified by "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldBeOnTheSelectedPageWithTitleIdentifiedByInTheRegion($selector, $region) {
    $title_element = $this->containerInRegion($selector, $region);
    $title_text = trim($title_element->getText());        
    if ($this->found_link_text && stripos($title_text, $this->selected_text) === FALSE) {
      throw new Exception('Page heading incorrect. Expected: ' . $this->selected_text . ' but page heading is: ' . $title_text);  
    }
    
    $this->iShouldBeOnTheSelectedPage();
  }
  
  /**
   * @Then /^I should see the randomly added recipe identified by "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeTheRandomlyAddedRecipeIdentifiedByInTheRegion($selector, $region) {
    $region_element = $this->getRegion($region);
    
    $saved_recipes = $region_element->findAll('css', $selector);
    
    $found_saved_recipe = FALSE;
    
    foreach($saved_recipes as $save_recipe) {
      $link_text = $save_recipe->getText();
      $selected_link_text = $this->selected_text;
      if (strcasecmp($link_text, $selected_link_text) == 0) {
        $found_saved_recipe = TRUE;
        break; 
      }
    }
    
    if (!$found_saved_recipe) {
      throw new Exception('Unable to find randomly selected recipe in the recipe box with "' . $selector . '" in the "' . $region . '" region.');
    }
  }
  
  public function spin($lambda, $wait = 12) {
    $exception_message = '';
    
    for ($i = 0; $i < $wait; $i++) {
        try {
            if ($lambda($this)) {
              return TRUE;
            }
        } catch (Exception $e) {
          $exception_message = $e->getMessage() . "\n";
        }
        sleep(1);
    }

    $backtrace = debug_backtrace();
    $exception_message .= "Timeout thrown by " . $backtrace[1]['class'] . "::" . $backtrace[1]['function'] . "()\n";
    if (array_key_exists('file', $backtrace[1])) {
      $exception_message .= $backtrace[1]['file'];
    }
    if (array_key_exists('line', $backtrace[1])) {
      $exception_message .= ", line " . $backtrace[1]['line'];      
    }
    
    throw new Exception($exception_message);
  }
  
  /**
   * @Given /^I manually press "([^"]*)"$/
   */
  public function iManuallyPress($key) {
    $script = "jQuery.event.trigger({ type : 'keypress', which : '" . $key . "' });";
    $this->getSession()->evaluateScript($script);
  }
  
  /**
   * @When /^I select "([^"]*)" from "([^"]*)" in the "([^"]*)" region$/
   * 
   * This function is a wrapper for MinkContext's selectOption.
   */ 
  public function iSelectFromInTheRegion($option, $select, $region) {
    $this->spinGetRegion($region);
    $this->spin(function($context) use ($option, $select) {
      $context->selectOption($select, $option);
      return TRUE;        
    });
  }  
  
  /**
   * @When /^I additionally select "([^"]*)" from "([^"]*)" in the "([^"]*)" region$/
   */
  public function iAdditionallySelectFromInTheRegion($option, $select, $region) {
    $this->getRegion($region);
    $this->spin(function($context) use ($option, $select) {
      $context->selectOption($select, $option, TRUE);
      return TRUE;        
    });
  }

  /**
   * @Then /^I should see an "([^"]*)" element in the "([^"]*)" region$/
   */
  public function iShouldSeeAnElementInTheRegion($selector, $region) {
    $region_node = $this->getRegion($region);

    $this->spin(function($context) use ($selector, $region_node) {
      $context->assertSession()->elementExists('css', $selector, $region_node);
      return TRUE;
    });
  }
  
  /**
   * @When /^I click the "(?P<link>[^"]*)" link in the "(?P<region>[^"]*)"(?:| region)$/
   */
  public function iClickTheLinkInTheRegion($link_text, $region) {
    
    $region_node = $this->spinGetRegion($region);
    
    $this->spin(function($context) use ($link_text, $region_node) {  
      // Find the link within the region
      $link_node = $region_node->findLink($link_text);
      if (empty($link_node)) {
        throw new \Exception(sprintf('The link "%s" was not found in the region "%s" on the page %s', $link_text, $region, $context->getSession()->getCurrentUrl()));
      }
      $link_node->click();
      return TRUE;
    });
  }
  
  /**
   * @Given /^I enter "([^"]*)" into "([^"]*)" in the "([^"]*)" region$/
   */
  public function iEnterIntoInTheRegion($value, $field, $region) {

    $region_node = $this->spinGetRegion($region);
    
    $this->spin(function($context) use ($value, $field, $region_node) {
      $region_node->fillField($field, $value);
      return TRUE;
    });
  }

  /**
   * @Then /^I should see a disabled button with id "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeADisabledButtonWithIdInTheRegion($button_id_name, $region_name) {
    $region_node = $this->getRegion($region_name);
    if (empty($region_node)){
      throw new Exception("Unable to find region " . $region_name);
    }
    $button_node = $region_node->findById($button_id_name);
    if (empty($button_node)) {
      throw new Exception("Unable to find button with id " . $button_id_name);
    }

    $this->getSession()->getDriver()->evaluateScript(
        "function(){ return jQuery('#footer-store-locator-submit:'); }()"
    );
    //$button_node->click();
    //if ($button_node->click()){
      //throw new Exception("Able to click button before state and store selected.");
    //}

/*    $submit = $element->findButton('signin');    //THIS IS SAMPLE CODE TO LOOK AT.  
    if (empty($submit)) {
      throw new \Exception(sprintf("No submit button at %s", $this->getSession()->getCurrentUrl()));
    }
    // Log in.
    $submit->click();
*/
    throw new Exception("Function not finished being written yet.");
  }

  /**
   * @Then /^I should see an enabled button with id "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeAnEnabledButtonWithIdInTheRegion($button_id_name, $region_name) {
    $region_node = $this->getRegion($region_name);
    if (empty($region_node)){
      throw new Exception("Unable to find region " . $region_name);
    }
    $button_node = $region_node->findById($button_id_name);
    if (empty($button_node)) {
      throw new Exception("Unable to find button with id " . $button_id_name);
    }
    throw new Exception("Function not finished being written yet.");
  }

  /**
   * @Given /^my selected store is "([^"]*)"$/
   */
  public function mySelectedStoreIs($arg1) {
    
    if (strlen($arg1) !== 3) {
      throw new Exception('Use the store three-letter code with this function!');
    }
    $store = $this->getStoreInfo($arg1);
    
    $this->getSession()->setCookie('local_store', $store->nid);
    $this->getSession()->setCookie('local_store_id', $store->tlc);
    $this->getSession()->setCookie('local_store_name', $store->storename);
    $this->getSession()->setCookie('local_store_url', '/stores/' . $store->path); 
  }

  /**
   * Get store info from request to wholefoodsmarket.com
   * @param string $tlc
   *   Store three-letter-code.
   * @return object
   *   json-decoded store object.
   * @todo This isn't working. Inexplicably cannot access properties of $nid object in foreach() even though returned json is valid in linters.
   */
  protected function getStoreInfo($tlc)
  {
    $url = 'http://www.wholefoodsmarket.com/ajax/stores/' . $tlc;
    $response = file_get_contents($url);
    if (!$response) {
      throw new Exception('Something is wrong with the request to ' . $url);
    }
    $storeobj = json_decode($response);
    foreach ($storeobj as $nid => $obj) {
      return $obj;
    }
  }

  /**
   * @Then /^I should be on a page with "([^"]*)" in the url$/
   */
  public function iShouldBeOnAPageWithInTheUrl($arg1) {
    $url = $this->getSession()->getCurrentUrl();
    $position = strpos($url, $arg1);
    if ($position === FALSE) {
      throw new Exception($arg1 . ' not found in ' . $url);
    }
  }

  /**
   * @Given /^I should see "([^"]*)" or more "([^"]*)" elements$/
   */
  public function iShouldSeeOrMoreElements($num, $element)
  {
    $container = $this->getSession()->getPage();
    $nodes = $container->findAll('css', $element);

    if (intval($num) > count($nodes)) {
      $message = sprintf('%d elements less than %s "%s" found on the page, but should be %d.', count($nodes), $selectorType, $selector, $count);
      throw new ExpectationException($message, $this->session);
    }
  }
} // FeatureContext