<?php
use Behat\Behat\Context\BehatContext;
use Drupal\DrupalExtension\Context\DrupalSubContextInterface;

class MobileSubContext extends BehatContext implements DrupalSubContextInterface
{
  
  const RECIPE_SEARCH_TERMS = 'chicken,banana,beef,rice,yellow,bean,spinach,grilled';
  const REGION_CONTENT = 'content';
  
  private $selected_recipe_url;
  private $selected_recipe_title;
  private $selected_store;
  
  public function __construct(array $parameters = null) {
    // do subcontext initialization
  }

  public static function getAlias() {
    return 'mobile';
  }

  public function getSession() {
    return $this->getMainContext()->getSession();
  }

  public function getRegion($region) {
    return $this->getMainContext()->getRegion($region);
  }
  
  /**
   * @Given /^that I search for stores in a random state$/
   */
  public function thatISearchForStoresInARandomState() {
    $input_node = $this->getSession()->getPage()->find("css", '#edit-address');
    
    if (empty($input_node)) {
      throw new Exception('Unable to find input field: #edit-address on page ' . $this->getSession()->getCurrentUrl());
    }
    
    $state = $this->getMainContext()->getRandomState();
    $input_node->setValue($state);
    
    $submit_node = $this->getSession()->getPage()->find("css", '#edit-submit');
    
    if (empty($submit_node)) {
      throw new Exception('Unable to find submit button: #edit-submit on page ' . $this->getSession()->getCurrentUrl());
    }
    
    $submit_node->click();
  }
  
  /**
   * @Given /^that I select a random store$/
   */
  public function thatISelectARandomStore() {
    $element = $this->getSession()->getPage();
    $select_store_buttons = $element->findAll('css', '.make-this-my-store');
    if (!is_array($select_store_buttons) || count($select_store_buttons) == 0) {
      throw new Exception("Not able to find 'Make This My Store' links with class make-this-my-store");
    }
    
    $num_buttons = count($select_store_buttons);
    $random_index = rand(0, $num_buttons - 1);
    $random_store_node = $select_store_buttons[$random_index];
    
    $parent_store_container = $random_store_node->getParent()->getParent()->getParent();
    $selected_store_node = $parent_store_container->find('css', 'h2.storefront-title');
    
    if (empty($selected_store_node)) {
      throw new Exception("Unable to find store name container identified by h2.storefront-title");
    }
    
    $this->selected_store = $selected_store_node->getText();
    
    $random_store_node->click();
  }

  /**
   * @Then /^I should selected store name in the "([^"]*)" region$/
   */
  public function iShouldSelectedStoreNameInTheRegion($region) {
    $region_element = $this->getRegion($region);
    
    $store_name_node = $region_element->find('css', '#block-store-mobile-store-indicator .content');
    
    if (empty($store_name_node)) {
      throw new Exception("Unable to find selected store element with '#block-store-mobile-store-indicator .content' selector");
    }
    
    $text = strtolower($store_name_node->getText());
    $selected_store = strtolower($this->selected_store);
    
    if (strpos($text, $this->selected_store) === FALSE) {
      throw new Exception("Expected store name not found. Expecting '" . $selected_store . "' Found '" . $text . "'");
    }
  }
    
  /**
   * @Given /^I fill in "([^"]*)" with a random search term$/
   */
  public function iFillInWithARandomSearchTerm($input_selector) {
    $page = $this->getSession()->getPage();
    $input = $page->find('css', $input_selector);
    if (empty($input)) {
      throw new Exception('Unable to find input with ' . $input_selector . ' selector.');
    }
    $term = $this->getRandomRecipeSearchTerm();
    $input->setValue($term);
  }
  
  /**
   * Helper function to load a random state to fill in forms.
   */
  public function getRandomRecipeSearchTerm() {
    $terms = explode(',', self::RECIPE_SEARCH_TERMS);
    $index = array_rand($terms);
    return $terms[$index];
  }
  
  /**
   * @Then /^I should see the selected recipe heading in the "([^"]*)" region$/
   */
  public function iShouldSeeTheSelectedRecipeHeadingInTheRegion($region) {
    $region_element = $this->getRegion($region);
    $title = $region_element->find('css', '.panel-panel .page-title');
    
    if (empty($title)) {
      throw new Exception('Unable to find recipe title');
    }
    
    $title = trim($title->getText());
    
    if (strtolower($title) != strtolower($this->selected_recipe_title)) {
      throw new Exception('Page title found but not correct. Expected: "' . $this->selected_recipe_title . '" but title is: ' . $title);
    }
  }
  
  /**
   * @Then /^the Share This block is visible and complete in the "([^"]*)" region$/
   */
  public function theShareThisBlockIsVisibleAndCompleteInTheRegion($region) {
    $region_element = $this->getRegion($region);
    $share_this_element = $region_element->find('css', '.sharethis-wrapper');
    
    if (empty($share_this_element)) {
      throw new Exception('Unable to locate an element with class .sharethis-wrapper in the ' . $region . ' region');
    }
    
    $facebook_count_element = $share_this_element->find('css', '.st_facebook_vcount');
    
    if (empty($facebook_count_element)) {
      throw new Exception('Unable to find the Facebook share count element');
    }
    
    $twitter_count_element = $share_this_element->find('css', '.st_twitter_vcount');
    
    if (empty($twitter_count_element)) {
      throw new Exception('Unable to find the Twitter share count element');
    }

    $pintrest_count_element = $share_this_element->find('css', '.st_pinterest_vcount');
    
    if (empty($pintrest_count_element)) {
      throw new Exception('Unable to find the Pintrest share count element');
    }
  }
  
  /**
   * @Given /^I view a random blog post in the "([^"]*)" region$/
   */
  public function iViewARandomBlogPostInTheRegion($region) {
    $region_element = $this->getRegion($region);
    
    $post_links = $region_element->findAll('css', '.views-row .post h3 a');
    
    if (empty($post_links)) {
      throw new Exception('Unable to retrieve blog post links in the "' . $region . '" region.');
    }

    $index = array_rand($post_links);
    $random_post = $post_links[$index];
    $random_post->click();
  }

  /**
   * @Given /^That my mobile recipe box is empty$/
   */
  public function thatMyMobileRecipeBoxIsEmpty() {
    //explicitly adding domain to the visit call because this 
    //test was going to some external domain and I couldn't figure out why
    $url = parse_url($this->getSession()->getCurrentUrl());
    //if already on myrecipebox - don't redirect there again
    if (strpos($url['path'], 'myrecipebox') === FALSE) {
      $url = $url['scheme'] . '://' . $url['host'] . '/myrecipebox';
      $this->getSession()->visit($url);
    }
    $region_element = $this->getRegion(self::REGION_CONTENT);
    
    $remove_recipe_links = $region_element->findAll('css', 'a.remove-recipe');
    
    if (is_array($remove_recipe_links) && count($remove_recipe_links) > 0) {
      
      $this->iToggleOpenAllPanelsInTheRegion(self::REGION_CONTENT);
      
      //recursive call until all stored recipes are removed from the recipe box
      foreach($remove_recipe_links as $remove_recipe_link) {
        $remove_recipe_link->click();
        $this->getSession()->getDriver()->getWebDriverSession()->accept_alert();
        $this->thatMyMobileRecipeBoxIsEmpty();
      }
    }
  }

  /**
   * @When /^I toggle open all panels in the "([^"]*)" region$/
   */
  public function iToggleOpenAllPanelsInTheRegion($region) {
    $region_element = $this->getRegion($region);
    
    //expand all toggle sections of the page so recipe links are visible
    $toggle_links = $region_element->findAll('css', 'a.toggle');
    
    foreach ($toggle_links as $toggle_link) {     
      $link_text = $toggle_link->getText();
      if ($link_text == '+') {
        $toggle_link->click();
      }
    }
  }
}
