Feature: Blog landing page   
  In order to assure basic functionality of the blog
  As an unauthenticated user 
  I need to assure all page blocks are displayed correctly 
  
Background:
  Given I am on "blog/whole-story"
  
@blog @nojs @quick
Scenario: Check basic blocks of the page are loading 
  Then I should see an ".blog-maintitle" element
  And I should see the link "Whole Story" in the "contentTop" region 
  And I should see "The Official Whole Foods Market® Blog" in the ".blog-maindesc" element
  And I should see at least "4" ".view-id-blogs .views-row"
  And I should see an ".pager" element 
  And I should see the link "next »" with href "/blog/whole-story?page=1" in the "content" region
  And I should see an "#sidebar-second" element
  And I should see the heading "Recent Posts"
  And I should see the heading "Blog Roll"
#  And I should see the heading "Blog Archives"
  And I should see the heading "Posts by Category"
  And I should see the heading "Search The Blog"
  And I should see the heading "Links"
  And I should see the heading "About the Blog"
  And I should see at least "10" ".blogpost-title"
  And I should see at least "10" ".blog-roll-link"
  And I should see at least "30" ".view-display-id-blog_category .field-content a"
#  And I should see at least "12" ".view-id-blogs_archive .views-summary li a"
#  Main blog page no longer features an expandable Blog Archives option...
#  And each article should have a title, author, comment link and read more link
  
@blog @javascript
Scenario Outline: Check that side bar tabs are expanding and collapsing
  When I click on the element with css selector "<heading>"
  Then "<content>" should be visible
  When I click on the element with css selector "<heading>"
  Then "<content>" should not be visible
  
  Examples:
    | heading | content |
    | #block-views-blogs-blog-roll h2 | #block-views-blogs-blog-roll .content |
    
@blog @javascript @search
Scenario: Blog Search with query
  Given for "edit-search-box" I enter "garden"
  And I press "Search" in the "sidebar" region
  Then I should be on "/blog/search/garden"
  And I should see many search results
  
@blog @javascript @search
Scenario: Blog Search with query and no results
  Given for "edit-search-box" I enter "foobar"
  And I press "Search" in the "sidebar" region
  Then I should be on "/blog/search/foobar"
  And I should see the heading "Sorry, no results were found."

@blog @nojs @search
Scenario: Blog pagination - checks three pages deep
  #When I click on the element with css selector ".pager-next.last a"
  #Then the url and querystring should match "/blog/whole-story?page=1"
  #And each article should have a title, author, comment link and read more link
  #When I click on the element with css selector ".pager-next.last a"
  #Then the url and querystring should match "/blog/whole-story?page=2"
  #And each article should have a title, author, comment link and read more link
  #When I click on the element with css selector ".pager-next.last a"
  #Then the url and querystring should match "/blog/whole-story?page=3"
  #And each article should have a title, author, comment link and read more link
  
@blog @javascript
Scenario: Search for article and follow link to page 
  Given for "edit-search-box" I enter "Make it Natural: Italian-Style Spaghetti Squash with Tempeh"
  And I press "Search" in the "sidebar" region  
  Then I should be on "/blog/search/Make%20it%20Natural%3A%20Italian-Style%20Spaghetti%20Squash%20with%20Tempeh?f%5B0%5D=field_blog%3A6860"
  And I follow "Make it Natural: Italian-Style Spaghetti Squash with Tempeh"
  Then I should be on "/blog/whole-story/make-it-natural-italian-style-spaghetti-squash-tempeh"