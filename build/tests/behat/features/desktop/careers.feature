Feature: Careers
To verify the Careers page is working
As an unauthenticated user
I need to see the main content on the page
I need to see the side content on the page

Background:
Given I am on "/careers"

@careers @unauth @nojs
Scenario: Careers page loads correctly
  Then I should see "Careers" in the "breadcrumb" region
  Then I should see the heading "Careers" in the "content" region
  Then I should see the text "Learn More About Jobs at Whole Foods Market" in the "sidebar" region
  Then I should see the link "Careers" in the "sidebar" region
  Then I should see the link "Find and Apply for Jobs" in the "sidebar" region
  Then I should see the link "Our Values and Mission" in the "sidebar" region
  Then I should see the link "Why We’re A Great Place to Work" in the "sidebar" region
  Then I should see the link "Tasty Jobs at Whole Foods Market" in the "sidebar" region