Feature: Customer Service
  To verify the customer service page is working
  As an unauthenticated user
  I need to be able to choose a state and store
  Then select a topic and submit the contact form

Background:
  Given I am on "/customer-service"
  
@customer-service @unauth @nojs @quick
Scenario: Page Content is Good
  Then I should see "Customer Service" in the "breadcrumb" region
  Then I should see the "Customer Service" heading in the "content" region
  Then I should see the link "discussion forums" with href "/forums" in the "content" region
  Then I should see the link "Community Involvement & Donations" with href "/mission-values/caring-communities/community-giving" in the "content" region
  Then I should see the link "Investor Relations FAQs" with href "company-info/investor-relations/investor-faq" in the "content" region
  Then I should see the link "Information for Potential Vendors" with href "/company-info/information-potential-vendors" in the "content" region

@customer-service @unauth @javascript
Scenario: State Select Loads Correct Stores
  Given I select "Store Experience" from "edit-submitted-choose-topic" in the "content" region
  When I select "Colorado" from "phone_number_state_list" in the "content" region
  When I additionally select "Roaring fork - 340 Reed Street" from "phone_number_store_list"
  When I select "California" from "phone_number_state_list"
  When I additionally select "Berkeley - 3000 Telegraph Ave" from "phone_number_store_list"
  When I additionally select "Beverly hills - 239 North Crescent Dr" from "phone_number_store_list"
  When I additionally select "Laguna niguel - 23932 Aliso Creek Road" from "phone_number_store_list"
  When I select "Louisiana" from "phone_number_state_list"
  When I additionally select "Baton rouge - 7529 Corporate Blvd" from "phone_number_store_list"
  When I select "Massachusetts" from "phone_number_state_list"
  When I additionally select "River street - 340 River St" from "phone_number_store_list"
  When I select "Nevada" from "phone_number_state_list"
  When I additionally select "Las vegas blvd - 6689 Las Vegas Blvd." from "phone_number_store_list"
  When I additionally select "Reno - 6139 S. Virginia Street" from "phone_number_store_list"
  When I select "North Carolina" from "phone_number_state_list"
  When I additionally select "Charlotte - 6610 Fairview Rd." from "phone_number_store_list"
  When I select "Tennessee" from "phone_number_state_list"
  When I additionally select "Green hills - 4021 Hillsboro Pike" from "phone_number_store_list"
  When I additionally select "Poplar avenue - 5014 Poplar Avenue" from "phone_number_store_list"
  When I select "Virginia" from "phone_number_state_list"
  When I additionally select "Charlottesville - 1797 Hydraulic Road" from "phone_number_store_list"
  When I select "Washington" from "phone_number_state_list"
  When I additionally select "Roosevelt square - 1026 NE 64th Street" from "phone_number_store_list"

@customer-service @unauth @javascript
Scenario: Contact Us Via Email Loads Correct Form Fields
  Given I select "Store Experience" from "edit-submitted-choose-topic" in the "content" region
  When I select "Colorado" from "phone_number_state_list" in the "content" region
  When I additionally select "Roaring fork - 340 Reed Street" from "phone_number_store_list"
  When I additionally select "Store Experience" from "edit-submitted-choose-topic"
  When I fill in "edit-submitted-name" with "Test Name"
  When I fill in "edit-submitted-email" with "test@test.com"
  When I fill in "edit-submitted-phone" with "123-123-1234"
  When I fill in "edit-submitted-body" with "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  When I additionally select "Sponsorship/Donation" from "edit-submitted-choose-topic"
  When I fill in "edit-submitted-name" with "Test Name"
  When I fill in "edit-submitted-email" with "test@test.com"
  When I fill in "edit-submitted-phone" with "123-123-1234"
  When I fill in "edit-submitted-body" with "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  When I additionally select "Employment (Jobs)" from "edit-submitted-choose-topic"
  When I fill in "edit-submitted-name" with "Test Name"
  When I fill in "edit-submitted-email" with "test@test.com"
  When I fill in "edit-submitted-phone" with "123-123-1234"
  When I fill in "edit-submitted-body" with "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  When I additionally select "365 Everyday Value/365 Organics" from "edit-submitted-choose-topic"
  When I fill in "edit-submitted-name" with "Test Name"
  When I fill in "edit-submitted-email" with "test@test.com"
  When I fill in "edit-submitted-phone" with "123-123-1234"
  When I fill in "edit-submitted-body" with "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  When I fill in "edit-submitted-product-upc" with "123456"
  When I additionally select "Quality Standards" from "edit-submitted-choose-topic"
  When I fill in "edit-submitted-name" with "Test Name"
  When I fill in "edit-submitted-email" with "test@test.com"
  When I fill in "edit-submitted-phone" with "123-123-1234"
  When I fill in "edit-submitted-body" with "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  When I additionally select "Product Request" from "edit-submitted-choose-topic"
  When I fill in "edit-submitted-name" with "Test Name"
  When I fill in "edit-submitted-email" with "test@test.com"
  When I fill in "edit-submitted-phone" with "123-123-1234"
  When I fill in "edit-submitted-body" with "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  When I additionally select "Company Policy" from "edit-submitted-choose-topic"
  When I fill in "edit-submitted-name" with "Test Name"
  When I fill in "edit-submitted-email" with "test@test.com"
  When I fill in "edit-submitted-phone" with "123-123-1234"
  When I fill in "edit-submitted-body" with "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  When I additionally select "Green Mission" from "edit-submitted-choose-topic"
  When I fill in "edit-submitted-name" with "Test Name"
  When I fill in "edit-submitted-email" with "test@test.com"
  When I fill in "edit-submitted-phone" with "123-123-1234"
  When I fill in "edit-submitted-body" with "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  When I additionally select "Store Location Request" from "edit-submitted-choose-topic"
  When I fill in "edit-submitted-name" with "Test Name"
  When I fill in "edit-submitted-email" with "test@test.com"
  When I fill in "edit-submitted-phone" with "123-123-1234"
  When I fill in "edit-submitted-body" with "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  When I fill in "edit-submitted-store-zip-code" with "99762"
  When I additionally select "Website Issues" from "edit-submitted-choose-topic"
  When I fill in "edit-submitted-name" with "Test Name"
  When I fill in "edit-submitted-email" with "test@test.com"
  When I fill in "edit-submitted-phone" with "123-123-1234"
  When I fill in "edit-submitted-body" with "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  When I additionally select "Potential Vendor" from "edit-submitted-choose-topic"
  When I fill in "edit-submitted-name" with "Test Name"
  When I fill in "edit-submitted-email" with "test@test.com"
  When I fill in "edit-submitted-phone" with "123-123-1234"
  When I fill in "edit-submitted-body" with "Lorem ipsum dolor sit amet, consectetur adipiscing elit."    