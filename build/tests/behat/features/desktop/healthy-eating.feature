Feature: Healthy-Eating
To verify healthy-eating and all subsequent pages load and link correctly
As an unauthenticated user
I need to see main content on the page
I need to see the three pods on the page
I need to see the Getting Started pod
I need to see the What to Eat pod
I need to see the Healthy Cooking pod

Background:
Given I am on "/healthy-eating"

@healthy-eating @unauth @nojs
Scenario: Healthy Eating page loads correctly
  Then I should see the text "HEALTHY EATING" in the "marquee" region
  Then I should see an image in container "div.gettingstarted"
  Then I should see the link "Getting Started" in the "div.gettingstarted" container with href "/getting-started"
  Then I should see an image in container "div.whattoeat"
  Then I should see the link "What to Eat" in the "div.whattoeat" container with href "/what-to-eat"
  Then I should see an image in container "div.healthycooking"
  Then I should see the link "Healthy Cooking" in the "div.healthycooking" container with href "/healthy-cooking"

@healthy-eating @unauth @nojs
Scenario: Getting Started page loads correctly
  Given I am on "/healthy-eating/getting-started"
  #contentTop
  Then I should see an image in the ".views-field-field-page-intro-image .field-content .file-image .content span" container
  Then I should see the text "Getting Started" in the "contentTop" region
  #bottomcontent
  Then I should see "4" "div.list-row" elements in the "#block-views-generic-page-items-list-copy" container
  Then I should see an image in each "div.list-row" element in the "#block-views-generic-page-items-list-copy" container
  Then I should see the text "Special Diets" in count "1" of "div.list-row" element in the "#block-views-generic-page-items-list-copy" container
  Then I should see the text "Simple Changes for Lifelong Health" in count "2" of "div.list-row" element in the "#block-views-generic-page-items-list-copy" container
  Then I should see the text "Four Ways to a Healthier You" in count "3" of "div.list-row" element in the "#block-views-generic-page-items-list-copy" container
  Then I should see the text "Resources" in count "4" of "div.list-row" element in the "#block-views-generic-page-items-list-copy" container
  #sidebar
  Then I should see the text "Healthy Eating" in the "sidebar" region
  Then I should see the link "Getting Started" in the "sidebar" region
  Then I should see the link "Four Pillars of Healthy Eating" in the "div.he-menu-item" container
  Then I should see the link "Special Diets" in the "div.he-menu-item" container
  Then I should see the link "Simple Changes for Lifelong Health" in the "div.he-menu-item" container
  Then I should see the link "Resources" in the "div.he-menu-item" container
  Then I should see the link "What to Eat" in the "sidebar" region
  Then I should see the link "Healthy Recipes Videos" in the "div.what-to-eat-sn" container
  Then I should see the link "Meal Plans" in the "div.what-to-eat-sn" container
  Then I should see the link "Healthy Eating for the Whole Family" in the "div.what-to-eat-sn" container
  Then I should see the link "Engine 2 Challenge" in the "div.what-to-eat-sn" container
  Then I should see the link "Healthy Cooking" in the "sidebar" region
  Then I should see the link "Healthy Cooking Videos" in the "div.healthy-cooking-sn" container
  Then I should see the link "Healthy Pantry Makeover" in the "div.healthy-cooking-sn" container
  Then I should see the link "Cooking with Whole Grains" in the "div.healthy-cooking-sn" container
  Then I should see the link "Add Flavor Naturally" in the "div.healthy-cooking-sn" container
  Then I should see the link "ANDI Guide" in the "div.healthy-cooking-sn" container
  Then I should see the text "Health Starts Here" in the "sidebar" region
  Then I should see the text "Healthy Eating Handbook" in the "sidebar" region
  Then I should see the text "Healthy Tips Delivered" in the "sidebar" region
  #breadcrumb
  #Then I should see the link "Healthy Eating" in the "breadcrumb" region
  #Then I should see "Getting Started" in the "breadcrumb" region 
  #Production error -- this does not currently exist on the page -- Healthy Eating is plain text

@healthy-eating @unauth @nojs
Scenario: What To Eat page loads correcty
  Given I am on "/healthy-eating/what-to-eat"
  #breadcrumb
  Then I should see the link "Healthy Eating" in the "breadcrumb" region
  Then I should see the text "What to Eat" in the "breadcrumb" region
  #contentTop
  Then I should see an image in the ".views-field-field-page-intro-image .field-content .file-image .content span" container
  Then I should see the text "What to Eat" in the "contentTop" region
  #contentBottom
  Then I should see "4" "div.list-row" elements in the "#block-views-generic-page-items-list-copy" container
  Then I should see an image in each "div.list-row" element in the "#block-views-generic-page-items-list-copy" container
  Then I should see the text "Healthy Recipe Videos" in count "1" of "div.list-row" element in the "#block-views-generic-page-items-list-copy" container
  Then I should see the text "Meal Plans & Shopping Lists" in count "2" of "div.list-row" element in the "#block-views-generic-page-items-list-copy" container
  Then I should see the text "Parents & Kids" in count "3" of "div.list-row" element in the "#block-views-generic-page-items-list-copy" container
  Then I should see the text "Engine 2 Diet" in count "4" of "div.list-row" element in the "#block-views-generic-page-items-list-copy" container
  #sidebar
  Then I should see the text "Healthy Eating" in the "sidebar" region
  Then I should see the link "Getting Started" in the "sidebar" region
  Then I should see the link "Four Pillars of Healthy Eating" in the "div.healthy-eating-sn" container
  Then I should see the link "Special Diets" in the "div.healthy-eating-sn" container
  Then I should see the link "Simple Changes for Lifelong Health" in the "div.healthy-eating-sn" container
  Then I should see the link "Resources" in the "div.healthy-eating-sn" container
  Then I should see the link "What to Eat" in the "sidebar" region
  Then I should see the link "Healthy Recipes Videos" in the "div.what-to-eat-sn" container
  Then I should see the link "Meal Plans" in the "div.what-to-eat-sn" container
  Then I should see the link "Healthy Eating for the Whole Family" in the "div.what-to-eat-sn" container
  Then I should see the link "Engine 2 Challenge" in the "div.what-to-eat-sn" container
  Then I should see the link "Healthy Cooking" in the "sidebar" region
  Then I should see the link "Healthy Cooking Videos" in the "div.healthy-cooking-sn" container
  Then I should see the link "Healthy Pantry Makeover" in the "div.healthy-cooking-sn" container
  Then I should see the link "Cooking with Whole Grains" in the "div.healthy-cooking-sn" container
  Then I should see the link "Add Flavor Naturally" in the "div.healthy-cooking-sn" container
  Then I should see the link "ANDI Guide" in the "div.healthy-cooking-sn" container
  Then I should see the text "Health Starts Here" in the "sidebar" region
  Then I should see the text "Healthy Eating Handbook" in the "sidebar" region
  Then I should see the text "Healthy Tips Delivered" in the "sidebar" region

@healthy-eating @unauth @nojs
Scenario: Healthy Cooking page loads correctly
  Given I am on "/healthy-eating/healthy-cooking"
  #And I wait for "2000" milliseconds
  #breadcrumb
  Then I should see the link "Healthy Eating" in the "breadcrumb" region
  Then I should see the text "Healthy Cooking" in the "breadcrumb" region
  #contentTop
  Then I should see an image in the ".views-field-field-page-intro-image .field-content .file-image .content span" container
  Then I should see the text "Healthy Cooking" in the "contentTop" region
  #contentBottom
  Then I should see "5" "div.list-row" elements in the "#block-views-generic-page-items-list-copy" container
  Then I should see an image in each "div.list-row" element in the "#block-views-generic-page-items-list-copy" container
  Then I should see the text "Healthy Cooking Videos" in count "1" of "div.list-row" element in the "#block-views-generic-page-items-list-copy" container
  Then I should see the text "Healthy Pantry Makeover" in count "2" of "div.list-row" element in the "#block-views-generic-page-items-list-copy" container
  Then I should see the text "Cooking with Whole Grains" in count "3" of "div.list-row" element in the "#block-views-generic-page-items-list-copy" container
  Then I should see the text "Add Flavor Naturally" in count "4" of "div.list-row" element in the "#block-views-generic-page-items-list-copy" container
  Then I should see the text "ANDI Guide" in count "5" of "div.list-row" element in the "#block-views-generic-page-items-list-copy" container
  #sidebar
  Then I should see the text "Healthy Eating" in the "sidebar" region
  Then I should see the link "Getting Started" in the "sidebar" region
  Then I should see the link "Four Pillars of Healthy Eating" in the "div.healthy-eating-sn" container
  Then I should see the link "Special Diets" in the "div.healthy-eating-sn" container
  Then I should see the link "Simple Changes for Lifelong Health" in the "div.healthy-eating-sn" container
  Then I should see the link "Resources" in the "div.healthy-eating-sn" container
  Then I should see the link "What to Eat" in the "sidebar" region
  Then I should see the link "Healthy Recipes Videos" in the "div.what-to-eat-sn" container
  Then I should see the link "Meal Plans" in the "div.what-to-eat-sn" container
  Then I should see the link "Healthy Eating for the Whole Family" in the "div.what-to-eat-sn" container
  Then I should see the link "Engine 2 Challenge" in the "div.what-to-eat-sn" container
  Then I should see the link "Healthy Cooking" in the "sidebar" region
  Then I should see the link "Healthy Cooking Videos" in the "div.healthy-cooking-sn" container
  Then I should see the link "Healthy Pantry Makeover" in the "div.healthy-cooking-sn" container
  Then I should see the link "Cooking with Whole Grains" in the "div.healthy-cooking-sn" container
  Then I should see the link "Add Flavor Naturally" in the "div.healthy-cooking-sn" container
  Then I should see the link "ANDI Guide" in the "div.healthy-cooking-sn" container
  Then I should see the text "Health Starts Here" in the "sidebar" region
  Then I should see the text "Healthy Eating Handbook" in the "sidebar" region
  Then I should see the text "Healthy Tips Delivered" in the "sidebar" region
