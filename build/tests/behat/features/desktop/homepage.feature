Feature: HOMEPAGE
To verify the homepage loads correctly
As an unauthenticated user
OR as a user with a store selected
I need to see the "Store Pods" iff no store selected
I need to see the "Featured Pods" load correctly
I need to see the "Promo Area" loads correctly
I need to see the "Alternative Pods" load correctly
I need to see the "Social Media Pods" load correctly

Background:
Given I am on homepage

@homepage @unauth @javascript @disabled
Scenario: Homepage Marquee loads correctly
  Given That the Window is Full Screen
  Given I am on "/healthy-eating"
#  Then I should see the Global Marquees

@homepage @unauth @javascript @disabled
Scenario: No-Store-Selected pods load correctly
  #First Suggested Store Pod:
  Then I should see an image in the "div.col-width-one .views-col .torn-pod-content .view-display-id-sales .views-field-field-pod-2-image .field-content" container in the "content" region
  Then I should see an ".view-display-id-front_page .view-content .views-row-first h2.storefront-title" element in the "content" region
  Then I should see a submit button in the ".view-display-id-front_page .view-content .views-row-first" container with value "Make This My Store" in the "content" region  

  #Second Suggested Store Pod:
  Then I should see an image in the ".view-display-id-front_page .view-content .views-row-last .storefront-img" container in the "content" region
  Then I should see an ".view-display-id-front_page .view-content .views-row-last h2.storefront-title" element in the "content" region
  Then I should see a submit button in the ".view-display-id-front_page .view-content .views-row-last" container with value "Make This My Store" in the "content" region

@homepage @unauth @nojs @disabled
Scenario: Featured Items pods load correctly
  #Featured Sales Block
  Then I should see the text "Featured Sale" in the "#block-views-promo-blocks-homepage" container in the "content" region
  Then I should see an image in the "div.col-width-one .views-col .torn-pod-content .view-display-id-sales .views-field-field-pod-2-image .field-content" container in the "content" region
  Then I should see the "The Whole Deal®" link with href "/about-our-products/whole-deal" in the "div.col-width-one .pod_sales" container in the "content" region
  #Featured Event Block
  Then I should see the text "Featured Event" in the "div.col-width-one .views-col .torn-pod-content .view-display-id-events" container in the "content" region  
  Then I should see an image in the "div.col-width-one .views-col .torn-pod-content .view-display-id-events .views-field-field-pod-1-image" container in the "content" region
  Then I should see the "Events" link with href "/events" in the "div.col-width-one .views-col .torn-pod-content .view-display-id-events" container in the "content" region
  #Featured Recipe Block
  Then I should see the text "Featured Recipe" in the ".pod_recipes .torn-pod-content .views-field .contextual-links-region .view-global-frontpage-promo-views .view-header h2" container in the "content" region
  Then I should see an image in the "div.col-width-one .views-col .torn-pod-content .view-display-id-product .views-field-field-pod-3-image" container in the "content" region
  Then I should see a link in the "div.col-width-one .views-col .torn-pod-content .view-display-id-product" container in the "content" region

@homepage @unauth @nojs @disabled
Scenario: Promo Content Area loads correctly
  Then I should see the promo content in "#free-form-container" in "#block-views-promo-blocks-homepage"

@homepage @unauth @nojs @disabled
Scenario: Alternative pods load correctly
  #Gift Cards Pod
  Then I should see the text "Gift Cards" in the ".views-row .pod_gift_cards .torn-pod-content .views-field p" container in the "content" region
  Then I should see an image in the "div.col-width-one .pod_gift_cards .views-field-field-promo-media" container in the "content" region
  Then I should see the "BUY GIFT CARDS" link with href "/buy-gift-cards" in the "div.col-width-one .pod_gift_cards" container in the "content" region

  #Catering Pod
  Then I should see the text "CATERING" in the ".pod_order_online .torn-pod-content .views-field p" container in the "content" region
  Then I should see an image in the "div.col-width-one .pod_order_online .views-field-field-promo-media" container in the "content" region
  Then I should see the "ORDER NOW" link with href "/online-ordering" in the "div.col-width-one .pod_order_online" container in the "content" region

  #Recipes App Pod
  Then I should see the text "RECIPES APP" in the ".pod_recipes_app .torn-pod-content .views-field p" container in the "content" region
  Then I should see an image in the "div.col-width-one .pod_recipes_app .views-field-field-promo-media" container in the "content" region
  Then I should see the "Get more info" link with href "/apps" in the "div.col-width-one .pod_recipes_app" container in the "content" region

@homepage @unauth @nojs @disabled
Scenario: Social Media pods load correctly
#Blog Pod
  Then I should see the text "Blogs" in the ".view-display-id-blog .view-header h2" container in the "content" region
  Then the "div.col-width-one .pod_blogs .view-content" element should contain at least "3" "div.views-row" elements

  #Twitter Pod
  Then I should see the text "Tweets" in the ".col-width-one .pod_twitter_feed .views-field h2" container in the "content" region
  Then the "div.col-width-one .pod_twitter_feed" element should contain at least "2" "div.tweet-content" elements
  Then the "div.col-width-one .pod_twitter_feed" element should contain at least "2" "div.tweet-actions" elements
  Then I should see the "Follow WholeFoods on Twitter" link with href "https://twitter.com/intent/user?screen_name=WholeFoods" in the "div.col-width-one .pod_twitter_feed" container in "#block-views-promo-blocks-homepage"

  #Facebook Pod
  Then I should see the text "Facebook" in the ".col-width-one .pod_facebook_feed .views-field h2" container in the "content" region
  Then the "div.col-width-one .pod_facebook_feed" element should contain at least "2" "div.views-field" elements
  Then I should see the "View Whole Foods Market on Facebook" link with href "https://facebook.com/profile.php?id=24922591487" in the "div.col-width-one .pod_facebook_feed" container in the "content" region