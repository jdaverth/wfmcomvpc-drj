Feature: Store Locator Page 
  In order to assure basic functionality of the locator page
  As an unauthenticated user 
  I need to be able to find stores based on postal code, or city and state/province
  And select a store as "my store"
  
Background:
  Given I am on "/stores/list"   

@locator @nojs @quick
Scenario: Check that the basic sections of the page are there 
  Then I should see the heading "Closest store"
  And I should see an "#mapCanvas" element
  And I should see the link "Load more stores"

@locator @nojs @quick
Scenario: List all stores in Canada 
  When I click "Canada Stores"
  Then I should be on "/stores/list/canada"
  And I should see an "#block-views-store-locations-by-state-canada .view-content" element
  And I should see at least "6" "#block-views-store-locations-by-state-canada .views-row"

@locator @nojs @quick
Scenario: List all stores in UK
  When I click "UK Stores"
  Then I should be on "/stores/list/uk"
  And I should see an "#block-views-store-locations-by-state-uk .view-content" element
  And I should see at least "5" "#block-views-store-locations-by-state-uk .views-row"

@locator @nojs @quick
Scenario: List all stores in development
  When I click "Stores In Development"
  Then I should be on "/stores/list/development"
  And I should see an ".view-id-store_locations_by_state .view-content" element
  And I should see at least "5" ".view-id-store_locations_by_state .views-row"
  And I should see the text "Coming Soon"
  And I should not see the text "Make This My Store"

@locator @javascript @quick
Scenario: List stores by state
  When I click "US Stores By State"
  Then I should be on "/stores/list/state"
  And I should see an "#block-views-store-locations-by-state-state .view-content" element
  And there should be at least "6" ".views-row" elements with display "block" in the "content" region
  And there should be at least "10" ".views-row" elements with display "none" in the "content" region
  When I click ".show_more_stores" via Javascript
  # this pager is always there - just hidden
  And I should see an "#block-views-store-locations-by-state-state .pager" element
  And I should see at least "10" ".views-field-field-storefront-image"
  #after they click the show more stores, all the view-row that were display:none will change to display:block
  And there should be at least "20" ".views-row" elements with display "block" in the "content" region
