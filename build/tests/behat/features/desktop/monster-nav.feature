Feature: Monster Navigation
  To ensure the monster navigation loads in entirety
  As an unauthenticated user
  I need to verify all menu elements load correctly

Background:
  Given I am on the homepage

##### - Healthy Eating Start - #####

@monster-nav @healthy-eating @nojs @unauth @quick
Scenario Outline: Healthy Eating - Getting Started
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Four Pillars of Healthy Eating | /healthy-eating/four-pillars-healthy-eating | Getting Started | /healthy-eating/getting-started | monsterNavHealthyEating |
    | Health Starts Here | /healthy-eating/health-starts-here | Getting Started | /healthy-eating/getting-started | monsterNavHealthyEating |
    | Special Diets | /healthy-eating/special-diets | Getting Started | /healthy-eating/getting-started | monsterNavHealthyEating |
    | Simple Changes for Lifelong Health | /healthy-eating/simple-changes-lifelong-health | Getting Started | /healthy-eating/getting-started | monsterNavHealthyEating |
    | Resources | /healthy-eating/resources | Getting Started | /healthy-eating/getting-started | monsterNavHealthyEating |

@monster-nav @healthy-eating @nojs @unauth
Scenario Outline: Healthy Eating - What to Eat
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Healthy Recipes | /healthy-eating/healthy-recipes | What to Eat | /healthy-eating/what-to-eat | monsterNavHealthyEating |
    | Weekly Meal Plans | /healthy-eating/meal-plans-shopping-lists | What to Eat | /healthy-eating/what-to-eat | monsterNavHealthyEating |
    | Parents & Kids | /healthy-eating/healthy-eating-for-whole-family | What to Eat | /healthy-eating/what-to-eat | monsterNavHealthyEating |
    | The Engine 2 Diet | /healthy-eating/engine-2 | What to Eat | /healthy-eating/what-to-eat | monsterNavHealthyEating |

@monster-nav @healthy-eating @nojs @unauth
Scenario Outline: Healthy Eating - Healthy Cooking
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Healthy Pantry Makeover | /healthy-eating/healthy-pantry-makeover | Healthy Cooking | /healthy-eating/healthy-cooking | monsterNavHealthyEating |
    | Healthy Cooking Videos | /healthy-eating/healthy-cooking-videos | Healthy Cooking | /healthy-eating/healthy-cooking | monsterNavHealthyEating |
    | Cooking with Whole Grains | /healthy-eating/cooking-whole-grains | Healthy Cooking | /healthy-eating/healthy-cooking | monsterNavHealthyEating |
    | Add Flavor, Naturally | /healthy-eating/add-flavor-naturally | Healthy Cooking | /healthy-eating/healthy-cooking | monsterNavHealthyEating |
    | ANDI Guide | /healthy-eating/andi-guide | Healthy Cooking | /healthy-eating/healthy-cooking | monsterNavHealthyEating |

##### - Healthy Eating End - #####

##### - About Our Products Start - #####

@monster-nav @about-our-products @nojs @unauth
Scenario Outline: About Our Products - Our Quality Standards
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Organic Food | /about-our-products/organic-food | Our Quality Standards | /about-our-products/quality-standards | monsterNavAboutOurProducts |
    | Food Ingredient Quality Standards | /about-our-products/quality-standards/food-ingredient | Our Quality Standards | /about-our-products/quality-standards | monsterNavAboutOurProducts |
    | Meat Animal Welfare Standards | /mission-values/animal-welfare/animal-welfare-basics | Our Quality Standards | /about-our-products/quality-standards | monsterNavAboutOurProducts |
    | Premium Body Care Standards | /premium-body-care-standards | Our Quality Standards | /about-our-products/quality-standards | monsterNavAboutOurProducts |
    | Responsibly Grown | /responsibly-grown | Our Quality Standards | /about-our-products/quality-standards | monsterNavAboutOurProducts |
    | More | /about-our-products/quality-standards | Our Quality Standards | /about-our-products/quality-standards | monsterNavAboutOurProducts |

@monster-nav @about-our-products @nojs @unauth @quick
Scenario Outline: About Our Products - Product Ingredient FAQs
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:

    | link_text | link_href | heading_text | heading_href | menu_region |
    | Product FAQs: GMOs | /about-our-products/product-faq/gmos | Product Ingredient FAQs | /about-our-products/product-faq | monsterNavAboutOurProducts |
    | Food Allergies | /about-our-products/product-faq/food-allergies | Product Ingredient FAQs | /about-our-products/product-faq | monsterNavAboutOurProducts |
    | Ingredients | /about-our-products/product-faq/ingredients | Product Ingredient FAQs | /about-our-products/product-faq | monsterNavAboutOurProducts |
    | Milk and Eggs | /about-our-products/product-faq/milk-and-eggs | Product Ingredient FAQs | /about-our-products/product-faq | monsterNavAboutOurProducts |
    | Olive Oil | /about-our-products/product-faq/olive-oil | Product Ingredient FAQs | /about-our-products/product-faq | monsterNavAboutOurProducts |
    | More | /about-our-products/product-faq | Product Ingredient FAQs | /about-our-products/product-faq | monsterNavAboutOurProducts |

@monster-nav @about-our-products @nojs @unauth
Scenario Outline: About Our Products - Food Safety
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Bisphenol-A | /about-our-products/food-safety/bisphenol | Food Safety | /about-our-products/food-safety | monsterNavAboutOurProducts |
    | Cold Storage Chart | /about-our-products/food-safety/cold-storage-chart | Food Safety | /about-our-products/food-safety | monsterNavAboutOurProducts |
    | Handling Meat & Poultry Safely | /about-our-products/food-safety/handling-meat-poultry-safely | Food Safety | /about-our-products/food-safety | monsterNavAboutOurProducts |
    | Handling Seafood Safely | /about-our-products/food-safety/handling-seafood-safely | Food Safety | /about-our-products/food-safety | monsterNavAboutOurProducts |
    | Irradiation (US and Canada) | /about-our-products/food-safety/irradiation-us-and-canada | Food Safety | /about-our-products/food-safety | monsterNavAboutOurProducts |
    | More | /about-our-products/food-safety | Food Safety | /about-our-products/food-safety | monsterNavAboutOurProducts |

@monster-nav @about-our-products @nojs @unauth
Scenario Outline: About Our Products -  GMOs: Your Right to Know
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  
  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | GMO Quick Facts: What, Why, Where | /gmo-quick-facts-what-why-where | GMOs: Your Right to Know | /gmo-your-right-know | monsterNavAboutOurProducts |
    | How To Shop if You’re Avoiding GMOs | /how-shop-if-youre-avoiding-gmos | GMOs: Your Right to Know | /gmo-your-right-know | monsterNavAboutOurProducts |
    | Our Commitment to GMO Labeling | /our-commitment-gmo-labeling | GMOs: Your Right to Know | /gmo-your-right-know | monsterNavAboutOurProducts |
    | FAQs on GMOs | /faqs-gmos | GMOs: Your Right to Know | /gmo-your-right-know | monsterNavAboutOurProducts |


@monster-nav @about-our-products @nojs
Scenario Outline: About Our Products -  Our Product Lines
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  
  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | 365 Everyday Value | /about-our-products/product-lines/365-everyday-value | Our Product Lines | /about-our-products/our-product-lines | monsterNavAboutOurProducts |
    | Engine 2 Plant-Strong® | /about-our-products/product-lines/engine-2 | Our Product Lines | /about-our-products/our-product-lines | monsterNavAboutOurProducts |
    | Whole Foods Market Brand | /about-our-products/product-lines/whole-foods-market-brand | Our Product Lines | /about-our-products/our-product-lines | monsterNavAboutOurProducts |
    | Whole Trade | /about-our-products/product-lines/whole-trade | Our Product Lines | /about-our-products/our-product-lines | monsterNavAboutOurProducts |

@monster-nav @about-our-products @nojs @unauth
Scenario Outline: About Our Products -  The Whole Deal
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Coupons | /coupons | The Whole Deal | /about-our-products/whole-deal | monsterNavAboutOurProducts |
    | Budget-Friendly Recipes | /about-our-products/whole-deal/budget-recipes | The Whole Deal | /about-our-products/whole-deal | monsterNavAboutOurProducts |
    | Money-Saving Tips | /about-our-products/whole-deal/money-saving-tips | The Whole Deal | /about-our-products/whole-deal | monsterNavAboutOurProducts |

@monster-nav @about-our-products @nojs @unauth
Scenario Outline: About Our Products -  Locally Grown
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Local Growers Map | /locally-grown | Locally Grown | /local | monsterNavAboutOurProducts |
    | Local Vendor Profiles | /local-vendor-profiles | Locally Grown | /local | monsterNavAboutOurProducts |

##### - About Our Products End - #####

##### - Recipies Start - #####

@monster-nav @recipies @nojs @unauth @quick
Scenario Outline: Recipies - Recipies
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Featured | /recipe-landing-featured | Recipes | /recipe-landing | monsterNavRecipies |
    | Newest | /recipe-landing-newest | Recipes | /recipe-landing | monsterNavRecipies |
    | Top Rated | /recipe-landing-toprated | Recipes | /recipe-landing | monsterNavRecipies |

@monster-nav @recipies @nojs @auth
Scenario Outline: Recipies - My Saved Recipies
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
#@here
@monster-nav @recipies @nojs @unauth
Scenario Outline: Recipies - Cooking and Entertainment Guides
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | More | /cooking-entertainment-guides | Cooking & Entertainment Guides | /cooking-entertainment-guides | monsterNavRecipies |
#These change seasonally

@monster-nav @recipies @nojs @unauth @quick
Scenario Outline: Recipies - Food Guides
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Beans | /recipes/food-guides/beans | Food Guides | /recipes/food-guides | monsterNavRecipies |
    | Chocolate | /recipes/food-guides/chocolate | Food Guides | /recipes/food-guides | monsterNavRecipies |
    | Cooking Oils | /recipes/food-guides/cooking-oils | Food Guides | /recipes/food-guides | monsterNavRecipies |
    | Dairy Products | /recipes/food-guides/dairy-products | Food Guides | /recipes/food-guides | monsterNavRecipies |
    | Eggs | /recipes/food-guides/eggs | Food Guides | /recipes/food-guides | monsterNavRecipies |
    | More | /recipes/food-guides | Food Guides | /recipes/food-guides | monsterNavRecipies |

@monster-nav @recipies @nojs @unauth @quick
Scenario Outline: Recipies - Seasonal Recipe Collections
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | More | /seasonal-recipe-collections | Seasonal Recipe Collections | /seasonal-recipe-collections | monsterNavRecipies |

##### - Recipies End - #####

##### - Online Ordering Start - #####

@monster-nav @online-ordering @javascript @api @unauth @no-store-selected
Scenario Outline: Online Ordering - No Store
  Given I have no store selected
  When I reload the page
  Given I wait for "1000" milliseconds
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Online Ordering | /?s= | Available At Your Store | /online-ordering-available-your-store | monsterNavOnlineOrdering |

@monster-nav @online-ordering @javascript @unauth @store-selected @disabled
Scenario: Online Ordering - Available At Your Store
  Given I have a store selected
  Given I wait for "1000" milliseconds
  
@monster-nav @online-ordering @nojs @unauth
Scenario Outline: Online Ordering - Nationwide
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Wine Club | /wine-club | Nationwide | /nationwide | monsterNavOnlineOrdering |
    | Gift Cards | /buy-gift-cards | Nationwide | /nationwide | monsterNavOnlineOrdering |
    | American Express Reload | /gift-cards/american-express-reload | Nationwide | /nationwide | monsterNavOnlineOrdering |
    | Gift Card(s) Terms and Conditions | /gift-cards/gift-cards-terms-and-conditions | Nationwide | /nationwide | monsterNavOnlineOrdering |
    | Gift Cards FAQ | /gift-cards/gift-cards-faq | Nationwide | /nationwide | monsterNavOnlineOrdering |

##### - Online Ordering End - #####
#@here
##### - Mission & Values Start - #####
@monster-nav @mission-values @nojs @unauth
Scenario Outline: Mission and Values - Our Values
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Values Matter | /values-matter | Our Values | /our-values | monsterNavMissionValues |
    | Core Values | /core-values | Our Values | /our-values | monsterNavMissionValues |

@monster-nav @mission-values @nojs @unauth
Scenario Outline: Mission and Values - Commitment to Society
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Community Giving | /mission-values/caring-communities/community-giving | Commitment to Society | /mission-values/caring-communities | monsterNavMissionValues |
    | Local Producer Loan Program | /mission-values/caring-communities/local-producer-loan-program | Commitment to Society | /mission-values/caring-communities | monsterNavMissionValues |
    | Whole Kids Foundation | /mission-values/caring-communities/whole-kids-foundation | Commitment to Society | /mission-values/caring-communities | monsterNavMissionValues |
    | Whole Planet Foundation | /mission-values/caring-communities/whole-planet-foundation | Commitment to Society | /mission-values/caring-communities | monsterNavMissionValues |
    | Whole Cities Foundation | /whole-cities-foundation | Commitment to Society | /mission-values/caring-communities | monsterNavMissionValues |


@monster-nav @mission-values @nojs @unauth
Scenario Outline: Mission and Values - Environmental Stewardship
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | A Better Bag | /mission-values/environmental-stewardship/better-bag | Environmental Stewardship | /mission-values/environmental-stewardship | monsterNavMissionValues |
    | Cloned Meat Q&A | /mission-values/environmental-stewardship/cloned-meat-qa | Environmental Stewardship | /mission-values/environmental-stewardship | monsterNavMissionValues |
    | Doing The Green Thing | /mission-values/environmental-stewardship/doing-green-thing | Environmental Stewardship | /mission-values/environmental-stewardship | monsterNavMissionValues |
    | Eco Scale | /mission-values/environmental-stewardship/eco-scale | Environmental Stewardship | /mission-values/environmental-stewardship | monsterNavMissionValues |
    | Green Mission | /mission-values/environmental-stewardship/green-mission | Environmental Stewardship | /mission-values/environmental-stewardship | monsterNavMissionValues |

@monster-nav @mission-values @nojs @unauth
Scenario Outline: Mission and Values - Organic Farming
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | The Basics of Organic | /mission-values/organic/basics-organic | Organic Farming | /mission-values/organic| monsterNavMissionValues |
    | Is Organic Food Better For You? | /mission-values/organic/organic-food-better-you | Organic Farming | /mission-values/organic| monsterNavMissionValues |
    | Growth of the Organics Industry | /mission-values/organic/growth-organics-industry | Organic Farming | /mission-values/organic| monsterNavMissionValues |
    | About Organic Farming | /mission-values/organic/about-organic-farming | Organic Farming | /mission-values/organic| monsterNavMissionValues |
    | Farming Organically | /mission-values/organic-farming/farming-organically | Organic Farming | /mission-values/organic| monsterNavMissionValues |
    | More | /mission-values/organic | Organic Farming | /mission-values/organic | monsterNavMissionValues |

@monster-nav @mission-values @nojs @unauth
Scenario Outline: Mission and Values - Seafood Sustainability
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Seafood Sustainability Basics | /seafood-sustainability-basics | Seafood Sustainability | /mission-values/seafood-sustainability | monsterNavMissionValues |
    | Our Collaboration with the Marine Stewardship Council | /missions-values/seafood-sustainability/our-collaboration-marine-stewardship-council | Seafood Sustainability | /mission-values/seafood-sustainability | monsterNavMissionValues |
    | Wild-Caught Seafood Sustainability Ratings | /mission-values/seafood-sustainability/wild-caught-seafood-sustainability-ratings | Seafood Sustainability | /mission-values/seafood-sustainability | monsterNavMissionValues |
    | Aquaculture | /mission-values/seafood-sustainability/aquaculture | Seafood Sustainability | /mission-values/seafood-sustainability | monsterNavMissionValues |
    | Seafood Sustainability FAQ | /mission-values/seafood-sustainability/seafood-sustainability-faq | Seafood Sustainability | /mission-values/seafood-sustainability | monsterNavMissionValues |
    | More | /mission-values/seafood-sustainability | Seafood Sustainability | /mission-values/seafood-sustainability | monsterNavMissionValues |

@monster-nav @mission-values @nojs @unauth
Scenario Outline: Mission and Values - Whole Trade
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Whole Trade Helps Real People | /whole-trade/whole-trade-helps-real-people | Whole Trade | /mission-values/whole-trade-program | monsterNavMissionValues |
    | Whole Trade Videos | /whole-trade/whole-trade-videos | Whole Trade | /mission-values/whole-trade-program | monsterNavMissionValues |
    | Whole Trade Products | /whole-trade/whole-trade-products | Whole Trade | /mission-values/whole-trade-program | monsterNavMissionValues |
    | Our Certifier Partners | /mission-values/whole-trade-program/certifier-partners | Whole Trade | /mission-values/whole-trade-program | monsterNavMissionValues |

@monster-nav @mission-values @nojs @unauth
Scenario Outline: Mission and Values - Animal Welfare
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | 5-Step™ Animal Welfare Rating | /mission-values/animal-welfare/5-step-animal-welfare-rating | Animal Welfare | /mission-values/animal-welfare | monsterNavMissionValues |
    | Animal Welfare Basics | /mission-values/animal-welfare/animal-welfare-basics | Animal Welfare | /mission-values/animal-welfare | monsterNavMissionValues |
    | Collaboration with Global Animal Partnership | /mission-values/animal-welfare/collaboration-global-animal-partnership | Animal Welfare | /mission-values/animal-welfare | monsterNavMissionValues |
    | Meet the Farmers & Ranchers | /mission-values/animal-welfare/meet-farmers-ranchers | Animal Welfare | /mission-values/animal-welfare | monsterNavMissionValues |

##### - Mission & Values End - #####

##### - Blog Start - #####
@monster-nav @blog @nojs @unauth @quick
Scenario Outline: Blog - Whole Story
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Whole Story Blog | /blog/whole-story | Whole Story | /whole-story | monsterNavBlog |

@monster-nav @blog @nojs @unauth
Scenario Outline: Blog - CEO Blogs
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | John Mackey's Blog | /blog/john-mackeys-blog | CEO Blogs | /ceo-blogs | monsterNavBlog |
    | Walter Robb's Blog | /blog/walter-robbs-blog | CEO Blogs | /ceo-blogs | monsterNavBlog |

##### - Blog End - #####

##### - Store Departments Start - #####
@monster-nav @store-departments @javascript @unauth
Scenario Outline: Store Departments - Unique To Your Store
  And I wait for ajax calls to finish
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Events Calendar | /events | Unique To Your Store | /unique-to-your-store | monsterNavStoreDepartments |
    | Find Your Store | /stores/list | Unique To Your Store | /unique-to-your-store | monsterNavStoreDepartments |

@monster-nav @store-departments @javascript @unauth
Scenario Outline: Store Departments - Store Departments
  And I wait for ajax calls to finish
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region

  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Bakery | /department/bakery | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Beer | /department/beer | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Bulk | /department/bulk | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Cheese | /department/cheese | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Coffee &amp; Tea | /department/coffee-tea | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Floral | /department/floral | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Grocery | /department/grocery | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Meat &amp; Poultry | /department/meat-poultry | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Prepared Foods | /department/prepared-foods | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Produce | /department/produce | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Seafood | /department/seafood | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Wine | /department/wine | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Whole Body | /department/whole-body | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Pets | /department/pets | Store Departments | /store-departments | monsterNavStoreDepartments |        

##### - Store Departments End - #####