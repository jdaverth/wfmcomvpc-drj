Feature: Individual Products
  In order to assure basic functionality of each product page
  As an unauthenticated user 
  I need to assure individual product pages load successfully. 
  
Background:
  Given I am on "/products/aspall-golden-malt-vinegar"

 @product @unauth @nojs @quick
 Scenario: Product Page Should Load
 Then I should see the text "Aspall Golden Malt Vinegar" in the "content" region