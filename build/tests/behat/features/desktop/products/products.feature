Feature: About Our Products Page  
  In order to assure basic functionality of the products page
  As an unauthenticated user 
  I need to assure all page blocks are displayed correctly 
  
Background:
  Given I am on "/about-our-products"
  
@products @nojs @quick
Scenario: Check basic blocks of the page are loading 
  Then I should see at least "2" "#block-views-generic-page-items-marqee .view-display-id-marqee .field-content" 
  And I should see an "#block-search-api-page-product-search" element
  And I should see the heading "Product Search"
  And I should see an "#search-api-page-search-form-product-search" element
  And I should see an "#edit-submit-14" element 
  And I should see an "#block-views-promo-blocks-block" element 
  And I should see an "#block-views-generic-page-items-list-copy" element
  And I should see at least "5" "#block-views-generic-page-items-list-copy .list-row .brief"
  And I should see an "#sidebar-second" element 
  
@products @product-search @javascript
Scenario: Product search 
  Given That the Window is Full Screen  
  Given for "edit-keys-14" I enter "bread"
  And I press "edit-submit-14"
  Then I should be on "/products/search/bread"
  And I should see "bread" in the ".search_term" element
  And I should see relevant search results for "bread" identified by ".search-results .search-result h3.title a" in the "content" region 
  
@products @product-search @javascript
Scenario: Blank query product search
  Given for "edit-keys-14" I enter ""
  And I press "edit-submit-14"
  Then I should be on "/about-our-products"
  And I should see the error message "Please enter at least one keyword."

@products @product-search @javascript
Scenario: Numeric query
  Given That the Window is Full Screen
  Given for "edit-keys-14" I enter "123"
  And I press "edit-submit-14"
  Then I should be on "/products/search/123"
  And I should see "123" in the ".search_term" element
  And I should see at least "4" ".search-result"

@products @product-search @javascript
Scenario: Query page pagination
  Given That the Window is Full Screen  
  Given for "edit-keys-14" I enter "cheese"
  And I press "edit-submit-14"
  Then I should be on "/products/search/cheese"
  And I should see "cheese" in the ".search_term" element
  And I should see an ".pager" element 
  #When I click on some of the paginated links in ".pager" there should be content for each link in ".search-result" elements