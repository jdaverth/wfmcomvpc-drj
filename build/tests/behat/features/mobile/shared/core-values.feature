Feature: Core Values
  To ensure the mobile core values page loads in entirety
  As an unauthenticated user
  I need to verify all page elements load correctly

Background:
  Given I am on "/mission-values/core-values"

@mobile @unauth @nojs @quick @core-values
Scenario: Core Values Page is Complete
  Then I should see the heading "Our Core Values" in the "content" region
  Then the ".view-content .faq-pages-list ul" element should contain at least "8" "li a" elements in the "content" region  
  Then the page element identified by ".view-generic-page-items .view-header p" has at least "30" words in the "content" region

@mobile @nojs @unauth @quick @core-values
Scenario: Individual Core Values Page - required page elements are visible
  Then I click a random element identified by ".view-content .faq-pages-list ul li a" in the "content" region
  Then I should be on the selected page
  Then the page element identified by ".view-content .views-row .views-field-field-body-mobile .field-content" has at least "100" words in the "content" region