Feature: Deals Page
  In order to ensure mobile access to deals and specials
  I need to visit the deals page
  And be certain that deals load

Background:
  Given I am on "/mobile/coupons/home"

@mobile @unauth @nojs @quick @deals
Scenario: On Sale Items are Visible
  Then the "div#coupon-wrapper" element should contain at least "10" ".coupon-container" elements in the "content" region

@mobile @unauth @javascript @deals
Scenario: Checkbox toggles work correctly
  When I click "Check all"
  Then all checkboxes in container "#coupon-wrapper" should be "checked" in the "content" region
  When I click "Uncheck all"
  Then all checkboxes in container "#coupon-wrapper" should be "unchecked" in the "content" region  

@mobile @unauth @javascript @deals
Scenario: Checkbox print loads print window with coupons
  When I click "Check all"
  When I click "Print Selected" in the "content" region to open a print window
  Then I switch to window "CouponPrint"
  Then the "div#coupon-wrapper" element should contain at least "10" "td.couponbody" elements
