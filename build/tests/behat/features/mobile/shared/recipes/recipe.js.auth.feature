Feature: Individual Mobile Recipe Page - Authenticatd Functionality
  In order to ensure individual recipe pages load
  As an authenticated user
  I need to be able to add and remove recipes to and from the recipe box

Background:
  Given I am not logged in
  Given I am on "mobile/recipes"
  When I click "Sign in or Create account" in the "header" region
  Then I should be on "https://users.wholefoodsmarket.com/oauth/signin"
  Then I login as a test user

@mobile @recipe @auth @javascript
Scenario: Add to Mobile Recipe Box and Remove From Mobile Recipe Box
  Given That my mobile recipe box is empty
  Given I am on "mobile/recipes"
  Then I click a random element identified by ".view-recipes .views-row a" in the "content" region
  Then I should be on the selected page with title identified by "h2.page-title" in the "content" region
  When I click "Add to My Recipe Box"
  Then I should see the link "Remove from My Recipe Box"
  When I click "Remove from My Recipe Box"
  Then I should see the link "Add to My Recipe Box"

@mobile @recipe @auth @javascript
Scenario: Add to Mobile Recipe Box View Saved Recipes Remove From Mobile Recipe Box
  Given That my mobile recipe box is empty
  Given I am on "mobile/recipes"
  Then I click a random element identified by ".view-recipes .views-row a" in the "content" region
  When I click "Add to My Recipe Box"
  And I wait for "3000" milliseconds
  Given I am on "myrecipebox"
  When I toggle open all panels in the "content" region
  Then I should see the randomly added recipe identified by "ul li a.recipe-link" in the "content" region
  Given That my mobile recipe box is empty
  Then I should see the text "You have no saved recipes." in the "content" region