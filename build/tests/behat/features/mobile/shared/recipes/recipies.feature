Feature: Recipe Search Features
  In order to ensure mobile recipe search functionality
  I need to test mobile recipe search page
  And be certain that search results load correctly

Background:
  Given I am on "/mobile/recipes"

@mobile @unauth @nojs @quick @recipes
Scenario: Recipe Search Page Loads
  Then I should see the heading "Recipe search" in the "content" region
  Then I should see the heading "Cooking & Entertaining Guides" in the "content" region
  Then I should see the heading "My Recipe Box" in the "content" region
  Then the ".pane-recipes .view-recipes" element should contain at least "6" ".views-row" elements in the "content" region

@mobile @unauth @nojs @quick @recipes
Scenario: Entertaining Guides Available
  Then the ".faq-pages-list ul" element should contain at least "5" "li" elements in the "content" region

@mobile @unauth @nojs @quick @recipes
Scenario: Link to My Recipe Box
  Then I should see the link "My Recipe Box" in the ".myrecipebox .pane-content" container

@mobile @unauth @javascript @recipes
Scenario: Recipe Search Works
  Given I fill in "#edit-search-box" with a random search term
  And I press "Search"
  Then I should see the regex "/Recipes:\s\d+\sresults/" identified by ".pane-content .view-recipe-search .view-header" in the "content" region
  Then the "select#edit-field-recipe-category" element should contain at least "11" "option" elements in the "content" region
  Then the "select#edit-field-recipe-course" element should contain at least "14" "option" elements in the "content" region
  Then the "select#edit-field-special-diet" element should contain at least "13" "option" elements in the "content" region
  Then the "select#edit-sort-by" element should contain at least "2" "option" elements in the "content" region
  Then the ".view-recipe-search .view-content" element should contain at least "5" ".views-row" elements in the "content" region
  Then I should see the link "Load More Recipes"
  Then I click a random element identified by ".panel-panel .inside .views-field-title .field-content a" in the "content" region
  Then I should be on the selected page with title identified by "h2.page-title" in the "content" region

@mobile @unauth @javascript @recipes
Scenario: Load More Recipes Works
  Given I fill in "edit-search-box" with "chicken"
  And I press "Search"
  Then I should see "10" search results identified by ".view-content .views-row" in the "content" region
  And I click "Load More Recipes"
  Then I should see "20" search results identified by ".view-content .views-row" in the "content" region
  And I click "Load More Recipes"
  Then I should see "30" search results identified by ".view-content .views-row" in the "content" region
