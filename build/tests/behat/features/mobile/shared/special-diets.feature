Feature: Special Diets
  To ensure the mobile special diets load in entirety
  As an unauthenticated user
  I need to verify all page elements load correctly

Background:
  Given I am on "/healthy-eating/special-diets"

@mobile @special-diets @javascript @unauth @quick
Scenario: Special diets page - required page elements are visible
  Then I should see the heading "Special Diets" in the "content" region
  Then the page element identified by ".pane-content .view-generic-page-items .view-header" has at least "50" words in the "content" region
  Then the ".view-generic-page-items .view-content .faq-pages-list ul" element should contain at least "8" "li a" elements in the "content" region

@mobile @special-diets @nojs @unauth @quick
Scenario: All product FAQ links are visible
  Then I should see the link "Gluten Free" with href "/healthy-eating/special-diets/gluten-free" in the "content" region
  Then I should see the link "Dairy Free" with href "/healthy-eating/special-diets/dairy-free" in the "content" region
  Then I should see the link "Sugar Conscious" with href "/healthy-eating/special-diets/sugar-conscious" in the "content" region
  Then I should see the link "Vegetarian" with href "/healthy-eating/special-diets/vegetarian" in the "content" region
  Then I should see the link "Vegan" with href "/healthy-eating/special-diets/vegan" in the "content" region
  Then I should see the link "Wheat Free" with href "/healthy-eating/special-diets/wheat-free" in the "content" region
  Then I should see the link "Low Fat" with href "/healthy-eating/special-diets/low-fat" in the "content" region
  Then I should see the link "Low Sodium" with href "/healthy-eating/special-diets/low-sodium" in the "content" region

@mobile @special-diets @nojs @unauth @quick
Scenario: Individual special diet page - required page elements are visible
  Then I click a random element identified by ".view-content .faq-pages-list ul li a" in the "content" region
  Then I should be on the selected page with title identified by "h1#page-title" in the "content" region
  Then the page element identified by ".view-id-special_diets .view-content .views-row" has at least "200" words in the "content" region
  Then the ".view-generic-page-items .view-content .faq-pages-list ul" element should contain at least "8" "li a" elements in the "content" region

