<?php

class BehatReportAggregator {

  const REPORT_PATH = 'reports';
  const XML_REPORT_PATH = 'reports/xml';
  const HTML_REPORT_PATH = 'reports/html';
  const REPORT_FILE_NAME = 'aggregate-behat-errors.html';
  
}
?>