<?php

require_once 'AcquiaSearchApiSolrHttpTransport.php';

class AcquiaSearchApiSolrConnection extends SearchApiSolrConnection {
  public function __construct(array $options) {
    parent::__construct($options);

    $this->newClient = trim(parent::SVN_REVISION, '$ :A..Za..z') > 40;
    if ($this->newClient) {
      $this->_httpTransport = new AcquiaSearchApiSolrHttpTransport($this->http_auth);
    }
    else {
      throw new Exception(t('This module only works with the latest version of SolrPhpClient'));
    }
  }
}