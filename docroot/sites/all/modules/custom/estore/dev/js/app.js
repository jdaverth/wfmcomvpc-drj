// App.js
// This is the main javascript file for this project.

if (typeof _NO_ROUTER === 'undefined') {
  window._NO_ROUTER = false;
}

jQuery(function() {

  if (jQuery.browser.msie && parseInt(jQuery.browser.version) < 10) {
    jQuery('body').addClass('old_ie');

  }

  if (jQuery('.user_menu:has(ul)').length) {
    jQuery( '.user_menu:has(ul)' ).doubleTapToGo();
  }
  //
  // if (jQuery('.create_account').length) {
  //   console.log('binding handler for account');
  //   jQuery('.create_account').bind('click',function(e) {
  //       jQuery.cookie('login_bounce','/shop/'+store_code,{expires:1,path:'/','domain':'wholefoodsmarket.com'});
  //       e.preventDefault();
  //       window.location='https://users.wholefoodsmarket.com/oauth/signin?redirect_uri=http%3A%2F%2Fd4.wholefoodsmarket.com%2Fjanrain_capture%2Foauth&xd_receiver=http%3A%2F%2Fd4.wholefoodsmarket.com%2Fsites%2Fall%2Fmodules%2Fcontrib%2Fjanrain_capture%2Fxdcomm.html&client_id=er96yccmjajudw2vu7t489fej7eky2cz&recover_password_callback=Drupal.janrainCapture.closeRecoverPassword&response_type=code'
  //       return false;
  //   });
  // }

});

(function(jQuery) {

angular.module('services', [])
.factory('Estore', ['$http','$timeout', '$rootScope', 'DataLayer',

    function($http, $timeout, $rootScope, DataLayer) {

      var urls = {
        'addtocart': '/shop/' + store_code + '/actions/add',
        'cartcount': '/shop/' + store_code + '/actions/count',
        'removeitem': '/shop/' + store_code + '/actions/remove',
        'updateqty': '/shop/' + store_code + '/actions/updateqty',
        'getcart' : '/shop/' + store_code + '/actions/getcart',
        'prepareOrder': '/shop/' + store_code + '/actions/prepareOrder',
      }

      return {

        ajax: function(options,success) {

          $http(options).success(function(data) {

              if (data.status=='error') {
                $rootScope.$broadcast('busy', false);
                $rootScope.$broadcast('apierror',data.message);
              } else {
                if (success) {
                  success.call(null,data);
                }
              }
          }).error(function(err) {

          });
        },

        cartCount: function(cb) {

          if (store_code && store_code !== "") {
            this.ajax({
              method: 'GET',
              url: urls.cartcount
            },function(data) {
              cb(data);
            });
          }
        },

         addToCart: function(order){

          order.ajax = true;

            this.ajax(
              {
                method  : 'POST',
                url     : urls.addtocart,
                data    : jQuery.param(order),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
              },
              function(data){
                $rootScope.$broadcast('cart.updated', data.cart);
                DataLayer.addToCartEvent(order, data.cart);
              }
            );

          },
          removeFromCart: function(cart_item_id,cb) {

              var params = {
                id: cart_item_id
              };

              this.ajax({
                method  : 'POST',
                url     : urls.removeitem,
                data    : jQuery.param(params),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
              },function(data){
                  $rootScope.$broadcast('cart.updated', {});
                  if (cb) cb(data);
                });
          },
          updateQuantity: function(cart_item_id,quantity,cb) {

              var params = {
                id: cart_item_id,
                qty: quantity
              };

              this.ajax({
                method  : 'POST',
                url     : urls.updateqty,
                data    : jQuery.param(params),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
              },function(data){
                $rootScope.$broadcast('cart.updated', {});
                DataLayer.changeQuantityEvent(params);
                if (cb) cb(data);
              });

          },
          getCart: function(cb){
            if (store_code && store_code !== "") {
              this.ajax({
                method: 'GET',
                url: urls.getcart
              },function(data) {
                cb(data);
              });
            }
          },
          prepareOrder: function(params,cb) {
              this.ajax({
                method: 'POST',
                url: urls.prepareOrder,
                data: jQuery.param(params),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
              },function(data){
                  if (cb) cb(data.cart);
                });
          },
          safeString: function(string, limit){
            if(!string){
              return;
            }
            limit = limit || 255;
            var div = document.createElement("div");
            div.innerHTML = string;
            var text = div.textContent || div.innerText || "";

            if(text.length <= limit){
              return text;
            }else{
              return false;
            }
          },
          dollars: function(value){
            value= Math.round(value* 100) / 100.
            return value.toFixed(2);
          },
          validateEmail: function(email){
                return /^.+@.+\..+$/.test(email);
          },
          numbersOnly: function(string){
                if(!string){
                    return;
                }
                return string.match(/[0-9]/g).join('');
            },

          validateQty: function(qty){
            if(isNaN(qty)){
              return 1;
            }
            if(!qty){
              return 1;
            }
            if(qty > 1000 ){
              return 1000;
            }
            if(qty < 1){
              return 1;
            }
            return qty;
          },

          formatPhoneNumber: function(number){
              number= number.match(/[0-9]/g).join('');
              return number= number.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "($1) $2-$3");
          },

      }
    }

  ]);

/**
 * DataLayer factory
 * Contains logic for pushing specific events into window.dataLayer
 * for Google Tag Manager from Analytics Pros. 
 */
angular.module('services')
.factory('DataLayer', [function() {
    var dataLayer = {};

    /**
     * Private function responsible for pushing the
     * event object into window.dataLayer.
     *
     * @param event
     */
    var push = function(event) {
      window.dataLayer = window.dataLayer || [];
      window.dataLayer.push(event);
    };

    /**
     * Add the 'add-to-cart' event to the DataLayer.
     *
     * @params order
     * @params cart
     */
    dataLayer.addToCartEvent = function(order, cart) {
      var cartEmpty = 'yes';
      // The cart will include the item we just added, so if the length is greater than 1
      // that means the cart was not empty.
      if (cart.length > 1) {
        cartEmpty = 'no';
      }

      push({
        event: 'add-to-cart',
        id: order.prod,
        quantity: order.qty,
        variant: '',
        brand: 'Prepared Foods',
        cartEmpty: cartEmpty
      });
    };

    /**
     * Add the 'change-quantity' event to the DataLayer.
     *
     * @param params
     */
    dataLayer.changeQuantityEvent = function(params) {
      var id = $('#items_list_' + params.id + ' .name a').attr('href').split('/');
      push({
        event: 'change-quantity',
        product: { id: id[id.length-1], quantity: params.qty }
      });
    };

    /**
     * Add the 'checkout-error' event to the dataLayer.
     *
     * @param step
     * @param message
     */
    dataLayer.validationErrorEvent = function(step, message) {
      push({
        event: 'checkout-error',
        checkoutStep: step,
        error: message
      });
    };

    /**
     * Add the 'order-review' event to the dataLayer
     */
    dataLayer.orderReviewEvent = function() {
      push({ event: 'order-review' });
    };

    return dataLayer;
}]);

var _estore = angular.module('estore', ['services', function(){

}]);


_estore.controller('app', ['$scope', '$rootScope','$sce', 'Estore',

  function($scope,$rootScope, $sce, Estore){

   $scope.busy = false;
   $scope.window = {};

  //  Estore.getCart(function(cart){
  //    $rootScope.cart = cart;
  //  });

   $scope.showCartDropDown = function(show){
     if(show){
       $rootScope.cart_dropdown = true;
     }else{
       $rootScope.cart_dropdown = false;
     }
   }

   //To open a dialog from anywhere in the app set $rootScope.dialog.open = true.
   //the message and state are optional
   //supported states: error, warning, success
   //if state is null default dialog styles are used
   $rootScope.dialog = {
     open: false,
     message: null,
     state: null,
     args: {}
   }

   $rootScope.$on('busy', function(e, state){

     if(state === true){
       $scope.busy = true;
     }else{
       $scope.busy = false;
     }
   })

   $scope.cartdropdown="/sites/all/modules/custom/estore/public/partials/cart.dropdown.html";

  // if (store_code && store_open) {
  // per keff's instructions, ignore SAGE's store closed flag.
    if (store_code) {
    Estore.cartCount(function(data) {
      $scope.updateCartData(data);
    });
  }

  $scope.updateCartData = function(data) {

    $scope.cartcount = data.count;
    $scope.window.earliest_pickup = data.earliest_pickup;
    $scope.window.latest_pickup = data.latest_pickup;
    $scope.window.delivery_method = data.delivery_method;
    jQuery('#cart-count').text(data.count);

  }

  $scope.$on('cart.updated', function(e, cart){


      $rootScope.cart = cart;

      Estore.cartCount(function(data) {

        $scope.cartcount = data.count;
        $scope.window.earliest_pickup = data.earliest_pickup;
        $scope.window.latest_pickup = data.latest_pickup;

        jQuery('#cart-count').text(data.count);
      });
    });


    $scope.$on('apierror',function(e,error) {
//      $scope.busy = false;
      $scope.openDialog(error,{title: 'Error',status:'error'});

    });


    // init product page modal
   $scope.modal = { open: false };

    // init generic dialog
    $scope.dialog = {
       open: false,
       message: null,
       state: null,
       title: 'Alert',
       args: {},
       ondismiss: function(){}
     }

    $scope.getEarliestPickup = function() {

      return $scope.window.earliest_pickup;
    }

    $scope.getLatestPickup = function() {

      return $scope.window.latest_pickup;
    }

    $scope.openDialog = function(message, options){
      options = options || {};
      $scope.dialog.message = message;
      $scope.dialog.status = options.status || null;
      $scope.dialog.template = options.template || null;
      $scope.dialog.title = options.title || 'Alert';
      $scope.dialog.args = options.args || {};
      var re = /(\$\d*\.\d+\s-\s\$\d*\.\d+)/i;
      if ((typeof $scope.dialog.args.total != 'undefined') && ($scope.dialog.args.total.match(re))) {
          var tots = $scope.dialog.args.total.split(' ');
          if (Array.isArray(tots) && tots.length == 3) {
              $scope.dialog.args.total = tots[2];
          }
      }
      $scope.dialog.open = true;
      $scope.dialog.onclose = options.onclose || function(){};
    }

    $scope.closeFromOverlay = function($event){
      if($event){
        if(!jQuery($event.srcElement).hasClass('overlay') ){
          return;
        }
      }
      $scope.closeDialog();
    }

    $scope.closeDialog = function(){

      $scope.dialog.status = null;
      $scope.dialog.message = null;
      $scope.dialog.open = false;
      $scope.template = null;

      if(typeof($scope.dialog.onclose) == 'function'){
        $scope.dialog.onclose();
      }
      $scope.args = {};
    }

    $scope.printThis = function($event) {
      window.print();
    }

    $scope.generateCalendar = function() {

        var cal = ics();

        var date = json_data.PICKUP_DATE;
        var start_time = json_data.PICKUP_TIME;
        var end_time = json_data.PICKUP_TIME;

        var store = json_data.STORE;
        var address = json_data.SHIPPINGADDRESS.STREET1 + ' ' + json_data.SHIPPINGADDRESS.CITY + ', ' + json_data.SHIPPINGADDRESS.STATE + ' ' + json_data.SHIPPINGADDRESS.ZIP;
        var orderid = json_data.ID;
        var start;
        var end;

        if (start_time=='AM') {
          start_time = '8:00 am';
          end_time = '12:00 pm';

          start = new Date(date + ' ' + start_time);
          end = new Date(date + ' ' + end_time);

        }
        if (end_time=='PM') {
          start_time = '12:00 pm';
          end_time = '8:00 pm';

          start = new Date(date + ' ' + start_time);
          end = new Date(date + ' ' + end_time);

        } else {

          start = new Date(date + ' ' + start_time);
          end = new Date(start.getTime() + 60 *60000);

        }


        cal.addEvent(
            'Pickup order from Whole Foods Market',
            'Your order #' + orderid + ' under the name ' + json_data.SHIPPINGADDRESS.FIRSTNAME + ' ' + json_data.SHIPPINGADDRESS.LASTNAME + ' will be available for pickup at ' + start_time + '!  View order info here: http://www.wholefoodsmarket.com/shop/'+store+'/customers/orders/' + orderid,
            address,
            start.toLocaleString(),
            end.toLocaleString()
        );

        cal.download();

    }


    $scope.goBack = function($event) {
       window.history.back();
    }

    $scope.clickHref = function($event) {
      if($event && $event.currentTarget){
        var href = jQuery($event.currentTarget).attr('data-href');
        window.location=href;
        return false;
      }
    }

    $scope.nonzero = function(val) {
      val = parseFloat(val);
      return val > 0;
    }

}]);

_estore.controller('cartdropdown', ['$scope', '$rootScope','$sce', 'Estore',
  function($scope, $rootScope, $sce, Estore){

    $scope.show = $rootScope.cart_dropdown;
    $scope.cart = $rootScope.cart;
    $rootScope.cart_hovering = true;
  }])

_estore.controller('product', ['$scope', '$rootScope','$http','$sce','Estore',

  function($scope, $rootScope,$http, $sce, Estore) {
    //initialize a baseprice and total
    //total = ((baseprice + options) * qty)
    if (jQuery('body').width() < 1024) {
      $scope.ismobileview = true;
    }else{
      $scope.ismobileview = false;
    }

    $scope.baseprice = Math.round(json_data.BASEPRICE * 100) / 100;
    $scope.increment_qty = 1;
    $scope.options = [];
    $scope.item = json_data;
    
    // set up micro copy for the page
    $scope.bodyCopy = {};
    $scope.bodyCopy.calories = Drupal.t('2,000 calories a day is used for general nutrition advice, but calorie needs vary. Additional nutritional information available upon request.');
    $scope.bodyCopy.chooseFrom = Drupal.t('Choose from:');
  
    // The types of items we can expect to find
    $scope.itemTypes = {
      STANDALONE: 0,
      GROUP: 1,
      LIKE_ITEM_PLATTER: 2,
      MEAL_KIT: 3
    };
    
    // Manages all the logic for displaying and interpreting nutrition data for this product
    var nutritionHandler = {
      /**
       * Gather all the nutrition information from the current product in order to display calorie
       * and serving size information to the user.
       *
       * @param item {Object} {Required} The item we'll be gathering our nutrition information from.
       * @param itemTypes {Object} {Required} A list of constants to make it easier to track what type of product we're dealing with.
       *
       * @returns The nutrition information for the current product.
       */
      initNutritionInfo: function (item, itemTypes) {
        var nutritionInfo;
    
        try {
          // set up nutrition data if it's available
          if (item.NUTRITION) {
            // This is a standalone item. Show calorie and serving info for the one item.
            nutritionInfo = nutritionHandler.parseStandaloneItem(item, itemTypes);
          } else if (item.OPTIONGROUPS && item.OPTIONGROUPS.OPTIONGROUP) { // does this item have multiple options?
            nutritionInfo = nutritionHandler.parseItemWithOptions(item.OPTIONGROUPS.OPTIONGROUP, itemTypes);
          } else if (item.ITEMCOMPONENTS && item.ITEMCOMPONENTS.ITEMS) {
            nutritionInfo = nutritionHandler.parseMealKitItem(item.ITEMCOMPONENTS.ITEMS, itemTypes);
          }
        } catch (e) {
          // If at any point we come across nutrition information that has invalid JSON data,
          // back out of the nutrition interpretation process entirely.
          console.error(e);
        }
    
        return nutritionInfo;
      },
  
      /**
       * Interpret nutrition data for standalone items.
       *
       * @param item {Object} {Required} The item we'll be gathering our nutrition information from.
       * @param itemTypes {Object} {Required} A list of constants to make it easier to track what type of product we're dealing with.
       *
       * @returns The nutrition information for the given item.
       */
      parseStandaloneItem: function (item, itemTypes) {
        var nutritionInfo = nutritionHandler.parseNutritionInfo(item.NUTRITION);
        
        nutritionInfo.calorieServingInfo = nutritionHandler.getCalorieText(nutritionInfo);
        nutritionInfo.itemType = itemTypes.STANDALONE;
        
        return nutritionInfo;
      },
  
      /**
       * Interpret nutrition data for items with options. This applies to group items as well as like item platters.
       *
       * @param optionGroups {Array} {Required} We'll be gathering our nutrition information from the item's options. (ex: sm/med/lg)
       * @param itemTypes {Object} {Required} A list of constants to make it easier to track what type of product we're dealing with.
       *
       * @returns The nutrition information for the given item.
       */
      parseItemWithOptions: function (optionGroups, itemTypes) {
        var component,
          option,
          options,
          i,
          j,
          k;
        // Unless we find component nutrition, this is a group item.
        // Show calorie and serving info for each option in the group.
        var nutritionInfo = {
          itemType: itemTypes.GROUP,
          components: []
        };
  
        // Go through our available options and look for the nutrition for the individual components
        for (i = 0; i < optionGroups.length; i++) {
          options = optionGroups[i].OPTIONS.OPTION;
          for (j = 0; j < options.length; j++) {
            option = options[j];
      
            // Listing the calories for the individual pieces that make up a product (blueberry muffin, strawberry muffin, etc.)
            // takes precedence over listing the calories for the options available for this product (small, medium, large)
            if (option.COMPONENTNUTRITION) {
              // Like item platters list out the components of a product individually
              nutritionInfo.itemType = itemTypes.LIKE_ITEM_PLATTER;
              nutritionInfo.components = nutritionHandler.parseNutritionInfo(option.COMPONENTNUTRITION);
        
              // all options should have the same list of components
              // (ex: all muffin platter options should consist of the same kind of muffins)
              // If not, this logic will need to get significantly more complicated,
              // as we would need to start diffing components between all the options.
              for (k = 0; k < nutritionInfo.components.length; k++) {
                component = nutritionInfo.components[k];
                component.calorieServingInfo = nutritionHandler.getCalorieText(component);
              }
        
              // No need to continue as we have all the components we need to show (hopefully)
              break;
            } else if (option.NUTRITION) {
              // add this option's nutrition items
              component = nutritionHandler.parseNutritionInfo(option.NUTRITION);
              component.name = option.NAME;
              component.calorieServingInfo = nutritionHandler.getCalorieText(component);
              nutritionInfo.components.push(component);
            } else {
              // no nutrition available
              component = {
                name: option.NAME
              };
              
              nutritionInfo.components.push(component);
            }
          }
        }
        
        return nutritionInfo;
      },
  
      /**
       * Interpret nutrition data for meal kit items.
       *
       * @param items {Array} {Required} The components of the mealkit that we'll be gathering our nutrition information from.
       * @param itemTypes {Object} {Required} A list of constants to make it easier to track what type of product we're dealing with.
       * @param servingText {String} {Required} The text to show next to the calories.
       *
       * @returns The nutrition information for the given item.
       */
      parseMealKitItem: function (items, itemTypes) {
        var component,
          item,
          i;
        var nutritionInfo = {
          itemType: itemTypes.MEAL_KIT,
          components: []
        };
        
        for (i = 0; i < items.length; i++) {
          item = items[i];
          
          if (item.NUTRITION) {
            component = nutritionHandler.parseNutritionInfo(item.NUTRITION);
            component.calorieServingInfo = nutritionHandler.getCalorieText(component);
          } else {
            component = {};
          }
          
          component.name = item.NAME;
          
          nutritionInfo.components.push(component);
        }
        
        return nutritionInfo;
      },
  
      /**
       * Parse the nutrition information from SPICE's feed.
       *
       * @param nutrition {Object} {Required} The nutrition json string from our API.
       *
       * @returns An object that contains nutrition information.
       */
      parseNutritionInfo: function (nutrition) {
        var nutritionInfo;
      
        try {
          nutritionInfo = JSON.parse(nutrition['@cdata']);
        } catch (e) {
          // Tell the caller specifically what went wrong
          throw new Error('Nutrition data for this item does not have valid JSON data.');
        }
      
        return nutritionInfo;
      },
  
      /**
       * Put together the copy for displaying calories per serving.
       *
       * @param nutrition   {Object} {Required} The relevant nutrition information.
       *
       * @returns {string}
       */
      getCalorieText: function (nutrition) {
        var servingSizeDesc;
        
        if (nutrition.servingSizeDesc) {
          servingSizeDesc = Drupal.t('calories per ' + nutrition.servingSizeDesc);
        } else {
          servingSizeDesc = Drupal.t('calories per serving');
        }
        
        return nutrition.calories + ' ' + servingSizeDesc;
      }
    };
    
    // get the nutrition information for this item
    $scope.nutrition = nutritionHandler.initNutritionInfo($scope.item, $scope.itemTypes);
    
    $scope.option_index = 0;

    if($scope.item.MINIMUMQTY > 1) {
      $scope.quantity = parseInt($scope.item.MINIMUMQTY);
    }else{
      $scope.quantity = 1;
    }


    $scope.category_link = "/shop/"+$scope.item.store.id+"/"+$scope.item.cat.id;
    $scope.cart_link = "/shop/"+$scope.item.store.id+"/cart";

    $scope.updateCartData(json_data.cart);

    $scope.validateQty = function(allow_null){

      if(allow_null){
        if($scope.quantity === null){
          return;
        }
      }

      if($scope.item.MINIMUMQTY > 1){
        if($scope.quantity < $scope.item.MINIMUMQTY){
          alert('This item has a minimum order of ' + $scope.item.MINIMUMQTY);
          $scope.quantity = parseInt($scope.item.MINIMUMQTY);
          return;
        }
      }

      $scope.quantity = Estore.validateQty($scope.quantity);
    }


    /**
     * Updates the price displaying to the customer based on what type of item it is, group master with components,
     * variable weight item or standard item with price modifiers.
     */
    $scope.updateDisplayPrice = function(){

      var priceModified = false;
      var total = $scope.baseprice;
      var options_cost = 0;

      //this minprice is reserved for products which have a ranged price
      //the main price is options_cost
      var minprice = 0;

      for(var product_id in $scope.options){
        if($scope.options[product_id]){

          for(var g in $scope.optiongroups){

              var group = $scope.optiongroups[g];
              var options = group.OPTIONS.OPTION;

              for(var o in options){
                  if(options[o]['@attributes']['ID'] == $scope.options[product_id]){
                      //This is the selected option of the current option group looping.

                      //If group master item, take baseprice from the component
                      //else legacy code
                      if ($scope.item.GROUPMASTERITEM == "True" && options[o]['baseprice']) {

                          total = options[o]['baseprice'];
                          priceModified = true;

                      } else {

                          if (options[o].priceHigher != '0.00') {
                              //Is var wt item with options, not a group or bundle master.
                              options_cost += Number(options[o].priceHigher);
                              priceModified = true;
                          }

                          if(options[o].priceLower != '0.00'){
                              minprice = Number(options[o].priceLower);
                          }

                      }

                  }
              }

          }
        }
      }


      if (priceModified || ($scope.options.length < 1)) {

          if (options_cost > 0) {
              total = options_cost;
          }

          total = Math.round(total * 100)/100;
          total = total.toFixed(2);

          if ($scope.item.VARIABLEWEIGHT == 'True' && minprice > 0) {

              //This is a variable weight item and the option selected had a price range
              minprice = Math.round(minprice * 100) / 100;
              minprice = minprice.toFixed(2)
              $scope.displayprice = $scope.item.store.currency + minprice + " - " + $scope.item.store.currency + total;

          } else if ($scope.item.VARIABLEWEIGHT == 'True' && $scope.options.length < 1) {

              if ($scope.item.WEIGHTLOWER == '0.000') {
                  //This is a var wt item with options at page load, customer has not selected option yet.
                  $scope.displayprice =  $scope.item.store.currency + total;
              } else {
                  //This is a stand alone variable weight item, no options or components...
                  minprice = ($scope.item.BASEPRICE * $scope.item.WEIGHTLOWER);
                  minprice = minprice.toFixed(2);
                  total = ($scope.item.BASEPRICE * $scope.item.WEIGHTHIGHER);
                  total = total.toFixed(2);
                  $scope.displayprice = $scope.item.store.currency + minprice + " - " + $scope.item.store.currency + total;
              }

          } else {

              $scope.displayprice =  $scope.item.store.currency + total;

          }


          if ($scope.displayprice === '$0.00') {
              $scope.displayprice = 'Select Option';
          }

      }


    } //END $scope.updateDisplayPrice



    if(json_data.OPTIONGROUPS && json_data.OPTIONGROUPS.OPTIONGROUP){
      $scope.optiongroups = json_data.OPTIONGROUPS.OPTIONGROUP;
    }else{
      $scope.optiongroups = [];
    }

    for(var i in $scope.optiongroups){

      var template;

      switch($scope.optiongroups[i].TYPE){

        case 'Radiobutton':
          template = "/sites/all/modules/custom/estore/public/partials/option.group.radio.html";
          //initialize first option
          if( $scope.optiongroups[i]['@attributes'] ){
            $scope.options[$scope.optiongroups[i]['@attributes']['ID']] = $scope.optiongroups[i].OPTIONS.OPTION[0]["@attributes"]['ID'];
          }
        break;
        case 'Dropdown':
          template = "/sites/all/modules/custom/estore/public/partials/option.group.select.html";

          //initialize to first option
          if( $scope.optiongroups[i]['@attributes'] ){
          //  $scope.options[$scope.optiongroups[i]['@attributes']['ID']] = $scope.optiongroups[i].OPTIONS.OPTION[0]["@attributes"]['ID'];
          }

        break;
        case 'Checkbox':
          template = "/sites/all/modules/custom/estore/public/partials/option.group.checkbox.html";
        break;
      }

      $scope.optiongroups[i].template = template;
    }

    $scope.$watchCollection('options', function(newValue, oldValue){
      if ( newValue !== oldValue ) {
        $scope.updateDisplayPrice();
      }
    });

    $scope.$watch('quantity', function(newValue, oldValue){
      if ( newValue !== oldValue ) {
        // Only increment the counter if the value changed
        $scope.updateDisplayPrice();
      }
    });

    // if($scope.item.AVAILABILITY){
     var pickup_date = new Date($scope.item.ORDER_START);
     if(pickup_date > new Date()){
       var datestring = (pickup_date.getMonth()+1) + "/" + pickup_date.getDate() + "/"+ pickup_date.getUTCFullYear();
       $scope.pickup_message = "Online ordering for this item begins on "+ datestring;
     }
    // }

    $scope.$on('cart.alert.outofstock', function(){
      //console.log('attempted to buy out of stock item');
    })

    //The estore team has requested:
    // If stand alone var wt item w/o options, ignore 'display price' on product page and instead display price range.
    // If var wt group item, use display price until user selects a component.
    // If var wt with options, use display price until user selects an option.
    if (!$scope.item.DISPLAYPRICE ||
        ($scope.item.VARIABLEWEIGHT == 'True' &&
        $scope.item.GROUPMASTERITEM == 'False' &&
        !$scope.item.OPTIONGROUPS))
    {
        $scope.updateDisplayPrice();
    } else {
        $scope.displayprice = $scope.item.DISPLAYPRICE;
    }

    $scope.decreaseQty = function($event){
      if ($event) {
        if ($event.stopPropagation) $event.stopPropagation();
        if ($event.preventDefault) $event.preventDefault();
      }

      $scope.quantity = Estore.validateQty($scope.quantity - $scope.increment_qty);
      $scope.updateDisplayPrice();
    }

    $scope.increaseQty = function($event){
      if ($event) {
          if ($event.stopPropagation) $event.stopPropagation();
          if ($event.preventDefault) $event.preventDefault();
      }

      $scope.quantity = Estore.validateQty($scope.quantity + $scope.increment_qty);
      $scope.updateDisplayPrice();
    }

    //modal methods
    $scope.next = function($event){
    if ($event) {
        if ($event.stopPropagation) $event.stopPropagation();
        if ($event.preventDefault) $event.preventDefault();
      }

      var optId = $scope.optiongroups[$scope.option_index]['@attributes']['ID'];

      if($scope.optiongroups[$scope.option_index].TYPE === 'Dropdown'){
        if($scope.options[optId]){
          if($scope.optiongroups[$scope.option_index + 1]){
              $scope.option_index++;
          }
        }else{
          alert("Please select an option");
        }
      }else{
        if($scope.optiongroups[$scope.option_index + 1]){
            $scope.option_index++;
        }
      }


    }

    $scope.back = function(){
      if($scope.optiongroups[$scope.option_index - 1]){
          $scope.option_index--;
      }
    }

    $scope.openmodal = function() {
      $scope.modal.open = true;
      $scope.option_index=0;
    }


    $scope.cancel = function(){
      $scope.option_index=0;
      $scope.modal.open=false;
    }

    $scope.cancelFromOverlay = function($event){
      if($event){
        if( jQuery($event.srcElement).hasClass('overlay') ){
          $scope.cancel();
        }
      }
    }

    $scope.appendOption = function(id, $event){

      if ($event) {
        if ($event.stopPropagation) $event.stopPropagation();
        if ($event.preventDefault) $event.preventDefault();
      }

      if($scope.options[id]){
        $scope.options[id] = false;
      }else{
          $scope.options[id] = id;
      }
    }

    $scope.selectOption = function(id, option,$event, index){
      if ($event) {
        if ($event.stopPropagation) $event.stopPropagation();
        if ($event.preventDefault) $event.preventDefault();
      }
      $scope.options[id] = option["@attributes"]['ID'];
      $scope.qisCheck(index, id);
    }


    /**
     * Quantity in stock validation
     *
     * This function is called by the select dropdown onChange event.
     * Therefore it will only run when the OPTIONSGROUPS exists.
     * If not enough quantity in stock this disables the ADD TO CART button
     * and displays a 'SOLD OUT ONLINE' message.
     *
     * @param index Array index of OPTIONGROUP selected from.
     * @param id ID of the optiongroup dropdown whose onChange event fired.
     */
    $scope.qisCheck = function(index, id) {
      //Var 'oid' is the item id of the option chosen. For a group master this is the master component item id.
      var oid = $scope.options[id];
      var opts = $scope.item.OPTIONGROUPS.OPTIONGROUP[index].OPTIONS;
      var button = jQuery('.actions .addToCart');

      angular.forEach(opts, function(value, key) {
        angular.forEach(value, function(v , k) {

          //For the option selected from drop down
          if (oid === v.id) {
              //Now evaluate the inventory tracking type of the option selected.
              if (v.QIS == '' || v.INVENTORYTYPE == 'None') {
                  //If the QIS quantity in stock value is an empty string or the inventory type is set to none,
                  // then there is no inventory tracking to worry about so simply allow the customer to add the
                  // item to their cart.
                  button.removeClass('disabled').text('ADD TO CART').prop('disabled', false);
                  $scope.item.INSTOCK = true;
              } else if ($scope.item.INVENTORYTYPE == 'Options' || v.INVENTORYTYPE == 'Item') {
                  //If the inventory type of the master item is set to 'Options' or inventory
                  // type of the selected option is set to 'Item' then review the QIS.
                  if (v.QIS < 1){
                      button.addClass('disabled').text('SOLD OUT ONLINE').prop('disabled', true);
                      $scope.item.INSTOCK = false;
                  } else {
                      //Now also review the minimum required quantity to purchase for the selected
                      // option.
                      if ($scope.item.MINIMUMQTY != undefined && v.QIS > $scope.item.MINIMUMQTY) {
                          button.removeClass('disabled').text('ADD TO CART').prop('disabled', false);
                          $scope.item.INSTOCK = true;
                          $scope.optionQIS = v.QIS;
                      } else {
                          button.addClass('disabled').text('SOLD OUT ONLINE').prop('disabled', true);
                          $scope.item.INSTOCK = false;
                      }
                  }
              } else {
                  button.removeClass('disabled').text('ADD TO CART').prop('disabled', false);
                  $scope.item.INSTOCK = true;
              }
          }
        });
      });
    }


    /**
     * Add item mobile check
     *
     * This function executes when customers click "SELECT OPTIONS" or "ADD TO CART" on a product page.
     * It distinguishes mobile window width from desktop and prompts with modal options groups to
     * select from if mobile width and options selected are less than options groups presented, meaning
     * they haven't selected options from every options group yet.
     *
     * @param $event Button click event
     */
    $scope.addItem = function($event) {

      if ($event) {
        if ($event.stopPropagation) $event.stopPropagation();
        if ($event.preventDefault) $event.preventDefault();
      }

      if ($scope.adding) {
        return;
      }

      if (jQuery('body').width() > 1024) {
        //Desktop width
        $scope.addToCart();
      } else {
        //Mobile width
        //Every option group requires at least one selection
        if ($scope.optiongroups.length && $scope.options.length < $scope.optiongroups.length) {
          $scope.openmodal();
        } else {
          $scope.addToCart();
        }
      }
    }


    /**
     * Add item to cart
     *
     * Performs item option validation then adds item to cart.
     * Validates:
     * - Delivery method of item against cart delivery method.
     * - Quantity in stock of item based on item type and inventory type.
     * - Required options are selected.
     * - Evaluates the date / time availability windows of this item against the cart.
     * If all validation passes, it then adds the item to the cart.
     *
     * @param $event Button click event
     */
    $scope.addToCart = function($event){

      if ($event) {
        if ($event.stopPropagation) $event.stopPropagation();
        if ($event.preventDefault) $event.preventDefault();
      }

      if ($scope.busy) {
        return;
      }

      if($scope.window.delivery_method!='both' && $scope.item.AVAILABILITY.DELIVERYMETHOD.toLowerCase() != "both" && $scope.item.AVAILABILITY.DELIVERYMETHOD.toLowerCase() != $scope.window.delivery_method){
          if($scope.item.AVAILABILITY.DELIVERYMETHOD.toLowerCase() == "delivery"){
              alert('The items currently in your cart are for in store pickup only, but this item is for delivery only. Please create a separate order.');
              return;
          }
          if($scope.item.AVAILABILITY.DELIVERYMETHOD.toLowerCase() == "pickup"){
              alert('The items currently in your cart are for in delivery only, but this item is for pickup only. Please create a separate order.');
              return;
          }
      }

      //Check the quantity in stock against requested quantity for the item itself.
      var maxQis = 10000; //Initialize a very large integer as an upper boundary of maximum quantity in stock.
      if ($scope.item.GROUPMASTERITEM === 'True') {
          if ($scope.options.length < 1) {
              alert('Please select an option.');
              return;
          }
          //For group master items check QIS on the component selected
          if ($scope.optionQIS && $scope.optionQIS != '') {
              maxQis = $scope.optionQIS;
          }
      } else if ($scope.item.FIXEDBUNDLEMASTERITEM === 'True') {
          //If is fixed bundle master item, loop through all components' quantity in stock values and make sure less
          // than quantity requested.
          if (Array.isArray($scope.item.ITEMCOMPONENTS.ITEMS)) {
              var compArr = $scope.item.ITEMCOMPONENTS.ITEMS;
              var i;
              //The components will only have a QIS value when INVENTORYTYPE = 'Item', so simply check the QIS
              // values if they exist.
              for (i = 0; i < compArr.length; i++) {
                  if (compArr[i].QIS && (maxQis > compArr[i].QIS)) {
                      maxQis = compArr[i].QIS;
                  }
              }
          }
      } else {
          //Standard item
          if ($scope.item.INVENTORYTYPE == 'Item' &&
              $scope.quantity > $scope.item.QIS) {
              maxQis = $scope.item.QIS;
          }
          //Check options inventory
          if (($scope.item.INVENTORYTYPE == 'Options') && ($scope.quantity > $scope.optionQIS) && (maxQis > $scope.optionQIS)) {
              maxQis = $scope.optionQIS;
          }
      }

      //Validate quantity requested against quantity in stock and minimum quantity required.
      if (maxQis < 1) {
          //Item is sold out online.
          $scope.quantity = 1;
          alert('Sorry, but this item is sold out online. Please add different items to your cart.');
          return;
      } else if (maxQis < $scope.item.MINIMUMQTY) {
          //If quantity in stock is less than minimum required quantity, then
          // Msg to customer that we can't add the item to their cart.
          $scope.quantity = 1;
          alert('Sorry, but this item is not available in the minimum quantities required to purchase. Please add different items to your cart.');
          return;
      } else if (maxQis < $scope.quantity) {
          //Requested quantity is greater than quantity in stock, but greater than 0. Reduce quantity to what is
          // in stock, msg to customer and add to cart.
          $scope.quantity = maxQis;
          alert('Sorry, but this item is not available in the quantities you requested. Your order quantity has been updated to ' + maxQis + '.');
      } else if ($scope.quantity < $scope.item.MINIMUMQTY) {
          //Requested quantity is less than the minimum required for purchase. Increate quantity requested to
          // minimum required, msg to customer, add to cart.
          $scope.quantity = $scope.item.MINIMUMQTY;
          alert('Sorry, but this item has a minimum quantity required of ' + $scope.item.MINIMUMQTY + '. Your order quantity has been updated.');
      }

      if (!$scope.item.INSTOCK) {
        $scope.$broadcast('cart.alert.outofstock', function(e, cart){
        });
        return;
      }

      var quant = parseInt($scope.quantity);

      if (!quant || quant < 1 || quant > 1000) {
        $scope.busy = false;
        $scope.openDialog("Sorry, the quantity specified is out of the allowed range.",{title: "Error"});
        if (!quant) {
          $scope.quantity = 1;
        }
        return;
      }

      for(var o in $scope.optiongroups){
        if($scope.optiongroups[o].TYPE === 'Dropdown'){
          //This is the id of the product page or the master item if group
          var dropId = $scope.optiongroups[o].id;
          if(!$scope.options[dropId] || $scope.options[dropId] === false ){
            alert('Please select an option');
            return;
          }
        }
      }

      $scope.cancel();

      if(json_data.AVAILABILITY){

        var earliest_pickup = new Date(json_data.PICKUP_START);
        var latest_pickup = new Date(json_data.PICKUP_END);

        // The existing order has pre-calculated START and END for pickup
        // We call this the PICKUP WINDOW
        // This item also has a pre-calculatd START and END for pickup
        // We call this ITEM AVAILABILITY
        // There are 7 scenarios
        // 1) The item is available for the entire pickup window.
        //    This means nothing will change about the pickup window
        //    and no warnings are necessary
        // 2) The item is available now, but stops being available
        //    at some point during the current pickup window.
        //    This means that the END DATE of the pickup window
        //    will be moved forward to this item's END DATE.
        //    No warning given to user for this.
        // 3) The item is not yet available, but will be available
        //    within the current pickup window.
        //    This means that the START DATE of the pickup window
        //    will be moved back to this item's START DATE.
        //    A warning confirmation is shown to users.
        // 4) The item's start and end are completely contained
        //    within the current pickup window.
        //    This means that both the start and end date of the
        //    pickup window will be modified.
        //    The same check and message for scenario #3 is triggered.
        // 5) The item's earliest date is after the current pickup
        //    window.
        //    This means that, though the item may be available to purchase
        //    it cannot be added to the current cart.
        //    A warning is displayed to the user, and the item is rejected.
        // 6) The items' latest date is before the current pickup window.
        //    This means that, though the item may be available to purchase
        //    it cannot be added to the current cart.
        //    A warning is displayed to the user and the item is rejected.

        // CHECK ONE
        // If this item is only available starting AFTER the latest pickup
        // then it can't be added to this cart.
        if($scope.getLatestPickup()) {
          if(earliest_pickup > new Date($scope.getLatestPickup())){

            alert('This item is not available for pickup until ' + (earliest_pickup.getMonth()+1) + "/" + earliest_pickup.getDate() + ', which is after your current order\'s latest possible pickup date. Please create a separate order to purchase this item.');
            return;
          }
        }

        // CHECK TWO
        // If this item's earliest pickup date is AFTER the order's earliest pickup
        // that means this item will truncate the order window
        // and a warning should be popped.
        if ($scope.getEarliestPickup()) {

          if (
            earliest_pickup > new Date($scope.getEarliestPickup())
            ) {
              // This alert is confusing and unnecessary so we are removing it.
              // Customers don't understand the message and it adds no value 
              // to the process. The cart is still valid, simply with a 
              // smaller pick up date / time window.
              console.log("The item added to the cart will adjust the order pick up time window availability. No need to prompt the customer.");
          }
        }


        // CHECK THREE
        // If this item's latest pickup date is BEFORE the order's earliest pickup,
        // that means this item can't be combined with the other items,
        // and it can't be added to this cart.
        if ($scope.getEarliestPickup()) {
        //  console.log('Check two',latest_pickup, new Date($scope.getEarliestPickup()));

          if (latest_pickup < new Date($scope.getEarliestPickup())) {
            alert('This item only available until ' + (latest_pickup.getMonth()+1) + "/" + latest_pickup.getDate() + ', but other items in your cart are only available after that date.  Please create a separate order to purchase this item.');
            return;
          }
        }
      }


      $rootScope.$broadcast('busy', true);

      $scope.openDialog("Adding to cart ...", {title: 'Adding item to cart...',status: 'waiting'});

      if($scope.quantity >= 1){

          if ($scope.item.GROUPMASTERITEM == "True") {
              var mid = $scope.item.ID;
              var addItemId = $scope.options[mid];
          } else {
              var addItemId = json_data.prod;
          }

        $scope.itemOrder = {
          store: json_data.store.id,
          cat: json_data.cat.id,
          prod: addItemId,
          qty: $scope.quantity,
          itemOption: []
        }

        $scope.itemOrder.qty = $scope.quantity;

        if (mid) {
          $scope.itemOrder.mid = mid;
        }
        var items = [];
        if($scope.item.OPTIONGROUPS && $scope.item.OPTIONGROUPS.OPTIONGROUP) {
            if ($scope.item.GROUPMASTERITEM != "True" && $scope.item.FIXEDBUNDLEMASTERITEM != "True") {
                //For group master items and bundle master items we don't treat the options like options.
                //For items with options use the following legacy logic.
                for (var o = 0; o < $scope.item.OPTIONGROUPS.OPTIONGROUP.length; o++) {
                    var id = $scope.item.OPTIONGROUPS.OPTIONGROUP[o]['@attributes']['ID'];
                    if ($scope.item.OPTIONGROUPS.OPTIONGROUP[o].TYPE == 'Checkbox') {
                        for (var z = 0; z < $scope.item.OPTIONGROUPS.OPTIONGROUP[o].OPTIONS.OPTION.length; z++) {
                            var zid = $scope.item.OPTIONGROUPS.OPTIONGROUP[o].OPTIONS.OPTION[z]['@attributes']['ID'];
                            if ($scope.options[zid] !== false && $scope.options[zid] !== undefined) {
                                items.push($scope.options[zid]);
                            }
                        }
                    } else {
                        items.push($scope.options[id]);
                    }
                }
            }
        }

        $scope.itemOrder.itemOption = items;

        Estore.addToCart($scope.itemOrder);

      }

    }



    $scope.$on('cart.updated', function(e, cart){

      $rootScope.$broadcast('busy', false);

      $scope.openDialog(null, {
          title: '1 item has been added to your cart',
          template: '/sites/all/modules/custom/estore/public/partials/addtocart.added.message.html',
          args: {
              category_url: $scope.category_link,
              cart_url: $scope.cart_link,
              item: $scope.item,
              quantity: $scope.quantity,
              total: $scope.displayprice
          }
      });
    });

}]) //END estore 'product' controller


_estore.controller('sequenceModal', ['$scope', '$http','Estore',

  function($scope, $http,Estore){
  }]);

_estore.controller('receipt',['$scope','Estore',function($scope,Estore) {

  $scope.trackEcommerce = function(order) {


    var commerce_report = {
    'transactionId': order.ID,                     // Transaction ID. Required.
    'transactionAffiliation': 'Estore',   // Affiliation or store name.
    'transactionTotal': order.ORDERTOTAL,               // Grand Total.
    'transactionShipping': order.SHIPPING||0,                  // Shipping.
    'transactionTax': order.ORDERTAX,                     // Tax.
    'transactionProducts': []
    };


    for (var i in order.ORDERITEMS.ORDERITEM) {

      commerce_report.transactionProducts.push({
        'id': order.ID,                     // Transaction ID. Required.
        'name': order.ORDERITEMS.ORDERITEM[i].NAME,    // Product name. Required.
        'sku': order.ORDERITEMS.ORDERITEM[i].UPC,                 // SKU/code.
        'category': order.ORDERITEMS.ORDERITEM[i].CATEGORY['@attributes']['NAME'],         // Category or variation.
        'price': order.ORDERITEMS.ORDERITEM[i].COST,                 // Unit price.
        'quantity': order.ORDERITEMS.ORDERITEM[i].QTY                   // Quantity.
      });
    }

    //console.log('Sending GTM Transaction:',commerce_report);
    dataLayer.push(commerce_report);
    dataLayer.push({'event': 'trackTrans'});
    dataLayer.push({'event': 'orderConfirmation'});
  }



  $scope.trackEcommerce(json_data);


}]);

_estore.controller('review', ['$scope','$rootScope','$http', '$sce','Estore','$timeout',
  function($scope, $rootScope, $http, $sce, Estore,$timeout){

    //console.log('review controller');
    $scope.list = {};
    $scope.items = angular.copy(json_data.items);

    jQuery('.qty').bind('blur',function(e) {
      var $input = jQuery(this);
      $input.val(Estore.validateQty( $input.val() ));
      $scope.changeQty(jQuery(this).attr('id'));
    });

    $scope.purchasePromo = function(cart_item_id, category_id){

        if($scope.busy){
          return;
        }

        $rootScope.$broadcast('busy', true);
        var quantity = 1
        var itemOrder = {
          store: json_data.store,
          cat: category_id,
          prod: cart_item_id,
          qty: 1,
          itemOption: []
          }
        Estore.addToCart(itemOrder);

        $scope.$on('cart.updated', function(e, cart){
          window.location.reload();
        });
    }

    $scope.increaseQty = function(cart_item_id){
      if($scope.busy){
        return;
      }
      $rootScope.$broadcast('busy', true);
      var $el = jQuery('#' + cart_item_id);
      $el.attr('disabled', 'disabled');
      var quantity = $el.val();
      quantity++;
      $scope.checkQty(cart_item_id, quantity);
    }

    $scope.changeQty = function(cart_item_id){
      if($scope.busy){
        return;
      }
      $rootScope.$broadcast('busy', true);
      var $el = jQuery('#' + cart_item_id);
      $el.attr('disabled', 'disabled');
      var quantity = $el.val();
      $scope.checkQty(cart_item_id, quantity);
    }

    $scope.decreaseQty = function(cart_item_id){
      if($scope.busy){
        return;
      }
      $rootScope.$broadcast('busy', true);
      var $el = jQuery('#' + cart_item_id);
      $el.attr('disabled', 'disabled');
      var quantity = $el.val();
      quantity--;
      $scope.checkQty(cart_item_id, quantity);
    }

    /**
     * Check the Quantity of an Item in Cart Against Minimum Required and Quantity in Stock
     *
     * This function is called by user interacting with quantity modifiers
     * on the checkout review page, /cart. It determines if the new quantity
     * is less than available quantity in stock QIS and greater than the
     * minimum required quantity for given items.
     *
     * @param cart_item_id The id integer of the cart item being modified.
     * @param quantity The new quantity being requested.
     */
    $scope.checkQty = function(cart_item_id, quantity){
        var qtyQis, qtyMin;
        var $el = jQuery('#' + cart_item_id);
        $el.attr('disabled', 'disabled');

        //Validate that the quantity is a number between 1 and 1000.
        quantity = Estore.validateQty(quantity);
        quantity = parseInt(quantity, 10);

        //Get the item's minimum required quantity and quantity in stock values.
        for (var x = 0; x < $scope.items.length; x++) {
            if ($scope.items[x].cart_item_id == cart_item_id) {
                qtyMin = parseInt($scope.items[x].minimumqty, 10);
                if ($scope.items[x].bundleQIS) {
                    qtyQis = parseInt($scope.items[x].bundleQIS, 10);
                } else if ($scope.items[x].QIS) {
                    qtyQis = parseInt($scope.items[x].QIS, 10);
                }
            }
        }

        //Validate quantity requested against quantity in stock and minimum quantity required.
        // If QIS was not found and qtyQis undefined, the first two conditions will be false.
        if (qtyQis < qtyMin) {
            //If quantity in stock is less than minimum required quantity, then
            // Msg to customer that we can't sell this item and remove the item from their cart.
            alert('This item is no longer in stock at the minimum quantity required to purchase. It will be removed from your cart');
            $rootScope.$broadcast('busy', false);
            $scope.removeItem(cart_item_id);
            return;
        } else if (qtyQis < quantity) {
            //If quantity in stock is less than requested quantity -
            // Msg the customer about stock levels and decrease the quantity requested to the QIS value.
            alert('Sorry, but this item is not available in the quantities you requested. Your order has been updated to ' + qtyQis + '.');
            quantity = qtyQis;
        } else if (quantity < qtyMin) {
            //If quantity requested is less than the minimum required quantity, msg customer and increase to min req.
            alert('The minimum required quantity for this item is ' + qtyMin + '. Your cart will be updated.');
            quantity = qtyMin;
        }

        $el.val(quantity);

        Estore.updateQuantity(cart_item_id,quantity,function(data) {

            $el.attr('disabled', false);
            $el.val(quantity);

            $scope.disc = data.discount;

            $timeout(function() {
                jQuery('#discount_amount').html(Estore.dollars(data.discount));
                var price = parseFloat(jQuery('#price_' +cart_item_id).text());
                var subtotal = Estore.dollars(price * quantity);
                jQuery('#subtotal_' + cart_item_id).html(subtotal);
                $scope.updateSubtotal();
            });
            $rootScope.$broadcast('busy', false);
        });
    }

    $scope.removeItem = function(cart_item_id){
        if($scope.busy){
          return;
        }
        $rootScope.$broadcast('busy', true);

        Estore.removeFromCart(cart_item_id,function(data) {
          $rootScope.$broadcast('busy', false);

          $scope.disc = data.discount;
          $timeout(function() {
            jQuery('#discount_amount').html(Estore.dollars(data.discount));
            jQuery('#items_list_'+cart_item_id).remove();
            $scope.updateSubtotal();
          });

        });
    }

    $scope.setDiscount = function(discount) {
      $scope.disc = discount;
      $timeout(function() {
        jQuery('#discount_amount').html(Estore.dollars(discount));
        $scope.updateSubtotal();
      });
    }

    $scope.updateSubtotal = function(){
      //console.log('list', $scope.list)
      var subtotal = 0;
      jQuery('.cart_item').each(function() {

          var cart_item_id = jQuery(this).attr('data-id');
          var price = parseFloat(jQuery('#price_' +cart_item_id).text());
          var quantity = parseFloat(jQuery('#'+cart_item_id).val());
          subtotal = subtotal + (price * quantity);
      });

      if (jQuery('#discount_amount').length) {
        var discount = jQuery('#discount_amount').text();
        discount = parseFloat(discount);
        subtotal = subtotal - discount;
      }
      $scope.subtotal =  Estore.dollars(subtotal);
    }


  }]
)


_estore.filter('price',[function() {

    Number.prototype.formatMoney = function(c, d, t){
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
       return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
     };

    return function(price_val) {
      var price;
      price = parseFloat(price_val);
      if (price) {
        return price.formatMoney(2);
      }

      return 0;

    }

}]);



_estore.controller('checkout', ['$scope','$http', '$sce','Estore','$rootScope','$timeout','$q','DataLayer',
  function($scope, $http, $sce, Estore,$rootScope, $timeout,$q, DataLayer){

    $scope.step = 2;
    $scope.newcard = false;

    $scope.country = {
      states: null
    };

    $scope.store_country = json_data.store_info.country;

    // if pickup times are present, initialize inputs
    if (json_data.times) {
      $scope.pickup = {};
      $scope.days = json_data.times;
      $scope.pickup.day = $scope.days[0];
      $scope.times = $scope.pickup.day.time;
      $scope.pickup.time = $scope.times[0];
    }

    $scope.shipping = {
      first_name: null,
      last_name: null,
      country: 1
    }
    $scope.delivery = {};

    $scope.billing = {
      first_name: null,
      last_name: null,
      country: 1
    }

    $scope.isDelivery = json_data.DELIVERY;
    $scope.copyShippingToBilling = false;
    $scope.stored_payments = json_data.stored_payments;
    $scope.storedpayment = {value:null};
    if (json_data.user && json_data.user.email) {
      $scope.email = json_data.user.email;
    }

    $scope.setCountryStates = function(){

      switch( Number($scope.shipping.country) ){
        case 1:
          //US
          $scope.shipping.states = json_data.usstates.STATES.STATE;
        break;
        case 2:
          //Canada
          $scope.shipping.states = json_data.castates.STATES.STATE;
        break;
        case 4:
          //UK
          $scope.shipping.states = json_data.ukstates.STATES.STATE;
        break;
        default:
          $scope.shipping.states = false;
      }

      switch( Number($scope.billing.country) ){
        case 1:
          //US
          $scope.billing.states = json_data.usstates.STATES.STATE;
        break;
        case 2:
          //Canada
          $scope.billing.states = json_data.castates.STATES.STATE;
        break;
        case 4:
          //UK
          $scope.billing.states = json_data.ukstates.STATES.STATE;
        break;
        default:
          $scope.billing.states = false;
      }
    }

    $scope.setBillingAddressToShipping = function(){

        $scope.setCountryStates();

        for(var field in $scope.shipping){
          $scope.billing[field] = $scope.shipping[field];
        }
    }

    $scope.back = function(e) {
      $scope.step = 2;
    }

    $scope.cleanStrings = function(str) {
      return str.replace(/\//gi, '');
    }

    $scope.nrLogMsg = function(msg) {
        if ((typeof NREUM === 'object') && (typeof NREUM.noticeError === 'function')) {
            try {
                throw new Error(msg);
            } catch (e) {
                var resp = NREUM.noticeError(e);
                console.log(e.name + ': ' + e.message + ' - NewRelic response: ' + resp);
            }
        }
    }

      $scope.payframeCallback = function(response) {
          Drupal.settings.WholeFoods = Drupal.settings.WholeFoods || {};
          Drupal.settings.WholeFoods.Litle = Drupal.settings.WholeFoods.Litle || {};
          var wfmLitle = Drupal.settings.WholeFoods.Litle;
          var successful = false;
          var errMsg = '';
          var oncloseLitleErrFunc = function(){
              jQuery('#delivery_method_form').submit();
          }

          if (response.timeout) {
              var elapsedTime = new Date().getTime() - wfmLitle.startTime;
              document.getElementById('timeoutMessage').value = 'Timed out after ' + elapsedTime + 'ms';
              successful = 'recoverable';
              errMsg = 'This order did not process correctly. You may try it again. If you continue to have issues please call the store for assistance.';
          }
          else {
              document.getElementById('response$code').value = response.response;
              document.getElementById('response$responseMessage').value = response.message;
              document.getElementById('response$responseTime').value = response.responseTime;
              document.getElementById('response$reportGroup').value = response.reportGroup;
              document.getElementById('response$merchantTxnId').value = response.id;
              document.getElementById('response$orderId').value = response.orderId;
              document.getElementById('response$litleTxnId').value = response.litleTxnId;
              document.getElementById('response$type').value = response.type;
              document.getElementById('response$lastFour').value = response.lastFour;
              document.getElementById('response$firstSix').value = response.firstSix;
              document.getElementById('response$paypageRegistrationId').value = response.paypageRegistrationId;
              document.getElementById('response$bin').value = response.bin;
              document.getElementById('response$expMonth').value = response.expMonth;
              document.getElementById('response$expYear').value = response.expYear;

              if (response.response === '870') {
                  successful = true;
              }
              else if (
                  response.response === '871' ||
                  response.response === '872' ||
                  response.response === '873' ||
                  response.response === '874' ||
                  response.response === '876'
              ) {
                  successful = 'recoverable';
                  errMsg = 'Please check and re-enter your credit card number and try again.';
              }
              else if (
                  response.response === '881' ||
                  response.response === '882' ||
                  response.response === '883'
              ) {
                  successful = 'recoverable';
                  errMsg = 'Please check and re-enter your card validation number and try again.';
              }
              else if (
                  response.response === '884' ||
                  response.response === '885'
              ) {
                  successful = 'unrecoverable';
              }
              else {
                  successful = 'unrecoverable';
              }
          }

          // Handle response status and messaging
          if (successful == true) {
              var litleErr = 'LitleSuccess:' + response.response
                  + ' Msg:' + response.message
                  + ' OrderId:' + response.orderId
                  + ' LitleTxnId:' + response.litleTxnId;
              $scope.nrLogMsg(litleErr);
              $scope.prepForSubmit($scope.form);
          } else if (successful == 'recoverable') {
              // Prompt customer to re-enter CC data
              var litleErr = 'LitleErrCode:' + response.response
                  + ' Msg:' + response.message
                  + ' OrderId:' + response.orderId
                  + ' LitleTxnId:' + response.litleTxnId;
              $scope.nrLogMsg(litleErr);
              DataLayer.validationErrorEvent('billing', 'missing required field(s)');
              $scope.openDialog(errMsg,
                  {onclose: function(){
                      $scope.focusToCCInput();
                  }
                  });
              $scope.$apply();
          } else {
              // Unrecoverable err. Toggle order to Pick-Up and Pay-In-Store
              var litleErr = 'LitleErrCode:' + response.response
                  + ' Msg:' + response.message
                  + ' OrderId:' + response.orderId
                  + ' LitleTxnId:' + response.litleTxnId;
              $scope.nrLogMsg(litleErr);
              $('#submitButton').attr('disabled','disabled');
              jQuery('#payframe').hide();
              $scope.payOnline = false;
              jQuery('#creditCardDiv').remove();
              jQuery('#paypagefs1').remove();
              jQuery('#paypagefs2').remove();
              window.scrollTo(0,0);
              errMsg = 'It appears we are experiencing technical issues and are unable to accept online payments at this time. You may still place your order to be picked up at the store and you can pay for it then. Always feel free to call you store for assistance in placing or updating your order. Click ok and we will update and reload your checkout form.';
              $scope.openDialog(errMsg,
                  {onclose: oncloseLitleErrFunc,
                   title: 'Error',
                   status: 'Error'
                  });
              $scope.$apply();
          }
      }


      $scope.prepForSubmit = function(form) {
          if(!form.$invalid && !$scope.busy){
              $rootScope.$broadcast('busy', true);
              // submit times, shipping info and method if applicable
              // receieve an updated cart record with tax and shipping and discounts applied
              var order_options = {};
              // Remove illegal characters from billing address for payment processor
              if ($scope.billing) {
                  if ($scope.billing.street1) {
                      $scope.billing.street1 = $scope.cleanStrings($scope.billing.street1);
                  }
                  if ($scope.billing.street2) {
                      $scope.billing.street2 = $scope.cleanStrings($scope.billing.street2);
                  }
                  if ($scope.billing.city) {
                      $scope.billing.city = $scope.cleanStrings($scope.billing.city);
                  }
                  if ($scope.billing.first_name) {
                      $scope.billing.first_name = $scope.cleanStrings($scope.billing.first_name);
                  }
                  if ($scope.billing.last_name) {
                      $scope.billing.last_name = $scope.cleanStrings($scope.billing.last_name);
                  }
              }
              if ($scope.type === "pickup") {
                  order_options = {
                      pickup_day: $scope.pickup.day.date,
                      pickup_time: $scope.pickup.time,
                      pickup_details: null,
                      firstname: $scope.billing.first_name,
                      lastname: $scope.billing.last_name,
                      email: $scope.email,
                      phone1: $scope.phone1,
                      phone1ext: $scope.phone1ext,
                      type: $scope.type
                  }
              }
              // need to add shipping info if applicable!
              if ($scope.isDelivery) {
                  order_options = {
                      firstname: $scope.shipping.first_name,
                      lastname: $scope.shipping.last_name,
                      delivery_details: null,
                      street1: $scope.shipping.street1,
                      street2: $scope.shipping.street2,
                      city: $scope.shipping.city,
                      state: $scope.shipping.state,
                      zip: $scope.shipping.zip,
                      country: $scope.shipping.country,
                      phone1: $scope.phone1,
                      phone1ext: $scope.phone1ext,
                      email: $scope.email,
                      shipping_method: $scope.shipping_method,
                      type: $scope.type
                  }
                  if($scope.pickup){
                      if($scope.pickup.day){
                          order_options.pickup_day = $scope.pickup.day.date;
                          order_options.pickup_time = $scope.pickup.time;
                      }
                  }
              }
              Estore.prepareOrder(order_options,function(final_order) {
                  $rootScope.$broadcast('busy', false);
                  jQuery('#reviewOrder').removeClass('disabled').val('REVIEW ORDER');
                  jQuery('#reviewOrder').prop('disabled', false);
                  if($scope.store_country == 'UK'){
                      $scope.final_order = final_order;
                      $scope.final_order.subtotal = final_order.subtotal - final_order.tax;
                      $scope.final_order.total_cost = final_order.total_cost - final_order.tax;
                  }else{
                      $scope.final_order = final_order;
                  }
                  $scope.step = 3;
                  window.scrollTo(0,0);
              });
          } else {
              var message = "Uh oh, looks like we're missing some information.";
              DataLayer.validationErrorEvent('billing', 'missing required field(s)');
              $scope.openDialog(message,
                  {onclose: function(){
                      $scope.focusToInvalidInput();
                  }
                  });
          }
      }


      $scope.validateCheckout = function(form){
          form.attempted_submit = true;
          if(!form.$invalid && !$scope.busy){
              return true;
          } else {
              var message = "Uh oh, looks like we're missing some information.";
              DataLayer.validationErrorEvent('billing', 'missing required field(s)');
              $scope.openDialog(message,
                  {onclose: function(){
                      $scope.focusToInvalidInput();
                  }
                  });
              return false;
          }
      }


      $scope.reviewOrder = function(form) {
          // According to Object.is() 'form' and '$scope.form' are equal.

          if(!$scope.busy){
              jQuery('#reviewOrder').addClass('disabled').val('PROCESSING...');
              jQuery('#reviewOrder').prop('disabled', true);

              Drupal.settings.WholeFoods = Drupal.settings.WholeFoods || {};
              Drupal.settings.WholeFoods.Litle = Drupal.settings.WholeFoods.Litle || {};
              var wfmLitle = Drupal.settings.WholeFoods.Litle;

              // First validate non-credit card form elements
              if ($scope.validateCheckout(form) == false) {
                  // Form is dirty, modal error will launch, do not continue with reviewOrder.
                  return;
              }

              if ((typeof payframeClient == 'object') && (jQuery("#vantiv-payframe").length > 0)) {
                  wfmLitle.startTime = new Date().getTime();
                  payframeClient.getPaypageRegistrationId(wfmLitle.paypageRegIdConfigure);
                  // At this point control has been passed to Litle and we wait
                  // for them to call our paypageCallback() function.
                  // We handle the remaining logic there.
                  return;
              } else {
                  // Payframe is not in use, proceed to the next step
                  $scope.prepForSubmit(form);
              }
          }
      }


    $scope.$watch('step', function(newValue, oldValue) {
      // Since this gets run for each $digest, we need to confirm that the
      // new value is 3 (order-review step) and that the old and new value are
      // not the same so we do not push dataLayer more than once.
      if (newValue === 3 && newValue !== oldValue) {
        DataLayer.orderReviewEvent();
      }
    }, true);

    $scope.submitOrder = function(e) {
      if(!$scope.busy){
        $rootScope.$broadcast('busy', true);
        jQuery('[checkout-form]').submit();
      }

    }

   $scope.update = function(){
     $scope.times = $scope.pickup.day.time;
   }

  $scope.setCountryStates();

  jQuery(document).ready(function() {

      if (!json_data.BOTH && !json_data.litleErr && !json_data.INSTORE) {

          Drupal.settings.WholeFoods.Litle = Drupal.settings.WholeFoods.Litle || {};
          var wfmLitle = Drupal.settings.WholeFoods.Litle;

          wfmLitle.startTime;

          wfmLitle.paypageRegIdConfigure = {
              "id"  : json_data.id,
              "orderId" : json_data.id
          };

          wfmLitle.litleConfigure = {
              //"paypageId" : document.getElementById("request$paypageId").value,
              "paypageId" : json_data.payPageId,
              "style" : "WFMdefaultStyle4", // StyleSheet file name loaded at Litle
              "height"  : "350px",
              "reportGroup" : "*merchant1500",
              //"reportGroup" : document.getElementById("request$reportGroup").value,
              "timeout" : "30000",
              //"timeout" : document.getElementById("request$timeout").value,
              "div" : "payframe",
              "callback"  : $scope.payframeCallback,
              "showCvv" : true,
              "months"  : {
                  "1":"January",
                  "2":"February",
                  "3":"March",
                  "4":"April",
                  "5":"May",
                  "6":"June",
                  "7":"July",
                  "8":"August",
                  "9":"September",
                  "10":"October",
                  "11":"November",
                  "12":"December"
              },
              "numYears"  : 8,
              "tooltipText" : "A CVV is the 3 digit code on the back of your Visa, MasterCard and Discover or a 4 digit code on the front of your American Express",
              "tabIndex" : {
                  "cvv" : 4,
                  "accountNumber" : 1,
                  "expMonth"  : 2,
                  "expYear" : 3
              },
              "placeholderText" : {
                  "cvv" : "CVV",
                  "accountNumber" : "Account Number"
              }
          };

          // If Litle widget didn't load, convert to Pay In Store order.
          // Else instantiate the widget.
          if (typeof LitlePayframeClient === 'undefined') {
              $scope.payOnline = false;
              jQuery('#creditCardDiv').remove();
              jQuery('#paypagefs1').remove();
              jQuery('#paypagefs2').remove();
              window.scrollTo(0,0);

              var message = 'We are unable to process a delivery order at this time. Your order will be converted to a pick-up and pay-in-store order. Click ok and we will reload your checkout form.';
              $scope.openDialog(message,
                  {onclose: function(){
                      // When they accept the message post the delivery method to pick up form
                      jQuery('#delivery_method_form').submit();
                  }
                  });

          }
          else {
              $scope.payOnline = true;
              payframeClient = new LitlePayframeClient(wfmLitle.litleConfigure);
          }
      }
  });

  }]
);

_estore.directive('phonefield', function() {

    return {
        restrict: 'C',
        require: 'ngModel',
        link: function($scope, element, attrs, ctrl) {

            jQuery(element).bind('blur', function() {


                var valid = false;
                var value = this.value;

                // if this store is in the UK, strip all non-digit characters
                // and then change the validator to look for a 10 or 11 digit number
                // (UK phone numbers can be 10 or 11 digits)
                if (json_data.store_info.country=='UK') {
                  value = value.replace(/[^0-9]/g,'');
                  if (value.length==10 || value.length==11) {
                    valid = true;
                  }
                } else {
                  // validate a us number
                  var PHONE_REGEXP = /^[(]{0,1}[0-9]{3}[)\.\-]{0,1}[0-9]{3}[\.\-]{0,1}[0-9]{4}$/;
                  value = value.replace(/ /g,'');
                  if(PHONE_REGEXP.test(value)) {
                    valid = true;
                  }
                }

                if (valid) {
                  ctrl.$setValidity("phonepattern", true);
                  if(value != this.value){
                      this.value= value;
                  }
                } else {
                  ctrl.$setValidity("phonepattern", false);
                }


            });
        }
    }
});

_estore.directive('zipcode', [function(){
  return {
    restrict: 'C',

    link: function($scope, element, attrs, form){

      $scope.$watchCollection(attrs.watchfield, function(n, o){

        var field = $scope.shipping.country;
        if(attrs.watchfield == 'billing.country'){
            field = $scope.billing.country;
        }

        switch( Number(field) ){
          case 1:
            //US
            attrs.$set("placeholder", "99999");
            element.mask("99999");
          break;
          case 2:
            //Canada
            attrs.$set("placeholder", "a9a9a9");
            element.mask("a9a9a9");
          break;
          /*
          case 4:
            //UK
            form.postal_pattern = false;
            attrs.$set("placeholder", '');
            element.unbind('.mask');
            //UK postal codes are insane. just make it text
          break;
          */
          default:
            element.unmask();
            attrs.$set("placeholder", '');
        }

      });

    }
  }
}]
);

_estore.directive('leavestore', [function() {
    return {
        restrict: 'A',
        link: function($scope, element, attrs) {

          element.on('click', function(e){

            e.preventDefault();

              if($scope.cartcount){
                var leavepage = confirm("Switching stores will start a new shopping session. The items currently in your cart will be removed.");
                  if(leavepage){
                    window.location = attrs.href;
                  }
              }else{
                  window.location = attrs.href;
              }
          });
        }
    };
}]);

_estore.directive('checkout', ['$timeout', function($timeout) {
    return {
        restrict: 'C',
        require: '^?form',
        link: function($scope, element, attrs, form) {

          $scope.focusToInvalidInput = function(){
            $timeout(function(){
                jQuery('#reviewOrder').removeClass('disabled').val('REVIEW ORDER');
                jQuery('#reviewOrder').prop('disabled', false);
                var $invalid = jQuery(element).find('.ng-invalid').eq(0);
                $invalid.focus();
            },0,false);
          }

          $scope.focusToCCInput = function(){
              $timeout(function(){
                  jQuery('#reviewOrder').removeClass('disabled').val('REVIEW ORDER');
                  jQuery('#reviewOrder').prop('disabled', false);
                  jQuery('html, body').animate({
                      scrollTop: jQuery("#payframe").offset().top
                  }, 1000);
              },0,false);
          }

        }
    };
}]);


/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2006, 2014 Klaus Hartl
 * Released under the MIT license
 */
(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // CommonJS
        factory(require('jquery'));
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {

    var pluses = /\+/g;

    function encode(s) {
        return config.raw ? s : encodeURIComponent(s);
    }

    function decode(s) {
        return config.raw ? s : decodeURIComponent(s);
    }

    function stringifyCookieValue(value) {
        return encode(config.json ? JSON.stringify(value) : String(value));
    }

    function parseCookieValue(s) {
        if (s.indexOf('"') === 0) {
            // This is a quoted cookie as according to RFC2068, unescape...
            s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
        }

        try {
            // Replace server-side written pluses with spaces.
            // If we can't decode the cookie, ignore it, it's unusable.
            // If we can't parse the cookie, ignore it, it's unusable.
            s = decodeURIComponent(s.replace(pluses, ' '));
            return config.json ? JSON.parse(s) : s;
        } catch(e) {}
    }

    function read(s, converter) {
        var value = config.raw ? s : parseCookieValue(s);
        return $.isFunction(converter) ? converter(value) : value;
    }

    var config = $.cookie = function (key, value, options) {

        // Write

        if (arguments.length > 1 && !$.isFunction(value)) {
            options = $.extend({}, config.defaults, options);

            if (typeof options.expires === 'number') {
                var days = options.expires, t = options.expires = new Date();
                t.setTime(+t + days * 864e+5);
            }

            return (document.cookie = [
                encode(key), '=', stringifyCookieValue(value),
                options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                options.path    ? '; path=' + options.path : '',
                options.domain  ? '; domain=' + options.domain : '',
                options.secure  ? '; secure' : ''
            ].join(''));
        }

        // Read

        var result = key ? undefined : {};

        // To prevent the for loop in the first place assign an empty array
        // in case there are no cookies at all. Also prevents odd result when
        // calling $.cookie().
        var cookies = document.cookie ? document.cookie.split('; ') : [];

        for (var i = 0, l = cookies.length; i < l; i++) {
            var parts = cookies[i].split('=');
            var name = decode(parts.shift());
            var cookie = parts.join('=');

            if (key && key === name) {
                // If second argument (value) is a function it's a converter...
                result = read(cookie, value);
                break;
            }

            // Prevent storing a cookie that we couldn't decode.
            if (!key && (cookie = read(cookie)) !== undefined) {
                result[name] = cookie;
            }
        }

        return result;
    };

    config.defaults = {};

    $.removeCookie = function (key, options) {
        if ($.cookie(key) === undefined) {
            return false;
        }

        // Must not alter options, thus extending a fresh object...
        $.cookie(key, '', $.extend({}, options, { expires: -1 }));
        return !$.cookie(key);
    };

}));


})(jQuery);
