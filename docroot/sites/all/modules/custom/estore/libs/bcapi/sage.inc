<?php

/**
 * Class SAGE
 *
 * For interacting with the SAGE WFM API. This class controls instantiating an
 * eStore Cache object, retrieving config settings from the Drupal variables
 * table, requesting an OAuth token from the API, calling the API and encoding /
 * decoding values between XML and JSON.
 *
 * This is where to debug eStore calls to the Spinutech API. Raw values for
 * requests and responses will output to the PHP error log if you set
 * $eStore_debug = true.
 */
class SAGE {

  /**
   * Variable to hold API configuration details.
   *
   * @var
   */
  public $SAGE_CONFIG;

  /**
   * Debug boolean for outputting Spinternet API call request and responses
   * to the PHP error log.
   *
   * @var bool
   */
  private $eStore_debug = false;

  /**
   * ESTORE_CACHE class interface.
   *
   * @var object
   */
  protected $estore_cache;

  /**
   * Class object constructor. Instantiates estore cache object and gets
   * an OAuth token from the SAGE API.
   */
  public function __construct() {
    $this->estore_cache = new ESTORE_CACHE();
    $this->getToken();
  }

  /**
   * @param $key Key string name for the variable value being selected.
   * @return mixed Mostly like a string value, the variable selected.
   */
  public function getParams($key){
    $this->SAGE_CONFIG = variable_get('sage_config');
    return $this->SAGE_CONFIG[$key];
  }

  /**
   * Calls SAGE API
   *
   * This function calls the SAGE API as defined by input parameters.
   *
   * @param $url The API end point url.
   * @param $params Array containing OAuth access token or client id / secret.
   * @param string $type 'GET' or 'POST'.
   * @return mixed
   */
  public function getAPI($url, $params, $type = 'GET') {

    if ($this->eStore_debug) {
      error_log('SAGE / Spinutech API Request URL: ' . $url);
      if (!empty($params)) {
        if (gettype($params) == 'array') {
          error_log('SAGE / SpinAPI Request Params: ' . json_encode($params));
        } else {
          error_log('SAGE / SpinAPI Request Params: ' . $params);
        }
      }
    }

    if ($type == 'GET') {
      // if we have a token at all, use it.
      // don't try to generate one here, because we may actually in the process
      // of trying to generate one...
      if ($token = $this->estore_cache->get('sage_token')) {
        $params['access_token'] = $token;
      } else {
        $params['client_id'] = $this->getParams('client_id');
        $params['client_secret'] = $this->getParams('client_secret');
      }
      $post_data ='';
      foreach ($params as $field=>$val) {
        $post_data = $post_data . urlencode($field) . "=" . urlencode($val) . "&";
      }
      $url = $url . '?' . $post_data;
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    }
    if($type == 'POST'){
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_POST, TRUE);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
    }
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    $result = curl_exec($ch);
    curl_close($ch);

    if ($this->eStore_debug) {
      if (!empty($result)) {
        if (gettype($result) == 'array') {
          error_log('SAGE/SpinAPI Response: ' . json_encode($result));
        } else {
          error_log('SAGE/SpinAPI Response: ' . $result);
        }
      } else {
        error_log('No response from SAGE/SpinAPI.');
      }
    }

    return $result;
  }

  /**
   * Retrieves SAGE API OAuth access token. Either from cache or requests a
   * fresh token if expired.
   *
   * @param bool $force Set to TRUE to ignore the cache.
   * @return bool
   */
  public function getToken($force = FALSE){
    if(!$this->estore_cache->get('sage_token') || ($force === TRUE)){
      $url = $this->getParams('sage_get_token');
      $options = array('grant_type' => 'client_credentials');
      $xml = $this->getApi($url, $options);
      $json = $this->xmltojson($xml);
      $result = $json->access_token->token;
      $this->estore_cache->set('sage_token', $result, 900);
    }else{
      $result = $this->estore_cache->get('sage_token');
    }
    return $result;
  }

  /**
   * Converts xml to json format.
   *
   * @param $xml
   * @return mixed
   */
  public function xmltojson($xml) {
    $fileContents = str_replace(array("\n", "\r", "\t"), '', $xml);
    $fileContents = trim(str_replace('"', "'", $fileContents));
    $simpleXml = json_decode(json_encode(simplexml_load_string($fileContents)));
    return $simpleXml;
  }

}