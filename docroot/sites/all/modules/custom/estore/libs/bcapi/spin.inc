<?php

class SPIN {

  /**
   * SAGE class interface.
   * @var object
   */
  protected $sage;

  /**
   * Class SPIN instatiation function.
   */
  public function __construct()
  {
    $this->sage = new SAGE();
  }


  public function spinAPI($endpoint, $data, $is_retry=false) {
    $token = $this->sage->getToken();
    $url = $this->sage->getParams('spin') . $token;

    $xml_data = array(
        '@attributes'=>array(
          'CLIENTID'=>9999,
          'METHOD'=>$endpoint,
          'VERSION'=>'1',
        )
      );

    foreach ($data as $key => $val)  {
      $xml_data[$key] = $val;
    }

    $post_data = Array2XML::createXML('SVC',$xml_data);
    $post_data = $post_data->saveXML();
    $xml = $this->sage->getAPI($url, $post_data,'POST');

    if ($xml === false) {
      // The Spinternet API call timed out. Display nice error to customer.
      $res = array(
        'status' => 'error',
        'message' => $array_data['SVC']['ERROR'],
        'endpoint' => $endpoint,
        'data' => $xml_data,
        );
      return $res;
    }

    // this is a hack to fix spinutech's response that specifies utf-16 but is actually in utf-8
    $xml = preg_replace('/utf\-16/','utf-8',$xml);

    try {
      $array_data = XML2Array::createArray($xml);
    } catch (Exception $e) {
      try {
        $json = json_decode($xml);
      } catch (Exception $e2) {
        return array('status'=>'error','message'=>'API returned non-XML response','endpoint'=>$endpoint,'data'=>$xml_data);
      }

      if (@$json->fault && $json->fault->faultstring) {
        if (!$is_retry && $json->fault->detail->errorcode=='keymanagement.service.access_token_expired') {
          return $this->spinAPI($endpoint,$data,true);
        }
        if (!$is_retry && $json->fault->detail->errorcode=='keymanagement.service.invalid_access_token') {
          return $this->spinAPI($endpoint,$data,true);
        }
        if (!$is_retry && $json->fault->detail->errorcode=='oauth.v2.InvalidAccessToken') {
          return $this->spinAPI($endpoint,$data,true);
        }
        return array('status'=>'error','message'=>$json->fault->faultstring,'endpoint'=>$endpoint,'data'=>$xml_data);
      } else {
        return array('status'=>'error','parseerror'=>true,'message'=>'Received an unexpected response: ' . $xml,'endpoint'=>$endpoint,'data'=>$xml_data);
      }
    }

    $status = $array_data['SVC']['@attributes']['RESULT'];
    if ($status=='SUCCESS') {
      $res = $array_data['SVC'];
    } else {
      if (isset($array_data['SVC']['ERROR'])) {
        $res =  array('status'=>'error','message'=>$array_data['SVC']['ERROR'],'endpoint'=>$endpoint,'data'=>$xml_data);
      } else {
        $res =  array('status'=>'error','message'=>$array_data['SVC']['@value'],'endpoint'=>$endpoint,'data'=>$xml_data);
      }
      if (isset($array_data['SVC']['DISCOUNTDETAILS'])) {
        $res['DISCOUNTDETAILS'] = $array_data['SVC']['DISCOUNTDETAILS'];
      }
    }
    return $res;
  }

  public function getKey() {

    $public_key = '28f694d4-202d-4050-adf8-8ce4ff89954b';
    $private_key = 'ac32f3fc-9d46-4b3a-95bc-20ea41f109da';

    date_default_timezone_set('GMT');

    $d = date("D, d M Y G:i:s T");
    $to_hash = $d.$public_key;
    $signed_key = base64_encode(hash_hmac('md5', $to_hash, $private_key, true));
    $key = implode('|', array($public_key, $d, $signed_key));

    return $key;

  }

}