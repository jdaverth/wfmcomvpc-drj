<?php

class WFM {

  /**
   * SAGE class interface.
   * @var object
   */
  protected $sage;

  /**
   * ESTORE_CACHE class interface.
   * @var object
   */
  protected $estore_cache;

  /**
   * Class WFM instatiation function.
   */
  public function __construct() {
    global $conf;
    $this->sage = new SAGE($conf['site_env']);
    $this->estore_cache = new ESTORE_CACHE();
  }


  public function getJanrainID($uid) {
    $authmaps = db_query("SELECT module, authname FROM {authmap} WHERE uid = :uid", array(':uid' =>$uid))->fetchAllKeyed();
    if (!empty($authmaps['janrain_capture'])) {
      return $authmaps['janrain_capture'];
    }
    return null;
  }

  public function getStoreImage($store_name) {
    if (function_exists('store_get_store_from_name')) {
      $store = store_get_store_from_name($store_name);
      if ($store && $store->field_storefront_image && $store->field_storefront_image['und'] && $store->field_storefront_image['und'][0]) {
        $image = image_style_url('large',$store->field_storefront_image['und'][0]['uri']);
      } else {
        $image = '/sites/all/modules/custom/estore/public/img/Store-Generic.png';
      }
    } else {
      $image = '/sites/all/modules/custom/estore/public/img/Store-Generic.png';
    }
    return $image;
  }

  public function getStoreTZ($state,$country) {

    if ($country=='UK') {
      return "GMT";
    } else if ($country=='CAN') {

      $tz_states = array(
        'America/Los_Angeles'=>array('British Columbia'),
        'America/New_York'=>array('Ontario'),
      );
      foreach ($tz_states as $timezone=>$states) {
        foreach ($states as $state_code) {
          if (strtolower($state)==strtolower($state_code)) {
            return $timezone;
          }
        }
      }

    } else if ($country=='USA') {

      $tz_states = array (
        'America/Anchorage'=>array('Alaska'),
        'America/Boise'=>array('Idaho'),
        'America/Chicago'=>array('Alabama',
          'Arkansas',
          'Illinois',
          'Iowa',
          'Kansas',
          'Louisiana',
          'Minnesota',
          'Mississippi',
          'Missouri',
          'Nebraska',
          'Oklahoma',
          'South Dakota',
          'Tennessee',
          'Texas',
          'Wisconsin'),
        'America/Denver'=>array('Colorado',
          'Montana',
          'New Mexico',
          'Utah',
          'Wyoming'),
        'America/Detroit'=>array('Michigan'),
        'America/Indiana/Indianapolis'=>array('Indiana'),
        'America/Kentucky/Louisville'=>array('Kentucky'),
        'America/Los_Angeles'=>array('California',
          'Nevada',
          'Oregon',
          'Washington'),
        'America/New_York'=>array('Connecticut',
          'Delaware',
          'Florida',
          'Georgia',
          'Maine',
          'Maryland',
          'Massachusetts',
          'New Hampshire',
          'New Jersey',
          'New York',
          'North Carolina',
          'Ohio',
          'Pennsylvania',
          'Rhode Island',
          'South Carolina',
          'Vermont',
          'Virginia',
          'District of Columbia',
          'West Virginia'),
        'America/North_Dakota/Center'=>array('North Dakota'),
        'America/Phoenix'=>array('Arizona'),
        'Pacific/Honolulu'=>array('Hawaii'),
      );

      foreach ($tz_states as $timezone=>$states) {
        foreach ($states as $state_code) {
          if (strtolower($state)==strtolower($state_code)) {
            return $timezone;
          }
        }
      }
    } else {
      return "America/Chicago";
    }
  }

  public function getStoreInfo($id) {

    global $conf;

    // filter $id for length, etc.
    $id = filter_maxlength(filter_var($id,FILTER_SANITIZE_STRING),3);

    if (!$res = $this->estore_cache->get('store_' . $id)) {
      $sage_config = variable_get('sage_config');
      $url = $sage_config['stores'] . $id;
      $json = $this->sage->getAPI($url,array('admin'=>'dGhpc2lzYXNlY3JldGFkbWludG9rZW4='));
      $res = json_decode($json);

      if (property_exists($res,'status_code') && ($res->status_code == 'ITEM_NOT_FOUND')) {
        $res = array(
          'id'=>$id,
          'name'=>'TEST STORE ' . $id,
          'address'=>'550 Bowie Street',
          'city'=>'Austin',
          'state'=>'Texas',
          'zip_code'=>'78704',
          'hours'=>'24/7 365!',
          'facebook'=>'wholefoods',
          'twitter'=>'wholefoods',
          'phone'=>'5125551234',
          'country'=>'USA',
          'can_order'=>true
        );
        $res = (object) $res;
      } else {
        $res->id = $res->tlc;
      }

      $res->image = $this->getStoreImage($res->name);

      $res->currency = '$';
      if ($res->country=='UK') {
        $res->currency = '£';
        $res->isUK = true;
      }
      if ($res->country=='CAN') {
        $res->currency = 'CDN$';
      }

      if ($res->country=='USA') {
        $res->isUSA = true;
      }

      if ($res->id=='ACO') {
        $res->isAllegro = true;
      }

      $res->timezone = $this->getStoreTZ($res->state,$res->country);

      $this->estore_cache->set('store_'.$id,$res);
    }
    return $res;

  }

  public function getTMDiscount($id,$is_retry=false) {

    $url = $this->sage->getParams('discount') . $id;

    if ($this->sage->getParams('always_error')) {
      return array('status'=>'error','message'=>'API is in always_error mode! This is a test error message.');
    }

    $json = json_decode($this->sage->getAPI($url));

    if ($json->fault && $json->fault->faultstring) {
      if (!$is_retry && $json->fault->detail->errorcode=='keymanagement.service.access_token_expired') {
        $this->debug("RETRYING DUE TO EXPIRED KEY!");
        return $this->getTMDiscount($id,true);
      }
      if (!$is_retry && $json->fault->detail->errorcode=='keymanagement.service.invalid_access_token') {
        $this->debug("RETRYING DUE TO INVALID KEY!");
        return $this->getTMDiscount($id,true);
      }
      if (!$is_retry && $json->fault->detail->errorcode=='oauth.v2.InvalidAccessToken') {
        $this->debug("RETRYING DUE TO INVALID OAUTH KEY KEY!");
        return $this->getTMDiscount($id,true);
      }

    }

    return $json;
  }
}