Description
-----------
This module adds the ability to set a weight for each content type.  Afterwards,
if creating a view of multiple types, you need to sort/order those types in a weighted
fashion, the node type weights allow you to do that.

Requirements
------------
Drupal 7.x

Installation
------------
1. Copy the entire type_weight directory the Drupal sites/all/modules directory.

2. Login as an administrator. Enable the module in the "Administer" -> "Modules"

4. Create or edit a Content Type and within each type a Content type weight tab should exist in the additional settings area.

Support
-------
Please use the issue queue for filing bugs with this module at
http://drupal.org/project/issues/type_weight

