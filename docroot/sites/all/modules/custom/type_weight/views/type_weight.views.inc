<?php

/**
 * @file
 * Type_weight views integration.
 */

/**
 * Implements hook_views_data().
 */
function type_weight_views_data() {
  // This joins the node_type table with the node table so the node_type.weight
  // field can be used.
  $data['node_type']['table'] = array(
    'group' => t('Content'),
    'join' => array(
      'node' => array(
        'left_field' => 'type',
        'field' => 'type',
      ),
    ),
  );
  // Defines the handlers for the different types of operations within the view.
  $data['node_type']['weight'] = array(
    'title' => t('Type Weight'),
    'help' => t('The weight of the node type, formatted as an integer.'),
    'field' => array(
      'handler' => 'type_weight_handler_field_weight',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
  // Defines how to handle the weight_value specifically -
  // still maps to the weight column in the node_type table.
  $data['node_type']['weight_value'] = array(
    'title' => t('Type Weight Value'),
    'help' => t('The weight of the node type.'),
    'real field' => 'weight',
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
  );
  return $data;
}

/**
 * Implements of hook_views_handlers().
 */
function type_weight_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'type_weight') . '/views',
    ),
    'handlers' => array(
      'type_weight_handler_field_weight' => array(
        'parent' => 'views_handler_field',
      ),
      'type_weight_handler_filter_weight_enabled' => array(
        'parent' => 'views_handler_filter',
      ),
    ),
  );
}
