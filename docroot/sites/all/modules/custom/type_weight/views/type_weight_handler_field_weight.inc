<?php

/**
 * @file
 * type_weight_handler_field_weight extending views_handler_field
 */

class type_weight_handler_field_weight extends views_handler_field {

  /**
   * Return options id and row_index in proper way.
   */
  public function render($values) {
    return '<!--form-item-' . $this->options['id'] . '--' . $this->view->row_index . '-->';
  }

  /**
   * Initialize $nids array to store nids for saving weights.
   */
  public function views_form(&$form, &$form_state) {
    // The view is empty, abort.
    if (empty($this->view->result)) {
      return;
    }
    // Initialize $nids array to store nids for saving weights.
    $field_name = $this->options['id'];
    $form[$field_name] = array(
      '#tree' => TRUE,
    );

    // At this point, the query has already been run, so we can access the results.
    foreach ($this->view->result as $row_id => $row) {
      $type = $this->get_node_type($row->nid);
      if (!empty($row->node_type_weight)) {
        $weight = $row->node_type_weight;
      }
      else {
        $weight = _get_node_type_weight($type);
      }
      $form[$field_name][$row_id][$row->nid] = array(
        '#value' => $weight,
        '#attributes' => array('class' => array('type-weight')),
      );
    }
  }

  /**
   * Return node type from nid.
   * 
   * @param int $nid
   *   nid of the node.
   *  
   * @return string
   *   Type of the node.
   */
  public function get_node_type($nid) {
    $type = db_query("SELECT type FROM {node} WHERE nid=:nid",
      array(':nid' => $nid))->fetchField();
    return $type;
  }
}
