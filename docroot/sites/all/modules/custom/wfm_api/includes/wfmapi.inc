<?php

/**
 * @file
 * Contains WFMApi.
 */

/**
 * The WFMApi class is the base class of all the other Whole Foods Market
 * API classes.
 */

// include_once(drupal_get_path('module', 'wfm_api' . '/includes/wfmapicache.inc'));

use Wfm\Api\SageClient\Category;

class WFMApi {

  /**
   * @var string SAGE API key.
   */
  protected $api_key;

  /**
   * @var string SAGE API secret.
   */
  protected $api_secret;

  /**
   * @var string SAGE API base url.
   */
  protected $api_url;

  /**
   * @var int SAGE API version.
   */
  protected $api_version;

  /**
   * @var string caching method for authentication token.
   */
  protected $caching_method;

  /**
   * @var array caching options for authentication token.
   */
  protected $caching_options;

  /**
   * @var string janrain access token. Provided by Janrain Capture module.
   */
  protected $janrain_access_token;

  /**
   * @var object implementation of Wfm\Api\SageClient\Category
   */
  protected $categoryApi;

  /**
   * @var string cache key for category list
   */
  protected $cache_category_list;

  /**
   * Constructor.
   */
  public function __construct() {
    $this->api_key = WFMVariables::get('wfmapi_key');
    $this->api_secret = WFMVariables::get('wfmapi_secret');
    $this->api_url = 'https://' . WFMVariables::get('wfmapi_host');
    $this->api_version = WFMVariables::get('wfmapi_version');
    $profile = $this->refreshJanrainAccessToken();
    if ($profile['stat'] == 'ok' && isset($_SESSION['janrain_capture_access_token'])) {
      $this->janrain_access_token = $_SESSION['janrain_capture_access_token'];
    }
    $this->cache_category_list = 'getCategories';

    $cache = new WfmApiDrupalCache();
    $this->categoryApi = new Category(
      $this->api_key,
      $this->api_secret,
      $this->api_url,
      $this->api_version,
      $cache
    );
  }

  /**
   * Get a Janrain User ID from the global $user.
   *
   * If not found look in the Drupal users table via the Drupal User ID.
   * Else call the Janrain API. Otherwise throw an exception.
   *
   * @param string $uid
   *   A Drupal User ID for a User.
   *
   * @retval string
   *   A Janrain User Id for the Drupal User.
   *
   * @throws WFMApiException_NoUser
   *   If the UID is invalid.
   * @throws WFMApiException_NoJUID
   *   If the User has no Janrain User ID in the database or global $user.
   */
  public function getJUIDFromUid($uid) {
    global $user;
    if ($user_fields = field_get_items('user', $user, 'field_janrain_capture_uuid')) {
      $duid = array_shift($user_fields);
      if (is_array($duid) && !empty($duid['value'])) {
        return $duid['value'];
      }
    }

    $drupal_user = user_load($uid);
    if (!empty($drupal_user->field_janrain_capture_uuid[LANGUAGE_NONE][0]['value'])) {
      return $drupal_user->field_janrain_capture_uuid[LANGUAGE_NONE][0]['value'];
    }

    if (module_exists('janrain_capture')) {
      $api = new JanrainCaptureApi();
      $profile = $api->loadUserEntity();
      if (!empty($profile[result][uuid])) {
        return $profile[result][uuid];
      }
    }
    throw new WFMApiException_NoJUID();
  }

  /**
   * Get a Pluck UID from a Drupal User ID.
   *
   * This method will attempt to get a pluck UID from Whole Foods Market,
   * or generate one if one does not already exist.
   *
   * @param string $uid
   *   A Drupal User ID for a User.
   *
   * @retval string
   *   A Pluck User Id for the Drupal User.
   *
   * @throws WFMApiException_NoUser
   *   If the UID is invalid.
   */
  public function getPUIDFromUid($uid) {
    $drupal_user = user_load($uid);
    if (!$drupal_user) {
      throw new WFMApiException_NoUser();
    }

    try {
      $user_api = new WFMApiUser();
      $pluck_id = $user_api->getPluckId($uid);
      return $pluck_id;
    }
    catch (Exception $e) {
      throw $e;
    }
  }

  /**
   * Builds a cache id based on a unique $lable and $args.
   *
   * @param string $label
   *   A unique lable for the cache id.
   * @param array $args
   *   A list of raplcement args $key=>$vale to replace :keys in $label.
   *
   * @retval string
   *   A cache id string based on $label with replacements from $args.
   */
  static protected function _buildCid($label, array $args) {
    $replacements = array(
      ':label' => drupal_strtolower($label),
      ':args' => drupal_strtolower(str_replace(' ', '', implode('-', $args))),
    );
    return str_replace(array_keys($replacements),
      array_values($replacements),
      'WFMAPI::label::args');
  }

  /**
   * Get cached data.
   *
   * @param string $cid
   *   The cache id to retrive data from
   *
   * @retval FALSE
   *   No valid cached data, or cached data has expired.
   * @retval object
   *   The cached object.
   */
  static protected function _getCache($cid, $bin = 'cache_wfm_api') {
    $options = array(
      'cache_bin' => $bin
    );
    $cache = new WfmApiDrupalCache($options);
    if ($cache_item = $cache->get($cid)) {
      return $cache_item->getValue();
    }
  }

  /**
   * Store data in Drupal cache.
   *
   * @param string $cid
   *   The cache id used to store the data for later retrival.
   * @param mixed $data
   *   The data to be stored in cache.
   * @param int $expire
   *   A time for the cache to expire in second past when it's set.
   *   Use CACHE_TEMPORARY to store until cache tables are cleared.
   */
  static protected function _setCache($cid, $data, $expire = CACHE_TEMPORARY, $bin = 'cache_wfm_api') {
    $options = array(
      'cache_bin' => $bin,
      'ttl' => $expire
    );
    $cache = new WfmApiDrupalCache($options);
    $cache->set($cid, $data);
  }

  /**
   * Expire specifc cached data in Drupal cache.
   *
   * @param string $cid
   *   the cache id of the object to expire.
   */
  static protected function _expireCache($cid, $bin = 'cache_wfm_api') {
    cache_set($cid, NULL, $bin, REQUEST_TIME - 1);
    $cache = New WfmApiDrupalCache(array('cache_bin' => $bin));
    $cache->remove($cid);
  }


  /**
   * Get shopping list/recipe categories from SAGE API.
   * @return array
   *   Array of category objects.
   */
  public function getCategories() {
    $categories = &drupal_static(__FUNCTION__);

    if (!$categories) {

      $cid = $this->_buildCid($this->cache_category_list, array());

      $categories = $this->_getCache($cid);

      if (!$categories) {
        $categories = $this->categoryApi->getCategories();
        $this->_setCache($cid, $categories);
      }

    }
    return $categories;
  }

  /**
   * Get a category name by it's ID.
   *
   * @param int $category_id
   *   Category ID.
   *
   * @return string
   *   Category Name.
   */
  public function getCategoryName($category_id) {
    $categories = $this->getCategories();
    foreach ($categories as $category) {
      if ($category['id'] == $category_id) {
        return $category['name'];
      }
    }
    return t('Other');
  }

  /**
   * Get recipe ingredients as an array for adding to a shopping list.
   *
   * @param array $recipe
   *   Recipe as an array as returned by SAGE v2 client
   *
   * @return array
   *   Ingredients array.
   */
  protected function getRecipeIngredientsAsShoppingListItems($recipe) {
    $items = array();
    foreach ($recipe['ingredients'] as $index => $ingredient) {
      $items[$index]['name'] = $ingredient['amount'] . ' ';
      if ($ingredient['amount'] <= 1) {
        $items[$index]['name'] .= $ingredient['uom'];
      }
      else {
        $items[$index]['name'] .= $ingredient['uom_plural'];
      }
      $items[$index]['name'] .= ' ' . $ingredient['name'];
      if (!empty($ingredient['extra_info'])) {
        $items[$index]['name'] .= ' ' . $ingredient['extra_info'];
      }
      $items[$index]['category_id'] = $this->getCategoryIdFromName($ingredient['category']);
    }
    return $items;
  }

  /**
   * Get a category id from a category name.
   *
   * @param string $category_name
   *   Category Name.
   *
   * @return int
   *   Category ID.
   */
  protected function getCategoryIdFromName($category_name) {
    $categories = $this->getCategories();
    foreach ($categories as $id => $text) {
      if ($text == $category_name) {
        return $id;
      }
    }
  }

  /**
   * Refresh Janrain Access Token.
   *
   * Calls a function that determines if access token needs refreshed
   * and does so if needed.
   */
  private function refreshJanrainAccessToken() {
    $user = &drupal_static(__FUNCTION__);
    if (empty($user)) {
      if (module_exists('janrain_capture')) {
        $janrain = new JanrainCaptureApi();
        $user = $janrain->loadUserEntity();
      }
    }
    return $user;
  }

  /**
   * Recursively travel array or object and perform filter_xss on strings.
   *
   * @param mixed $values
   *   Values to sanitize
   * @param bool $allow_html
   *   Indicate whether to run check_plain or filter_xss.
   *
   * @return string
   *   Filtered string
   */
  protected function sanitizeValues($values, $allow_html = FALSE) {
    if (is_string($values)) {
      // filter_xss default tags + 'b'
      if ($allow_html) {
        $allowed_tags = array(
          'a',
          'em',
          'strong',
          'cite',
          'blockquote',
          'code',
          'ul',
          'ol',
          'li',
          'dl',
          'dt',
          'dd',
          'b'
        );
        $values = filter_xss($values, $allowed_tags);
      }
      else {
        $values = check_plain($values);
      }
    }
    elseif (is_array($values) || is_object($values)) {
      foreach ($values as $child) {
        $this->sanitizeValues($child);
      }
    }
    return $values;
  }

}
