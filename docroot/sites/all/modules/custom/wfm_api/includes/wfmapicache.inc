<?php

/**
 * @file
 * Interface that wraps Drupal cache system for use with the SAGE API PHP Library™.
 */
use Joomla\Cache\Cache;
use Joomla\Cache\Item;

/**
 * Drupal cache driver for the SAGE API PHP Library.
 * Extends interface provided by Joomla\Cache\Cache.
 * This class exists so that we can inject the caching mechanism into
 * the Sage Api PHP Library for caching the authentication token.
 */
class WfmApiDrupalCache extends Cache {

  /**
   * @var string Drupal cache bin.
   */
  private $cache_bin;

  /**
   * @var int expiration time for cache item in seconds.
   */
  private $ttl;

  /**
   * @var string  Used to persist previously defined cache bin.
   */
  private $previousBin;

  /**
   * Constructor.
   *
   * @param array $options
   *   An options array.
   */
  public function __construct($options = array()) {
    if (isset($options['cache_bin'])) {
      $this->cache_bin = $options['cache_bin'];
    }
    else {
      $this->cache_bin = 'cache_wfm_api';
    }
    if (isset($options['ttl'])) {
      $this->ttl = $options['ttl'];
    }
    else {
      $this->ttl = CACHE_TEMPORARY;
    }
    $this->previousBin = NULL;
  }

  /**
   * Clear all cache_wfm_api tables.
   *
   * @return bool
   *   The result of the clear operation.
   */
  public function clear() {
    $tables = array(
      'cache_wfm_api',
      'cache_wfm_api_recipe_lists',
      'cache_wfm_api_sales_flyers',
      'cache_wfm_api_shopping_lists'
    );
    try {
      foreach ($tables as $table) {
        db_truncate($table)->execute();
      }
    }
    catch (Exception $e) {
      watchdog('wfm_api', $e);
    }
    return TRUE;
  }

  /**
   * Get cached data by id.
   *
   * @param string $key
   *   Cache item key.
   *
   * @return mixed
   *   Cached value
   */
  public function get($key) {
    $result = FALSE;

    $this->setCacheBin($key);

    $item = new Item($key);
    $cache = cache_get($key, $this->cache_bin);

    if ($cache == FALSE) {
      $result = FALSE;
    }
    else {
      if ($cache->expire == CACHE_PERMANENT
        || $cache->expire == CACHE_TEMPORARY
        || $cache->expire + $cache->created > REQUEST_TIME) {
        $item->setValue($cache->data);
        $result = $item;
      }
    }

    $this->setCacheBin($key);
    return $result;
  }

  /**
   * Remove a cache entry.
   *
   * @param string $key
   *   Cache key.
   */
  public function remove($key) {
    $this->setCacheBin($key);
    cache_clear_all($key, $this->cache_bin);
    $this->setCacheBin($key);
  }

  /**
   * Set a cache item.
   *
   * @param string $key
   *   Key value for cache item.
   *
   * @param mixed $data
   *   Data to be cached.
   *
   * @return bool
   *   Cache set result.
   */
  public function set($key, $data, $ttl = NULL) {
    $this->setCacheBin($key);

    if ($ttl) {
      $this->ttl = $ttl;
    }

    $result = cache_set($key, $data, $this->cache_bin, $this->ttl);

    $this->setCacheBin($key);

    return $result;
  }

  /**
   * Method to determine if a cache item exists.
   *
   * @param string $key
   *   Cache Key value.
   *
   * @return bool
   *   True if item exist.
   */
  public function exists($key) {
    if ($this->get($key)) {
      return TRUE;
    }
  }

  /**
   * Determines the correct cache bin to place a cache item.
   *
   * @param string $key
   *   Cache bin id
   */
  private function setCacheBin($key) {
    if ($key == 'wfmSageAccessToken') {
      $this->previousBin = $this->cache_bin;
      $this->cache_bin = 'cache_wfm_api';
    }
    elseif (isset($this->previousBin) && $this->previousBin != 'cache_wfm_api') {
      $this->cache_bin = $this->previousBin;
    }
  }

}
