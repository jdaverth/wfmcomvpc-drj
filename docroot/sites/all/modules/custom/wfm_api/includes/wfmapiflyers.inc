<?php

/**
 * @file
 * Contains WFMApiFlyers.
 */

use Wfm\Api\SageClient\Store;

class WFMApiFlyers extends WFMApi {

  /**
   * @var object Store API container.
   */
  protected $storeAPI;

  /**
   * @var string Base cache id.
   */
  protected $baseCacheCid;

  /**
   * @var string Cache id for sales flyer cache table
   */
  protected $cacheBin;

  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct();
    $this->baseCacheCid = 'getStoreItemsFlyer_';
    $this->cacheBin = 'cache_wfm_api_sales_flyers';

    $cache = new WfmApiDrupalCache(array('cache_bin' => $this->cacheBin));
    $this->storeApi = new Store(
      $this->api_key,
      $this->api_secret,
      $this->api_url,
      $this->api_version,
      $cache
    );
  }

  /**
   * Get sales items by store TLC.
   *
   * @param string $tlc
   *   Store TLC.
   *
   * @return array
   *   A list of all current sales items.
   */
  public function getAllItemsByStore($tlc) {
    $cache_cid = $this->baseCacheCid . $tlc;
    $sales_items = NULL;

    if ($sales_items = $this->_getCache($cache_cid, $this->cacheBin)) {
      return $sales_items;
    }
    $sales_response = $this->storeApi->getStoreSales($tlc);
    if ($sales_response) {
      $sales_items = $this->formatSalesItems($sales_response);
      $sales_items = $this->sanitizeValues($sales_items);
      $this->_setCache($cache_cid, $sales_items, REQUEST_TIME + 43200, $this->cacheBin);
    }
    return $sales_items;
  }

  /**
   * Format sales flyer response to be passed on for presentation.
   *
   * @param array $response
   *   SAGE API response.
   *
   * @return array
   *   Array of sales flyer items cast as objects.
   */
  private function formatSalesItems($response) {
    $sales_items_processed = array();
    $all_sales = $response[0]['sales'];

    // Sort sales groups so that those expiring first are listed first.
    usort($all_sales, function($a, $b) {
      $a_time = strtotime($a['end_date']);
      $b_time = strtotime($b['end_date']);

      if ($a_time < $b_time) {
        return -1;
      }

      if ($a_time == $b_time) {
        return 0;
      }

      if ($a_time > $b_time) {
        return 1;
      }
    });

    foreach ($all_sales as $sale_group) {
      $sales_items_processed = array_merge($sales_items_processed, $this->addDateToSalesItems($sale_group));
    }
    return $sales_items_processed;
  }

  /**
   * Iterate over individual sale items in a sales group and add start date and end date.
   *
   * @param array $sale_group
   *   Grouped set of sale items from SAGE response.
   *
   * @return array
   *   An array of sales items.
   */
  private function addDateToSalesItems($sale_group) {
    $sales_items = array();
    $i = 0;
    foreach ($sale_group['items'] as $sale_item) {
      $sales_items[$i] = (object) $sale_item;
      $sales_items[$i]->end_date = $sale_group['end_date'];
      $sales_items[$i]->start_date = $sale_group['start_date'];
      $i++;
    }
    return $sales_items;
  }
}
