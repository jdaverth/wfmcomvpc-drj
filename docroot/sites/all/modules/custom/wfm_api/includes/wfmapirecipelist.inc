<?php
/**
 * @file
 * Contains WFMApiRecipeList.
 */

use Wfm\Api\SageClient\User;

class WFMApiRecipeList extends WFMApi {

  /**
   * @var object Recipe List API container.
   */
  protected $apiUser;

  /**
   * @var string base cache id for class WFMApiRecipeList
   */
  protected $base_cid;

  /**
   * @var string cache bin for class WFMApiRecipeList
   */
  protected $cache_bin;

  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct();

    $this->cache_bin = 'cache_wfm_api_recipe_lists';

    $cache = new WfmApiDrupalCache(array(
      'cache_bin' => $this->cache_bin
    ));

    $this->base_cid = 'getRecipeList';
    $this->apiUser = new User(
      $this->api_key,
      $this->api_secret,
      $this->api_url,
      $this->api_version,
      $cache
    );
  }

  /**
   * Get the total count of recipes in a user's recipe list.
   *
   * @param string $uid
   *   A valid Drupal user id.
   *
   * @return int
   *   The number of recipes in a user's recipe list.
   */
  static public function getCount($uid) {
    try {
      $list = $this->getList($uid);
      return count($list);
    }
    catch (Exception $e) {
      return 0;
    }
  }

  /**
   * Test if a recipe (Drupal NID) is in a user's recipe list.
   *
   * @param string $uid
   *   A valid Drupal user id.
   * @param string $nid
   *   A valid Drupal node id for a node of tyoe 'recipe'.
   *
   * @return bool
   *   TRUE if the node is in the user's recipe list, FALSE if not.
   */
  public function isInList($uid, $nid) {
    try {
      $list = $this->getList($uid);
      $rid = $this->_getMongoIdFromNid($nid);

      if (isset($list[$rid])) {
        return TRUE;
      }
      return FALSE;
    }
    catch (Exception $e) {
    }
  }

  /**
   * Get a list of recipes in the user's recipe list.
   *
   * This will only return recipes that are actual Drupal nodes. If the recipe
   * is in the user's recipe list in the WFM SAGE API, but the recipe doesn't
   * exist in Drupal (an old recipe for instance), it will not be returned in
   * this list.
   *
   * The Drupal node will be added to each recipe item that is returned.
   *
   * NOTE: This data is cached, and may not be up to date if the data was
   *       altered outside of this class.
   *
   * @param string $uid
   *   A valid Drupal user id.
   *
   * @return array
   *   An array of all recipes in the user's recipe list that are actual Drupal
   *   nodes.
   */
  public function getList($uid) {
    $list = &drupal_static(__FUNCTION__);
    if (!$list) {
      $cid = $this->_buildCid($this->base_cid, array($uid));
      $list = $this->_getCache($cid, $this->cache_bin);
      if (!$list) {
        $list = array();
        $mongo_ids = array();

        // Make request.
        try {
          $uuid = $this->getJUIDFromUid($uid);
          $this->apiUser->setJanrainUuid($uuid);
          $response = $this->apiUser->getRecipes();
        }
        catch (BadResponseException $e) {
          watchdog('wfmapi', $e->getResponse()->getBody(TRUE));
        }

        // Get a list of all mongo ids from response.
        foreach ($response as $item) {
          $mongo_ids[] = $item['recipe'];
        }

        // Get node ids of recipe nodes associated to mongo ids.
        try {
          $query = new EntityFieldQuery();
          $query->entityCondition('entity_type', 'node')
            ->entityCondition('bundle', 'recipe')
            ->fieldCondition('field_mongo_id', 'value', $mongo_ids)
            ->addMetaData('account', user_load(1));
          $query_result = $query->execute();
        }
        catch(Exception $e) {
          watchdog('wfmapi', $e);
        }

        // Get recipe nodes.
        $nodes = node_load_multiple(array_keys($query_result['node']));
        foreach ($nodes as $key => $node) {
          $rmid = $node->field_mongo_id[LANGUAGE_NONE][0]['value'];
          $list[$rmid] = array(
            'node' => $node,
          );
        }
        if ($uid) {
          $list = $this->sanitizeValues($list);
          $this->_setCache($cid, $list, CACHE_TEMPORARY, $this->cache_bin);
        }
      }
    }
    return $list;
  }

  /**
   * Remove a recipe from the user's recipe list.
   *
   * NOTE: This method will expire the self::getList() cache.
   *
   * @param string $uid
   *   A valid Drupal user id.
   * @param string $nid
   *   A valid Drupal node id of a recipe.
   */
  public function deleteItem($uid, $nid) {
    $nid = $this->sanitizeValues($nid);
    try {
      $recipe_id = $this->_getMongoIdFromNid($nid);
      if ($recipe_id) {
        // Expire Cache.
        $cid = $this->_buildCid($this->base_cid, array($uid));
        $this->_expireCache($cid, $this->cache_bin);

        // Send request.
        $uuid = $this->getJUIDFromUid($uid);
        $this->apiUser->setJanrainUuid($uuid);
        return $this->apiUser->deleteRecipe($recipe_id);
      }
    }
    catch (BadResponseException $e) {
      watchdog('wfmapi', $e->getResponse()->getBody(TRUE));
    }
  }

  /**
   * Add a recipe to the user's recipe list.
   *
   * NOTE: This method will expire the self::getList() cache.
   *
   * @param string $uid
   *   A valid Drupal user id.
   * @param string $nid
   *   A valid Drupal node id of a recipe.
   */
  public function addItem($uid, $nid) {
    $recipe_id = $this->_getMongoIdFromNid($nid);

    if ($recipe_id) {
      // Build post data.
      $post_data = array(
        'recipes' => array(
          array('recipe' => $recipe_id),
        ),
      );
      // Expire cache.
      $cid = $this->_buildCid($this->base_cid, array($uid));
      $this->_expireCache($cid, $this->cache_bin);
      try {
        // Send request.
        $post_data = $this->sanitizeValues($post_data);
        $uuid = $this->getJUIDFromUid($uid);
        $this->apiUser->setJanrainUuid($uuid);
        return $this->apiUser->addRecipes($post_data);
      }
      catch (BadResponseException $e) {
        watchdog('wfmapi', $e->getResponse()->getBody(TRUE));
      }
    }
    else {
      drupal_set_message(t('Sorry, there was a problem adding this recipe to your recipe box.'));
    }
  }

  /**
   * Retrieve the Sage Mongo ID of a Recipe Node in Drupal.
   *
   * @param string $nid
   *   A valid Drupal node if of a recipe.
   *
   * @retval integer
   *   0 - No valid drupal node.
   *   RID of the node.
   */
  protected function _getMongoIdFromNid($nid) {
    $node = node_load($nid);
    if (isset($node->field_mongo_id[LANGUAGE_NONE][0]['value'])) {
      return $node->field_mongo_id[LANGUAGE_NONE][0]['value'];
    }
    return 0;
  }
}
