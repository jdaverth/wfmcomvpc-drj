<?php
/**
 * @file
 * Contains WFMApiUser.
 */

use Wfm\Api\SageClient\User;

class WFMApiUser extends WFMApi {

  /**
   * @var string Cache key for pluck ids
   */
  protected $pluck_id_cache;

  /**
   * @var object implementation of Wfm\Api\SageClient\User
   */
  protected $userApi;

  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct();
    $cache = new WfmApiDrupalCache();

    $this->pluck_id_cache = 'getPUId';
    $this->userApi = new User(
      $this->api_key,
      $this->api_secret,
      $this->api_url,
      $this->api_version,
      $cache
    );

    $this->getWfmId();
  }

  /**
   * Get the Whole Foods Market ID for current user.
   * @return string
   *   The WFM ID for the current User.
   */
  public function getWfmId() {
    $janrain = new JanrainCaptureApi();
    $janrain_user = $janrain->loadUserEntity();
    return $janrain_user['result']['wfm_id'];
  }

  /**
   * Get the Pluck ID for a Drupal User.
   *
   * @param string $uid
   *   A valid Drupal user id.
   *
   * @retval string
   *   The Pluck ID for the Drupal User.
   */
  public function getPluckId($uid) {
    return $this->getPluckIdFromJanrain($uid);
  }

  /**
   * Get the Pluck ID from a Janrain ID.
   *
   * NOTE: This data is cached for performance reasons.
   *
   * @param string $uid
   *   Drupal user id.
   *
   * @return string
   *   The Pluck ID for the user.
   */
  public function getPluckIdFromJanrain($uid) {
    $pluck_id = &drupal_static(__FUNCTION__);
    if (!$pluck_id) {
      $janrain = new JanrainCaptureApi();
      $janrain_user = $janrain->loadUserEntity();
      $cid = self::_buildCid($this->pluck_id_cache, array($uid));
      $pluck_id = $this->_getCache($cid);

      if (!$pluck_id) {
        try {
          // Load Janrain user.
          $display_name = $janrain_user['result']['displayName'];
          $email = $janrain_user['result']['email'];

          // Make request.
          $uuid = $this->getJUIDFromUid($uid);
          $this->userApi->setJanrainUuid($uuid);
          $response = $this->userApi->getPluckCredentials($display_name, $email, time());
          $pluck_id = $this->readPluckUserIdFromCookieString($response['pluck_cookie']);
          if ($uid && $pluck_id) {
            $this->_setCache($cid, $pluck_id);
          }
        }
        catch (BadResponseException $e) {
          watchdog('wfmapi', $e->getResponse()->getBody(TRUE));
        }
      }
    }
    return $pluck_id;
  }

  /**
   * Get Janrain ID from Drupal DB by Drupal User Id.
   * @param int $uid, Drupal user id.
   * @return string||FALSE Return Janrain UUID or false.
   * @throws Exception
   */
  public function getJanrainUuidFromDrupalUserId($uid) {
    $janrain_uuid = &drupal_static(__FUNCTION__ . $uid);
    if (!$janrain_uuid) {
      if (!is_numeric($uid)) {
        throw new Exception('Supplied Drupal UID is not numeric');
      }
      try {
        $janrain_uuid = db_select('field_data_field_janrain_capture_uuid', 'u')
          ->fields('u', array('field_janrain_capture_uuid_value'))
          ->condition('entity_id', $uid)
          ->execute()
          ->fetchField();
      } catch (Exception $e) {
        watchdog('wfmapiuser|getJanrainUuidFromDrupalUserId', $e);
      }
    }
    return $janrain_uuid;
  }

  /**
   * Get pluck user id from cookie string.
   *
   * @var string $cookie_string
   *   Pluck cookie string.
   *
   * @return string
   *   Pluck user id.
   */
  public function readPluckUserIdFromCookieString($cookie_string) {
    $amp_position = strpos($cookie_string, '&');
    $user_id = substr($cookie_string, 2, $amp_position - 2);
    return $user_id;
  }

  /**
   * Get UPM profile id from email address.
   * @param string $email_address
   * @return string UPM profile id.
   * @see https://api-v3.wholelabs.com/v3/docs/#get--v3-users-profile-translate-email-{emailAddress}
   */
  public function getUpmProfileIdFromEmail($email_address) {
    $this->userApi->setApiVersion(3);
    try {
      $profile = $this->userApi->getProfileIdFromEmail($email_address);
      return $profile['profile_id'];
    }
    catch (Exception $e) {
      watchdog('wfmapi', $e);
    }
  }

  /**
   * Get UPM profile id from Janrain UUID
   * @param $janrain_id
   * @return string UPM profile id.
   * @see https://api-v3.wholelabs.com/v3/docs/#get--v3-users-profile-translate-{janrainUUID}
   */
  public function getUpmProfileIdFromJanrainId($janrain_id) {
    $this->userApi->setApiVersion(3);
    $this->userApi->setJanrainUuid($janrain_id);
    try {
      $profile = $this->userApi->getProfileIdFromJanrain();
      return $profile['profile_id'];
    }
    catch (Exception $e) {
      watchdog('wfmapi', $e);
    }
  }

  /**
   * Get UPM profile as an array.
   * @param $upm_id UPM UUID.
   * @return array
   * @see https://api-v3.wholelabs.com/v3/docs/#get--v3-users-profile-{profileId}
   * @see https://api-v3.wholelabs.com/v3/users/profile/data_types
   */
  public function getUpmProfile($upm_id) {
    $this->userApi->setApiVersion(3);
    $this->userApi->setProfileId($upm_id);
    try {
      return $this->userApi->getProfileInfo();
    }
    catch (Exception $e) {
      watchdog('wfmapi', $e);
    }
  }

  /**
   * Update an individual field inside a program in a UPM profile.
   * @param string $program Program key.
   * @param string $field Field key.
   * @param mixed $value Value to save in field.
   * @param string $upm_id UPM UUID.
   * @return array Response values.
   * @see https://api-v3.wholelabs.com/v3/docs/#put--v3-users-profile-{profileId}-programs-{program}-fields-{field}
   */
  public function updateUpmProgramField($program, $field, $value, $upm_id) {
    $this->userApi->setApiVersion(3);
    $this->userApi->setProfileId($upm_id);
    try {
      return $this->userApi->updateProgramFieldData($program, $field, $value);
    }
    catch (Exception $e) {
      watchdog('wfmapi', $e);
    }
  }

  /**
   * Update any number of fields inside a UPM program.
   * @param string $program Program key.
   * @param array $data Program data represented as an array.
   * @param $upm_id
   * @return array
   * @see https://api-v3.wholelabs.com/v3/docs/#put--v3-users-profile-{profileId}-programs-{program}-fields
   */
  public function updateUpmProgramData($program, $data, $upm_id) {
    $this->userApi->setApiVersion(3);
    $this->userApi->setProfileId($upm_id);

    try {
      return $this->userApi->updateProgramData($program, $data, $upm_id);
    }
    catch (Exception $e) {
      watchdog('wfmapi', $e);
    }
  }

  /**
   * Delete a program field from a UPM profile.
   * @param string $program Program key
   * @param string $field Program field key
   * @param string $upm_id UPM UUID
   * @return array
   * @see https://api-v3.wholelabs.com/v3/docs/#delete--v3-users-profile-{profileId}-programs-{program}-fields-{field}
   */
  public function deleteUpmProgramField($program, $field, $upm_id) {
    $this->userApi->setApiVersion(3);
    $this->userApi->setProfileId($upm_id);

    try {
      return $this->userApi->deleteProgramField($program, $field);
    }
    catch (Exception $e) {
      watchdog('wfmapi', $e);
    }
  }

  /**
   * Update UPM profile preferences.
   * @param array $preferences Profile preferences represented as an array.
   * @param string $upm_id UPM UUID
   * @return array
   * @see https://api-v3.wholelabs.com/v3/docs/#put--v3-users-profile-{profileId}-preferences
   */
  public function updateUpmPreferences($preferences, $upm_id) {
    $this->userApi->setApiVersion(3);
    $this->userApi->setProfileId($upm_id);

    try {
      return $this->userApi->updateProfilePreferences($preferences);
    }
    catch (Exception $e) {
      watchdog('wfmapi', $e);
    }
  }

  /**
   * Delete a preference field in a UPM profile.
   * @param string $preference_group Preference group key.
   * @param string $field Preference field.
   * @param string $upm_id UPM UUID.
   * @return array
   * @see https://api-v3.wholelabs.com/v3/docs/#delete--v3-users-profile-{profileId}-preferences-{preferenceName}-values-{preferenceValueName}
   */
  public function deleteUpmPreference($preference_group, $field, $upm_id) {
    $this->userApi->setApiVersion(3);
    $this->userApi->setProfileId($upm_id);

    try {
      return $this->userApi->deleteProfilePreference($preference_group, $field);
    }
    catch (Exception $e) {
      watchdog('wfmapi', $e);
    }
  }

  /**
   * Update a single field in a UPM profile core data.
   * @param string $field field key
   * @param mixed $value Value to save.
   * @param string $upm_id UPM UUID.
   * @return array
   * @see https://api-v3.wholelabs.com/v3/docs/#put--v3-users-profile-{profileId}-core-{coreFieldName}
   */
  public function updateUpmCoreField($field, $value, $upm_id) {
    $this->userApi->setApiVersion(3);
    $this->userApi->setProfileId($upm_id);

    try {
      return $this->userApi->updateCoreField($field, $value);
    }
    catch (Exception $e) {
      watchdog('wfmapi', $e);
    }
  }

  /**
   * Update core data in UPM profile.
   * @param array $data Core data presented as array.
   * @param string $upm_id UPM UUID
   * @return array
   * @see https://api-v3.wholelabs.com/v3/docs/#put--v3-users-profile-{profileId}-core
   */
  public function updateUpmCoreData($data, $upm_id) {
    $this->userApi->setApiVersion(3);
    $this->userApi->setProfileId($upm_id);

    try {
      return $this->userApi->updateCoreData($data);
    }
    catch (Exception $e) {
      watchdog('wfmapi', $e);
    }
  }

  /**
   * Delete a core profile field. This will set the value to null, not remove the field.
   * @param string $field Field name.
   * @param string $upm_id UPM UUID
   * @return array
   * @see https://api-v3.wholelabs.com/v3/docs/#delete--v3-users-profile-{profileId}-{coreFieldName}
   */
  public function deleteUpmCoreField($field, $upm_id) {
    $this->userApi->setApiVersion(3);
    $this->userApi->setProfileId($upm_id);

    try {
      return $this->userApi->deleteCoreField($field);
    }
    catch (Exception $e) {
      watchdog('wfmapi', $e);
    }
  }
}
