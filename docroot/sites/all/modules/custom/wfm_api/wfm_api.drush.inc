<?php

/**
 * @file
 * WFM API module drush integration.
 */

/**
 * Implements hook_drush_command().
 */
function wfm_api_drush_command() {
  $items = array();
  $items['wfm-clear-fields-cache'] = array(
    'description' => dt('Clear out all fields cache.'),
    'aliases' => array('wfm-ccf'),
  );
  $items['wfm_ip_whitelist'] = array(
    'description' => 'Set reverse_proxy_addresses variable with Fastly ips.',
    'callback' => '_wfm_api_update_ip_whitelist',
    'aliases' => array('wfmip'),
  );

  return $items;
}

/**
 * Clear out all fields cache.
 */
function drush_wfm_api_wfm_clear_fields_cache() {
  drush_print('Clearing entire field cache');
  cache_clear_all('*', 'cache_field', TRUE);
  drush_print('Field cache cleared');
}

/**
 * Callback function that defines the functionality of wfmip drush command.
 * @see wfm_api_drush_command
 */
function _wfm_api_update_ip_whitelist() {
  $response = drupal_http_request('https://api.fastly.com/public-ip-list');
  if ($response->status_message == 'OK') {
    $ips = array();
    $ranges = (array)json_decode($response->data);
    foreach($ranges['addresses'] as $range) {
      $ips = array_merge(_wfm_api_get_ip_ranges($range), $ips);
    }
    if (file_unmanaged_save_data(serialize($ips), 'private://ip_addresses.txt', FILE_EXISTS_REPLACE)) {
      watchdog('wfm_api', 'fastly_whitelist_ip_addresses updated');
      drupal_set_message(t('Success! fastly_whitelist_ip_addresses updated!'));
    }
    else {
      watchdog('wfm_api', 'Error writing ip_addresses.txt file.', WATCHDOG_ERROR);
      drupal_set_message(t('Error! Error writing ip_addresses.txt file.'), 'error');
    }
  }
  else {
    watchdog('wfm_api', 'Error connecting to Fastly whitelist ips.', WATCHDOG_ERROR);
    drupal_set_message(t('Error! Unable to get list of ip addresses'), 'error');
  }
}

/**
 * Create array of ip addresses from ip range.
 * @param $range string | ip address range formatted like 127.0.0.1/100
 * @return array
 */
function _wfm_api_get_ip_ranges($range) {
  $addresses = array();
  $octets = explode('.', $range);
  list($bottom, $top) = explode('/', $octets[3]);
  while ($bottom <= $top) {
    // Cast all as ints to filter any possible funny business.
    $addresses[] = (int)$octets[0] . '.' . (int)$octets[1] . '.' . (int)$octets[2] . '.' . (int)$bottom;
    $bottom++;
  }
  return $addresses;
}