/**
 * @file
 * Behavior for store select forms.
 */

//Self-invoking function
(function($){
  "use strict";
  $.fn.WFMstoreSelect = function() {
    var storeselect = {

      settings: {
        storeoptions: {},
      },

      /**
       * Add listeners to dom elements
       */
      addListeners: function() {
        $('.state-select').change(function() {
          storeselect.setFormId(this);
          storeselect.enableStoreSelect(this);
          storeselect.setStoreOptionsByState(this);
          storeselect.disableSubmit(this);
        });
        $('.store-select').change(function() {
          storeselect.setFormId(this);
          storeselect.enableSubmit(this);
        });
      },

      /**
       * Ajax callback to set default user store.
       * @param storeObject
       */
      ajaxCallBack: function(storeObject) {
        var store = Drupal.WholeFoods.removeStoreNidKey(storeObject);

        if (store && typeof(store) == 'object') {
          storeselect.setUserDefaults(store);
        }
      },

      /**
       * Sometimes multiple store select forms can come into the page with the
       * same id. This function will check if there are multiple forms with
       * #store-select-form. In that case we iterate over the forms and add a
       * number to the form. eg #store-select-form-0, #store-select-form-1
       */
      changeForMultipleIds: function() {
        var j = 0;
        if ($('form#store-select-form').length > 1) {
          $('form#store-select-form').each(function(i){
            this.setAttribute("id", "store-select-form-" + j);
            var storeSelect = $("form#store-select-form-" + j).find('.state-select');
            storeselect.disableStoreSelect(storeSelect);
            storeselect.setStoreOptionsByState();
            storeselect.disableSubmit(storeSelect);
            storeselect.addListeners();
            j++;
          });
        }
      },

      /**
       * Disable store-select form store drop-down.
       * @param object state_select
       *   DOM element of state select.
       */
      disableStoreSelect: function(state_select) {
        var selected_state = $(state_select).val(),
            form_id = '#' + $(state_select).parents('form').attr('id');
        if (!selected_state || selected_state == '0') { //Zero as a string. Intentional.
          $(form_id + ' .store-select').addClass('disabled').prop('disabled', true);
        }
      },

      /**
       * Disable store-select form submit.
       * @param object state_select
       *   DOM element of state select.
       */
      disableSubmit: function(state_select) {
        var form_id = '#' + $(state_select).parents('form').attr('id'),
            selected_store = $(form_id + ' .store-select').val();
        if (!selected_store) {
          $(form_id + ' .store-select-submit').addClass('disabled').prop('disabled', true);
        }
      },

      /**
       * Enable store-select form store drop-down.
       * @param object state_select
       *   DOM element of state select.
       */
      enableStoreSelect: function(state_select){
        var form_id = '#' + storeselect.form_id;
        if ($(state_select).val() && $(state_select).val() !== '0') { // Zero as a string. Intentional.
          $(form_id + ' .store-select').removeClass('disabled').prop('disabled', false);
        }
      },

      /**
       * Enable store-select form submit.
       * @param object store_select
       *   DOM element of store select.
       */
      enableSubmit: function(store_select) {
        var form_id = '#' + storeselect.form_id;
        $(form_id + ' .store-select-submit').prop('disabled', false).removeClass('disabled');
      },

      /**
       * Get id's for each store select on the page
       * @returns {Array}
       */
      findStoreSelectForms: function() {
        var ids = [];
        $('.store-select-form').each(function(){
          var id = $(this).attr('id');
          ids.push(id);
        });
        return ids;
      },

      /**
       * Build <option> element.
       * @param object values
       *   Attributes for the option element in key/value pairs.
       * @return object
       *   <option> DOM element
       */
      getOption: function(values) {
        var opt = document.createElement('OPTION'),
            option_values = {},
            defaults = {
              label: Drupal.t('Please Select a Store'),
              text: Drupal.t('Please Select a Store'),
              value: ''
            };
        if (typeof values == 'undefined') {
          values = {};
        }
        option_values = $.extend(option_values, defaults, values);
        for (var attribute in option_values) {
          // Check to make sure property is not coming from object prototype.
          if (option_values.hasOwnProperty(attribute)) {
            opt[attribute] = option_values[attribute];
          }
        }
        return opt;
      },

      /**
       * Get store option values from the store select drop-down and save them
       * in storeselect.settings.storeoptions variable.
       */
      getStoreOptions: function() {
        var forms = storeselect.findStoreSelectForms();
        forms.map(function(form_id){
          var options = {};
          $('#' + form_id + ' .store-select').children('optgroup').each(function(){
            var label = $(this).attr('label');
            options[label] = {};
            $(this).children('option').each(function(){
              var value = $(this).val();
              options[label][value] = {};
              options[label][value].name = $(this).text();
              options[label][value].selected = false;
              if ($(this).prop('selected') === true) {
                options[label][value].selected = true;
              }
            });
          });
          storeselect.settings.storeoptions[form_id] = options;
        });
      },

      /**
       * Initialization function.
       */
      init: function() {
        storeselect.addListeners();
        storeselect.changeForMultipleIds();
        storeselect.getStoreOptions();
        $('.state-select').each(function(){
          storeselect.disableStoreSelect(this);
          storeselect.setStoreOptionsByState(this);
          storeselect.disableSubmit(this);
        });
        storeselect.prepareUserDefaults();
      },

      /**
       * Determine if user has a store selected
       */
      prepareUserDefaults: function() {
        var storenid = '',
            settings = storeselect.getSettingsForm();

        if (Drupal.WholeFoods.getCookie('local_store')) {
          storenid = Drupal.WholeFoods.getCookie('local_store');
        }

        if (settings.store) {
           storenid = settings.store;
        }

        if (storenid) {
          Drupal.WholeFoods.getStoreInfo(storenid, storeselect.ajaxCallBack);
        }
      },

      /**
      * Get settings from url
      */
      getSettingsForm: function() {
        var params = {};
        if (location.search) {
           var parts = location.search.substring(1).split('&');
             for (var i = 0; i < parts.length; i++) {
                 var nv = parts[i].split('=');
                 if (!nv[0]) continue;
                 params[nv[0]] = nv[1] || true;
                }
        }
        return params;
      },

      /**
       * Set form_id variable so we know which form we're dealing with
       * setFormId(this);
       */
      setFormId: function(element) {
        storeselect.form_id = $(element).parents('form').attr('id');
      },

      /**
       * Filter store select options by selected state.
       * @param object state_select
       *   State select DOM object
       */
      setStoreOptionsByState: function(state_select){
        var form_id = $(state_select).parents('form').attr('id'),
            stateabbr = $(state_select).val(),
            defaultopt = storeselect.getOption(),
            store_select_element = $('#' + form_id + ' .store-select');

        var options = storeselect.settings.storeoptions[form_id] || {};

        store_select_element.find('option').each(function(){
          $(this).detach();
        });
        store_select_element.find('optgroup').each(function(){
          $(this).detach();
        });
        store_select_element.append(defaultopt);

        //loop through options object, create optgroups and options
        $.each(options, function(index, value) {
          var abbr = index.substring(0, 2),
              optgroup = {};
          if (abbr == stateabbr) {
            optgroup = document.createElement('optgroup');
            optgroup.label = index.substring(2);
            store_select_element.append(optgroup);
            $.each(value, function(optindex, optvalue) {
              var option_values = {
                    label: optvalue.name,
                    text: optvalue.name,
                    value: optindex,
                    selected: optvalue.selected
                  },
                  opt = storeselect.getOption(option_values);
              store_select_element.find('optgroup').last().append(opt);
            });
          }
        });
      }, //setStateOptions

      /**
       * Set default state/store values from user's selected store.
       * @param object store
       *   Store info object from Drupal.setting.WholeFoods.stores
       */
      setUserDefaults: function(store) {
        var validStore = store && typeof(store) == 'object';
        var hasLocation,
            hasAbbreviation,
            hasNid;

        if (!validStore) {
          return;
        }

        // Remove store nid key from store object.
        // store = { 6606 { name: "" ... } } becomes store = { name: "" ... }.
        if (Object.keys(store).length === 1) {
          store = Drupal.WholeFoods.removeStoreNidKey(store);
        }

        hasLocation = store.hasOwnProperty('location');

        // Don't attempt to access sub-property of location if it doesn't exist.
        if (!hasLocation) {
          return;
        }

        hasAbbreviation = store.location.hasOwnProperty('stateabbr');
        hasNid = store.hasOwnProperty('nid');

        if (!hasLocation || !hasAbbreviation || !hasNid) {
          return;
        }

        $('.state-select option[value="' + store.location.stateabbr + '"]').prop('selected', true);

        //This trigger is necessary for FF to notice the change.
        $('.state-select').trigger('change');

        $('.store-select').removeClass('disabled').prop('disabled', false);
        $('.store-select option[value="' + store.nid + '"]').prop('selected', true);
        $('.store-select').each(function () {
          storeselect.enableSubmit(this);
        });
      }

    }; //storeselect
    storeselect.init();

    Drupal.WholeFoods.setStoreSelectStore = storeselect.setUserDefaults;
  };

})(jQuery);

/**
 * Attach to Drupal behaviors.
 */
(function wfm_storeselect($){
   Drupal.behaviors.wfmstoreselect = {
    attach: function (context) {
      jQuery('body', context).once('ss-attach', function () {
        jQuery('.store-select-form', context).WFMstoreSelect();
      });
    }
  };
})(jQuery);
