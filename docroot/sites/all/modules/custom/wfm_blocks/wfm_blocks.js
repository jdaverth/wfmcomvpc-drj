/**
* @file
* Behavior for the main newsletter subscription form.
*/
Drupal.WholeFoods.wfmBlocks = {};
(function($) {
  /**
   *	Add listeners to fields for validation.
   */
  Drupal.behaviors.validation = {
    attach: function(context) {
      // Wrapping form submit insice jQuery.once to prevent
      // form form submitting multiple times
      $('body', context).once('wfm-blocks-subscription-form', function() {
        var form = $('.nl-subscription-form form');
        var emailInput = $('.nl-subscription-form .email');
        var errorArea = $('.nl-subscription-form .error-area');
        var checkEmailPattern = Drupal.WholeFoods.wfmBlocks.checkEmailPattern;

        form.submit(function(event) {
          if (!checkEmailPattern(emailInput.val())) {
            // Email Validation Failure
            event.preventDefault();
            errorArea.text(Drupal.t('Please, enter a valid email address'));
            emailInput.addClass('error');
            Drupal.WholeFoods.wfmBlocks.pushEvent('error - invalid email');
          } else {
            // Email Validation Success
            errorArea.text('');
            emailInput.removeClass('error');
            Drupal.WholeFoods.wfmBlocks.pushEvent('multi');
          }
        });
      });
    }
  };

  /**
   * push Google Analytics event into window.dataLayer
   *
   * @param {string} status
   *    subscribeStatus for GA - either 'success' or 'error'
   */
  Drupal.WholeFoods.wfmBlocks.pushEvent = function(status) {
    var obj = {
      event: 'newsletter-subscribe',
      formLocation: 'global-footer',
      subscribeStatus: status
    };

    if (window.dataLayer) {
      window.dataLayer.push(obj);
    }
  };

  /**
   *	Check for valid email address
   *
   * @param string email
   *		Text to check to see if it is valid email address
   *
   *	@return boolean
   *		true if is valid email address, false if is not a valid email address
   */
  Drupal.WholeFoods.wfmBlocks.checkEmailPattern = function(email) {
    var re = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    return re.test(email);
  };
})(jQuery);


