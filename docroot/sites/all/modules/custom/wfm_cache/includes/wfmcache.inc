<?php

/**
 * @file
 * Defines the WFMCache class.
 */

define('WFMCACHE_CID', 'WFMAPI::label::args');

class WFMCache {

  protected $cid;
  protected $expireCid;
  protected $data = NULL;
  /**
   * Class WFMCache instatiation function.
   */
  public function __construct($cid_label, array $cid_args = array()) {
    $this->cid = self::_buildCid($cid_label, $cid_args);
    $this->expireCid = self::_buildTCid($cid_label, $cid_args);
    $this->_retrieveData();
  }

  /**
   * Get data function.
   */
  public function getData($pull_data = FALSE) {
    if (TRUE == $pull_data) {
      $this->_retrieveData();
    }

    return $this->data;
  }

  /**
   * Set data function.
   */
  public function setData($data, $expire = CACHE_TEMPORARY) {
    $this->data = $data;
    $expire_time = strtotime('+10 years');

    if (CACHE_TEMPORARY != $expire && CACHE_PERMANENT != $expire) {
      $expire_time = strtotime($expire);
    }

    self::_setCache($this->expireCid, $expire_time);
    self::_setCache($this->cid, $this->data, $expire_time);
  }

  /**
   * Expire cache function.
   */
  public function expire() {
    $this->data = NULL;
    self::_expireCache($this->expireCid);
    self::_expireCache($this->cid);
  }

  /**
   * Helper function retrive data.
   */
  protected function _retrieveData() {
    $data = self::_getCache($this->cid);

    if (FALSE === $data) {
      $this->data = NULL;
    }
    else {
      $cached_time = self::_getCache($this->expireCid);
      if (FALSE === $cached_time || $cached_time < REQUEST_TIME) {
        $this->data = NULL;
      }
      else {
        $this->data = $data;
      }
    }
  }

  /**
   * Builds a cache id based on a unique $lable and $args.
   *
   * @param string $label
   *   A unique lable for the cache id.
   * @param array $args
   *   A list of raplcement args $key=>$vale to replace :keys in $label.
   *
   * @retval string
   *   A cache id string based on $label with replacements from $args.
   */
  static protected function _buildCid($label, array $args) {
    $replacements = array(
      ':label' => drupal_strtolower($label),
      ':args' => drupal_strtolower(str_replace(' ', '', implode('-', $args))),
    );
    return str_replace(array_keys($replacements), array_values($replacements), WFMCACHE_CID);
  }

  /**
   * Build a cache id with a prefix specific to this module and marked as Time.
   *
   * @param string $label
   *   A unique label for this cache objet (ie, twitter_posts).
   * @param array $args
   *   Array of args to build CID.
   *
   * @return string
   *   A unique CID based on $label and $store_id with a 'Time' marker.
   */
  protected function _buildTCid($label, array $args) {
    $cid = $this->_buildCid($label, $args);
    $time_cid = $cid . '__Time';

    return $time_cid;
  }

  /**
   * Get cached data.
   *
   * @param string $cid
   *   The cache id to retrive data from
   *
   * @retval FALSE
   *   No valid cached data, or cached data has expired.
   * @retval object
   *   The cached object.
   */
  static protected function _getCache($cid) {
    $cache = cache_get($cid);

    if ($cache == FALSE) {
      return FALSE;
    }

    if ($cache->expire == CACHE_PERMANENT || $cache->expire == CACHE_TEMPORARY || $cache->expire > REQUEST_TIME) {
      return $cache->data;
    }

    return FALSE;
  }

  /**
   * Store data in Drupal cache.
   *
   * @param string $cid
   *   The cache id used to store the data for later retrival.
   * @param object $object
   *   The data to be stored in cache.
   * @param int $expire
   *   A time for the cache to expire in second past when it's set.
   *   Use CACHE_TEMPORARY to store until cache tables are cleared.
   */
  static protected function _setCache($cid, $object, $expire = CACHE_TEMPORARY) {
    cache_set($cid, $object, 'cache', $expire);
  }

  /**
   * Expire specifc cached data in Drupal cache.
   *
   * @param string $cid
   *   the cache id of the object to expire.
   */
  static protected function _expireCache($cid) {
    cache_set($cid, NULL, 'cache', REQUEST_TIME - 1);
  }

}
