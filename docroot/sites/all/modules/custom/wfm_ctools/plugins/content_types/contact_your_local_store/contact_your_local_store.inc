<?php

/**
 * @file
 * Plugin to provide contact your local store form.
 */

$plugin = array(
  'title' => t('Contact your local store form'),
  'description' => t('Customer service widget'),
  'single' => TRUE,
  'content_types' => array('contact_your_local_store'),
  'render callback' => 'wfm_mobile_contact_your_local_store_ct_render',
  'edit form' => 'wfm_mobile_contact_your_local_store_ct_edit_form',
  'category' => array(t('Custom'), 0),
);

/**
 * Render function.
 */
function wfm_mobile_contact_your_local_store_ct_render($subtype, $conf, $args = array()) {
  $block = new stdClass();
  $block->title = t('Contact Your Local Store');
  $block->content = '';
  $content = drupal_get_form('wfm_contact_your_local_store');
  if (count($content)) {
    $block->content = drupal_render($content);
    ctools_add_js('contact_your_local_store', 'wfm_ctools', 'plugins/content_types/contact_your_local_store');
  }
  return $block;
}

/**
 * Settings function.
 */
function wfm_mobile_contact_your_local_store_ct_edit_form($form) {
  return $form;
}

/**
 * Wfm_contact_your_local_store form.
 */
function wfm_contact_your_local_store($form, $form_state) {
  $state_list = _local_get_state_list(TRUE);
  $list = array();
  $results = views_get_view_result('store_lookup_by_address', 'state_number_list');
  $form = array();
  $phones = array();
  $store_ids = array();
  if (count($results)) {
    foreach ($results as $node) {
      $wrapper = entity_metadata_wrapper('node', $node->entity);
      $title = $wrapper->label();
      if ($title && $wrapper->access('view') && (!isset($wrapper->field_opening_soon) || !$wrapper->field_opening_soon->value())) {
        $nid = $wrapper->getIdentifier();
        $state = $wrapper->field_postal_address->administrative_area->value();
        $city = $wrapper->field_postal_address->locality->value();
        if (empty($state)) {
          $state = $wrapper->field_postal_address->country->value();
        }
        if (empty($state)) {
          $sate = 'ZOther';
        }
        elseif ($state == 'GB') {
          $state = 'UK';
        }
        if (empty($city)) {
          $city = $title;
        }
        $options[$state_list[$state]][$city][$state . '__' . $city . '__' . $nid] = ucfirst(strip_tags($title));
        $phones[$state . '__' . $city . '__' . $nid] = $wrapper->field_phone_number->value();
        if (!empty($wrapper->{RAP_FIELD})) {
          $store_tlc = store_get_tlc_by_acl_name($wrapper->{RAP_FIELD}[0]->label());
          $store_ids[$state . '__' . $city . '__' . $nid] = $store_tlc;
        }
        if (array_key_exists($state, $state_list)) {
          $list[$state] = $state_list[$state];
        }
      }
    }
    foreach ($options as $k => $state) {
      ksort($options[$k]);
    }
    asort($list);
    asort($options);
    $form['header'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array(
          'header-content',
        ),
      ),
    );
    $form['header']['description']['#markup'] = t('Individual stores are responsible for, respond to and correct the
      vast majority of issues. If you have a concern with a product or service, please contact a Shift Leader at the
      store where the problem arose.');
    $form['phone_number_state_list'] = array(
      '#type' => 'select',
      '#title' => t('Choose your state'),
      '#options' => $list,
    );
    $form['phone_number_store_list'] = array(
      '#type' => 'select',
      '#title' => t('Choose your store'),
      '#options' => $options,
      '#empty_option' => t('Select a Store'),
    );
    $form['phone_number'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array('phone-number-box'),
      ),
    );

    $form['phone_number']['empty'] = array(
      '#type' => 'item',
      '#markup' => t('Many issues can be solved by a quick call to your local store. Please select your store from the
        above menu to see phone number.'),
      '#states' => array(
        'visible' => array(
          ':input[name="phone_number_store_list"]' => array('value' => 'undefined'),
        ),
      ),
    );
    foreach ($phones as $key => $phone) {
      $form['phone_number'][$key] = array(
        '#type' => 'item',
        '#markup' => t('Many issues can be resolved with a quick phone call. This store can be reached directly at !phone.',
          array('!phone' => '<span data-store_id="' . $store_ids[$key] . '">' . $phone . '</span>')),
        '#states' => array(
          'visible' => array(
            ':input[name="phone_number_store_list"]' => array('value' => $key),
          ),
        ),
      );
    }

    // Save state and store values in cookie in case webform submission
    // has errors (https://digitalandweb.atlassian.net/browse/QD-1290#comment-15770)
    $form['#attached']['js'][] = array(
      'data' => '(function($) {
        $(function() {
          var wfmStateListElem = $("#edit-phone-number-state-list"),
              wfmStoreListElem = $("#edit-phone-number-store-list");

          wfmStoreListElem.change(function() {
            $.cookie("wfm_contact_state", wfmStateListElem.val());
            var val = $(this).val();
            "undefined" !== val && $.cookie("wfm_contact_store", val);
          });

          if ($.cookie("wfm_contact_state") && $.cookie("wfm_contact_store")) {
            wfmStateListElem.val($.cookie("wfm_contact_state")).change();
            wfmStoreListElem.val($.cookie("wfm_contact_store")).change();
          }
        });
       })(jQuery);',
      'type' => 'inline', 'scope' => 'footer',
    );
  }
  return $form;
}
