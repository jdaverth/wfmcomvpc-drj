/**
 * @file
 * Ctools content type contact your local store js
 */

(function($) {
  $(function() {
    var cs_options = [];
    var cs_store = null;
    var local_store = $.cookie('local_store');
    var _doStateSelectorChange = function(store_selector, state_selector) {
      $('optgroup', store_selector).detach();
      $('option', store_selector).detach();
      var store_selector2 = [];
      $('option:selected', state_selector).each(function() {
        store_selector2.push('<option value="undefined">' + Drupal.t('Select a Store') + '</option>');
        
        for (var key in cs_options[$(this).val()]) {
          if (key !== 'find') {
            store_selector2.push($('<div />').append(cs_options[$(this).val()][key]).clone().html());
          }
        }
      });
      store_selector.html(store_selector2.join(' '));
      if (!$('option[value="' + cs_store + '"]', store_selector).length) {
        var first = $('option', store_selector).first();
        first.attr('selected', 'selected');
      }
      else {
        $('option[value="' + cs_store + '"]', store_selector).attr('selected', 'selected');
      }
      store_selector.change();
    };
    var _initCustomerServiceForm = function() {
      var state_selector = $('#edit-phone-number-state-list');

      if (!state_selector.length) {
        return;
      }

      var store_selector = $('#edit-phone-number-store-list');

      var city = null;
      var city_state = null;
      var city_opt = null;

      var selected_state = null;

      $('option', store_selector).each(function() {
        var val = $(this).val();
        if (val !== '' && val !== 'undefined') {
          var pieces = val.split('__');

          if (pieces[0] === '') {
            pieces[0] = 'unknown';
          }
          if (pieces[2] === local_store) {
            cs_store = val;
          }

          if (pieces[1] !== city) {
            city = pieces[1];
            city_state = pieces[0];
            city_opt = $('<optgroup label="' + city + '"></optgroup>');
            if (typeof cs_options[city_state] === 'undefined') {
              cs_options[city_state] = [];
            }
            cs_options[city_state].push(city_opt);
          }

          $(this).detach();
          $(this).appendTo(city_opt);
        }
      });

      state_selector.change(function() {
        _doStateSelectorChange(store_selector, state_selector);
      });

      store_selector.change(function() {
        if ($(this).val() !== 'undefined') {
          var tlc = $('#edit-phone-number div:visible span');
          if (tlc.length) {
            $('input[name="submitted[tlc]"]').val(tlc.data('store_id'));
          }
          $('.webform-client-form').show();
          $('.hidden_form_message').hide();
        }
        else {
          $('.webform-client-form').hide();
          $('.hidden_form_message').show();
        }
      });

      if (selected_state) {
        state_selector.val(selected_state);
        var state_opt = $('option[value="' + selected_state + '"]', state_selector);
        state_opt.attr('selected', 'selected');
      }

      state_selector.change();

      var form = $('.webform-client-form');
      
      if (local_store && !selected_state) {
        Drupal.WholeFoods.getStoreInfo(local_store, function(storeinfo){
          storeinfo = Drupal.WholeFoods.removeStoreNidKey(storeinfo);
          state_selector.val(storeinfo.location.stateabbr);
          _doStateSelectorChange(store_selector, state_selector);
          $('#phone-number-store-list').attr('value', local_store);
          $('#phone-number-store-list option[value="' + cs_store + '"]').attr('selected', 'selected');
          $('#phone-number-store-list').change();
        });
      }

      if (!selected_state && !local_store) {
        $('.form-item, #edit-submit, #webform-component-product-upc, #webform-component-store-zip-code', form).hide();
        $('#webform-component-store-zip-code label')
                .append('<span class="form-required" title="This field is required.">*</span>');
        $('#webform-component-choose-topic').show();
      }
      else {
        $('#webform-component-choose-topic', form).show();
      }

      $('#webform-component-choose-topic').change(function() {
        if ($('option:selected', $(this)).val().length) {
          $('.form-item, #edit-submit', form).show();
          if ($('option:selected', $(this)).val() !== '365_edv') {
            $('#webform-component-product-upc').hide();
          }

          if ($('option:selected', $(this)).val() !== 'store_location_request') {
            $('#webform-component-store-zip-code').hide();
          }
        }
        else {
          $('.form-item, #edit-submit', form).hide();
          $('#webform-component-choose-topic').show();
        }
      });
      $('#webform-component-choose-topic').change();
    };
    _initCustomerServiceForm();

  });
})(jQuery);
