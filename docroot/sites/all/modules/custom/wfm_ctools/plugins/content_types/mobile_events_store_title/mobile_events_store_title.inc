<?php

/**
 * @file
 * Plugin to provide Mobile Events Store Title.
 */

$plugin = array(
  'title' => t('Mobile Events Store Title'),
  'description' => t('ER MAH GERD, ERT DERS THINS'),
  'single' => TRUE,
  'content_types' => array('mobile_events_store_title'),
  'render callback' => 'wfm_ctools_mobile_events_store_title_ct_render',
  'edit form' => 'wfm_ctools_mobile_events_store_title_ct_edit_form',
  'category' => array(t('Custom'), 0),
);


/**
 * Settings function.
 */
function wfm_ctools_mobile_events_store_title_ct_edit_form($form, &$form_state) {
  return $form;
}

/**
 * Render function.
 */
function wfm_ctools_mobile_events_store_title_ct_render($subtype, $conf, $args, $contexts) {
  $params = drupal_get_query_parameters();
  $block = new stdClass();
  $block->title = t('Events.');
  $block->content = l(t('change store'), 'stores/list', array('attributes' => array('class' => array('change-store'))))
    . '<div class="clearfix"></div>';
  if (array_key_exists('store', $params)) {
    $store = store_get_store_from_tid($params['store']);
    $block->title = t('Events: ' . $store->title);
  }
  return $block;
}
