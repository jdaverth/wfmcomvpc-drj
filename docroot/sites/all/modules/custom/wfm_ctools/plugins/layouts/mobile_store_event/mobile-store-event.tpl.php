<?php
/**
 * @file
 * Plugin to provide Special Diets Shopping List block.
 */
?>
<div class="panel-display panel-mobile-store-event clearfix" <?php if (!empty($css_id)) : print "id=\"$css_id\""; endif; ?>>
  <div class="left-wrapper">
    <div class="panel-panel panel-sidebar">
      <div class="inside"><?php print $content['sidebar']; ?></div>
    </div>
  </div>
  <div class="right-wrapper">
    <div class="top-wrapper">
      <div class="panel-panel panel-left-1">
        <div class="inside"><?php print $content['left_1']; ?></div>
      </div>
      <div class="panel-panel panel-right-1">
        <div class="inside"><?php print $content['right_1']; ?></div>
      </div>
      <div class="panel-panel panel-wide-1">
        <div class="inside"><?php print $content['wide_1']; ?></div>
      </div>
    </div>
    <div class="bottom-wrapper">
      <div class="panel-panel panel-left-2">
        <div class="inside"><?php print $content['left_2']; ?></div>
      </div>
      <div class="panel-panel panel-right-2">
        <div class="inside"><?php print $content['right_2']; ?></div>
      </div>
      <div class="panel-panel panel-wide-2">
        <div class="inside"><?php print $content['wide_2']; ?></div>
      </div>
    </div>
  </div>
</div>
