<?php

/**
 * @file
 * Plugin to provide Mobile Store Event.
 */

/**
 * Plugin definition.
 */
$plugin = array(
  'title' => t('Mobile Store Event'),
  'category' => t('Custom'),
  'icon' => 'mobile_store_event.png',
  'theme' => 'mobile_store_event',
  'css' => 'mobile_store_event.css',
  'regions' => array(
    'sidebar' => t('Sidebar'),
    'left_1' => t('1st Left'),
    'right_1' => t('1st Right'),
    'wide_1' => t('1st Wide'),
    'left_2' => t('2nd Left'),
    'right_2' => t('2nd Right'),
    'wide_2' => t('2nd Wide'),
  ),
);
