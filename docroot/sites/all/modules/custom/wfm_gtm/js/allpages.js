/**
 * Created by selwyn.polit on 8/4/15.
 *
 * Attach to Drupal behaviors.
 */

(function allPages($) {
  'use strict';

  var allPages = {};

  allPages.init = function() {
    // Get store info
    // Note. This could cause an async js call so I need to wrap it in
    // a callback so I have access to the store data to build my data object.
    allPages.getUserStoreInfo(allPages.processDataLayer);
  };

  /**
   * This is the callback for getUserStoreInfo, builds the event object
   * using the user's selected store information and pushes that into window.dataLayer.
   *
   * @param storeInfo
   *        user's store information from Drupal.settings.WholeFoods.stores
   */
  allPages.processDataLayer = function(storeInfo) {
    var obj = allPages.buildDataObject(storeInfo);
    if (obj && window.dataLayer){
      window.dataLayer.push(obj);
    }
  };

  /**
   * Gets the store information from the Drupal getStoreInfo method
   * and executes the registered callback.
   *
   * @param callback
   */
  allPages.getUserStoreInfo = function(callback) {
    var localStoreNid = Drupal.settings.WholeFoods.localStore;
    if (localStoreNid && (localStoreNid > 0)) {
      Drupal.WholeFoods.getStoreInfo(localStoreNid, function(userStore) {
        callback(userStore[localStoreNid]);
      });
    }
  };

  /**
   * Handles building the actual event object.
   *
   * @param   storeInfo
   * @returns obj
   */
  allPages.buildDataObject = function(storeInfo) {
    var obj = {};
    obj.event = 'dataLayer-loaded';

    // category for blogs is stored in Drupal.settings.Wholefoods.category var
    if (typeof Drupal.settings.WholeFoods.category !== "undefined") {
      obj.category = Drupal.settings.WholeFoods.category;
    }

    // Content Type is stored in Drupal.settings.Wholefoods.contentType var
    if (typeof Drupal.settings.WholeFoods.contentType !== "undefined") {
      obj.contentType = Drupal.settings.WholeFoods.contentType;
    }

    // JanRainUserId
    var janrainUserId = allPages.getJanrainUserId();
    //var janrainUserId = Drupal.settings.WholeFoods.user.janrain;
    if (janrainUserId) {
      obj.JanRainUserId = janrainUserId;
    }

    // Whole Foods Region
    // If you are on a store page, the region will be in the dom for you.
    // If you are on a services page, the region will be there also.
    if (typeof Drupal.settings.WholeFoods.pageStoreRegion !== "undefined") {
      obj.wholeFoodsRegion = Drupal.settings.WholeFoods.pageStoreRegion;
    }

    // User store #.
    if (storeInfo.tlc) {
      obj.userStoreNumber = storeInfo.tlc;
    }

    // User store name.
    if (storeInfo.storename) {
      obj.userStoreName = storeInfo.storename;
    }

    // TODO: contentCampaign huh?

    // TODO: annualProgramName huh?

    // recipeCategory
    // This will only appear if we are viewing a recipe node.
    // Global var inserted in wfm_gtm.module
    if (typeof recipeCategory !== 'undefined' && recipeCategory) {
      obj.recipeCategory = recipeCategory;
    }

    // StoreDepartment
    // This will only appear if we are viewing a department page for a store.
    // A variable is inserted in wfm_gtm.module
    if (typeof Drupal.settings.WholeFoods.storeDepartment !== "undefined") {
      obj.storeDepartment = Drupal.settings.WholeFoods.storeDepartment;
    }

    // TODO: missionsAndValues

    // pageStoreNumber
    // if viewing a store, the pageStoreTLC variable will be set.
    if (typeof Drupal.settings.WholeFoods.pageStoreTLC !== 'undefined') {
      obj.pageStoreNumber = Drupal.settings.WholeFoods.pageStoreTLC;
    }

    // Return the created object ready to push to the dataLayer
    return obj;
  };

  /**
   * Grab the Janrain user id.
   * This sometimes occurs at Drupal.settings.WholeFoods.user.janrain
   * If it doesn't use this function instead.
   * 
   * @returns {*}
   */
  allPages.getJanrainUserId = function() {
    var jsonString = localStorage.getItem('janrainCaptureProfileData');
    if (!jsonString) {
      return false;
    }
    var profileData = JSON.parse(jsonString);
    if (!profileData) {
      return false;
    }
    var uuid = profileData.uuid;
    if (!uuid) {
      return false;
    }
    return uuid;
  };

  // Attach to Drupal behaviors.
  Drupal.behaviors.wfm_gtm_coupon = {
    attach: function (context, settings) {
      $('body', context).once('allPages', function() {
        allPages.init();
      });
    }
  };

  // Executes when complete page is fully loaded, including all frames, objects and images.
  $(window).load(function() {
    // Wrapped in a try/catch to disregard any errors that occur if
    // the janrain module has not fully loaded in time.
    try {
      // Adding Janrain registration Event Handler.
      // This fires when a new user successfully registers.
      // Push the 'account-created' event into the dataLayer on successful registration.
      janrain.events.onCaptureRegistrationSuccess.addHandler(function() {
        if (window.dataLayer) {
          window.dataLayer.push({ event: 'account-created' });
        }
      });
    } catch(e) {}
  });
})(jQuery);
