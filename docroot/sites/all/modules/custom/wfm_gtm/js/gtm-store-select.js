(function wfm_gtm_store_select($) {
  'use strict';

  var obj = {};

  /**
   * Wrapper for the getStoreInfo function, returns the store information object.
   *
   * @param nid
   * @param callback
   */
  obj.getStore = function(nid, callback) {
    Drupal.WholeFoods.getStoreInfo(nid, function(stores) {
      callback(stores[nid]);
    });
  };

  /**
   * Uses storeInfo to build an event object and pushes that into window.dataLayer
   *
   * @param storeInfo
   *        return value from Drupal.WholeFoods.getStoreInfo, as seen in the getStore method.
   */
  obj.pushDataLayer = function(storeInfo) {
    if (window.dataLayer && storeInfo) {
      window.dataLayer.push({
        event: 'select-store',
        userLocation: storeInfo.location.statename,
        userStoreName: storeInfo.storename
      });
    }
  };

  /**
   * find the store nid that is nested in the form_values array.
   * The structure looks like this:
   *    [{ name: 'store', value: [nid] }, ...]
   *
   * @param formValues
   *    array of hidden fields attached to each ajax form
   * @returns {Number}|{Boolean}
   *    returns the store nid or false if it is not present
   */
  obj.findNid = function(formValues) {
    var nid = false;
    $.each(formValues, function(key, element) {
      // get nid from the hidden form element { name: 'store', value: [nid] }
      if (element.name == 'store') {
        nid = element.value;
      }
    });
    return nid;
  };

  /**
   * Attach a beforeSubmit event handler to each ajax form present on the page.
   *
   * @param settings
   */
  obj.ajaxEventHandler = function(settings) {
    // settings.ajax contains an array of form IDs for each ajax form on the page.
    for (var formId in settings.ajax) {
      // skip this formId if the property doesn't exist
      if (!settings.ajax.hasOwnProperty(formId)) {
        continue;
      }

      // add beforeSubmit handler for each ajax form on the page
      Drupal.ajax[formId].options.beforeSubmit = function(form_values, element, options) {
        var nid = obj.findNid(form_values);
        if (!nid) {
          return false;
        }

        // get the store info and push the event into dataLayer
        obj.getStore(nid, obj.pushDataLayer);
      };
    }
  };

  /**
   * Event handler for /stores/{store-name}
   */
  obj.individualStoreEventHandler = function() {
    var $form = $("form[id*='store-make-users-store']");
    // Specifically for /stores/{store-name}, there should only be one of these forms
    // so if there is more than 1, don't attach the event handler.
    if (!$form.length || $form.length > 1) {
      return false;
    }

    $form.submit(function() {
      var nid = $(this).find('[name="store"]').val();
      if (!nid) {
        return false;
      }

      //get the store info and push the event into dataLayer
      obj.getStore(nid, obj.pushDataLayer);
    });
  };

  /**
   * Event handler for store-select-form.
   * This includes the global footer, /coupons, /online-ordering, etc
   */
  obj.storeSelectFormEventHandler = function() {
    var $form = $('form.store-select-form');
    if (!$form.length) {
      return false;
    }

    $form.submit(function() {
      var nid = $(this).find('.store-select').val();
      if (!nid) {
        return false;
      }

      obj.getStore(nid, obj.pushDataLayer);
    });
  };

  /**
   * Attach to Drupal behaviors.
   */
  Drupal.behaviors.wfm_gtm_store_select = {
    attach: function (context, settings) {
      $('body', context).once('wfm_gtm_store_select', function () {
        // If there are ajax forms on the page, add event listeners for each one.
        if (settings.ajax) {
          obj.ajaxEventHandler(settings);
        }

        // handles the global footer, /online-ordering, /coupons, etc
        obj.storeSelectFormEventHandler();

        // handles the form on /stores/{store-name}
        obj.individualStoreEventHandler();
      });
    }
  };
})(jQuery);