(function wfm_gtm_recipe($) {
  'use strict';
  
  function recipeClickHandler() {
    var obj = recipe.buildDataObject();
    if (obj && window.dataLayer) {
      window.dataLayer.push(obj);
    }
  }

  try {
    var recipe = {};
    
    /**
     * Build an object for pushing into the datalayer
     */
    recipe.buildDataObject = function () {
      var recipeInfo = Drupal.settings.WholeFoods.recipe;

      // No valid recipe name, category or recipeType
      if (!recipeInfo.productTitle) {
        return false;
      }

      if (!recipeInfo.recipeCategory) {
        recipeInfo.recipeCategory = '';
      }

      if (!recipeInfo.course) {
        recipeInfo.course = '';
      }

      return {
        event: 'recipe-printed',
        recipeName: recipeInfo.productTitle,
        recipeCategory: recipeInfo.recipeCategory,
        recipeType: recipeInfo.course
      };
    };
  
    recipe.addEventListeners = function () {
      $('.recipe-print').click(recipeClickHandler);
    };
  
    /**
     * Attach to Drupal behaviors
     */
    Drupal.behaviors.wfm_gtm_recipe = {
      attach: function (context, settings) {
        $('body', context).once('wfm_gtm_recipe', function () {
          recipe.addEventListeners();
        });
      }
    };
  
  } catch (err) {
    console.log(err);
  }
})(jQuery);