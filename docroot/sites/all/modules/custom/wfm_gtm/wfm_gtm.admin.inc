<?php

/**
 * @file
 * Admin settings.
 */

/**
 * Page callback for Google Tag Manager configuration.
 */
function wfm_gtm_admin_settings() {
  $themes = list_themes();
  $modes = array();
  global $conf;
  foreach ($themes as $theme) {
    if ($theme->status === '1') {
      $modes[$theme->name] = $theme->info['name'];
    }
  }
  $envs = array(
    'prod' => t('Production'),
    'dev' => t('Development'),
  );
  $current_mode = $conf['theme_default'];
  $current_env = 'prod' == variable_get('site_env') ? 'prod' : 'dev';

  $form = array();
  $prefix = 'wfm_gtm_settings_';
  $form['keys'] = array(
    '#type' => 'value',
    '#value' => array(),
  );
  foreach ($modes as $m => $mode) {
    $form[$m] = array(
      '#type' => 'fieldset',
      '#title' => t($mode),
      '#collapsible' => TRUE,
      '#collapsed' => $current_mode != $m,
    );
    $display_key = $prefix . $m . '_display';
    $form['keys']['#value'][] = $display_key;
    $form[$m][$display_key] = array(
      '#type' => 'checkbox',
      '#title' => t('Display GTM in this theme?'),
      '#collapsible' => FALSE,
      '#default_value' => variable_get($display_key, 0)
    );
    foreach ($envs as $e => $env) {
      $form[$m][$e] = array(
        '#type' => 'fieldset',
        '#title' => t($e),
        '#collapsible' => TRUE,
        '#collapsed' => $current_env != $e,
      );
      $key = $prefix . $m . '_' . $e;
      $form['keys']['#value'][] = $key;
      $form[$m][$e][$key] = array(
        '#type' => 'textfield',
        '#title' => t('Google Tag Manager ID for %env %mode site', array(
          '%env' => $env,
          '%mode' => $mode,
        )),
        '#default_value' => variable_get($key, ''),
        '#size' => 16,
      );
    }
  }
  $form['#submit'][] = 'wfm_gtm_admin_settings_submit';

  return system_settings_form($form);
}


/**
 * Submission handler for wfm_gtm_admin_settings form.
 */
function wfm_gtm_admin_settings_submit($form, &$form_state) {
  foreach ($form_state['values']['keys'] as $key) {
    $form_state['values'][$key] = check_plain($form_state['values'][$key]);
  }
}
