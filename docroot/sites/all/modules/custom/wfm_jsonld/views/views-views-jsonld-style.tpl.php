<?php

/**
 * @file
 * Default template for the Views JSON_LD style plugin using the simple format
 *
 * Variables:
 * - $view: The View object
 * - $features: Features data to be serialized as GeoJSON
 * - $options: Array of options for this style
 *
 * @ingroup views_templates
 */

?>
<?php if ($view->override_path): ?>
  <?php print "<code>" . json_encode($features) . "</code>"; ?>
<?php else: ?>
  <?php print json_encode($features); ?>
<?php endif; ?>
