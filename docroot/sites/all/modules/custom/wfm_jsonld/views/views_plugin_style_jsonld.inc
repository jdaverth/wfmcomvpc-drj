<?php

/**
 * @file
 * Views style plugin to render node fields in the JSON_LD format.
 */

/**
 * Implementation of views_plugin_style.
 */
class views_plugin_style_jsonld extends views_plugin_style {
  /**
   * Set default options.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['attributes'] = array('default' => NULL, 'translatable' => FALSE);
    $options['jsonld_context'] = array('default' => NULL, 'translatable' => FALSE);
    $options['jsonld_type'] = array('default' => 'local_business', 'translatable' => FALSE);
    return $options;
  }

  /**
   * Provide a form for setting options.
   *
   * @param array $form
   *   Array of the form.
   * @param array $form_state
   *   Array of the form state.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Attributes description.
    $form['jsonld_context'] = array(
      '#type' => 'textfield',
      '#title' => t('JSON_LD context'),
      '#default_value' => $this->options['jsonld_context'],
      '#description' => t('JSON_LD context link'),
    );
    $form['jsonld_type'] = array(
      '#type' => 'select',
      '#title' => t('JSON_LD Type'),
      '#options' => array(
        'Article' => t('Article'),
        'Blog' => t('Blog'),
        'Event' => t('Event'),
        'LocalBusiness' => t("LocalBusiness"),
        'Organization' => t("Organization"),
        'Product' => t('Product'),
        'Recipe' => t('Recipe'),
      ),
      '#default_value' => $this->options['jsonld_type'],
      '#description' => t('The @type attribute in JSON_LD output')
    );
  }

  /**
   * Implementation of view_style_plugin::theme_functions(). 
   * 
   * Returns an array of theme functions to use for the current style plugin.
   * 
   * @return array
   *   List of theme function names.
   */
  public function theme_functions() {
    return views_theme_functions('views_views_jsonld_style', $this->view, $this->display);
  }

  /**
   * Implementation of views_style_plugin::additional_theme_functions().
   * 
   * @return array
   *   Returns empty array.
   */
  public function additional_theme_functions() {
    return array();
  }

  /**
   * Implementation of view_style_plugin::render().
   */
  public function render() {
    $view = $this->view;
    $options = $this->options;
    $field = $view->field;
    $display = $this->display;

    $features = array();
    foreach ($view->result as $count => $row) {
      $features[] = _views_jsonld_render_fields($view, $row, $display);
    }
    unset($view->row_index);

    $jsonld_context = $options['jsonld_context'];
    $jsonld_type = $options['jsonld_type'];
    if ($jsonld_type) {
      $jsonld_options = array(
        '@type' => $options['jsonld_type'],
      );
      foreach ($features as $key => $value) {
        $features[$key] = $jsonld_options + $value;
      }
    }

    if (count($features) == 1) {
      $features = array_pop($features);
    }
    else {
      $features = array(
        '@type' => 'ItemList',
        'itemListElement' => $features
      );
    }

    if ($jsonld_context) {
      $features = array('@context' => $jsonld_context) + $features;
    }
    return theme($this->theme_functions(), array(
      'view' => $view, 
      'options' => $options, 
      'features' => $features
    ));
  }
}
