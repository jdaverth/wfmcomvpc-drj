<?php
/**
 * @file
 * Views plugin definition.
 */


/**
 * Implements hook_views_plugins().
 */
function wfm_jsonld_views_plugins() {
  $path = drupal_get_path('module', 'wfm_jsonld');
  $plugins = array(
    'style' => array(
      'jsonld' => array(
        'title' => t('JSON_LD Output'),
        'help' => t('Displays node fields in JSON_LD format'),
        'handler' => 'views_plugin_style_jsonld',
        'theme' => 'views_views_jsonld_style',
        'theme path' => $path . '/views',
        'path' => $path . '/views',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'even empty' => TRUE,
      ),
    ),
  );

  return $plugins;
}
