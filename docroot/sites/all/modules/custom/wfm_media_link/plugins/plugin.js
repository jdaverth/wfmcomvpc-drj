(function($) {
  // Get a CKEDITOR.dialog.contentDefinition object by its ID.
  var getById = function(array, id, recurse) {
    for (var i = 0, item; (item = array[i]); i++) {
      if (item.id == id) return item;
      if (recurse && item[recurse]) {
        var retval = getById(item[recurse], id, recurse);
        if (retval) return retval;
      }
    }
    return null;
  };

  var extractPath = function(value) {
    var match = /^[^#]*?:\/\/.*?(\/.*)$/i.exec(value);
    if (match && match[1]) {
      return match[1];
    }
    return false;
  };

  CKEDITOR.plugins.add('wfm_media_link', {

    init: function(editor, pluginPath) {
      CKEDITOR.on('dialogDefinition', function(e) {
        if ((e.editor != editor) || (e.data.name != 'link')) return;

        // Add media button
        var infoTab = e.data.definition.getContents('info');
        var content = getById(infoTab.elements, 'urlOptions');
        content.children.push({
          type: 'button',
          label: Drupal.settings.wfmMediaLink.buttonLabel,
          id: 'wfm_media_link_button',
          onClick: function() {

            // update CKEditor popups z-indexes to move media browser modal to the top
            $('.cke_dialog_background_cover').css('z-index', '90');
            $('table.cke_dialog').css('z-index', '95');

            // open media browser and update CKE dialog values if item selected
            var dialog = this.getDialog();
            Drupal.media.popups.mediaBrowser(function(media) {
              if (media && media.length) {
                var item = media.shift();
                dialog.setValueOf('info', 'protocol', '');
                dialog.setValueOf('info', 'url', extractPath(item.url));
              }
            });
          }
        });
      });
    }
  });
})(jQuery);