<?php

/**
 * @file
 * Custom plugin Recipe Back widget.
 */

$plugin = array(
  'title' => t('Recipe Back'),
  'description' => t('Recipe Back widget'),
  'single' => TRUE,
  'content_types' => array('recipe_back'),
  'render callback' => 'wfm_mobile_recipe_back_ct_render',
  'edit form' => 'wfm_mobile_recipe_back_ct_edit_form',
  'category' => array(t('Custom'), 0),
);


/**
 * Render function.
 */
function wfm_mobile_recipe_back_ct_render($subtype, $conf) {
  $block = new stdClass();
  $block->title = '';
  $block->content = user_is_logged_in() && isset($_GET['recipebox']) ? l(t('back to My Recipe Box'), 'myrecipebox') : '';
  return $block;
}


/**
 * Settings function.
 */
function wfm_mobile_recipe_back_ct_edit_form($form) {
  return $form;
}
