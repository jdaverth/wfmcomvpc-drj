<?php

/**
 * @file
 * Custom plugin Recipe Box widget.
 */

$plugin = array(
  'title' => t('Recipe Box'),
  'description' => t('Recipe Box widget'),
  'single' => TRUE,
  'content_types' => array('recipe_box'),
  'render callback' => 'wfm_mobile_recipe_box_ct_render',
  'edit form' => 'wfm_mobile_recipe_box_ct_edit_form',
  'category' => array(t('Custom'), 0),
);


/**
 * Render function.
 */
function wfm_mobile_recipe_box_ct_render($subtype, $conf) {
  $block = new stdClass();
  $block->title = t('My Recipe Box');
  $data = wfm_mobile_recipe_box_get();
  if ($data) {
    $content = '';
    foreach ($data as $cat => $recipes) {
      $content .= '<div class="panel-pane collapsible collapsed"><h2 class="pane-title">' . $cat . '</h2>'
        . '<div class="pane-content">';
      $items = array();
      foreach ($recipes as $recipe) {
        $items[] = l(t('Remove'), 'myrecipebox/remove/' . $recipe['nid'], array(
          'query' => array(drupal_get_destination()),
          'attributes' => array('class' => array('remove-recipe')),
        )) . l($recipe['title'], 'node/' . $recipe['nid'], array(
          'query' => array('recipebox' => NULL),
          'attributes' => array('class' => array('recipe-link')),
        ));
      }
      $content .= theme('item_list', array('items' => $items)) . '</div></div>';
    }
    ctools_add_js('recipe_box', 'wfm_mobile_recipe_box', 'plugins/content_types/recipe_box');
  }
  else {
    $content = t('You have no saved recipes. To save a recipe, click the "Add to My Recipe Box" button on any recipe detail page.');
  }
  $block->content = $content;
  return $block;
}


/**
 * Settings function.
 */
function wfm_mobile_recipe_box_ct_edit_form($form) {
  return $form;
}
