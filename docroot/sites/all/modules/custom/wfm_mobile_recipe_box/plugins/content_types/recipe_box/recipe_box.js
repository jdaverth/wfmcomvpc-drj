(function($) {
  $(function() {
    var title = Drupal.t('Whole Foods Market') + '\n\n';
    $('.pane-recipe-box a.remove-recipe').click(function() {
      return confirm(title + Drupal.t('Are you sure you want to delete "!name" recipe from your Recipe Box?', {
        '!name': $(this).next().text()
      }));
    });
  });
})(jQuery);
