<?php

/**
 * @file
 * Custom plugin Recipe Button widget.
 */

$plugin = array(
  'title' => t('Recipe Button'),
  'description' => t('Recipe Button widget'),
  'single' => TRUE,
  'content_types' => array('recipe_button'),
  'render callback' => 'wfm_mobile_recipe_button_ct_render',
  'required context' => new ctools_context_required(t('Node'), 'node'),
  'edit form' => 'wfm_mobile_recipe_button_ct_edit_form',
  'category' => array(t('Custom'), 0),
);


/**
 * Render function.
 */
function wfm_mobile_recipe_button_ct_render($subtype, $conf, $args, $context) {
  if ($context && 'recipe' == $context->data->type) {
    $id = $context->data->nid;
    $in_list = wfm_mobile_recipe_box_in_list($id);
    $params = array(
      'query' => array(drupal_get_destination()),
      'attributes' => array('class' => array('button')),
    );
    if ($in_list) {
      $content = l(t('Remove from My Recipe Box'), 'myrecipebox/remove/' . $id, $params);
    }
    else {
      $content = l(t('Add to My Recipe Box'), 'myrecipebox/add/' . $id, $params);
    }
    $block = new stdClass();
    $block->title = '';
    $block->content = $content;
    return $block;
  }
}


/**
 * Settings function.
 */
function wfm_mobile_recipe_button_ct_edit_form($form) {
  return $form;
}
