(function($) {
  $(function() {
    var title = Drupal.t('Whole Foods Market') + '\n\n';
    var addItem = $('#edit-item-add a');
    addItem.click(function() {
      var self = $(this);
      $('#edit-new').slideToggle(function() {
        var text = self.text();
        self.text(('+' === text[0] ? '−' : '+') + text.substr(1));
      });
      return false;
    });
    $('#edit-new .error').length && addItem.click();
    var order = {
      date: [],
      cat: []
    };
    $('#edit-list-items > div').each(function() {
      $('.toggle', $(this)).click(function() {
        var target = $(this)
          , parent = target.parent()
          , content = $('.form-edit', parent);
        content.slideToggle(function() {
          target.hasClass('collapsed') ? target.text('+') : target.text('−');
        });
        target.toggleClass('expanded collapsed');
        return false;
      }).appendTo(this);
      var error = $('.error', this);
      error.length && error.closest('.form-wrapper').siblings('.toggle').click();
      order.date.push($(this));
      order.cat.push($(this));
      var target = $('#edit-item-sort');
      $('a', target).click(function() {
        if ($(this).hasClass('cat')) {
          var show = 'date';
          var hide = 'cat';
        }
        else {
          show = 'cat';
          hide = 'date';
        }
        $('.' + hide, target).hide();
        $('.' + show, target).show();
        order[hide].forEach(function(item) {
          item.appendTo('#edit-list-items');
        });
        return false;
      });
      $('.form-edit .item-delete', $(this)).click(function() {
        var name = $.trim($(this).parent().siblings('.form-type-checkbox').find('label').text());
        return confirm(title + Drupal.t('Are you sure you want to delete "!name" list item?', {
          '!name': name
        }));
      });
    });
    order.cat.sort(function(a, b) {
      return a.attr('order-cat') - b.attr('order-cat');
    });
    $('#list-delete').click(function() {
      var name = $('.pane-shopping-list-details h2.pane-title').text();
      return confirm(title + Drupal.t('Are you sure you want to delete "!name" shopping list?', {
        '!name': name
      }));
    });
    $('#edit-delete-items-top, #edit-delete-items-bottom').click(function() {
      var items = [];
      $('#edit-list-items .form-type-checkbox input:checked').next().each(function() {
        items.push('\n  - ' + $(this).text());
      });
      if (!items.length) {
        alert(title + Drupal.t('There are no selected items to delete.'));
        return false;
      }
      return confirm(title + Drupal.t('Are you sure you want to delete the following list items?') + items.join(''));
    });
  });
})(jQuery);
