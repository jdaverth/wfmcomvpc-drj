<?php

/**
 * @file
 * Custom plugin Shopping Lists.
 */

$plugin = array(
  'title' => t('Shopping Lists'),
  'description' => t('Shopping Lists widget'),
  'single' => TRUE,
  'content_types' => array('shopping_lists'),
  'render callback' => 'wfm_mobile_shopping_lists_ct_render',
  'edit form' => 'wfm_mobile_shopping_lists_ct_edit_form',
  'category' => array(t('Custom'), 0),
);

/**
 * Render function.
 */
function wfm_mobile_shopping_lists_ct_render($subtype, $conf) {
  $block = new stdClass();
  $block->title = '';
  $content = drupal_get_form('wfm_mobile_shopping_lists_ct_create_list_form');
  $block->content = drupal_render($content);
  return $block;
}

/**
 * Settings function.
 */
function wfm_mobile_shopping_lists_ct_edit_form($form) {
  return $form;
}

/**
 * Form constructor for Create Shopping List form.
 */
function wfm_mobile_shopping_lists_ct_create_list_form($form, &$form_state) {
  $form['new_list'] = array(
    '#type' => 'container',
    'name' => array(
      '#type' => 'textfield',
      '#title' => t('Name of Shopping List'),
      '#default_value' => '',
      '#maxlength' => 128,
      '#required' => TRUE,
    ),
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Add List'),
    ),
  );

  $form['items'] = array(
    '#type' => 'container',
  );

  $lists = wfm_mobile_shopping_lists_get_lists();
  foreach ($lists as $list) {
    $form['items']['item_' . $list->_id] = array(
      '#markup' => '<div class="list-row">' . l($list->name, current_path() . '/' . $list->_id)
      . '<span class="list-link">' . l('>', current_path() . '/' . $list->_id) . '</span></div>',
    );
  }

  return $form;
}

/**
 * Submission handler for wfm_mobile_shopping_lists_ct_create_list_form().
 */
function wfm_mobile_shopping_lists_ct_create_list_form_submit($form, &$form_state) {
  $id = wfm_mobile_shopping_lists_create_list($form_state['values']['name']);
  if ($id) {
    drupal_goto(current_path() . '/' . $id);
  }
}
