/**
 * @file
 * Add Masonry to Drupal.behaviors object.
 */

(function ($) {
  Drupal.behaviors.wfm_pinterest_masonry = {
    attach: function (context, settings) {  
      $('body').once('masonry', function(){
        var container = document.querySelector('#masonry');
        var msnry;
        var i = 0;
        $('#pins img').load(function(){
          msnry = new Masonry( container, {
            itemSelector: '.item', 
          });    
        });
      });
    }
  };
})(jQuery);