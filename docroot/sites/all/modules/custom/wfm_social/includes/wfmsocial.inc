<?php
/**
 * @file
 * Define the base class for wfm_social classes, Facebook and Twitter.
 */

/**
 * The base class for wfm_social classes.
 *
 * This class contains methods useful to any social class.
 */
abstract class WFMSocial {
  /**
   * Get the posts for this social object.
   *
   * Each sub-class needs to define how this method works.
   *
   * @param int $limit
   *   A positive integer representing the number of posts to return.
   *
   * @return array
   *   An array of posts objects equal to $limit if available.
   */
  public function getPosts($limit = 3, $store = NULL, $exclusive = FALSE) {
  }

  /**
  * Find urls in text change change them into html links.
  *
  * @param string $string
  *   String to search for urls. 
  *
  * @return string
  *   String with html links added if urls were found.
  */
  public function makeLinks($string) {
    $regex = '@(https?://([-\w\.]+)+(:\d+)?(/([\w/_\.]*(\?\S+)?)?)?)@'; 
    $replacement = '<a rel="nofollow" href="$1">$1</a>'; 
    $string = preg_replace($regex, $replacement, $string); 
    return $string;
  }

  /**
  * Find @username Twitter style usernames in text and create html links.
  *
  * @param string $string
  *   String to search for usernames. 
  *
  * @return string
  *   String with html links added if usernames were found.  
  */
  public function linkTwitterUser($string) {
    $regex = '/(?<=^|(?<=[^a-zA-Z0-9-_\.]))@([A-Za-z]+[A-Za-z0-9_]+)/i'; 
    $replacement = '<a rel="nofollow" href="http://twitter.com/$1">@$1</a>'; 
    $string = preg_replace($regex, $replacement, $string); 
    return $string;
  }  

  /**
  * Find @username Twitter style hashtags in text and create html links.
  *
  * @param string $string
  *   String to search for hashtags. 
  *
  * @return string
  *   String with html links added if hashtags were found.  
  */
  public function linkTwitterHashtag($string) {
    $regex = '/(?<=^|(?<=[^a-zA-Z0-9-_\.]))#([A-Za-z]+[A-Za-z0-9_]+)/i'; 
    $replacement = '<a rel="nofollow" href="https://twitter.com/search?q=%23$1&src=hash">#$1</a>'; 
    $string = preg_replace($regex, $replacement, $string); 
    return $string;
  }
  
  public function formatBodyText($string) {
    $string = $this->makeLinks($string);
    $string = $this->linkTwitterUser($string);
    $string = $this->linkTwitterHashtag($string);
    return $string;
  }

  /**
  * Trim text to defined length.  Default value of 150.
  *
  * @param string $string
  *   String to trim to length.
  *
  * @param int $length
  *   String to trim to length.
  *  
  * @return string
  *   String trimmed to length, if initally longer than length.  
  */
  public function trimMessage($string, $length=150) {
    if (strlen($string) > $length) {
      $string = substr($string, 0, $length) . '...';
    }
    return $string;
  }

  /**
  * Function for Facebook like times: '3 weeks ago'
  *
  * Note: The separate array with plurals is for multilingual purposes (eg. in Dutch it's 'minuut' and 'minuten').
  *
  * @param integer $timestamp
  * @return string
  */
  function formatTime($timestamp) {
    if(!is_numeric($timestamp)) {
      $timestamp = strtotime($timestamp);
    }
    // Configuration
    $singular = array( 's' , 'm' , 'h' , 'd' , 'w' , 'month' , 'y' , 'decade'  );
    $plural = array( 's', 'm', 'h', 'd', 'w', 'months', 'y', 'decades' );
    $now = 'now';
 
    // Prepare
    $time = time();
    $difference = $time - $timestamp;
    $lengths = array( 1, 60, 3600, 86400, 604800, 2630880, 31570560, 315705600 );
 
    // Calculate lowest dividable amount
    for( $i = 7; $i >= 0 && ( $amount = $difference / $lengths[$i] ) <= 1; --$i );
 
    // Now or in the future
    if( $amount <= 0 ) { 
      return $now;
    }

    // Return as string
    $amount = floor( $amount );
 
    return $amount . ( $amount > 1 ? $plural[$i] : $singular[$i] );
  }

  /**
   * Get a cached value.
   *
   * @param string $cid
   *   The Cache ID of the cached object.
   *
   * @return bool
   *   FALSE if no cached object with $cid exists or the object is invalid.
   *
   * @return object
   *   The cached object with id of $cid.
   */
  static protected function _getCache($cid) {
    $cache = cache_get($cid);

    if ($cache == FALSE) {
      return FALSE;
    }

    if ($cache->expire == CACHE_PERMANENT
      || $cache->expire == CACHE_TEMPORARY
      || $cache->expire > REQUEST_TIME) {
      return $cache->data;
    }

    return FALSE;
  }

  /**
   * Cache an object in Drupal's temporary cache bin 'cache'.
   *
   * This is really just a wrapper around Drupal's built int cache_set
   * function, with $expire defaulting to CACHE_TEMPORARY.
   *
   * @param string $cid
   *   A unique cache id to retrieve the cahced objet.
   *
   * @param mixed $object
   *   The object to be stored.
   *
   * @param string $expire
   *   A Unix Timestamp of when the cache expires. Defaults to CACHE_TEMPORARY.
   */
  static protected function _setCache($cid, $object, $expire = CACHE_TEMPORARY) {
    cache_set($cid, $object, 'cache', $expire);
  }

  /**
   * Expire a cached object.
   *
   * This will forcefully expire a cached object. After calling this method,
   * there will be no cached object for $cid.
   *
   * @param string $cid
   *   The Cache ID of the cached object.
   */
  static protected function _expireCache($cid) {
    cache_set($cid, NULL, 'cache', REQUEST_TIME - 1);
  }

  /**
   * Retrieve the stored ID for the social object from store or global node.
   *
   * This method will do the logic of getting the value for the social obejct
   * off the store, or the global store node.
   *
   * In the event the local store doesn't have a value, the global value is
   * looked for. If there is no global value, an exception is thrown.
   *
   * @param object $store
   *   The node of the store (global or local) we're looking up.
   *   NULL may be passed, and the method will look for the local store of
   *   the current user.
   *
   * @param bool $exclusive
   *   TRUE or FALSE if we're looking for JUST the $store, or if we can look up
   *   the chain at global values as well.
   *
   * @param string $field
   *   The name of the node field where the social value is stored.
   *   (ie, store_field_facebook).
   *
   * @param string $value
   *   The name of the value column for $field. Defaults to 'value'.
   *
   * @return string
   *   The social ID value. (ie, WholeFoods).
   *
   * @throws Exception
   *   No value was found.
   */
  protected function _getStoreFieldValue($store, $exclusive, $field, $value = 'value') {
    if ($store == NULL) {
      $store = store_get_user_store();
    }

    $global_store = wholefoods_get_global_node(TRUE);
    $national_store = wholefoods_get_global_node();

    if ($store == NULL && $exclusive == FALSE) {
      $store = $national_store;
    }

    if (!isset($store->{$field}[LANGUAGE_NONE][0][$value]) &&
        $exclusive == FALSE) {
      if ($store != $national_store && $store != $global_store) {
        $store = $national_store;
      }

      if ($store != $global_store &&
          !isset($store->{$field}[LANGUAGE_NONE][0][$value])) {
        $store = $global_store;
      }
      else {
      }
    }

    if (!isset($store->{$field}[LANGUAGE_NONE][0][$value])) {
      throw new Exception('No stored socal value.');
    }

    $value = $store->{$field}[LANGUAGE_NONE][0][$value];
    return $value;
  }

  /**
   * Build a cache id with a prefix specific to this module.
   *
   * @param string $store_id
   *   A unique ID for the store.
   *
   * @param string $label
   *   A unique label for this cache objet (ie, twitter_posts).
   *
   * @return string
   *   A unique CID based on $label and $store_id.
   */
  protected function _buildCid($store_id, $label) {
    $cid = 'WFM_Social__' . $label . '_' . $store_id;
    return $cid;
  }

  /**
   * Build a cache id with a prefix specific to this module and marked as Time.
   *
   * @param string $store_id
   *   A unique ID for the store.
   *
   * @param string $label
   *   A unique label for this cache objet (ie, twitter_posts).
   *
   * @return string
   *   A unique CID based on $label and $store_id with a 'Time' marker.
   */
  protected function _buildTCid($store_id, $label) {
    $cid = $this->_buildCid($store_id, $label);
    $time_cid = $cid . '__Time';

    return $time_cid;
  }
}
