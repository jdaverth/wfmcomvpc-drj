<?php

require_once('wfmsocial.inc');
require_once('twitteroauth/twitteroauth.php');

class WFMTwitter extends WFMSocial {
  /**
  * Get store twitter account name.
  *
  * @param object $store
  *   The node of the store (global or local) we're looking up.
  *   NULL may be passed, and the method will look for the local store of
  *   the current user.
  *
  * @param bool $exclusive
  *   TRUE or FALSE if we're looking for JUST the $store, or if we can look up
  *   the chain at global values as well.
  *
  * @return string
  *   twitter username
  */
  private function _getStoreId($store, $exclusive = TRUE) {
    try {
      return $this->_getStoreFieldValue($store, $exclusive, 'field_store_twitter');
    }
    catch (Exception $e) {
      throw $e;
    }
  }

  /**
   * Get store twitter account name.
   *
   * @param int $limit
   *   Maximum number of posts returned.
   *
   * @param object $store
   *   The node of the store (global or local) we're looking up.
   *   NULL may be passed, and the method will look for the local store of
   *   the current user.
   *
   * @param bool $exclusive
   *   TRUE or FALSE if we're looking for JUST the $store, or if we can look up
   *   the chain at global values as well.
   * @return array|null
   */
  public function getPosts($limit = 3, $store = NULL, $exclusive = FALSE) {
    $connection = $this->getTwitterConnection();
    try {
      $store_id = $this->_getStoreId($store, $exclusive);
    }
    catch (Exception $e) {
      return NULL;
    }

    $cid = $this->_buildCid($store_id, 'Twitter');
    $cid_time = $this->_buildTCid($store_id, 'Twitter');

    $cached = $this->_getCache($cid);
    $cached_time = $this->_getCache($cid_time);

    if ($cached == NULL || $cached_time < REQUEST_TIME) {
      $url = TWITTER_JSON_URL . '?screen_name=' . $store_id . '&count=' . $limit;
      $data = $connection->get($url);
      if (!$data) {
        watchdog('Twitter Feed', '(' . $store_id . ') file_get_contents Error');
        if ($cached !== NULL) {
          $decoded_data = $cached;
        }
      }
      else {
        $this->_setCache($cid, $data);
        $this->_setCache($cid_time, REQUEST_TIME + 3600);
      }
    }
    else {
      $data = $cached;
    }
    if (is_array($data)) {
      $count = count($data);
      for ($i = 0; $i < $count; $i++) {
        $data[$i]->text = $this->formatBodyText($data[$i]->text);
        $data[$i]->timeago = $this->formatTime($data[$i]->created_at);
        if(isset($data[$i]->retweeted_status)) {
          $data[$i]->retweeted_status->text = $this->formatBodyText($data[$i]->retweeted_status->text);
        }
      }
      return $data;
    }
    return NULL;
  }

  /**
  * Get Twitter access token.
  *
  * @param string $key
  *   Twitter application key.
  *
  * @param string $secret
  *   Twitter application secret.
  *  
  * @param string $oauth_token
  *   Twitter OAuth token.  
  *
  * @param string $oauth_token_secret
  *   Twitter OAuth token secret. 
  *  
  * @return object
  *   Object with Twitter API connection variables. 
  */
  public function getTwitterConnection($key = NULL, $secret = NULL, $oauth_token = NULL, $oauth_token_secret = NULL){
    if ($key === NULL) {
      $key = variable_get('wfm_variable__social_twitter_consumer_key');
    }
    if ($secret === NULL) {
      $secret = variable_get('wfm_variable__social_twitter_consumer_secret');
    }
    if ($oauth_token === NULL) {
      $oauth_token = variable_get('wfm_variable__social_twitter_oauth_access_token');
    }
    if ($oauth_token_secret === NULL) {
      $oauth_token_secret = variable_get('wfm_variable__social_twitter_oauth_access_token_secret');
    }
    if ($connection = new TwitterOAuth($key, $secret, $oauth_token, $oauth_token_secret)) {
      return $connection;
    }
    else {
      watchdog('Twitter Feed', 'Error getting Authentication token, function getTwitterConnection.');
    }
  }
} 