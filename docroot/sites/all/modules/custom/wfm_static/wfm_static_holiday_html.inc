<?php
/**
* @file
* Nav, header, & footer elements for holiday 2013 section.
*/

/**
* Returns HTML for Holiday 2013 Nav 
*/
function _wfm_static_holiday_nav_html() {
  $ts = 1385701200; //Friday, November 29th 2013, 00:00:00 (GMT -5), the day after Thanksgiving
  if (time() < $ts) {
    $holidaymenu = '  
    <li class="column">
      <h3><a href="/bestholidayever/thanksgiving-menus">' . t('Thanksgiving Menus') . '</a></h3>
      <ul>
        <li><a href="/bestholidayever/thanksgiving-menus#classic-thanksgiving-dinner">' . t('Classic Thanksgiving Dinner') . '</a></li>
        <li><a href="/bestholidayever/thanksgiving-menus#small-gathering">' . t('Small Gathering') . '</a></li>
        <li><a href="/bestholidayever/thanksgiving-menus#vegetarian-feast">' . t('Vegetarian Feast') . '</a></li>
        <li><a href="/bestholidayever/thanksgiving-menus#vegan-feast">' . t('Vegan Feast') . '</a></li>
        <li><a href="/bestholidayever/thanksgiving-menus#simplest-celebratory-supper">' . t('Simplest Celebratory Supper') . '</a></li>
        <li><a href="/bestholidayever/thanksgiving-menus#unexpected-thanksgiving">' . t('Unexpected Thanksgiving') . '</a></li>
      </ul>
    ';
  }
  else {
    $holidaymenu = '  
    <li class="column">
      <h3><a href="/bestholidayever/christmas-party-ideas">Christmas Party Ideas</a></h3>
      <ul class="menus">
        <li><a href="/bestholidayever/christmas-party-ideas#christmas-morning-brunch">' . t('Christmas Morning Brunch') . '</a></li>
        <li><a href="/bestholidayever/christmas-party-ideas#classic-holiday-dinner">' . t('Classic Holiday Dinner') . '</a></li>
        <li><a href="/bestholidayever/christmas-party-ideas#simplest-celebratory-dinner">' . t('Simplest Celebratory Dinner') . '</a></li>
        <li><a href="/bestholidayever/christmas-party-ideas#new-years-eve-cocktail-party">' . t('New Years Eve Cocktail Party') . '</a></li>
        <li><a href="/bestholidayever/christmas-party-ideas#vegetarian-feast">' . t('Vegetarian Feast') . '</a></li>
        <li><a href="/bestholidayever/christmas-party-ideas#vegan-feast">' . t('Vegan Feast') . '</a></li>
      </ul>
    ';
  }

  $html = ' 
  <div id="holiday-nav">
  <div id="holiday-nav-head">
    <h2><a href="/bestholidayever"><span></span>' . t('Holiday Cheat Sheet') . '</a></h2><a href="#" class="togglenav">' . t('Jump To') . '<span></span></a>
  </div>
  <ul class="topmenu clearfix">
    <li class="column">
    <h3><a href="/bestholidayever/our-favorite-recipes/main-courses">' . t('Our Favorite Recipes') . '</a></h3>
    <ul class="recipes">
      <li><a href="/bestholidayever/our-favorite-recipes/appetizers">' . t('Appetizers') . '</a></li>
      <li><a href="/bestholidayever/our-favorite-recipes/side-dishes">' . t('Side Dishes') . '</a></li>
      <li><a href="/bestholidayever/our-favorite-recipes/potatoes-and-sweet-potatoes">' . t('Potatoes &amp; Sweet Potatoes') . '</a></li>
      <li><a href="/bestholidayever/our-favorite-recipes/main-courses">' . t('Main Courses') . '</a></li>
      <li><a href="/bestholidayever/our-favorite-recipes/breads-muffins-rolls">' . t('Breads, Muffins &amp; Rolls') . '</li>
      <li><a href="/bestholidayever/our-favorite-recipes/cookies">' . t('Cookies') . '</a></li>
      <li><a href="/bestholidayever/our-favorite-recipes/pies">' . t('Pies') . '</a></li>
      <li><a href="/bestholidayever/our-favorite-recipes/desserts">' . t('Desserts') . '</a></li>
      <li><a href="/bestholidayever/our-favorite-recipes/vegetarian">' . t('Vegetarian') . '</a></li>
      <li><a href="/bestholidayever/our-favorite-recipes/vegan">' . t('Vegan') . '</a></li>
      <li><a href="/bestholidayever/our-favorite-recipes/gluten-free">' . t('Gluten Free') . '</a></li>
      <li><a href="/bestholidayever/our-favorite-recipes/healthy">' . t('Healthy') . '</a></li>
      <li><a href="/bestholidayever/our-favorite-recipes/seasonal-favorites">' . t('Seasonal Favorites') . '</a></li>
      <li><a href="/bestholidayever/our-favorite-recipes/hanukkah">' . t('Hanukkah') . '</a></li>
      <li><a href="/bestholidayever/our-favorite-recipes/leftovers">' . t('Leftovers') . '</a></li>
    </ul>
  </li><!--column-->';

    $html .= $holidaymenu;

    $html .= '
    <h3><a href="/bestholidayever/holiday-baking/baking-guide">' . t('Holiday Baking') . '</a></h3>
    <ul>
      <li><a href="/bestholidayever/holiday-baking/top-ten-baking-tips">' . t('Top 10 Baking Tips') . '</a></li>
      <li><a href="/bestholidayever/holiday-baking/easy-substitutions">' . t('Easy Substitutions') . '</a></li>
      <li><a href="/bestholidayever/holiday-baking/natural-sweeteners">' . t('Natural Sweeteners') . '</a></li>
    </ul>
    <h3><a href="/bestholidayever/videos">' . t('Holiday Videos') . '</a></h3>
  </li><!--column-->  
  
  <li class="column">
    <h3><a href="/bestholidayever/guide-to-turkey/">' . t('Turkey 101') . '</a></h3>
    <ul class="turkey">
      <li><a href="/bestholidayever/guide-to-turkey/turkey-tips">' . t('Turkey Tips') . '</a></li>
      <li><a href="/bestholidayever/guide-to-turkey/buying-turkey">' . t('Buying a Turkey') . '</a></li>
      <li><a href="/bestholidayever/guide-to-turkey/brining-turkey">' . t('Brining a Turkey') . '</a></li>
      <li><a href="/bestholidayever/guide-to-turkey/roasting-turkey">' . t('Roasting a Turkey') . '</a></li>
      <li><a href="/bestholidayever/guide-to-turkey/carving-turkey">' . t('Carving a Turkey') . '</a></li>
      <li><a href="/bestholidayever/servings-calculator">' . t('Servings Calculator') . '</a></li>
    </ul>
    
    <h3><a href="/bestholidayever/beer-wine-cheese/">' . t('Beer, Wine &amp; Cheese') . '</a></h3>
    <ul>
      <li><a href="/bestholidayever/beer-wine-cheese/our-favorite-beers">' . t('Our Favorite Beers') . '</a></li>
      <li><a href="/bestholidayever/beer-wine-cheese/our-favorite-wines">' . t('Our Favorite Wines') . '</a></li>
      <li><a href="/bestholidayever/beer-wine-cheese/our-favorite-cheeses">' . t('Our Favorite Cheeses') . '</a></li>
      <li><a href="/bestholidayever/beer-wine-cheese/wine-101">' . t('Wine 101') . '</a></li>
      <li><a href="/bestholidayever/beer-wine-cheese/wine-food-pairings">' . t('Wine &amp; Food Pairings') . '</a></li>
      <li><a href="/bestholidayever/beer-wine-cheese/building-cheese-plate">' . t('Building a Cheese Plate') . '</a></li>
    </ul>
  </li><!--column-->
  
  <li class="column">
    <h3><a href="/bestholidayever/gifts-decor/gifts-kitchen">' . t('Gifts &amp; Decor') . '</a></h3>
    <ul>
      <li><a href="/bestholidayever/gifts-decor/gifts-kitchen">' . t('Gifts from the Kitchen') . '</a></li>
      <li><a href="/bestholidayever/gifts-decor/diy-decorating-etsy">' . t('DIY Decorating with Etsy') . '</a></li>
      <li><a href="/bestholidayever/gifts-decor/gifts-give-back">' . t('Gifts That Give Back') . '</a></li>
      <li><a href="/bestholidayever/gifts-decor/gifts-under-5-10-15-30">' . t('Gifts Under $5, $10, $15 and $30') . '</a></li>
      <li><a href="/bestholidayever/gifts-decor/limited-edition-gift-card">' . t('Limited Edition Gift Card') . '</a></li>
    </ul>

    <h3>' . t('More Help') . '</h3>
    <ul>
      <li><a href="/online-ordering">' . t('Order Meals Online') . '</a></li>
      <li><a href="/about-our-products/whole-deal">' . t('Whole Deal') . '</a></li>
      <li><a href="http://darkrye.com" target="_blank">' . t('Dark Rye - Issue #16: Gather') . '</a></li>
      <li><a href="/apps">' . t('Recipe App') . '</a></li>
      <li><a href="/newsletters">' . t('Sign Up for Newsletters') . '</a></li>
    </ul>
  </li><!--column-->
  </ul>
  </div><!--holiday-nav-->';

  return $html;
}

/**
* Returns HTML for Holiday 2013 Recipe section header
*/
function _wfm_static_holiday_recipe_header_html() {
  $html = '
    <div class="holiday-text-head clearfix">
      <div class="heading">
        <h2>' . t('Our Favorite Recipes') . '</h2>
        <p class="subhead">' . t('Inspiration for every holiday complication.') . '</p>
      </div><!--heading-->
    
      <div class="holiday-promo">
        <a href="/apps"><span class="icon-iphone"></span></a>
        <p class="cta"><a href="/apps">' . t('Check Out Our') . '<br>' . t('Recipe App') . '</a></p>
        <p class="flavor">' . t('1,000s of Recipes in Your Pocket') . '</p>
        <p class="arrow"><a href="/apps">' . t('Apps') . '</a></p>
      </div><!--holiday-promo-->
    </div><!--holiday-text-head-->
  
    <nav id="inline-nav">
      <ul class="holiday-inline-nav">
          <li class="appetizers"><a href="/bestholidayever/our-favorite-recipes/appetizers">' . t('Appetizers') . '</a></li>
          <li class="side-dishes"><a href="/bestholidayever/our-favorite-recipes/side-dishes">' . t('Side Dishes') . '</a></li>
          <li class="potatoes-and-sweet-potatoes"><a href="/bestholidayever/our-favorite-recipes/potatoes-and-sweet-potatoes">' . t('Potatoes &amp; Sweet Potatoes') . '</a></li>
          <li class="main-courses"><a href="/bestholidayever/our-favorite-recipes/main-courses">' . t('Main Courses') . '</a></li>
          <li class="breads-muffins-rolls"><a href="/bestholidayever/our-favorite-recipes/breads-muffins-rolls">' . t('Breads, Muffins &amp; Rolls') . '</a></li>
          <li class="cookies"><a href="/bestholidayever/our-favorite-recipes/cookies">' . t('Cookies') . '</a></li>
          <li class="pies"><a href="/bestholidayever/our-favorite-recipes/pies">' . t('Pies') . '</a></li>
          <li class="desserts"><a href="/bestholidayever/our-favorite-recipes/desserts">' . t('Desserts') . '</a></li>
          <li class="vegetarian"><a href="/bestholidayever/our-favorite-recipes/vegetarian">' . t('Vegetarian') . '</a></li>
          <li class="vegan"><a href="/bestholidayever/our-favorite-recipes/vegan">' . t('Vegan') . '</a></li>
          <li class="gluten-free"><a href="/bestholidayever/our-favorite-recipes/gluten-free">' . t('Gluten Free') . '</a></li>
          <li class="healthy"><a href="/bestholidayever/our-favorite-recipes/healthy">' . t('Healthy') . '</a></li>
          <li class="seasonal-favorites"><a href="/bestholidayever/our-favorite-recipes/seasonal-favorites">' . t('Seasonal Favorites') . '</a></li>
          <li class="hanukkah"><a href="/bestholidayever/our-favorite-recipes/hanukkah">' . t('Hanukkah') . '</a></li>
          <li class="leftovers"><a href="/bestholidayever/our-favorite-recipes/leftovers">' . t('Leftovers') . '</a></li>
      </ul>
    </nav>
  ';
  return $html;
}

/**
* Returns HTML for Holiday 2013 Gift section header 
*/
function _wfm_static_holiday_gift_header_html() {
  $html = '
    <div class="holiday-text-head clearfix">
      <div class="heading">
        <h2>' . t('Gifts &amp; Decor') . '</h2>
        <p class="subhead">' . t("From hand-made gift ideas to DIY decorating, we've 
        got some crafty") . '<br />' . t("ideas for a homemade Thanksgiving.") . '</p>
      </div><!--heading-->
    
      <div class="holiday-promo">
        <a href="/bestholidayever/gifts-decor/gifts-give-back"><span class="icon-gift"></span></a>
        <p class="cta"><a href="/bestholidayever/gifts-decor/gifts-give-back">' . t('Check Out Our')
          . '<br />' . t('Gifts That Give Back') . '</a></p>
        <p class="flavor">' . t('Browse some gifts for good causes') . '</p>
        <p class="arrow"><a href="/bestholidayever/gifts-decor/gifts-give-back">' . t('Gifts That Give Back') . '</a></p>
      </div><!--holiday-promo-->
    </div><!--holiday-text-head-->

    <nav id="inline-nav">
      <ul class="holiday-inline-nav">
          <li class="gifts-kitchen"><a href="/bestholidayever/gifts-decor/gifts-kitchen">'
            . t('Gifts from the Kitchen') . '</a></li>
          <li class="diy-decorating-etsy"><a href="/bestholidayever/gifts-decor/diy-decorating-etsy">'
            . t('DIY Decorating with Etsy') . '</a></li>
          <li class="gifts-give-back"><a href="/bestholidayever/gifts-decor/gifts-give-back">' 
            . t('Gifts that Give Back') . '</a></li>
          <li class="gifts-under-5-10-15-30"><a href="/bestholidayever/gifts-decor/gifts-under-5-10-15-30">' 
            . t('Gifts Under $5, $10, $15 and $30') . '</a></li> 
          <li class="limited-edition-gift-card"><a href="/bestholidayever/gifts-decor/limited-edition-gift-card">' 
            . t('Limited Edition Gift Card') . '</a></li> 
      </ul>
    </nav>
  ';
  return $html;
}    

/**
* Returns HTML for Holiday 2013 Footer 
*/
function _wfm_static_holiday_footer_html() {
  $blog_posts = views_embed_view('holiday_2013_blog_posts', 'holiday_footer');

  $html = '<div id="holiday-footer-wrap">
    <div class="holiday-footer clearfix">
      <div class="social-links">
        <h3><span class="script">' . t('Spreading') . '</span><br />' . t('Holiday Sanity') . '</h3>
        <ul class="clearfix">
          <li>
            <a href="https://www.facebook.com/wholefoods">
              <img src="/sites/all/themes/wholefoods/images/holiday2013/facebook.png" alt="' . t('Facebook') . '" />
            </a>
          </li>
          <li>
            <a href="https://twitter.com/wholefoods/">
              <img src="/sites/all/themes/wholefoods/images/holiday2013/twitter.png" alt="' . t('Twitter') . '" />
            </a>
          </li>
          <li>
            <a href="http://pinterest.com/wholefoods/">
              <img src="/sites/all/themes/wholefoods/images/holiday2013/pinterest.png" alt="' . t('Pinterest') . '" />
            </a>
          </li>
          <li>
            <a href="http://instagram.com/wholefoodsmarket">
              <img src="/sites/all/themes/wholefoods/images/holiday2013/instagram.png" alt="' . t('Instagram') . '" />
            </a>
          </li>
        </ul>
      </div><!--social-links-->
      <div class="blog">' .
        $blog_posts
      . '</div><!--blog-->
      <div class="whole-deal">
        <h3><span class="script">' . t('The') . '</span> ' . t('Whole Deal') . '<sup>&reg;</sup></h3>
        <p><a href="/about-our-products/whole-deal">' . t('Holiday savings. One coupon at a time.') . '</a></p>
        <p class="arrow"><a href="/about-our-products/whole-deal"><img src="/sites/all/themes/wholefoods/images/holiday2013/arrow-orange.png" alt="" /></a></p>
      </div><!--whole-deal-->
      <div class="newsletter">
        <h3>' . t('Keep') . ' <span class="script">' . t('in') . '</span> ' . t('Touch') . '</h3>
        <p><a href="/newsletters">' . t('Sign up for our newsletter and get the best all year long.') . '</a></p>
        <p class="arrow"><a href="/newsletters"><img src="/sites/all/themes/wholefoods/images/holiday2013/arrow-orange.png" alt="" /></a></p>
      </div><!--newsletter-->
    </div><!--holiday-footer-->
    </div><!--holiday-footer-wrap-->';

  return $html;
} 
