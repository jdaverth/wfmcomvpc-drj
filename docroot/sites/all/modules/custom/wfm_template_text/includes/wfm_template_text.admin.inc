<?php
/**
 * @file Admin form for wfm_template_text
 */

/**
 * Page callback admin form.
 */
function wfm_template_text_admin_form() {
  $defaults = array(
    // Sales and coupons defaults.
    'sales_and_coupons' => array(
      'sales_no_results' => variable_get('wfm_template_text_sales_no_results'),
      'coupons_no_results' => variable_get('wfm_template_text_coupons_no_results'),
      'select_all_coupons' => variable_get('wfm_template_text_coupons_select_all'),
      'check_coupon' => variable_get('wfm_template_text_coupons_check_coupon'),
      'store_select' => variable_get('wfm_template_text_sales_coupons_store_search_label'),
      'store_select_submit' => variable_get('wfm_template_text_sales_coupons_store_select_submit'),
      'print_selected' => variable_get('wfm_template_text_coupons_print_coupons'),
      'sales_tab' => variable_get('wfm_template_text_sales_coupons_sales_tab_label'),
      'coupons_tab' => variable_get('wfm_template_text_sales_coupons_coupons_tab_label'),
      'pdf_link' => variable_get('wfm_template_text_sales_coupons_pdf_download_text'),
      'no_store_selected' => variable_get('wfm_template_text_no_store_selected'),
    ),
    'blogs' => array(
      'comment_guidelines' => variable_get('wfm_template_text_blog_comment_guidelines'),
    ),
    'newsletter_preferences' => array(
      'page_title' => variable_get('wfm_template_text_newsletter_preferences_page_title'),
      'intro_text' => variable_get('wfm_template_text_newsletter_preferences_intro_text'),
      'lifestyle_text' => variable_get('wfm_template_text_newsletter_preferences_lifestyle_text'),
      'subscriptions_text' => variable_get('wfm_template_text_newsletter_preferences_subscriptions_text'),
      'profile_error' => variable_get('wfm_template_text_newsletter_preferences_profile_error'),
      'verify_identity' => variable_get('wfm_template_text_newsletter_preferences_verify_identity'),
      'request_link_confirmation_heading' => variable_get('wfm_template_text_newsletter_preferences_request_link_confirmation_heading'),
      'request_link_confirmation_body' => variable_get('wfm_template_text_newsletter_preferences_request_link_confirmation_body'),
      'no_email_help_text' => variable_get('wfm_template_text_newsletter_preferences_request_no_email_help_text'),

    ),
    'estore' => array(
      'chooser' => variable_get('wfm_template_text_estore_chooser'),
    ),
  );
  $form = array();

  // Sales and Coupons form fields
  $form['sales_coupons'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sales and Coupons'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['sales_coupons']['wfm_template_text_sales_coupons_sales_tab_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Sales Tab Label'),
    '#default_value' => $defaults['sales_and_coupons']['sales_tab'],
  );
  $form['sales_coupons']['wfm_template_text_sales_coupons_coupons_tab_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Coupons Tab Label'),
    '#default_value' => $defaults['sales_and_coupons']['coupons_tab'],
  );
  $form['sales_coupons']['wfm_template_text_sales_coupons_pdf_download_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Sales Flyer PDF Link Text'),
    '#default_value' => $defaults['sales_and_coupons']['pdf_link'],
  );
  $form['sales_coupons']['wfm_template_text_coupons_select_all'] = array(
    '#type' => 'textfield',
    '#title' => t('Coupons Select All Label'),
    '#default_value' => $defaults['sales_and_coupons']['select_all_coupons'],
  );
  $form['sales_coupons']['wfm_template_text_coupons_check_coupon'] = array(
    '#type' => 'textfield',
    '#title' => t('Coupons Check Coupon For Print Label'),
    '#default_value' => $defaults['sales_and_coupons']['check_coupon'],
  );
  $form['sales_coupons']['wfm_template_text_coupons_print_coupons'] = array(
    '#type' => 'textfield',
    '#title' => t('Coupons Print Selected Coupons Button Text'),
    '#default_value' => $defaults['sales_and_coupons']['print_selected'],
  );
  $form['sales_coupons']['wfm_template_text_sales_coupons_store_search_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Store Select Label'),
    '#default_value' => $defaults['sales_and_coupons']['store_select'],
  );
  $form['sales_coupons']['wfm_template_text_sales_coupons_store_select_submit'] = array(
    '#type' => 'textfield',
    '#title' => t('Store Select Submit Text'),
    '#default_value' => $defaults['sales_and_coupons']['store_select_submit'],
  );
  $form['sales_coupons']['wfm_template_text_sales_no_results'] = array(
    '#type' => 'text_format',
    '#title' => t('Sales No Results Text'),
    '#format' => $defaults['sales_and_coupons']['sales_no_results']['format'],
    '#default_value' => $defaults['sales_and_coupons']['sales_no_results']['value'],
  );
  $form['sales_coupons']['wfm_template_text_coupons_no_results'] = array(
    '#type' => 'text_format',
    '#title' => t('Coupons No Results Text'),
    '#format' => $defaults['sales_and_coupons']['coupons_no_results']['format'],
    '#default_value' => $defaults['sales_and_coupons']['coupons_no_results']['value'],
  );
  $form['sales_coupons']['wfm_template_text_no_store_selected'] = array(
    '#type' => 'text_format',
    '#title' => t('Sales and Coupons No Store Selected'),
    '#format' => $defaults['sales_and_coupons']['no_store_selected']['format'],
    '#default_value' => $defaults['sales_and_coupons']['no_store_selected']['value'],
  );

  // Blogs
  $form['blogs'] = array(
    '#type' => 'fieldset',
    '#title' => t('Blogs'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['blogs']['wfm_template_text_blog_comment_guidelines'] = array(
    '#type' => 'text_format',
    '#title' => t('Blog Comment Guidelines'),
    '#format' => $defaults['blogs']['comment_guidelines']['format'],
    '#default_value' => $defaults['blogs']['comment_guidelines']['value'],
  );

  // Newsletter Preference Center
  $form['newsletter_preferences'] = array(
    '#type' => 'fieldset',
    '#title' => t('Newsletter Preference Center'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['newsletter_preferences']['wfm_template_text_newsletter_preferences_page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Page Title'),
    '#default_value' => $defaults['newsletter_preferences']['page_title'],
  );

  $form['newsletter_preferences']['wfm_template_text_newsletter_preferences_intro_text'] = array(
    '#type' => 'text_format',
    '#title' => t('Newsletter Preferences Intro Text'),
    '#format' => $defaults['newsletter_preferences']['intro_text']['format'],
    '#default_value' => $defaults['newsletter_preferences']['intro_text']['value'],
  );

  $form['newsletter_preferences']['wfm_template_text_newsletter_preferences_lifestyle_text'] = array(
    '#type' => 'text_format',
    '#title' => t('Newsletter Preferences Lifestyle Text'),
    '#format' => $defaults['newsletter_preferences']['lifestyle_text']['format'],
    '#default_value' => $defaults['newsletter_preferences']['lifestyle_text']['value'],
  );

  $form['newsletter_preferences']['wfm_template_text_newsletter_preferences_subscriptions_text'] = array(
    '#type' => 'text_format',
    '#title' => t('Newsletter Preferences Email Subscriptions Text'),
    '#format' => $defaults['newsletter_preferences']['subscriptions_text']['format'],
    '#default_value' => $defaults['newsletter_preferences']['subscriptions_text']['value'],
  );

  $form['newsletter_preferences']['wfm_template_text_newsletter_preferences_profile_error'] = array(
    '#type' => 'text_format',
    '#title' => t('Newsletter Preferences Profile Error Text'),
    '#format' => $defaults['newsletter_preferences']['profile_error']['format'],
    '#default_value' => $defaults['newsletter_preferences']['profile_error']['value'],
  );

  $form['newsletter_preferences']['wfm_template_text_newsletter_preferences_verify_identity'] = array(
    '#type' => 'text_format',
    '#title' => t('Newsletter Preferences Verify Identity Text'),
    '#format' => $defaults['newsletter_preferences']['verify_identity']['format'],
    '#default_value' => $defaults['newsletter_preferences']['verify_identity']['value'],
  );

  $form['newsletter_preferences']['wfm_template_text_newsletter_preferences_request_link_confirmation_heading'] = array(
    '#title' => t('Newsletter Preferences Request Email Verification Confirmation Heading'),
    '#type' => 'textfield',
    '#default_value' => $defaults['newsletter_preferences']['request_link_confirmation_heading'],
  );

  $form['newsletter_preferences']['wfm_template_text_newsletter_preferences_request_link_confirmation_body'] = array(
    '#title' => t('Newsletter Preferences Request Email Verification Confirmation Body'),
    '#type' => 'textfield',
    '#default_value' => $defaults['newsletter_preferences']['request_link_confirmation_body'],
  );

  $form['newsletter_preferences']['wfm_template_text_newsletter_preferences_request_no_email_help_text'] = array(
    '#title' => t('Newsletter Preferences No Email Help Text'),
    '#type' => 'text_format',
    '#format' => $defaults['newsletter_preferences']['no_email_help_text']['format'],
    '#default_value' => $defaults['newsletter_preferences']['no_email_help_text']['value'],
  );

  // Estore
  $form['estore'] = array(
    '#type' => 'fieldset',
    '#title' => t('Estore'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['estore']['wfm_template_text_estore_chooser'] = array(
    '#type' => 'text_format',
    '#title' => t('Estore Store Chooser Text'),
    '#format' => $defaults['estore']['chooser']['format'],
    '#default_value' => $defaults['estore']['chooser']['value'],
  );

  // Add copy-and-pastable token api shortcodes.
  foreach ($form as $category => $form_item) {
    foreach ($form_item as $form_element => $attributes) {
      if (is_array($attributes)) {
        $form[$category][$form_element]['#description'] = t('Embed:') . ' [api:get_template_text["' . $form_element . '"]/]';
      }
    }
  }

  return system_settings_form($form);
}
