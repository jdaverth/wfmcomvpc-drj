<?php
/**
* @file
* Defines variable include files.
*
*/

$variables = array();

require_once 'wfmvariables_pluck.inc';
require_once 'wfmvariables_wfmapi.inc';
require_once 'wfmvariables_newsletter.inc';
require_once 'wfmvariables_gmap.inc';
require_once 'wfmvariables_social.inc';
require_once 'wfmvariables_estore.inc';
require_once 'wfmvariables_giftango.inc';
require_once 'wfmvariables_salesforce.inc';