<?php

/**
* @file
* Default variables file for eStore
*
*/

$variables[self::ESTORE_TOP_CATEGORIES_URL] = array(
  'title' => 'eStore Top Categories URL',
  'category' => self::CATEGORY_ESTORE,
  'help' => 'The URL string for the eStore top category API.  This should be the host and path only, the protocol and specific query string will be added at runtime. @see WFM_MONSTER_NAV',
  'default' => variable_get('wfm_estore_url', 'shop.wholefoodsmarket.com') . '/calls/menuGet.aspx',
);

/*

 * $variables[self::ESTORE_VARIABLENAME] = array(
  'title' => 'eStore Variable',
  'category' => self::CATEGORY_ESTORE,
  'help' => '',
  'default' => 'default value',
);

 */
