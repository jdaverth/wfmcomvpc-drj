<?php
$variables[self::GIFTANGO_IFRAME_URL] = array(
  'title' => 'Giftango iframe Source URL',
  'category' => self::CATEGORY_GIFTANGO,
  'help' => '',
  'default' => 'https://giftcards.wholefoodsmarket.com/GiftCardPortal/WholeFoods/GiftCardPortal.aspx',
);

$variables[self::GIFTANGO_CRYPTO_KEY] = array(
  'title' => 'Giftango Cryptographic Signing Key',
  'category' => self::CATEGORY_GIFTANGO,
  'help' => '',
  'default' => 'B0336A65C158FE1B41FC135574349F0B',
);