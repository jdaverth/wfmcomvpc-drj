<?php
/**
* @file
* Default variables file for pluck
*
*/

$variables[self::PLUCK_COMMUNITY] = array(
  'title' => 'Pluck Community',
  'category' => self::CATEGORY_PLUCK,
  'help' => '',
  'default' => 'http://community.wholefoodsmarket.com',
);

$variables[self::PLUCK_RECIPE_FORUM_ID] = array(
  'title' => 'Pluck Recipes Fourm ID',
  'category' => self::CATEGORY_PLUCK,
  'help' => '',
  'default' => 'Cat:aac80c666-ce42-49c8-8941-7c335cf4efcb',
);

$variables[self::PLUCK_DOCUMENT_DOMAIN] = array(
  'title' => 'Pluck Document Domain',
  'category' => self::CATEGORY_PLUCK,
  'help' => '',
  'default' => '.wholefoodsmarket.com',
);
$variables[self::PLUCK_RECIPE_GALLERY_KEY] = array(
  'title' => 'Pluck Recipes Gallery Key',
  'category' => self::CATEGORY_PLUCK,
  'help' => '',
  'default' => 'c7b087be-7bbf-4095-bab6-84f259580a65',
);