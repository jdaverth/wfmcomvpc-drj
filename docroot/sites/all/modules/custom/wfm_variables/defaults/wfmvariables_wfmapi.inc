<?php
/**
* @file
* Variable defaults for WFMAPI.
*
*/

$variables[self::WFMAPI_HOST] = array(
  'title' => 'WFM API Host',
  'category' => self::CATEGORY_WFMAPI,
  'help' => 'The WFM API Host (no protocol).',
  'default' => 'api.wholefoodsmarket.com',
);
$variables[self::WFMAPI_KEY] = array(
  'title' => 'WFMP API Key',
  'category' => self::CATEGORY_WFMAPI,
  'help' => '',
  'default' => 'uZCNDfRt4tdRARzgKN92OPp1FYZXCNMp',
);
$variables[self::WFMAPI_SECRET] = array(
  'title' => 'WFMP API Secret',
  'category' => self::CATEGORY_WFMAPI,
  'help' => '',
  'default' => 'd9SQCxgysBTHTh6Z',
);
$variables[self::WFMAPI_VERSION] = array(
  'title' => 'WFM API Version String',
  'category' => self::CATEGORY_WFMAPI,
  'help' => '',
  'default' => '2',
);
$variables[self::CONTACT_POST_URL] = array(
  'title' => 'WFM Contact Form Post URL',
  'category' => self::CATEGORY_WFMAPI,
  'help' => 'The contact form post url, complete with protocol',
  'default' => 'http://qa.wholefoodsmarket.com/company/contact_send.php',
);
$variables[self::SPECIALS_PDF_URL] = array(
  'title' => 'WFM Store Special PDF URL',
  'category' => self::CATEGORY_WFMAPI,
  'help' => 'Base url for store special pdf downloads',
  'default' => 'assets.wholefoodsmarket.com/storespecials/',
);
$variables[self::SPECIALS_DIETS_PDF_URL] = array(
  'title' => 'WFM Store Special Diet PDF URL',
  'category' => self::CATEGORY_WFMAPI,
  'help' => 'Base url for store special diet pdf downloads',
  'default' => 'assets.wholefoodsmarket.com/specialdiets/',
);
$variables[self::UK_OFFICE] = array(
  'title' => 'UK National Office NID',
  'category' => self::CATEGORY_WFMAPI,
  'help' => '',
  'default' => '88367',
);
$variables[self::USA_OFFICE] = array(
  'title' => 'USA National Office NID',
  'category' => self::CATEGORY_WFMAPI,
  'help' => '',
  'default' => '95',
);
$variables[self::CANADA_OFFICE] = array(
  'title' => 'Canada National Office NID',
  'category' => self::CATEGORY_WFMAPI,
  'help' => '',
  'default' => '94296',
);
