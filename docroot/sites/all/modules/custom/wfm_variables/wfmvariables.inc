<?php
/**
 * @file
 * Contains WFMVariables.
 */

class WFMVariables {
  /**
   * Variable names
   */

  /** Pluck **/
  const PLUCK_COMMUNITY = 'pluck_community';
  const PLUCK_RECIPE_FORUM_ID = 'pluck_recipe_forum_id';
  const PLUCK_DOCUMENT_DOMAIN = 'pluck_document_domain';
  const PLUCK_RECIPE_GALLERY_KEY = 'pluck_recipe_gallery_key';

  /** WFMAPI **/
  const WFMAPI_HOST = 'wfmapi_host';
  const WFMAPI_KEY = 'wfmapi_key';
  const WFMAPI_SECRET = 'wfmapi_secret';
  const WFMAPI_VERSION = 'wfmapi_version';
  const CONTACT_POST_URL = 'wfmapi_contact_post_url';
  const UK_OFFICE = 'uk_office';
  const USA_OFFICE = 'usa_office';
  const CANADA_OFFICE = 'canada_office';
  const SPECIALS_PDF_URL = 'specials_pdf_url';
  const SPECIALS_DIETS_PDF_URL = 'special_diets_pdf_url';


  /** Newsletters **/
  const NEWSLETTER_UNSUBSCRIBE_LINK = 'newsletter_unsubscribe_link';
  const NEWSLETTER_RI = 'newsletter_ri';
  const NEWSLETTER_EI = 'newsletter_ei';

  const NEWSLETTER_RI_PREF = 'newsletter_ri_pref';
  const NEWSLETTER_EI_PREF = 'newsletter_ei_pref';

  /** GMaps **/
  const GMAPS_JS_API_URL = 'gmaps_js_api_url';
  const GMAPS_JSON_API_URL = 'gmaps_json_api_url';
  const GMAPS_CRYPTO_KEY = 'gmaps_crypto_key';

  /** Social **/
  const SOCIAL_FACEBOOK_APP_ID = 'social_facebook_app_id';
  const SOCIAL_FACEBOOK_APP_SECRET = 'social_facebook_app_secret';
  const SOCIAL_TWITTER_CONSUMER_KEY = 'social_twitter_consumer_key';
  const SOCIAL_TWITTER_CONSUMER_SECRET = 'social_twitter_consumer_secret';
  const SOCIAL_TWITTER_OAUTH_ACCESS_TOKEN = 'social_twitter_oauth_access_token';
  const SOCIAL_TWITTER_OAUTH_ACCESS_TOKEN_SECRET = 'social_twitter_oauth_access_token_secret';

  /** eStore API  @see WFM_MONSTER_NAV **/
  const ESTORE_TOP_CATEGORIES_URL = 'estore_top_categories_url';

  /** eStore API**/
  const ESTORE_TOP_CATEGORIES = 'estore_top_categories';

  /** Giftango **/
  const GIFTANGO_IFRAME_URL = 'giftango_iframe_url';
  const GIFTANGO_CRYPTO_KEY = 'giftango_crypto_key';

  /**
   * Category names
   */
  const CATEGORY_PLUCK = 'Pluck';
  const CATEGORY_WFMAPI = 'WFM API';
  const CATEGORY_NEWSLETTER = 'Newsletters';
  const CATEGORY_GMAPS = 'GMaps';
  const CATEGORY_SOCIAL = 'Social';
  const CATEGORY_ESTORE = 'eStore';
  const CATEGORY_GIFTANGO = 'Giftango';
  const CATEGORY_SALESFORCE = 'Salesforce';

  /** Salesforce **/
  const SALESFORCE_ORGANIZATION_ID = "sf_org_id";
  const SALESFORCE_POST_URL = "sf_post_url";
  const SALESFORCE_CATEGORY_FIELD = "sf_category_field";
  const SALESFORCE_STORE_FIELD = "sf_store_field";
  const SALESFORCE_UPC_FIELD = "sf_upc_field";
  const SALESFORCE_ZIP_FIELD = "sf_zip_field";
  const SALESFORCE_TLC_FIELD = "sf_tlc_field";
  const SALESFORCE_UA_FIELD = "sf_ua_field";

  public static $instance = NULL;
  public $variableList = array();

  /**
   * Get a variable's value from the system's variable table.
   *
   * Return the default value if the value hasn't been saved to
   * Drupal's variable table.
   *
   * @param string $name
   *   The string that is the name of the variable.
   *
   * @retval string
   *   The value set in the variable table or the default.
   * @retval NULL
   *    The value does not exist.
   */
  static public function get($name) {
    return self::_getInstance()->_get($name);
  }

  /**
   * Set a value in the system's variable table.
   *
   * @param string $name
   *   A string that is the name of the variable.
   * @param mixed $value
   *   A value to save in the variable table.
   */
  static public function set($name, $value) {
    return self::_getInstance()->_set($name, $value);
  }

  /**
   * Set multiple values in the system's variable table.
   *
   * @param array $values
   *   An array of multiple values to set such as ...
   *   array(
   *     'variable name' => 'value',
   *     'variable name' => 'value)
   */
  static public function setMultiple(array $values) {
    return self::_getInstance()->_setMultiple($values);
  }

  /**
   * Get a list of system variables by category.
   *
   * The response will be an array such as ...
   *   array(
   *     'Category 1' => array('Var1 Name' => 'Value', 'Var2 Name' => 'value'),
   *     'Category 2' => array('Var3 Name' => 'Value', 'Var4Name' => 'value'),
   *   )
   *
   * @retval array
   *   An array of all the system variables by category managed by this class.
   */
  static public function getList() {
    return self::_getInstance()->_list();
  }

  /**
   * Get the singleton instance of this class.
   *
   * @retval object
   *   The singleton instance of this class.
   */
  static public function _getInstance() {
    if (self::$instance === NULL) {
      self::$instance = new WFMVariables();
    }

    return self::$instance;
  }

  /**
   * Get a variable's value from the system's variable table.
   *
   * Return the default value if the value hasn't been saved to
   * Drupal's variable table.
   *
   * @param string $name
   *   The string that is the name of the variable.
   *
   * @retval string
   *   The value set in the variable table or the default.
   * @retval NULL
   *    The value does not exist.
   */
  protected function _get($name) {
    if (!isset($this->variableList[$name])) {
      // TODO: Throw a warning.
      return '';
    }
    else {
      $value = $this->variableList[$name];
      $value['value'] = variable_get($this->_makeVariableName($name),
                                     $value['default']);
      return $value['value'];
    }

  }

  /**
   * Set a value in the system's variable table.
   *
   * @param string $name
   *   A string that is the name of the variable.
   * @param mixed $value
   *   A value to save in the variable table.
   */
  protected function _set($name, $value) {
    $key = $this->_makeVariableName($name);

    if (!isset($this->variableList[$name])) {
      drupal_set_message(t('There was a problem saving your change.'), 'error');
    }
    else {
      if (WFMVariables::get($name) != $value) {
        $msg = t('Updating variable !name to !value',
                 array('!name' => $name, '!value' => $value));
        drupal_set_message($msg);

        variable_set($key, $value);
      }
    }
  }

  /**
   * Set multiple values in the system's variable table.
   *
   * @param array $values
   *   An array of multiple values to set such as ...
   *   array(
   *     'variable name' => 'value',
   *     'variable name' => 'value)
   */
  protected function _setMultiple(array $values) {
    foreach ($values as $key => $value) {
      $this->_set($key, $value);
    }
  }

  /**
   * Get a list of system variables by category.
   *
   * The response will be an array such as ...
   *   array(
   *     'Category 1' => array('Var1 Name' => 'Value', 'Var2 Name' => 'value'),
   *     'Category 2' => array('Var3 Name' => 'Value', 'Var4Name' => 'value'),
   *   )
   *
   * @retval array
   *   An array of all the system variables by category managed by this class.
   */
  protected function _list() {
    $response = array();

    foreach ($this->variableList as $key => $value) {
      $value['value'] = variable_get($this->_makeVariableName($key),
                                     $value['default']);
      $response[$value['category']][$key] = $value;
    }

    return $response;
  }

  /**
   * Creates a unique, prefixed variable name out of the variables name.
   *
   * @param string $name
   *   The name of the variable.
   *
   * @retval string
   *   A unique, prefixed variable name that conforms to this class' style.
   */
  protected function _makeVariableName($name) {
    return drupal_strtolower('wfm_variable__' . $name);
  }

  /**
   * Constructor method to setup variables.
   */
  protected function __construct() {
    require_once 'defaults/wfmvariables_defaults.inc';

    foreach ($variables as $key => $value) {
      $this->_addVariable($key, $value);
    }
  }

  /**
   * Add a variable that should be managed by this class.
   *
   * @param string $key
   *   A unique key to identify the variable.
   * @param array $value
   *   An array of parameters that make up a variable that looks like this ...
   *     array(
   *       'title' => 'Google Maps JavaScript API URL',
   *       'category' => self::CATEGORY_GMAPS,
   *       'help' => 'This is the URL of the JavaScropt Map API',
   *       'default' => 'http://maps.googleapis.com/maps/api/js',
   *     );
   */
  protected function _addVariable($key, $value) {
    if (!isset($value['category']) || drupal_strlen($value['category']) < 1) {
      $value['category'] = 'Other';
    }

    if (!isset($value['default'])) {
      // Throw a notice.
    }

    $this->variableList[$key] = $value;
  }
}
