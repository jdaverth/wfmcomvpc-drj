<?php
/**
 * Replacement for the default WordPressAuthor class, because we want to create
 * nodes instead of users.
 */
class BlogImportAuthor extends WordPressMigration {
  /**
   * Set it up
   */
  public function __construct(array $arguments = array()) {
    parent::__construct($arguments);

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'author_login' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'description' => 'WordPress author username',
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    $fields = array(
      'author_id' => 'Unique WordPress ID of the author',
      'author_login' => 'Username (for login) of the author',
      'author_email' => 'Author email address',
      'author_display_name' => 'Displayed author name',
      'author_first_name' => 'Author first name',
      'author_last_name' => 'Author last name',
    );

    // Construct the source and destination objects.
    $source_options = array(
      'reader_class' => 'WordPressXMLReader',
      'cache_counts' => TRUE,
    );
    $this->source = new WordPressSourceXML($this->wxrFile, '/rss/channel/wp:author',
      'wp:author_login', $fields, $source_options);
    $this->destination = new BlogImportNodeDestination('person');

    // The basic mappings
    $this->addFieldMapping('title', 'author_display_name');
    $this->addFieldMapping('status')
         ->defaultValue(NODE_PUBLISHED);
    $this->addFieldMapping('uid')
         ->defaultValue(1);

    // Unmapped destination fields
    $this->addUnmigratedDestinations(array('revision_uid', 'created', 'changed', 'promote',
      'sticky', 'revision', 'log', 'language', 'tnid', 'is_new', 'body', 'body:summary',
      'body:format', 'body:language', 'field_image', 'field_image:file_class',
      'field_image:language', 'field_image:destination_dir', 'field_image:destination_file',
      'field_image:file_replace', 'field_image:preserve_files', 'field_image:source_dir',
      'field_image:alt', 'field_image:title', 'field_is_director', 'field_person_job',
      'field_user_reference', 'comment', 'path', 'pathauto'));

    // Unmapped source fields
    $this->addUnmigratedSources(array('author_first_name', 'author_email',
                                'author_last_name', 'author_id'));
  }
}

/**
 * Override the default BlogEntry class, so we can set field_blog appropriately.
 */
class BlogImportBlogEntry extends WordPressBlogEntry {
  public function __construct(array $arguments = array()) {
    global $conf;
    $conf['wordpress_migrate_podcast_field'] = 'field_blog_podcast';

    parent::__construct($arguments);
    // All environments come from a common database where the blogs have been
    // defined, so we can count on the hard-coded nid
    switch ($this->machineName) {
      case 'JohnMackeysBlogBlogEntry':
        $blog_nid = 6861;
        break;
      case 'WholeStoryBlogEntry':
        $blog_nid = 6860;
        break;
      default:
        $blog_nid = 0;
        Migration::displayMessage(t('Could not identify the parent blog for !name',
                                  array('!name' => $this->machineName)));
        break;
    }
    $this->addFieldMapping('field_blog')
         ->defaultValue($blog_nid);
    $this->addFieldMapping('field_blog_author', 'creator')
         ->sourceMigration($this->generateMachineName('WordPressAuthor'));
    $this->removeFieldMapping('uid');
    $this->addFieldMapping('uid')
         ->defaultValue(1);
  }

  /**
   * Override of WordPressItemMigration::prepare().
   *
   * Since we've
   *
   * @param stdClass $node
   * @param stdClass $row
   */
  public function prepare(stdClass $node, stdClass $row) {
    // If we have an attached podcast, replace [powerpress] in the body with
    // a link to the audio file.
    if (isset($node->field_blog_podcast)) {
      $podcast_url = $node->field_blog_podcast[LANGUAGE_NONE][0]['url'];
      if ($podcast_url) {
        $link = '<p><a href="' . $podcast_url .
                '"><img src="/modules/file/icons/audio-x-generic.png" /></a></p>';
        $node->body[LANGUAGE_NONE][0]['value'] = str_replace('[powerpress]',
          $link, $node->body[LANGUAGE_NONE][0]['value']);
      }
    }
  }
}

/**
 * Override the default Page class, so it doesn't try to interpret our Person
  * nodes created by BlogImportAuthor as user IDs.
 */
class BlogImportPage extends WordPressPage {
  public function __construct(array $arguments = array()) {
    parent::__construct($arguments);
    $this->removeFieldMapping('uid');
    $this->addFieldMapping('uid')
         ->defaultValue(1);
  }

  /**
   * Override of WordPressItemMigration::prepare().
   *
   * Since we've
   *
   * @param stdClass $node
   * @param stdClass $row
   */
  public function prepare(stdClass $node, stdClass $row) {
    // If we have an attached podcast, replace [powerpress] in the body with
    // a link to the audio file.
    if (isset($node->field_blog_podcast)) {
      $podcast_url = $node->field_blog_podcast[LANGUAGE_NONE][0]['url'];
      if ($podcast_url) {
        $link = '<p><a href="' . $podcast_url .
                '"><img src="/modules/file/icons/audio-x-generic.png" /></a></p>';
        $node->body[LANGUAGE_NONE][0]['value'] = str_replace('[powerpress]',
          $link, $node->body[LANGUAGE_NONE][0]['value']);
      }
    }
  }
}

/**
 * Override the default Attachment class, so it doesn't try to interpret our Person
 * nodes created by BlogImportAuthor as user IDs.
 */
class BlogImportAttachment extends WordPressAttachment {
  public function __construct(array $arguments = array()) {
    parent::__construct($arguments);
    $this->removeFieldMapping('uid');
    $this->addFieldMapping('uid')
         ->defaultValue(1);
  }

  /**
   * Override of WordPressItemMigration::prepare().
   *
   * Since we've
   *
   * @param stdClass $node
   * @param stdClass $row
   */
  public function prepare(stdClass $node, stdClass $row) {
  }
}

class BlogImportNodeDestination extends MigrateDestinationNode {
  /**
   * Override of MigrateDestinationNode::import().
   *
   * On initial import, if the title already exists we want to link to
   * that node, and remember that we did so. On updates, if we see that it
   * is a linked node, we don't want to update it.
   *
   * @param stdClass $node
   * @param stdClass $row
   */
  public function import(stdClass $node, stdClass $row) {
    if (isset($row->migrate_map_destid1)) {
      // Updating the node - if it's linked, just return the ID of the linked
      // node (note that the table columns reflect the usual usage with user
      // accounts).
      $nid = db_select('wordpress_migrate_linked_authors', 'a')
             ->fields('a', array('uid'))
             ->condition('mail', $node->title)
             ->execute()
             ->fetchField();
      if ($nid) {
        $this->numUpdated++;
        return array($nid);
      }
    }
    else {
      // Initial import - if already in the node table, add to our linked_authors
      // list and return the nid of the existing node.
      $nid = db_select('node', 'n')
             ->fields('n', array('nid'))
             ->condition('title', $node->title)
             ->condition('type', 'person')
             ->execute()
             ->fetchField();
      if ($nid) {
        db_merge('wordpress_migrate_linked_authors')
          ->key(array('mail' => $node->title))
          ->fields(array('uid' => $nid))
          ->execute();
        $this->numCreated++;
        return array($nid);
      }
    }
    // If there's no linkage, do the normal thing
    return parent::import($node, $row);
  }

  /**
   * Override of MigrateDestinationNode::bulkRollback().
   *
   * We want to make sure we don't delete any nodes that existed before we first
   * imported.
   *
   * @param $nids
   *  Array of node IDs to be deleted.
   */
  public function bulkRollback(array $nids) {
    $delete_nids = array();
    foreach ($nids as $nid) {
      $title = db_select('wordpress_migrate_linked_authors', 'a')
              ->fields('a', array('mail'))
              ->condition('uid', $nid)
              ->execute()
              ->fetchField();
      if ($title) {
        db_delete('wordpress_migrate_linked_authors')
          ->condition('mail', $title)
          ->execute();
      }
      else {
        $delete_nids[] = $nid;
      }
    }
    parent::bulkRollback($delete_nids);
  }
}

/**
 * Override the default blog class so we can override components.
 */
class BlogImportBlog extends WordPressBlog {
  /**
   * Substitute our own BlogEntry class, so we can assign blog posts to the
   * right blog parent.
   */
  public function migrationClasses() {
    $classes = parent::migrationClasses();
    $classes['WordPressAuthor'] = 'BlogImportAuthor';
    $classes['WordPressBlogEntry'] = 'BlogImportBlogEntry';
    $classes['WordPressPage'] = 'BlogImportPage';
    $classes['WordPressAttachment'] = 'BlogImportAttachment';
    return $classes;
  }
}

/*
 * Implementation of hook_migrate_api().
 */
function blog_import_migrate_api() {
  $api = array(
    'api' => 2,
  );
  return $api;
}
