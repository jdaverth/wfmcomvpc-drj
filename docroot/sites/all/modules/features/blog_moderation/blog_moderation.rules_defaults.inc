<?php
/**
 * @file
 * blog_moderation.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function blog_moderation_default_rules_configuration() {
  $items = array();
  $items['rules_blog_moderation_closed'] = entity_import('rules_config', '{ "rules_blog_moderation_closed" : {
      "LABEL" : "Blog Moderation: Closed",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "flag" ],
      "ON" : { "flag_flagged_blog_comment_closed" : [] },
      "DO" : [
        { "flag_unflagcomment" : {
            "flag" : "blog_comment_pending",
            "comment" : [ "flagged-comment" ],
            "flagging_user" : [ "flagging_user" ],
            "permission_check" : 0
          }
        },
        { "flag_unflagcomment" : {
            "flag" : "blog_comment_emailed",
            "comment" : [ "flagged-comment" ],
            "flagging_user" : [ "flagging_user" ],
            "permission_check" : 0
          }
        },
        { "flag_unflagcomment" : {
            "flag" : "blog_comment_replied",
            "comment" : [ "flagged-comment" ],
            "flagging_user" : [ "flagging_user" ],
            "permission_check" : 0
          }
        }
      ]
    }
  }');
  $items['rules_blog_moderation_comment_publish'] = entity_import('rules_config', '{ "rules_blog_moderation_comment_publish" : {
      "LABEL" : "Blog Moderation: Comment Publish",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "flag" ],
      "ON" : { "flag_flagged_blog_comment_publish" : [] },
      "DO" : [
        { "data_set" : { "data" : [ "flagged-comment:status" ], "value" : "1" } }
      ]
    }
  }');
  $items['rules_blog_moderation_comment_unpublish'] = entity_import('rules_config', '{ "rules_blog_moderation_comment_unpublish" : {
      "LABEL" : "Blog Moderation: Comment Unpublish",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "flag" ],
      "ON" : { "flag_unflagged_blog_comment_publish" : [] },
      "DO" : [
        { "data_set" : { "data" : [ "flagged-comment:status" ], "value" : "0" } }
      ]
    }
  }');
  $items['rules_blog_moderation_emailed'] = entity_import('rules_config', '{ "rules_blog_moderation_emailed" : {
      "LABEL" : "Blog Moderation: Emailed",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "flag" ],
      "ON" : { "flag_flagged_blog_comment_emailed" : [] },
      "DO" : [
        { "flag_unflagcomment" : {
            "flag" : "blog_comment_closed",
            "comment" : [ "flagged-comment" ],
            "flagging_user" : [ "flagging_user" ],
            "permission_check" : 0
          }
        },
        { "flag_unflagcomment" : {
            "flag" : "blog_comment_pending",
            "comment" : [ "flagged-comment" ],
            "flagging_user" : [ "flagging_user" ],
            "permission_check" : 0
          }
        },
        { "flag_unflagcomment" : {
            "flag" : "blog_comment_replied",
            "comment" : [ "flagged-comment" ],
            "flagging_user" : [ "flagging_user" ],
            "permission_check" : 0
          }
        }
      ]
    }
  }');
  $items['rules_blog_moderation_first_touch_moderator_1'] = entity_import('rules_config', '{ "rules_blog_moderation_first_touch_moderator_1" : {
      "LABEL" : "Blog Moderation: First Touch Moderator 1",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "flag" ],
      "ON" : { "flag_flagged_blog_comment_first_touch_1" : [] },
      "IF" : [
        { "flag_flagged_comment" : {
            "flag" : "blog_comment_first_touch_2",
            "comment" : [ "flagged-comment" ],
            "flagging_user" : [ "flagging_user" ]
          }
        }
      ],
      "DO" : [
        { "flag_unflagcomment" : {
            "flag" : "blog_comment_first_touch_2",
            "comment" : [ "flagged-comment" ],
            "flagging_user" : [ "flagging_user" ],
            "permission_check" : 0
          }
        }
      ]
    }
  }');
  $items['rules_blog_moderation_first_touch_moderator_2'] = entity_import('rules_config', '{ "rules_blog_moderation_first_touch_moderator_2" : {
      "LABEL" : "Blog Moderation: First Touch Moderator 2",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "flag" ],
      "ON" : { "flag_flagged_blog_comment_first_touch_2" : [] },
      "IF" : [
        { "flag_flagged_comment" : {
            "flag" : "blog_comment_first_touch_1",
            "comment" : [ "flagged-comment" ],
            "flagging_user" : [ "flagging_user" ]
          }
        }
      ],
      "DO" : [
        { "flag_unflagcomment" : {
            "flag" : "blog_comment_first_touch_1",
            "comment" : [ "flagged-comment" ],
            "flagging_user" : [ "flagging_user" ],
            "permission_check" : 0
          }
        }
      ]
    }
  }');
  $items['rules_blog_moderation_moderator_1'] = entity_import('rules_config', '{ "rules_blog_moderation_moderator_1" : {
      "LABEL" : "Blog Moderation: Moderator 1",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "flag" ],
      "ON" : { "flag_flagged_blog_comment_moderate" : [] },
      "IF" : [
        { "flag_flagged_comment" : {
            "flag" : "blog_comment_moderate_2",
            "comment" : [ "flagged-comment" ],
            "flagging_user" : [ "flagging_user" ]
          }
        }
      ],
      "DO" : [
        { "flag_unflagcomment" : {
            "flag" : "blog_comment_moderate_2",
            "comment" : [ "flagged-comment" ],
            "flagging_user" : [ "flagging_user" ],
            "permission_check" : 0
          }
        }
      ]
    }
  }');
  $items['rules_blog_moderation_moderator_2'] = entity_import('rules_config', '{ "rules_blog_moderation_moderator_2" : {
      "LABEL" : "Blog Moderation: Moderator 2",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "flag" ],
      "ON" : { "flag_flagged_blog_comment_moderate_2" : [] },
      "IF" : [
        { "flag_flagged_comment" : {
            "flag" : "blog_comment_moderate",
            "comment" : [ "flagged-comment" ],
            "flagging_user" : [ "flagging_user" ]
          }
        }
      ],
      "DO" : [
        { "flag_unflagcomment" : {
            "flag" : "blog_comment_moderate",
            "comment" : [ "flagged-comment" ],
            "flagging_user" : [ "flagging_user" ],
            "permission_check" : 0
          }
        }
      ]
    }
  }');
  $items['rules_blog_moderation_pending'] = entity_import('rules_config', '{ "rules_blog_moderation_pending" : {
      "LABEL" : "Blog Moderation: Pending",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "flag" ],
      "ON" : { "flag_flagged_blog_comment_pending" : [] },
      "DO" : [
        { "flag_unflagcomment" : {
            "flag" : "blog_comment_closed",
            "comment" : [ "flagged-comment" ],
            "flagging_user" : [ "flagging_user" ],
            "permission_check" : 0
          }
        },
        { "flag_unflagcomment" : {
            "flag" : "blog_comment_emailed",
            "comment" : [ "flagged-comment" ],
            "flagging_user" : [ "flagging_user" ],
            "permission_check" : 0
          }
        },
        { "flag_unflagcomment" : {
            "flag" : "blog_comment_replied",
            "comment" : [ "flagged-comment" ],
            "flagging_user" : [ "flagging_user" ],
            "permission_check" : 0
          }
        }
      ]
    }
  }');
  $items['rules_blog_moderation_replied'] = entity_import('rules_config', '{ "rules_blog_moderation_replied" : {
      "LABEL" : "Blog Moderation: Replied",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "flag" ],
      "ON" : { "flag_flagged_blog_comment_replied" : [] },
      "DO" : [
        { "flag_unflagcomment" : {
            "flag" : "blog_comment_closed",
            "comment" : [ "flagged-comment" ],
            "flagging_user" : [ "flagging_user" ],
            "permission_check" : 0
          }
        },
        { "flag_unflagcomment" : {
            "flag" : "blog_comment_pending",
            "comment" : [ "flagged-comment" ],
            "flagging_user" : [ "flagging_user" ],
            "permission_check" : 0
          }
        },
        { "flag_unflagcomment" : {
            "flag" : "blog_comment_emailed",
            "comment" : [ "flagged-comment" ],
            "flagging_user" : [ "flagging_user" ],
            "permission_check" : 0
          }
        }
      ]
    }
  }');
  return $items;
}
