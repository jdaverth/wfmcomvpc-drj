<?php
/**
 * @file
 * blogs.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function blogs_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blog_author';
  $context->description = '';
  $context->tag = 'Blogs';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'person' => 'person',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'theme' => array(
      'values' => array(
        'wholefoods' => 'wholefoods',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-blog_posts_by_author-header' => array(
          'module' => 'views',
          'delta' => 'blog_posts_by_author-header',
          'region' => 'content_top',
          'weight' => '-10',
        ),
        'views-blog_posts_by_author-block' => array(
          'module' => 'views',
          'delta' => 'blog_posts_by_author-block',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blogs');
  $export['blog_author'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blog_header';
  $context->description = 'Add\'s the blog header to all blog pages.';
  $context->tag = 'Blogs';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'blog/*' => 'blog/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-blogs-blog_header' => array(
          'module' => 'views',
          'delta' => 'blogs-blog_header',
          'region' => 'content_top',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Add\'s the blog header to all blog pages.');
  t('Blogs');
  $export['blog_header'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blog_page';
  $context->description = '';
  $context->tag = 'Blogs';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'blog' => 'blog',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'theme' => array(
      'values' => array(
        'wholefoods' => 'wholefoods',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-blogs-blog_pages' => array(
          'module' => 'views',
          'delta' => 'blogs-blog_pages',
          'region' => 'content',
          'weight' => NULL,
        ),
        'views-71ff75b7c01aab9ef9b515fc308eb864' => array(
          'module' => 'views',
          'delta' => '71ff75b7c01aab9ef9b515fc308eb864',
          'region' => 'sidebar_second',
          'weight' => '7',
        ),
        'views-blogs-blog_recent_posts' => array(
          'module' => 'views',
          'delta' => 'blogs-blog_recent_posts',
          'region' => 'sidebar_second',
          'weight' => '1',
        ),
        'views-blogs-blog_roll' => array(
          'module' => 'views',
          'delta' => 'blogs-blog_roll',
          'region' => 'sidebar_second',
          'weight' => '2',
        ),
        'views-892febee60674721f68993bbf6e03395' => array(
          'module' => 'views',
          'delta' => '892febee60674721f68993bbf6e03395',
          'region' => 'sidebar_second',
          'weight' => '3',
        ),
        'blogs-blog_search_form' => array(
          'module' => 'blogs',
          'delta' => 'blogs_search_form',
          'region' => 'sidebar_second',
          'weight' => '8',
        ),
        'newsletters-newsletters_sidebar_form' => array(
          'module' => 'newsletters',
          'delta' => 'newsletters_sidebar_form',
          'region' => 'sidebar_second',
          'weight' => '9',
        ),
        'views-blogs-blog_about' => array(
          'module' => 'views',
          'delta' => 'blogs-blog_about',
          'region' => 'sidebar_second',
          'weight' => '11',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blogs');
  $export['blog_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blog_posts';
  $context->description = '';
  $context->tag = 'Blogs';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'blog_post' => 'blog_post',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'theme' => array(
      'values' => array(
        'wholefoods' => 'wholefoods',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-blogs-blog_related_posts' => array(
          'module' => 'views',
          'delta' => 'blogs-blog_related_posts',
          'region' => 'sidebar_second',
          'weight' => '1',
        ),
        'views-blogs-blog_recent_posts' => array(
          'module' => 'views',
          'delta' => 'blogs-blog_recent_posts',
          'region' => 'sidebar_second',
          'weight' => '2',
        ),
        'views-blogs-blog_roll_posts' => array(
          'module' => 'views',
          'delta' => 'blogs-blog_roll_posts',
          'region' => 'sidebar_second',
          'weight' => '3',
        ),
        'views-19ab08af5e9757afc825cf82459bf65b' => array(
          'module' => 'views',
          'delta' => '19ab08af5e9757afc825cf82459bf65b',
          'region' => 'sidebar_second',
          'weight' => '4',
        ),
        'views-15a3b4d30175fa899bea7991d2ac5070' => array(
          'module' => 'views',
          'delta' => '15a3b4d30175fa899bea7991d2ac5070',
          'region' => 'sidebar_second',
          'weight' => '5',
        ),
        'blogs-blogs_search_form' => array(
          'module' => 'blogs',
          'delta' => 'blogs_search_form',
          'region' => 'sidebar_second',
          'weight' => '6',
        ),
        'newsletters-newsletters_sidebar_form' => array(
          'module' => 'newsletters',
          'delta' => 'newsletters_sidebar_form',
          'region' => 'sidebar_second',
          'weight' => '7',
        ),
        'views-blogs-blog_about_posts' => array(
          'module' => 'views',
          'delta' => 'blogs-blog_about_posts',
          'region' => 'sidebar_second',
          'weight' => '8',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blogs');
  $export['blog_posts'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blog_rightcolumn';
  $context->description = '';
  $context->tag = 'Blogs';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'blog/archive*' => 'blog/archive*',
        'blog/category*' => 'blog/category*',
        'blog/search*' => 'blog/search*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-ec5aa9060121c8cca74f577e3856876c' => array(
          'module' => 'views',
          'delta' => 'ec5aa9060121c8cca74f577e3856876c',
          'region' => 'sidebar_second',
          'weight' => NULL,
        ),
        'blogs-blogs_search_form' => array(
          'module' => 'blogs',
          'delta' => 'blogs_search_form',
          'region' => 'sidebar_second',
          'weight' => '0',
        ),
        'views-892febee60674721f68993bbf6e03395' => array(
          'module' => 'views',
          'delta' => '892febee60674721f68993bbf6e03395',
          'region' => 'sidebar_second',
          'weight' => '3',
        ),
        'views-blogs-blog_recent_posts' => array(
          'module' => 'views',
          'delta' => 'blogs-blog_recent_posts',
          'region' => 'sidebar_second',
          'weight' => '1',
        ),
        'views-blogs-blog_roll' => array(
          'module' => 'views',
          'delta' => 'blogs-blog_roll',
          'region' => 'sidebar_second',
          'weight' => '2',
        ),
        'views-blogs-blog_about' => array(
          'module' => 'views',
          'delta' => 'blogs-blog_about',
          'region' => 'sidebar_second',
          'weight' => '5',
        ),
        'views-15a3b4d30175fa899bea7991d2ac5070' => array(
          'module' => 'views',
          'delta' => '15a3b4d30175fa899bea7991d2ac5070',
          'region' => 'sidebar_second',
          'weight' => '4',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blogs');
  $export['blog_rightcolumn'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blog_search';
  $context->description = '';
  $context->tag = 'Blogs';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'blog/search/*' => 'blog/search/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-blog_miscellaneous-our_blogs' => array(
          'module' => 'views',
          'delta' => 'blog_miscellaneous-our_blogs',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blogs');
  $export['blog_search'] = $context;

  return $export;
}
