<?php
/**
 * @file
 * blogs.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function blogs_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_blog_info|node|blog|form';
  $field_group->group_name = 'group_blog_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'blog';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_blog';
  $field_group->data = array(
    'label' => 'Blog Info',
    'weight' => '42',
    'children' => array(
      0 => 'field_contributors',
      1 => 'field_blog_about',
      2 => 'field_blog_subtitle',
      3 => 'body',
      4 => 'field_image',
      5 => 'locations_acl',
      6 => 'metatags',
      7 => 'title',
      8 => 'path',
      9 => 'redirect',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_blog_info|node|blog|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_blog_post_basics|node|blog_post|form';
  $field_group->group_name = 'group_blog_post_basics';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'blog_post';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_blog_post';
  $field_group->data = array(
    'label' => 'Basics',
    'weight' => '1',
    'children' => array(
      0 => 'body',
      1 => 'field_blog',
      2 => 'field_blog_media',
      3 => 'field_blog_taxonomy',
      4 => 'field_blog_podcast',
      5 => 'field_blog_author',
      6 => 'locations_acl',
      7 => 'remove_from_search_field',
      8 => 'title',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_blog_post_basics|node|blog_post|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_blog_post|node|blog_post|form';
  $field_group->group_name = 'group_blog_post';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'blog_post';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Blog Post',
    'weight' => '0',
    'children' => array(
      0 => 'group_blog_post_basics',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $field_groups['group_blog_post|node|blog_post|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_blog_related_content|node|blog|form';
  $field_group->group_name = 'group_blog_related_content';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'blog';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_blog';
  $field_group->data = array(
    'label' => 'Related Content',
    'weight' => '45',
    'children' => array(
      0 => 'field_related_content',
      1 => 'field_blog_roll',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_blog_related_content|node|blog|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_blog|node|blog|form';
  $field_group->group_name = 'group_blog';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'blog';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Blog',
    'weight' => '41',
    'children' => array(
      0 => 'group_blog_info',
      1 => 'group_blog_related_content',
      2 => 'group_marquee',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $field_groups['group_blog|node|blog|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_marquee|node|blog|form';
  $field_group->group_name = 'group_marquee';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'blog';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_blog';
  $field_group->data = array(
    'label' => 'Marquee',
    'weight' => '44',
    'children' => array(
      0 => 'field_heading_background',
      1 => 'field_heading_copy',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_marquee|node|blog|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Basics');
  t('Blog');
  t('Blog Info');
  t('Blog Post');
  t('Marquee');
  t('Related Content');

  return $field_groups;
}
