<?php
/**
 * @file
 * brand_camp.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function brand_camp_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'brand_camp_content';
  $context->description = '';
  $context->tag = 'BC';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'node/*' => 'node/*',
      ),
    ),
    'theme' => array(
      'values' => array(
        'brand_camp_theme' => 'brand_camp_theme',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-brand_camp_list-block_1' => array(
          'module' => 'views',
          'delta' => 'brand_camp_list-block_1',
          'region' => 'content',
          'weight' => '11',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('BC');
  $export['brand_camp_content'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'brand_camp_global';
  $context->description = '';
  $context->tag = 'BC';
  $context->conditions = array(
    'theme' => array(
      'values' => array(
        'brand_camp_theme' => 'brand_camp_theme',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-brand_camp_quotes-block' => array(
          'module' => 'views',
          'delta' => 'brand_camp_quotes-block',
          'region' => 'content',
          'weight' => '15',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('BC');
  $export['brand_camp_global'] = $context;

  return $export;
}
