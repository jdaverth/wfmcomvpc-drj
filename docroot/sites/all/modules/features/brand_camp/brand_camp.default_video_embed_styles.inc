<?php
/**
 * @file
 * brand_camp.default_video_embed_styles.inc
 */

/**
 * Implements hook_default_video_embed_styles().
 */
function brand_camp_default_video_embed_styles() {
  $export = array();

  $video_embed_style = new stdClass();
  $video_embed_style->disabled = FALSE; /* Edit this to true to make a default video_embed_style disabled initially */
  $video_embed_style->api_version = 1;
  $video_embed_style->name = 'brandcamp';
  $video_embed_style->title = 'BrandCamp';
  $video_embed_style->data = array(
    'youtube' => array(
      'width' => '1060',
      'height' => '596',
      'theme' => 'dark',
      'autoplay' => 0,
      'vq' => 'hd720',
      'rel' => 0,
      'showinfo' => 0,
      'modestbranding' => 0,
      'iv_load_policy' => '1',
      'controls' => '1',
      'autohide' => '2',
    ),
    'vimeo' => array(
      'width' => '1060',
      'height' => '596',
      'color' => '00adef',
      'portrait' => 0,
      'title' => 0,
      'byline' => 0,
      'autoplay' => 0,
      'loop' => 0,
      'froogaloop' => 0,
    ),
    'data__active_tab' => '',
  );
  $export['brandcamp'] = $video_embed_style;

  return $export;
}
