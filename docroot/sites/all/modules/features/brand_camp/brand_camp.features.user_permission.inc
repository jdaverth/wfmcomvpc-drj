<?php
/**
 * @file
 * brand_camp.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function brand_camp_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'chute settings admin'.
  $permissions['chute settings admin'] = array(
    'name' => 'chute settings admin',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'brand_camp',
  );

  // Exported permission: 'create bc_article content'.
  $permissions['create bc_article content'] = array(
    'name' => 'create bc_article content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create bc_connect content'.
  $permissions['create bc_connect content'] = array(
    'name' => 'create bc_connect content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create bc_coupon content'.
  $permissions['create bc_coupon content'] = array(
    'name' => 'create bc_coupon content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create bc_image content'.
  $permissions['create bc_image content'] = array(
    'name' => 'create bc_image content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create bc_video content'.
  $permissions['create bc_video content'] = array(
    'name' => 'create bc_video content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any bc_article content'.
  $permissions['delete any bc_article content'] = array(
    'name' => 'delete any bc_article content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any bc_connect content'.
  $permissions['delete any bc_connect content'] = array(
    'name' => 'delete any bc_connect content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any bc_coupon content'.
  $permissions['delete any bc_coupon content'] = array(
    'name' => 'delete any bc_coupon content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any bc_image content'.
  $permissions['delete any bc_image content'] = array(
    'name' => 'delete any bc_image content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any bc_video content'.
  $permissions['delete any bc_video content'] = array(
    'name' => 'delete any bc_video content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own bc_article content'.
  $permissions['delete own bc_article content'] = array(
    'name' => 'delete own bc_article content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own bc_connect content'.
  $permissions['delete own bc_connect content'] = array(
    'name' => 'delete own bc_connect content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own bc_coupon content'.
  $permissions['delete own bc_coupon content'] = array(
    'name' => 'delete own bc_coupon content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own bc_image content'.
  $permissions['delete own bc_image content'] = array(
    'name' => 'delete own bc_image content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own bc_video content'.
  $permissions['delete own bc_video content'] = array(
    'name' => 'delete own bc_video content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any bc_article content'.
  $permissions['edit any bc_article content'] = array(
    'name' => 'edit any bc_article content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any bc_connect content'.
  $permissions['edit any bc_connect content'] = array(
    'name' => 'edit any bc_connect content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any bc_coupon content'.
  $permissions['edit any bc_coupon content'] = array(
    'name' => 'edit any bc_coupon content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any bc_image content'.
  $permissions['edit any bc_image content'] = array(
    'name' => 'edit any bc_image content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any bc_video content'.
  $permissions['edit any bc_video content'] = array(
    'name' => 'edit any bc_video content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own bc_article content'.
  $permissions['edit own bc_article content'] = array(
    'name' => 'edit own bc_article content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own bc_connect content'.
  $permissions['edit own bc_connect content'] = array(
    'name' => 'edit own bc_connect content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own bc_coupon content'.
  $permissions['edit own bc_coupon content'] = array(
    'name' => 'edit own bc_coupon content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own bc_image content'.
  $permissions['edit own bc_image content'] = array(
    'name' => 'edit own bc_image content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own bc_video content'.
  $permissions['edit own bc_video content'] = array(
    'name' => 'edit own bc_video content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'view any unpublished bc_article content'.
  $permissions['view any unpublished bc_article content'] = array(
    'name' => 'view any unpublished bc_article content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'view_unpublished',
  );

  // Exported permission: 'view any unpublished bc_connect content'.
  $permissions['view any unpublished bc_connect content'] = array(
    'name' => 'view any unpublished bc_connect content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'view_unpublished',
  );

  // Exported permission: 'view any unpublished bc_coupon content'.
  $permissions['view any unpublished bc_coupon content'] = array(
    'name' => 'view any unpublished bc_coupon content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'view_unpublished',
  );

  // Exported permission: 'view any unpublished bc_image content'.
  $permissions['view any unpublished bc_image content'] = array(
    'name' => 'view any unpublished bc_image content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'view_unpublished',
  );

  // Exported permission: 'view any unpublished bc_video content'.
  $permissions['view any unpublished bc_video content'] = array(
    'name' => 'view any unpublished bc_video content',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'view_unpublished',
  );

  return $permissions;
}
