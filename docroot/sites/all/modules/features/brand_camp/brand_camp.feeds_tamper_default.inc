<?php
/**
 * @file
 * brand_camp.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function brand_camp_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'chute_image_imprter-jsonpath_parser_1-copy_source_value';
  $feeds_tamper->importer = 'chute_image_imprter';
  $feeds_tamper->source = 'jsonpath_parser:1';
  $feeds_tamper->plugin_id = 'copy';
  $feeds_tamper->settings = array(
    'to_from' => 'to',
    'source' => 'jsonpath_parser:2',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Copy source value';
  $export['chute_image_imprter-jsonpath_parser_1-copy_source_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'chute_image_imprter-jsonpath_parser_1-truncate_caption_to_40';
  $feeds_tamper->importer = 'chute_image_imprter';
  $feeds_tamper->source = 'jsonpath_parser:1';
  $feeds_tamper->plugin_id = 'truncate_text';
  $feeds_tamper->settings = array(
    'num_char' => '40',
    'ellipses' => 0,
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Truncate caption to 40';
  $export['chute_image_imprter-jsonpath_parser_1-truncate_caption_to_40'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'chute_image_imprter-jsonpath_parser_10-explode_url_to_get_album_id';
  $feeds_tamper->importer = 'chute_image_imprter';
  $feeds_tamper->source = 'jsonpath_parser:10';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => '/',
    'limit' => '',
    'real_separator' => '/',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Explode url to get album id';
  $export['chute_image_imprter-jsonpath_parser_10-explode_url_to_get_album_id'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'chute_image_imprter-jsonpath_parser_2-find_replace_regex_drop_japanese';
  $feeds_tamper->importer = 'chute_image_imprter';
  $feeds_tamper->source = 'jsonpath_parser:2';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/[^(\\x20-\\x7F)]*/',
    'replace' => '',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Find replace REGEX - Drop Japanese Smileys (drupal.org/node/1824506#comment-7225408)';
  $export['chute_image_imprter-jsonpath_parser_2-find_replace_regex_drop_japanese'] = $feeds_tamper;

  return $export;
}
