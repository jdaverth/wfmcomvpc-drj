<?php
/**
 * @file
 * deprecated_brand_camp.box.inc
 * File Should be removed after brand_camp_update_7007() has been processed.
 */

/**
 * Implements hook_default_box().
 */
function _deprecated_brand_camp_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'bc_back';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Brand Camp: Background';
  $box->options = array(
    'body' => array(
      'value' => '<p>
<script>
var headerMapping = {
  seafood:	   \'grandpafisherman.jpg\',
  produce:   \'farmerfrank.jpg\',
};

var images = [
  \'chickenlady.jpg\',
  \'cows.jpg\',
  \'farmerfrank.jpg\',
  \'fedora.jpg\',
  \'grandpafisherman.jpg\',
  \'herbguy.jpg\',
];

var headerImage = images[Math.floor(Math.random()*images.length)];

//check if we need to override header image
var bc = jQuery.urlParam(\'bc\');
if (bc != null && headerMapping.hasOwnProperty(bc)) {
  headerImage = headerMapping[bc];
}
document.write(\'<img height="948" src="/sites/all/themes/brand_camp_theme/images/header/\' + headerImage + \'" width="1260">\');
</script>
</p>
<p class="text"><img src="/sites/all/themes/brand_camp_theme/images/intro.png"></p>
<p class="text animated headerTextSlides" id="header1">Calling all challengers and cheese lovers </p>
<p class="text animated headerTextSlides invisible" id="header2">Calling all foragers and big thinkers</p>
<p class="text animated headerTextSlides invisible" id="header3">Calling the kale curious</p>
<p class="text animated headerTextSlides invisible" id="header4">Calling all naturalists and ringleaders </p>',
      'format' => 'source_editor',
    ),
    'additional_classes' => '',
  );
  $export['bc_back'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'bc_back_content';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Brand Camp: content page background';
  $box->options = array(
    'body' => array(
      'value' => '<p><img height="948" src="/sites/all/themes/brand_camp_theme/images/back_content.jpg" width="1260"></p>
',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['bc_back_content'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'bc_twitter';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Brand Camp: Twitter';
  $box->options = array(
    'body' => array(
      'value' => '<p>
<script>
var images = [
  \'<img height="481" src="/sites/all/themes/brand_camp_theme/images/twit.jpg" width="1260">\',
  \'<img height="481" src="/sites/all/themes/brand_camp_theme/images/twit.jpg" width="1260">\',
  \'<img height="481" src="/sites/all/themes/brand_camp_theme/images/twit.jpg" width="1260">\',
  \'<img height="481" src="/sites/all/themes/brand_camp_theme/images/twit.jpg" width="1260">\',
];
var randomPicChoice = Math.floor(Math.random()*images.length);
document.write(images[randomPicChoice]);
</script>
</p>
<p class="text footerTextSlides invisible" id="footer1">"We sell product—a beautiful, worthy and ancient product—so that we can plant trees and encourage kids to stay in school. One by one, we try to expand what’s possible for individuals, then for neighborhoods.”<br/> — Olowo-n’djo Tchala, Co-Founder of Alaffia</p>
<p class="text footerTextSlides invisible" id="footer2">“Food is community. Food brings people together. It’s a language when you can’t speak the same language; it’s the way that we celebrate milestones and show love; that’s the bigger picture.”<br/> — Amelia Pape, WFM Team Member & Founder of My Street Grocery</p>
<p class="text footerTextSlides invisible" id="footer3">“I love visiting the place where something’s being grown. The people who are growing that food are almost always the most passionate and know so much about what they’re growing.”<br/> — Elly Truesdell, WFM Team Member</p>
<p class="text footerTextSlides invisible" id="footer4">“If we change the way we treat the ocean…we can move out of the direction we’re going into a new wave of sustainability. It’s in all the little individual decisions we make every day.”<br/> — Cole Meeker, Co-Founder of The Great and Wonderful Sea of Change Trading Co.</p>

<script>
var quotes = ["#footer1", "#footer2", "#footer3", "#footer4"];
var randomQuoteChoice = 0;
if (quotes.length = images.length) {
  randomQuoteChoice = randomPicChoice;
} else {
  randomQuoteChoice = Math.floor(Math.random()*quotes.length);
}
jQuery(quotes[randomQuoteChoice]).removeClass(\'invisible\');
</script>',
      'format' => 'source_editor',
    ),
    'additional_classes' => '',
  );
  $export['bc_twitter'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'bc_mobile_twitter';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Brand Camp: Twitter - Mobile';
  $box->options = array(
    'body' => array(
      'value' => '<p class="text footerTextSlides invisible" id="footer1">"We sell product—a beautiful, worthy and ancient product—so that we can plant trees and encourage kids to stay in school. One by one, we try to expand what’s possible for individuals, then for neighborhoods.”<br/> — Olowo-n’djo Tchala, Co-Founder of Alaffia</p>
<p class="text footerTextSlides invisible" id="footer2">“Food is community. Food brings people together. It’s a language when you can’t speak the same language; it’s the way that we celebrate milestones and show love; that’s the bigger picture.”<br/> — Amelia Pape, WFM Team Member & Founder of My Street Grocery</p>
<p class="text footerTextSlides invisible" id="footer3">“I love visiting the place where something’s being grown. The people who are growing that food are almost always the most passionate and know so much about what they’re growing.”<br/> — Elly Truesdell, WFM Team Member</p>
<p class="text footerTextSlides invisible" id="footer4">“If we change the way we eat, if we change the way we treat the ocean…we can move out of the direction we’re going into a new wave of sustainability. It’s in all the little individual decisions we make every day.”<br/> — Cole Meeker, Co-Founder of The Great and Wonderful Sea of Change Trading Co.</p>

<script>
var quotes = ["#footer1", "#footer2", "#footer3", "#footer4"];
var randomQuoteChoice = Math.floor(Math.random()*quotes.length);
jQuery(quotes[randomQuoteChoice]).removeClass(\'invisible\');
</script>',
      'format' => 'source_editor',
    ),
    'additional_classes' => '',
  );
  $export['bc_mobile_twitter'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'bc_mobile_header';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Brand Camp Mobile: Header Image';
  $box->options = array(
    'body' => array(
      'value' => '<p>
<script>
jQuery.urlParam = function(name){
  var results = new RegExp(\'[\?&]\' + name + \'=([^&#]*)\').exec(window.location.href);
  if (results==null){
    return null;
  }
  else{
    return results[1] || 0;
  }
}
var headerMapping = {
  seafood:     \'grandpafisherman.jpg\',
  produce:   \'farmerfrank.jpg\',
};

var images = [
  \'chickenlady.jpg\',
  \'cows.jpg\',
  \'farmerfrank.jpg\',
  \'fedora.jpg\',
  \'grandpafisherman.jpg\',
  \'herbguy.jpg\',
];

var headerImage = images[Math.floor(Math.random()*images.length)];

//check if we need to override header image
var bc = jQuery.urlParam(\'bc\');
if (bc != null && headerMapping.hasOwnProperty(bc)) {
  headerImage = headerMapping[bc];
}
document.write(\'<img height="820" src="//assets.wholefoodsmarket.com/www/values-matter/mobile-header-images/\' + headerImage + \'" width="640">\');
</script>
</p>
<p class="text"><img src="/sites/all/themes/brand_camp_theme/images/intro.png"></p>
<p class="text animated headerTextSlides" id="header1">Calling all challengers and cheese lovers </p>
<p class="text animated headerTextSlides bounceOutLeft invisible" id="header2">Calling all foragers and big thinkers</p>
<p class="text animated headerTextSlides bounceOutLeft invisible" id="header3">Calling the kale curious</p>
<p class="text animated headerTextSlides bounceOutLeft invisible" id="header4">Calling all naturalists and ringleaders </p>
',
      'format' => 'source_editor',
    ),
    'additional_classes' => '',
  );
  $export['bc_mobile_header'] = $box;

  return $export;
}
