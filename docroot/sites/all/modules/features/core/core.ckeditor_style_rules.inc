<?php
/**
 * @file
 * core.ckeditor_style_rules.inc
 */

/**
 * Implements hook_ckeditor_style_rule_default().
 */
function core_ckeditor_style_rule_default() {
  $export = array();

  $ckeditor_style_rule = new stdClass();
  $ckeditor_style_rule->disabled = FALSE; /* Edit this to true to make a default ckeditor_style_rule disabled initially */
  $ckeditor_style_rule->api_version = 1;
  $ckeditor_style_rule->rid = '2';
  $ckeditor_style_rule->name = 'Address';
  $ckeditor_style_rule->machine_name = 'address';
  $ckeditor_style_rule->styleset = 'full_html';
  $ckeditor_style_rule->description = '';
  $ckeditor_style_rule->element = 'address';
  $ckeditor_style_rule->attributes = array(
    'class' => '',
    'style' => '',
  );
  $export['address'] = $ckeditor_style_rule;

  $ckeditor_style_rule = new stdClass();
  $ckeditor_style_rule->disabled = FALSE; /* Edit this to true to make a default ckeditor_style_rule disabled initially */
  $ckeditor_style_rule->api_version = 1;
  $ckeditor_style_rule->rid = '12';
  $ckeditor_style_rule->name = 'Cursive Font';
  $ckeditor_style_rule->machine_name = 'cursive_font';
  $ckeditor_style_rule->styleset = 'full_html';
  $ckeditor_style_rule->description = '';
  $ckeditor_style_rule->element = 'span';
  $ckeditor_style_rule->attributes = array(
    'class' => 'cursivefont',
    'style' => '',
  );
  $export['cursive_font'] = $ckeditor_style_rule;

  $ckeditor_style_rule = new stdClass();
  $ckeditor_style_rule->disabled = FALSE; /* Edit this to true to make a default ckeditor_style_rule disabled initially */
  $ckeditor_style_rule->api_version = 1;
  $ckeditor_style_rule->rid = '3';
  $ckeditor_style_rule->name = 'Formatted';
  $ckeditor_style_rule->machine_name = 'formatted';
  $ckeditor_style_rule->styleset = 'full_html';
  $ckeditor_style_rule->description = '';
  $ckeditor_style_rule->element = 'pre';
  $ckeditor_style_rule->attributes = array(
    'class' => '',
    'style' => '',
  );
  $export['formatted'] = $ckeditor_style_rule;

  $ckeditor_style_rule = new stdClass();
  $ckeditor_style_rule->disabled = FALSE; /* Edit this to true to make a default ckeditor_style_rule disabled initially */
  $ckeditor_style_rule->api_version = 1;
  $ckeditor_style_rule->rid = '11';
  $ckeditor_style_rule->name = 'Heading1';
  $ckeditor_style_rule->machine_name = 'heading1';
  $ckeditor_style_rule->styleset = 'full_html';
  $ckeditor_style_rule->description = '';
  $ckeditor_style_rule->element = 'span';
  $ckeditor_style_rule->attributes = array(
    'class' => 'header1',
    'style' => '',
  );
  $export['heading1'] = $ckeditor_style_rule;

  $ckeditor_style_rule = new stdClass();
  $ckeditor_style_rule->disabled = FALSE; /* Edit this to true to make a default ckeditor_style_rule disabled initially */
  $ckeditor_style_rule->api_version = 1;
  $ckeditor_style_rule->rid = '4';
  $ckeditor_style_rule->name = 'Heading 1';
  $ckeditor_style_rule->machine_name = 'heading_1';
  $ckeditor_style_rule->styleset = 'full_html';
  $ckeditor_style_rule->description = '';
  $ckeditor_style_rule->element = 'h1';
  $ckeditor_style_rule->attributes = array(
    'class' => '',
    'style' => '',
  );
  $export['heading_1'] = $ckeditor_style_rule;

  $ckeditor_style_rule = new stdClass();
  $ckeditor_style_rule->disabled = FALSE; /* Edit this to true to make a default ckeditor_style_rule disabled initially */
  $ckeditor_style_rule->api_version = 1;
  $ckeditor_style_rule->rid = '5';
  $ckeditor_style_rule->name = 'Heading 2';
  $ckeditor_style_rule->machine_name = 'heading_2';
  $ckeditor_style_rule->styleset = 'full_html';
  $ckeditor_style_rule->description = '';
  $ckeditor_style_rule->element = 'h2';
  $ckeditor_style_rule->attributes = array(
    'class' => '',
    'style' => '',
  );
  $export['heading_2'] = $ckeditor_style_rule;

  $ckeditor_style_rule = new stdClass();
  $ckeditor_style_rule->disabled = FALSE; /* Edit this to true to make a default ckeditor_style_rule disabled initially */
  $ckeditor_style_rule->api_version = 1;
  $ckeditor_style_rule->rid = '6';
  $ckeditor_style_rule->name = 'Heading 3';
  $ckeditor_style_rule->machine_name = 'heading_3';
  $ckeditor_style_rule->styleset = 'full_html';
  $ckeditor_style_rule->description = '';
  $ckeditor_style_rule->element = 'h3';
  $ckeditor_style_rule->attributes = array(
    'class' => '',
    'style' => '',
  );
  $export['heading_3'] = $ckeditor_style_rule;

  $ckeditor_style_rule = new stdClass();
  $ckeditor_style_rule->disabled = FALSE; /* Edit this to true to make a default ckeditor_style_rule disabled initially */
  $ckeditor_style_rule->api_version = 1;
  $ckeditor_style_rule->rid = '7';
  $ckeditor_style_rule->name = 'Heading 4';
  $ckeditor_style_rule->machine_name = 'heading_4';
  $ckeditor_style_rule->styleset = 'full_html';
  $ckeditor_style_rule->description = '';
  $ckeditor_style_rule->element = 'h4';
  $ckeditor_style_rule->attributes = array(
    'class' => '',
    'style' => '',
  );
  $export['heading_4'] = $ckeditor_style_rule;

  $ckeditor_style_rule = new stdClass();
  $ckeditor_style_rule->disabled = FALSE; /* Edit this to true to make a default ckeditor_style_rule disabled initially */
  $ckeditor_style_rule->api_version = 1;
  $ckeditor_style_rule->rid = '8';
  $ckeditor_style_rule->name = 'Heading 5';
  $ckeditor_style_rule->machine_name = 'heading_5';
  $ckeditor_style_rule->styleset = 'full_html';
  $ckeditor_style_rule->description = '';
  $ckeditor_style_rule->element = 'h5';
  $ckeditor_style_rule->attributes = array(
    'class' => '',
    'style' => '',
  );
  $export['heading_5'] = $ckeditor_style_rule;

  $ckeditor_style_rule = new stdClass();
  $ckeditor_style_rule->disabled = FALSE; /* Edit this to true to make a default ckeditor_style_rule disabled initially */
  $ckeditor_style_rule->api_version = 1;
  $ckeditor_style_rule->rid = '9';
  $ckeditor_style_rule->name = 'Heading 6';
  $ckeditor_style_rule->machine_name = 'heading_6';
  $ckeditor_style_rule->styleset = 'full_html';
  $ckeditor_style_rule->description = '';
  $ckeditor_style_rule->element = 'h6';
  $ckeditor_style_rule->attributes = array(
    'class' => '',
    'style' => '',
  );
  $export['heading_6'] = $ckeditor_style_rule;

  $ckeditor_style_rule = new stdClass();
  $ckeditor_style_rule->disabled = FALSE; /* Edit this to true to make a default ckeditor_style_rule disabled initially */
  $ckeditor_style_rule->api_version = 1;
  $ckeditor_style_rule->rid = '1';
  $ckeditor_style_rule->name = 'Normal';
  $ckeditor_style_rule->machine_name = 'normal';
  $ckeditor_style_rule->styleset = 'full_html';
  $ckeditor_style_rule->description = '';
  $ckeditor_style_rule->element = 'p';
  $ckeditor_style_rule->attributes = array(
    'class' => '',
    'style' => '',
  );
  $export['normal'] = $ckeditor_style_rule;

  $ckeditor_style_rule = new stdClass();
  $ckeditor_style_rule->disabled = FALSE; /* Edit this to true to make a default ckeditor_style_rule disabled initially */
  $ckeditor_style_rule->api_version = 1;
  $ckeditor_style_rule->rid = '10';
  $ckeditor_style_rule->name = 'Normal (DIV)';
  $ckeditor_style_rule->machine_name = 'normal_div';
  $ckeditor_style_rule->styleset = 'full_html';
  $ckeditor_style_rule->description = '';
  $ckeditor_style_rule->element = 'div';
  $ckeditor_style_rule->attributes = array(
    'class' => '',
    'style' => '',
  );
  $export['normal_div'] = $ckeditor_style_rule;

  return $export;
}
