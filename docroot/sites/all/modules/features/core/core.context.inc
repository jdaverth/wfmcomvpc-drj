<?php
/**
 * @file
 * core.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function core_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'footer_nav';
  $context->description = '';
  $context->tag = 'Global';
  $context->conditions = array(
    'theme' => array(
      'values' => array(
        'wholefoods' => 'wholefoods',
      ),
    ),
    'path' => array(
      'values' => array(
        '~admin' => '~admin',
        '~admin/*' => '~admin/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'wfm_blocks-wfm_blocks_footer' => array(
          'module' => 'wfm_blocks',
          'delta' => 'wfm_blocks_footer',
          'region' => 'footer',
          'weight' => NULL,
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Global');
  $export['footer_nav'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'front_page';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-marquee_scroller-block' => array(
          'module' => 'views',
          'delta' => 'marquee_scroller-block',
          'region' => 'marquee',
          'weight' => '-10',
        ),
        'views-promo_blocks-homepage' => array(
          'module' => 'views',
          'delta' => 'promo_blocks-homepage',
          'region' => 'content',
          'weight' => '-8',
        ),
        'views-promo_blocks-homepage_local' => array(
          'module' => 'views',
          'delta' => 'promo_blocks-homepage_local',
          'region' => 'content',
          'weight' => '-7',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['front_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'generic_page';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'page' => 'page',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'theme' => array(
      'values' => array(
        'wholefoods' => 'wholefoods',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-generic_page_items-marqee' => array(
          'module' => 'views',
          'delta' => 'generic_page_items-marqee',
          'region' => 'marquee',
          'weight' => NULL,
        ),
        'views-generic_page_items-marquee_box' => array(
          'module' => 'views',
          'delta' => 'generic_page_items-marquee_box',
          'region' => 'content_top',
          'weight' => NULL,
        ),
        'views-generic_page_items-block_2' => array(
          'module' => 'views',
          'delta' => 'generic_page_items-block_2',
          'region' => 'content',
          'weight' => NULL,
        ),
        'views-generic_page_items-list_copy' => array(
          'module' => 'views',
          'delta' => 'generic_page_items-list_copy',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;
  $export['generic_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'lplp_recipients_profiles';
  $context->description = '';
  $context->tag = 'Local Vendors';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'profiles-loan-recipients/*' => 'profiles-loan-recipients/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-lplp_recipient_profiles-block' => array(
          'module' => 'views',
          'delta' => 'lplp_recipient_profiles-block',
          'region' => 'content',
          'weight' => NULL,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Local Vendors');
  $export['lplp_recipients_profiles'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'lplp_recipients_profiles_landing';
  $context->description = '';
  $context->tag = 'Local Vendors';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'profiles-loan-recipients' => 'profiles-loan-recipients',
      ),
    ),
    'theme' => array(
      'values' => array(
        'wholefoods_mobile' => 'wholefoods_mobile',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'wfm_blocks-wfm_blocks_vendors' => array(
          'module' => 'wfm_blocks',
          'delta' => 'wfm_blocks_vendors',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Local Vendors');
  $export['lplp_recipients_profiles_landing'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'page_headers';
  $context->description = '';
  $context->tag = 'Global';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~admin' => '~admin',
        '~admin/*' => '~admin/*',
      ),
    ),
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-marquee_page-block' => array(
          'module' => 'views',
          'delta' => 'marquee_page-block',
          'region' => 'marquee',
          'weight' => NULL,
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Global');
  $export['page_headers'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'related_pages';
  $context->description = '';
  $context->tag = 'Global';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~admin' => '~admin',
        '~admin/*' => '~admin/*',
      ),
    ),
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-related_content-block' => array(
          'module' => 'views',
          'delta' => 'related_content-block',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Global');
  $export['related_pages'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'site_errors';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'page-not-found' => 'page-not-found',
      ),
    ),
  );
  $context->reactions = array(
    'theme_html' => array(
      'class' => 'sans-marquee',
    ),
  );
  $context->condition_mode = 0;
  $export['site_errors'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'video_page';
  $context->description = '';
  $context->tag = 'Video';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'videos' => 'videos',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-videos-featured_header' => array(
          'module' => 'views',
          'delta' => 'videos-featured_header',
          'region' => 'content_top',
          'weight' => '-10',
        ),
        'quicktabs-videos' => array(
          'module' => 'quicktabs',
          'delta' => 'videos',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Video');
  $export['video_page'] = $context;

  return $export;
}
