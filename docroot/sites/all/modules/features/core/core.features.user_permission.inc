<?php
/**
 * @file
 * core.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function core_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer fastly'.
  $permissions['administer fastly'] = array(
    'name' => 'administer fastly',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'fastly',
  );

  // Exported permission: 'show format selection for comment'.
  $permissions['show format selection for comment'] = array(
    'name' => 'show format selection for comment',
    'roles' => array(
      'Blog Admin' => 'Blog Admin',
      'Blog Author' => 'Blog Author',
      'Global Content Admin' => 'Global Content Admin',
      'Local Content Admin' => 'Local Content Admin',
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'better_formats',
  );

  // Exported permission: 'show format selection for field_collection_item'.
  $permissions['show format selection for field_collection_item'] = array(
    'name' => 'show format selection for field_collection_item',
    'roles' => array(
      'Blog Admin' => 'Blog Admin',
      'Blog Author' => 'Blog Author',
      'Global Content Admin' => 'Global Content Admin',
      'Local Content Admin' => 'Local Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'better_formats',
  );

  // Exported permission: 'show format selection for file'.
  $permissions['show format selection for file'] = array(
    'name' => 'show format selection for file',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'better_formats',
  );

  // Exported permission: 'show format selection for node'.
  $permissions['show format selection for node'] = array(
    'name' => 'show format selection for node',
    'roles' => array(
      'Blog Admin' => 'Blog Admin',
      'Blog Author' => 'Blog Author',
      'Global Content Admin' => 'Global Content Admin',
      'Local Content Admin' => 'Local Content Admin',
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'better_formats',
  );

  // Exported permission: 'show format selection for profile2'.
  $permissions['show format selection for profile2'] = array(
    'name' => 'show format selection for profile2',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'better_formats',
  );

  // Exported permission: 'show format selection for rules_config'.
  $permissions['show format selection for rules_config'] = array(
    'name' => 'show format selection for rules_config',
    'roles' => array(
      'Blog Admin' => 'Blog Admin',
      'Blog Author' => 'Blog Author',
      'Global Content Admin' => 'Global Content Admin',
      'Local Content Admin' => 'Local Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'better_formats',
  );

  // Exported permission: 'show format selection for taxonomy_term'.
  $permissions['show format selection for taxonomy_term'] = array(
    'name' => 'show format selection for taxonomy_term',
    'roles' => array(
      'Blog Admin' => 'Blog Admin',
      'Blog Author' => 'Blog Author',
      'Global Content Admin' => 'Global Content Admin',
      'Local Content Admin' => 'Local Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'better_formats',
  );

  // Exported permission: 'show format selection for user'.
  $permissions['show format selection for user'] = array(
    'name' => 'show format selection for user',
    'roles' => array(
      'Blog Admin' => 'Blog Admin',
      'Blog Author' => 'Blog Author',
      'Global Content Admin' => 'Global Content Admin',
      'Local Content Admin' => 'Local Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'better_formats',
  );

  // Exported permission: 'show format tips'.
  $permissions['show format tips'] = array(
    'name' => 'show format tips',
    'roles' => array(
      'Blog Admin' => 'Blog Admin',
      'Blog Author' => 'Blog Author',
      'Global Content Admin' => 'Global Content Admin',
      'Local Content Admin' => 'Local Content Admin',
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'better_formats',
  );

  // Exported permission: 'show more format tips link'.
  $permissions['show more format tips link'] = array(
    'name' => 'show more format tips link',
    'roles' => array(
      'Blog Admin' => 'Blog Admin',
      'Blog Author' => 'Blog Author',
      'Global Content Admin' => 'Global Content Admin',
      'Local Content Admin' => 'Local Content Admin',
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'better_formats',
  );

  return $permissions;
}
