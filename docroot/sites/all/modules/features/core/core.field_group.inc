<?php
/**
 * @file
 * core.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function core_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_blog_widget|field_collection_item|field_related_content|form';
  $field_group->group_name = 'group_blog_widget';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_related_content';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Blog Widget',
    'weight' => '34',
    'children' => array(
      0 => 'field_related_blog_headline',
      1 => 'field_related_blog_categories',
      2 => 'field_blog',
      3 => 'field_related_blog_count',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_blog_widget|field_collection_item|field_related_content|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_body_copy|node|page|form';
  $field_group->group_name = 'group_body_copy';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_body';
  $field_group->data = array(
    'label' => 'Copy',
    'weight' => '44',
    'children' => array(
      0 => 'body',
      1 => 'group_body_mobile',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_body_copy|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_body_free_form|node|page|form';
  $field_group->group_name = 'group_body_free_form';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_body';
  $field_group->data = array(
    'label' => 'Free Form',
    'weight' => '47',
    'children' => array(
      0 => 'field_body_free_form',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_body_free_form|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_body_list|node|page|form';
  $field_group->group_name = 'group_body_list';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_body';
  $field_group->data = array(
    'label' => 'List',
    'weight' => '46',
    'children' => array(
      0 => 'field_body_list_expand',
      1 => 'field_body_list_fc',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_body_list|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_body_mobile|node|page|form';
  $field_group->group_name = 'group_body_mobile';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_body_copy';
  $field_group->data = array(
    'label' => 'Mobile Body',
    'weight' => '48',
    'children' => array(
      0 => 'field_body_mobile',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Mobile Body',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_body_mobile|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_body|node|page|form';
  $field_group->group_name = 'group_body';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_layout';
  $field_group->data = array(
    'label' => 'Body',
    'weight' => '11',
    'children' => array(
      0 => 'field_page_body_layout',
      1 => 'group_body_copy',
      2 => 'group_body_free_form',
      3 => 'group_body_list',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_body|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_by_page|field_collection_item|field_related_content|form';
  $field_group->group_name = 'group_by_page';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_related_content';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'By Individual Page',
    'weight' => '2',
    'children' => array(
      0 => 'field_related_content_node',
      1 => 'field_related_content_image',
      2 => 'field_related_content_copy',
      3 => 'field_related_content_link_copy',
      4 => 'field_related_page_title',
      5 => 'field_link_by',
      6 => 'field_related_link',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'By Individual Page',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_by_page|field_collection_item|field_related_content|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_by_view|field_collection_item|field_related_content|form';
  $field_group->group_name = 'group_by_view';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_related_content';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'By View',
    'weight' => '11',
    'children' => array(
      0 => 'field_related_content_view',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_by_view|field_collection_item|field_related_content|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_heading_intro_content|node|page|form';
  $field_group->group_name = 'group_heading_intro_content';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_heading';
  $field_group->data = array(
    'label' => 'Intro Heading Content',
    'weight' => '7',
    'children' => array(),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_heading_intro_content|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_heading_marquee_box_mobile|node|page|form';
  $field_group->group_name = 'group_heading_marquee_box_mobile';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_heading_marquee_box';
  $field_group->data = array(
    'label' => 'Mobile Marquee Box',
    'weight' => '6',
    'children' => array(
      0 => 'field_page_intro_copy_mobile',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Mobile Marquee Box',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_heading_marquee_box_mobile|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_heading_marquee_box|node|page|form';
  $field_group->group_name = 'group_heading_marquee_box';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_heading';
  $field_group->data = array(
    'label' => 'Marquee Box',
    'weight' => '5',
    'children' => array(
      0 => 'field_page_intro_copy',
      1 => 'field_page_intro_heading',
      2 => 'field_page_intro_image',
      3 => 'field_page_intro_subheading',
      4 => 'group_heading_marquee_box_mobile',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Marquee Box',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => 'This option will allow you to add heading images, text and copy to a bordered box that will display in the marquee section.

<div><a style="display:inline-block" rel="lightbox" href="/sites/all/themes/wholefoods/images/marqee_box.jpg"><img src="/sites/all/themes/wholefoods/images/marqee_box_thumb.jpg"><br>Click To Enlarge</a></div>

<div><a rel="lightbox" style="display: inline-block" href="/sites/all/themes/wholefoods/images/marqee_box_anotated.jpg"><img src="/sites/all/themes/wholefoods/images/marqee_box_anotated_thumb.jpg"><br>Click To Enlarge</a></div>',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_heading_marquee_box|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_heading_marquee_content|node|page|form';
  $field_group->group_name = 'group_heading_marquee_content';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_heading';
  $field_group->data = array(
    'label' => 'Marquee Heading Content',
    'weight' => '8',
    'children' => array(
      0 => 'field_page_marquee_copy',
      1 => 'field_page_marquee_heading',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_heading_marquee_content|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_heading|node|page|form';
  $field_group->group_name = 'group_heading';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_layout';
  $field_group->data = array(
    'label' => 'Heading',
    'weight' => '10',
    'children' => array(
      0 => 'field_heading_background',
      1 => 'field_page_heading_layout',
      2 => 'group_heading_intro_content',
      3 => 'group_heading_marquee_box',
      4 => 'group_heading_marquee_content',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_heading|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_layout|node|page|form';
  $field_group->group_name = 'group_layout';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_page_tabs';
  $field_group->data = array(
    'label' => 'Layout',
    'weight' => '6',
    'children' => array(
      0 => 'field_page_tags',
      1 => 'field_allow_local_promos',
      2 => 'field_add_share_button_to_marque',
      3 => 'title',
      4 => 'group_body',
      5 => 'group_heading',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_layout|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_page_basics|node|page|form';
  $field_group->group_name = 'group_page_basics';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_page_tabs';
  $field_group->data = array(
    'label' => 'Basics',
    'weight' => '35',
    'children' => array(
      0 => 'body',
      1 => 'field_promo_category',
      2 => 'field_page_tags',
      3 => 'field_page_layout',
      4 => 'field_page_subtitle',
      5 => 'metatags',
      6 => 'title',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_page_basics|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_page_freeform|node|page|form';
  $field_group->group_name = 'group_page_freeform';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_page_tabs';
  $field_group->data = array(
    'label' => 'Free Form',
    'weight' => '37',
    'children' => array(
      0 => 'field_free_form',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_page_freeform|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_page_marquee|node|page|form';
  $field_group->group_name = 'group_page_marquee';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_page_tabs';
  $field_group->data = array(
    'label' => 'Marquee',
    'weight' => '38',
    'children' => array(
      0 => 'field_heading_background',
      1 => 'field_heading_copy',
      2 => 'field_heading_title',
      3 => 'group_heading',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_page_marquee|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_page_navigation|node|page|form';
  $field_group->group_name = 'group_page_navigation';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_page_tabs';
  $field_group->data = array(
    'label' => 'Navigation',
    'weight' => '8',
    'children' => array(
      0 => 'field_nav_title_rewrite',
      1 => 'field_sub_nav_view',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_page_navigation|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_page_related|node|page|form';
  $field_group->group_name = 'group_page_related';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_page_tabs';
  $field_group->data = array(
    'label' => 'Related Content',
    'weight' => '7',
    'children' => array(
      0 => 'field_related_content',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_page_related|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_page_tabs|node|page|form';
  $field_group->group_name = 'group_page_tabs';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Page',
    'weight' => '3',
    'children' => array(
      0 => 'locations_acl',
      1 => 'remove_from_search_field',
      2 => 'group_layout',
      3 => 'group_page_navigation',
      4 => 'group_page_related',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $field_groups['group_page_tabs|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_torn_edges|node|page|default';
  $field_group->group_name = 'group_torn_edges';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Torn Edges',
    'weight' => '0',
    'children' => array(
      0 => 'body',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => '',
      ),
    ),
  );
  $field_groups['group_torn_edges|node|page|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_video_basics|node|video|form';
  $field_group->group_name = 'group_video_basics';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'video';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_video';
  $field_group->data = array(
    'label' => 'Basics',
    'weight' => '102',
    'children' => array(
      0 => 'body',
      1 => 'field_video',
      2 => 'field_video_dislike_count',
      3 => 'field_video_duration',
      4 => 'field_video_favorite_count',
      5 => 'field_video_featured',
      6 => 'field_video_keywords',
      7 => 'field_video_like_count',
      8 => 'field_video_playlists',
      9 => 'field_video_rating_avg',
      10 => 'field_video_rating_count',
      11 => 'field_video_view_count',
      12 => 'field_video_youtube_published',
      13 => 'field_video_youtube_updated',
      14 => 'field_video_ytid',
      15 => 'field_related_content_node',
      16 => 'field_video_related_content',
      17 => 'title',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_video_basics|node|video|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_video|node|video|form';
  $field_group->group_name = 'group_video';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'video';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Video',
    'weight' => '0',
    'children' => array(
      0 => 'locations_acl',
      1 => 'group_video_basics',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $field_groups['group_video|node|video|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Basics');
  t('Blog Widget');
  t('Body');
  t('By Individual Page');
  t('By View');
  t('Copy');
  t('Free Form');
  t('Heading');
  t('Intro Heading Content');
  t('Layout');
  t('List');
  t('Marquee');
  t('Marquee Box');
  t('Marquee Heading Content');
  t('Mobile Body');
  t('Mobile Marquee Box');
  t('Navigation');
  t('Page');
  t('Related Content');
  t('Torn Edges');
  t('Video');

  return $field_groups;
}
