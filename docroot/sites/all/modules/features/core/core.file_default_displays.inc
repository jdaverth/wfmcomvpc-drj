<?php
/**
 * @file
 * core.file_default_displays.inc
 */

/**
 * Implements hook_file_default_displays().
 */
function core_file_default_displays() {
  $export = array();

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'application__file_styles_featured_video__file_field_styles_file_featured_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['application__file_styles_featured_video__file_field_styles_file_featured_video'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'application__file_styles_header_hero__file_field_styles_file_header_hero';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['application__file_styles_header_hero__file_field_styles_file_header_hero'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'application__file_styles_medium__file_field_styles_file_medium';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['application__file_styles_medium__file_field_styles_file_medium'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'application__file_styles_newsletter_image__file_field_styles_file_newsletter_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['application__file_styles_newsletter_image__file_field_styles_file_newsletter_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'application__file_styles_product_hero__file_field_styles_file_product_hero';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['application__file_styles_product_hero__file_field_styles_file_product_hero'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'application__file_styles_promo__file_field_styles_file_promo';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['application__file_styles_promo__file_field_styles_file_promo'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'application__file_styles_related_image__file_field_styles_file_related_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['application__file_styles_related_image__file_field_styles_file_related_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'application__file_styles_small_pod__file_field_styles_file_small_pod';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['application__file_styles_small_pod__file_field_styles_file_small_pod'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'application__file_styles_square_thumbnail__file_field_styles_file_square_thumbnail';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['application__file_styles_square_thumbnail__file_field_styles_file_square_thumbnail'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'application__file_styles_thumbnail__file_field_styles_file_thumbnail';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['application__file_styles_thumbnail__file_field_styles_file_thumbnail'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__file_styles_featured_video__file_field_styles_file_featured_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['audio__file_styles_featured_video__file_field_styles_file_featured_video'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__file_styles_header_hero__file_field_styles_file_header_hero';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['audio__file_styles_header_hero__file_field_styles_file_header_hero'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__file_styles_large__file_field_styles_file_large';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['audio__file_styles_large__file_field_styles_file_large'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__file_styles_medium__file_field_styles_file_medium';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['audio__file_styles_medium__file_field_styles_file_medium'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__file_styles_original__file_field_styles_file_original';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['audio__file_styles_original__file_field_styles_file_original'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__file_styles_product_hero__file_field_styles_file_product_hero';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['audio__file_styles_product_hero__file_field_styles_file_product_hero'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__file_styles_promo__file_field_styles_file_promo';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['audio__file_styles_promo__file_field_styles_file_promo'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__file_styles_small_pod__file_field_styles_file_small_pod';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['audio__file_styles_small_pod__file_field_styles_file_small_pod'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__file_styles_square_thumbnail__file_field_styles_file_square_thumbnail';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['audio__file_styles_square_thumbnail__file_field_styles_file_square_thumbnail'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__file_styles_thumbnail__file_field_styles_file_thumbnail';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['audio__file_styles_thumbnail__file_field_styles_file_thumbnail'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'default__file_styles_featured_video__file_field_styles_file_featured_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['default__file_styles_featured_video__file_field_styles_file_featured_video'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'default__file_styles_header_hero__file_field_styles_file_header_hero';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['default__file_styles_header_hero__file_field_styles_file_header_hero'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'default__file_styles_large__file_field_styles_file_large';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['default__file_styles_large__file_field_styles_file_large'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'default__file_styles_medium__file_field_styles_file_medium';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['default__file_styles_medium__file_field_styles_file_medium'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'default__file_styles_original__file_field_styles_file_original';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['default__file_styles_original__file_field_styles_file_original'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'default__file_styles_product_hero__file_field_styles_file_product_hero';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['default__file_styles_product_hero__file_field_styles_file_product_hero'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'default__file_styles_promo__file_field_styles_file_promo';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['default__file_styles_promo__file_field_styles_file_promo'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'default__file_styles_small_pod__file_field_styles_file_small_pod';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['default__file_styles_small_pod__file_field_styles_file_small_pod'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'default__file_styles_square_thumbnail__file_field_styles_file_square_thumbnail';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['default__file_styles_square_thumbnail__file_field_styles_file_square_thumbnail'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'default__file_styles_thumbnail__file_field_styles_file_thumbnail';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['default__file_styles_thumbnail__file_field_styles_file_thumbnail'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__file_styles_featured_video__file_field_styles_file_featured_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['image__file_styles_featured_video__file_field_styles_file_featured_video'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__file_styles_header_hero__file_field_styles_file_header_hero';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['image__file_styles_header_hero__file_field_styles_file_header_hero'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__file_styles_large__file_field_styles_file_large';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['image__file_styles_large__file_field_styles_file_large'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__file_styles_medium__file_field_styles_file_medium';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['image__file_styles_medium__file_field_styles_file_medium'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__file_styles_original__file_field_styles_file_original';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['image__file_styles_original__file_field_styles_file_original'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__file_styles_product_hero__file_field_styles_file_product_hero';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['image__file_styles_product_hero__file_field_styles_file_product_hero'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__file_styles_promo__file_field_styles_file_promo';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['image__file_styles_promo__file_field_styles_file_promo'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__file_styles_small_pod__file_field_styles_file_small_pod';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['image__file_styles_small_pod__file_field_styles_file_small_pod'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__file_styles_square_thumbnail__file_field_styles_file_square_thumbnail';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['image__file_styles_square_thumbnail__file_field_styles_file_square_thumbnail'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__file_styles_thumbnail__file_field_styles_file_thumbnail';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['image__file_styles_thumbnail__file_field_styles_file_thumbnail'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'text__file_styles_featured_video__file_field_styles_file_featured_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['text__file_styles_featured_video__file_field_styles_file_featured_video'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'text__file_styles_header_hero__file_field_styles_file_header_hero';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['text__file_styles_header_hero__file_field_styles_file_header_hero'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'text__file_styles_medium__file_field_styles_file_medium';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['text__file_styles_medium__file_field_styles_file_medium'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'text__file_styles_product_hero__file_field_styles_file_product_hero';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['text__file_styles_product_hero__file_field_styles_file_product_hero'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'text__file_styles_promo__file_field_styles_file_promo';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['text__file_styles_promo__file_field_styles_file_promo'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'text__file_styles_small_pod__file_field_styles_file_small_pod';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['text__file_styles_small_pod__file_field_styles_file_small_pod'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'text__file_styles_square_thumbnail__file_field_styles_file_square_thumbnail';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['text__file_styles_square_thumbnail__file_field_styles_file_square_thumbnail'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'text__file_styles_thumbnail__file_field_styles_file_thumbnail';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['text__file_styles_thumbnail__file_field_styles_file_thumbnail'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__file_styles_featured_video__file_field_styles_file_featured_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['video__file_styles_featured_video__file_field_styles_file_featured_video'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__file_styles_header_hero__file_field_styles_file_header_hero';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['video__file_styles_header_hero__file_field_styles_file_header_hero'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__file_styles_large__file_field_styles_file_large';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['video__file_styles_large__file_field_styles_file_large'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__file_styles_medium__file_field_styles_file_medium';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['video__file_styles_medium__file_field_styles_file_medium'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__file_styles_original__file_field_styles_file_original';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['video__file_styles_original__file_field_styles_file_original'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__file_styles_product_hero__file_field_styles_file_product_hero';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['video__file_styles_product_hero__file_field_styles_file_product_hero'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__file_styles_promo__file_field_styles_file_promo';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = '';
  $export['video__file_styles_promo__file_field_styles_file_promo'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__file_styles_promo__media_youtube_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'version' => '3',
    'theme' => 'dark',
    'width' => '430',
    'height' => '286',
    'fullscreen' => 1,
    'autoplay' => 0,
    'related' => 1,
    'hd' => 0,
    'showsearch' => 1,
    'modestbranding' => 0,
    'showinfo' => 1,
    'chromeless' => 0,
  );
  $export['video__file_styles_promo__media_youtube_video'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__file_styles_small_pod__file_field_styles_file_small_pod';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['video__file_styles_small_pod__file_field_styles_file_small_pod'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__file_styles_square_thumbnail__file_field_styles_file_square_thumbnail';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['video__file_styles_square_thumbnail__file_field_styles_file_square_thumbnail'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__file_styles_thumbnail__file_field_styles_file_thumbnail';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['video__file_styles_thumbnail__file_field_styles_file_thumbnail'] = $file_display;

  return $export;
}
