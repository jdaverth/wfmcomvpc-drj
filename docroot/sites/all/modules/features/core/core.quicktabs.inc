<?php
/**
 * @file
 * core.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function core_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'videos';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Videos';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'videos',
      'display' => 'featured',
      'args' => '',
      'title' => 'Featured',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'videos',
      'display' => 'latest',
      'args' => '',
      'title' => 'Latest',
      'weight' => '-99',
      'type' => 'view',
    ),
    2 => array(
      'vid' => 'videos',
      'display' => 'category',
      'args' => '',
      'title' => 'Category',
      'weight' => '-98',
      'type' => 'view',
    ),
    3 => array(
      'vid' => 'videos',
      'display' => 'block_1',
      'args' => '',
      'title' => 'Search',
      'weight' => '-97',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Category');
  t('Featured');
  t('Latest');
  t('Search');
  t('Videos');

  $export['videos'] = $quicktabs;

  return $export;
}
