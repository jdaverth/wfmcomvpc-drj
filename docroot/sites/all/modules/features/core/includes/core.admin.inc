<?php


/**
 * Form constructor for the Default National Office form.
 */
function core_default_national_office_form($form, &$form_state) {
  $form['national_office'] = array(
    '#type' => 'radios',
    '#title' => t('National Office'),
    '#options' => core_national_office_list(),
    '#default_value' => variable_get('default_national_office'),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}


/**
 * Form submission handler for core_default_national_office_form().
 */
function core_default_national_office_form_submit($form, &$form_state) {
  variable_set('default_national_office', $form_state['values']['national_office']);
  drupal_set_message(t('Default national office settings have been saved.'));
}


/**
 * Returns a list of national offices.
 */
function core_national_office_list() {
  $data = db_select('node', 'N')
    ->fields('N', array('nid', 'title'))
    ->condition('N.type', 'national_offices')
    ->condition('N.status', 1)
    ->orderBy('N.title', 'ASC')
    ->execute()
    ->fetchAllKeyed();
  foreach ($data as $nid => &$title) {
    $title .= ' (' . l(t('View'), 'node/' . $nid, array('attributes' => array('target' => '_blank'))) . ')';
  }

  return $data;
}
