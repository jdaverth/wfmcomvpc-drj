<?php

/**
 * @file
 * Custom Tokens using "Tokens API" API (as opposed to the Tokens API - whatever!).
 */

/**
 * Implements hook_api_tokens_info().
 */
function core_api_tokens_info() {
  $tokens = array();

  $tokens['view'] = array(
    'title' => t('View'),
    'description' => t('Renders a view.'),
    // Don't set caching for this token. Will use views' caching.
  );
  $tokens['facebook-feed'] = array(
    'title' => t('Facebook Feed'),
    'description' => t('Displays a feed of store facebook posts.'),
  );
  $tokens['twitter-feed'] = array(
    'title' => t('Twitter Feed'),
    'description' => t('Displays a feed of store twitter posts.'),
  );

  $tokens['store_hours_link'] = array(
    'title' => t('Store Hours Link'),
    'description' => t('Displays store hours link for select store if exists.'),
  );

  $tokens['cooking_classes_link'] = array(
    'title' => t('Cooking Classes Link'),
    'description' => t('Displays cooking classes link'),
  );

  $tokens['upcoming_events_link'] = array(
    'title' => t('Upcoming Events Link'),
    'description' => t('Displays upcoming events link.'),
  );

  $tokens['weekly_flyer_link'] = array(
    'title' => t('Weekly Flyer Link'),
    'description' => t('Displays weekly flyer link.'),
  );

  $tokens['make_this_my_store'] = array(
    'title' => t('Make This My Store link button'),
    'description' => t('Displays Make This My Store button.'),
    'inc' => 'includes/core.api_tokens',
  );

  $tokens['more_about_store_link'] = array(
    'title' => t('More Info About This Store link'),
    'description' => t('Displays More Info About This Store link.'),
  );

  $tokens['modal_message'] = array(
    'title' => t('Message'),
    'description' => t('Displays user message box (using psudo-messaging service).'),
  );

  $tokens['recipe_list'] = array(
    'title' => t('Recipe List'),
    'description' => t('Displays list of recipes by node id. Quote arguments and
     separate recipe node ids with a "+". "101181+13901+187346", for example.'),
  );

  $tokens['print_and_share'] = array(
    'title' => t('Print and share links'),
    'description' => t('Display print and share links on a page.'),
  );
  
  $tokens['product_list'] = array(
    'title' => t('Product List'),
    'description' => t('Displays list of products by node id. Quote arguments and
     separate product node ids with a "+". "244736+237076+237081", for example.'),
  );

  $tokens['facebook-share-button'] = array(
    'title' => t('Facebook sharing on individual pages.'),
    'description' => t('Adds facebook sharing button on individual pages.'),
  );

  $tokens['twitter-share-button'] = array(
    'title' => t('Twitter sharing on individual pages.'),
    'description' => t('Adds twitter sharing button on individual pages.'),
  );

  $tokens['pinterest-share-button'] = array(
    'title' => t('Pinterest sharing on individual pages.'),
    'description' => t('Adds pinterest sharing button on individual pages.'),
  );

  $tokens['google-share-button'] = array(
    'title' => t('Google+ sharing on individual pages.'),
    'description' => t('Adds google+ sharing button on individual pages.'),
  );

  return $tokens;
}


/**
 * Defines CORE API_tokens.
 */
function core_apitoken_view($name, $display_id = 'default', $args = NULL) {
  $result = $args ? views_embed_view($name, $display_id) : views_embed_view($name, $display_id, $args);
  return $result;
}

function core_apitoken_facebook_feed() {
  $block = module_invoke('core', 'block_view', 'facebook_feed');

  return $block['content'];
}

function core_apitoken_twitter_feed() {
  $block = module_invoke('core', 'block_view', 'twitter_feed');

  return $block['content'];
}

function core_apitoken_store_hours_link() {
    $new = '';
    if (store_get_user_store() != NULL) {
          $store = store_get_user_store();
          $new = l(t('Get Store Address, Phone and Hours'), 'node/' . $store->nid) . "<br />";
    };
    return $new ;
}

function core_apitoken_cooking_classes_link() {
    return l(t('Find Cooking Classes'), 'events', array('query' => array('category' => '83')));
}

function core_apitoken_upcoming_events_link() {
    return l(t('Learn About Upcoming Events'), 'events');
}

function core_apitoken_weekly_flyer_link() {
  return l(t('Download the Weekly Sales Flyer'), 'sales-flyer');
}

function core_apitoken_make_this_my_store($nid) {
  $form = drupal_get_form('store_make_users_store_' . $nid , $nid);
  return drupal_render($form);
}

function core_apitoken_more_about_store_link() {
  $nid = $data->_entity_properties['nid'];
  return l(t('More info about this store'), "node/$nid");
}

function core_apitoken_modal_message() {
  $messages = core_modal_get_message();
  return $messages;
}

function core_apitoken_recipe_list($nids) {
  $view = views_embed_view('recipes', 'by_argument', $nids);
  return $view;
}

function core_apitoken_print_and_share($print=FALSE) {
  $out = '<div class="print-and-share-box">' . theme('wholefoods_sharelinks');
  if ($print) {
    $path = '/print/' . current_path();
    $out .= '<div class="print-icon"><a href="' . $path . '">' . t('Print') . '</a></div>';
  }
  $out .= '</div>';
  return $out;
}

function core_apitoken_product_list($nids) {
  $view = views_embed_view('products_365_edv', 'products_by_argument', $nids);
  return $view;
}

/**
 * Prepares markup facebook button.
 * @return string
 */
function core_apitoken_facebook_share_button() {
  $output = '<div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
      fjs.parentNode.insertBefore(js, fjs);
      }(document, \'script\', \'facebook-jssdk\'));
    </script>
    <fb:like send="false" layout="button_count" width="450" show_faces="false"></fb:like>';
  drupal_add_css('.fb_iframe_widget{vertical-align:top;} #fb-root{display:inline-block;}', 'inline');

  return $output;
}

/**
 * Prepares markup twitter button.
 * @return string
 */
function core_apitoken_twitter_share_button() {
  $output = '<a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
    <script>!function(d,s,id){
      var js,fjs=d.getElementsByTagName(s)[0];
        if(!d.getElementById(id)){
          js=d.createElement(s);
          js.id=id;js.src="//platform.twitter.com/widgets.js";
          fjs.parentNode.insertBefore(js,fjs);
        }
      }(document,"script","twitter-wjs");
    </script>';

  return $output;
}

/**
 * Prepares markup google+ button.
 * @return string
 */
function core_apitoken_google_share_button() {
  $output = '<div class="g-plusone" data-size="medium"></div>
    <script src="https://apis.google.com/js/platform.js" async defer></script>';

  return $output;
}

/**
 * Prepares markup pinterest button.
 * @return string
 */
function core_apitoken_pinterest_share_button() {
  if (!isset($params['url'])) {
    $url = request_path();
    if (isset($params['query'])) {
      $get = $params['query'];
    } else {
      $get = $_GET;
      unset($get['q']);
    }
    $options = array(
      'absolute' => TRUE,
      'query' => $get,
    );
    if (isset($params['fragment'])) {
      if (strlen($params['fragment']) > 0) {
        $options['fragment'] = $params['fragment'];
      }
    }
    $url = url($url, $options);
  }
  else {
    $url = $params['url'];
  }

  $node = node_load(arg(1));
  $pin_descr = strip_tags($node->body[LANGUAGE_NONE][0]['value']);
  $output =
    '<a data-pin-config="beside"
     href="//pinterest.com/pin/create/button/?url=' . urlencode($url)
    . '&title="Share on Pinterest"'
    . '&description=' . urlencode($pin_descr) . '"'
    . 'data-pin-do="buttonPin" >
    <img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" /></a>
    <script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>';

  return $output;
}
