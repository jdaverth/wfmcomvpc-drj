<?php

/**
 * @file
 * Admin page callbacks for the advanced cache clear.
 */

/**
 * Form builder; Advanced cache clear.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function core_advanced_cache_clear_form($form, $form_state) {
  drupal_set_title(t('Advanced cache clear'));
  //Check whether enabled Acquia Purge.
  if (module_exists('acquia_purge')) {
    $form['core_advanced_cache_clear_button'] = array(
      '#type' => 'submit',
      '#value' => t('Clear Varnish cache for following URL\'s.'),
      '#weight' => 1,
      '#submit' => array('core_advanced_cache_clear_submit'),
    );
    $form['core_advanced_cache_clear_drupal'] = array(
      '#type' => 'checkbox',
      '#title' => t('Also clear <b>all</b> drupal caches.'),
      '#weight' => 2,
      '#default_value' => variable_get('core_advanced_cache_clear_drupal', 0),
    );
    $form['core_advanced_cache_clear_urls'] = array(
      '#title' => t('Url\'s to clear varnish cache'),
      '#type' => 'textarea',
      '#description' => t('Varnish cache on this URLs and aliases for this URLs will be cleared.'),
      '#default_value' => variable_get('core_advanced_cache_clear_urls', ''),
      '#weight' => 3,
    );
    $form = system_settings_form($form);
    $form['actions']['submit']['#value'] = 'Save URL list';
  }
  else {
    $form['required_modules'] = array(
      '#markup' => t('To use this form, please, install and enable the <a href="@url">Acquia Purge</a> module.', 
      array('@url' => url( 'https://www.drupal.org/project/acquia_purge', array('external' => true)))),
    );
  }
  return $form;
}

/**
 * Submit callback; clear system caches and varnish cache.
 *
 * @ingroup forms
 */
function core_advanced_cache_clear_submit($form, &$form_state) {
  $paths = explode("\n", $form_state['values']['core_advanced_cache_clear_urls']);
  if (count($paths) > 0) {
    _acquia_purge_input_path_variations($paths);
    $job = 'acquia_purge_cron';
    acquia_purge_purge_paths($paths);
    elysia_cron_initialize();
    elysia_cron_execute($job);
    drupal_set_message(t('Cron job %job ended.', array('%job' => $job)));
  }
  else {
    drupal_set_message(t('No pathes to clean cache.'));
  }
  if ($form_state['values']['core_advanced_cache_clear_drupal'] == 1) {
    drupal_flush_all_caches();
    drupal_set_message(t('Drupal caches cleared.'));
  }
}