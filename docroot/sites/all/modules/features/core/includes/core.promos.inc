<?php
/**
 * @file
 * Promo specific code pulled out of core.module.
 */

/**
 */
define('WHOLEFOODS_GLOBAL_PROMO_PERMISSION', 'can add global promos');

/**
 */
define('WHOLEFOODS_LOCAL_PROMO_PERMISSION', 'can add local promos');

/**
 */
function _core_menu_promos(&$items) {
  $items['node/%node/promos'] = array(
    'title' => 'Promos',
    'page callback' => 'core_promo_control',
    'page arguments' => array(1),
    'access callback' => 'core_promo_control_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'weight' => 100,
  );
}

/**
 */
function core_promo_control_access($node) {

  if (user_access(WHOLEFOODS_GLOBAL_PROMO_PERMISSION)) {
    return TRUE;
  }

  $allow_local = FALSE;

  if (isset($node->field_allow_local_promos)) {
    if (isset($node->field_allow_local_promos[LANGUAGE_NONE])) {
      if ($node->field_allow_local_promos[LANGUAGE_NONE][0]['value'] == TRUE) {
        $allow_local = TRUE;
      }
    }
  }

  if (user_access('can add local promos') && $allow_local) {
    return TRUE;
  }

  return FALSE;
}

/**
 */
function core_promo_control($node) {
  $entity_type = 'node';
  $entity = $node;
  $langcode = LANGUAGE_NONE;

  drupal_add_js('
jQuery(document).ready(function($) {
  var button = $("#addNewPromo");

  button.click(function() {
    var selector = $("#edit-field-store-nid");
    var selector_list = $("option", selector);
    var store;

    selector_list.each(function() {
      var attr = $(this).attr("selected");
      if (typeof attr != "undefined" && attr == true) {
        store = $(this).val();
      }
    });
    if (store == "All") {
      store = "";
    } else {
      store = "&store=" + store;
    }

    $(this).attr("href", "/node/add/promo?page=' . $node->nid . '" + store);
    return true;
  });
});
  ', 'inline');
  $output = views_embed_view('promo_blocks', 'explorer', $node->nid);
  $output .= '<br/>' . '<a id="addNewPromo" href="">Add New Promo</button>';

  return $output;
}

/**
 */
Function core_promo_control_submit($form, &$form_state) {
  $node = node_load($form_state['values']['node']);

  $fields = array(
    'field_promos_global' => user_access(WHOLEFOODS_GLOBAL_PROMO_PERMISSION),
    'field_promos_store' => user_access(WHOLEFOODS_LOCAL_PROMO_PERMISSION) || $user->uid == $node->nid,
  );

  foreach ($fields as $key => $access) {
    if (!isset($form_state['values'][$key]) || !$access) {
      continue;
    }

    $new_field = array();
    foreach ($form_state['values'][$key][LANGUAGE_NONE] as $k => $v) {
      if (is_array($v)) {
        if ($v['nid'] !== NULL) {
          $new_field[$v['_weight']] = array('nid' => $v['nid']);
        }
      }
    }
    ksort($new_field);
    $promos = array(LANGUAGE_NONE => array());
    $i = 0;
    foreach ($new_field as $value) {
      $promos[LANGUAGE_NONE][$i] = $value;
      ++$i;
    }

    $node->{$key} = $promos;
  }
  node_save($node);
}
