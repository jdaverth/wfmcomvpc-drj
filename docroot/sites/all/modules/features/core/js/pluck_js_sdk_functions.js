/**
 * Output the HTML to the 'plckPersonaHeader' element
 * 
 * @param {string} HTML to display
 */
function plckWriteOutput(outString) {
    document.getElementById("plckPersonaHeader").innerHTML += "\n" + outString;
}

/**
 * Constructs the html for the persona header from the User object
 * 
 * @param {PluckSDK.UserRequest.User} Pluck User object
 */
function plckPrintUserSummary(user) {

  var uName = Drupal.t(Drupal.checkPlain(user.DisplayName));
  var uSex = '';
  var uAge = '';
  var uLoc = '';
  var uAboutMe = '';

  if (user.Sex !== 'None') {
    uSex = Drupal.t(Drupal.checkPlain(user.Sex));
    if ( (user.Age !== '') || (user.Location !== '') ) {
      uSex += ', ';
    }
  };

  if (user.Age !== '') {
    uAge = 'Age: ' + Drupal.t(Drupal.checkPlain(user.Age));
    if ( user.Location !== '' ) {
      uAge += ', ';
    }
  };

  if (user.Location !== '') {
    uLoc = Drupal.t(Drupal.checkPlain(user.Location));
  };

  if (user.AboutMe !== '') {
    uAboutMe = '<span class="PersonaHeader_InfoHeading">About Me:</span> ' + Drupal.t(Drupal.checkPlain(user.AboutMe));
  };

  plckWriteOutput( '<div id="personaHDest" class="Persona_Main"><div style="display: inline;"><div class="PersonaHeader_Container">' + 
    '<table class="PersonaHeader_Table" cellpadding="0" cellspacing="0"><tbody><tr>' + 
    '<td class="PersonaHeader_TableLeft">' + 
    '<img id="" src="' + user.AvatarPhotoUrl + '" alt="Your image" class="PluckUserAvatar">' + 
    '</td>' + 
    '<td class="PersonaHeader_TableMiddle">' + 
    '<div class="PersonaHeader_Title">' + uName + '</div>' + 
    '<div class="PersonaHeader_Info">' + uSex + uAge + uLoc + '</div>' + 
    '<div class="PersonaHeader_Info">' + uAboutMe + '</div>' + 
    '</td>' + 
    '<td class="PersonaHeader_TableRight"></td>' + 
    '</tr></tbody></table></div></div></div>');
}

/**
 * Handle the PluckSDK User Response array
 * 
 * @param {PluckSDK.UserResponse[]} Array of Pluck SDK UserResponse objects
 */
function plckUserCallback(responses) {
  //check to see if there were any exceptions at the Error level
  if (responses[0].ResponseStatus.StatusCode != PluckSDK.ResponseStatusCode.OK) {
    //display html to browser that an error happened
    //plckWriteOutput("Error occurred (user might not exist).");
  } else {
    plckPrintUserSummary(responses[0].User);
  }
}

/**
 * Retrieve the Pluck User object using the Pluck SDK
 * 
 * @param {PluckSDK.UserKey} Pluck user id
 */
function plckGetPersonaHeader(userId) {
    //create the action request
    var userReq = new PluckSDK.UserRequest();
  
    //to get info on the executing user, omit the next 2 lines; a UserRequest with
    //no Key gets the current user, or a placeholder for anonymous users
    userReq.UserKey = new PluckSDK.UserKey();
    userReq.UserKey.Key = userId;
    
    //build the requests array and send the request
    PluckSDK.SendRequests([userReq], plckUserCallback);
}
