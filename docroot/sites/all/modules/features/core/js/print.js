jQuery(document).ready(function($) {
  var form = $('#print-box');

  $('a', form).click(function() {
    var selected = $('input:checked', form);
    window.open(Drupal.settings.WholeFoods.print.nodePath + '?css=' + selected.val(), "something", "height=1,width=1000,modal=yes");
    Drupal.CTools.Modal.dismiss();
    return false;
  });
});
