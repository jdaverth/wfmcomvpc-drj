(function($) {
  var l = Drupal.settings.wfmOembed.langs;
  var data = {};
  CKEDITOR.plugins.add('wfm_oembed', {
    init: function(editor) {
      editor.addCommand('wfm_oembed', new CKEDITOR.dialogCommand('wfm_oembed_dialog'));
      editor.ui.addButton('wfm_oembed', {
        label: l.buttonTitle,
        command: 'wfm_oembed',
        icon: this.path + 'images/video.png'
      });
      CKEDITOR.dialog.add('wfm_oembed_dialog', function(editor) {
        return {
          title: l.title,
          minWidth: 360,
          minHeight: 80,
          contents: [{
            elements: [{
              type: 'text',
              id: 'url',
              label: l.url,
              commit: function(data){
                data.url = this.getValue();
              }
            }
          ]
          }],
          buttons: [
            CKEDITOR.dialog.cancelButton,
            CKEDITOR.dialog.okButton
          ],
          onOk: function() {
            this.commitContent(data);
            var editorInstance = this.getParentEditor();
            $('body').oembed(data.url, {
                onEmbed: function (e) {
                  if (typeof e.code === 'string') {
                      editorInstance.insertElement( CKEDITOR.dom.element.createFromHtml( editor.config.oembed_WrapperClass != null ? '<div class="' + editor.config.oembed_WrapperClass + '">' : '<div>' + e.code + '</div>' ));
                  } else {
                      alert(editor.lang.oembed.noEmbedCode);
                  }
                },
                maxWidth: data.width,
                maxHeight: data.height,
                embedMethod: 'editor'
            });
          }
        };
      });
    }
  });
})(jQuery);
