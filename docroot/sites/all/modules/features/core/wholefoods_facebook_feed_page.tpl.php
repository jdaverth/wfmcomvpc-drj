<?php

/**
 * @file
 * Default module/theme implementation to display Facebook wall posts.
 *
 * Available variables:
 * - $posts: Object containing an array of Facebook posts and paging URLs.
 *
 */
?>
<?php $TRIM_LEN = 150; ?>
<div id="social-feed-container">
  <?php foreach ($posts->data as $count => $post): ?>
  <div class="facebook-post-fetcher-item">
    <?php // Display Facebook object/wall name. ?>
    <h3><?php print l($post->from->name, 'http://www.facebook.com/'. $post->from->id); ?></h3>

    <?php // Display date. ?>
    <p class="facebook-post-fetcher-date-time"><?php print date('l, F j, Y', $post->created_time); ?></p>

    <?php // Display picture. ?>
    <?php if (isset($post->picture) && isset($post->link)): ?>
      <?php print l('<img src="'. $post->picture .'" />', $post->link, array('attributes' => array('class' => 'facebook-post-fetcher-image'), 'html' => TRUE)); ?>
    <?php elseif (isset($post->picture)): ?>
      <img src="<?php print $post->picture; ?>" alt="<?php print $post->name?>" class="facebook-post-fetcher-image" />
    <?php endif; ?>

    <?php // Display message. ?>
    <?php if (isset($post->message)): ?>
      <p class="facebook-post-fetcher-message"><?php print substr($post->message, 0, $TRIM_LEN) . ((strlen($post->message) >= $TRIM_LEN) ? '...' : ''); ?></p>
    <?php endif; ?>

    <?php // Display link/name. ?>
    <?php if (isset($post->link)): ?>
    <p class="facebook-post-fetcher-name"><?php print l($post->from->name, $post->link, array('html' => TRUE)); ?></p>
    <?php else: ?>
    <p class="facebook-post-fetcher-name"><?php print $post->from->name; ?></p>
    <?php endif; ?>

    <?php // Display description. ?>
    <?php if (!isset($post->message) && isset($post->description)): ?>
      <p class="facebook-post-fetcher-description"><?php print substr($post->description, 0, $TRIM_LEN) . ((strlen($post->description) >= $TRIM_LEN) ? '...' : ''); ?></p>
    <?php endif; ?>

    <?php // Display caption. ?>
    <?php if (isset($post->caption) && strpos($post->caption, '.')): ?>
    <?php print l($post->caption, $post->caption, array('attributes' => array('class' => 'facebook-post-fetcher-caption'), 'html' => TRUE)); ?>
    <?php endif; ?>

    <div class="clearfix"></div>
  </div>
  <?php endforeach;?>
</div>
