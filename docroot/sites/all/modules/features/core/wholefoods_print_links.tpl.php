<form id="print-box" action="#">
   <input name="print-type" type="radio" value="recipe_with_image">Full Recipe (+ Image)</input>
   <input name="print-type" type="radio" value="recipe_no_image">Full Recipe (No Image)</input>
 <input name="print-type" type="radio" value="recipe_3x5">3 X 5 Recipe Card</input>
   <?php print l('Continue', '#', array('attributes' => array('class' => array('orange-link')))); ?>
</form>
<?php
$path = drupal_lookup_path('alias', 'node/' . $node->nid);
$path = '/print/' . $path;
?>
<script>
jQuery(document).ready(function($) {
    var form = $('#print-box');

    $('a', form).click(function() {
        window.open("<?php print $path; ?>", "print", "height=1,width=1,modal=yes,alwaysRaised=yes");
        Drupal.CTools.Modal.dismiss();
        return false;
      });
});
</script>
