<div class="upload-photo-form">
  <h2>Upload Customer Image for Recipe:</h2>
  <h3><?php print $node->title ?></h3>
  <div id="upload_wrapper">
    <p>Thank you for sharing your photo of a Whole Foods Market recipe. We can't wait to see your culinary creation. Here are a few guidelines to keep in mind before you get started:</p>
    <ul>
      <li>All photos are moderated. Your photo will not appear on the recipe page until it has been approved. Please allow a few business days for review and approval, and please do not upload the same photo more than once.</li>
      <li>Photos must be smaller than 2 MB. If your photo is larger than 2 MB, please reduce its size before you add it to this gallery.</li>
      <li>This is about sharing food pics! We all have fabulous people and pets in our lives, but let's keep them out of these photos, focusing instead on the recipe and how it turned out for you.</li>
      <li>Our favorite photos showcase the dish using good lighting and a simple, uncluttered presentation. You might get some inspiration from our recipe photos.</li>
      <li>Unrelated or obscene content will not be published.</li>
      <li>Make sure it's yours. Original material only, please.</li>
      <li>Whole Foods Market reserves the right to not post or to remove photos at any time for any reason.</li>
      <li>When you upload a photo, you grant Whole Foods Market the nonexclusive rights to use such content throughout the world in any media. See our full <a href="http://www.wholefoodsmarket.com/users/terms.php">terms of use</a> for more information.</li>
    </ul>
    <p>Click "Browse" or "Choose file" to find and select images on your computer.</p>
    <div id="upload-customer-photo">
      <form id="uploadPhotoForm" method="post" action="<?php print $photo_upload_url; ?>" enctype="multipart/form-data">
        <input type="hidden" name="galleryKey" value="<?php print $gallery_key; ?>"/>
        <?php
        if (isset($_COOKIE["at"])) {
          $atVals = explode("&h=", $_COOKIE["at"]);
          $ath = $atVals[1];
        }
        else {
          $ath = "";
        }
        ?>
        <input type="hidden" name="ath" value="<?php print $ath; ?>"/>
        <div id="upload_fields">
          <div class="upload_row"><input type="file" name="Filedata" id="file0" size="40" /></div>
        </div>
        <br />
        <input type="button" value="Upload Photo" id="btn_upload" />
      </form>
    </div>
  </div>
  <div id="results-list-wrapper">
    <ul id="results-list"></ul>
  </div>
</div>
