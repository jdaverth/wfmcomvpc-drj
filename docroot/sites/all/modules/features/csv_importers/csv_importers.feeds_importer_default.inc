<?php
/**
 * @file
 * csv_importers.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function csv_importers_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'product_brand';
  $feeds_importer->config = array(
    'name' => 'product_brand',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 1,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsTermProcessor',
      'config' => array(
        'vocabulary' => 'product_brands',
        'force_update' => 1,
        'mappings' => array(
          0 => array(
            'source' => '1',
            'target' => 'name',
            'unique' => 1,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => 1800,
    'expire_period' => 3600,
    'import_on_create' => TRUE,
    'process_in_background' => FALSE,
  );
  $export['product_brand'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'product_certification_csv_import';
  $feeds_importer->config = array(
    'name' => 'Product Certification CSV Import',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 1,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'content_type' => 'product_certification',
        'expire' => '-1',
        'author' => '1',
        'mappings' => array(
          0 => array(
            'source' => '0',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => '1',
            'target' => 'title',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['product_certification_csv_import'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'product_csv_importer';
  $feeds_importer->config = array(
    'name' => 'Product CSV Importer',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'content_type' => 'product',
        'expire' => '-1',
        'author' => '1',
        'mappings' => array(
          0 => array(
            'source' => 'pid',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'name',
            'target' => 'title',
            'unique' => 0,
          ),
          2 => array(
            'source' => 'headnote',
            'target' => 'body',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'category',
            'target' => 'field_product_category',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'ing',
            'target' => 'field_ingredients',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'allergens',
            'target' => 'field_allergen_info',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'pid',
            'target' => 'field_product_pid',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'image',
            'target' => 'field_image',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'ex_date',
            'target' => 'field_exclusive_until:start',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'field_product_shelf_life',
            'target' => 'field_product_shelf_life',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'nutrition',
            'target' => 'field_product_nutrition_info',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'units',
            'target' => 'field_product_size',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'upc',
            'target' => 'field_product_upc',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'origin',
            'target' => 'field_product_origin',
            'unique' => FALSE,
          ),
          14 => array(
            'source' => 'certified_by',
            'target' => 'field_product_certifier',
            'unique' => FALSE,
          ),
          15 => array(
            'source' => 'status',
            'target' => 'status',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['product_csv_importer'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'product_line_csv_importer';
  $feeds_importer->config = array(
    'name' => 'Product Line CSV Importer',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 1,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'content_type' => 'product_line',
        'expire' => '-1',
        'author' => '1',
        'mappings' => array(
          0 => array(
            'source' => '0',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => '1',
            'target' => 'title',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'input_format' => 'plain_text',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['product_line_csv_importer'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'special_diet_csv_importer';
  $feeds_importer->config = array(
    'name' => 'Special Diet CSV Importer',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 1,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'content_type' => 'special_diet',
        'expire' => '-1',
        'author' => '1',
        'mappings' => array(
          0 => array(
            'source' => '0',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => '1',
            'target' => 'title',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['special_diet_csv_importer'] = $feeds_importer;

  return $export;
}
