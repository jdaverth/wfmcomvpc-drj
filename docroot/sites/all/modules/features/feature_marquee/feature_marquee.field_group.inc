<?php
/**
 * @file
 * feature_marquee.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function feature_marquee_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_content|node|marquee|form';
  $field_group->group_name = 'group_content';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'marquee';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Content',
    'weight' => '0',
    'children' => array(
      0 => 'field_marquee_media',
      1 => 'field_heading_copy',
      2 => 'field_marquee_priority',
      3 => 'field_marquee_video',
      4 => 'locations_acl',
      5 => 'title',
      6 => 'group_links',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Content',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_content|node|marquee|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_links|node|marquee|form';
  $field_group->group_name = 'group_links';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'marquee';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_content';
  $field_group->data = array(
    'label' => 'Marquee Button',
    'weight' => '-1',
    'children' => array(
      0 => 'field_marquee_url',
      1 => 'field_estore_item',
      2 => 'field_estore_item_type',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Marquee Button',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => 'This Marquee can be linked to an internal or external page, or eStore Category or Item. The fields below govern where this Marquee will be linked to.<br /> You can:
<ul><li>Leave all fields blank and disable the hyperlink</li>
<li>Enter a <strong>Title</strong> to label the hyperlink button.</li>  
<li>Enter a relative or complete URL (eg: "/products" or "http://othersite.com/page") to direct them to a specific page within this site or an external site.</li>  
<li>Enter an eStore Item or Category to reference a particular page within the eStore.*</li>
</ul><br />
<strong>An eStore item will always take precedence over a URL which takes precedence over Page Title.</strong><br />
* You must have additional privileges to add an eStore Item.<br />',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_links|node|marquee|form'] = $field_group;

  return $export;
}
