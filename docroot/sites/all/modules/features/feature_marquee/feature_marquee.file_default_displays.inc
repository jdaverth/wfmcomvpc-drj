<?php
/**
 * @file
 * feature_marquee.file_default_displays.inc
 */

/**
 * Implements hook_file_default_displays().
 */
function feature_marquee_file_default_displays() {
  $export = array();

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'application__file_styles_marquee__file_field_styles_file_marquee';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['application__file_styles_marquee__file_field_styles_file_marquee'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__file_styles_marquee__file_field_styles_file_marquee';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['audio__file_styles_marquee__file_field_styles_file_marquee'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'default__file_styles_marquee__file_field_styles_file_marquee';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['default__file_styles_marquee__file_field_styles_file_marquee'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__file_styles_marquee__file_field_styles_file_marquee';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['image__file_styles_marquee__file_field_styles_file_marquee'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'text__file_styles_marquee__file_field_styles_file_marquee';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['text__file_styles_marquee__file_field_styles_file_marquee'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__file_styles_marquee__file_field_styles_file_marquee';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['video__file_styles_marquee__file_field_styles_file_marquee'] = $file_display;

  return $export;
}
