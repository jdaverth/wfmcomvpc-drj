<?php
/**
 * @file
 * forums.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function forums_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'forums';
  $context->description = '';
  $context->tag = 'Forums';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'forums' => 'forums',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'core-wholefoods_forums' => array(
          'module' => 'core',
          'delta' => 'wholefoods_forums',
          'region' => 'content',
          'weight' => '20',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Forums');
  $export['forums'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'forums_recipe';
  $context->description = '';
  $context->tag = 'Forums';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'recipe-forums' => 'recipe-forums',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'core-wholefoods_recipe_forums' => array(
          'module' => 'core',
          'delta' => 'wholefoods_recipe_forums',
          'region' => 'content',
          'weight' => '20',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Forums');
  $export['forums_recipe'] = $context;

  return $export;
}
