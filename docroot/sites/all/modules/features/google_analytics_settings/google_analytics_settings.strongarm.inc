<?php
/**
 * @file
 * google_analytics_settings.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function google_analytics_settings_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'googleanalytics_altsearch';
  $strongarm->value = '1';
  $export['googleanalytics_altsearch'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'googleanalytics_altsearch_pages';
  $strongarm->value = 'site_search/*
products/search/*
blog/search/*
recipe/search/*';
  $export['googleanalytics_altsearch_pages'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'googleanalytics_category_tracking';
  $strongarm->value = '1';
  $export['googleanalytics_category_tracking'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'googleanalytics_category_tracking_pages';
  $strongarm->value = 'site_search/*|site search
products/search/*|products
blog/search/*|blog
recipe/search/*|recipe';
  $export['googleanalytics_category_tracking_pages'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'googleanalytics_site_search';
  $strongarm->value = 1;
  $export['googleanalytics_site_search'] = $strongarm;

  return $export;
}
