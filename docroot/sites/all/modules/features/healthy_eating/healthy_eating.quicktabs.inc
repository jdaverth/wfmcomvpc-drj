<?php
/**
 * @file
 * healthy_eating.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function healthy_eating_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'recipes';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Recipes';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'recipes',
      'display' => 'featured',
      'args' => '',
      'title' => 'Featured',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'recipes',
      'display' => 'top_rated',
      'args' => '',
      'title' => 'Top Rated',
      'weight' => '-99',
      'type' => 'view',
    ),
    2 => array(
      'vid' => 'recipes',
      'display' => 'newest',
      'args' => '',
      'title' => 'Newest',
      'weight' => '-97',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Featured');
  t('Newest');
  t('Recipes');
  t('Top Rated');

  $export['recipes'] = $quicktabs;

  return $export;
}
