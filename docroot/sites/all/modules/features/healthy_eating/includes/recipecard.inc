<?php
/**
 * @file
 * Contains the implementation of RecipeCard.
 */

class RecipeCard {
  const LINES_PER_CARD = 19;
  const M_LINES_PER_CARD = 18;
  const HEADER_LINES = 4;
  const CHARS_PER_LINE = 80;
  const MULTIPLIER = 1.2;

  protected $build = NULL;

  /**
   * Constructor method.
   *
   * Assigns build to $this->build.
   */
  public function __construct($build) {
    $this->build = $build;
  }

  /**
   * Parse the build variable to be ready to render as 3x5 cards.
   */
  public function runCardParser() {
    $build = &$this->build;
    $page_break_line = '<div  class="page-break">&nbsp;</div>';

    $ing_lines = 0;
    $card_lines = self::HEADER_LINES;
    foreach ($build['field_ingredients'] as $k => $v) {
      if ('#' == substr($k, 0, 1)) {
        continue;
      }

      ++$card_lines;
      ++$ing_lines;

      if ($card_lines >= self::LINES_PER_CARD) {
        $build['field_ingredients'][$k]['#prefix'] = $page_break_line;
        $card_lines = 0;
      }
    }

    $this->hideUnwantedFields();
    $this->addJavaScript();
  }

  /**
   * Return the build variable with any changes this object has made to it.
   */
  public function returnBuild() {
    return $this->build;
  }

  /**
   * Add the javascript support files and settings.
   */
  protected function addJavaScript() {
    $module_path = drupal_get_path('module', 'healthy_eating');
    drupal_add_js($module_path . '/js/page_break.js');
  }

  /**
   * Hides the fields that are not supposed to be rendered on a 3x5 card.
   */
  protected function hideUnwantedFields() {
    $build = &$this->build;
    unset($build['body']);
    unset($build['field_hero_image']);
    unset($build['field_special_diet']);
    unset($build['field_nutritional_info']);
    unset($build['#groups']);
    unset($build['#fieldgroups']);
    unset($build['#groupchildren']);
  }

}
