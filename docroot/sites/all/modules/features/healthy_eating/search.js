jQuery(document).ready(function() {
  var resultsPage = jQuery('body').hasClass('page-recipe-search');
  if(resultsPage == 0){
    var clicked = 0;
    jQuery('#edit-search-box').addClass('unfocused');
    jQuery('#edit-search-box').focus(function(){
      if(clicked == 0){
        jQuery('#edit-search-box').attr('value', '');
        jQuery('#edit-search-box').removeClass('unfocused');
        clicked = 1;
      }
    });
    jQuery('a.advanced-search-link').click(function() {
      if(clicked == 0){  
        jQuery('#edit-search-box').attr('value', '');
        jQuery('#edit-search-box').removeClass('unfocused');
        clicked = 1;
      }  
    });
  }
});
