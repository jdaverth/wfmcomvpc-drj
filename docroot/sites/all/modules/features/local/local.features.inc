<?php
/**
 * @file
 * local.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function local_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "openlayers" && $api == "openlayers_maps") {
    return array("version" => "1");
  }
  if ($module == "openlayers" && $api == "openlayers_styles") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function local_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function local_image_default_styles() {
  $styles = array();

  // Exported image style: local_vendor_brief.
  $styles['local_vendor_brief'] = array(
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 190,
          'height' => 135,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'local_vendor_brief',
  );

  // Exported image style: local_vendor_detail.
  $styles['local_vendor_detail'] = array(
    'effects' => array(
      3 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 565,
          'height' => 375,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'local_vendor_detail',
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function local_node_info() {
  $items = array(
    'local_vendor' => array(
      'name' => t('Local Vendor'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
