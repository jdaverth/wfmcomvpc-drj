<?php
/**
 * @file
 * mobile_marquees.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mobile_marquees_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function mobile_marquees_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function mobile_marquees_image_default_styles() {
  $styles = array();

  // Exported image style: mobile_333x210.
  $styles['mobile_333x210'] = array(
    'label' => 'mobile_333x210',
    'effects' => array(
      52 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 333,
          'height' => 210,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_styles_default_styles().
 */
function mobile_marquees_styles_default_styles() {
  $styles = array();

  // Exported style: mobile_333x210

  $styles['file']['styles']['mobile_333x210'] = array(
    'label' => 'mobile_333x210',
    'description' => '',
    'preset_info' => array(
      'image' => array(
        'mobile_333x210' => array(
          'default preset' => 'original',
          'preset' => 'mobile_333x210',
        ),
      ),
      'audio' => array(
        'mobile_333x210' => array(
          'default preset' => 'original',
        ),
      ),
      'video' => array(
        'mobile_333x210' => array(
          'default preset' => 'original',
        ),
      ),
      'default' => array(
        'mobile_333x210' => array(
          'default preset' => 'original',
        ),
      ),
      'media_youtube' => array(
        'mobile_333x210' => array(
          'default preset' => 'unlinked_thumbnail',
        ),
      ),
    ),
  );
  // Exported style: mobile_333x210

  $styles['file']['styles']['mobile_333x210'] = array(
    'label' => 'mobile_333x210',
    'description' => '',
    'preset_info' => array(
      'image' => array(
        'mobile_333x210' => array(
          'default preset' => 'original',
          'preset' => 'mobile_333x210',
        ),
      ),
      'audio' => array(
        'mobile_333x210' => array(
          'default preset' => 'original',
        ),
      ),
      'video' => array(
        'mobile_333x210' => array(
          'default preset' => 'original',
        ),
      ),
      'default' => array(
        'mobile_333x210' => array(
          'default preset' => 'original',
        ),
      ),
      'media_youtube' => array(
        'mobile_333x210' => array(
          'default preset' => 'unlinked_thumbnail',
        ),
      ),
    ),
  );
  return $styles;
}

/**
 * Implements hook_styles_default_presets_alter().
 */
function mobile_marquees_styles_default_presets_alter(&$presets) {
  $styles = styles_default_styles();
  if ($styles['file']['styles']['mobile_333x210']['storage'] == STYLES_STORAGE_DEFAULT) {
    $presets['file']['containers']['image']['styles']['mobile_333x210'] = array(
      'default preset' => 'original',
      'preset' => 'mobile_333x210',
    );

    $presets['file']['containers']['audio']['styles']['mobile_333x210'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['video']['styles']['mobile_333x210'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['default']['styles']['mobile_333x210'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['media_youtube']['styles']['mobile_333x210'] = array(
      'default preset' => 'unlinked_thumbnail',
    );

  }
  if ($styles['media']['styles']['mobile_333x210']['storage'] == STYLES_STORAGE_DEFAULT) {
    $presets['media']['containers']['image']['styles']['mobile_333x210'] = array(
      'default preset' => 'original',
      'preset' => 'mobile_333x210',
    );

    $presets['media']['containers']['audio']['styles']['mobile_333x210'] = array(
      'default preset' => 'original',
    );

    $presets['media']['containers']['video']['styles']['mobile_333x210'] = array(
      'default preset' => 'original',
    );

    $presets['media']['containers']['default']['styles']['mobile_333x210'] = array(
      'default preset' => 'original',
    );

    $presets['media']['containers']['media_youtube']['styles']['mobile_333x210'] = array(
      'default preset' => 'unlinked_thumbnail',
    );

  }
}
