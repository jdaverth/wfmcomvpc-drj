<?php
/**
 * @file
 * mobile_marquees.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function mobile_marquees_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'mobile_marquee';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Mobile Marquee';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'collapsed';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['format'] = 'plain_text';
  /* Relationship: Content: Marquees (field_marquee) */
  $handler->display->display_options['relationships']['field_marquee_nid']['id'] = 'field_marquee_nid';
  $handler->display->display_options['relationships']['field_marquee_nid']['table'] = 'field_data_field_marquee';
  $handler->display->display_options['relationships']['field_marquee_nid']['field'] = 'field_marquee_nid';
  $handler->display->display_options['relationships']['field_marquee_nid']['required'] = TRUE;
  $handler->display->display_options['relationships']['field_marquee_nid']['delta'] = '-1';
  /* Field: Field: Button (URL) */
  $handler->display->display_options['fields']['field_marquee_url']['id'] = 'field_marquee_url';
  $handler->display->display_options['fields']['field_marquee_url']['table'] = 'field_data_field_marquee_url';
  $handler->display->display_options['fields']['field_marquee_url']['field'] = 'field_marquee_url';
  $handler->display->display_options['fields']['field_marquee_url']['relationship'] = 'field_marquee_nid';
  $handler->display->display_options['fields']['field_marquee_url']['ui_name'] = 'Field: Button (URL)';
  $handler->display->display_options['fields']['field_marquee_url']['label'] = '';
  $handler->display->display_options['fields']['field_marquee_url']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_marquee_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_marquee_url']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_marquee_url']['type'] = 'link_absolute';
  /* Field: Content: eStore Item */
  $handler->display->display_options['fields']['field_estore_item']['id'] = 'field_estore_item';
  $handler->display->display_options['fields']['field_estore_item']['table'] = 'field_data_field_estore_item';
  $handler->display->display_options['fields']['field_estore_item']['field'] = 'field_estore_item';
  $handler->display->display_options['fields']['field_estore_item']['relationship'] = 'field_marquee_nid';
  $handler->display->display_options['fields']['field_estore_item']['label'] = '';
  $handler->display->display_options['fields']['field_estore_item']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_estore_item']['element_label_colon'] = FALSE;
  /* Field: Content: Item Type */
  $handler->display->display_options['fields']['field_estore_item_type']['id'] = 'field_estore_item_type';
  $handler->display->display_options['fields']['field_estore_item_type']['table'] = 'field_data_field_estore_item_type';
  $handler->display->display_options['fields']['field_estore_item_type']['field'] = 'field_estore_item_type';
  $handler->display->display_options['fields']['field_estore_item_type']['relationship'] = 'field_marquee_nid';
  $handler->display->display_options['fields']['field_estore_item_type']['label'] = '';
  $handler->display->display_options['fields']['field_estore_item_type']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_estore_item_type']['element_label_colon'] = FALSE;
  /* Field: Field: Button (Title) */
  $handler->display->display_options['fields']['field_marquee_url_1']['id'] = 'field_marquee_url_1';
  $handler->display->display_options['fields']['field_marquee_url_1']['table'] = 'field_data_field_marquee_url';
  $handler->display->display_options['fields']['field_marquee_url_1']['field'] = 'field_marquee_url';
  $handler->display->display_options['fields']['field_marquee_url_1']['relationship'] = 'field_marquee_nid';
  $handler->display->display_options['fields']['field_marquee_url_1']['ui_name'] = 'Field: Button (Title)';
  $handler->display->display_options['fields']['field_marquee_url_1']['label'] = '';
  $handler->display->display_options['fields']['field_marquee_url_1']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_marquee_url_1']['alter']['text'] = '[field_marquee_url_1-title]';
  $handler->display->display_options['fields']['field_marquee_url_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_marquee_url_1']['element_wrapper_type'] = 'h2';
  $handler->display->display_options['fields']['field_marquee_url_1']['element_wrapper_class'] = 'pane-title';
  $handler->display->display_options['fields']['field_marquee_url_1']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_marquee_url_1']['type'] = 'link_separate';
  /* Field: Content: Media */
  $handler->display->display_options['fields']['field_marquee_media']['id'] = 'field_marquee_media';
  $handler->display->display_options['fields']['field_marquee_media']['table'] = 'field_data_field_marquee_media';
  $handler->display->display_options['fields']['field_marquee_media']['field'] = 'field_marquee_media';
  $handler->display->display_options['fields']['field_marquee_media']['relationship'] = 'field_marquee_nid';
  $handler->display->display_options['fields']['field_marquee_media']['label'] = '';
  $handler->display->display_options['fields']['field_marquee_media']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_marquee_media']['alter']['text'] = '[field_marquee_media]';
  $handler->display->display_options['fields']['field_marquee_media']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_marquee_media']['alter']['path'] = '[field_marquee_url]';
  $handler->display->display_options['fields']['field_marquee_media']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_marquee_media']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_marquee_media']['type'] = 'media';
  $handler->display->display_options['fields']['field_marquee_media']['settings'] = array(
    'file_view_mode' => 'file_styles_mobile_333x210',
  );
  /* Field: Content: Heading Copy */
  $handler->display->display_options['fields']['field_heading_copy']['id'] = 'field_heading_copy';
  $handler->display->display_options['fields']['field_heading_copy']['table'] = 'field_data_field_heading_copy';
  $handler->display->display_options['fields']['field_heading_copy']['field'] = 'field_heading_copy';
  $handler->display->display_options['fields']['field_heading_copy']['relationship'] = 'field_marquee_nid';
  $handler->display->display_options['fields']['field_heading_copy']['label'] = '';
  $handler->display->display_options['fields']['field_heading_copy']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_heading_copy']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_heading_copy']['alter']['path'] = '[field_marquee_url]';
  $handler->display->display_options['fields']['field_heading_copy']['alter']['trim_whitespace'] = TRUE;
  $handler->display->display_options['fields']['field_heading_copy']['alter']['nl2br'] = TRUE;
  $handler->display->display_options['fields']['field_heading_copy']['alter']['max_length'] = '100';
  $handler->display->display_options['fields']['field_heading_copy']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['field_heading_copy']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_heading_copy']['type'] = 'text_plain';
  /* Field: Heading Copy (wrapped) */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['ui_name'] = 'Heading Copy (wrapped)';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '[field_heading_copy]';
  $handler->display->display_options['fields']['nothing']['alter']['path'] = '[field_marquee_url]';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Contextual filter: Content: Content UUID */
  $handler->display->display_options['arguments']['uuid']['id'] = 'uuid';
  $handler->display->display_options['arguments']['uuid']['table'] = 'node';
  $handler->display->display_options['arguments']['uuid']['field'] = 'uuid';
  $handler->display->display_options['arguments']['uuid']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['uuid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['uuid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['uuid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['uuid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['uuid']['limit'] = '0';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['relationship'] = 'field_marquee_nid';
  $handler->display->display_options['filters']['status']['value'] = '1';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['relationship'] = 'field_marquee_nid';
  $handler->display->display_options['filters']['type']['value'] = array(
    'marquee' => 'marquee',
  );

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'mobile_marquee_featured_items');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 0;
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 'title_override';
  $handler->display->display_options['allow']['exposed_form'] = 0;
  $handler->display->display_options['allow']['fields_override'] = 0;
  $handler->display->display_options['argument_input'] = array(
    'uuid' => array(
      'type' => 'user',
      'context' => 'entity:comment.flag-blog-comment-publish-count',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '',
      'label' => 'Content: Node UUID',
    ),
  );
  $export['mobile_marquee'] = $view;

  return $export;
}
