Drupal.behaviors.wfm_newsletter_preferences = {
  attach: (function ($) {

    var pref = {

      /**
       * Add event listeners.
       */
      addListeners: function() {
        $('#edit-lifestyle-select-all').on('change', function(){
          pref.lifestyleToggleCallback(this);
        });

        $('#edit-subscribe-all').on('change', function(){
          pref.subscribeAllCallback(this);
        });

        $('#edit-coreemail-opt-out').on('change', function(){
          pref.unsubscribeAllCallback(this);
        });

        $('#edit-email-subscriptions').on('mouseover', '.form-type-checkbox', function(){
          pref.mouseOverCallback(this);
        });

        $('#edit-email-subscriptions').on('mouseout', '.form-type-checkbox', function(){
          pref.mouseOutCallback(this);
        });

        $('#edit-corebirth-month').on('change', function(){
          pref.monthChangeCallback(this);
        });

        $('#newsletters-preferences-form #edit-options .form-type-checkbox').on('click', function(){
          pref.newsletterOptionClickCallback(this);
        });
      },

      buildNumericOptions: function(number) {
        for (var i = 1; i <= number; i++) {
          var opt = document.createElement('option');
          var value = i.toString();
          if (value.length == 1) {
            value = '0' + value;
          }
          opt.value = value;
          opt.innerHTML = i;
          document.getElementById('edit-corebirth-day').appendChild(opt);
        }
      },

      /**
       *
       */
      init: function(context) {
        $('#newsletters-preferences-form', context).once('wfm-newsletter-preferences', function(){
          pref.addListeners();
        });
        pref.monthChangeCallback('#edit-corebirth-month');
      },

      lifestyleToggleCallback: function(checkbox) {
        var checked = $(checkbox).prop('checked');

        if (checked) {
          $('#edit-lifestyle-interests .form-checkbox').prop('checked', true);
        }
        else {
          $('#edit-lifestyle-interests .form-checkbox').prop('checked', false);
        }
      },

      monthChangeCallback: function(select) {
        var selection = parseInt($(select).val()),
          days_in_month = [
            31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
          ],
          daySelectElement = $('#edit-corebirth-day'),
          selectedDay = daySelectElement.val();

        daySelectElement.children().each(function () {
          var option = $(this);
          if (option.val() != '0') {
            option.detach();
          }
        });

        if (selection) {
          pref.buildNumericOptions(days_in_month[selection - 1]);
        }

        // re-instate the selected day if there was one already selected
        daySelectElement.val(selectedDay);
      },

      mouseOutCallback: function(div) {
        var hasHover = $(div).hasClass('hover');

        if (hasHover) {
          $(div).removeClass('hover');
        }
      },

      mouseOverCallback: function(div) {
        var hasHover = $(div).hasClass('hover');

        if (hasHover) {
          return;
        }

        $(div).addClass('hover');
      },

      newsletterOptionClickCallback: function(checkbox) {
        $('#edit-coreemail-opt-out').prop('checked', false);
      },

      subscribeAllCallback: function(checkbox) {
        var checked = $(checkbox).prop('checked');

        if (checked) {
          $('#edit-email-subscriptions .form-checkboxes .form-checkbox').prop('checked', true);
          $('#edit-coreemail-opt-out').prop('checked', false);
        }
        else {
          $('#edit-email-subscriptions .form-checkboxes .form-checkbox').prop('checked', false);
        }
      },

      unsubscribeAllCallback: function(checkbox) {
        var checked = $(checkbox).prop('checked');

        if (checked) {
          $('#edit-email-subscriptions .form-checkboxes .form-checkbox').prop('checked', false);
          $('#edit-subscribe-all').prop('checked', false);
        }
      }

    };

    return pref.init;

  })(jQuery)
};

