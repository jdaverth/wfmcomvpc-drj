/**
 * Self-invoking function.
 */
(function wfm_newsletter_subscription($){
  "use strict";
  var newsletter = {
    loading_gif: '/sites/all/themes/wholefoods/images/ajax-loader2.gif',
    /**
     * Add listeners
     */
    addListeners: function() {
      $('#newsletter-email-input').change(function() {
        newsletter.getEmailAddress();
      });
      $('#newsletter-store-select').change(function() {
        newsletter.getStoreSelection();
      });
      $('#subscribe').click(function() {
        newsletter.selectAllToggle();
        return false;
      });
      $('.selectlink a').click(function(){
        newsletter.selectLinks(this);
        return false;
      });
      $('#newsletter-subscribe-submit').click(function(){
        if (!newsletter.checkSelection()) {
          return false;
        }
      });
      $('#newsletter-header-form').submit(function(){
        newsletter.validateEmailField();
        return false;
      });
      $('#newsletter-email-input').focus(function(){
        newsletter.resetInput('#newsletter-email-input');
      });
      $('#newsletter-store-select').focus(function(){
        newsletter.resetInput('#newsletter-store-select');
      });
      $('#edit-head-info a').click(function() {
        newsletter.revealTopForm();
        return false;
      });
    },

    /**
     *	AJAX post to responsys to check if email exists.  Handles response as well.
     *
     * @param string email
     *		email address
     */
    ajaxEmailAddressCheck: function(email){
      var url = '/sites/all/modules/features/newsletters/check_email_exists.php';
      $.ajax({
        beforeSend: function(){
          newsletter.beforeSendAjaxRequest();
        },
        dataType: 'text',
        data: 'address=' + email,
        error: function(jqXHR, textStatus, errorThrown) {
          newsletter.ajaxResponseOK();
          newsletter.pushEvent('error');
        },
        jsonp: 'callback',
        timeout: 10000,
        url: url,
        success: function(response) {
          if(response == 'OK') {
            newsletter.pushEvent('success'); //GTM push to dataLayer
            newsletter.ajaxResponseOK();
          }
          if(response == 'EMAIL_EXISTS'){
            newsletter.ajaxResponseEmailExists();
            newsletter.pushEvent('error - email exists');
          }
        }
      });
    },

    /**
     *	Handler for AJAX email check in the case that response is "EMAIL_EXISTS"
     */
    ajaxResponseEmailExists: function() {
      var prefsLink = 'http://communications.wholefoodsmarket.com/pub/sf/ResponseForm?_ri_=X0Gzc2X%3DWQpglLjHJlTQGmFkwj7knwL3pW7WIUiX1tzajEi1cVXMtX%3DWQpglLjHJlTQGnSqOdp53zavuFR9Y14zeTcWAPChlG&_ei_=EhD67L35CWQqkTBBOptvy60';
      var message = Drupal.t('This email address is already registered.') + '  <a href="' + prefsLink + '">' + Drupal.t('Manage your preferences') + '</a>.';
      newsletter.setUserMessage('#newsletter-email-input', message, 'error');
    },

    /**
     *	Handler for AJAX email check in the case that response is "OK"
     */
    ajaxResponseOK: function() {
      $('label.userMessage').remove();
      $('#newsletter-email-input').removeClass('error status').addClass('valid');
      newsletter.continueSignup();
    },

    /**
     *	Handler for AJAX email check that runs before request is sent.
     *	Displays user status message to let them know check is being performed.
     */
    beforeSendAjaxRequest: function() {
      var imageTag = '<img src="' + newsletter.loading_gif + '" alt="Loading...." />';
      var message = Drupal.t('Checking to see if email address is already registered. ') + imageTag;
      newsletter.setUserMessage('#newsletter-email-input', message, 'status');
    },

    /**
     *	Check for valid email address
     *
     * @param string email
     *		Text to check to see if it is valid email address
     *
     *	@return boolean
     *		true if is valid email address, false if is not a valid email address
     */
    checkEmailPattern: function(email) {
      var re = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
      return re.test(email);
    },

    /**
     *	Check that an email newsletter option is chosen before submitting
     *
     *	@return boolean
     *		Returns false if no newsletter options are selected.
     */
    checkSelection: function() {
      var checked = false;
      $('#newsletters-form-dbu-update input[type=checkbox]').each(function(){
        if($(this).prop('checked') === true){
          checked = true;
        }
      });
      if(checked === false) {
        alert('Please select a newsletter to subscribe!');
      }
      return checked;
    },

    /**
     * Hide #content-top, reveal #content
     */
    continueSignup: function() {
      var loader = '<img src="' + newsletter.loading_gif + '" alt="Loading...">';
      if($('#newsletter-email-input').hasClass('valid')) {
        newsletter.getStoreSelection();
        $('#StoreSpecials').attr('rel', 'checked').prop('checked',true);
        $('#Local_Store_News').attr('rel', 'checked').prop('checked',true);
        $('#newsletter-header-submit').replaceWith(loader);
        $('#newsletters-form-dbu-update').submit();
      }
    },

    /**
     *	Place email address value into span and hidden form in second part of
     *  sign up process.
     */
    getEmailAddress: function() {
      var email = $('#newsletter-email-input').val();
      if(email.length > 0) {
        $('#useremail').text(email);
        $('input[name="newsletter_options[EMAIL_ADDRESS_]"]').val(email);
      }
    },

    /**
     *  Gets the chosen store in the store select input and fills the value in the
     *  hidden input in the bottom form.
     */
    getStoreSelection: function(){
      var storename = $('#newsletter-store-select').find('option').filter(':selected').attr('label'),
          storenid = $('#newsletter-store-select').find('option').filter(':selected').attr('value'),
          statecode = $('#newsletter-state-select').find('option').filter(':selected').attr('value'),
          storetext = Drupal.t('Your Store') + ': <span>' + storename + '</span>';

      $('#userstore').text(storename);
      $('.form-item-newsletter-options-StoreSpecials .description .summary').html(storetext);
      $('.form-item-newsletter-options-Local-Store-News .description .summary').html(storetext);
      $('input[name="newsletter_options[storenid]"]').val(storenid);
      $('input[name="newsletter_options[statecode]"]').val(statecode);
    },

    /**
     * Hide #content
     */
    hideBody: function() {
      $('#content').css('display', 'none');
    },

    /**
     * Load necessary functions for initialization of newsletter signup page
     */
    init: function() {
      newsletter.addListeners();
      newsletter.hideBody();
      newsletter.getEmailAddress();
      newsletter.getStoreSelection();
    },

    /**
     *	Reset email input to initial state of signup process
     *
     *	@param string selector
     *		jQuery selector of email input field. '#edit-email-address' for example
     */
    resetInput: function(selector) {
      if ($(selector).hasClass('error')) {
        $(selector).removeClass('error');
      }
      if ($(selector).hasClass('valid')) {
        $(selector).removeClass('valid');
      }
      $(selector).next('label.userMessage').remove();
    },

    /**
     * Hide #content, reveal #content-top
     */
    revealTopForm: function() {
      jQuery('#content').slideUp();
      jQuery('#content-top').slideDown();
    },

    /**
     *	Behavior to select or deselect all newsletter options depending on the link class.
     */
    selectAllToggle: function() {
      var subscribe = $('#subscribe'),
          checkbox = $('#newsletters-form-dbu-update input[type=checkbox]'),
          form_item = $('.form-item'),
          selectlink = $('.selectlink a');

      if (subscribe.hasClass('clicked')) {
        subscribe.toggleClass('clicked').text('Subscribe to All');
        checkbox.attr('rel', '').prop('checked', false);
        form_item.removeClass('selected');
        selectlink.text('Select');
      }
      else {
        subscribe.toggleClass('clicked').text('Deselect All');
        checkbox.attr('rel', 'checked').prop('checked', true);
        form_item.addClass('selected');
        selectlink.text('Deselect');
      }
    },

    /**
     *	Check and unchecks inputs based on clicking the select links.
     * Toggles Select All/Deselect All when all options are selected or deselected
     */
    selectLinks: function(context) {
      var inputs = $('#newsletters-form-dbu-update input[type=checkbox]'),
          countInputs = inputs.length,
          checkCount = 0;

      $(context).parents('div.form-item').toggleClass('selected');
      if ($(context).text() == 'Select') {
        $(context).text('Deselect');
        $(context).parents('div.description').siblings('input').attr('rel', 'checked').prop('checked',true);
      } else {
        $(context).text('Select');
        $(context).parents('div.description').siblings('input').attr('rel', '').prop('checked', false);
      }
      inputs.each(function() {
        if($(this).prop('checked')) {
          checkCount++;
        } 
        else {
          checkCount--;
        }
      });
      if (checkCount == countInputs) {
        $('#subscribe').addClass('clicked').text('Deselect All');
      }
      if (checkCount == (0 - countInputs)) {
        $('#subscribe').removeClass('clicked').text('Subscribe to all');
      }
    },

    /**
     *	Displays message of status of field validation check to user.
     *
     *	@param string selector
     *		jQuery selector of field to which to display message
     *	@param string message
     *		Message to display to user
     *	@param htmlclass
     *		Class to apply to field and field label that is created to contain the message.
     */
    setUserMessage: function(selector, message, htmlclass) {
      $(selector).next('label.userMessage').remove();
      var html = '<label class="' + htmlclass + ' userMessage">' + message + '</label>';
      $(selector).removeClass('status error').addClass(htmlclass).after(html);
    },

    /**
     *	Check email address input field for valid email addresses.
     *		1st check regex expression to match pattern of valid email address
     *		2nd check Responsys API to see if email exists via AJAX request
     *
     *	@param string email
     *		email address to be validated
     */
    validateEmailField: function() {
      var email = $('#newsletter-email-input').val();
      if(!$('#newsletter-email-input').hasClass('valid')){
        if(newsletter.checkEmailPattern(email)){
          newsletter.ajaxEmailAddressCheck(email);
        } else {
          var message = email + ' is not a valid email address';
          newsletter.setUserMessage('#newsletter-email-input', message, 'error');
        }
      }
    },

    /**
     * Push Google Analytics event into window.dataLayer
     *
     * @param {string} status
     *    subscribe status for event - either 'success' or 'error'
     */
    pushEvent: function(status) {
      var obj = {
        event: 'newsletter-subscribe',
        formLocation: 'newsletter-main',
        subscribeStatus: status
      };

      if (window.dataLayer) {
        window.dataLayer.push(obj);
      }
    }

  }; //var newsletter

  /**
   * Attach to Drupal behaviors
   */
  Drupal.behaviors.wfm_newsletter_subscription = {
    attach: function (context, settings) {
      $('body', context).once('wfm_newsletter_subscription', function () {
        newsletter.init();
      });
    }
  };
})(jQuery);
