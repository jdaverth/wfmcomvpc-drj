/**
 * Add an event listener to the "Yes- Sign in now" button to launch a login pop-up
 * @type {{attach: Drupal.behaviors.newslettersRequestLink.attach}}
 */
Drupal.behaviors.newslettersRequestLink = {
  attach: function(context, settings) {
    return (function(context, settings, $){
      $('#newsletter-preferences-request-link-unauthenticated-form', context).once('newsletter-login', function(){
        $('#newsletters-preferences-login', context).on('click', function(){
          if (janrain.capture.ui.modal.open) {
            janrain.capture.ui.modal.open();
            return false;
          }
        });
      });
    })(context, settings, jQuery);
  }
};