<?php
/**
 * @file
 * Code for the Newsletters admin screen(s).
 */

function newsletters_admin_form() {
  $form = array();
  $options_object = newsletters_get_newsletter_checkbox_options_object(NULL, FALSE);
  $options = newsletters_make_options_array($options_object);
  $form['heading'] = array (
    '#markup' => '<h2>' . t('Create Newsletter Sidebar Blocks') . '</h2>',
  );
  if ($settings = variable_get('wfm_newsletter_sidebar_blocks')) {
    $i = 0;
    foreach ($settings as $block_setting) {
      $form['newsletter_group_' . $i] = array(
        '#type' => 'fieldset',
        '#weight' => $i,
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
        '#attributes' => array(
          'class' => array('form-top-wrap'),
        ),
      );
      $form['newsletter_group_' . $i]['block_title_' . $i] = array(
        '#type' => 'textfield',
        '#title' => t('Block Title'),
        '#default_value' => $block_setting['title'],
      );
      $form['newsletter_group_' . $i]['block_blurb_' . $i] = array(
        '#type' => 'text_format',
        '#title' => t('Block Blurb'),
        '#default_value' => $block_setting['blurb']['value'],
      );
      $form['newsletter_group_' . $i]['newsletter_sidebar_blocks_' . $i] = array(
        '#type' => 'checkboxes',
        '#title' => t('Newsletter Opt-ins'),
        '#options' => $options,
        '#default_value' => newsletter_get_default_optin_values($block_setting['subscriptions']),
      );
      $form['newsletter_group_' . $i]['ri_value_' . $i] = array(
        '#type' => 'textfield',
        '#title' => t('Enter the ri value for this subscription form.'),
        '#default_value' => $block_setting['ri'],
        '#maxlength' => 256,
      );
      $form['newsletter_group_' . $i]['sub_promo_' . $i] = array(
        '#type' => 'textfield',
        '#title' => t('Enter the sub-promo value for this subscription form.'),
        '#default_value' => $block_setting['sub_promo'],
      );
      $form['newsletter_group_' . $i]['block_paths_' . $i] = array(
        '#type' => 'textarea',
        '#title' => t('Define Where Block Appears'),
        '#default_value' => $block_setting['paths'],
        '#field_suffix' => t("Specify pages by using their paths. Enter one path
          per line. The '*' character is a wildcard. Example paths are blog for
          the blog page and blog/* for every blog post. <front> is the front
          page."),
      );
      $form['newsletter_group_' . $i]['delete_button_' . $i] = array(
        '#markup' => '<div class="delete_button"><a class="remove" href="#">' .
          t('Delete this block') . '</a></div>',
        '#weight' => 10 + $i,
      );
      $i++;
    }
  }
  else {
    $form['newsletter_group_0'] = array(
      '#type' => 'fieldset',
      '#weight' => 0,
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#attributes' => array(
        'class' => array('form-top-wrap'),
      )
    );
    $form['newsletter_group_0']['block_title_0'] = array(
      '#type' => 'textfield',
      '#title' => t('Block Description'),
    );
    $form['newsletter_group_0']['block_blurb_0'] = array(
      '#type' => 'text_format',
      '#title' => t('Block Blurb'),
    );
    $form['newsletter_group_0']['newsletter_sidebar_blocks_0'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Newsletter Opt-ins'),
      '#options' => $options
    );
    $form['newsletter_group_0']['ri_value_0'] = array(
      '#type' => 'textfield',
      '#title' => t('Enter the ri value for this subscription form.'),
    );
    $form['newsletter_group_0']['sub_promo_0'] = array(
      '#type' => 'textfield',
      '#title' => t('Enter the sub-promo value for this subscription form.'),
    );
    $form['newsletter_group_0']['block_paths_0'] = array(
      '#type' => 'textarea',
      '#title' => t('Define Where Block Appears'),
      '#default_value' => $block_setting['paths'],
      '#field_suffix' => t("Specify pages by using their paths. Enter one path
        per line. The '*' character is a wildcard. Example paths are blog for
        the blog page and blog/* for every blog post. <front> is the front
        page."),
    );
  }
  $form['add_more_button'] = array(
    '#markup' => '<div class="addmore"><a id="newsletters_addmore" href="#">' .
      t('Add Another Newsletter Block') . '</a></div>',
    '#weight' => 10 + $i,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Options'),
    '#weight' => 20 + $i,
  );
  $form['#attached']['js'] = array(
    drupal_get_path('module', 'newsletters') . '/newsletters.admin.js',
  );
  $form['#attached']['css'] = array(
    drupal_get_path('module', 'newsletters') . '/newsletters.admin.css',
  );
  $form['#submit'] = array('newsletters_admin_form_submit');
  return $form;
}

function newsletters_admin_form_submit($form, &$form_state) {
  $settings = array();
  $submitted_info = $form_state['input'];
  $i = 0;
  $has_submissions = TRUE;
  while ($has_submissions) {
    if(isset($submitted_info['block_title_' . $i])) {
      $settings[$i] = array();
      $settings[$i]['title'] = $submitted_info['block_title_' . $i];
      $settings[$i]['subscriptions'] = $submitted_info['newsletter_sidebar_blocks_' . $i];
      $settings[$i]['ri'] = $submitted_info['ri_value_' . $i];
      $settings[$i]['sub_promo'] = $submitted_info['sub_promo_' . $i];
      $settings[$i]['blurb'] = $submitted_info['block_blurb_' . $i];
      $settings[$i]['paths'] = $submitted_info['block_paths_' . $i];
      $i++;
    }
    else {
      $has_submissions = FALSE;
    }
  }
  variable_set('wfm_newsletter_sidebar_blocks', $settings);
}

function newsletters_make_options_array($options_object) {
  $options_array = array();
  foreach ($options_object as $option) {
    $options_array[$option->field_newsletter_key_value] = $option->title;
  }
  return $options_array;
}

function newsletter_get_default_optin_values($options) {
  $out = array();
  foreach ($options as $option) {
    if (isset($options)) {
      $out[] = $option;
    }
  }
  return $out;
}
