/**
 * @file
 * Javascript file for Newsletters admin.
 */

(function( $ ){
  $.fn.WFMnewsletterMenu = function() {
    var newslettermenu = {

      /**
       * Add Listeners
       */
      addListeners: function() {
        $('#newsletters_addmore').click(function() {
          var count = newslettermenu.countForms();
          newslettermenu.duplicateField('#edit-newsletter-group-0', count);
          return false;
        });
        $('body').delegate('.remove', 'click', function(){
          newslettermenu.deleteFieldset(this);
          return false;
        });
      },

      countForms: function() {
        return $('.form-top-wrap').length;
      },

      deleteFieldset: function(clicked) {
        if (window.confirm(Drupal.t('Do you really want to delete this?'))) {
          $(clicked).parents('fieldset').remove();
        }
      },

      /**
       * Clone repeatable form elements
       */
      duplicateField: function(wrapper, id) {
        var clone = $(wrapper).clone();
        clone = newslettermenu.prepareClone(clone, id);
        $('.form-top-wrap').last().after(clone);
      },

      /**
       * Get everything ready.
       */
      init: function() {
        newslettermenu.addListeners();
      },

      prepareClone: function(form_item, newIdNumber) {
        if ($(form_item).children().length > 0) {
          $(form_item).children().each(function(index){
            newslettermenu.prepareClone(this, newIdNumber);
          })
        }
        if ($(form_item).attr('class')) {
          var replacement_class = newslettermenu.replaceZeros($(form_item).attr('class'), newIdNumber);
          $(form_item).removeClass().addClass(replacement_class);
        }
        if ($(form_item).attr('id')) {
          var replacement_id = newslettermenu.replaceZeros($(form_item).attr('id'), newIdNumber);
          $(form_item).attr('id', replacement_id);
        }
        if ($(form_item).attr('name')) {
          var replacement_name = newslettermenu.replaceZeros($(form_item).attr('name'), newIdNumber);
          $(form_item).attr('name', replacement_name);
        }
        if ($(form_item).attr('for')) {
          var replacement_for = newslettermenu.replaceZeros($(form_item).attr('for'), newIdNumber);
          $(form_item).attr('for', replacement_for);
        }
        $(form_item).prop('checked', false).val('');
        return form_item;
      },

      replaceZeros: function(string, newID) {
        var re = /0/gi;
        var newstring = string.replace(re, newID);
        return newstring;
      }

    };

    //Start
    newslettermenu.init();
  };
})(jQuery)

jQuery(document).ready(function(){
  jQuery().WFMnewsletterMenu();
});
