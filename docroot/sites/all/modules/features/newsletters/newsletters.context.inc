<?php
/**
 * @file
 * newsletters.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function newsletters_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = TRUE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'newsletter';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'newsletters' => 'newsletters',
      ),
    ),
    'theme' => array(
      'values' => array(
        'wholefoods' => 'wholefoods',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'newsletters-newsletters_form_dbu_update' => array(
          'module' => 'newsletters',
          'delta' => 'newsletters_form_dbu_update',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;
  $export['newsletter'] = $context;

  return $export;
}
