<?php

/**
 * @file
 * Contains WfmNewsletterToken.
 */

/**
 * Class WfmNewsletterToken
 * Class intended for the creation of random tokens used for tokenized email links.
 */
class WfmNewsletterToken {

  /**
   * @var string UPM id
   */
  private $upm_id;

  /**
   * @var int timestamp
   */
  private $timestamp;

  /**
   * @var string A date string in MySQL datetime format.
   */
  private $datetime;

  /**
   * @var string, Formatting string to be passed into PHP's date function to format date as MySQL datetime string.
   */
  private $datetimeString;

  /**
   * @var string Drupal database table to hold tokens.
   */
  private $table;

  /**
   * @var int How long tokens should be valid in seconds.
   */
  private $ttl;

  /**
   * WfmNewsletterToken constructor.
   * @param string $upm_id  UPM id
   * @param int $timestamp Timestamp
   */
  public function __construct($upm_id, $timestamp) {
    if (is_string($upm_id) && strlen($upm_id) == 36) {
      $this->upm_id = $upm_id;
    }
    else {
      throw new Exception('WfmNewsletterToken: Please supply a valid UPM id.');
    }

    if (!$timestamp) {
      $timestamp = time();
    }

    $this->datetimeString = 'Y-m-d H:i:s';

    $this->timestamp = $timestamp;

    $this->datetime = date($this->datetimeString, $timestamp); // MySQL DATETIME format

    $this->table = 'wfm_newsletter_email_tokens';

    $this->ttl = 86400; // 1 day in seconds.
  }

  /**
   * Check if a token already exists in the database associated to upm id.
   * @return boolean
   */
  public function checkIfTokenExistsByUpmId() {
    $count = db_select($this->table, 't')
      ->fields('t')
      ->condition('upm_id', $this->upm_id)
      ->execute()
      ->rowCount();

    return (bool) $count;
  }

  /**
   * Get tokens older than $this->ttl.
   */
  public function getExpiredTokens() {
    // Get expired time as MySQL datetime string.
    $expired_time = date($this->datetimeString, $this->timestamp - $this->ttl);
    $tokens = db_select('wfm_newsletter_email_tokens', 't')
      ->fields('t', array('token'))
      ->condition('created_date', $expired_time, '<')
      ->execute()
      ->fetchCol();
    return $tokens;
  }

  /**
   * Generate a token and insert it along with upm_id and created date into database.
   * @return string
   */
  public function insertTokenIntoDatabase() {
    // Only allow a single token per UPM id in database.
    if ($this->checkIfTokenExistsByUpmId()) {
      $this->deleteTokensFromDatabaseByUpmId();
    }
    $token = $this->generateToken();

    try {
      db_insert($this->table)
        ->fields(array(
          'token' => $token,
          'upm_id' => $this->upm_id,
          'created_date' => $this->datetime,
        ))
        ->execute();

      return $token;
    }
    catch(Exception $e) {
      watchdog('newsletters', $e);
    }
  }

  /**
   * Delete the database row containing the supplied token.
   * @param $tokens array, Array of tokens to be deleted.
   * @return bool
   */
  public function deleteTokensFromDatabase($tokens) {
    if (!is_array($tokens)) {
      throw new Exception('Error: $tokens must be an array');
    }
    try {
      db_delete($this->table)
        ->condition('token', $tokens, 'IN')
        ->execute();

      return TRUE;
    }
    catch (Exception $e) {
      watchdog('newsletters', $e);
    }
  }

  /**
   * Delete all tokens associated to upm id.
   * @return TRUE on success
   */
  public function deleteTokensFromDatabaseByUpmId() {
    try {
      db_delete($this->table)
        ->condition('upm_id', $this->upm_id)
        ->execute();

      return TRUE;
    }
    catch (Exception $e) {
      watchdog('newsletters', $e);
    }
  }

  /**
   * Determine if $token is for a valid upm id and if it has expired.
   * @param string $token Token string
   * @return bool
   */
  public function validateToken($token) {
    $entry = $this->getDatabaseEntryByToken($token);

    if ($entry) {
      if ($entry['upm_id'] != $this->upm_id) {
        return FALSE;
      }
      if (!$this->validateDatetime($entry['created_date'])) {
        return FALSE;
      }
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Generate a random token. Based on the code used to generate image tokens.
   * @return string
   * @see https://api.drupal.org/api/drupal/modules%21image%21image.module/function/image_style_path_token/7.x
   */
  protected function generateToken() {
    return substr(drupal_hmac_base64($this->upm_id . ':' . time(), drupal_get_private_key() . drupal_get_hash_salt()), 0, 32);
  }

  /**
   * Queries database and returns the row associated with the supplied token.
   * @param string $token Token string
   * @return array
   */
  protected function getDatabaseEntryByToken($token) {
    try {
      return db_select($this->table, 'w')
        ->fields('w')
        ->condition('token', $token)
        ->execute()
        ->fetchAssoc();
    }
    catch(Exception $e) {
      watchdog('newsletters', $e);
    }
  }

  /**
   * Determine if supplied datetime is within expiration time.
   * @param string $datetime Datetime string in MySQL datetime format.
   * @return bool
   */
  protected function validateDatetime($datetime) {
    $timestamp = strtotime($datetime);
    $difference = $this->timestamp - $timestamp;
    if ($difference <= $this->ttl) {
      return true;
    }
    return false;
  }

}
