<?php
/**
 * @file
 * newsletters.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function newsletters_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_newsletter_basics|node|newsletter|form';
  $field_group->group_name = 'group_newsletter_basics';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'newsletter';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_newsletter';
  $field_group->data = array(
    'label' => 'Basics',
    'weight' => '102',
    'children' => array(
      0 => 'body',
      1 => 'field_newsletter_category',
      2 => 'field_newsletter_image',
      3 => 'field_newsletter_key',
      4 => 'field_newsletter_sample',
      5 => 'field_frequency',
      6 => 'locations_acl',
      7 => 'title',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_newsletter_basics|node|newsletter|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_newsletter|node|newsletter|form';
  $field_group->group_name = 'group_newsletter';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'newsletter';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Newsletter',
    'weight' => '0',
    'children' => array(
      0 => 'group_newsletter_basics',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_newsletter|node|newsletter|form'] = $field_group;

  return $export;
}
