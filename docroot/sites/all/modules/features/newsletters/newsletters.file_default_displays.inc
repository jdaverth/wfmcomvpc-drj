<?php
/**
 * @file
 * newsletters.file_default_displays.inc
 */

/**
 * Implements hook_file_default_displays().
 */
function newsletters_file_default_displays() {
  $export = array();

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'default__file_styles_newsletter_image__file_field_styles_file_newsletter_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['default__file_styles_newsletter_image__file_field_styles_file_newsletter_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__file_styles_newsletter_image__file_field_styles_file_newsletter_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['image__file_styles_newsletter_image__file_field_styles_file_newsletter_image'] = $file_display;

  return $export;
}
