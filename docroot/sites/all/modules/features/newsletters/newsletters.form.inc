<?php
/**
 * @file
 * Code for the Newsletter forms.
 */

/**
 *  Form constructor for the store email address/store picker in newsletter page header.
 */
function newsletter_header_form($form, &$form_state) {
  $form = array();
  global $user;
  $defaults = newsletters_get_user_defaults($user);

  $form['email_address'] = array(
    '#type' => 'textfield',
    '#title' => t('Email Address'),
    '#default_value' => $defaults['email'],
    '#attributes' => array(
      'class' => array('email-input'),
      'id' => 'newsletter-email-input',
    ),
  );
  $form['state'] = array(
    '#type' => 'select',
    '#title' => t('Select Your Location'),
    '#options' => _wfm_blocks_get_state_options(),
    '#default_value' => $defaults['state'],
    '#attributes' => array(
      'class' => array('state-select'),
      'id' => 'newsletter-state-select'
    ),
  );
  $form['store'] = array(
    '#type' => 'select',
    '#title' => t('Select Your Store'),
    '#options' => _wfm_blocks_get_store_options(array('stores_in_development' => TRUE,'exclude_365_stores' => TRUE)),
    '#default_value' => $defaults['store'],
    '#attributes' => array(
      'class' => array('store-select'),
      'id' => 'newsletter-store-select'
    ),    
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#attributes' => array(
      'id' => 'newsletter-header-submit',
      'class' => array('newsletter-header-submit'),
    ),
  );
  $form['#attributes'] = array(
    'class' => array('store-select-form'),
  );
  $form['#attached']['js'][] = drupal_get_path('module', 'wfm_blocks') . '/store-select.js';

  return $form;
}

function newsletter_header_form_validate($form, $form_state) {
  if (!valid_email_address($form_state['values']['email_address'])) {
    form_set_error(t('Email Address'), t('Please enter a valid email address.'));
  }
  $form_state['values']['state'] = check_plain($form_state['values']['state']);
  $form_state['values']['store'] = (int)$form_state['values']['store'];
}

function newsletter_header_form_submit($form, $form_state) {
  $userApi = new WFMApiUser();
  $upm_id = $userApi->getUpmProfileIdFromEmail($form_state['values']['email_address']);
  _store_set_user_store($form_state['values']['store']);

  $subscription_options = newsletters_get_default_subscription_options();
  $subscription_options['core']['email'] = $form_state['values']['email_address'];
  if ($form_state['values']['store']) {
    $subscription_options['core']['store_primary_wfm'] = store_get_bu_from_nid($form_state['values']['store']);
  }

  newsletters_update_newsletter_subscription($subscription_options, $upm_id);
  drupal_goto('/newsletters/preferences/' . $upm_id);
}

function newsletters_get_default_subscription_options() {
  return array(
    'core' => array(
      'email_opt_out' => FALSE,
    ),
    'programs' => array(
      'esp' => array(
        'newsletter_subscription' => TRUE,
        'wfm_email_opt_in' => TRUE,
        'local_store_news_email_subscription' => TRUE,
        'store_coupons_email_subscription' => TRUE,
      ),
    ),
    'preferences' => array(
      'esp.lifestyle' => array(
        'health_wellness_email_subscription' => TRUE,
      ),
    ),
  );
}

function newsletters_update_newsletter_subscription($options, $upm_id) {
  $userApi = new WFMApiUser();

  if (isset($options['core'])) {
    $userApi->updateUpmCoreData($options['core'], $upm_id);
  }
  if (isset($options['preferences'])) {
    $userApi->updateUpmPreferences($options['preferences'], $upm_id);
  }
  if (isset($options['programs'])) {
    foreach ($options['programs'] as $program => $preferences) {
      $userApi->updateUpmProgramData($program, $preferences, $upm_id);
    }
  }
}

/**
 * Run query to get newsletter form options. Serve from cache if is in cache.
 */
function newsletters_get_newsletter_checkbox_options_object($tid = NULL, $expanded = TRUE) {
  $options = &drupal_static(__FUNCTION__);
  if (!isset($options)) {
    $cache_id = newsletters_build_options_cache_id($tid, $expanded);
    if ($cache = cache_get($cache_id)) {
      $options = $cache->data;
    }
    else {
      $query = db_select('node', 'n');
      $query->leftJoin('field_data_field_newsletter_key', 'k', 'k.entity_id = n.nid');
      $query->leftJoin('field_data_field_bc_position', 'p', 'p.entity_id = n.nid');
      $query
        ->condition('n.type', 'newsletter', '=')
        ->condition('n.status', 1, '=')
        ->fields('n', array('nid', 'title'))
        ->fields('k', array('field_newsletter_key_value'))
        ->orderBy('p.field_bc_position_value', 'ASC');

      if ($expanded) {
        $query->leftJoin('field_data_field_newsletter_image', 'i', 'i.entity_id = n.nid');
        $query->leftJoin('field_data_field_newsletter_sample', 's', 's.entity_id = n.nid');
        $query->leftJoin('field_data_field_frequency', 'f', 'f.entity_id = n.nid');
        $query->leftJoin('field_data_body', 'b', 'b.entity_id = n.nid');
        $query->leftJoin('field_data_field_bc_weight', 'w', 'w.entity_id = n.nid');
        $query
          ->fields('i', array('field_newsletter_image_fid'))
          ->fields('s', array('field_newsletter_sample_url', 'field_newsletter_sample_title'))
          ->fields('f', array('field_frequency_value'))
          ->fields('b', array('body_summary', 'body_value'))
          ->orderBy('field_bc_weight_value');
      }

      if ($tid) {
        $query->leftJoin('field_data_field_newsletter_category', 'c', 'c.entity_id = n.nid');
        $query->condition('c.field_newsletter_category_tid', $tid, '=');
      }

      $options = $query->execute()->fetchAll();
      cache_set($cache_id, $options, 'cache', CACHE_TEMPORARY);
    }
  }
  return $options;
}


function newsletters_build_options_cache_id($tid, $expanded) {
  $cache_id = 'wfm_newsletter_options';
  if ($tid) {
    $cache_id .= '_' . $tid;
  }
  if ($expanded) {
    $cache_id .= '_' . 'expanded';
  }
  return $cache_id;
}

/**
 * Function generate select options for state select.
 *
 *	@return array
 *		Form option values to be merged with form API array
 */
function newsletters_get_state_options() {
  $cache = cache_get('wfm_state_select_options', 'cache');
  if ($cache) {
    $state_options = $cache->data;
  }
  else {
    $state_options = array('0' => 'Please Select Your Location');
    $statenames = _local_get_state_list(TRUE);

    $statequery = db_select('field_data_field_postal_address', 'a');
    $statequery
      ->fields('a', array('field_postal_address_country', 'field_postal_address_administrative_area'))
      ->condition('a.bundle', 'store')
      ->distinct()
      ->orderBy('a.field_postal_address_country', 'ASC')
      ->orderBy('a.field_postal_address_administrative_area', 'ASC');
      $stateresult = $statequery->execute();

    $countries = array(
      'CA' => 'Canada',
      'UK' => 'United Kingdom',
      'US' => 'United States'
    );
    foreach ($stateresult as $row) {
      $country = $row->field_postal_address_country;
      $state = $row->field_postal_address_administrative_area;
      if ($country == 'GB') {
        $country = 'UK';
        $state = 'UK';
      }
      $state_options[$countries[$country]][$state] = $statenames[$state];
    }
    ksort($state_options);
    asort($state_options['United States']);
    cache_set('wfm_state_select_options', $state_options, 'cache');
  }
  return $state_options;
}

/**
 * Function generate select options for store select.
 *
 *	@return array
 *		Form option values to be merged with form API array
 */
function newsletters_get_store_options() {
  $cache = cache_get('wfm_store_select_options', 'cache');
  if ($cache) {
    $store_options = $cache->data;
  }
  else {
    $store_options = array('' => 'Please Select a Store');
    $statenames = _local_get_state_list(TRUE);

    $query = db_select('node', 'n');

    $query->leftJoin('field_data_field_postal_address', 'a', 'a.entity_id = n.nid');
    $query->leftJoin('field_data_field_opening_soon', 'o', 'o.entity_id = n.nid');
    $query
      ->condition('n.type', 'store')
      ->condition('n.status', '1')
      ->condition('o.field_opening_soon_value', '0')
      ->fields('n', array('title', 'nid'))
      ->fields('a', array('field_postal_address_administrative_area', 'field_postal_address_country', 'field_postal_address_locality', 'field_postal_address_thoroughfare'))
      ->orderBy('title', 'ASC');
    $result = $query->execute();
    foreach ($result as $row) {
      $country = $row->field_postal_address_country;
      $state = $row->field_postal_address_administrative_area;
      $name = $row->title;
      $nid = $row->nid;
      $city = $row->field_postal_address_locality;
      $street = $row->field_postal_address_thoroughfare;
      if ($country == 'GB') {
        $country = 'UK';
        $state = 'UK';
      }
      $store_options[$state . $city][$nid]= $name . ' - ' . $street;
    }
    ksort($store_options);
    cache_set('wfm_store_select_options', $store_options, 'cache');
  }
  return $store_options;
}

/**
 * Get default values from user object
 *
 *	@return array
 *		Default user values for email address and local store
 */
function newsletters_get_user_defaults($user) {
  $defaults = array();
  $defaults['email'] = NULL;
  $defaults['state'] = NULL;
  $defaults['store'] = NULL;
  if ($store = store_get_user_store()) {
    $defaults['state'] = $store->field_postal_address[LANGUAGE_NONE][0]['administrative_area'];
    $defaults['store'] = $store->nid;
  }
  if ($user->uid > 0) {
    $defaults['email'] = $user->mail;
  }
  if (isset($_GET['state'])) {
    $defaults['state'] = check_plain($_GET['state']);
  }
  return $defaults;
}

/**
 * Creates form renderable array for Healthy Eating Subscription Form
 */
function newsletters_he_newsletter_form() {
  $form = array(
    '#attached' => array(
      'js' => array(
        drupal_get_path('module', 'newsletters') . '/healthy-eating.js' => array(
          'type' => 'file',
          'scope' => 'footer',
        ),
      ),
    ),
    'EMAIL_ADDRESS_' => array(
      '#type' => 'textfield',
      '#title' => t('Email Address'),
      '#attributes' => array(
        'class' => array('email'),
      ),
    ),
    'HEALTHY_EATING' => array(
      '#type' => 'hidden',
      '#value' => 1,
    ),
    'STORESPECIALS' => array(
      '#type' => 'hidden',
      '#value' => '1',
    ),
    'RECIPES' => array(
      '#type' => 'hidden',
      '#value' => '1',
    ),
    'WHOLE_DEAL' => array(
      '#type' => 'hidden',
      '#value' => '1',
    ),
    'PROMO' => array(
      '#type' => 'hidden',
      '#value' => 'WFM.com',
    ),
    'SUB_PROMO' => array(
      '#type' => 'hidden',
      '#value' => 'HE_Series_SignUp_2014',
    ),
    'EMAIL_PERMISSION_STATUS_' => array(
      '#type' => 'hidden',
      '#value' => 'I',
    ),
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Subscribe'),
    ),
    '#submit' => array(
      0 => 'newsletters_form_dbu_update_callback',
    ),
  );
  return $form;
}

/**
 * Creates form renderable array for sidebar subscription forms
 */
function newsletters_sidebar_subscription_form($form_vars) {
  $form = array(
    '#attributes' => array (
      'class' => array('newsletter-ajax-form'),
    ),
    'blurb_wrap' => array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array('newsletter-text-wrap')
      ),
      'Heading' => array(
        '#markup' => 'This will go away.'
      ),
      'Blurb' => array(
        '#markup' => 'This will go away.'
      ),
    ),
    '#attached' => array(
      'js' => array(
        drupal_get_path('module', 'newsletters') . '/newsletters.sidebar.js' => array(
          'type' => 'file',
          'scope' => 'footer',
        ),
      ),
    ),
    'input_wrap' => array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array('newsletter-input-wrap')
      ),
      'EMAIL_ADDRESS_' => array(
        '#type' => 'textfield',
        '#title' => t('Email Address'),
        '#attributes' => array(
          'class' => array('email'),
        ),
      ),
      'submit' => array(
        '#type' => 'submit',
        '#value' => t('Subscribe'),
      ),
    ),
    'PROMO' => array(
      '#type' => 'hidden',
      '#value' => 'WFM.com',
    ),
    'SUB_PROMO' => array(
      '#type' => 'hidden',
      '#value' => 'default',
    ),
  );
  return $form;
}

function newsletters_sidebar_subscription_form_validate($form, $form_state) {
  if (!valid_email_address($form_state['values']['email_address'])) {
    form_set_error(t('Email Address'), t('Please enter a valid email address.'));
  }
}

function _newsletters_get_webform_selections() {
  $component = array();
  $query = db_select('node', 'n');
  $query->leftJoin('field_data_field_newsletter_key', 'k', 'k.entity_id = n.nid');
  $query->leftJoin('field_data_field_newsletter_image', 'i', 'i.entity_id = n.nid');
  $query->leftJoin('field_data_field_newsletter_category', 'c', 'c.entity_id = n.nid');
  $query->leftJoin('field_data_field_newsletter_sample', 's', 's.entity_id = n.nid');
  $query->leftJoin('field_data_field_frequency', 'f', 'f.entity_id = n.nid');
  $query->leftJoin('field_data_body', 'b', 'b.entity_id = n.nid');
  $query
    ->condition('n.type', 'newsletter', '=')
    ->condition('c.field_newsletter_category_tid', '167', '=')
    ->fields('n', array('nid', 'title'))
    ->fields('k', array('field_newsletter_key_value'))
    ->fields('i', array('field_newsletter_image_fid'))
    ->fields('s', array('field_newsletter_sample_url', 'field_newsletter_sample_title'))
    ->fields('f', array('field_frequency_value'))
    ->fields('b', array('body_summary'))
  ;
  $result = $query->execute();
  $items = '';
  foreach ($result as $row) {
    $items .= $row->field_newsletter_key_value . '|' . $row->title . "\n";
  }
  $component['name'] = 'Select Newsletters for Subscription';
  $component['form_key'] = 'wfm_newsletter_subscriptions';
  $component['type'] = 'select';
  $component['extra'] = array(
    'items' => $items,
    'multiple' => 1,
    'title_display' => 'before',
  );
  $component['mandatory'] = 1;
  $component['weight'] = 2;
  $component['pid'] = 0;

  return $component;
}
