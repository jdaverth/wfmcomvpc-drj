<?php
// http://local.wholefoodsmarket.com/newsletters/preferences/849730FB-0305-4988-B287-7B9EFABC0F89
// https://api-v3-integration.wholelabs.com/v3/users/profile/data_types
// https://api-v3-qa.wholelabs.com/v3/docs/
// https://ignite.epsilon.com/harmony/harmony_documentation/w/harmony_documentation2/12773.send-a-real-time-message-rtm

/**
 * Page callback function.
 * @param string $url_arg Third url segment. /newsletters/preferences/$url_arg
 * @return array|string Renderable array.
 * @see newsletters_menu()
 */
function newsletters_preferences_page_callback($url_arg) {

  $userApi = new WFMApiUser();

  $uuid = check_plain($url_arg);

  $upm_profile = $userApi->getUpmProfile($uuid);

  if (!$upm_profile) {
    return array(
      'header' => array(
        '#type' => 'markup',
        '#markup' => '<h2>' . wfm_template_text_get_text('wfm_template_text_newsletter_preferences_error_header') . '</h2>' .
          '<p>' . wfm_template_text_get_text('wfm_template_text_newsletter_preferences_error_message') . '</p>'
      )
    );
  }

  if (newsletters_user_can_edit_preferences($upm_profile, $uuid)) {

    // This page is meant for interacting with UPM data. Varnish could interfere. Send no-cache header.
    drupal_add_http_header('Cache-Control', 'private, no-cache, no-store, must-revalidate, max-age=0');
    drupal_add_http_header('Pragma', 'no-cache');

    // This is a hacky work-around because there's currently a delay on updating core data.
    // This should be removed if the delay on updating core data goes away.
    $core_data = FALSE;
    if (isset($_COOKIE['Drupal_visitor_wfm_newsletter_preference_submitted_data'])) {
      $cookie_data = $_COOKIE['Drupal_visitor_wfm_newsletter_preference_submitted_data'];
      $core_data = json_decode($cookie_data);
    }
    if ($core_data) {
      foreach ($core_data as $key => $value) {
        $upm_profile['core'][$key] = $value;
      }
      user_cookie_delete('wfm_newsletter_preference_submitted_data');
    }

    drupal_set_title(t('Email Preference Center'));
    $form = drupal_get_form('newsletters_preferences_form', $upm_profile);
    return array(
      'header' => array(
        '#type' => 'markup',
        '#markup' => newsletters_preferences_header_callback($upm_profile['core']),
      ),
      'form' => array(
        '#type' => 'markup',
        '#markup' => drupal_render($form),
      ),
    );
  }
  else {
    // If user is not authorized to edit preferences.
    if (user_is_logged_in()) {
      drupal_goto('newsletters/preferences/request-link/auth');
    }
    else {
      drupal_goto('newsletters/preferences/request-link');
    }
  }
}

/**
 * Page callback function for 'newsletters/preferences/request-link/%' and 'newsletters/preferences/request-link'
 * @param $step int Step we're in for multi-step form.
 * @return array Renderable array.
 */
function newsletters_preferences_request_link_page_callback($step) {
  global $user;
  drupal_set_title(t('Email Preference Center'));
  if ($user->uid > 0) {
    // If user is authenticated. This will happen if they choose the "Login Now" option
    $user_api = new WFMApiUser;
    $janrain_uuid = $user_api->getJanrainUuidFromDrupalUserId($user->uid);
    $upm_uuid = check_plain($user_api->getUpmProfileIdFromJanrainId($janrain_uuid));
    drupal_goto('newsletters/preferences/' . $upm_uuid);
  }
  else {
    // If user is unauthenticated.
    $header_text = '<h2>' . wfm_template_text_get_text('wfm_template_text_newsletter_preferences_page_title') . '</h2>';
    $form = drupal_get_form('newsletter_preferences_request_link_unauthenticated_form', intval($step));
    $header_text .=  wfm_template_text_get_text('wfm_template_text_newsletter_preferences_verify_identity');
  }

  return array(
    'text' => array(
      '#type' => 'markup',
      '#markup' => $header_text,
    ),
    'form' => array(
      '#type' => 'markup',
      '#markup' => drupal_render($form),
    ),
  );
}

/**
 * Page callback function for 'newsletters/preferences/request-link/auth'.
 * @return array Renderable array
 */
function newsletters_preferences_request_link_auth_page_callback() {
  global $user;
  if ($user->uid == 0) {
    // If user is unauthenticated. Just in case.
    drupal_goto('newsletters/preferences/request-link');
  }
  drupal_set_title(t('Email Preference Center'));
  $form = drupal_get_form('newsletter_preferences_request_link_authenticated_form', $user);
  $header_text = '<h2>' . wfm_template_text_get_text('wfm_template_text_newsletter_preferences_page_title') . '</h2>';
  $header_text .= wfm_template_text_get_text('wfm_template_text_newsletter_preferences_verify_identity');
  $header_text .= '<p>We\'ll send an email to <strong>' . $user->mail . '</strong> with a private link.';

  return array(
    'text' => array(
      '#type' => 'markup',
      '#markup' => $header_text,
    ),
    'form' => array(
      '#type' => 'markup',
      '#markup' => drupal_render($form),
    ),
    'after_form' => array(
      '#type' => 'markup',
      '#markup' => wfm_template_text_get_text('wfm_template_text_newsletter_preferences_request_no_email_help_text'),
    )
  );
}

/**
 * Page callback for 'newsletters/preferences/request-link/confirmation'.
 * @return string HTML for body content of page.
 */
function newsletters_preferences_request_link_confirmation_callback() {
  // Disable Varnish caching because we have a dynamic email address in the body of the page.
  drupal_add_http_header('Cache-Control', 'private, no-cache, no-store, must-revalidate, max-age=0');
  drupal_add_http_header('Pragma', 'no-cache');
  drupal_page_is_cacheable(FALSE);

  drupal_set_title(t('Email Preference Center'));
  $email_address = 'your email address';

  if (isset($_SESSION['newsletter_preferences']['email_address'])) {
    $email_address = $_SESSION['newsletter_preferences']['email_address'];
  }

  $html = '<h2>' . wfm_template_text_get_text('wfm_template_text_newsletter_preferences_request_link_confirmation_heading') . '</h2>';
  $html .= '<p class="request-link-confirmation">' . t(wfm_template_text_get_text('wfm_template_text_newsletter_preferences_request_link_confirmation_body'),
      array('@email' => $email_address)) . '</p>';
  return $html;
}

/**
 * Renders header text for /newsletters/preferences/{upm_id} page.
 * @param array $upm_core The 'core' portion of a UPM user profile.
 * @return string Rendered HTML for header portion of the /newsletters/preferences page.
 * @see newsletters_preferences_page_callback()
 */
function newsletters_preferences_header_callback($upm_core) {
  $html = '<h2>' . wfm_template_text_get_text('wfm_template_text_newsletter_preferences_page_title') . '</h2>';
  $html .= wfm_template_text_get_text('wfm_template_text_newsletter_preferences_intro_text');
  return $html;
}

/**
 * Form constructor function for big form on /newsletters/preferences.
 * @param array $form Form array.
 * @param array $form_state Form state array.
 * @param array $user_profile UPM user profile.
 * @return array Form array.
 */
function newsletters_preferences_form($form, &$form_state, $user_profile) {
  $newsletter_options_object = newsletters_get_newsletter_checkbox_options_object(2007391);
  $newsletter_options = newsletters_render_options_array($newsletter_options_object);
  $newsletter_option_keys = newsletters_get_option_keys($newsletter_options_object);

  $form['#attributes'] = array(
    'class' => array(
      'store-select-form'
    ),
  );

  $form['personal_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Personal Info'),
  );
  $form['personal_info']['description'] = array(
    '#type' => 'markup',
    '#prefix' => '<span class="required_note">',
    '#markup' => t('* required fields'),
    '#suffix' => '</span>',
  );
  $form['personal_info']['core|first_name'] = array(
    '#type' => 'textfield',
    '#title' => t('First Name'),
    '#attributes' => array(
      'class' => array('newsletter-preferences__text-input', 'newsletter-preferences__first-name')
    ),
    '#default_value' => $user_profile['core']['first_name'],
  );
  $form['personal_info']['core|last_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Last Name'),
    '#attributes' => array(
      'class' => array('newsletter-preferences__text-input', 'newsletter-preferences__last-name')
    ),
    '#default_value' => $user_profile['core']['last_name'],
  );
  $form['personal_info']['core|email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email address'),
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('newsletter-preferences__text-input', 'newsletter-preferences__email')
    ),
    '#default_value' => $user_profile['core']['email'],
  );

  $form['personal_info']['core|zip'] = array(
    '#type' => 'textfield',
    '#title' => t('Zip/Postal Code'),
    '#default_value' => $user_profile['core']['zip'],
  );

  $form['personal_info']['state-store'] = array(
    '#type' => 'fieldset',
    '#title' => t('Store'),
    '#attributes' => array(
      'id' => 'newsletter-preferences-state-store',
    ),
  );
  $form['personal_info']['state-store']['core|state_province'] = array(
    '#type' => 'select',
    '#options' => _wfm_blocks_get_state_options(),
    '#attributes' => array(
      'class' => array('state-select'),
    ),
    '#default_value' => $user_profile['core']['state_province'],
  );
  $form['personal_info']['state-store']['core|store_primary_wfm'] = array(
    '#type' => 'select',
    '#options' => _wfm_blocks_get_store_options(array('stores_in_development' => TRUE, 'exclude_365_stores' => TRUE)),
    '#attributes' => array(
      'class' => array('store-select'),
    ),
    '#default_value' => store_get_storenid_from_business_unit($user_profile['core']['store_primary_wfm']),
  );

  $form['personal_info']['birthday'] = array(
    '#type' => 'fieldset',
    '#title' => t('Birthday'),
  );
  $form['personal_info']['birthday']['core|birth_month'] = array(
    '#type' => 'select',
    '#options' => array(
      0 => t('Month'),
      '01' => t('January'),
      '02' => t('February'),
      '03' => t('March'),
      '04' => t('April'),
      '05' => t('May'),
      '06' => t('June'),
      '07' => t('July'),
      '08' => t('August'),
      '09' => t('September'),
      '10' => t('October'),
      '11' => t('November'),
      '12' => t('December')
    ),
    '#default_value' => newsletters_convert_int_to_upm_strings($user_profile['core']['birth_month']),
    '#disabled' => (bool)$user_profile['core']['birth_month'],
  );
  $form['personal_info']['birthday']['core|birth_day'] = array(
    '#type' => 'select',
    '#options' => newsletters_get_numeric_options(1, 31, 'Day'),
    '#default_value' => newsletters_convert_int_to_upm_strings($user_profile['core']['birth_day']),
    '#disabled' => (bool)$user_profile['core']['birth_day'],
  );

  $form['personal_info']['core|gender'] = array(
    '#type' => 'select',
    '#title' => t('Gender'),
    '#options' => array(
      0 => t('Select'),
      'Female' => t('Female'),
      'Male' => t('Male'),
      'Other' => t('Other'),
      'None_Specified' => t('Not sayin\''),
    ),
    '#default_value' => $user_profile['core']['gender'],
  );

  $form['personal_info']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Changes'),
    '#attributes' => array(
      'id' => 'top-save-changes',
    ),
  );

  $form['lifestyle_interests'] = array(
    '#type' => 'fieldset',
    '#title' => t('Lifestyle &amp; Interests'),
  );
  $form['lifestyle_interests']['description'] = array(
    '#type' => 'markup',
    '#markup' => wfm_template_text_get_text('wfm_template_text_newsletter_preferences_lifestyle_text'),
  );
  $form['lifestyle_interests']['lifestyle_select_all'] = array(
    '#type' => 'checkbox',
    '#title' => t('Select all lifestyle &amp; interest topics'),
  );

  $lifestyle_local_options = array(
    'preferences|esp.lifestyle|new_products_preference' => t('New Products'),
    //'programs|esp|store_coupons_email_subscription' => t('Sales and Coupons'),
    'preferences|esp.lifestyle|body_beauty_preference' => t('Beauty and Body Care'),
    'programs|wfm_mobile_app|ic_delivery' => t('Grocery Delivery'),
    'preferences|esp.lifestyle|online_ordering_preference' => t('Catering'),
  );

  $form['lifestyle_interests']['lifestyle_local'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Local'),
    '#options' => $lifestyle_local_options,
    '#default_value' => newsletter_preferences_checkbox_default_values($lifestyle_local_options, $user_profile),
  );

  $lifestyle_diet_options = array(
    'preferences|dietary|gluten_free_preference' => t('Gluten Free'),
    'preferences|esp.dietary|dairy_free_preference' => t('Dairy Free'),
    'preferences|dietary|vegetarian_preference' => t('Vegetarian'),
    'preferences|dietary|vegan_preference' => t('Vegan'),
  );

  $form['lifestyle_interests']['lifestyle_diet'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Diet'),
    '#options' => $lifestyle_diet_options,
    '#default_value' => newsletter_preferences_checkbox_default_values($lifestyle_diet_options, $user_profile),
  );

  $lifestyle_lifestyle_options = array(
    'preferences|esp.dietary|healthy_eating_preference' => t('Healthy Eating'),
    'preferences|esp.lifestyle|children_parenting_preference' => t('Cooking for Children or Family'),
    'preferences|esp.lifestyle|pets_preference' => t('Pets'),
    'preferences|esp.lifestyle|recipe_preference' => t('Recipes'),
  );

  $form['lifestyle_interests']['lifestyle_lifestyle'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Lifestyle'),
    '#options' => $lifestyle_lifestyle_options,
    '#default_value' => newsletter_preferences_checkbox_default_values($lifestyle_lifestyle_options, $user_profile),
  );

  $lifestyle_jobs_options = array(
    'programs|esp|career_preference' => t('Whole Foods Market Careers'),
  );

  $form['lifestyle_interests']['lifestyle_jobs'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Jobs'),
    '#options' => $lifestyle_jobs_options,
    '#default_value' => newsletter_preferences_checkbox_default_values($lifestyle_jobs_options, $user_profile),
  );

  $form['email_subscriptions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email Subscriptions'),
  );
  $form['email_subscriptions']['description'] = array(
    '#type' => 'markup',
    '#markup' => wfm_template_text_get_text('wfm_template_text_newsletter_preferences_subscriptions_text'),
  );
  $form['email_subscriptions']['subscribe_all'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sign me up for everything!'),
  );
  $form['email_subscriptions']['options'] = array(
    '#type' => 'checkboxes',
    '#options' => $newsletter_options,
    '#default_value' => newsletter_preferences_checkbox_default_values($newsletter_option_keys, $user_profile),
  );

  $form['core|email_opt_out'] = array(
    '#type' => 'checkbox',
    '#title' => t('Unsubscribe me from everything.'),
    '#default_value' => $user_profile['core']['email_opt_out'],
  );

  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#attributes' => array(
      'id' => 'cancel-button',
    ),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Changes'),
    '#attributes' => array(
      'id' => 'save-button',
    ),
  );

  $form['#attached']['js'] = array(
    drupal_get_path('module', 'wfm_blocks') . '/store-select.js',
    drupal_get_path('module', 'newsletters') . '/newsletter-preferences.js'
  );

  $form['upm_id'] = array(
    '#type' => 'hidden',
    '#value' => check_plain($user_profile['core']['profile_id']),
  );

  return $form;
}

/**
 * Form validation function.
 * @param $form
 * @param $form_state
 */
function newsletters_preferences_form_validate($form, &$form_state) {
  $zip_error = false;
  // Email validation
  if (!valid_email_address($form_state['values']['core|email'])) {
    form_set_error(t('Email Address Error'), t('Please enter a valid email address.'));
  }

  // Zip code validation
  $form_state['values']['core|zip'] = trim(check_plain($form_state['values']['core|zip']));

  if ($form_state['values']['core|zip']) {
    if (strlen($form_state['values']['core|zip']) < 5 || strlen($form_state['values']['core|zip']) > 8) {
      $zip_error = true;
    }
    $zip_regex = '/^[a-zA-Z0-9-_ ]+$/'; // Matches alphanumeric, underscore, hyphen and space.
    if (!preg_match($zip_regex, $form_state['values']['core|zip'])) {
      $zip_error = true;
    }
  }

  if ($zip_error) {
    form_set_error(t('Postal Code Error'), t('Please enter a valid postal code.'));
  }

  // Filter submitted values
  $form_state['values']['upm_id'] = check_plain($form_state['values']['upm_id']);
  $form_state['values']['core|first_name'] = check_plain($form_state['values']['core|first_name']);
  $form_state['values']['core|last_name'] = check_plain($form_state['values']['core|last_name']);
  $form_state['values']['core|store_primary_wfm'] = check_plain($form_state['values']['core|store_primary_wfm']);
  $form_state['values']['core|birth_month'] = check_plain($form_state['values']['core|birth_month']);
  $form_state['values']['core|birth_day'] = check_plain($form_state['values']['core|birth_day']);
  $form_state['values']['core|gender'] = check_plain($form_state['values']['core|gender']);
  $form_state['values']['lifestyle_local'] = newsletters_convert_checkboxes_to_boolean($form_state['values']['lifestyle_local']);
  $form_state['values']['lifestyle_diet'] = newsletters_convert_checkboxes_to_boolean($form_state['values']['lifestyle_diet']);
  $form_state['values']['lifestyle_lifestyle'] = newsletters_convert_checkboxes_to_boolean($form_state['values']['lifestyle_lifestyle']);
  $form_state['values']['lifestyle_jobs'] = newsletters_convert_checkboxes_to_boolean($form_state['values']['lifestyle_jobs']);
  $form_state['values']['options'] = newsletters_convert_checkboxes_to_boolean($form_state['values']['options']);
  $form_state['values']['core|email_opt_out'] = (bool)$form_state['values']['core|email_opt_out'];

  // Uncheck all subscription options if "Unsubscribe All" option is set.
  if ($form_state['values']['core|email_opt_out'] == 1) {
    foreach ($form_state['values']['options'] as $key => $value) {
      $form_state['values']['options'][$key] = false;
    }
  }
}

/**
 * Form submit callback.
 * @param $form
 * @param $form_state
 */
function newsletters_preferences_form_submit($form, &$form_state) {
  // Exit if 'Cancel' button was used to submit form.
  if ($form_state['triggering_element']['#id'] == 'edit-cancel') {
    return;
  }

  // Take form submission and munge it into the structure of a UPM profile.
  // These are all text fields and select boxes. No checkboxes.
  $profile_id = $form_state['values']['upm_id'];
  $profile = newsletters_build_profile_array($form_state['values']);
  $profile = newsletters_filter_non_upm_fields($profile);

  // Remove core values with '0' values. These are fields that haven't been changed by the use.
  // Sending these along to UPM results in 500 error.
  $profile['core'] = array_filter($profile['core'], function($value) {
    if ($value !== '0') {
      return true;
    }
    return false;
  });

  if ($profile_id && $profile) {
    // Save submitted values in a cookie in a hacky work-around to get deal with message queue delay on core fields.
    user_cookie_save(array('wfm_newsletter_preference_submitted_data' => json_encode($profile['core'])));

    if ($profile['core']['store_primary_wfm']) {
      _store_set_user_store($profile['core']['store_primary_wfm']);
      $profile['core']['store_primary_wfm'] = store_get_bu_from_nid($profile['core']['store_primary_wfm']);
    }

    // Make requests to UPM api endpoints.
    $error = false;
    $userApi = new WFMApiUser();
    if (!$response = $userApi->updateUpmCoreData($profile['core'], $profile_id)) {
      $error = true;
    }
    else {
      if (!isset($response['msg']) || $response['msg'] != 'Messages put on queue.') {
        $error = true;
      }
    }
    if (!$response = $userApi->updateUpmPreferences($profile['preferences'], $profile_id)) {
      $error = true;
    }
    else {
      if (!isset($response['did_update']) || !$response['did_update']) {
        $error = true;
      }
    }
    foreach($profile['programs'] as $program => $data) {
      if (!$response = $userApi->updateUpmProgramData($program, $data, $profile_id)) {
        $error = true;
      }
      else {
        if (!isset($response['status']) || !$response['status']) {
          $error = true;
        }
      }
    }
    if ($error) {
      drupal_set_message(t('There was an error submitting your data. Please check your selections below and try saving again.'), 'error');
    }
  }
}

/**
 * Render array of newsletter option objects into HTML usable as checkbox options.
 * @param array $options_array Array of newsletter option objects.
 * @return array
 */
function newsletters_render_options_array($options_array) {
  $options = array();
  foreach ($options_array as $object) {
    $label = '<span class="newsletter-preferences__option-title">' . check_plain($object->title) . '</span>';;
    if ($object->field_newsletter_image_fid) {
      $file = file_load($object->field_newsletter_image_fid);
      $derivative = image_style_url('newsletters_155x100', $file->uri);
      $label .= '<img class="newsletter-preferences__option-image" src="' . $derivative . '" alt="' . $object->title . '" />';
    }
    $label .= '<span class="newsletter-preferences__option-text">' . check_plain($object->body_summary) . '</span>';
    $options[$object->field_newsletter_key_value] = $label;
  }
  return $options;
}

/**
 * Get an array of numbers usable as checkbox options.
 * @param int $bottom Lowest value.
 * @param int $top Highest value.
 * @param string $default_text Default option text.
 * @return array Array usable as checkbox options.
 */
function newsletters_get_numeric_options($bottom, $top, $default_text = '') {
  $options = array();
  $i = 0;

  if ($default_text) {
    $options[0] = t($default_text);
  }

  while ($i <= $top) {
    if ($i >= $bottom) {
      $key = strval($i);
      if (strlen($key) < 2) {
        $key = '0' . $key;
      }
      $options[$key] = $i;
    }
    $i++;
  }

  return $options;
}

/**
 * Iterate over checkbox options array and compare them to UPM profile to determine if they're checked.
 * This will filter out options that aren't selected in the UPM profile.
 * @param array $options Checkbox options array.
 * @param array $profile UPM profile array.
 * @return array Array of values that are present in the options array and UPM profile.
 */
function newsletter_preferences_checkbox_default_values($options, $profile) {
  return array_filter(array_keys($options), function($key) use ($profile){
    $array_parts = explode('|', $key);

    foreach ($array_parts as $array_key) {
      if (array_key_exists($array_key, $profile)) {
        $profile = $profile[$array_key];
      }
    }

    if ($profile && !is_array($profile)) {
      return TRUE;
    }
    return FALSE;
  });
}

/**
 * Get key values from array of newsletter option objects.
 * @param array $options Array of newsletter option objects.
 * @return array Array of newsletter option key values.
 */
function newsletters_get_option_keys($options) {
  $keys = array();
  foreach($options as $option) {
    $keys[$option->field_newsletter_key_value] = $option->title;
  }
  return $keys;
}

/**
 * Take values submitted to the newsletter preference form and rebuild them into the structure of the UPM profile array.
 * @param $submission Submitted values from $form_state['values']
 * @param array $profile UPM user profile array.
 * @return array
 */
function newsletters_build_profile_array($submission, $profile = array()) {
  foreach($submission as $field_key => $field_value) {
    if (is_array($field_value)) {
      // If value is an array then recursively call this function on the child array.
      $profile = newsletters_build_profile_array($field_value, $profile);
    }
    else {
      // Field keys were written like preferences|esp.dietary|dairy_free_preference.
      // The "|" was an arbitrarily chosen character because "." appears in field key text.
      $keys = explode('|', $field_key);
      // Assigning $arr to profile by reference means that changes performed to $arr will also happen to $profile.
      $arr = &$profile;
      foreach ($keys as $key) {
        if (!isset($arr[$key])) {
          $arr[$key] = array();
        }
        // Assigning this by reference in a loop will cause the key values to be assigned to $profile as an associative
        // array. It will become $profile['preferences']['esp.dietary']['dairy_free_preference'].
        $arr = &$arr[$key];
      }
      // Assign the value. $profile['preferences']['esp.dietary']['dairy_free_preference'] = bool
      $arr = $field_value;
      // Now unset so that the key values don't continue to stack.
      unset($arr);
    }
  }
  return $profile;
}

/**
 * Filter out non-array values.
 * @param array $profile Submitted profile with UPM values sorted into arrays.
 * @return array Array of profile values that are arrays.
 */
function newsletters_filter_non_upm_fields($profile) {
  return array_filter($profile, function($field){
    return is_array($field);
  });
}

/**
 * Convert Drupal checkbox submitted form option values into booleans from strings.
 * @param array $profile_group Array of checkobx options.
 * @return array
 */
function newsletters_convert_checkboxes_to_boolean($profile_group) {
  array_walk_recursive($profile_group, function(&$value){
    $value = (bool)$value;
  });
  return $profile_group;
}

/**
 * Convert integers into at least two-character string. 1 becomes '01', for example.
 * @param int $int Number to convert.
 * @return string Number converted to string.
 */
function newsletters_convert_int_to_upm_strings($int) {
  $out = check_plain(strval($int));
  if (strlen($out) < 2) {
    $out = '0' . $out;
  }
  return $out;
}

/**
 * Determine if user has access to edit the newsletters preference page. Returns TRUE if user has access, FALSE if not.
 * @param $upm_profile
 * @return bool
 * @throws Exception
 */
function newsletters_user_can_edit_preferences($upm_profile, $upm_id) {
  if (user_is_logged_in()) {
    // Authenticated user.
    global $user;
    $user_api = new WFMApiUser();
    $janrain_uuid = $user_api->getJanrainUuidFromDrupalUserId($user->uid);
    if ($upm_profile['related_ids']['janrain'] == $janrain_uuid) {
      return TRUE;
    }
  }
  else {
    // Unauthenticated user.
    $parameters = drupal_get_query_parameters();
    if (isset($parameters['token'])) {
      // If we have an access token.
      $interface = new WfmNewsletterToken($upm_id, time());
      $token = check_plain($parameters['token']);
      if ($interface->validateToken($token)) {
        return TRUE;
      }
    }
    else {
      // No access token. Checking to see if we have PII in the user profile.
      $contains_pii = FALSE;
      // Our legal definition of PII is if the info contains first name and last name and another piece of data.
      // We'll always have email, so this meets the requirements.
      if ($upm_profile['core']['first_name'] && $upm_profile['core']['last_name']) {
        $contains_pii = TRUE;
      }
      if (!$contains_pii) {
        // If there is no PII generate an auth token and redirect to profile with token in URL.
        $interface = new WfmNewsletterToken($upm_id, time());
        $token = $interface->insertTokenIntoDatabase();
        drupal_goto('/newsletters/preferences/' . $upm_id, array('query' => array('token' => $token)));
      }
    }
  }
  return false;
}

/**
 * Form constructor callback function. Used on /newsletters/preferences/request-link/auth page.
 * @param $form
 * @param $form_state
 * @param $user Drupal user object
 * @return mixed
 */
function newsletter_preferences_request_link_authenticated_form($form, &$form_state, $user) {
  $form['email'] = array(
    '#type' => 'hidden',
    '#default_value' => $user->mail
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Request Link'),
  );
  return $form;
}

/**
 * Form validate function.
 * @param $form
 * @param $form_state
 * @see newsletter_preferences_request_link_authenticated_form()
 */
function newsletter_preferences_request_link_authenticated_form_validate($form, &$form_state) {
  // The only way this would occur is if somebody edited a hidden field and put in garbage.
  if (!valid_email_address($form_state['input']['email'])) {
    form_set_error('email', t('Looks like that\'s an invalid email address. Please try again.'));
  }
}

/**
 * Form submit function.
 * @param $form
 * @param $form_state
 * @see newsletter_preferences_request_link_authenticated_form()
 */
function newsletter_preferences_request_link_authenticated_form_submit($form, &$form_state) {
  // Email is validated in validate callback function.
  $email = $form_state['input']['email'];
  $token = newsletters_preferences_get_harmony_auth_token();
  if (!$token) {
    drupal_set_message(wfm_template_text_get_text('wfm_template_text_newsletter_preferences_error_message'), 'error');
    return;
  }
  $success = newsletters_preferences_send_tokenized_email($email, $token);
  if ($success) {
    drupal_goto('newsletters/preferences/request-link/confirmation');
  }
  else {
    drupal_set_message(wfm_template_text_get_text('wfm_template_text_newsletter_preferences_error_message'), 'error');
  }
}

/**
 * Form constuctor function. Used on /newsletters/preferences/request-link.
 * @param $form
 * @param $form_state
 * @return array
 */
function newsletter_preferences_request_link_unauthenticated_form($form, &$form_state) {
  $current_step = $form_state['build_info']['args'][0];

  $form['#attached']['js'] = array(
    drupal_get_path('module', 'newsletters') . '/newsletters-request-link.js',
  );

  $form['#submit'] = array('newsletter_preferences_request_link_unauthenticated_form_submit');

  $steps = array(
    0 => array(
      'description' => array(
        '#type' => 'markup',
        '#prefix' => '<p>',
        '#suffix' => '</p>',
        '#markup' => t('Do you have a Whole Foods Market account?'),
      ),
      'login' => array(
        '#type' => 'button',
        '#value' => t('Yes - Sign In Now'),
        '#attributes' => array(
          'id' => 'newsletters-preferences-login'
        ),
      ),
      'verify' => array(
        '#type' => 'submit',
        '#value' => t('No - Verify Via Email'),
      ),
    ),
    1 => array(
      'description' => array(
        '#type' => 'markup',
        '#prefix' => '<p>',
        '#suffix' => '</p>',
        '#markup' => t('Enter an email address and we\'ll send you a verification link right away.'),
      ),
      'email' => array(
        '#type' => 'textfield',
      ),
      'submit' => array(
        '#type' => 'submit',
        '#value' => t('Send Link'),
      ),
      '#suffix' => '<p id="newsletters-request-link-cancel">' . l(t('Cancel'), '<front>') . '</p>',
    ),
  );
  return array_merge($form, $steps[$current_step]);
}

/**
 * Form validate callback function.
 * @param $form
 * @param $form_state
 * @see newsletter_preferences_request_link_unauthenticated_form()
 */
function newsletter_preferences_request_link_unauthenticated_form_validate($form, &$form_state) {
  if (isset($form_state['values']['email'])) {
    if (!valid_email_address($form_state['values']['email'])) {
      form_set_error('email', t('Please enter a valid email address'));
    }
  }
}

/**
 * Form submit callback function.
 * @param $form
 * @param $form_state
 * @see newsletter_preferences_request_link_unauthenticated_form()
 */
function newsletter_preferences_request_link_unauthenticated_form_submit($form, &$form_state) {
  $form_state['redirect'] = 'newsletters/preferences/request-link/1';

  if ($form_state['values']['email']) {
    $auth_token = newsletters_preferences_get_harmony_auth_token();
    $success = newsletters_preferences_send_tokenized_email($form_state['values']['email'], $auth_token);

    if ($success) {
      $_SESSION['newsletter_preferences']['email_address'] = $form_state['values']['email'];
      $form_state['redirect'] = 'newsletters/preferences/request-link/confirmation';
    }
    else {
      drupal_set_message(wfm_template_text_get_text('wfm_template_text_newsletter_preferences_error_message'), 'error');
      $form_state['redirect'] = FALSE;
    }
  }
}

/**
 * Get a Harmony API auth token.
 * @return string Harmony auth token
 */
function newsletters_preferences_get_harmony_auth_token() {
  $cache_id = 'epsilon_harmony_api_token';

  $cache = cache_get($cache_id);

  if ($cache && ((int)$cache->expire > (int)$_SERVER['REQUEST_TIME'])) {
    $token = $cache->data;
  }

  if (!isset($token)) {
    global $conf;

    $headers = array(
      'Authorization' => 'Basic ' . base64_encode($conf['harmony']['client_id'] . ':' . $conf['harmony']['secret_key']),
      'Content-Type' => 'application/x-www-form-urlencoded',
    );

    $data = http_build_query(
      array(
        'scope' => $conf['harmony']['scope'],
        'grant_type' => 'password',
        'username' => $conf['harmony']['user_name'],
        'password' => $conf['harmony']['password'],
      )
    );

    $options = array(
      'headers' => $headers,
      'method' => 'POST',
      'data' => $data,
      'timeout' => '10'
    );

    $result = drupal_http_request($conf['harmony']['auth_url'], $options);

    if ($result->code != 200) {
      watchdog('newsletters', $result->error, NULL, WATCHDOG_ERROR);
      return FALSE;
    }
    if (!$result->data) {
      $message = 'There is no data in response object.';
      watchdog('newsletters', $message, NULL, WATCHDOG_ERROR);
      return FALSE;
    }

    $response = json_decode($result->data);

    if (!$response->access_token) {
      $message = 'There is no access token in response data.';
      watchdog('newsletters', $message, NULL, WATCHDOG_ERROR);
      return FALSE;
    }

    $token = $response->access_token;

    $message = 'Successful get Harmony access token: ' . $token;
    watchdog('newsletters', $message, NULL, WATCHDOG_INFO);
    cache_set($cache_id, $token, 'cache', time() + $response->expires_in);
  }
  return $token;
}

/**
 * Make request to Harmony API to send a RTM (real-time message), aka send an email containing tokenized link.
 * @param $email Email address.
 * @param $auth_token Harmony authorization token.
 * @return string Request status message.
 */
function newsletters_preferences_send_tokenized_email($email, $auth_token) {
  global $conf;
  $userApi = new WFMApiUser;
  $upm_profile_id = $userApi->getUpmProfileIdFromEmail($email);
  $tokenApi = new WfmNewsletterToken($upm_profile_id, $_SERVER['REQUEST_TIME']);
  $token = $tokenApi->insertTokenIntoDatabase();
  $message_template_id = '8932ac9c-cdd1-4933-be6e-9c663ed4c764';

  $endpoint_url = 'https://api.harmony.epsilon.com/v3/messages/' . $message_template_id . '/send';

  // All this casting as objects is necessary to get properly formatted JSON for the request.
  $body = (object) array(
    'recipients' => array(
      array (
        'attributes' => array(
          (object) array(
            'attributeName' => 'emailAddress',
            'attributeType' => 'String',
            'attributeValue' => $email,
          ),
          (object) array(
            'attributeName' => 'Token',
            'attributeType' => 'String',
            'attributeValue' => $token,
          ),
        ),
        'customerKey' => $upm_profile_id,
        'emailAddress' => $email,
      ),
    ),
  );

  $headers = array(
    'Authorization' => 'Bearer ' . $auth_token,
    'Content-Type' => 'application/json',
    'X-OUID' => $conf['harmony']['uid'],
  );

  $request_options = array(
    'headers' => $headers,
    'method' => 'PUT',
    'data' => json_encode($body),
    'timeout' => '10'
  );

  $result = drupal_http_request($endpoint_url, $request_options);

  if ($result->code != 200) {
    watchdog('newsletters', $result->error, NULL, WATCHDOG_ERROR);
    return FALSE;
  }
  if (!$result->data) {
    $message = 'There is no data in response object.';
    watchdog('newsletters', $message, NULL, WATCHDOG_ERROR);
    return FALSE;
  }

  $data = json_decode($result->data);

  if ($data->resultCode != 'OK') {
    watchdog('newsletters', $result->data, NULL, WATCHDOG_ERROR);
    return FALSE;
  }

  $message = 'Successful send tokenized link for user ' . $upm_profile_id;
  watchdog('newsletters', $message, NULL, WATCHDOG_INFO);
  return TRUE;
}
