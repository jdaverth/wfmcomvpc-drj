<?php
/**
 * @file
 * newsletters.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function newsletters_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'newsletter_form_heading';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Newsletter Form Heading';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Global: PHP */
  $handler->display->display_options['fields']['php']['id'] = 'php';
  $handler->display->display_options['fields']['php']['table'] = 'views';
  $handler->display->display_options['fields']['php']['field'] = 'php';
  $handler->display->display_options['fields']['php']['label'] = '';
  $handler->display->display_options['fields']['php']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['php']['use_php_setup'] = 0;
  $handler->display->display_options['fields']['php']['php_output'] = '<?php
$href = WFMVariables::get(WFMVariables::NEWSLETTER_UNSUBSCRIBE_LINK);
$href .= \'?_ri_=\' . WFMVariables::get(WFMVariables::NEWSLETTER_RI_PREF);
$href .= \'&_ei_=\' . WFMVariables::get(WFMVariables::NEWSLETTER_EI_PREF);
?>

<h3>EMAIL NEWSLETTER SUBSCRIPTIONS</h3>
<p>Sign up for our newsletters for a chance to win a $50 gift card! <a href="/50-gift-card-official-rules">See official rules.</a><br />
<a href="<?php echo $href; ?>">Already a subscriber? Click here to change your preferences or unsubscribe.</a> | <a href="/newsletter-privacy-policy">Read our Privacy Policy.</a></p>';
  $handler->display->display_options['fields']['php']['use_php_click_sortable'] = '0';
  $handler->display->display_options['fields']['php']['php_click_sortable'] = '';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $export['newsletter_form_heading'] = $view;

  $view = new view();
  $view->name = 'newsletters';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Newsletters';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Newsletters';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_newsletter_category',
      'rendered' => 0,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'field_newsletter_category' => 'field_newsletter_category',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_newsletter_category' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Category */
  $handler->display->display_options['fields']['field_newsletter_category']['id'] = 'field_newsletter_category';
  $handler->display->display_options['fields']['field_newsletter_category']['table'] = 'field_data_field_newsletter_category';
  $handler->display->display_options['fields']['field_newsletter_category']['field'] = 'field_newsletter_category';
  $handler->display->display_options['fields']['field_newsletter_category']['label'] = '';
  $handler->display->display_options['fields']['field_newsletter_category']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_newsletter_category']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_newsletter_image']['id'] = 'field_newsletter_image';
  $handler->display->display_options['fields']['field_newsletter_image']['table'] = 'field_data_field_newsletter_image';
  $handler->display->display_options['fields']['field_newsletter_image']['field'] = 'field_newsletter_image';
  $handler->display->display_options['fields']['field_newsletter_image']['label'] = '';
  $handler->display->display_options['fields']['field_newsletter_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_newsletter_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_newsletter_image']['type'] = 'styles_media_original';
  $handler->display->display_options['fields']['field_newsletter_image']['settings'] = array(
    'file_view_mode' => 'media_small',
  );
  /* Field: Content: Key */
  $handler->display->display_options['fields']['field_newsletter_key']['id'] = 'field_newsletter_key';
  $handler->display->display_options['fields']['field_newsletter_key']['table'] = 'field_data_field_newsletter_key';
  $handler->display->display_options['fields']['field_newsletter_key']['field'] = 'field_newsletter_key';
  $handler->display->display_options['fields']['field_newsletter_key']['label'] = '';
  $handler->display->display_options['fields']['field_newsletter_key']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Category (field_newsletter_category) */
  $handler->display->display_options['filters']['field_newsletter_category_tid']['id'] = 'field_newsletter_category_tid';
  $handler->display->display_options['filters']['field_newsletter_category_tid']['table'] = 'field_data_field_newsletter_category';
  $handler->display->display_options['filters']['field_newsletter_category_tid']['field'] = 'field_newsletter_category_tid';
  $handler->display->display_options['filters']['field_newsletter_category_tid']['value'] = array(
    166 => '166',
    167 => '167',
  );
  $handler->display->display_options['filters']['field_newsletter_category_tid']['group'] = 1;
  $handler->display->display_options['filters']['field_newsletter_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_newsletter_category_tid']['vocabulary'] = 'newsletter_category';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'newsletter' => 'newsletter',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  $export['newsletters'] = $view;

  return $export;
}
