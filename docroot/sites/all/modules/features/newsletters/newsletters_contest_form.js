/**
* @file
* Behavior for the newsletter contest webform.
*/
(function($) {
  /**
  * Function WFMnewsletterContestForm
  * Creates this method as a plugin on the jQuery object
  */
	$.fn.WFMnewsletterContestForm = function(options) {
    var WFMnews = {
      element: this,
      options: $.extend({
        emailSelector: '#edit-submitted-email-address',
        stateSelector: '#edit-submitted-wfm-state-selections',
        storeSelector: '#edit-submitted-wfm-store-selections'
			}, options),

      /**
      * Function init
      * Initializes plugin, gets default values ready, adds listeners
      */
			init: function() {
        var storeOptions = this.getStoreOptions();
        this.getUserEmail();
        this.setStateDefault();
        this.filterStoreSelect(storeOptions);
        this.setStoreDefault();
        this.listeners(storeOptions);
			},

      /**
      * Function FilterStoreSelect
      * Filters stores in the store select based on the choses state in the state select.
      *
      * @param object storeOptions
      *   Object of all stores containing structure necessary to generate store select options.
      */
      filterStoreSelect: function(storeOptions) {
        var state = $(this.options.stateSelector).val(),
            defaultopt = document.createElement('OPTION');
        $(this.options.storeSelector + ' option').each(function(){
          $(this).detach();
        });
        $(this.options.storeSelector + ' optgroup').each(function(){
          $(this).detach();
        });
        defaultopt.label = '- Select -';
        defaultopt.text = '- Select -';
        defaultopt.value = '';
        $(this.options.storeSelector).append(defaultopt);
        $.each(storeOptions, function(index, value) {
          var abbr = index.substring(0, 2);
          if(abbr === state){
            var optgroup = document.createElement('optgroup');
            optgroup.label = index.substring(2);
            $(WFMnews.options.storeSelector).append(optgroup);
            $.each(value, function(optindex, optvalue) {
              var opt = document.createElement('option');
              opt.label = optvalue.name;
              opt.text = optvalue.name;
              opt.value = optindex;
              opt.selected = optvalue.selected;
              $(WFMnews.options.storeSelector + ' optgroup').last().append(opt);
            });
          }
        });
      },

      /**
      * Function getStoreOptions
      * Reads options and optgroups from store select, creates an object representing the
      * structure of the optgroups and options.
      *
      * @return object storeOptions
      *   Object that represents the structure of <optgroups> and <options> from store select.
      */
      getStoreOptions: function() {
        var storeOptions = {};
        $(this.options.storeSelector).children('optgroup').each(function(){
          var label = $(this).attr('label');
          storeOptions[label] = {};
          $(this).children('option').each( function(){
            var value = $(this).val();
            storeOptions[label][value] = {};
            storeOptions[label][value]['name'] = $(this).text();
            storeOptions[label][value]['selected'] = false;
          });
        });
        return storeOptions;
      },

      /**
      * Function getUserEmail
      * Sets email value from user object
      */
      getUserEmail: function() {
        var email = Drupal.settings.WholeFoods.user.email;
        $(this.options.emailSelector).val(email);
      },

      /**
      * Function getUserStore
      * Shortcut function that reads and returns user's localstore.
      *
      * @return int
      *   Node id of user's selected store.
      */
      getUserStore: function() {
        return Drupal.settings.WholeFoods.localStore;
      },

      /**
      * Function listeners
      * Adds listeners necessary for functionality of newsletter contest form.
      *
      * @param object storeOptions
      *   Object representing structure of <optgroup>s and <option>s from store select.
      */
      listeners: function(storeOptions) {
        $(this.options.stateSelector).change(function(){
          WFMnews.filterStoreSelect(storeOptions);
        });
      },

      /**
      * Function setStateDefault
      * Sets default state in state select based on user's selected store.
      */
			setStateDefault: function() {
        var storenid = this.getUserStore();
        Drupal.WholeFoods.getStoreInfo(storenid, function(storeinfo){
          var storestate = '';
          storeinfo = Drupal.WholeFoods.removeStoreNidKey(storeinfo);
          storestate = storeinfo.location.stateabbr;
          $(this.options.stateSelector).find('option').each(function(){
            if( $(this).val() === storestate) {
              $(this).attr('selected', true);
            }
          });
        });
			},

      /**
      * Function setStoreDefault
      * Sets default store in store select based on user's selected store.
      */
      setStoreDefault: function() {
        var storenid = this.getUserStore();
        $(this.options.storeSelector).find('option').each(function(){
          if( $(this).val() === storenid) {
            $(this).attr('selected', true);
          }
        });
      }
		};

    //Kick it off.
		WFMnews.init();
	};
})(jQuery);

//Load the plugin onto the webform element.
jQuery(window).load(function($) {
	jQuery('.webform-client-form').WFMnewsletterContestForm({
	});
});
