<?php
/**
 * @file
 * product_recalls.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function product_recalls_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'product_recalls_list_block';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Product Recalls List Block';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_type'] = 'h2';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 0;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['title']['link_to_node'] = 0;
  /* Field: Content: Recall Date */
  $handler->display->display_options['fields']['field_recall_date']['id'] = 'field_recall_date';
  $handler->display->display_options['fields']['field_recall_date']['table'] = 'field_data_field_recall_date';
  $handler->display->display_options['fields']['field_recall_date']['field'] = 'field_recall_date';
  $handler->display->display_options['fields']['field_recall_date']['label'] = '';
  $handler->display->display_options['fields']['field_recall_date']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_recall_date']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_recall_date']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['element_type'] = 'p';
  $handler->display->display_options['fields']['field_recall_date']['element_class'] = 'recalldate';
  $handler->display->display_options['fields']['field_recall_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_recall_date']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_recall_date']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_recall_date']['settings'] = array(
    'format_type' => 'blog_date',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_repeat_rule' => 'show',
  );
  $handler->display->display_options['fields']['field_recall_date']['field_api_classes'] = 0;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['body']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['body']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['body']['alter']['external'] = 0;
  $handler->display->display_options['fields']['body']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['body']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['body']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['body']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['body']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['body']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['body']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['body']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['body']['alter']['html'] = 0;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['body']['hide_empty'] = 0;
  $handler->display->display_options['fields']['body']['empty_zero'] = 0;
  $handler->display->display_options['fields']['body']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['body']['field_api_classes'] = 0;
  /* Sort criterion: Content: Recall Date (field_recall_date) */
  $handler->display->display_options['sorts']['field_recall_date_value']['id'] = 'field_recall_date_value';
  $handler->display->display_options['sorts']['field_recall_date_value']['table'] = 'field_data_field_recall_date';
  $handler->display->display_options['sorts']['field_recall_date_value']['field'] = 'field_recall_date_value';
  $handler->display->display_options['sorts']['field_recall_date_value']['order'] = 'DESC';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['nid']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['nid']['not'] = 0;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'product_recall' => 'product_recall',
  );

  /* Display: Product Recalls: List View */
  $handler = $view->new_display('block', 'Product Recalls: List View', 'block');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Product Recalls';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = 0;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Recall Date */
  $handler->display->display_options['fields']['field_recall_date']['id'] = 'field_recall_date';
  $handler->display->display_options['fields']['field_recall_date']['table'] = 'field_data_field_recall_date';
  $handler->display->display_options['fields']['field_recall_date']['field'] = 'field_recall_date';
  $handler->display->display_options['fields']['field_recall_date']['label'] = '';
  $handler->display->display_options['fields']['field_recall_date']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_recall_date']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_recall_date']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['element_type'] = 'p';
  $handler->display->display_options['fields']['field_recall_date']['element_class'] = 'recalldate';
  $handler->display->display_options['fields']['field_recall_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_recall_date']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_recall_date']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_recall_date']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_recall_date']['settings'] = array(
    'format_type' => 'blog_date',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_repeat_rule' => 'show',
  );
  $handler->display->display_options['fields']['field_recall_date']['field_api_classes'] = 0;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_type'] = 'p';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['body']['alter']['text'] = '<div class="target">[body]</div>';
  $handler->display->display_options['fields']['body']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['body']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['body']['alter']['external'] = 0;
  $handler->display->display_options['fields']['body']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['body']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['body']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['body']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['body']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['body']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['body']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['body']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['body']['alter']['html'] = 0;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['body']['hide_empty'] = 0;
  $handler->display->display_options['fields']['body']['empty_zero'] = 0;
  $handler->display->display_options['fields']['body']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['body']['field_api_classes'] = 0;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  $handler->display->display_options['block_description'] = 'Product Recalls List View';

  /* Display: Product Recalls: Single View */
  $handler = $view->new_display('block', 'Product Recalls: Single View', 'block_1');
  $handler->display->display_options['block_description'] = 'Product Recalls: Single View';
  $export['product_recalls_list_block'] = $view;

  return $export;
}
