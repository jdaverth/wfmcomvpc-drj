<?php
/**
 * @file
 * products.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function products_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function products_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function products_image_default_styles() {
  $styles = array();

  // Exported image style: featured_prod_200x200.
  $styles['featured_prod_200x200'] = array(
    'effects' => array(
      1 => array(
        'name' => 'coloractions_convert',
        'data' => array(
          'format' => 'image/png',
          'quality' => 75,
        ),
        'weight' => 1,
      ),
      2 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 170,
          'height' => 170,
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
      3 => array(
        'name' => 'canvasactions_definecanvas',
        'data' => array(
          'RGB' => array(
            'HEX' => '',
          ),
          'under' => 1,
          'exact' => array(
            'width' => 200,
            'height' => 200,
            'xpos' => 'center',
            'ypos' => 'center',
          ),
          'relative' => array(
            'leftdiff' => '',
            'rightdiff' => '',
            'topdiff' => '',
            'bottomdiff' => '',
          ),
        ),
        'weight' => 3,
      ),
    ),
    'label' => 'featured_prod_200x200',
  );

  // Exported image style: prodline_hero_300x260.
  $styles['prodline_hero_300x260'] = array(
    'effects' => array(
      1 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 300,
          'height' => 260,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'prodline_hero_300x260',
  );

  // Exported image style: prodline_logo.
  $styles['prodline_logo'] = array(
    'effects' => array(
      6 => array(
        'name' => 'coloractions_convert',
        'data' => array(
          'format' => 'image/png',
          'quality' => 75,
        ),
        'weight' => 1,
      ),
      7 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => '',
          'height' => 125,
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
    ),
    'label' => 'prodline_logo',
  );

  // Exported image style: product_hero.
  $styles['product_hero'] = array(
    'effects' => array(
      53 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => '',
          'height' => 285,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'product_hero',
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function products_node_info() {
  $items = array(
    'product' => array(
      'name' => t('Product'),
      'base' => 'node_content',
      'description' => t('Products to be showcased on the site.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'product_certification' => array(
      'name' => t('Product Certification'),
      'base' => 'node_content',
      'description' => t('Certifications that can be applied to products. (e.g. Kosher, Organic, etc.).'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'product_line' => array(
      'name' => t('Product Line'),
      'base' => 'node_content',
      'description' => t('Whole Food product lines that Products may belong to. (e.g. 365 Everyday Value, Whole Foods Market Brand, etc.).'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_styles_default_styles().
 */
function products_styles_default_styles() {
  $styles = array();

  // Exported style: prodline_hero_300x260

  $styles['file']['styles']['prodline_hero_300x260'] = array(
    'label' => 'prodline_hero_300x260',
    'description' => '',
    'preset_info' => array(
      'image' => array(
        'prodline_hero_300x260' => array(
          'default preset' => 'original',
          'preset' => 'prodline_hero_300x260',
        ),
      ),
      'audio' => array(
        'prodline_hero_300x260' => array(
          'default preset' => 'original',
        ),
      ),
      'video' => array(
        'prodline_hero_300x260' => array(
          'default preset' => 'original',
        ),
      ),
      'default' => array(
        'prodline_hero_300x260' => array(
          'default preset' => 'original',
        ),
      ),
      'media_youtube' => array(
        'prodline_hero_300x260' => array(
          'default preset' => 'unlinked_thumbnail',
        ),
      ),
    ),
  );
  // Exported style: prodline_hero_300x260

  $styles['file']['styles']['prodline_hero_300x260'] = array(
    'label' => 'prodline_hero_300x260',
    'description' => '',
    'preset_info' => array(
      'image' => array(
        'prodline_hero_300x260' => array(
          'default preset' => 'original',
          'preset' => 'prodline_hero_300x260',
        ),
      ),
      'audio' => array(
        'prodline_hero_300x260' => array(
          'default preset' => 'original',
        ),
      ),
      'video' => array(
        'prodline_hero_300x260' => array(
          'default preset' => 'original',
        ),
      ),
      'default' => array(
        'prodline_hero_300x260' => array(
          'default preset' => 'original',
        ),
      ),
      'media_youtube' => array(
        'prodline_hero_300x260' => array(
          'default preset' => 'unlinked_thumbnail',
        ),
      ),
    ),
  );
  return $styles;
}

/**
 * Implements hook_styles_default_presets_alter().
 */
function products_styles_default_presets_alter(&$presets) {
  $styles = styles_default_styles();
  if ($styles['file']['styles']['prodline_hero_300x260']['storage'] == STYLES_STORAGE_DEFAULT) {
    $presets['file']['containers']['image']['styles']['prodline_hero_300x260'] = array(
      'default preset' => 'original',
      'preset' => 'prodline_hero_300x260',
    );

    $presets['file']['containers']['audio']['styles']['prodline_hero_300x260'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['video']['styles']['prodline_hero_300x260'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['default']['styles']['prodline_hero_300x260'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['media_youtube']['styles']['prodline_hero_300x260'] = array(
      'default preset' => 'unlinked_thumbnail',
    );

  }
  if ($styles['media']['styles']['prodline_hero_300x260']['storage'] == STYLES_STORAGE_DEFAULT) {
    $presets['media']['containers']['image']['styles']['prodline_hero_300x260'] = array(
      'default preset' => 'original',
      'preset' => 'prodline_hero_300x260',
    );

    $presets['media']['containers']['audio']['styles']['prodline_hero_300x260'] = array(
      'default preset' => 'original',
    );

    $presets['media']['containers']['video']['styles']['prodline_hero_300x260'] = array(
      'default preset' => 'original',
    );

    $presets['media']['containers']['default']['styles']['prodline_hero_300x260'] = array(
      'default preset' => 'original',
    );

    $presets['media']['containers']['media_youtube']['styles']['prodline_hero_300x260'] = array(
      'default preset' => 'unlinked_thumbnail',
    );

  }
}
