<?php
/**
 * @file
 * related_content_views.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function related_content_views_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function related_content_views_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function related_content_views_image_default_styles() {
  $styles = array();

  // Exported image style: sidebar_square_55x55.
  $styles['sidebar_square_55x55'] = array(
    'label' => 'sidebar_square_55x55',
    'effects' => array(
      3 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 55,
          'height' => 55,
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_styles_default_styles().
 */
function related_content_views_styles_default_styles() {
  $styles = array();

  // Exported style: square_55x55

  $styles['file']['styles']['square_55x55'] = array(
    'label' => 'square_55x55',
    'description' => '',
    'preset_info' => array(
      'image' => array(
        'square_55x55' => array(
          'default preset' => 'original',
          'preset' => 'sidebar_square_55x55',
        ),
      ),
      'audio' => array(
        'square_55x55' => array(
          'default preset' => 'original',
        ),
      ),
      'video' => array(
        'square_55x55' => array(
          'default preset' => 'original',
        ),
      ),
      'default' => array(
        'square_55x55' => array(
          'default preset' => 'original',
        ),
      ),
      'media_youtube' => array(
        'square_55x55' => array(
          'default preset' => 'unlinked_thumbnail',
        ),
      ),
    ),
  );
  // Exported style: square_55x55

  $styles['file']['styles']['square_55x55'] = array(
    'label' => 'square_55x55',
    'description' => '',
    'preset_info' => array(
      'image' => array(
        'square_55x55' => array(
          'default preset' => 'original',
          'preset' => 'sidebar_square_55x55',
        ),
      ),
      'audio' => array(
        'square_55x55' => array(
          'default preset' => 'original',
        ),
      ),
      'video' => array(
        'square_55x55' => array(
          'default preset' => 'original',
        ),
      ),
      'default' => array(
        'square_55x55' => array(
          'default preset' => 'original',
        ),
      ),
      'media_youtube' => array(
        'square_55x55' => array(
          'default preset' => 'unlinked_thumbnail',
        ),
      ),
    ),
  );
  return $styles;
}

/**
 * Implements hook_styles_default_presets_alter().
 */
function related_content_views_styles_default_presets_alter(&$presets) {
  $styles = styles_default_styles();
  if ($styles['file']['styles']['square_55x55']['storage'] == STYLES_STORAGE_DEFAULT) {
    $presets['file']['containers']['image']['styles']['square_55x55'] = array(
      'default preset' => 'original',
      'preset' => 'sidebar_square_55x55',
    );

    $presets['file']['containers']['audio']['styles']['square_55x55'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['video']['styles']['square_55x55'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['default']['styles']['square_55x55'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['media_youtube']['styles']['square_55x55'] = array(
      'default preset' => 'unlinked_thumbnail',
    );

  }
  if ($styles['media']['styles']['square_55x55']['storage'] == STYLES_STORAGE_DEFAULT) {
    $presets['media']['containers']['image']['styles']['square_55x55'] = array(
      'default preset' => 'original',
      'preset' => 'sidebar_square_55x55',
    );

    $presets['media']['containers']['audio']['styles']['square_55x55'] = array(
      'default preset' => 'original',
    );

    $presets['media']['containers']['video']['styles']['square_55x55'] = array(
      'default preset' => 'original',
    );

    $presets['media']['containers']['default']['styles']['square_55x55'] = array(
      'default preset' => 'original',
    );

    $presets['media']['containers']['media_youtube']['styles']['square_55x55'] = array(
      'default preset' => 'unlinked_thumbnail',
    );

  }
}
