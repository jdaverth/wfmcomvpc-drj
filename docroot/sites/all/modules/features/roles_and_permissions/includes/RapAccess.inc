<?php

/**
 * @file
 * RAP Class
 */


/**
 * Enclosure element, can be part of the result array.
 */
class RapAccess {

  protected $userTids;
  protected $nodeTids;
  protected $node;
  protected $user;


  /**
   * Constructor, requires node.
   *
   * @param $node
   *   A node object.
   * @param $account
   *   A user object.
   *
   * Try to figure out which page you are on or which user etc
   */
  public function __construct($node = NULL, $account = NULL) {
    $args = func_get_args();
    if (count($args)) {
      foreach ($args as $arg) {
        if (isset($arg->nid) && isset($arg->type)) {
          $this->set('node', $arg);
        }
        elseif (isset($arg->uid) && is_numeric($arg->uid) && isset($arg->name) && isset($arg->mail)) {
          $this->set('user', $arg);
        }
        elseif (is_string($arg)) {
          $field = field_info_field(RAP_FIELD);
          if (isset($field['bundles']['node']) && in_array($arg, $field['bundles']['node'])) {
            $this->set('node', $arg);
          }
        }
        elseif (is_array($arg) && count($arg)) {
          foreach ($arg as $key => $value) {
            if (is_string($key)) {
              $this->set($key, $value);
            }
          }
        }
      }
    }
  }


  /**
   * Get needed variable.
   *
   * @param string $name
   *   Variable name.
   * @param mixed $default
   *   Default value to return, if variable not set.
   *
   * @return mixed
   *   Existing variable or NULL
   */
  public function get($name, $default = NULL) {
    if (!isset($this->{$name})) {
      $this->set($name, $default);
    }
    return $this->{$name};
  }


  /**
   * Setter.
   */
  public function set($var, $value = NULL) {
    $this->{$var} = $value;
    $func_name = 'set' . ucfirst($var);
    if (method_exists($this, $func_name)) {
      call_user_func_array(array($this, $func_name), array($value));
    }
  }


  /**
   * Set user variable.
   */
  protected function setUser($account = NULL) {
    global $user;

    if (!isset($account)) {
      $account = $user;
    }
    $this->user = user_load($account->uid);
  }


  /**
   * Set user tids.
   *
   * Get the taxonomy TID's that are stored in the users rap field
   */
  protected function setUserTids() {
    $this->userTids = $this->get_all_tids_from_rap_field($this->get('user'));
  }


  /**
   * Set node tids.
   *
   * Get the taxonomy TID's that are stored in this nodes rap field
   */
  protected function setNodeTids() {
    $this->nodeTids = $this->get_all_tids_from_rap_field($this->get('node'), FALSE);
  }


  /**
   * Set node.
   */
  protected function setNode($node = NULL) {
    if (isset($node->nid) && !empty($node->nid) && !isset($node->original)) {
      $node = entity_load_unchanged('node', $node->nid);
      $this->node = $node;
    }
    elseif (isset($node->original)) {
      $this->node = $node->original;
    }
  }


  /**
   * @return boolean
   *   TRUE if user have access to current operation and FALSE otherwise.
   */
  public function access($op) {
    // Setting result to nothing from this function would have the same effect.
    $result = NODE_ACCESS_IGNORE;
    if (1 !== $this->get('user')->uid) {
      switch ($op) {
        case 'delete':
        case 'update':
        case 'save':
          if (!$this->grant_access($op)) {
            if (count($this->get('userTids')) && (!count($this->get('nodeTids'))
                || count(array_intersect($this->get('userTids'), $this->get('nodeTids'))))) {
              $result = NODE_ACCESS_IGNORE;
            }
            else {
              $result = NODE_ACCESS_DENY;
            }
          }
          else {
            $result = NODE_ACCESS_IGNORE;
          }
          break;

        default:
          $result = NODE_ACCESS_IGNORE;
          break;
      }
    }
    return $result;
  }


  /**
   * Check if user role is limited in access to provide this operation for node.
   *
   * @return boolean
   *   TRUE if account have a limitations for current node type, FALSE otherwise.
   */
  protected function grant_access($op) {
    // User #1 has all privileges:
    if ($this->get('user')->uid == 1) {
      return TRUE;
    }
    $type = is_string($this->get('node')) ? $this->get('node') : $this->get('node')->type;
    if (in_array($type, node_permissions_get_configured_types())) {
      if ('save' == $op) {
        if (user_access("edit any $type content in bypass " . RAP_VOCABULARY, $this->get('user'))
            || (user_access("edit own $type content in bypass " . RAP_VOCABULARY, $this->get('user'))
                && ($this->get('user')->uid == $node->uid))) {
          return TRUE;
        }
      }
      if ('delete' == $op) {
        if (user_access("delete any $type content in bypass " . RAP_VOCABULARY, $this->get('user'))
            || (user_access("delete own $type content in bypass " . RAP_VOCABULARY, $this->get('user'))
                && ($this->get('user')->uid == $node->uid))) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }


  /**
   * Get all available term tids for current $entity.
   *
   * @param object $entity
   *   User or node object with binded field_rap_location field.
   * @param boolean $children
   *   attach to result children terms or not. TRUE by default.
   *
   * @return array
   *   An array of available term ids for current entity.
   */
  protected function get_all_tids_from_rap_field($entity, $children = TRUE) {
    $vocabulary = taxonomy_vocabulary_machine_name_load(RAP_VOCABULARY);
    if (isset($entity->{RAP_FIELD_USER})) {
      $field = RAP_FIELD_USER;
    }
    elseif (isset($entity->{RAP_FIELD})) {
      $field = RAP_FIELD;
    }
    else {
      return array();
    }
    $id = md5(serialize($entity->{$field}));
    $tree_list = &drupal_static(__FUNCTION__, array());
    if (!isset($tree_list[$id])) {
      $tree = array();
      if (!user_access('bypass locations acl while creating content') && isset($entity->{$field}[LANGUAGE_NONE])) {
        foreach ($entity->{$field}[LANGUAGE_NONE] as $term_array) {
          $tree[$term_array['tid']] = $term_array['tid'];
          if ($children) {
            $tree += $this->get_allowed_tids($term_array['tid'], $vocabulary->vid);
          }
        }
      }
      else {
        $tree +=$this->get_allowed_tids(0, $vocabulary->vid);
      }
      $tree_list[$id] = $tree;
    }
    return $tree_list[$id];
  }


  public function get_allowed_tids($parent, $vid = NULL) {
    if (!$vid) {
      $vocabulary = taxonomy_vocabulary_machine_name_load(RAP_VOCABULARY);
      $vid = $vocabulary ? $vocabulary->vid : 0;
    }
    $result = array();
    foreach (taxonomy_get_tree($vid, $parent) as $term) {
      $result[$term->tid] = $term->tid;
    }
    return $result;
  }

}
