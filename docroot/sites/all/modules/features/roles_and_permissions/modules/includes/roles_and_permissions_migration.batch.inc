<?php

/**
 * @file
 *   Roles And Permissions Migration batch functions for migration.
 */

/**
 * Batch callback to update nodes.
 *
 * @param array $nids
 *   An indexed array of node ids.
 * @param int $tid
 *   Term ID
 */
function roles_and_permissions_migration_batch_process($nids, $tid, &$context = array()) {
  if (!isset($context['results']['max_count'])) {
    $context['results']['max_count'] = $_SESSION['roles_and_permissions_counter'];
    $context['results']['current_count'] = 0;
    unset($_SESSION['roles_and_permissions_counter']);
  }
  $context['results']['current_count'] += count($nids);

  $nodes = node_load_multiple($nids);
  foreach ($nodes as $node) {
    switch ($node->type) {
      case 'store':
      case 'national_offices':
        // simple workaround for national offices without field_store_id filled
        if ($node->type == 'national_offices' && !isset($node->field_store_id[LANGUAGE_NONE][0]['value'])) {
          $national_offices_migration = array('129556' => 'US', '94296' => 'CA', '93096' => 'UK');
          if (isset($national_offices_migration[$node->nid])) {
            $node->field_store_id[LANGUAGE_NONE][0]['value'] = $national_offices_migration[$node->nid];
          }
        }
        if (isset($node->field_store_id[LANGUAGE_NONE][0]['value'])) {
          $terms = taxonomy_get_term_by_name($node->field_store_id[LANGUAGE_NONE][0]['value'], RAP_VOCABULARY);
          if (count($terms)) {
            $term = array_pop($terms);
            $term_id = array(array('tid' => $term->tid));
            if (!isset($node->{RAP_FIELD}[LANGUAGE_NONE]) || $term_id != $node->{RAP_FIELD}[LANGUAGE_NONE]) {
              roles_and_permissions_migration_node_save($node, $term_id, $context);
            }
          }
          else {
            $vocabulary = taxonomy_vocabulary_machine_name_load(RAP_VOCABULARY);
            $term = new stdClass();
            $term->name = $node->field_store_id[LANGUAGE_NONE][0]['value'];
            $term->vid = $vocabulary->vid;
            $term->description = $node->title;
            $term->parent = $tid;
            $term->field_bu_type = array(
              LANGUAGE_NONE => array(
                array(
                  'value' => 'store' == $node->type ? 'Whole Foods Market' : 'National Office'
                )
              )
            );
            taxonomy_term_save($term);
            roles_and_permissions_migration_node_save($node, $term->tid, $context);
          }
        }
        else {
          roles_and_permissions_migration_default_node_type_process($node, $tid, $context);
        }
        break;

      case 'region':
        if (isset($node->field_region_code[LANGUAGE_NONE][0]['value'])) {
          $terms = taxonomy_get_term_by_name($node->field_region_code[LANGUAGE_NONE][0]['value'], RAP_VOCABULARY);
          if (count($terms)) {
            $term = array_pop($terms);
            $term_id = array(array('tid' => $term->tid));
            if (!isset($node->{RAP_FIELD}[LANGUAGE_NONE]) || $term_id != $node->{RAP_FIELD}[LANGUAGE_NONE]) {
              roles_and_permissions_migration_node_save($node, $term_id, $context);
            }
          }
        }
        else {
          roles_and_permissions_migration_default_node_type_process($node, $tid, $context);
        }
        break;

      case 'blog':
      case 'promo':
      case 'special_diet_shopping_list':
        roles_and_permissions_migration_node_reference_node_type_process($node, $tid, 'field_store', $context);
        break;

      case 'local_vendor':
        roles_and_permissions_migration_node_reference_node_type_process($node, $tid, 'field_region', $context);
        break;

      case 'event':
        roles_and_permissions_migration_node_reference_node_type_process($node, $tid, 'field_event_store', $context);
        break;

      case 'service':
        $store_tid = roles_and_permissions_get_tid_from_service_node($node);
        $tid = (isset($store_tid) && !empty($store_tid)) ? $store_tid : $tid;
        roles_and_permissions_migration_default_node_type_process($node, $tid, $context);
        break;

      case 'department_article':
        if (isset($node->field_da_store[LANGUAGE_NONE])) {
          $tids = roles_and_permissions_migration_get_store_tid_from_store_nid($node->field_da_store[LANGUAGE_NONE], $tid);
          roles_and_permissions_migration_node_save($node, $tids, $context);
        }
        elseif (isset($node->field_store[LANGUAGE_NONE])) {
          $tids = roles_and_permissions_migration_get_store_tid_from_store_nid($node->field_store[LANGUAGE_NONE], $tid);
          roles_and_permissions_migration_node_save($node, $tids, $context);
        }
        else {
          roles_and_permissions_migration_default_node_type_process($node, $tid, $context);
        }
        break;

      default:
        roles_and_permissions_migration_default_node_type_process($node, $tid, $context);
        break;
    }
  }
  $context['message'] = t('Processed @current out of @total nodes.', array(
    '@current' => $context['results']['current_count'],
    '@total' => $context['results']['max_count'],
  ));
}


/**
 * Batch 'finished' callback.
 */
function roles_and_permissions_migration_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = t('Migration finished. @count nodes updated.', array('@count' => count($results['saved'])));
    drupal_set_message($message);
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
    drupal_set_message($message, 'error');
  }
}
