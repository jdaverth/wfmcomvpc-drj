<?php

/**
 * @file
 *   Roles And Permissions Migration module drush integration.
 */


/**
 * Implements hook_drush_command().
 *
 * @See drush_parse_command() for a list of recognized keys.
 *
 * @return
 *   An associative array describing your command(s).
 */
function roles_and_permissions_migration_drush_command() {
  $items = array();

  $items['rap-migrate-content'] = array(
    'description' => "Migrate all content into Locations ACL Taxonomy Term.",
    'arguments' => array(
      'exclude' => 'A comma-separated list of content type '
      . 'machine-readable names to exclude from being migrated.',
    ),
    'options' => array(
      'force' => "Force migrate (starting migration from the begining).",
    ),
    'drupal dependencies' => array('roles_and_permissions_migration'),
    'aliases' => array('rap-migrate', 'rap-mc', 'rapm', 'rapmc'),
  );
  $items['rap-migrate-users'] = array(
    'description' => "Migrate all users into Locations ACL Taxonomy Term.",
    'arguments' => array(
      'roles' => 'A comma-separated list of user roles to being migrated.',
    ),
    'drupal dependencies' => array('roles_and_permissions_migration'),
    'aliases' => array('rap-migrate-user', 'rap-mu', 'rapmu'),
  );
  $items['rap-migrate-terms'] = array(
    'description' => "Migrate all terms into Locations vocabulary.",
    'arguments' => array(
      'url' => 'An URL to external xml file with store terms.',
    ),
    'drupal dependencies' => array('roles_and_permissions_migration'),
    'aliases' => array('rap-migrate-term', 'rap-mt', 'rapmt'),
  );

  return $items;
}


/**
 * Implements hook_drush_help().
 */
function roles_and_permissions_migration_drush_help($section) {
  switch ($section) {
    case 'drush:rap-migrate-content':
      return dt("Migrate all content into Locations ACL Taxonomy Term.");

    case 'drush:rap-migrate-users':
      return dt("Migrate all users into Locations ACL Taxonomy Term.");

    case 'drush:rap-migrate-terms':
      return dt("Migrate all stores into Locations vocabulary.");
  }
}


/**
 * Migrate all content into Locations ACL Taxonomy Term.
 *
 * @param ...
 *   (Optional) A list of machine readable names of content types to exclude
 *   from being migrated.
 */
function drush_roles_and_permissions_migration_rap_migrate_content() {
  $required_types = roles_and_permissions_migration_required_types();
  $excluded_types = array_merge($required_types, drupal_map_assoc(drupal_explode_tags(implode(' ', func_get_args()))),
      roles_and_permissions_migration_excluded_types());
  // Determine if migration should be started from the begining.
  if (drush_get_option('force')) {
    variable_set('drush_rap_migration_start_from', 0);
  }
  // Get all content types wich have our field.
  $field = field_info_field(RAP_FIELD);
  if (isset($field['bundles']['node'])) {
    foreach (node_permissions_get_configured_types() as $type) {
      if (in_array($type, $field['bundles']['node'])) {
        $info = node_type_get_type($type);
        if (isset($excluded_types[$type])) {
          $excluded_types[$type] = $info->name;
        }
      }
    }
  }

  drush_print(dt('Only the following content types will be excluded from migrate: !types', array(
    '!types' => implode(', ', array_diff_key($excluded_types, $required_types)),
  )));
  if (drush_confirm(dt('Do you really want to continue?'))) {
    module_load_include('inc', 'roles_and_permissions_migration', 'includes/roles_and_permissions_migration.drush');
    module_load_include('inc', 'roles_and_permissions_migration', 'includes/roles_and_permissions_migration.batch');
    $results = _drush_roles_and_permissions_migration_migrate($required_types, $excluded_types);
    $message = 'Migration finished. @count nodes updated.';
    variable_del('drush_rap_migration_start_from');
    drush_log(dt($message, array('@count' => count($results['results']['saved']))), 'ok');
  }
  else {
    drush_die('Aborting.');
  }
}


/**
 * Migrate all users into Locations ACL Taxonomy Term.
 *
 * @param ...
 *   (Optional) A list of user roles to filter migrated users.
 *   from being migrated.
 */
function drush_roles_and_permissions_migration_rap_migrate_users() {
  $selected_roles = drupal_map_assoc(drupal_explode_tags(implode(' ', func_get_args())));
  if (count($selected_roles)) {
    $roles = array_intersect_key(array_flip(user_roles(TRUE)), $selected_roles);
    drush_print(dt('Only the following user roles will be included in migration process: !roles', array(
      '!roles' => implode(', ', $selected_roles),
    )));
  }
  if (drush_confirm(dt('Do you really want to continue?'))) {
    $result = roles_and_permissions_migration_users_process(isset($roles) ? $roles : array());
    drush_log(dt($result['message']), $result['status'] == 'status' ? 'ok' : $result['status']);
  }
  else {
    drush_die('Aborting.');
  }
}


/**
 * Migrate all Store terms into Locations Vocabulary.
 *
 * @param ...
 *   (Optional) A non default URL for import.
 */
function drush_roles_and_permissions_migration_rap_migrate_terms($url = NULL) {
  if (drush_confirm(dt('Do you really want to continue?'))) {
    if (!isset($url)) {
      $token = roles_and_permission_get_token('ATlB6Ny8dalhyvs4lHAGXOWOWOt2N2Jf', 'uEGzC616cyaOkiAD');
      $url = 'https://api.wholefoodsmarket.com/v2/stores?access_token=' . $token . '&limit=1000';
    }
    $source = feeds_source('locations_acl_taxonomy_xml');
    $source->addConfig(array(
      'RAPFeedsHTTPSFetcher' => array(
        'source' => $url,
      ),
    ));
    $source->save();
    $source->import();
    drush_log('Migration completed.');
  }
  else {
    drush_die('Aborting.');
  }
}


/**
 * Helper function to provide drush content migration.
 */
function _drush_roles_and_permissions_migration_migrate($required_types, $excluded_types) {
  $terms = taxonomy_get_term_by_name('WFM', RAP_VOCABULARY);
  if (count($terms)) {
    $term = array_pop($terms);
    $tid = $term->tid;
  }
  else {
    $tid = 0;
  }
  $context = array();
  $context['results']['current_count'] = 0;
  $context['results']['max_count'] = 0;
  $context['results']['saved'] = array();

  // Map Stores, National offices, Metros and Regions first.
  foreach (roles_and_permissions_migration_get_all_nids(array_keys($required_types)) as $chunk) {
    $chunks[] = $chunk;
    $context['results']['max_count'] += count($chunk);
  }
  // Map other content types except $excluded_types and not selected in form.
  foreach (roles_and_permissions_migration_get_all_nids(array(), array_keys($excluded_types),
      variable_get('drush_rap_migration_start_from', 0)) as $chunk) {
    $chunks[] = $chunk;
    $context['results']['max_count'] += count($chunk);
  }
  foreach ($chunks as $chunk) {
    $current_count = count($context['results']['saved']);
    roles_and_permissions_migration_batch_process($chunk, $tid, $context);
    variable_set('drush_rap_migration_start_from', array_pop($chunk));
    $saved_nodes_count = count($context['results']['saved']) - $current_count;
    $current_count_text = format_plural($context['results']['current_count'], '1 node', '@count nodes');
    $message = dt('@current of @total processed.', array(
      '@current' => $current_count_text,
      '@total' => $context['results']['max_count'],
    ));
    if ($saved_nodes_count) {
      $message .= ' ' . format_plural($saved_nodes_count, '1 node successfully updated.',
          '@count nodes successfully updated.');
    }
    drush_print($message);
  }

  // copy data from locations_acl to field_locations for event and promo content types
  $query = db_select('field_data_' . RAP_FIELD, 't')
    ->fields('t')
    ->condition('t.bundle', array('promo', 'event', 'department_article'), 'IN');
  db_insert('field_data_field_locations')
    ->from($query)
    ->execute();
  db_insert('field_revision_field_locations')
    ->from($query)
    ->execute();
  drush_print('Event and Promo CT: locations_acl values copied to field_locations successfully.');

  return $context;
}
