<?php

/**
 * @file
 * Update hooks - enable modules, etc.
 */


/**
 * Implements hook_disable().
 */
function roles_and_permissions_migration_disable() {
  if (db_table_exists('field_data_' . RAP_FIELD) && db_table_exists('field_data_' . RAP_FIELD_USER)) {
    $field = db_select('field_data_' . RAP_FIELD, 'f')
        ->fields('f')
        ->condition('f.entity_type', 'node')
        ->execute();
    $field_user = db_select('field_data_' . RAP_FIELD_USER, 'f')
        ->fields('f')
        ->condition('f.entity_type', 'user')
        ->execute();

    // Delete unused user roles if migration completed.
    if ($field->rowCount() && $field_user->rowCount()) {
      $role_names = array(
        'Regional Content Admin',
        'Metro Content Admin',
        'National Office Admin',
      );
      $roles = array();
      foreach ($role_names as $role_name) {
        $role = user_role_load_by_name($role_name);
        $roles[$role->rid] = $role->name;
      }
      $local_role = user_role_load_by_name('Local Content Admin');
      $query = db_select('users_roles', 'ur')->distinct();
      $query->addField('ur', 'uid');
      $query->condition('rid', array_keys($roles), 'IN');
      $query->orderBy('uid');
      $uids = $query->execute()->fetchCol();
      if (count($uids)) {
        db_delete('users_roles')
            ->condition('uid', $uids, 'IN')
            ->condition('rid', $local_role->rid)
            ->execute();
        $insert = db_insert('users_roles');
        foreach($uids as $uid) {
          $insert->fields(array(
            'uid' => $uid,
            'rid' => $local_role->rid,
          ));
        }
        $insert->execute();
      }
      foreach($roles as $role) {
        user_role_delete($role);
      }

      $del_field_ids = array(129, 140);
      foreach (array('data', 'revision') as $type) {
        foreach ($del_field_ids as $fid) {
          if (db_table_exists('field_deleted_' . $type . '_' . $fid)) {
            db_drop_table('field_deleted_' . $type . '_' . $fid);
          }
        }
      }
      // Delete unused fields.
      $fields = array(
        'field_da_store',
        'field_metro',
//        'field_region', // Commented out because of store.module: store_get_region_by_nid function.
        'field_region_code',
        'field_region_maintenance',
        'field_store_id',
        'field_store_maintenance',
//        'field_event_store',
//        'field_store', // Used in 'drupal' user profile and used for getting store for user.
//        'field_flyer_store',
      );
      foreach ($fields as $field_name) {
        try {
          field_delete_field($field_name);
        }
        catch (Exception $exc) {
        }
        $fids = db_select('field_config', 'f')
            ->fields('f', array('id'))
            ->condition('field_name', $field_name)
            ->execute()
            ->fetchCol();
        if (count($fids)) {
          db_delete('field_config_instance')
              ->condition('field_id', $fids, 'IN')
              ->execute();
          db_delete('field_config')
              ->condition('id', $fids, 'IN')
              ->execute();

          foreach ($fids as $fid) {
            if (db_table_exists('field_deleted_data_' . $fid)) {
              db_drop_table('field_deleted_data_' . $fid);
            }
            if (db_table_exists('field_deleted_revision_' . $fid)) {
              db_drop_table('field_deleted_revision_' . $fid);
            }
          }
        }
        if (db_table_exists('field_data_' . $field_name)) {
          db_drop_table('field_data_' . $field_name);
        }
        if (db_table_exists('field_revision_' . $field_name)) {
          db_drop_table('field_revision_' . $field_name);
        }
      }
    }
  }
}
