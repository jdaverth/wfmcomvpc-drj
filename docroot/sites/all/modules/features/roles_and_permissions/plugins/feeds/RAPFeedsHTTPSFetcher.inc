<?php

/**
 * @file
 * Home of the FeedsHTTPSFetcher and related classes.
 */


/**
 * Result of FeedsHTTPFetcher::fetch().
 */
class RAPFeedsHTTPSFetcherResult extends FeedsFetcherResult {
  protected $url;
  protected $accept_invalid_cert;
  protected $file_path;

  /**
   * Constructor.
   */
  public function __construct($url = NULL, $accept_invalid_cert = FALSE) {
    $this->url = $url;
    $this->accept_invalid_cert = $accept_invalid_cert;

    parent::__construct('');
  }

  /**
   * Overrides FeedsFetcherResult::getRaw();
   */
  public function getRaw() {
    feeds_include_library('http_request.inc', 'http_request');
    $result = http_request_get($this->url, NULL, NULL, $this->accept_invalid_cert);
    if (!in_array($result->code, array(200, 201, 202, 203, 204, 205, 206))) {
      throw new Exception(t('Download of @url failed with code !code.', array('@url' => $this->url, '!code' => $result->code)));
    }
    return $this->sanitizeRaw($result->data);
  }
}


/**
 * Fetches data via HTTP.
 */
class RAPFeedsHTTPSFetcher extends FeedsHTTPFetcher {

  /**
   * Implements FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    if ($this->config['use_pubsubhubbub'] && ($raw = $this->subscriber($source->feed_nid)->receive())) {
      return new FeedsFetcherResult($raw);
    }
    return new RAPFeedsHTTPSFetcherResult($source_config['source'], TRUE);
  }

  /**
   * Override parent::sourceFormValidate().
   */
  public function sourceFormValidate(&$values) {
    if (!feeds_valid_url($values['source'], TRUE)) {
      $form_key = 'feeds][' . get_class($this) . '][source';
      form_set_error($form_key, t('The URL %source is invalid.', array('%source' => $values['source'])));
    }
    elseif ($this->config['auto_detect_feeds']) {
      feeds_include_library('http_request.inc', 'http_request');
      if ($url = http_request_get_common_syndication($values['source'], array('accept_invalid_cert' => TRUE))) {
        $values['source'] = $url;
      }
    }
  }
}
