<?php
/**
 * @file
 * roles_and_permissions.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function roles_and_permissions_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_bu_type'.
  $field_bases['field_bu_type'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_bu_type',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'Whole Foods Market' => 'Whole Foods Market',
        'Culinary Center' => 'Culinary Center',
        'National Office' => 'National Office',
        'metro' => 'Metro',
        'region' => 'Region',
      ),
      'allowed_values_function' => '',
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'locations_acl_user'.
  $field_bases['locations_acl_user'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'locations_acl_user',
    'field_permissions' => array(
      'type' => 2,
    ),
    'indexes' => array(
      'tid' => array(
        0 => 'tid',
      ),
    ),
    'locked' => 1,
    'module' => 'taxonomy',
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => 'locations_acl',
          'parent' => 0,
        ),
      ),
      'required' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'taxonomy_term_reference',
  );

  return $field_bases;
}
