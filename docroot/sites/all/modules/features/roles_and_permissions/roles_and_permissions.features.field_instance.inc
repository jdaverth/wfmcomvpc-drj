<?php
/**
 * @file
 * roles_and_permissions.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function roles_and_permissions_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'taxonomy_term-locations_acl-field_bu_type'.
  $field_instances['taxonomy_term-locations_acl-field_bu_type'] = array(
    'bundle' => 'locations_acl',
    'default_value' => array(
      0 => array(
        'value' => 'Whole Foods Market',
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_bu_type',
    'label' => 'Business Unit',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'always_empty' => 0,
      ),
      'type' => 'options_select',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'user-user-locations_acl'.
  $field_instances['user-user-locations_acl'] = array(
    'bundle' => 'user',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'This is the store the user currently has selected as
      their "home" store, which allows them to see store specific content.
      This field can also be managed by having an authenticated user click
      "Find a store" at the top of the website.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 33,
      ),
    ),
    'entity_type' => 'user',
    'field_name' => 'locations_acl',
    'label' => 'User\'s Home Store',
    'required' => FALSE,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'taxonomy',
      'settings' => array(
        'autocomplete_path' => 'taxonomy/autocomplete',
        'size' => 60,
      ),
      'type' => 'taxonomy_autocomplete',
      'weight' => 59,
    ),
  );

  // Exported field_instance: 'user-user-locations_acl_user'.
  $field_instances['user-user-locations_acl_user'] = array(
    'bundle' => 'user',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Select the stores, metros, or regions that this user should have access to edit. Separate multiple locations by a comma. Ex: “Lamar (LMR)” or “Lamar (LMR), Gateway (GWY)”',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 33,
      ),
    ),
    'entity_type' => 'user',
    'field_name' => 'locations_acl_user',
    'label' => 'Permissions (Store name, TLC, metro name, region, or two-letter region code)',
    'required' => FALSE,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'taxonomy',
      'settings' => array(
        'autocomplete_path' => 'taxonomy/autocomplete',
        'size' => 60,
      ),
      'type' => 'taxonomy_autocomplete',
      'weight' => 58,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Business Unit');
  t('Permissions (Store name, TLC, metro name, region, or two-letter region code)');
  t('Select the stores, metros, or regions that this user should have access to edit. Separate multiple locations by a comma. Ex: “Lamar (LMR)” or “Lamar (LMR), Gateway (GWY)”');
  t('This is the store the user currently has selected as
      their "home" store, which allows them to see store specific content.
      This field can also be managed by having an authenticated user click
      "Find a store" at the top of the website.');
  t('User\'s Home Store');

  return $field_instances;
}
