<?php
/**
 * @file
 * roles_and_permissions.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function roles_and_permissions_taxonomy_default_vocabularies() {
  return array(
    'locations_acl' => array(
      'name' => 'Locations',
      'machine_name' => 'locations_acl',
      'description' => 'Locations ACL taxonomy for relating store (and business units) with data objects and users',
      'hierarchy' => 1,
      'module' => 'taxonomy',
      'weight' => 0,
      'metatags' => array(),
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
