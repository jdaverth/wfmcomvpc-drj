<?php
/**
 * @file
 * roles_and_permissions.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function roles_and_permissions_user_default_roles() {
  $roles = array();

  // Exported role: Blog Admin.
  $roles['Blog Admin'] = array(
    'name' => 'Blog Admin',
    'weight' => 3,
  );

  // Exported role: Blog Author.
  $roles['Blog Author'] = array(
    'name' => 'Blog Author',
    'weight' => 2,
  );

  // Exported role: Global Content Admin.
  $roles['Global Content Admin'] = array(
    'name' => 'Global Content Admin',
    'weight' => 8,
  );

  // Exported role: Local Content Admin.
  $roles['Local Content Admin'] = array(
    'name' => 'Local Content Admin',
    'weight' => 4,
  );

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 9,
  );

  return $roles;
}
