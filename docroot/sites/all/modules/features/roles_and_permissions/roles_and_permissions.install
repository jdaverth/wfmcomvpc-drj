<?php

/**
 * @file
 * Update hooks - enable modules, etc.
 */

/**
 * Implements hook_install().
 */
function roles_and_permissions_install() {
  // set schema as we need to run hook updates after install
  db_update('system')
    ->fields(array('schema_version' => 7000))
    ->condition('name', 'roles_and_permissions')
    ->execute();

  module_load_include('module', 'roles_and_permissions');

  $vocabulary = taxonomy_vocabulary_machine_name_load(RAP_VOCABULARY);
  if (!$vocabulary) {
    $vocabulary = new stdClass();
    $vocabulary->name = 'Locations';
    $vocabulary->machine_name = RAP_VOCABULARY;
    taxonomy_vocabulary_save($vocabulary);
  }
  // Create Global Content Admin role if not exist.
  $role = user_role_load_by_name('Global Content Admin');
  if (!$role) {
    $role = new stdClass;
    $role->name = 'Global Content Admin';
    $role->weight = 8;
    $role_id = user_role_save($role);
  }
  else {
    $role_id = $role->rid;
  }
  // Set permissions for this role to edit and delete Locations terms.
  $perms = array(
    'delete terms in ' . $vocabulary->vid,
    'edit terms in ' . $vocabulary->vid,
  );
  foreach ($perms as $perm) {
    $value = array(
      'rid' => $role_id,
      'permission' => $perm,
      'module' => 'taxonomy',
    );
    db_merge('role_permission')
        ->key($value)
        ->fields($value)
        ->execute();
  }
}


/**
 * Implements hook_enable().
 */
function roles_and_permissions_enable() {
  if (!function_exists('roles_and_permissions_create_fields')) {
    module_load_include('module', 'roles_and_permissions');
  }
  roles_and_permissions_create_fields();

  // Clear Feeds plugins cache for enabling HTTPS Fetcher plugin for Feeds.
  cache_clear_all('plugins:feeds:plugins', 'cache');

  features_revert(array('roles_and_permissions' => array('field', 'taxonomy')));

  roles_and_permissions_import_regions();
  roles_and_permissions_import_metro();
}


/**
 * Implements hook_disable().
 */
function roles_and_permissions_disable() {
  $instances = field_info_instances();
  foreach ($instances['node'] as $bundle => $instance) {
    if (isset($instance[RAP_FIELD])) {
      field_delete_instance($instance[RAP_FIELD], FALSE);
    }
  }
  if (isset($instances['user']['user'][RAP_FIELD_USER])) {
    field_delete_instance($instances['user']['user'][RAP_FIELD_USER], FALSE);
  }

  // Clear Feeds plugins cache for disabling HTTPS Fetcher plugin for Feeds.
  cache_clear_all('plugins:feeds:plugins', 'cache');
}


/**
 * Implements hook_uninstall().
 */
function roles_and_permissions_uninstall() {
  module_load_include('module', 'roles_and_permissions');

  foreach (array(RAP_FIELD, RAP_FIELD_USER) as $field_name) {

    // Delete all non-deleted instances.
    $field = field_info_field($field_name);
    if (isset($field['bundles'])) {
      foreach ($field['bundles'] as $entity_type => $bundles) {
        foreach ($bundles as $bundle) {
          $instance = field_info_instance($entity_type, $field_name, $bundle);
          field_delete_instance($instance);
        }
      }
    }

    // Mark field data for deletion.
    // skip this step if storage module is field_sql_storage and table doesn't exists
    if ('field_sql_storage' != $field['storage']['module'] || db_table_exists(_field_sql_storage_tablename($field))) {
      module_invoke($field['storage']['module'], 'field_storage_delete_field', $field);
    }

    // Mark the field for deletion.
    db_update('field_config')
        ->fields(array('deleted' => 1))
        ->condition('field_name', $field_name)
        ->execute();

    // Clear the cache.
    field_cache_clear(TRUE);

    module_invoke_all('field_delete_field', $field);

    $fids = db_select('field_config', 'f')
        ->fields('f', array('id'))
        ->condition('field_name', $field_name)
        ->execute()
        ->fetchCol();
    if (count($fids)) {
      db_delete('field_config_instance')
          ->condition('field_id', $fids, 'IN')
          ->execute();
      db_delete('field_config')
          ->condition('id', $fids, 'IN')
          ->execute();

      foreach ($fids as $fid) {
        if (db_table_exists('field_deleted_data_' . $fid)) {
          db_drop_table('field_deleted_data_' . $fid);
        }
        if (db_table_exists('field_deleted_revision_' . $fid)) {
          db_drop_table('field_deleted_revision_' . $fid);
        }
      }
    }
  }

  // Delete saved permissions.
  db_delete('role_permission')
      ->condition('module', 'roles_and_permissions')
      ->execute();
}


/**
 * Disable duplicate 'admin\/people' view provided by admin_views module.
 */
function roles_and_permissions_update_7001() {
  $views_status = variable_get('views_defaults', array());
  $views_status['admin_views_user'] = TRUE; // TRUE = disabled
  $views_status['admin_people'] = FALSE; // TRUE = disabled
  variable_set('views_defaults', $views_status);
  views_invalidate_cache();
}


/**
 * Enable default page manager term view for panels.
 */
function roles_and_permissions_update_7002() {
  // Enable page manager term view: 0 - Enabled.
  variable_set('page_manager_term_view_disabled', false);
}


/**
 * Set term WFM instead Global term as parent term for all others.
 */
function roles_and_permissions_update_7003() {
  if (module_exists('roles_and_permissions')) {
    $globalterms = taxonomy_get_term_by_name('Global', 'locations_acl');
    $global = reset($globalterms);
    $terms = taxonomy_get_term_by_name('WFM', 'locations_acl');
    $wfm = reset($terms);
    if ($wfm && $global) {
      $ready_nids = db_select('taxonomy_index', 'ti')
        ->fields('ti', array('nid'))
        ->condition('tid', $global->tid)
        ->execute()
        ->fetchCol();
      $acl_fields = array(
        'locations_acl',
        'locations_acl_user',
      );
      foreach ($acl_fields as $fieldname) {
        foreach (array('data', 'revision') as $type) {
          $query = db_update('field_' . $type . '_' . $fieldname)
            ->fields(array($fieldname . '_tid' => $global->tid))
            ->condition($fieldname . '_tid', $wfm->tid);
          if ('locations_acl' == $fieldname) {
            $query->condition('entity_id', $ready_nids, 'NOT IN');
          }
          $result = $query->execute();
        }
      }
      db_update('taxonomy_index')
        ->fields(array('tid' => $global->tid))
        ->condition('tid', $wfm->tid)
        ->condition('nid', $ready_nids, 'NOT IN')
        ->execute();
      $global->name = 'WFM';
      taxonomy_term_delete($wfm->tid);
      taxonomy_term_save($global);
    }
  }
}


/**
 * Reorder the structure of regions in Locations vocabulary.
 */
function roles_and_permissions_update_7004() {
  if (!function_exists('roles_and_permissions_import_regions')) {
    module_load_include('module', 'roles_and_permissions');
  }
  roles_and_permissions_import_regions();
}


/**
 * Update Blog Whole Story with new global location: WFM.
 */
function roles_and_permissions_update_7005() {
  if (module_exists('roles_and_permissions')) {
    $node = node_load(6860);
    $terms = array(
      'WFM' => 'Whole Foods Market',
    );
    $locations = array();
    $vocabularies = taxonomy_vocabulary_get_names();
    $conditions = array('vid' => $vocabularies['locations_acl']->vid);
    foreach ($terms as $term_name => $term_description) {
      $conditions['name'] = $term_name;
      $terms = taxonomy_term_load_multiple(array(), $conditions);
      $term = reset($terms);
      if (!$term) {
        $term = new stdClass();
        $term->name = $term_name;
        $term->description = $term_description;
        $term->vid = $vocabularies['locations_acl']->vid;
        $term->field_bu_type = array(
          LANGUAGE_NONE => array(
            array(
              'value' => 'National Office',
            ),
          ),
        );
        taxonomy_term_save($term);
      }
      $locations[] = array('tid' => $term->tid);
    }
    $node->locations_acl[LANGUAGE_NONE] = $locations;
    node_save($node);
  }
}

/**
 * Remap field_store for users to use locations_acl field instead of it.
 */
function roles_and_permissions_update_7006() {
  roles_and_permissions_create_fields(array('store'));
  $query = db_select('field_data_field_store', 's');
  $query->innerJoin('profile', 'p', 'p.pid = s.entity_id');
  $query->innerJoin('field_data_locations_acl', 'l', 'l.entity_id = s.field_store_nid');
  $query->addExpression("CONCAT('user')", 'entity_type');
  $query->addExpression("CONCAT('user')", 'bundle');
  $query->addField('s', 'deleted', 'deleted');
  $query->addField('p', 'uid', 'entity_id');
  $query->addField('p', 'uid', 'revision_id');
  $query->addExpression("CONCAT('und')", 'language');
  $query->addExpression("CONCAT('0')", 'delta');
  $query->addField('l', 'locations_acl_tid', 'locations_acl_tid');
  $query->condition('l.entity_type', 'node');
  $query->condition('s.entity_type', 'profile2');
  $query->condition('s.bundle', 'drupal');
  $query->condition('s.deleted', 0);
  $query->groupBy('p.uid');
  $query->orderBy('p.uid');
  db_insert('field_data_locations_acl')
    ->from($query)
    ->execute();
  db_insert('field_revision_locations_acl')
    ->from($query)
    ->execute();
}


/**
 * Delete unused fields: field_event_store.
 */
function roles_and_permissions_update_7007() {
  $fields = array(
    'field_event_store',
    'field_office_ishq',
    'field_metro_maintenance',
    'field_flyer_store', // Used in Sale Item.
    'field_store', // Used in 'drupal' user profile and used for getting store for user.
  );
  foreach ($fields as $field_name) {
    try {
      field_delete_field($field_name);
    }
    catch (Exception $exc) {
    }

    $fids = db_select('field_config', 'f')
      ->fields('f', array('id'))
      ->condition('field_name', $field_name)
      ->execute()
      ->fetchCol();
    if (count($fids)) {
      db_delete('field_config_instance')
        ->condition('field_id', $fids, 'IN')
        ->execute();
      db_delete('field_config')
        ->condition('id', $fids, 'IN')
        ->execute();

      foreach ($fids as $fid) {
        if (db_table_exists('field_deleted_data_' . $fid)) {
          db_drop_table('field_deleted_data_' . $fid);
        }
        if (db_table_exists('field_deleted_revision_' . $fid)) {
          db_drop_table('field_deleted_revision_' . $fid);
        }
      }
    }
    if (db_table_exists('field_data_' . $field_name)) {
      db_drop_table('field_data_' . $field_name);
    }
    if (db_table_exists('field_revision_' . $field_name)) {
      db_drop_table('field_revision_' . $field_name);
    }
  }
}


/**
 * Update taxonomy terms to include description in term name.
 */
function roles_and_permissions_update_7008() {
  $vocabulary = taxonomy_vocabulary_machine_name_load('locations_acl');
  $query = db_update('taxonomy_term_data')
    ->expression('name', "CONCAT(name, ' (', description, ')')")
    ->fields(array('description' => ''))
    ->isNotNull('description')
    ->condition('vid', $vocabulary->vid)
    ->execute();
}

/**
 *  Update draggableviews settings.
 */
function roles_and_permissions_update_7009() {
  // home page promos: update draggableviews settings with term ids instead of nodes
  $args_list = db_select('draggableviews_structure')
    ->fields('draggableviews_structure', array('args'))
    ->condition('view_name', 'promo_blocks')
    ->condition('view_display', 'page_2')
    ->groupBy('args')
    ->execute()
    ->fetchCol();

  foreach ($args_list as $args) {
    $arguments = json_decode($args);
    if ($arguments[0] == "14" && !empty($arguments[1])) {
      $tids = array();
      $nids = explode('+', $arguments[1]);
      $nodes = node_load_multiple($nids);
      foreach($nodes as $node) {
        if (isset($node->locations_acl[LANGUAGE_NONE][0]['tid'])) {
          array_unshift($tids, $node->locations_acl[LANGUAGE_NONE][0]['tid']);
        }
      }
      $new_args = json_encode(array($arguments[0], implode('+', $tids)));

      db_update('draggableviews_structure')
        ->fields(array('args' => $new_args))
        ->condition('view_name', 'promo_blocks')
        ->condition('view_display', 'page_2')
        ->condition('args', $args)
        ->execute();
    }
  }
}

/**
 * Removes field_promo_view().
 */
function roles_and_permissions_update_7010() {
  $instance = field_info_instance('node', 'field_promo_view', 'promo');
  field_delete_instance($instance, TRUE);
}

/**
 * Removes instance of field_add_share_button_to_marque and
 * fully removes field_service_media.
 */
function roles_and_permissions_update_7011() {
  $instance = field_info_instance('node', 'field_add_share_button_to_marque', 'service');
  field_delete_instance($instance, TRUE);
  field_delete_field('field_service_media');
}

/**
 * Fully removes field_department_media.
 */
function roles_and_permissions_update_7012() {
  field_delete_field('field_department_media');
}

/**
 * Fully removes field_page_media.
 */
function roles_and_permissions_update_7013() {
  field_delete_field('field_page_media');
}