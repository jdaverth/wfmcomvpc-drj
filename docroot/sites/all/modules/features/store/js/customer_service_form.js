(function WFMcustomerServiceForm($){
    'use strict';

    // references to elements on the page to avoid requesting the elements multiple times. defined in init
    var containers,
        inputs,
        steps;

    // functions that update text on elements of the page
    var helpText = {
        setOnlineOrdering: function (element) {
            var result = '<ul><li>';

            result += Drupal.t('All online catering and holiday meals are handled by your local store. Please call the store for the fastest response.');
            result += '<strong>';
            result += Drupal.t('Select your store below to see the phone number.');
            result += '</strong></li><li>';
            result += Drupal.t('For questions about gift cards call 1-844-WFM-CARD');
            result += '</li> <li>';
            result += Drupal.t('Instacart order questions are best handled by e-mailing <a href="mailto:happycustomers@instacart.com">happycustomers@instacart.com</a>');
            result += '</li></ul>';

            element.html(result);
        },
        setFastResponse: function (element, storeData) {
            var textElement = element.find('#phone-number-box'),
                phoneNumberElement = element.find('.phone-number');

            if (storeData) {
                textElement.text(Drupal.t('For the fastest response, call this store directly at'));
                phoneNumberElement.text(storeData.phone);
            } else {
                textElement.text(Drupal.t('Many issues can be solved by a quick call to your local store. Please select your store from the above menu to see phone number.'));
                phoneNumberElement.text('');
            }
        },
        setProductRequest: function (element) {
            element.text(Drupal.t('Individual stores are responsible for their own product sourcing. Select your store below to send your product request along to your local store team.'));
        },
        setCompanyFeedback: function (element) {
            element.text(Drupal.t('Although company feedback is handled by our corporate team, it helps us resolve your need faster if we know which store you visit most regularly.'))
        },
        setFeedbackConcern: function (element, data) {
            var textElement = element.find('#phone-number-box'),
                phoneNumberElement = element.find('.phone-number');

            if (data) {
                textElement.text(Drupal.t('If your feedback concerns this store, you can contact the store directly at'));
                phoneNumberElement.text(data.phone);
            } else {
                textElement.text(Drupal.t('Many issues can be solved by a quick call to your local store. Please select your store from the above menu to see phone number.'));
                phoneNumberElement.text('');
            }
        },
        setProductStandards: function (element) {
            element.text(Drupal.t('Product standards include animal welfare, unacceptable ingredients, organic certification, and other similar topics. Feedback about store-prepared products and holiday meals should be sent to your local store. Select Store Feedback in the menu above to contact them.'));
        },
        setWebsiteIssue: function (element) {
            element.text(Drupal.t('Please include a link to the page on our site about which you have a question or comment in the Message section. For questions about store sales and flyers please contact your local store. Select Store Feedback in the menu above to contact them.'));
        },
        setPotentialVendor: function (element) {
            element.html(Drupal.t('New vendors are evaluated at the regional level. Vendors of all types - including service providers - should follow the process detailed on our '
                + '<a href="' + location.protocol + '//' +location.hostname + '/company-info/information-potential-suppliers">Potential Supplier page</a>'
                +'. We can’t guarantee a response to all inquiries.'));
        }
    };

    var customerServiceForm = {
        //Properties
        tlc: 'input[name="submitted[tlc]"]',
        store_select: '#edit-submitted-step2-store-wrap-store-select',
        store365_select: '#edit-submitted-step2-store-wrap-365-store-select',
        store_options: {},

        /**
         * Add event listeners
         */
        addListeners: function() {
            $(inputs.topic).change(function topicChangeListenerCallback(){
                customerServiceForm.topicChangeCallback();
            });
            $(inputs.state).change(function stateChangeListenerCallback(){
                customerServiceForm.stateChangeCallback();
            });
            $(inputs.store).change(function storeChangeListenerCallback(){
                customerServiceForm.storeChangeCallback();
            });
            $(inputs.state365).change(function stateChangeListenerCallback(){
                customerServiceForm.stateChangeCallback();
            });
            $(inputs.store365).change(function storeChangeListenerCallback(){
                customerServiceForm.storeChangeCallback();
            });
            $(inputs.device).change(function deviceListenerCallback() {
                customerServiceForm.storeChangeCallback();
            });
            $(inputs.signinMethod).change(function signinMethodListenerCallback() {
                customerServiceForm.storeChangeCallback();
            });
            $(inputs.submit).click(function formSubmitClickListenerCallback() {
                customerServiceForm.submitClickCallback();
                return false;
            });
            $(inputs.zip).keyup(function storeZipListenerCallback() {
                customerServiceForm.storeChangeCallback();
            });
        },

        /**
         * Build option DOM element from object
         *
         * @param object option_object
         * @return object
         *   DOM option object
         */
        buildOptionElement: function(option_object) {
            var option = document.createElement('option');
            $(option).val(option_object.value).text(option_object.label).prop('selected', option_object.selected);
            return option;
        },

        /**
         * Create object representing store select object, save in customerServiceForm.store_options
         */
        createStoreOptionsObject: function(is365) {
            var store_select;
            var options = {};

            if (is365) {
                store_select = $(customerServiceForm.store365_select);
            } else {
                store_select = $(customerServiceForm.store_select);
            }

            store_select.first().children('optgroup').each(function(){
                var label = $(this).attr('label');
                options[label] = {};
                $(this).children('option').each( function(){
                    var value = $(this).val();
                    options[label][value] = {};
                    options[label][value]['name'] = $(this).text();
                    options[label][value]['selected'] = false;
                    if($(this).prop('selected') === true) {
                        options[label][value]['selected'] = true;
                    }
                });
            });

            if (is365) {
                customerServiceForm.store365_options = options;
            } else {
                customerServiceForm.store_options = options;
            }
        },

        /**
         * Disable Store Select
         */
        disableStoreSelect: function(is365) {
            if (is365) {
                inputs.store365.prop('disabled', true).addClass('disabled');
            } else {
                inputs.store.prop('disabled', true).addClass('disabled');
            }
        },

        /**
         * Enable Store Select
         */
        enableStoreSelect: function(is365) {
            if (is365) {
                inputs.store365.prop('disabled', false).removeClass('disabled');
            } else {
                inputs.store.prop('disabled', false).removeClass('disabled');
            }
        },

        /**
         * Filter store select options based on selected state.
         */
        filterStoreOptions: function(is365) {
            var store_options,
                store_select,
                state_abbreviation;
            var default_option = '<option value=0>' + Drupal.t('Select a Store') + '</option>';

            if (is365) {
                store_options = customerServiceForm.store365_options;
                store_select = customerServiceForm.store365_select;
                state_abbreviation = inputs.state365.val();
            } else {
                store_options = customerServiceForm.store_options;
                store_select = customerServiceForm.store_select;
                state_abbreviation = inputs.state.val();
            }

            $(store_select + ' option').detach();
            $(store_select + ' optgroup').detach();
            $(store_select).append(default_option);
            $.each(store_options, function(index, value) {
                var abbr = index.substring(0, 2),
                    optgroup = {};
                if (abbr == state_abbreviation) {
                    optgroup = document.createElement('optgroup');
                    optgroup.label = index.substring(2);
                    $(store_select).append(optgroup);
                    $.each(value, function(optindex, optvalue) {
                        var option_values = {
                                label: optvalue.name,
                                text: optvalue.name,
                                value: optindex,
                                selected: optvalue.selected
                            },
                            opt = customerServiceForm.buildOptionElement(option_values);
                        $(store_select).find('optgroup').last().append(opt);
                    });
                }
            });
        },

        /**
         * @param object element
         *   object representing option DOM element
         * @return object
         *   Object represented necessary values of select element
         */
        getOptionAsObject: function(element) {
            var optobj = {},
                value_string = $(element).val(),
                value_array = value_string.split('__');

            optobj.phone = $(element).attr('number');
            optobj.tlc = $(element).attr('tlc');
            optobj.stateabbr = value_array[0];
            optobj.city = value_array[1];
            optobj.nid = value_array[2];
            optobj.value = value_string;
            optobj.display = $(element).text();
            return optobj;
        },

        /**
         * Callback function for AJAX request to /ajax/stores
         *
         * @param store object
         *   Object of store info
         */
        setDefaultStoreCallback: function(store) {
            store = store[Object.keys(store)[0]];

            if (store) {
                inputs.state.val(store.location.stateabbr).change();
                $(customerServiceForm.store_select + ' option[value="' + store.nid + '"]').prop('selected', true).change();
            }
        },

        /**
         * Set the form into it's virgin state.
         * @param topic
         */
        clearForm: function() {
            // @TODO: Do we want to clear the input fields as well?
            for (var element in containers) {
                containers[element].hide();
            }

            containers.name.show();
            containers.email.show();
            containers.phone.show();
            containers.topic.show();
            containers.webform.show();
        },

        /**
         * Initialization function
         */
        init: function() {
            customerServiceForm.cacheElements();
            customerServiceForm.initComponents();
            customerServiceForm.resetUI();
            customerServiceForm.createStoreOptionsObject();
            customerServiceForm.createStoreOptionsObject(true);
            customerServiceForm.addListeners();
            customerServiceForm.disableStoreSelect();
            customerServiceForm.disableStoreSelect(true);
            customerServiceForm.setDefaultValues();
            customerServiceForm.setStep(1);
            customerServiceForm.prefillTopic();
            containers.phoneHelpText.hide();
        },

        /**
         * Override undesired Webform limitations and behaviors.
         */
        resetUI: function () {
            var zipCodeLabel = $('label[for="edit-submitted-step2-store-zip-code"]');

            // Add the required asterisk because it is required, but only when it's visible,
            // which webform doesn't support.
            zipCodeLabel.html(zipCodeLabel.text() + '<span class="form-required" title="This field is required.">*</span>');
        },
        
        /**
         * Prefill topic if it's in the url, or reset the topic value if there isn't any.
         */
        prefillTopic: function () {
            var topic,
                topicIdx;
            
            if (window.location.search) {
                topicIdx = window.location.search.indexOf('topic=');
            }
            
            if (topicIdx >= 0) {
                // prefill topic if topic is in the url
                topic = window.location.search.substr(topicIdx + 6);
                inputs.topic.val(topic);
            } else {
                // Reset topic dropdown because the value is occasionally retained, breaking the form.
                inputs.topic.val('');
            }
        },

        /**
         * Get references to all the elements we'll need for the page,
         * to avoid getting them multiple times.
         */
        cacheElements: function () {
            containers = {
                topic: $('#webform-component-step1--choose-topic'),
                state: $('.form-item-submitted-step2-store-wrap-state-select'),
                store: $('.form-item-submitted-step2-store-wrap-store-select'),
                state365: $('.form-item-submitted-step2-store-wrap-365-state-select'),
                store365: $('.form-item-submitted-step2-store-wrap-365-store-select'),
                phoneHelpText: $('#phone-number-box-wrapper'),
                body: $('#webform-component-step3--body'),
                name: $('#webform-component-name'),
                email: $('#webform-component-email'),
                phone: $('#webform-component-phone'),
                stepOneHelp: $('#step1-help'),
                stepTwoHelp: $('#step2-help'),
                stepThreeHelp: $('#step3-help'),
                upc: $('#webform-component-step3--product-upc'),
                webform: $('#webform-client-form-7477'),
                zip: $('#webform-component-step2--store-zip-code'),
                problemUrl: $('#webform-component-step2--trouble-url'),
                productCategory: $('#webform-component-step2--product-category'),
                device: $('#webform-component-step2--what-device'),
                signinMethod: $('#webform-component-step2--signin-method'),
                newsletterEmail: $('#webform-component-step2--user-email')
            };

            inputs = {
                topic: $('#edit-submitted-step1-choose-topic'),
                state: $('#edit-submitted-step2-store-wrap-state-select'),
                store: $('#edit-submitted-step2-store-wrap-store-select'),
                state365: $('#edit-submitted-step2-store-wrap-365-state-select'),
                store365: $('#edit-submitted-step2-store-wrap-365-store-select'),
                zip: $('#edit-submitted-step2-store-zip-code'),
                submit: $('#edit-submit'),
                device: $('input[name="submitted[step2][what_device]"]'),
                signinMethod: $('input[name="submitted[step2][signin_method]"]'),
                newsletterEmail: $('#edit-submitted-step2-user-email')
            };

            steps = [
                $('#edit-submitted-step1'),
                $('#edit-submitted-step2'),
                $('#edit-submitted-step3')
            ]
        },

        initComponents: function () {
            // the object is structured as follows:
            // each component is the set of steps following a topic selection.
            // each element in the array is a step.
            // each object in the step is a field or component that needs to be shown/updated
            // once the previous step is completed.
            // text is done through functions so that we can maintain Drupal template calls,
            // as well as account for data that gets dynamically added to the string (ex. store data)
            customerServiceForm.csComponents = {
                // topic
                store_experience: [
                    // step 2
                    {
                        // components to show after topic is selected
                        state: {
                            element: containers.state,
                            input: inputs.state,
                            required: true
                        },
                        store: {
                            element: containers.store,
                            input: inputs.store,
                            required: true
                        }
                    },
                    // step 3
                    {
                        // components to show after store is selected
                        phoneHelpText: {
                            element: containers.phoneHelpText,
                            setTextFn: helpText.setFastResponse
                        },
                        message: {
                            element: containers.body
                        }
                    }
                ],
                instacart: [
                    {
                        state: {
                            element: containers.state,
                            input: inputs.state,
                            required: true
                        },
                        store: {
                            element: containers.store,
                            input: inputs.store,
                            required: true
                        },
                        stepOneHelp: {
                            element: containers.stepOneHelp,
                            setTextFn: helpText.setOnlineOrdering
                        }
                    },
                    {
                        phoneHelpText: {
                            element: containers.phoneHelpText,
                            setTextFn: helpText.setFastResponse
                        },
                        message: {
                            element: containers.body
                        }
                    }
                ],
                product_request: [
                    {
                        state: {
                            element: containers.state,
                            input: inputs.state,
                            required: true
                        },
                        store: {
                            element: containers.store,
                            input: inputs.store,
                            required: true
                        },
                        stepTwoHelp: {
                            element: containers.stepTwoHelp,
                            setTextFn: helpText.setProductRequest
                        }
                    },
                    {
                        message: {
                            element: containers.body
                        }
                    }
                ],
                company_policy: [
                    {
                        state: {
                            element: containers.state,
                            input: inputs.state,
                            required: true
                        },
                        store: {
                            element: containers.store,
                            input: inputs.store,
                            required: true
                        },
                        stepTwoHelp: {
                            element: containers.stepTwoHelp,
                            setTextFn: helpText.setCompanyFeedback
                        }
                    },
                    {
                        phoneHelpText: {
                            element: containers.phoneHelpText,
                            setTextFn: helpText.setFeedbackConcern
                        },
                        message: {
                            element: containers.body
                        }
                    }
                ],
                '365_edv': [
                    {
                        state: {
                            element: containers.state,
                            input: inputs.state,
                            required: true
                        },
                        store: {
                            element: containers.store,
                            input: inputs.store,
                            required: true
                        }
                    },
                    {
                        upc: {
                            element: containers.upc
                        },
                        message: {
                            element: containers.body
                        }
                    }
                ],
                '365_stores': [
                    {
                        state: {
                            element: containers.state365,
                            input: inputs.state365,
                            required: true
                        },
                        store: {
                            element: containers.store365,
                            input: inputs.store365,
                            required: true
                        }
                    },
                    {
                        message: {
                            element: containers.body
                        }
                    }
                ],
                quality_standards: [
                    {
                        stepOneHelp: {
                            element: containers.stepOneHelp,
                            setTextFn: helpText.setProductStandards
                        },
                        productCategory: {
                            element: containers.productCategory
                        },
                        message: {
                            element: containers.body
                        }
                    }
                ],
                website_issues: [
                    {
                        stepOneHelp: {
                            element: containers.stepOneHelp,
                            setTextFn: helpText.setWebsiteIssue
                        },
                        problemUrl: {
                            element: containers.problemUrl
                        },
                        message: {
                            element: containers.body
                        }
                    }
                ],
                store_location_request: [
                    {
                        zip: {
                            element: containers.zip,
                            input: inputs.zip,
                            required: true
                        }
                    },
                    {
                        message: {
                            element: containers.body
                        }
                    }
                ],
                app_issues: [
                    {
                        device: {
                            element: containers.device,
                            input: inputs.device,
                            required: true
                        }
                    },
                    {
                        message: {
                            element: containers.body
                        }
                    }
                ],
                account_issues: [
                    {
                        signinMethod: {
                            element: containers.signinMethod,
                            input: inputs.signinMethod
                        }
                    },
                    {
                        message: {
                            element: containers.body
                        }
                    }
                ],
                newsletter: [
                    {
                        newsletterEmail: {
                            element: containers.newsletterEmail,
                            input: inputs.newsletterEmail
                        },
                        message: {
                            element: containers.body
                        }
                    }
                ]
            };
        },

        /**
         * Set default selected store. Email is handled server-side.
         */
        setDefaultValues: function() {
            var user_store = $.cookie('local_store');

            if (user_store) {
                Drupal.WholeFoods.getStoreInfo(user_store, customerServiceForm.setDefaultStoreCallback);
            }
        },

        /**
         * Set value on hidden input store tlc.
         */
        setHiddenStoreValue: function(storeData) {
            var tlcInput = $('input[name="submitted[tlc]"]');

            if (storeData) {
                tlcInput.val(storeData.tlc);
            } else {
                tlcInput.val('');
            }
        },

        /**
         * Callback function to state select change listener.
         */
        stateChangeCallback: function() {
            var selected_state;
            var is365 = customerServiceForm.topicNeeds365Stores();

            if (is365) {
                selected_state = inputs.state365.val();
            } else {
                selected_state = inputs.state.val();
            }

            customerServiceForm.disableStoreSelect(is365);

            if (selected_state != false) {
                customerServiceForm.enableStoreSelect(is365);
            }

            customerServiceForm.filterStoreOptions(is365);

            helpText.setFastResponse(containers.phoneHelpText);
        },

        /**
         * Callback function to store select change listener.
         * Keep in mind that if the user has already chosen a store elsewhere,
         * this is called as the page loads.
         */
        storeChangeCallback: function() {
            var components,
                nid;
            var topic = inputs.topic.val();

            if (customerServiceForm.topicNeeds365Stores()) {
                nid = inputs.store365.val();
            } else {
                nid = inputs.store.val();
            }

            if (topic) {
                components = customerServiceForm.csComponents[topic][1];
            }

            if (nid) {
                Drupal.WholeFoods.getStoreInfo(nid, function(storeData) {
                    storeData = Drupal.WholeFoods.removeStoreNidKey(storeData);
                    customerServiceForm.setHiddenStoreValue(storeData);
                    customerServiceForm.updateFormFromStore(components, topic, storeData);
                    if (topic) {
                        customerServiceForm.setStep(3);
                    }
                });
            }
            else {
                if (topic) {
                    customerServiceForm.setStep(3);
                }

                customerServiceForm.updateFormFromStore(components, topic);
                customerServiceForm.setHiddenStoreValue();
            }
        },
        topicNeeds365Stores: function () {
            var topic = inputs.topic.val();

            return topic === '365_stores';
        },
        /**
         *  Update the form based on the situation with the store.
         *  We want to update the rest of the form either way, but
         *  we don't want to show the rest of the form unless there's a
         *  topic selected.
         */
        updateFormFromStore: function (components, topic, data) {
            if (components) {
                if (topic) {
                    customerServiceForm.showFieldsSection(components, data);
                } else {
                    customerServiceForm.updateFieldsSection(components, data);
                }
            }
        },

        /**
         * Show/Hide/Update a section of the form.
         *
         * @param components [REQUIRED] [OBJECT] The array of components to act on (from csComponents)
         * @param data       [OPTIONAL] [OBJECT] Relevant data necessary for updating components (store data)
         * @param action     [OPTIONAL] [STRING] The action to take on it: show, hide, or update
         */
        handleFieldsSection: function (components, data, action) {
            var componentObj;

            //Show the components
            for (var component in components) {
                componentObj = components[component];

                if (componentObj.element) {
                    // update to the relevant text if available
                    if (componentObj.setTextFn) {
                        componentObj.setTextFn(componentObj.element, data);
                    }

                    if (action === 'show') {
                        // show the component
                        componentObj.element.show();
                    } else if (action === 'hide') {
                        // hide the component
                        componentObj.element.hide();
                    }
                }
            }
        },

        showFieldsSection: function (components, data) {
            customerServiceForm.handleFieldsSection(components, data, 'show');
        },

        hideFieldsSection: function (components, data) {
            customerServiceForm.handleFieldsSection(components, data, 'hide');
        },

        updateFieldsSection: function (components, data) {
            // fields will neither be shown nor hidden; the text will be updated
            customerServiceForm.handleFieldsSection(components, data, 'update');
        },

        /**
         * Set active step.
         */
        setStep: function(stepNumber) {
            $.each(steps, function(index, value) {
                if (index <= stepNumber - 1) {
                    value.addClass('is-active');
                }
                else {
                    value.removeClass('is-active');
                }
            });
        },

        /**
         * Callback function for customer service form submit.
         */
        submitClickCallback: function() {
            var component,
                components;
            var topic = inputs.topic.val(),
                stepTwoCompleted = true;

            components = customerServiceForm.csComponents[topic][0];

            customerServiceForm.showFieldsSection(components);

            // look for the input that the user needs to fill in in order
            // to complete the form, if there is one
            for (var prop in components) {
                component = components[prop];

                if (component.hasOwnProperty('input')) {
                    // get the input value
                    if (this.isUserInputValid(component)) {
                        component.input.removeClass('error');
                        customerServiceForm.storeChangeCallback();
                    } else {
                        component.input.addClass('error');
                        stepTwoCompleted = false;
                    }
                }
            }

            if (!stepTwoCompleted) {
                alert('Please fill out the fields in step 2.');
            } else {
                inputs.submit.prop('disabled', true);
                inputs.submit.addClass('disabled');
                inputs.submit.val('Sending…');
                containers.webform[0].submit();
            }
        },

        /**
         * Callback function to topic select change listener.
         */
        topicChangeCallback: function() {
            var components,
                component;
            var stepTwoCompleted = true,
                requiredInputExists = false,
                topic = inputs.topic.val();

            // hide everything in the form except the topic,
            // since we switched topics, and we only want to show
            // components relevant to the topic selected
            customerServiceForm.clearForm();

            if (topic) {
                // based on the topic, get all the components we want to display
                // as the next step
                components = customerServiceForm.csComponents[topic][0];

                customerServiceForm.showFieldsSection(components);

                // look for the input that the user needs to fill in in order
                // to show step 3, if there is one
                for (var prop in components) {
                    component = components[prop];

                    if (component.hasOwnProperty('input')) {
                        requiredInputExists = this.isUserInputRequired(component) || requiredInputExists;

                        // get the input value
                        if (this.isUserInputValid(component)) {
                            customerServiceForm.storeChangeCallback();
                            customerServiceForm.setStep(3);
                        } else {
                            stepTwoCompleted = false;
                        }
                    }
                }
                // if no input is required for step 2, go ahead and enable step 3
                if (!requiredInputExists) {
                    customerServiceForm.setStep(3);
                } else if (!stepTwoCompleted) {
                    // no input value found, so assume step 2 is not complete
                    customerServiceForm.setStep(2);
                }
            }
            else {
                customerServiceForm.setStep(1);
            }
        },
        isUserInputRequired: function (component) {
            return component.hasOwnProperty('required') && component.required;
        },
        /**
         * Given a component (an object defined in the initComponents function), return
         * the value of the input property, if there is one.
         *
         * @param component
         * @returns The value of the component's input.
         */
        isUserInputValid: function (component) {
            var checkedInput,
                inputValue;
            var isRequired = this.isUserInputRequired(component),
                isInput = component.hasOwnProperty('input');

            // does the input property exist?
            if (isInput) {
                // .val() doesn't work as expected for radio buttons or checkboxes.
                // need to check for :checked to find the proper value.
                if (component.input.is('input[type=radio]') || component.input.is('input[type=checkbox]')) {
                    checkedInput = component.input.filter(':checked');

                    if (checkedInput.length) {
                        inputValue = component.input.val();
                    }
                } else if (component.input.is('select')) {
                    // only get a value from the drop down if it's not on the default option
                    if (component.input.val() !== '0') {
                        inputValue = component.input.val();
                    }
                } else {
                    // other inputs are less particular and work as expected.
                    inputValue = component.input.val();
                }
            }

            // input is valid if it's required, and therefore needs an input value,
            // or the input isn't required and it doesn't matter.
            return ((inputValue && isRequired) || !isRequired) && isInput;
        }
    };

    //Attach to Drupal behaviors
    Drupal.behaviors.customerServiceForm = {
        attach: function() {
            $('body').once('customerServiceForm', function(){
                customerServiceForm.init();
                // sometimes the server will set the topic, so make sure the form is set to the proper state
                customerServiceForm.topicChangeCallback();
            });
        }
    };

})(jQuery);