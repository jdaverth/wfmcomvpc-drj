(function ($) {
    Drupal.behaviors.storePromoEditor = {
        attach: function(context, settings) {
            // hide "page" field if custom location selected
            var store_nid = $.cookie('local_store');
            $('#edit-field-promo-custom-location-und', context).on('change', function() {
                if (this.value == '_none') {
                    $('.field-name-field-promo-page').show();
                } else {
                    $('.field-name-field-promo-page').hide();
                }
            } );
            $('#edit-field-promo-custom-location-und').trigger('change');
        }
    };
})(jQuery);
