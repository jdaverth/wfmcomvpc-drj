
(function ($) {
  Drupal.behaviors.storeIndicator = {
    attach: function(context, settings) {
      var storeBlockContainer = $('#block-store-mobile-store-indicator', context);

      if (storeBlockContainer.length && $.cookie('local_store_name')) {
        storeBlockContainer.find('.content').html(Drupal.t('Your store is <a href="/mobile!store_url">!store_name</a>', {
          '!store_url': $.cookie('local_store_url'),
          '!store_name': $.cookie('local_store_name').replace(/\+/g, ' ')}));
      }
      else {
        storeBlockContainer.remove();
      }
    }
  };
  
  Drupal.behaviors.storeIndicatorState = {
    attach: function(context, settings) {
      $("#store-make-users-store-" + $.cookie('local_store') + " .set_store:not(.title)").replaceWith('<div class="user-store">This Is Your Store</div>');
    }
  };
})(jQuery);

