jQuery(document).on('store_data_ready', function(event, storeData){
  'use strict';
  /**
   * VueRouter defined in vue-router javascript file.
   * @link https://router.vuejs.org/en/
   */
  var router = new VueRouter({
    routes: [
      { path: '/store/:id', component: storeMapApp }
    ]
  });

  /**
   * VueGoogleMap defined in vue-google-maps javascript file.
   * @link https://github.com/xkjyeah/vue-google-maps
   * @link http://xkjyeah.github.io/vue-google-maps/
   */
  Vue.use(VueGoogleMap, {
    load: {
      client: 'gme-wfoods',
      v: '3.26'
    }
  });

  /**
   * Create a new Vue component.
   * @link https://vuejs.org/v2/guide/components.html
   */
  var CurrentStore = Vue.component('current-store', {
    /**
     * Template for the component.
     * @link https://vuejs.org/v2/guide/syntax.html
     */
    template: '<section class="selectedStore">' +
      '<h3 class="selectedStore-title">' +
        '<span class="selectedStore-label">Current Store:</span>&nbsp;' +
        '<a :href="/stores/ + store.path">{{store.storename}}</a>' +
      '</h3>' +
      '<address class="selectedStore-address">' +
        '<span class="selectedStore-address-streetAddress">{{ store.location.street }}</span><br>' +
        '<span class="selectedStore-address-addressLocality">{{ store.location.city }}</span>,&nbsp;' +
        '<span class="selectedStore-address-addressRegion">{{ store.location.statename }}</span>&nbsp;' +
        '<span class="selectedStore-address-postalCode">{{ store.location.zip }}</span>' +
      '</address>' +
    '</section>',

    /**
     * Component Properties.
     * @link https://vuejs.org/v2/guide/instance.html#Properties-and-Methods
     */
    props: ['store']
  });

  /**
   * Create a new Vue component.
   * @link https://vuejs.org/v2/guide/components.html
   */
  var NearbyStores = Vue.component('nearby-stores', {
    /**
     * Template for the component.
     * @link https://vuejs.org/v2/guide/syntax.html
     */
    template: '<section class="storeList">' +
      '<h3 class="storeList-title">Stores Close To {{ currentStore.storename }}</h3>' +
      '<ol class="storeList-list">' +
        // The variable 'unit' could be miles or km depending on country of current store. We're displaying all stores
        // within 100 miles or 100 km, depending on nationality of the chosen store.
        '<li class="storeList-listItem" v-if="storeObj[unit] < 100" v-for="storeObj in stores">' +
          '<router-link :to="storeObj.nid">{{ storeObj.storename }}</router-link>:&nbsp;' +
          '<span v-if="unit == \'mi\'">' +
            '{{ storeObj.mi}}&nbsp;' +
            '<span v-if="storeObj.mi > 1">miles</span>' +
            '<span v-if="storeObj.mi == 1">mile</span>&nbsp;' +
          '</span>' +
          '<span v-if="unit == \'km\'">' +
            '{{ storeObj.km }} km,&nbsp;' +
          '</span>' +
          '{{ storeObj.location.street }},&nbsp;' +
          '{{ storeObj.location.city }}, ' +
          '{{ storeObj.location.statename }}' +
        '</li>' +
      '</ol>' +
    '</section>',

    /**
     * Component Properties.
     * @link https://vuejs.org/v2/api/#props
     */
    props: {
      stores: {
        type: Array,
        required: true
      },
      currentStore: {
        type: Object,
        required: true
      }
    },

    /**
     * Computed properties.
     * @link https://vuejs.org/v2/api/#computed
     */
    computed: {
      unit: function() {
        return this.$store.state.unit_of_distance;
      }
    }
  });

  /**
   * Create a new Vue component.
   * @link https://vuejs.org/v2/guide/components.html
   */
  var AddressForm = Vue.component('address-form', {
    /**
     * Template for the component.
     * @link https://vuejs.org/v2/guide/syntax.html
     */
    template: '<form v-on:submit.prevent="transcodeAddress" class="address-form">' +
      '<input class="address-form-input" type="text" v-model="address" placeholder="Enter your address">' +
      '<input class="address-form-submit" type="submit" value="Submit">' +
    '</form>',

    /**
     * Data / properties for component.
     * @returns {{address: string}}
     * @link https://vuejs.org/v2/api/#data
     */
    data: function() {
      return {
        address: ''
      };
    },

    /**
     * Component methods.
     * @link https://vuejs.org/v2/api/#methods
     */
    methods: {
      /**
       * From submit callback. Calls Google Geocoder to translate an address to latitude/longitude.
       * @note the 'v-on:submit.prevent="transcodeAddress" above.
       */
      transcodeAddress: function() {
        var geocoder = new google.maps.Geocoder();
        // .bind makes Vue application accessible inside callback.
        geocoder.geocode({'address': this.address}, this.geocodeCallback.bind(this));
        this.address = '';
      },

      /**
       * Callback function for Google Geocoder.
       * On success sets the user position in the application state.
       * @param results Results object
       * @param status String
       * @link https://developers.google.com/maps/documentation/javascript/geocoding
       */
      geocodeCallback: function(results, status) {
        if (status == 'OK') {
          this.$store.dispatch('setUserPosition', {
            'lat': results[0].geometry.location.lat(),
            'lng': results[0].geometry.location.lng()
          });
        }
        else {
          console.log('Geocode was not successful for the following reason: ' + status);
        }
      }
    }
  });

  /**
   * Create a new Vue component.
   * @link https://vuejs.org/v2/guide/components.html
   */
  var WfmGoogleMap = Vue.component('wfm-google-map', {

    /**
     * Template for the component.
     * @link https://vuejs.org/v2/guide/syntax.html
     */
    template: '<google-map ' +
        ':center="center"' +
        ':zoom="zoom"' +
        'style="width: 100%; height: 300px"' +
      '>' +
        '<gmap-marker ' +
          ':position="marker_position"' +
          '@click="toggleInfoWindow"' +
          'v-if="!this.directionsDisplay"' +
        '>' +
          '<gmap-info-window ' +
            ':opened="info_window_open"' +
            '@closeclick="toggleInfoWindow"' +
          '>' +
            '<span class="gmap-info-window-title">' +
              '<a :href="/stores/ + store.path">' +
                '{{ store.storename }}' +
              '</a>' +
            '</span>' +
            '<address class="selectedStore-address">' +
              '<span class="selectedStore-address-streetAddress">{{ store.location.street }}</span><br>' +
              '<span class="selectedStore-address-addressLocality">{{ store.location.city }}</span>,&nbsp;' +
              '<span class="selectedStore-address-addressRegion">{{ store.location.statename }}</span>&nbsp;' +
              '<span class="selectedStore-address-postalCode">{{ store.location.zip }}</span>' +
            '</address>' +
            '<span class="gmap-info-window-phone">' +
              'Phone: {{ store.phone }}' +
            '</span>' +
          '</gmap-info-window>' +
        '</gmap-marker>' +
      '</google-map>',

    /**
     * Child components to be made available to this component.
     * @link https://vuejs.org/v2/api/#components
     */
    components: {
      'google-map': VueGoogleMap.Map
    },

    /**
     * Data / properties for component.
     * @returns {{zoom: number, info_window_open: boolean}}
     * @link https://vuejs.org/v2/api/#data
     */
    data: function() {
      return {
        zoom: 12,
        info_window_open: false
      };
    },

    /**
     * Computed properties.
     * @link https://vuejs.org/v2/api/#computed
     */
    computed: {
      /**
       * Map center.
       * @returns {{lat: number, lng: number}}
       */
      center: function() {
        return {
          lat: Number(this.store.location.lat),
          lng: Number(this.store.location.lon)
        };
      },

      /**
       * Store marker position.
       * @returns {{lat: number, lng: number}}
       */
      marker_position: function() {
        return {
          lat: Number(this.store.location.lat),
          lng: Number(this.store.location.lon)
        };
      },

      /**
       * User position.
       * @returns {{lat: string, lng: string}}
       */
      user_position: function() {
        return {
          lat: this.$store.state.user_position.lat,
          lng: this.$store.state.user_position.lng
        };
      }
    },

    /**
     * Component Properties.
     * @link https://vuejs.org/v2/api/#props
     */
    props: ['store'],

    /**
     * Component methods.
     * @link https://vuejs.org/v2/api/#methods
     */
    methods: {
      /**
       * Toggle infowindow on Google Map.
       * @see '@click="toggleInfoWindow"' in template above.
       */
      toggleInfoWindow: function() {
        if (this.info_window_open) {
          this.info_window_open = false;
        }
        else {
          this.info_window_open = true;
        }
      },

      /**
       * Callback function for HTML5 Geolocation.
       * On success, save user postion in the application state object.
       * @param data Object { coords: { latitude: ..., longitude: ... } }
       * @param error String
       */
      getCurrentPositionCallback: function(data, error) {
        if (error) {
          console.log(error);
        }
        else {
          this.$store.dispatch('setUserPosition', {
            'lat': data.coords.latitude,
            'lng': data.coords.longitude
          });
        }
      },

      /**
       * Get driving directions from arg 1 to arg 2 using Google Maps Directions Service.
       * @param from Object { lat: ..., lng: ... }
       * @param to Object { lat: ..., lng: ... }
       * @link https://developers.google.com/maps/documentation/javascript/directions
       */
      getDrivingDirections: function(from, to) {
        var pointA = new google.maps.LatLng(from.lat, from.lng),
          pointB = new google.maps.LatLng(to.lat, to.lng),
          directionsService = new google.maps.DirectionsService();

        directionsService.route({
          origin: pointA,
          destination: pointB,
          travelMode: google.maps.TravelMode.DRIVING
        }, this.getDrivingDirectionsCallback.bind(this));
      },

      /**
       * Callback function for Google Maps Directions Service call.
       * @param response Object
       * @param status String
       * @link https://developers.google.com/maps/documentation/javascript/directions
       */
      getDrivingDirectionsCallback: function(response, status) {
        var map = {};
        if (status == 'OK') {

          // Find google maps object in child component.
          this.$children.forEach(function(child) {
            if (child.$mapObject) {
              map = child.$mapObject;
            }
          });

          // Save directionsDisplay in 'this' so that we don't lose reference and are able to clear map and text
          // in the case that subsequent searches are performed after the first one.
          if (!this.directionsDisplay) {
            this.directionsDisplay = new google.maps.DirectionsRenderer({
              map: map
            });
          }

          // Clear directions in case this is not the first search.
          this.directionsDisplay.set('directions', null);
          // Set map directions.
          this.directionsDisplay.setDirections(response);
          // Set text directions.
          this.directionsDisplay.setPanel(document.getElementById('directions-panel'));
        }
        else {
          if (this.directionsDisplay) {
            this.directionsDisplay.set('directions', null);
          }
          console.log('Driving directions did not work because of: ' + status);
        }
      }
    }, // Methods

    /**
     * Object where keys are expressions to watch and values are the corresponding callbacks.
     * @link https://vuejs.org/v2/api/#watch
     */
    watch: {
      /**
       * Watch user_position for new position. Update driving directions when detected.
       * @param newPosition Object lat/lng pair.
       */
      user_position: function(newPosition) {
        this.getDrivingDirections(newPosition, this.marker_position);
      },

      /**
       * Watch marker_position for new position. Update diriving directions when detected.
       */
      marker_position: function() {
        // Checking latitude because user.position is always true due to the lat & lng keys with empty values.
        if (this.user_position.lat) {
          this.getDrivingDirections(this.user_position, this.marker_position);
        }
      }
    }, // watch

    /**
     * Mounted == Vue ready.
     * If HTML5 geolocation is available, do geolocation.
     * @link https://vuejs.org/v2/api/#mounted
     * @link https://developer.mozilla.org/en-US/docs/Web/API/Geolocation/Using_geolocation
     */
    mounted: function() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(this.getCurrentPositionCallback.bind(this));
      }
    }
  });

  /**
   * Root component of store map application.
   */
  var storeMapApp = new Vue({
    router: router,
    store: window.wfm_store_maps_state,
    el: '#wfm-map-app',
    components: {
      'nearby-stores': NearbyStores,
      'current-store': CurrentStore,
      'address-form': AddressForm,
      'wfm-google-map': WfmGoogleMap
    },

    /**
     * Data for component
     * @returns {{current_store: (*|Window.wfm_store_maps_state.state.current_store|{storename, hours, phone, path, location}), current_store_nid: (*|string), stores_by_distance: (*|Array), unit_of_distance: (*|string)}}
     * @link https://vuejs.org/v2/api/#data
     */
    data: function() {
      return {
        current_store: this.$store.state.current_store,
        current_store_nid: this.$store.state.current_store_nid,
        stores_by_distance: this.$store.state.stores_by_distance,
        unit_of_distance: this.$store.state.unit_of_distance
      };
    },

    /**
     * Component methods.
     * @link https://vuejs.org/v2/api/#methods
     */
    methods: {
      /**
       * Get distance between a set of latitude/longitude points.
       * This was adapted from sources on the internet. I don't know math.
       *
       * For background info and general mathiness:
       * @link http://www.movable-type.co.uk/scripts/latlong.html
       *
       * For a practical code implementation see:
       * @link http://stackoverflow.com/questions/14560999/using-the-haversine-formula-in-javascript
       *
       * @param from Object { lat: ..., lng: ... }
       * @param to Object { lat: ..., lng: ... }
       * @param uom String, mi || km || m
       * @returns {number} Distance between lat/lng points in supplied unit of measure.
       */
      getDistanceBetweenLatLonPoints: function(from, to, uom) {
        var R = '';
        switch(uom) {
          case 'mi': // miles
            R = 3961;
            break;
          case 'km': // kilometers
            R = 6373;
            break;
          case 'm': // meters
            R = 637300;
            break;
          default: // default miles
            R = 3961;
        }

        var dLat = this.toRadians(to.lat-from.lat);
        var dLon = this.toRadians(to.lon-from.lon);
        var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
          Math.cos(this.toRadians(from.lat)) * Math.cos(this.toRadians(to.lat)) *
          Math.sin(dLon/2) * Math.sin(dLon/2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var d = R * c;

        return d;
      },

      /**
       * Get array of stores sorted by distance from supplied store.
       * @param selectedStore Object store object
       * @param storeData Object of store sub-objects. Same thing you see in Drupal.settings.WholeFoods.stores
       * @returns {Array} Store object sorted by distance from selectedStore.
       */
      getStoresByDistanceFromStore: function(selectedStore, storeData) {
        var storesByDistance = [];
        var to = {};
        var from = {
          lat: selectedStore.location.lat,
          lon: selectedStore.location.lon
        };
        for (var storenid in storeData) {
          if (storeData.hasOwnProperty(storenid)) {
            to.lat = storeData[storenid].location.lat;
            to.lon = storeData[storenid].location.lon;

            if (storenid != selectedStore.nid) { // Don't add selected store to list of stores.
              storesByDistance.push({
                storename: storeData[storenid].storename,
                nid: storenid,
                location: storeData[storenid].location,
                mi: Math.round(this.getDistanceBetweenLatLonPoints(from, to, 'mi')),
                km: Math.round(this.getDistanceBetweenLatLonPoints(from, to, 'km'))
              });
            }
          }
        }
        // Sort storesByDistance by km. Ascending.
        storesByDistance.sort(function (a, b) {
          if (a.km < b.km) {
            return -1;
          }
          else if (a.km > b.km) {
            return 1;
          }
          else {
            return 0;
          }
        });
        return storesByDistance;
      },

      /**
       * Set unit of distance by country. US gets miles. Everybody else gets kilometers.
       * @param country
       */
      setUnitOfDistance: function(country) {
        if (country == 'US') {
          this.$store.dispatch('setUnitOfDistance', { 'unit': 'mi' });
        }
        else {
          this.$store.dispatch('setUnitOfDistance', { 'unit': 'km' });
        }
      },

      /**
       * Set application state. State as in state machine, not as in Texas.
       * @param storenid Store node id.
       */
      setState: function(storenid) {
        var country = storeData[storenid].location.country;
        var stores = [];
        storenid = parseInt(storenid);

        if (typeof storenid == 'number') {
          stores = this.getStoresByDistanceFromStore(storeData[storenid], storeData);
          this.$store.dispatch('setCurrentStoreNid', { "storenid": storenid });
          this.$store.dispatch('setCurrentStore', { "store": storeData[storenid] });
          this.$store.dispatch('setStoresByDistanceFromStore', { "stores": stores });

          this.setUnitOfDistance(country);
        }
      },

      /**
       * Converts an angle in degrees to an angle in radians.
       * @link https://en.wikipedia.org/wiki/Radian
       * @param num Number
       * @returns {number}
       */
      toRadians: function(num) {
        return num * Math.PI / 180;
      }
    }, // methods

    /**
     * Mounted == Vue ready.
     * Get store node id from url, set application state based on store node id.
     */
    mounted: function() {
      var storenid = this._route.params.id;
      this.setState(storenid);
    },

    /**
     * Watch router for changes in url. Set application state to reflect the change in store node id.
     */
    watch: {
      $route: function(to) {
        this.setState(to.params.id);
      }
    },

    /**
     * Template for root component.
     * @link https://vuejs.org/v2/api/#template
     */
    template: '<div class="application-root">' +
      '<current-store :store="current_store" v-if="current_store"></current-store>' +
      '<address-form></address-form>' +
      '<wfm-google-map v-if="current_store"" :store="current_store"></wfm-google-map>' +
      '<section class="directionsPanel">' +
        '<div id="directions-panel"></div>' +
      '</section>' +
      '<nearby-stores :stores="stores_by_distance" :currentStore="current_store"></nearby-stores>' +
    '</div>'
  });
});