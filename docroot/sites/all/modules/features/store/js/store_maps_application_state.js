(function(){
  'use strict';
  window.wfm_store_maps_state = new Vuex.Store({
    /**
     * Application state object.
     */
    state: {
      /**
       * @see https://vuejs.org/v2/guide/reactivity.html#Change-Detection-Caveats
       * For an explanation of why we explicitly list properties of an object.
       */
      current_store: {
        storename: '',
        hours: '',
        phone: '',
        path: '',
        location: {
          'street': '',
          'city': '',
          'statename': '',
          'zip': ''
        }
      },
      current_store_nid: '',
      stores_by_distance: [],
      unit_of_distance: 'mi',
      user_position: {
        lat: '',
        lng: ''
      }
    },
    /**
     * @see https://vuex.vuejs.org/en/mutations.html
     * For an explanation of Vuex mutations.
     */
    mutations: {
      /**
       * Update current store node id.
       * @param state Object
       *   state object from Vuex.Store.
       * @param payload Object
       *   { storenid: store_node_id }
       */
      setCurrentStoreNid: function(state, payload) {
        state.current_store_nid = payload.storenid;
      },

      /**
       * Update current store object
       * @param state Object
       *   State object from Vuex.Store.
       * @param payload Object
       *   { store: {store data object... } }
       */
      setCurrentStore: function(state, payload) {
        for (var prop in payload.store) {
          if (payload.store.hasOwnProperty(prop)) {
            state.current_store[prop] = payload.store[prop];
          }
        }
      },

      /**
       * Set array of stores sorted by distance from current store.
       * @param state Object
       *   State object from Vuex.Store.
       * @param payload Object
       *   { stores: [ array of store objects... ] }
       */
      setStoresByDistanceFromStore: function(state, payload) {
        // Clear stores_by_distance array otherwise we're appending store objects to the array.
        if (state.stores_by_distance.length > 0) {
          state.stores_by_distance.splice(0, state.stores_by_distance.length);
        }
        // Iterate over payload and add store objects to the stores_by_distance array.
        payload.stores.forEach(function(store){
          state.stores_by_distance.push(store);
        });
      },

      /**
       * Set unit of measurement for distance.
       * @param state Object
       *   State object from Vuex.Store.
       * @param payload Object
       *   { unit: unit_of_measure }
       */
      setUnitOfDistance: function(state, payload) {
        state.unit_of_distance = payload.unit;
      },

      /**
       * Set user position.
       * @param state Object
       *   State object from Vuex.Store.
       * @param payload Object
       *   { lat: ..., lng: ... }
       */
      setUserPosition: function(state, payload) {
        state.user_position.lat = payload.lat;
        state.user_position.lng = payload.lng;
      }
    },
    /**
     * @see https://vuex.vuejs.org/en/actions.html
     * For an explanation of VueX actions.
     */
    actions: {
      /**
       * Update current store node id.
       * @param context Object
       *   Operating context of the VueX store.
       * @param payload Object
       *   { storenid: store_node_id }
       */
      setCurrentStoreNid: function(context, payload) {
        context.commit('setCurrentStoreNid', payload);
      },

      /**
       * Update current store object
       * @param context Object
       *   Operating context of the VueX store.
       * @param payload Object
       *   { store: {store data object... } }
       */
      setCurrentStore: function(context, payload) {
        context.commit('setCurrentStore', payload);
      },

      /**
       * Set array of stores sorted by distance from current store.
       * @param context Object
       *   Operating context of the VueX store.
       * @param payload Object
       *   { stores: [ array of store objects... ] }
       */
      setStoresByDistanceFromStore: function(context, payload) {
        context.commit('setStoresByDistanceFromStore', payload);
      },

      /**
       * Set unit of measurement for distance.
       * @param context Object
       *   Operating context of the VueX store.
       * @param payload Object
       *   { unit: unit_of_measure }
       *
       */
      setUnitOfDistance: function(context, payload) {
        context.commit('setUnitOfDistance', payload);
      },

      /**
       * Set user position.
       * @param context Object
       *   Operating context of the VueX store.
       * @param payload Object
       *   { lat: ..., lng: ... }
       */
      setUserPosition: function(context, payload) {
        context.commit('setUserPosition', payload);
      }
    }
  });
})();