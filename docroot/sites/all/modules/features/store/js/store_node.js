(function ($) {
  Drupal.behaviors.storeNodeUserStore = {
    attach: function(context, settings) {
      var store_nid = $.cookie('local_store');
      // $('#store-node-show-' + store_nid, context).show();
      $('#store-node-show-' + store_nid, context).removeClass('element-invisible');
      $('#store-node-hide-' + store_nid, context).hide();
    }
  };
})(jQuery);
