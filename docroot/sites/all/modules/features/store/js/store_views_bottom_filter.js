Drupal.behaviors.StoreViewsAjaxBottomFilters = {};
Drupal.behaviors.StoreViewsAjaxBottomFilters.attach = function(context) {
  jQuery.each(Drupal.views.instances, function(i, settings) {
    if (settings.$view.length) {
      jQuery(settings.$view.selector + ' .view-filters-bottom').once(function () {
        var button = jQuery('input[type=submit], button[type=submit], input[type=image]', this);
        button = button[0];
        var element_settings = jQuery.extend( {}, settings.element_settings, {event: 'click'});
        delete element_settings.submit.page;
        delete element_settings.submit.address;
        new Drupal.ajax(jQuery(button).attr('id'), button, element_settings);
      });
    }
  });
};

(function storesListPage($){
  Drupal.behaviors.storesListPage = {};
  var _initStoresListPage = {
    init: function(){
      if($('.view-display-id-store_locations_block').length > 0) {
        $('.view-display-id-store_locations_block').delegate('.online-ordering-link', 'click', function(){
          var destination = $(this).attr('href');
          $(this).parent().siblings('form').find('input[name="destination"]').val(destination);
          $(this).parent().siblings('form').submit();
          return false;
        });
      }
    },
  };
  Drupal.behaviors.storesListPage.attach = function() {
    $('body').once('stores-list-page', function(){
      _initStoresListPage.init();
    });
  };
})(jQuery);

/*
 * Call jquery.sticky.js.
 * Calculate bottomSpacing for Sticky.
 */
(function ($) {
  Drupal.behaviors.storeStickyMap = {
    attach: function(context, settings) {
      var bodyHeight = $('body').height();
      var footerPosition = $('.view-display-id-store_locations_block .view-content-bottom').offset();
      var bottomPosition = bodyHeight - footerPosition.top;
      if ($(window).width() >= 768) {
        $(".view-stores-right").sticky({topSpacing:100, bottomSpacing:bottomPosition, center:true});
      }
    }
  };
})(jQuery);
