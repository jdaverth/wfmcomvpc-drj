<?php
/**
 * @file
 * store.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function store_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'event_page';
  $context->description = '';
  $context->tag = 'Events';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'event' => 'event',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-event_store-block' => array(
          'module' => 'views',
          'delta' => 'event_store-block',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-a1e65372696b4ddcfc5bbd5d00f89f1d' => array(
          'module' => 'views',
          'delta' => 'a1e65372696b4ddcfc5bbd5d00f89f1d',
          'region' => 'sidebar_second',
          'weight' => NULL,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Events');
  $export['event_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'events';
  $context->description = '';
  $context->tag = 'Store';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'events' => 'events',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-calendar_2-featured' => array(
          'module' => 'views',
          'delta' => 'calendar_2-featured',
          'region' => 'content_top',
          'weight' => '-10',
        ),
        'views-calendar_2-upcomming' => array(
          'module' => 'views',
          'delta' => 'calendar_2-upcomming',
          'region' => 'content_top',
          'weight' => '-9',
        ),
        'views-calendar_2-nearby' => array(
          'module' => 'views',
          'delta' => 'calendar_2-nearby',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Store');
  $export['events'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'related_promos';
  $context->description = '';
  $context->tag = 'Global';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~admin' => '~admin',
        '~admin/*' => '~admin/*',
        '~<front>' => '~<front>',
      ),
    ),
    'taxonomy_term' => array(
      'values' => array(
        'promo_category' => 'promo_category',
      ),
      'options' => array(
        'term_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-promo_blocks-block' => array(
          'module' => 'views',
          'delta' => 'promo_blocks-block',
          'region' => 'content_top',
          'weight' => '-10',
        ),
        'views-promo_blocks-three' => array(
          'module' => 'views',
          'delta' => 'promo_blocks-three',
          'region' => 'content_top',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Global');
  $export['related_promos'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_departments';
  $context->description = '';
  $context->tag = 'Store';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'department' => 'department',
        'service' => 'service',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'theme' => array(
      'values' => array(
        'wholefoods' => 'wholefoods',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-06dc1b3d1e69562fdf9ec466eda5ed5d' => array(
          'module' => 'views',
          'delta' => '06dc1b3d1e69562fdf9ec466eda5ed5d',
          'region' => 'content',
          'weight' => NULL,
        ),
        'views-store_departments-block_3' => array(
          'module' => 'views',
          'delta' => 'store_departments-block_3',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Store');
  $export['store_departments'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_departments_article';
  $context->description = '';
  $context->tag = 'Store';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'department_article' => 'department_article',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-store_departments-article_title' => array(
          'module' => 'views',
          'delta' => 'store_departments-article_title',
          'region' => 'content',
          'weight' => '-10',
        ),
        'views-06dc1b3d1e69562fdf9ec466eda5ed5d' => array(
          'module' => 'views',
          'delta' => '06dc1b3d1e69562fdf9ec466eda5ed5d',
          'region' => 'content',
          'weight' => NULL,
        ),
        'views-store_departments-block_3' => array(
          'module' => 'views',
          'delta' => 'store_departments-block_3',
          'region' => 'sidebar_second',
          'weight' => '-100',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Store');
  $export['store_departments_article'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_departments_landing_page';
  $context->description = '';
  $context->tag = 'Store';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'store-departments' => 'store-departments',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-store_departments-local' => array(
          'module' => 'views',
          'delta' => 'store_departments-local',
          'region' => 'content_bottom',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Store');
  $export['store_departments_landing_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_list';
  $context->description = '';
  $context->tag = 'Store';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'stores/list' => 'stores/list',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-80946ef4b139b2cead1c5f9f9cb3d671' => array(
          'module' => 'views',
          'delta' => '80946ef4b139b2cead1c5f9f9cb3d671',
          'region' => 'content',
          'weight' => NULL,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Store');
  $export['store_list'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_list_365';
  $context->description = '';
  $context->tag = 'Store';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'stores/list/365' => 'stores/list/365',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-store-list-pages' => array(
          'module' => 'menu',
          'delta' => 'store-list-pages',
          'region' => 'content',
          'weight' => '5',
        ),
        'views-d8e8fa84ece742264946ae1be8481b49' => array(
          'module' => 'views',
          'delta' => 'd8e8fa84ece742264946ae1be8481b49',
          'region' => 'content',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Store');
  $export['store_list_365'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_list_canada';
  $context->description = '';
  $context->tag = 'Store';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'stores/list/canada' => 'stores/list/canada',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-store_locations_by_state-canada' => array(
          'module' => 'views',
          'delta' => 'store_locations_by_state-canada',
          'region' => 'content',
          'weight' => '10',
        ),
        'menu-store-list-pages' => array(
          'module' => 'menu',
          'delta' => 'store-list-pages',
          'region' => 'content',
          'weight' => '9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Store');
  $export['store_list_canada'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_list_development';
  $context->description = '';
  $context->tag = 'Store';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'stores/list/development' => 'stores/list/development',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-513e1a74a1cb0e24f04860ca3bf2079c' => array(
          'module' => 'views',
          'delta' => '513e1a74a1cb0e24f04860ca3bf2079c',
          'region' => 'content',
          'weight' => '10',
        ),
        'menu-store-list-pages' => array(
          'module' => 'menu',
          'delta' => 'store-list-pages',
          'region' => 'content',
          'weight' => '5',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Store');
  $export['store_list_development'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_list_state';
  $context->description = '';
  $context->tag = 'Store';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'stores/list/state' => 'stores/list/state',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-store_locations_by_state-state' => array(
          'module' => 'views',
          'delta' => 'store_locations_by_state-state',
          'region' => 'content',
          'weight' => '10',
        ),
        'menu-store-list-pages' => array(
          'module' => 'menu',
          'delta' => 'store-list-pages',
          'region' => 'content',
          'weight' => '5',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Store');
  $export['store_list_state'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_list_uk';
  $context->description = '';
  $context->tag = 'Store';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'stores/list/uk' => 'stores/list/uk',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-store_locations_by_state-uk' => array(
          'module' => 'views',
          'delta' => 'store_locations_by_state-uk',
          'region' => 'content',
          'weight' => '10',
        ),
        'menu-store-list-pages' => array(
          'module' => 'menu',
          'delta' => 'store-list-pages',
          'region' => 'content',
          'weight' => '5',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Store');
  $export['store_list_uk'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_locations';
  $context->description = '';
  $context->tag = 'Store';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'store-locations' => 'store-locations',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'store-store-location-map' => array(
          'module' => 'store',
          'delta' => 'store-location-map',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Store');
  $export['store_locations'] = $context;

  return $export;
}
