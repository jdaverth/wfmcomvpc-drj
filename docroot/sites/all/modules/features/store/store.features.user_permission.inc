<?php
/**
 * @file
 * store.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function store_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_business_unit'.
  $permissions['create field_business_unit'] = array(
    'name' => 'create field_business_unit',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_business_unit'.
  $permissions['edit field_business_unit'] = array(
    'name' => 'edit field_business_unit',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_business_unit'.
  $permissions['edit own field_business_unit'] = array(
    'name' => 'edit own field_business_unit',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_business_unit'.
  $permissions['view field_business_unit'] = array(
    'name' => 'view field_business_unit',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_business_unit'.
  $permissions['view own field_business_unit'] = array(
    'name' => 'view own field_business_unit',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
