<?php
/**
 * @file
 * store.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function store_default_rules_configuration() {
  $items = array();
  $items['rules_store_updated_mail_development_team'] = entity_import('rules_config', '{ "rules_store_updated_mail_development_team" : {
      "LABEL" : "Store Updated - Mail Development Team",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "OWNER" : "rules",
      "TAGS" : [ "Store" ],
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_update" : [] },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "store" : "store" } } } }
      ],
      "DO" : [
        { "mail" : {
            "to" : "WebsiteUpdateswholefoodscom@wholefoods.com",
            "subject" : "Store Page Updated - [node:title]",
            "message" : "The store page \\u003Ca href=\\u0022[node:url]\\u0022\\u003E[node:title]\\u003C\\/a\\u003E has been updated by [site:current-user]."
          }
        }
      ]
    }
  }');
  return $items;
}
