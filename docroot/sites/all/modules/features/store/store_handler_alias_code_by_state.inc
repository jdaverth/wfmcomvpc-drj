<?php

/**
 * @file
 * Definition of store_handler_alias_code_by_state
 */

/**
 * Provides a alias code by State field.
 */
class store_handler_alias_code_by_state extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

  function query() {
    // do nothing -- to override the parent query.
  }

  public function render($data) {
    $output = drupal_get_path_alias('node/' . $data->nid);
    return $output;
  }
}

