<?php

/**
 * @file
 * Definition of store_handler_store_make_users_store
 */

/**
 * Provides a Store make users store field.
 */
class store_handler_store_make_users_store extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

  function query() {
    // do nothing -- to override the parent query.
  }

  public function render($data) {
    $form = drupal_get_form('store_make_users_store_' . $data->nid , $data->nid);
    $output = drupal_render($form);
    return $output;
  }
}
