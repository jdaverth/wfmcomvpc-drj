<?php
/**
 * @file
 * store_alcohol.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function store_alcohol_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'stores_sell_alcohol';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Stores That Sell Alcohol';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Stores That Sell Alcohol';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_postal_address',
      'rendered' => 1,
      'rendered_strip' => 1,
    ),
    1 => array(
      'field' => 'field_postal_address_2',
      'rendered' => 1,
      'rendered_strip' => 1,
    ),
    2 => array(
      'field' => 'field_postal_address_3',
      'rendered' => 1,
      'rendered_strip' => 1,
    ),
  );
  $handler->display->display_options['style_options']['class'] = 'stores_alcohol';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = 0;
  $handler->display->display_options['row_options']['default_field_elements'] = 0;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = FALSE;
  $handler->display->display_options['header']['area']['content'] = '<h2>Stores That Sell Alcohol</h2>';
  $handler->display->display_options['header']['area']['format'] = 'php_code';
  $handler->display->display_options['header']['area']['tokenize'] = 0;
  /* Field: Content: Address */
  $handler->display->display_options['fields']['field_postal_address']['id'] = 'field_postal_address';
  $handler->display->display_options['fields']['field_postal_address']['table'] = 'field_data_field_postal_address';
  $handler->display->display_options['fields']['field_postal_address']['field'] = 'field_postal_address';
  $handler->display->display_options['fields']['field_postal_address']['label'] = '';
  $handler->display->display_options['fields']['field_postal_address']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_postal_address']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['field_postal_address']['alter']['text'] = '[field_postal_address-country]

';
  $handler->display->display_options['fields']['field_postal_address']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_postal_address']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_postal_address']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_postal_address']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_postal_address']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_postal_address']['alter']['nl2br'] = 1;
  $handler->display->display_options['fields']['field_postal_address']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_postal_address']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_postal_address']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_postal_address']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_postal_address']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_postal_address']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_postal_address']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_postal_address']['element_wrapper_type'] = 'h2';
  $handler->display->display_options['fields']['field_postal_address']['element_wrapper_class'] = 'country_name';
  $handler->display->display_options['fields']['field_postal_address']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_postal_address']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_postal_address']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_postal_address']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_postal_address']['click_sort_column'] = 'administrative_area';
  $handler->display->display_options['fields']['field_postal_address']['settings'] = array(
    'use_widget_handlers' => 1,
    'format_handlers' => array(
      'address' => 'address',
    ),
  );
  $handler->display->display_options['fields']['field_postal_address']['field_api_classes'] = 0;
  /* Field: Content: Address */
  $handler->display->display_options['fields']['field_postal_address_2']['id'] = 'field_postal_address_2';
  $handler->display->display_options['fields']['field_postal_address_2']['table'] = 'field_data_field_postal_address';
  $handler->display->display_options['fields']['field_postal_address_2']['field'] = 'field_postal_address';
  $handler->display->display_options['fields']['field_postal_address_2']['label'] = '';
  $handler->display->display_options['fields']['field_postal_address_2']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['text'] = '[field_postal_address_2-administrative_area]';
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_postal_address_2']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_postal_address_2']['element_wrapper_type'] = 'h3';
  $handler->display->display_options['fields']['field_postal_address_2']['element_wrapper_class'] = 'state_name';
  $handler->display->display_options['fields']['field_postal_address_2']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_postal_address_2']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_postal_address_2']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_postal_address_2']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_postal_address_2']['click_sort_column'] = 'country';
  $handler->display->display_options['fields']['field_postal_address_2']['settings'] = array(
    'use_widget_handlers' => 1,
    'format_handlers' => array(
      'address' => 'address',
    ),
  );
  $handler->display->display_options['fields']['field_postal_address_2']['field_api_classes'] = 0;
  /* Field: Content: Address */
  $handler->display->display_options['fields']['field_postal_address_3']['id'] = 'field_postal_address_3';
  $handler->display->display_options['fields']['field_postal_address_3']['table'] = 'field_data_field_postal_address';
  $handler->display->display_options['fields']['field_postal_address_3']['field'] = 'field_postal_address';
  $handler->display->display_options['fields']['field_postal_address_3']['label'] = '';
  $handler->display->display_options['fields']['field_postal_address_3']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['text'] = '[field_postal_address_3-locality]';
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_postal_address_3']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_postal_address_3']['element_wrapper_type'] = 'h4';
  $handler->display->display_options['fields']['field_postal_address_3']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_postal_address_3']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_postal_address_3']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_postal_address_3']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_postal_address_3']['click_sort_column'] = 'country';
  $handler->display->display_options['fields']['field_postal_address_3']['settings'] = array(
    'use_widget_handlers' => 1,
    'format_handlers' => array(
      'address' => 'address',
    ),
  );
  $handler->display->display_options['fields']['field_postal_address_3']['field_api_classes'] = 0;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_type'] = 'h5';
  $handler->display->display_options['fields']['title']['element_class'] = 'store-name';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Content: Address */
  $handler->display->display_options['fields']['field_postal_address_1']['id'] = 'field_postal_address_1';
  $handler->display->display_options['fields']['field_postal_address_1']['table'] = 'field_data_field_postal_address';
  $handler->display->display_options['fields']['field_postal_address_1']['field'] = 'field_postal_address';
  $handler->display->display_options['fields']['field_postal_address_1']['label'] = '';
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_postal_address_1']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_postal_address_1']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_postal_address_1']['click_sort_column'] = 'country';
  $handler->display->display_options['fields']['field_postal_address_1']['settings'] = array(
    'use_widget_handlers' => 1,
    'format_handlers' => array(
      'address' => 'address',
    ),
  );
  $handler->display->display_options['fields']['field_postal_address_1']['field_api_classes'] = 0;
  /* Sort criterion: Content: Address (field_postal_address:country) */
  $handler->display->display_options['sorts']['field_postal_address_country_1']['id'] = 'field_postal_address_country_1';
  $handler->display->display_options['sorts']['field_postal_address_country_1']['table'] = 'field_data_field_postal_address';
  $handler->display->display_options['sorts']['field_postal_address_country_1']['field'] = 'field_postal_address_country';
  /* Sort criterion: Content: Address (field_postal_address:administrative_area) */
  $handler->display->display_options['sorts']['field_postal_address_administrative_area']['id'] = 'field_postal_address_administrative_area';
  $handler->display->display_options['sorts']['field_postal_address_administrative_area']['table'] = 'field_data_field_postal_address';
  $handler->display->display_options['sorts']['field_postal_address_administrative_area']['field'] = 'field_postal_address_administrative_area';
  /* Filter criterion: Content: Sells Alcohol (field_sells_alcohol) */
  $handler->display->display_options['filters']['field_sells_alcohol_value']['id'] = 'field_sells_alcohol_value';
  $handler->display->display_options['filters']['field_sells_alcohol_value']['table'] = 'field_data_field_sells_alcohol';
  $handler->display->display_options['filters']['field_sells_alcohol_value']['field'] = 'field_sells_alcohol_value';
  $handler->display->display_options['filters']['field_sells_alcohol_value']['value'] = array(
    1 => '1',
  );
  $handler->display->display_options['filters']['field_sells_alcohol_value']['group'] = 1;
  $handler->display->display_options['filters']['field_sells_alcohol_value']['expose']['operator_id'] = 'field_sells_alcohol_value_op';
  $handler->display->display_options['filters']['field_sells_alcohol_value']['expose']['label'] = 'Sells Alcohol (field_sells_alcohol)';
  $handler->display->display_options['filters']['field_sells_alcohol_value']['expose']['operator'] = 'field_sells_alcohol_value_op';
  $handler->display->display_options['filters']['field_sells_alcohol_value']['expose']['identifier'] = 'field_sells_alcohol_value';
  $handler->display->display_options['filters']['field_sells_alcohol_value']['expose']['reduce'] = 0;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'store' => 'store',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'stores-that-sell-alcohol';
  $export['stores_sell_alcohol'] = $view;

  $view = new view;
  $view->name = 'stores_that_do_not_sell_alcohol';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Stores That Do Not Sell Alcohol';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Stores That Do Not Sell Alcohol';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_postal_address',
      'rendered' => 1,
      'rendered_strip' => 1,
    ),
    1 => array(
      'field' => 'field_postal_address_2',
      'rendered' => 1,
      'rendered_strip' => 1,
    ),
    2 => array(
      'field' => 'field_postal_address_3',
      'rendered' => 1,
      'rendered_strip' => 1,
    ),
  );
  $handler->display->display_options['style_options']['class'] = 'stores_alcohol';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = 0;
  $handler->display->display_options['row_options']['default_field_elements'] = 0;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = '<h2>Stores That Do Not Sell Alcohol</h2>';
  $handler->display->display_options['header']['area']['format'] = 'php_code';
  $handler->display->display_options['header']['area']['tokenize'] = 0;
  /* Field: Content: Address */
  $handler->display->display_options['fields']['field_postal_address']['id'] = 'field_postal_address';
  $handler->display->display_options['fields']['field_postal_address']['table'] = 'field_data_field_postal_address';
  $handler->display->display_options['fields']['field_postal_address']['field'] = 'field_postal_address';
  $handler->display->display_options['fields']['field_postal_address']['label'] = '';
  $handler->display->display_options['fields']['field_postal_address']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_postal_address']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['field_postal_address']['alter']['text'] = '[field_postal_address-country]

';
  $handler->display->display_options['fields']['field_postal_address']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_postal_address']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_postal_address']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_postal_address']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_postal_address']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_postal_address']['alter']['nl2br'] = 1;
  $handler->display->display_options['fields']['field_postal_address']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_postal_address']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_postal_address']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_postal_address']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_postal_address']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_postal_address']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_postal_address']['element_type'] = 'h2';
  $handler->display->display_options['fields']['field_postal_address']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_postal_address']['element_wrapper_type'] = 'h2';
  $handler->display->display_options['fields']['field_postal_address']['element_wrapper_class'] = 'country_name';
  $handler->display->display_options['fields']['field_postal_address']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_postal_address']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_postal_address']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_postal_address']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_postal_address']['click_sort_column'] = 'administrative_area';
  $handler->display->display_options['fields']['field_postal_address']['settings'] = array(
    'use_widget_handlers' => 1,
    'format_handlers' => array(
      'address' => 'address',
    ),
  );
  $handler->display->display_options['fields']['field_postal_address']['field_api_classes'] = 0;
  /* Field: Content: Address */
  $handler->display->display_options['fields']['field_postal_address_2']['id'] = 'field_postal_address_2';
  $handler->display->display_options['fields']['field_postal_address_2']['table'] = 'field_data_field_postal_address';
  $handler->display->display_options['fields']['field_postal_address_2']['field'] = 'field_postal_address';
  $handler->display->display_options['fields']['field_postal_address_2']['label'] = '';
  $handler->display->display_options['fields']['field_postal_address_2']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['text'] = '[field_postal_address_2-administrative_area]';
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_postal_address_2']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_postal_address_2']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_postal_address_2']['element_wrapper_type'] = 'h3';
  $handler->display->display_options['fields']['field_postal_address_2']['element_wrapper_class'] = 'state_name';
  $handler->display->display_options['fields']['field_postal_address_2']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_postal_address_2']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_postal_address_2']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_postal_address_2']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_postal_address_2']['click_sort_column'] = 'country';
  $handler->display->display_options['fields']['field_postal_address_2']['settings'] = array(
    'use_widget_handlers' => 1,
    'format_handlers' => array(
      'address' => 'address',
    ),
  );
  $handler->display->display_options['fields']['field_postal_address_2']['field_api_classes'] = 0;
  /* Field: Content: Address */
  $handler->display->display_options['fields']['field_postal_address_3']['id'] = 'field_postal_address_3';
  $handler->display->display_options['fields']['field_postal_address_3']['table'] = 'field_data_field_postal_address';
  $handler->display->display_options['fields']['field_postal_address_3']['field'] = 'field_postal_address';
  $handler->display->display_options['fields']['field_postal_address_3']['label'] = '';
  $handler->display->display_options['fields']['field_postal_address_3']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['text'] = '[field_postal_address_3-locality]';
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_postal_address_3']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_postal_address_3']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_postal_address_3']['element_wrapper_type'] = 'h4';
  $handler->display->display_options['fields']['field_postal_address_3']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_postal_address_3']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_postal_address_3']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_postal_address_3']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_postal_address_3']['click_sort_column'] = 'country';
  $handler->display->display_options['fields']['field_postal_address_3']['settings'] = array(
    'use_widget_handlers' => 1,
    'format_handlers' => array(
      'address' => 'address',
    ),
  );
  $handler->display->display_options['fields']['field_postal_address_3']['field_api_classes'] = 0;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_type'] = 'h5';
  $handler->display->display_options['fields']['title']['element_class'] = 'store-name';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Content: Address */
  $handler->display->display_options['fields']['field_postal_address_1']['id'] = 'field_postal_address_1';
  $handler->display->display_options['fields']['field_postal_address_1']['table'] = 'field_data_field_postal_address';
  $handler->display->display_options['fields']['field_postal_address_1']['field'] = 'field_postal_address';
  $handler->display->display_options['fields']['field_postal_address_1']['label'] = '';
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_postal_address_1']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_postal_address_1']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_postal_address_1']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_postal_address_1']['click_sort_column'] = 'country';
  $handler->display->display_options['fields']['field_postal_address_1']['settings'] = array(
    'use_widget_handlers' => 1,
    'format_handlers' => array(
      'address' => 'address',
    ),
  );
  $handler->display->display_options['fields']['field_postal_address_1']['field_api_classes'] = 0;
  /* Sort criterion: Content: Address (field_postal_address:country) */
  $handler->display->display_options['sorts']['field_postal_address_country_1']['id'] = 'field_postal_address_country_1';
  $handler->display->display_options['sorts']['field_postal_address_country_1']['table'] = 'field_data_field_postal_address';
  $handler->display->display_options['sorts']['field_postal_address_country_1']['field'] = 'field_postal_address_country';
  /* Sort criterion: Content: Address (field_postal_address:administrative_area) */
  $handler->display->display_options['sorts']['field_postal_address_administrative_area']['id'] = 'field_postal_address_administrative_area';
  $handler->display->display_options['sorts']['field_postal_address_administrative_area']['table'] = 'field_data_field_postal_address';
  $handler->display->display_options['sorts']['field_postal_address_administrative_area']['field'] = 'field_postal_address_administrative_area';
  /* Filter criterion: Content: Sells Alcohol (field_sells_alcohol) */
  $handler->display->display_options['filters']['field_sells_alcohol_value']['id'] = 'field_sells_alcohol_value';
  $handler->display->display_options['filters']['field_sells_alcohol_value']['table'] = 'field_data_field_sells_alcohol';
  $handler->display->display_options['filters']['field_sells_alcohol_value']['field'] = 'field_sells_alcohol_value';
  $handler->display->display_options['filters']['field_sells_alcohol_value']['operator'] = 'not in';
  $handler->display->display_options['filters']['field_sells_alcohol_value']['value'] = array(
    1 => '1',
  );
  $handler->display->display_options['filters']['field_sells_alcohol_value']['group'] = 1;
  $handler->display->display_options['filters']['field_sells_alcohol_value']['expose']['operator_id'] = 'field_sells_alcohol_value_op';
  $handler->display->display_options['filters']['field_sells_alcohol_value']['expose']['label'] = 'Sells Alcohol (field_sells_alcohol)';
  $handler->display->display_options['filters']['field_sells_alcohol_value']['expose']['operator'] = 'field_sells_alcohol_value_op';
  $handler->display->display_options['filters']['field_sells_alcohol_value']['expose']['identifier'] = 'field_sells_alcohol_value';
  $handler->display->display_options['filters']['field_sells_alcohol_value']['expose']['reduce'] = 0;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'store' => 'store',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'stores-that-do-not-sell-alcohol';
  $export['stores_that_do_not_sell_alcohol'] = $view;

  return $export;
}
