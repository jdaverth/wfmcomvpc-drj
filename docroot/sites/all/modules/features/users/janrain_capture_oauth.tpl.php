<?php global $base_url; ?>
<?php global $user; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
  <body>
    <p>Please wait...</p>
   <?php
   $return = '';
   if ((time() - $user->created) < 10) {
		 $ga_id = variable_get('googleanalytics_account', '');

  ?>
  <script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', '<?php print $ga_id; ?>']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
    _gaq.push(['_trackEvent', 'accountcreation', 'clicks', 'complete']);
  </script>

  <?php
    }
  ?>
    <script type="text/javascript">
     if (navigator.appName != 'Microsoft Internet Explorer') {
     //  document.domain = '<?php print WFMVariables::get(WFMVariables::PLUCK_DOCUMENT_DOMAIN); ?>';
		 //document.domain = document.domain;
     }
      if (window.location.href != parent.window.location.href) {
        if (parent.window.location.href.indexOf("logout") > 1) {
          parent.window.location.href = "<?php echo $base_url; ?>";
        } else {
          parent.window.location.reload();
        }
      } else {
        window.location.href = "<?php echo $base_url; ?>";
      }
    </script>
  </body>
</html>
