<?php
/*
 * Frontend Callback functions for Users module.
 */

/**
 * Render HTML for header for user profile pages
 */
function users_profile_header() {
  $userApi = new WFMApiUser;
  global $user;
  try {
    $puid = $userApi->getPUIDFromUid($user->uid);
  }
  catch (Exception $e) {
    $puid = NULL;
  }
  if ( isset($_GET['uid']) && ($_GET['uid'] != $puid) ) {
    //Viewing another user's profile
    return '';
  }
  else {
    $block = module_invoke('users', 'block_view', 'users_profile_header');
    $html = '<div id="block-users-users-profile-header">' . drupal_render($block) . '</div>';
    return $html;
  }
}

/**
 * Callback Method: Redirects the user to the login URL. Suitable to be used
 * in the menu since this way the menu path is always static.
 */
function users_janrain_user_register() {
  if (module_exists('janrain_capture')) {
    if (user_register_access()) {
      if (isset($_REQUEST['code'])) {
        janrain_capture_oauth();
      }
      else {
        $login_url = janrain_capture_url();
        drupal_goto(empty($login_url) ? url('user/login') : $login_url);
      }
    }
    else {
      echo '<script type="text/javascript">';
      echo 'parent.window.location.reload();';
      echo '</script>';
    }
    exit();
  }
  else {
    drupal_goto(url('signin'));
  }
}

/**
 * Callback Method: Render a page with the Janrain Capture index.html sign in
 * / sign up form.
 *
 * Suitable to be used in the menu since this way the menu path is always static.
 *
 */
function users_janrain_user_signin() {
  if (user_is_logged_in()) {
    drupal_goto('user');
  } else {
    $headingtext = t('Sign in or create an account');
    $contents = '<h2>' . $headingtext . '</h2><div id="janrainInlineAuthDiv"></div>';
    return $contents;
  }
}

/**
 * Callback Method: /user/special-diets
 *
 * Render the Pluck persona edit widget
 */
function users_user_special_diets() {
  if (user_is_logged_in()) {
    $profile_header = users_profile_header();
    $form = drupal_get_form('users_special_diet_selection');
    $special_diet_form = drupal_render($form);
    $contents = $profile_header . $special_diet_form;
    return $contents;
  } else {
    drupal_goto('signin');
  }
}

/**
 * Callback Method: /user
 *
 * Render the Janrain edit profile widget
 */
function users_user_account_settings() {
  if (user_is_logged_in()) {
    //This will refresh the access token if it has expired. Default lifespan is 1 hour.
    //If access token is expired the edit profile form will err silently upon submission.
    if ((isset($_SESSION['janrain_capture_expires_in'])) && (REQUEST_TIME >= $_SESSION['janrain_capture_expires_in'])) {
      $api = new JanrainCaptureApi();
      $api->refreshAccessToken();
    }

    $profile_header = users_profile_header();
    $contents = $profile_header;

    $contents .= file_get_contents($_SERVER['DOCUMENT_ROOT'] .
      '/sites/all/themes/janrain-capture-screens/edit-profile.html');

    $js = file_get_contents($_SERVER['DOCUMENT_ROOT'] .
      '/sites/all/themes/janrain-capture-screens/edit-profile.js');
    if ($js) {
      // JS for the Janrain edit profile screen that should go into <head> before the widget.
      drupal_add_js($js, array('type' => 'inline', 'every_page' => TRUE, 'weight' => 2, 'preprocess' => FALSE));
    }else{
      drupal_set_message(t("An error has occurred."), $type = 'error', $repeat = TRUE);
      watchdog('features_users.pages.inc', 'An error occurred in users_user_account_settings() - '
        . 'Unable to get contents of file /sites/all/themes/janrain-capture-screens/edit-profile.js', array(), WATCHDOG_ERROR);
    }




// TODO: DETERMINE IF VIEWING OWN PROFILE OR OTHER PERSON
//       USERS SHOULD ONLY SEE THEIR OWN MANAGE ACCOUNT SCREEN
//    $contents .= '<!-- Link to the profile page as needed -->';
//    $contents .= '<a href="edit-profile.html" id="capture_profile_link" style="display:none">Edit Profile</a><br /><br />';



    return $contents;
  } else {
    drupal_goto('signin');
  }
}

/**
 * Callback Method: /user/shopping_lists
 *
 * Render the users shopping lists
 */
function users_user_shopping_lists() {
  if (user_is_logged_in()) {
    $contents = '<div class="useless"></div>';
    return $contents;
  } else {
    drupal_goto('signin');
  }
}

/**
 * Callback Method: /user/recipe_box
 *
 * Render the users recipe box
 */
function users_user_recipe_box() {
  if (user_is_logged_in()) {
    $profile_header = users_profile_header();
    $block = module_invoke('users', 'block_view', 'user_recipe_self');
    $contents = $profile_header . $block['content'];
    return $contents;
  }
  else {
    drupal_goto('signin');
  }
}

