<?php
/**
 * @file
 * users.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function users_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'user_profile_self';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'User Profile - Self';
  $quicktabs->tabs = array(
    0 => array(
      'bid' => 'users_delta_user_profile_self',
      'hide_title' => 1,
      'title' => 'Profile',
      'weight' => '-100',
      'type' => 'block',
    ),
    1 => array(
      'bid' => 'users_delta_user_preferences_self',
      'hide_title' => 1,
      'title' => 'Preferences',
      'weight' => '-99',
      'type' => 'block',
    ),
    2 => array(
      'bid' => 'users_delta_user_recipe_self',
      'hide_title' => 1,
      'title' => 'Recipe Box',
      'weight' => '-98',
      'type' => 'block',
    ),
    3 => array(
      'bid' => 'users_delta_user_shopping_self',
      'hide_title' => 1,
      'title' => 'Shopping List',
      'weight' => '-97',
      'type' => 'block',
    ),
    4 => array(
      'bid' => 'users_delta_user_activity_self',
      'hide_title' => 1,
      'title' => 'Activity',
      'weight' => '-96',
      'type' => 'block',
    ),
    5 => array(
      'bid' => 'users_delta_user_comments_self',
      'hide_title' => 1,
      'title' => 'Comments',
      'weight' => '-95',
      'type' => 'block',
    ),
    6 => array(
      'bid' => 'users_delta_user_messages_self',
      'hide_title' => 1,
      'title' => 'Messages',
      'weight' => '-94',
      'type' => 'block',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Activity');
  t('Comments');
  t('Messages');
  t('Preferences');
  t('Profile');
  t('Recipe Box');
  t('Shopping List');
  t('User Profile - Self');

  $export['user_profile_self'] = $quicktabs;

  return $export;
}
