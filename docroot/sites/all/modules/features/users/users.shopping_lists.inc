<?php

/**
 * Modal callback of the add to shopping list form.
 *
 * Used to add a product to shopping list from product page.
 */
function users_modal_add_to_shopping_list($js = NULL, $node = NULL) {
  ctools_include('modal');
  ctools_include('ajax');

  $form_state = array('title' => t('Add To Shopping List'), 'ajax' => TRUE);
  $form_state['product'] = $node;
  // wrapper call is necessary for ctools forms with modals
  $output = ctools_modal_form_wrapper('users_add_to_shopping_list_form', $form_state);

  if ($form_state['executed']) {
    print ajax_render(array(ctools_modal_command_dismiss()));
  }
  else {
    print ajax_render($output);
  }

  exit;
}

/**
 * Form constructor to add an item to a user's shopping list.
 *
 * Used to add a product to shopping list from product page.
 */
function users_add_to_shopping_list_form($form, &$form_state, $node) {

  try {
    $shoppingListApi = new WFMApiShoppingList;
    $list = $shoppingListApi->getLists($the_user->uid);
    $product = $form_state['product'];
    $selection_list = array();

    foreach ($list as $value) {
      $selection_list[$value->_id] = $value->name;
    }

    if ($product->type == 'product') {
      $item_list = array(
        0 => array(
          'name' => $product->title,
          'qty' => 1,
          'desc' => t('Added from product page'),
        ),
      );
    }
    else { // if recipe
      $item_list = array();
    }

    $form['list_id'] = array(
      '#type' => 'select',
      '#options' => $selection_list,
    );

    $options = array();
    $default = array();

    foreach ($item_list as $key => $item) {
      $options[$key] = $item['qty'] . ' ' . $item['name'] . ' (' . $item['desc'] . ')';
      $default[] = $key;
    }

    // TODO: Theme function.
    foreach ($list as $value) {
      $markup  = '<div class="shopping_list" id="shopping_list_' . $value->_id . '">';
      $markup .= '<h3>' . $value->name . '</h3>';
      $markup .= '<ul>';

      foreach ($value->Item as $item) {
        $markup .= '<li>';
        $markup .= $item->note . '(' . $item->description . ')';
        $markup .= '</li>';
      }

      $markup .= '</ul></div>';

      $form['shopping_list_' . $value->_id] = array(
        '#type' => 'markup',
        '#attributes' => array('class' => array('shopping_list')),
        '#markup' => $markup,
      );
    }

    // Get a list of pre-selected ones.
    $form['selection'] = array(
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $default,
    );

    $form['selection_list'] = array(
      '#type' => 'hidden',
      '#value' => serialize($item_list),
    );

    // #ajax is necessary in order for the form to submit, since having an action doesn't appear to work
    $form['submit'] = array(
      '#type' => 'submit',
      '#ajax' => array(
        'callback' => 'users_add_to_shopping_list_form_submit'
      ),
      '#value' => t('Add Item To Shopping List'),
    );
  }
  catch (Exception $e) {
    //Do nothing
  }

  return $form;
}

/**
 * Submit handler for users_add_to_shopping_list_form().
 *
 * Used to add a product to shopping list from product page.
 */
function users_add_to_shopping_list_form_submit($form, &$form_state) {
  $selection_list = unserialize($form_state['values']['selection_list']);
  $list = array();

  foreach ($form_state['values']['selection'] as $k => $v) {
    $list[$k] = $selection_list[$k];
  }

  try {
    global $user;
    $shoppingListApi = new WFMApiShoppingList();
    $list_id = $form_state['values']['list_id'];

    foreach ($list as $value) {
      $name = $value['name'];
      $qty  = $value['qty'];
      $desc = $value['desc'];

      $shoppingListApi->addItemToList($user->uid, $list_id, $name, $desc, $qty);
    }

    // dismiss the loaded modal
    ctools_include('modal');
    ctools_include('ajax');

    return array(
      '#type'     => 'ajax',
      '#commands' => array(ctools_modal_command_dismiss())
    );
  }
  catch (Exception $e) {
    //Do nothing
  }
}

/**
 * Callback to remove an item from the logged in user's shopping list via
 * the WFM API.
 */
function users_remove_shopping_list_item($list_id) {
  global $user;

  try {
    $shoppingListApi = new WFMApiShoppingList;

    $shopping_lists = $shoppingListApi->getLists($user->uid);
    foreach ($shopping_lists as $shopping_list) {
      if ($shopping_list->_id == $list_id) {
        $list = $shopping_list;
      }
    }
    wfm_datalayer_cookie_push(array(
      'event' => 'shoplist-interaction',
      'shop-list-name' => $list->name,
      'interaction' => 'delete',
    ));
    
    $shoppingListApi->deleteList($user->uid, $list_id);
    
    //Synchronize with shared list(s)
    users_shopping_list_sync_delete_list($list_id);
  }
  catch (Exception $e) {
    //Do nothing
  }

  drupal_goto("user/shopping_lists?show_list=$list_id");
}

/**
 * Callback to rename a shopping list for the logged in user via the WFM API.
 */
function users_rename_shopping_list($listID, $new_name) {
  global $user;

  try {
    $new_name = urldecode($new_name);
    $shoppingListApi = new WFMApiShoppingList;
    $shoppingListApi->renameList($user->uid, $listID, $new_name);

    //Synchronize with shared list(s)
    users_shopping_list_sync_rename_list($listID, $new_name);
  }
  catch (Exception $e) {
    //Do nothing
  }

  $_GET['destination'] = url('user/shopping_lists', array('query' => array('show_list' => $listID)));
  drupal_goto("user/shopping_lists?show_list=$listID");
}

/**
 * Form constructor for adding an item to a user's shopping list.
 */
function users_add_shopping_list_item_form($form, &$form_state, $item = NULL, $list_id = NULL) {
  $shoppingList = new WFMApiShoppingList();
  $categories = $shoppingList->getCategories();
  $category_options = users_get_options_from_categories($categories);

  $defaults = array('name' => '', 'description' => '', 'category' => '22');

  if ($item !== NULL) {
    $defaults['name'] = decode_entities($item->name);
    $defaults['description'] = decode_entities($item->note);
    if ($item->category_id) {
      $defaults['category'] = check_plain($item->category_id);
    }
  }

  $form['item_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Item'),
    '#required' => TRUE,
    '#description' => t('ex: Apples'),
    '#default_value' => $defaults['name'],
  );

  $form['item_desc'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#description' => t('ex: Nice ones for pie.'),
    '#default_value' => $defaults['description'],
  );

  $form['item_category'] = array(
    '#type' => 'select',
    '#title' => t('Category'),
    '#options' => $category_options,
    '#default_value' => $defaults['category'],
  );

  $form['list_id'] = array(
    '#type' => 'hidden',
  );

  if ($list_id !== NULL) {
    $form['list_id']['#value'] = $list_id;
  }

  if ($item !== NULL) {
    $form['item_id'] = array(
      '#type' => 'hidden',
      '#value' => $item->_id,
    );

    $form['update'] = array(
      '#type' => 'submit',
      '#value' => t('Update Item'),
    );
    $form['delete'] = array(
      '#attributes' => array(
        'class' => array('delete-list-item'),
      ),
      '#type' => 'submit',
      '#value' => t('Remove Item From List'),
    );
  }
  else {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add Item to List'),
    );
  }

  return $form;
}

/**
 * Form submit handler for users_add_shopping_list_item_form().
 */
function users_add_shopping_list_item_form_submit($form, &$form_state) {
  global $user;
  $shoppingListApi = new WFMApiShoppingList();
  $listID = $form_state['values']['list_id'];

  if (strpos($form_state['triggering_element']['#id'], 'edit-delete') !== FALSE) {
    // DELETE CALL BACK
    try {
      $itemID = $form_state['values']['item_id'];
      $response = $shoppingListApi->deleteItemFromList($user->uid, $listID, $itemID);

      //Synchronize with shared list(s)
      users_shopping_list_sync_delete_item($listID, $itemID);

      if (!$response || $response['status'] != 'success') {
        drupal_set_message(t("There was an error deleting the item from your list. If this continues, please let us know about it!"), "error");
      }
      else {
        drupal_set_message(t(check_plain($response['status_message'])));
      }

    }
    catch (Exception $e) {
      //Do nothing
    }
  }
  elseif (strpos($form_state['triggering_element']['#id'], 'edit-update') !== FALSE) {
    try {
      $itemID = check_plain($form_state['values']['item_id']);
      $name = check_plain($form_state['values']['item_name']);
      $desc = check_plain($form_state['values']['item_desc']);
      $category = check_plain($form_state['values']['item_category']);

      $shoppingListApi->editItemInList($user->uid, $listID, $itemID, $name, $desc, $category);

      //Synchronize with shared list(s)
      users_shopping_list_sync_update_item($listID, $itemID, $name, $desc, $category);

      $item_string = $name;

      if ($desc != '') {
        $item_string .= ' (' . $desc . ')';
      }

      drupal_set_message(t("!item has been updated in your shopping list.", array('!item' => $item_string)));
    }
    catch (Exception $e) {
      //Do nothing
    }
  }
  else {
    try {
      $name = filter_xss($form_state['values']['item_name']);
      $desc = filter_xss($form_state['values']['item_desc']);
      $category = check_plain($form_state['values']['item_category']);

      $itemResponse = $shoppingListApi->addItemToList($user->uid, $listID, $name, $desc, $category);

      if (isset($itemResponse['success']) && $itemResponse['success'] == 1) {
        $itemID = $itemResponse['new_id'];

        //Synchronize with shared list(s)
        users_shopping_list_sync_add_item($listID, $itemID, $name, $desc, $category);
      }
    }
    catch (Exception $e) {
      //Do nothing
    }
  }

  $_GET['destination'] = url('user/shopping_lists', array('query' => array('show_list' => $listID)));
  drupal_goto("user/shopping_lists?show_list=$listID");
}

/**
 * Form constructor for adding a new shopping list for a user.
 */
function users_add_shopping_list_form($form, &$form_state) {
  $form['list_name'] = array(
    '#type' => 'textfield',
    '#title' => 'Name Of Shopping List',
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Add Shopping List',
  );

  $form['#action'] = url('user/shopping_lists');
  return $form;
}

/**
 * Submit handler for users_add_shopping_list_form().
 *
 * Builds you a brand new shopping list.
 */
function users_add_shopping_list_form_submit($form, &$form_state) {
  try {
    global $user;

    $shoppingListApi = new WFMApiShoppingList();
    $response = $shoppingListApi->addList($user->uid, $form_state['values']['list_name']);
    $listID = $response['new_id'];
    
    wfm_datalayer_cookie_push(array(
      'event' => 'shoplist-interaction',
      'shop-list-name' => check_plain($form_state['values']['list_name']),
      'interaction' => 'create',
    ));

    $_GET['destination'] = url('user/shopping_lists', array('query' => array('show_list' => $listID)));
    drupal_goto("user/shopping_lists?show_list=$listID");
  }
  catch (Exception $e) {
    watchdog('wfmapi', $e);
  }
}

/**
 * Callback to copy a shared shopping list for the logged in user via the WFM API.
 */
function users_shopping_list_add_shared_list($shortCode = NULL) {
  GLOBAL $user;
  $shoppingListApi = new WFMApiShoppingList();

  //Make sure this is a logged in user
  if (empty($user->uid)) {
    drupal_set_message('You must be a registered user to use this feature.');
    drupal_goto("<front>");
  }

  //Verify a short code was provided
  if (empty($shortCode)) {
    //Throw an error or otherwise inform the user they must provide a valid shortcode
    drupal_set_message('You must provide a short code to add this shopping list.');
    drupal_goto("<front>");
  }

  //Decode the shortCode into the shareID and get the share
  $share = WFMShare::getShareFromShortCode($shortCode);

  //See if the share exists (already checked by the class)
  if (!$share) {
    //Throw an error about this not being an existant share.
    drupal_set_message('We were unable to find the shared information. Please check the URL and try again.');
    drupal_goto("<front>");
  }

  //Verify the recieving user is the intended recipient
  if ($share->recipient_email != $user->mail) {
    //Throw an error about not being a valid user
    drupal_set_message('Your account must be registered with the same email as the shared list to use this feature.');
    drupal_goto("<front>");
  }

  //Make sure this has not been used yet
  if ($share->recipient_item_id != '[none given]') {
    //Throw an error about not being a valid user
    drupal_set_message('You have already accepted this shared list. Please look for it on your shopping list page.');
    drupal_goto("user");
  }

  //Load the shared list
  $listID = $share->shared_item_id;
  $sharerUID = $share->uid;
  $shoppingList = user_build_shopping_list($listID, NULL, $sharerUID);

  if (empty($shoppingList)) {
    drupal_set_message('Unable to load the shared shopping list. Please check the URL and try again.');
    drupal_goto("user");
  }

  //Everything has been verified. Now lets clone the list
  try {
    //Duplicate the list
    $shoppingListApi = new WFMApiShoppingList;
    $shoppingListApi->addList($user->uid, $shoppingList->name);
    $list = $shoppingListApi->getLists($the_user->uid);
    $first = array_shift($list);
    $newListID = $first->id;
    $shareItemMap = array();

    //Save the ID of the new list to the share table
    WFMShare::update($share->shareid, array('recipient_item_id' => $newListID));

    //Duplicate the list items
    foreach ($shoppingList->items as $item) {
      $name = $item->note;
      $desc = $item->description;
      $category = $item->category;
      $itemResponse = $shoppingListApi->addItemToList($user->uid, $newListID, $name, $desc, $category);

      //Get new item ID
      $newItemID = $itemResponse['new_id'];

      //Save to map
      $shareItemMap[$item->id] = $newItemID;
    }

    //Create map for both directions and save to share
    $data->itemMap = new stdClass();
    $data->itemMap->FromSharerToRecipient = $shareItemMap;
    $data->itemMap->FromRecipientToSharer = array();

    foreach ($shareItemMap as $sharerItemID => $recipientItemID) {
      $data->itemMap->FromRecipientToSharer[$recipientItemID] = $sharerItemID;
    }

    //Save this to the share
    WFMShare::update($share->shareid, array('data' => $data));

    $_GET['show_list'] = $newListID;
    $_GET['destination'] = url('user/shopping_lists', array('query' => array('show_list' => $newListID)));
    drupal_goto();
  }
  catch (Exception $e) {
    //Do nothing
  }

  drupal_goto("user/shopping_lists?show_list=$newListID");

}

/**
 * Callback to generate a share of a list and send it to a specific person.
 * Returns the share shortcode and add share link. For AJAX.
 */
function users_shopping_list_generate_share($listID = NULL, $recipientEmail = NULL) {
  //First we need to store some information
  GLOBAL $user, $base_url;

  $info = array(
    'recipient_email' => $recipientEmail,
    'shared_item_id' => $listID,
    'type' => 'shopping list',
    'title' => 'Shared by ' . format_username($user),
    'data' => array(),
  );

  $shareID = WFMShare::create($info);
  $shortCode = WFMShare::getShortCode($shareID);
  $url = "$base_url/action/shopping_list/add_shared_list/$shortCode";

  print $url;
  exit();
}

//Deletes a share when the list is deleted
function users_shopping_list_sync_delete_list($listID) {
  //Not sure, but I think that deleted lists will just be marked as such in the shared info
  $syncs = users_shopping_list_synchronize_shared_lists($listID);

  if (empty($syncs)) {
    return FALSE;
  }

  //Synchronize with all shares
  foreach ($syncs as $syncInfo) {
    extract($syncInfo); //Extracts vars: share, otherList, otherUser, sharedToMe, and itemMapping

    //Delete the share
    WFMShare::delete($share->shareid);
  }
}

//Updates list name in other list
function users_shopping_list_sync_rename_list($listID, $newName) {
  //Just synchronize the renaming of a list
  $syncs = users_shopping_list_synchronize_shared_lists($listID);

  if (empty($syncs)) {
    return FALSE;
  }

  //Synchronize with all shares
  foreach ($syncs as $syncInfo) {
    extract($syncInfo); //Extracts vars: share, otherList, otherUser, sharedToMe, and itemMapping

    // Run this at the end:
    WFMApiShoppingList::renameList($otherUser->uid, $otherList->id, $newName);

    //No need to update the share
  }
}

//Deletes item from both ends of share
function users_shopping_list_sync_delete_item($listID, $itemID) {
  //Deleting an item in the list
  $syncs = users_shopping_list_synchronize_shared_lists($listID);

  if (empty($syncs)) {
    return FALSE;
  }

  //Synchronize with all shares
  foreach ($syncs as $syncInfo) {
    extract($syncInfo); //Extracts vars: share, otherList, otherUser, sharedToMe, and itemMapping

    if (isset($itemMapping[$itemID])) {
      $otherItemID = $itemMapping[$itemID];

      // Run this at the end:
      $shoppingListApi = new WFMApiShoppingList;
      $shoppingListApi->deleteItemFromList($otherUser->uid, $otherList->id, $otherItemID);

      //Remove item mappings (both directions)
      $data = $share->data;

      if ($sharedToMe) {
        unset($data->itemMap->FromSharerToRecipient[$otherItemID]);
        unset($data->itemMap->FromRecipientToSharer[$itemID]);
      }
      else {
        unset($data->itemMap->FromSharerToRecipient[$itemID]);
        unset($data->itemMap->FromRecipientToSharer[$otherItemID]);
      }

      //Save this to the share
      WFMShare::update($share->shareid, array('data' => $data));
    }
  }
}

//Updates a list item in the other list
function users_shopping_list_sync_update_item($listID, $itemID, $name, $desc, $category) {
  $shoppingListApi = new WFMApiShoppingList;
  //Updating an item in the list
  $syncs = users_shopping_list_synchronize_shared_lists($listID);

  if (empty($syncs)) {
    return FALSE;
  }

  //Synchronize with all shares
  foreach ($syncs as $syncInfo) {
    extract($syncInfo); //Extracts vars: share, otherList, otherUser, sharedToMe, and itemMapping

    // Run this at the end:
    $shoppingListApi->editItemInList($otherUser->uid, $otherList->id, $itemMapping[$itemID], $name, $desc, $category);

    //No need to update the mappings
  }
}

//Adds a list item to the other list
function users_shopping_list_sync_add_item($listID, $itemID, $name, $desc, $category) {
  //Adding an item to the list
  $syncs = users_shopping_list_synchronize_shared_lists($listID);
  $shoppingListApi = new WFMApiShoppingList();

  if (empty($syncs)) {
    return FALSE;
  }

  //Synchronize with all shares
  foreach ($syncs as $syncInfo) {
    extract($syncInfo); //Extracts vars: share, otherList, otherUser, sharedToMe, and itemMapping

    // Run this at the end:
    $itemResponse = $shoppingListApi->addItemToList($otherUser->uid, $otherList->id, $name, $desc, $category);

    //We need the new item ID so that we can add it to the item mapping in the share
    $newItemID = $itemResponse['new_id'];

    //Add to the item mapping in the share (both directions)
    $data = $share->data;

    if ($sharedToMe) {
      $data->itemMap->FromSharerToRecipient[$newItemID] = $itemID;
      $data->itemMap->FromRecipientToSharer[$itemID] = $newItemID;
    }
    else {
      $data->itemMap->FromSharerToRecipient[$itemID] = $newItemID;
      $data->itemMap->FromRecipientToSharer[$newItemID] = $itemID;
    }

    //Save this to the share
    WFMShare::update($share->shareid, array('data' => $data));
  }
}

/**
 * Checks to see if the list has been shared.
 * Returns FALSE if not or an issue, otherwise
 * it returns share information.
 */
function users_shopping_list_synchronize_shared_lists($listID = NULL) {
  if (empty($listID)) {
    return FALSE;
  }

  GLOBAL $user;
  $sharedToMe = TRUE;

  //First we need to make sure the list is shared. If not, no foul.
  $share = WFMShare::readByRecipient($listID, $user->mail); //See if this list was shared to us

  if (empty($share)) {
    $share = WFMShare::readBySharer($listID, $user->uid); //See if you have shared this list
    $sharedToMe = FALSE;
  }

  if (!$share) {
    return FALSE; //Nothing found, no worries
  }

  //We have found a share, now we have to prepare individual and multiple shares
  if (is_object($share)) {
    $shares[] = user_shopping_list_prepare_share($share, $sharedToMe);
  }
  else {
    foreach ($share as $item) {
      $shares[] = user_shopping_list_prepare_share($item, $sharedToMe);
    }
  }

  $goodShares = FALSE;

  //Make sure we have good shares
  foreach ($shares as $share) {
    if (is_array($share)) {
      $goodShares = TRUE;
    }
  }

  //Now we can begin the actual synchronization
  if ($goodShares) {
    return $shares;
  }

  drupal_set_message('Unable to synchronize shared list(s)');
  return FALSE;
}

function user_shopping_list_prepare_share($share, $sharedToMe) {
  $itemMap = $share->data->itemMap;

  if ($sharedToMe) {
    $itemMapping = $itemMap->FromRecipientToSharer;
    $otherListID = $share->shared_item_id; //The list ID from the sharer
    $otherUser = user_load($share->uid);
  }
  else {
    $itemMapping = $itemMap->FromSharerToRecipient;
    $otherListID = $share->recipient_item_id; //The list ID from the recipient
    $otherUser = user_load_by_mail($share->recipient_email);
  }

  if (!$otherUser) { //<<<--- Probably need to handle these differently
    #drupal_set_message("Unable to syncronize shared list. Problem with user information.");
    return FALSE;
  }

  //Load the other list
  $otherList = user_build_shopping_list($otherListID, NULL, $otherUser->uid);

  if (!$otherList) { //<<<--- Probably need to handle these differently
    #drupal_set_message('Unable to syncronize list. Problem loading the shared list.');
    return FALSE;
  }

  //Now we can begin the actual synchronization
  return array(
    'share' => $share,
    'otherList' => $otherList,
    'otherUser' => $otherUser,
    'sharedToMe' => $sharedToMe,
    'itemMapping' => $itemMapping,
  );
}

/**
 * Modal callback of the email form.
 */
function user_modal_email_shopping_list($js = NULL, $template = NULL, $listID = NULL, $sortBy = NULL) {
  GLOBAL $user;

  $shoppingList = user_build_shopping_list($listID, $sortBy);
  $emailVars = array(
    'list' => $shoppingList,
    'listID' => $listID,
    'sortBy' => $sortBy,
    'user' => $user,
  );

  if ($template == 'share') {
    $html = theme('users_profile_share_shopping_list', $emailVars);
  }
  else {
    $html = theme('users_profile_email_shopping_list', $emailVars);
  }

  $output[] = ctools_modal_Command_display(t('Success!'), $html);

  print ajax_render($output);
  exit();
}

/**
 * Callback to print a shopping list for the logged in user via the WFM API.
 * We output a printable page with no theming. Just a template and we are good to go.
 */
function users_print_shopping_list($listID = NULL, $sortBy = NULL) {
  drupal_set_breadcrumb(array()); //Disable the breadcrumbs
  $shoppingList = user_build_shopping_list($listID, $sortBy);
  $printVars = array(
    'list' => $shoppingList,
    'listID' => $listID,
    'sortBy' => $sortBy,
  );

  drupal_set_title("Print {$shoppingList->name} Shopping List");
  $content = array(
    '#markup' => theme('users_profile_print_shopping_list', $printVars),
  );
  return $content;
}

/**
 * Callback to sms a shopping list for the logged in user via the WFM API.
 */
function users_sms_shopping_list($js = NULL, $listID = NULL, $sortBy = NULL) {  
  GLOBAL $user;

  $smsVars = array(
    'user' => $user
  );

  $html = theme('users_profile_sms_shopping_list', $smsVars);
  $output[] = ctools_modal_Command_display(t('Success!'), $html);

  print ajax_render($output);
  exit();
}

/**
 * View a shared shopping list (via SMS URL)
 */
function users_sms_view_shopping_list($juid = NULL, $listID  = NULL, $sortBy = NULL) {

  // This is a temporary fix to ensure Varnish doesn't cache users' shopping lists
  // We should reevaluate this and come up with a more long term solution 
  // See QD-928 
  drupal_add_http_header('Cache-Control', 'no-cache');
  drupal_page_is_cacheable(FALSE);
 
  //Quick decode of the short codes.
  //$listID = WFMShare::decodeShortCode($listIDEncoded);
  //$userID = WFMShare::decodeShortCode($userIDEncoded);

  $shoppingList = user_build_shopping_list($listID, $sortBy, NULL, $juid);
  $smsVars = array(
    'list' => $shoppingList,
    'listID' => $listID,
    'sortBy' => $sortBy,
  );

  drupal_set_title("SMS View {$shoppingList->name} Shopping List");
  $content = array(
    '#markup' => theme('users_profile_sms_view_shopping_list', $smsVars),
  );

  return $content;
}

/**
 * This builds the shopping list for printing and emailing.
 */
function user_build_shopping_list($listID = NULL, $sortBy = NULL, $otherUserID = NULL, $juid = NULL) {
  global $user;
  $uid = $user->uid;

  if (!empty($otherUserID)) {
    $uid = $otherUserID;
  }

  //Load the shopping lists
  $shoppingListApi = new WFMApiShoppingList;
  $lists = $shoppingListApi->getLists($the_user->uid);

  if (empty($lists)) {
    return FALSE;
  }

  //Select the list of doom
  foreach ($lists as $list) {
    if ($list->id == $listID) {
      $shoppingList = $list;
      break;
    }
  }

  //Get the items, sorted and all
  $shoppingList->items = users_get_and_sort_shopping_list_items($uid, $listID, $sortBy, $juid);
  return $shoppingList;
}

/**
 * Takes the list of shopping list items, adds the category name and sorts it.
 */
function users_get_and_sort_shopping_list_items($userID, $listID, $sortBy, $juid = NULL) {
  //$items = WFMApiShoppingList::getItems($lists, $listID, $userID);
  $shoppingListApi = new WFMApiShoppingList;
  $items = $shoppingListApi->getItems($userID, $listID);

  //First we need to get the categories
  foreach ($items as $key => $item) {
    $items[$key]->category = $shoppingListApi->getCategoryName($item->category_id);
  }

  //We output a printable page with no theming. Just a template and we are good to go.
  if ($sortBy == 'category') {
    uasort($items, 'users_item_list_sort_by_cat');
  }
  else {
    uasort($items, 'users_item_list_sort_by_latest');
  }

  return $items;
}

/**
 * Sorts shopping list items by created date/time.
 */
function users_item_list_sort_by_latest($a, $b) {
  $aTime = strtotime($a->created_at);
  $bTime = strtotime($b->created_at);

  if ($aTime == $bTime) {
    return 0;
  }

  return ($aTime > $bTime) ? -1 : 1;
}

/**
 * Sorts shopping list items by Category.
 */
function users_item_list_sort_by_cat($a, $b) {
  return strcmp($a->category, $b->category);
}

/**
 * Get array of categories to be used as select options.
 * @param array Array of categories from the getCategories api call.
 * @return array Array of categories to be used as select options.
 */
function users_get_options_from_categories($categories) {
  $options = array();
  foreach ($categories as $category) {
    $options[$category['id']] = $category['name'];
  }
  return $options;
}
