<?php
/**
 * @file
 * users.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function users_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'admin_people';
  $view->description = 'Emulates the Drupal user administration page.';
  $view->tag = 'default';
  $view->base_table = 'users';
  $view->human_name = 'People';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'People';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer users';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['columns'] = array(
    'views_bulk_operations' => 'views_bulk_operations',
    'field_computed_name_1' => 'field_computed_name_1',
    'mail' => 'mail',
    'locations_acl_user' => 'locations_acl_user',
    'status' => 'status',
    'rid' => 'rid',
    'access' => 'access',
    'edit_node' => 'edit_node',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'views_bulk_operations' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_computed_name_1' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'mail' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'locations_acl_user' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'status' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'rid' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'access' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'edit_node' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['sticky'] = TRUE;
  /* Field: Bulk operations: User */
  $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['table'] = 'users';
  $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['label'] = '';
  $handler->display->display_options['fields']['views_bulk_operations']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['views_bulk_operations']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '0';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
    'action::user_block_user_action' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 1,
      'label' => 'Block selected users',
    ),
    'action::views_bulk_operations_delete_item' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 1,
      'label' => 'Delete selected users',
    ),
    'action::views_bulk_operations_script_action' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::flag_user_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_modify_action' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 1,
      'label' => 'Set location for selected users',
      'settings' => array(
        'show_all_tokens' => 0,
        'display_values' => array(
          'user::locations_acl_user' => 'user::locations_acl_user',
        ),
      ),
    ),
    'action::views_bulk_operations_user_roles_action' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_argument_selector_action' => array(
      'selected' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'url' => '',
      ),
    ),
    'action::system_send_email_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
  );
  /* Field: Field: Computed Name */
  $handler->display->display_options['fields']['field_computed_name_1']['id'] = 'field_computed_name_1';
  $handler->display->display_options['fields']['field_computed_name_1']['table'] = 'field_data_field_computed_name';
  $handler->display->display_options['fields']['field_computed_name_1']['field'] = 'field_computed_name';
  $handler->display->display_options['fields']['field_computed_name_1']['label'] = 'Username';
  /* Field: User: E-mail */
  $handler->display->display_options['fields']['mail']['id'] = 'mail';
  $handler->display->display_options['fields']['mail']['table'] = 'users';
  $handler->display->display_options['fields']['mail']['field'] = 'mail';
  $handler->display->display_options['fields']['mail']['link_to_user'] = 'user';
  /* Field: Location */
  $handler->display->display_options['fields']['locations_acl_user']['id'] = 'locations_acl_user';
  $handler->display->display_options['fields']['locations_acl_user']['table'] = 'field_data_locations_acl_user';
  $handler->display->display_options['fields']['locations_acl_user']['field'] = 'locations_acl_user';
  $handler->display->display_options['fields']['locations_acl_user']['ui_name'] = 'Location';
  $handler->display->display_options['fields']['locations_acl_user']['label'] = 'Location';
  $handler->display->display_options['fields']['locations_acl_user']['delta_offset'] = '0';
  $handler->display->display_options['fields']['locations_acl_user']['multi_type'] = 'ul';
  /* Field: User: Active */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'users';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  $handler->display->display_options['fields']['status']['not'] = 0;
  /* Field: User: Roles */
  $handler->display->display_options['fields']['rid']['id'] = 'rid';
  $handler->display->display_options['fields']['rid']['table'] = 'users_roles';
  $handler->display->display_options['fields']['rid']['field'] = 'rid';
  $handler->display->display_options['fields']['rid']['type'] = 'ul';
  /* Field: User: Last access */
  $handler->display->display_options['fields']['access']['id'] = 'access';
  $handler->display->display_options['fields']['access']['table'] = 'users';
  $handler->display->display_options['fields']['access']['field'] = 'access';
  $handler->display->display_options['fields']['access']['date_format'] = 'time ago';
  /* Field: User: Edit link */
  $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['table'] = 'users';
  $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['label'] = 'Operations';
  /* Filter criterion: name */
  $handler->display->display_options['filters']['field_computed_name_value_1']['id'] = 'field_computed_name_value_1';
  $handler->display->display_options['filters']['field_computed_name_value_1']['table'] = 'field_data_field_computed_name';
  $handler->display->display_options['filters']['field_computed_name_value_1']['field'] = 'field_computed_name_value';
  $handler->display->display_options['filters']['field_computed_name_value_1']['ui_name'] = 'name';
  $handler->display->display_options['filters']['field_computed_name_value_1']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_computed_name_value_1']['group'] = 1;
  $handler->display->display_options['filters']['field_computed_name_value_1']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_computed_name_value_1']['expose']['operator_id'] = 'name_op';
  $handler->display->display_options['filters']['field_computed_name_value_1']['expose']['label'] = 'Name';
  $handler->display->display_options['filters']['field_computed_name_value_1']['expose']['use_operator'] = TRUE;
  $handler->display->display_options['filters']['field_computed_name_value_1']['expose']['operator'] = 'field_computed_name_value_1_op';
  $handler->display->display_options['filters']['field_computed_name_value_1']['expose']['identifier'] = 'name';
  $handler->display->display_options['filters']['field_computed_name_value_1']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    6 => 0,
    3 => 0,
  );
  /* Filter criterion: User: Name */
  $handler->display->display_options['filters']['uid']['id'] = 'uid';
  $handler->display->display_options['filters']['uid']['table'] = 'users';
  $handler->display->display_options['filters']['uid']['field'] = 'uid';
  $handler->display->display_options['filters']['uid']['operator'] = 'not in';
  $handler->display->display_options['filters']['uid']['value'] = array(
    0 => 0,
  );
  $handler->display->display_options['filters']['uid']['group'] = 1;
  /* Filter criterion: User: Roles */
  $handler->display->display_options['filters']['rid']['id'] = 'rid';
  $handler->display->display_options['filters']['rid']['table'] = 'users_roles';
  $handler->display->display_options['filters']['rid']['field'] = 'rid';
  $handler->display->display_options['filters']['rid']['group'] = 1;
  $handler->display->display_options['filters']['rid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['rid']['expose']['operator_id'] = 'rid_op';
  $handler->display->display_options['filters']['rid']['expose']['label'] = 'Role';
  $handler->display->display_options['filters']['rid']['expose']['use_operator'] = TRUE;
  $handler->display->display_options['filters']['rid']['expose']['operator'] = 'rid_op';
  $handler->display->display_options['filters']['rid']['expose']['identifier'] = 'rid';
  /* Filter criterion: User: Active */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'users';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 'All';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['exposed'] = TRUE;
  $handler->display->display_options['filters']['status']['expose']['operator_id'] = '';
  $handler->display->display_options['filters']['status']['expose']['label'] = 'Active';
  $handler->display->display_options['filters']['status']['expose']['operator'] = 'status_op';
  $handler->display->display_options['filters']['status']['expose']['identifier'] = 'status';
  /* Filter criterion: User: E-mail */
  $handler->display->display_options['filters']['mail']['id'] = 'mail';
  $handler->display->display_options['filters']['mail']['table'] = 'users';
  $handler->display->display_options['filters']['mail']['field'] = 'mail';
  $handler->display->display_options['filters']['mail']['operator'] = 'contains';
  $handler->display->display_options['filters']['mail']['group'] = 1;
  $handler->display->display_options['filters']['mail']['exposed'] = TRUE;
  $handler->display->display_options['filters']['mail']['expose']['operator_id'] = 'mail_op';
  $handler->display->display_options['filters']['mail']['expose']['label'] = 'E-mail';
  $handler->display->display_options['filters']['mail']['expose']['use_operator'] = TRUE;
  $handler->display->display_options['filters']['mail']['expose']['operator'] = 'mail_op';
  $handler->display->display_options['filters']['mail']['expose']['identifier'] = 'mail';
  /* Filter criterion: Location */
  $handler->display->display_options['filters']['locations_acl_user_tid']['id'] = 'locations_acl_user_tid';
  $handler->display->display_options['filters']['locations_acl_user_tid']['table'] = 'field_data_locations_acl_user';
  $handler->display->display_options['filters']['locations_acl_user_tid']['field'] = 'locations_acl_user_tid';
  $handler->display->display_options['filters']['locations_acl_user_tid']['ui_name'] = 'Location';
  $handler->display->display_options['filters']['locations_acl_user_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['locations_acl_user_tid']['expose']['operator_id'] = 'location_op';
  $handler->display->display_options['filters']['locations_acl_user_tid']['expose']['label'] = 'Location';
  $handler->display->display_options['filters']['locations_acl_user_tid']['expose']['operator'] = 'locations_acl_user_tid_op';
  $handler->display->display_options['filters']['locations_acl_user_tid']['expose']['identifier'] = 'location';
  $handler->display->display_options['filters']['locations_acl_user_tid']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['locations_acl_user_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    6 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['locations_acl_user_tid']['reduce_duplicates'] = TRUE;
  $handler->display->display_options['filters']['locations_acl_user_tid']['type'] = 'select';
  $handler->display->display_options['filters']['locations_acl_user_tid']['vocabulary'] = 'locations_acl';
  $handler->display->display_options['filters']['locations_acl_user_tid']['hierarchy'] = 1;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'admin/people/people';
  $handler->display->display_options['menu']['type'] = 'default tab';
  $handler->display->display_options['menu']['title'] = 'People';
  $handler->display->display_options['menu']['description'] = 'Administer Users';
  $handler->display->display_options['menu']['weight'] = '-10';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['tab_options']['type'] = 'normal';
  $handler->display->display_options['tab_options']['title'] = 'People';
  $handler->display->display_options['tab_options']['description'] = 'Administer Users.';
  $handler->display->display_options['tab_options']['weight'] = '-4';
  $handler->display->display_options['tab_options']['name'] = 'management';
  $export['admin_people'] = $view;

  return $export;
}
