<?php

/**
 * @file
 * Shopping list print module template
 *
 * @ingroup print
 */

?>
<style type="text/css">
table.shopping-list {
  font-size: 12px;
  width: 640px;
}
table.shopping-list thead {
  font-size: 22px;
  font-weight: bold;
}
table.shopping-list th {
  color: #000000;
  border-bottom: 0px #ffffff;
}
table.shopping-list tbody {
  border-top: 0px;
}
table.shopping-list td {
  text-align: left;
  padding: 5px 0px 15px 0px;
}
table.shopping-list td.category {
  padding-left: 10px;
}
.list-checkbox {
  border: 1px solid black;
  height: 15px;
  width: 15px;
  display: inline-block;
}
.line-desc {
  font-style: italic;
  margin-top: 3px;
  display: inline-block;
}
.print-source_url {
  display: none;
}
</style>

<div class="print-content">
  <table class="shopping-list">
    <colgroup>
      <col width="5%">
      <col width="60%">
      <col width="35%">
    </colgroup>
    <thead>
      <tr>
        <th colspan="3"><?php echo t('Shopping List'); ?>: <?php print $list->name; ?></th>
      </tr>
    </thead>
    <tbody>
  <?php
  if (!empty($list->items)) {
    foreach ($list->items as $item) {
      $desc = '';
      
      if ($item->note != '-1' && $item->note != '') {
        $desc = $item->note;
      }

      ?>
      <tr>
        <td class="checkbox" width="20px">
          <span class='list-checkbox'></span>
        </td>
        <td class="item-info" valign="middle">
          <span class="line-name"><?php print $item->name; ?></span>
          <?php if($desc != '') {
            ?>
            <br>
            <span class="line-desc"><?php print $desc; ?></span>
            <?php
          }?>        
        </td>
        <td class="category">
          <?php print $item->category; ?>
        </td>
      </tr>
      <?php
    }
  }
  ?>
    </tbody>
  </table><!--shopping-list-->
</div><!--print-content-->


