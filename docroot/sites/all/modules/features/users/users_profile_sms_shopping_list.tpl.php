<?php
/**
 * NOTES
 */

?>
<div class="shopping-list-model-box email-modal share-modal sms-modal">
  <div class="modal-title">Send a link via SMS</div>
  <form id="email-box">
    <div class="email-info">
      Web access on phone is required to view the full list. Standard text message rates from your carrier will apply. You will be sending this SMS message through your e-mail program. We will build everything for you. You will just have to send it.
    </div>

    <div class="mobile-carrier">
      <span class="carrier-label">Mobile carrier:&nbsp;</span>
      <select name="email_to_email">
        <option value='' selected="selected">Select one</option>
        <?php print users_carrier_options(); ?>
      </select>
    </div>

    <div class="recipient-phone">
      <span class="recipients-phone">Recipient's phone:&nbsp;</span>
      <input type="text" name="phone_number" value="" /><br>
      <span class="subnote">Enter phone number including area code. Numbers only, no other characters.</span>
    </div>

    <div class="email-buttons">
      <a href="#" class="orange-link send-mail" target="_blank">Send link</a>
      <a href="#" class="simple-link cancel" onclick="Drupal.CTools.Modal.dismiss();">Cancel</a>
    </div>
  </form>
</div>

<script>
jQuery(document).ready(function($) {
  var eSubject = "Whole Foods Market Shopping List";
  var eTo = "";
  var eFrom = "<?php print $user->mail; ?>";

  update_email();

  $('a.send-mail').click(function() {
    if($(".mobile-carrier select").val() == '') {
      alert('Please select a mobile carrier');
      return false;  
    }
    if ($(".recipient-phone input").val() == '') {
      alert("You must enter an phone number!");
      return false;
    }
	
	Drupal.CTools.Modal.dismiss();
  });

  //Replace this with something for the SMS carrier emailTo stuff
  $(".mobile-carrier select").change(function(){
    if ($(this).val() != '') {
      update_to(true);
    }
  });

  $(".recipient-phone input").change(function(){
    if ($(this).val() != '') {
      update_to();
    }
  });

  function update_to(calledBySelect) {
    if (calledBySelect != true) {
      calledBySelect = false;
    }

    var phone = $(".recipient-phone input").val().replace(/[^\d.]/g, ""); //Get the phone number and leave only digits

    //A little validation
    if (phone.length != 10 && !calledBySelect) {
      alert('You must put in a 10 digit phone number.');
      return false;
    }

    var carrier = $('.mobile-carrier select').val();//Gotta load the carrier domain
    eTo = phone + '@' + carrier; //Build the email to address
    update_email(); //Make sure all of this gets updated
  }

  function update_email() {
    var ref = Drupal.settings.WholeFoods.ShoppingList.current;
    var body = Drupal.settings.WholeFoods.ShoppingList[ref]["sms"] + ' '; <?php // set in users.module -> users_block_view ?>

    $('.email-buttons a.send-mail').attr( 'href', 'mailto:' + encodeURIComponent(eTo) + '?subject=' + encodeURIComponent(eSubject) + '&body=' + encodeURIComponent(body));
    //$('.email-buttons a.send-mail').attr( 'href', 'mailto:' + encodeURIComponent(eTo) + '?subject=' + encodeURIComponent(eSubject) + '&body=' + encodeURIComponent(body) + '&from=' + encodeURIComponent(eFrom) );
  }
});
</script>
