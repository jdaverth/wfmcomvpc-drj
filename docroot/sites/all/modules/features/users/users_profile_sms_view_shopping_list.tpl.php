<?php
/**
 * NOTES
 */
?>
<table class="sms-content">
  <caption><?php echo t('Shopping List'); ?>: <?php print $list->name; ?></caption>
  <thead>
    <tr>
      <th scope="col"><?php echo t('Item Name'); ?></th>
      <th scope="col"><?php echo t('Department'); ?></th>
    </tr>
  </thead> 
  <tbody>
  <?php
  if (!empty($list->items)) {
    foreach ($list->items as $item) {
      $desc = '';
      
      if ($item->description != '-1' && $item->description != '') {
        $desc = $item->description;
      }

      ?>
      <tr>
        <td valign="middle">
          <span class='line-name'><?php print $item->note; ?></span>
          <?php if($desc != '') {
            ?>
            <br>
            <span class='line-desc'><?php print nl2br(stripcslashes($desc)); ?></span>
            <?php
          }?> 
        </td>
        <td valign="middle">
          <span class='line-category'><?php print $item->category; ?></span>
        </td>
      </tr>
      <?php
    }
  }
  ?>
  </tbody>
</table><!--sms-content-->