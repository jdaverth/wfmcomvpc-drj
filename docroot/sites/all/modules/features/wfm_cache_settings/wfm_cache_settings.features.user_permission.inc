<?php
/**
 * @file
 * wfm_cache_settings.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function wfm_cache_settings_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'purge on-screen'.
  $permissions['purge on-screen'] = array(
    'name' => 'purge on-screen',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'acquia_purge',
  );

  // Exported permission: 'use manual purge blocks'.
  $permissions['use manual purge blocks'] = array(
    'name' => 'use manual purge blocks',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'acquia_purge',
  );

  return $permissions;
}
