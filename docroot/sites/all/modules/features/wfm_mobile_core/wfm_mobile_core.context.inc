<?php
/**
 * @file
 * wfm_mobile_core.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function wfm_mobile_core_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'mobile_global';
  $context->description = 'Site-wide blocks for mobile';
  $context->tag = 'Global';
  $context->conditions = array(
    'theme' => array(
      'values' => array(
        'wholefoods_mobile' => 'wholefoods_mobile',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'wfm_welcome_block-wfm_welcome_mobile_block' => array(
          'module' => 'wfm_welcome_block',
          'delta' => 'wfm_welcome_mobile_block',
          'region' => 'userzone',
          'weight' => '0',
        ),
        'menu-menu-mobile-header-menu' => array(
          'module' => 'menu',
          'delta' => 'menu-mobile-header-menu',
          'region' => 'header',
          'weight' => '0',
        ),
        'store-mobile-store-indicator' => array(
          'module' => 'store',
          'delta' => 'mobile-store-indicator',
          'region' => 'header',
          'weight' => '1',
        ),
        'menu-menu-mobile-footer' => array(
          'module' => 'menu',
          'delta' => 'menu-mobile-footer',
          'region' => 'footer',
          'weight' => 0,
        ),
        'menu-menu-mobile-footer-2' => array(
          'module' => 'menu',
          'delta' => 'menu-mobile-footer-2',
          'region' => 'footer',
          'weight' => 5,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Global');
  t('Site-wide blocks for mobile');
  $export['mobile_global'] = $context;

  return $export;
}
