<?php
/**
 * @file
 * wfm_mobile_core.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function wfm_mobile_core_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-mobile-footer.
  $menus['menu-mobile-footer'] = array(
    'menu_name' => 'menu-mobile-footer',
    'title' => 'Mobile Footer',
    'description' => '',
  );
  // Exported menu: menu-mobile-footer-2.
  $menus['menu-mobile-footer-2'] = array(
    'menu_name' => 'menu-mobile-footer-2',
    'title' => 'Mobile Footer_2',
    'description' => '',
  );
  // Exported menu: menu-mobile-header-menu.
  $menus['menu-mobile-header-menu'] = array(
    'menu_name' => 'menu-mobile-header-menu',
    'title' => 'Mobile Header Menu',
    'description' => 'Show menu icons in Header.',
  );
  // Exported menu: menu-mobile-menu.
  $menus['menu-mobile-menu'] = array(
    'menu_name' => 'menu-mobile-menu',
    'title' => 'Mobile Menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Mobile Footer');
  t('Mobile Footer_2');
  t('Mobile Header Menu');
  t('Mobile Menu');
  t('Show menu icons in Header.');

  return $menus;
}
