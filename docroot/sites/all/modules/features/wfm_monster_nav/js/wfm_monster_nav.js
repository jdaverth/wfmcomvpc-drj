/**
 * @file
 * Monster Navigation javascript handlers
 */

Drupal.behaviors.wfm_monster_nav = {
  attach: (function ($) {
    return function() {
      var callback = function(storeinfo) {
        storeinfo = Drupal.WholeFoods.removeStoreNidKey(storeinfo);
        $('#menu-link-unique-to-your-store').attr('href', '/stores/' + storeinfo.path);
      };

      var storenid = $.cookie('local_store');
      if (storenid) {
        Drupal.WholeFoods.getStoreInfo(storenid, callback);
      }
    };
  })(jQuery)
};