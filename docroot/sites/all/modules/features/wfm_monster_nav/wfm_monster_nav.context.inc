<?php
/**
 * @file
 * wfm_monster_nav.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function wfm_monster_nav_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'monster_navigation';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'wfm_monster_nav-wfm-monster-navigation' => array(
          'module' => 'wfm_monster_nav',
          'delta' => 'wfm-monster-navigation',
          'region' => 'navigation',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['monster_navigation'] = $context;

  return $export;
}
