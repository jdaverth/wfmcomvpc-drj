<?php
/**
 * @file
 * wfm_monster_nav.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wfm_monster_nav_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
}
