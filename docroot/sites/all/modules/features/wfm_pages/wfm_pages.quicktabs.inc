<?php
/**
 * @file
 * wfm_pages.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function wfm_pages_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'mobile_recipe_details';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Mobile Recipe Details';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'recipe_details_mobile',
      'display' => 'default',
      'args' => '',
      'title' => 'Summary',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'recipe_details_mobile',
      'display' => 'mobile_recipe_ingredients',
      'args' => '',
      'title' => 'Ingredients',
      'weight' => '-99',
      'type' => 'view',
    ),
    2 => array(
      'vid' => 'recipe_details_mobile',
      'display' => 'mobile_recipe_method',
      'args' => '',
      'title' => 'Method',
      'weight' => '-98',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Ingredients');
  t('Method');
  t('Mobile Recipe Details');
  t('Summary');

  $export['mobile_recipe_details'] = $quicktabs;

  return $export;
}
