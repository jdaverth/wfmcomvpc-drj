<?php
/**
 * @file
 * wfm_pages.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function wfm_pages_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'recipe_details_mobile';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Recipe Details Mobile';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Summary';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Hero Image */
  $handler->display->display_options['fields']['field_hero_image']['id'] = 'field_hero_image';
  $handler->display->display_options['fields']['field_hero_image']['table'] = 'field_data_field_hero_image';
  $handler->display->display_options['fields']['field_hero_image']['field'] = 'field_hero_image';
  $handler->display->display_options['fields']['field_hero_image']['label'] = '';
  $handler->display->display_options['fields']['field_hero_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_hero_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_hero_image']['settings'] = array(
    'image_style' => 'mobile_300x190',
    'image_link' => '',
  );
  /* Field: Content: Rating */
  $handler->display->display_options['fields']['field_recipe_rating']['id'] = 'field_recipe_rating';
  $handler->display->display_options['fields']['field_recipe_rating']['table'] = 'field_data_field_recipe_rating';
  $handler->display->display_options['fields']['field_recipe_rating']['field'] = 'field_recipe_rating';
  $handler->display->display_options['fields']['field_recipe_rating']['label'] = '';
  $handler->display->display_options['fields']['field_recipe_rating']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_recipe_rating']['settings'] = array(
    'thousand_separator' => '',
    'decimal_separator' => '.',
    'scale' => '2',
    'prefix_suffix' => 1,
  );
  /* Field: Content: Serves */
  $handler->display->display_options['fields']['field_serves']['id'] = 'field_serves';
  $handler->display->display_options['fields']['field_serves']['table'] = 'field_data_field_serves';
  $handler->display->display_options['fields']['field_serves']['field'] = 'field_serves';
  $handler->display->display_options['fields']['field_serves']['label'] = '';
  $handler->display->display_options['fields']['field_serves']['element_label_colon'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Field: Field: Special Diets */
  $handler->display->display_options['fields']['field_special_diet']['id'] = 'field_special_diet';
  $handler->display->display_options['fields']['field_special_diet']['table'] = 'field_data_field_special_diet';
  $handler->display->display_options['fields']['field_special_diet']['field'] = 'field_special_diet';
  $handler->display->display_options['fields']['field_special_diet']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_special_diet']['delta_offset'] = '0';
  /* Field: Content: Nutritional Info */
  $handler->display->display_options['fields']['field_nutritional_info']['id'] = 'field_nutritional_info';
  $handler->display->display_options['fields']['field_nutritional_info']['table'] = 'field_data_field_nutritional_info';
  $handler->display->display_options['fields']['field_nutritional_info']['field'] = 'field_nutritional_info';
  $handler->display->display_options['fields']['field_nutritional_info']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_nutritional_info']['delta_offset'] = '0';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'recipe' => 'recipe',
  );

  /* Display: Summary */
  $handler = $view->new_display('block', 'Summary', 'mobile_recipe_summary');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['block_description'] = 'Recipe Summary';

  /* Display: Ingredients */
  $handler = $view->new_display('block', 'Ingredients', 'mobile_recipe_ingredients');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Ingredients';
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Serves */
  $handler->display->display_options['fields']['field_serves']['id'] = 'field_serves';
  $handler->display->display_options['fields']['field_serves']['table'] = 'field_data_field_serves';
  $handler->display->display_options['fields']['field_serves']['field'] = 'field_serves';
  $handler->display->display_options['fields']['field_serves']['label'] = '';
  $handler->display->display_options['fields']['field_serves']['element_label_colon'] = FALSE;
  /* Field: Content: Ingredients */
  $handler->display->display_options['fields']['field_ingredients']['id'] = 'field_ingredients';
  $handler->display->display_options['fields']['field_ingredients']['table'] = 'field_data_field_ingredients';
  $handler->display->display_options['fields']['field_ingredients']['field'] = 'field_ingredients';
  $handler->display->display_options['fields']['field_ingredients']['label'] = '';
  $handler->display->display_options['fields']['field_ingredients']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_ingredients']['delta_offset'] = '0';
  $handler->display->display_options['block_description'] = 'Recipe Ingredients';

  /* Display: Method */
  $handler = $view->new_display('block', 'Method', 'mobile_recipe_method');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Method';
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Serves */
  $handler->display->display_options['fields']['field_serves']['id'] = 'field_serves';
  $handler->display->display_options['fields']['field_serves']['table'] = 'field_data_field_serves';
  $handler->display->display_options['fields']['field_serves']['field'] = 'field_serves';
  $handler->display->display_options['fields']['field_serves']['label'] = '';
  $handler->display->display_options['fields']['field_serves']['element_label_colon'] = FALSE;
  /* Field: Content: Method */
  $handler->display->display_options['fields']['field_recipe_directions']['id'] = 'field_recipe_directions';
  $handler->display->display_options['fields']['field_recipe_directions']['table'] = 'field_data_field_recipe_directions';
  $handler->display->display_options['fields']['field_recipe_directions']['field'] = 'field_recipe_directions';
  $handler->display->display_options['fields']['field_recipe_directions']['label'] = '';
  $handler->display->display_options['fields']['field_recipe_directions']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_recipe_directions']['delta_offset'] = '0';
  $handler->display->display_options['block_description'] = 'Recipe Method';
  $export['recipe_details_mobile'] = $view;

  return $export;
}
