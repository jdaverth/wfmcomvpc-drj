<?php

/**
 * Implements hook_views_data_alter().
 */
function wfm_search_views_data_alter(&$data) {
  $data['sponsored_search_result']['changed']['field']['handler'] = 'views_handler_field_date';
  $data['sponsored_search_result']['nid']['relationship'] = array(
    'base' => 'node',
    'base field' => 'nid',
    'field' => 'nid',
    'handler' => 'views_handler_relationship',
    'label' => t('Sponsored node'),
    'title' => t('Sponsored node'),
    'help' => t('Content associated with the sponsored search result.'),
  );
}
