<?php

/**
 * Custom entity form.
 */
function sponsored_search_result_form($form, &$form_state, $entity = NULL) {
  $form['term'] = array(
    '#title' => t('Search term'),
    '#type' => 'textfield',
    '#default_value' => isset($entity->term) ? $entity->term : '',
    '#required' => TRUE,
  );
  $form['nid'] = array(
    '#title' => t('Promoted node'),
    '#type' => 'textfield',
    '#description' => t('You can enter either a path or a full link to the promoted node.'),
    '#default_value' => isset($entity->nid) ? drupal_get_path_alias('node/' . $entity->nid) : '',
    '#required' => TRUE,
  );
  $form['changed'] = array(
    '#type' => 'value',
    '#default_value' => REQUEST_TIME,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => isset($entity->id) ? t('Update result') : t('Save result'),
    '#weight' => 50,
  );
  return $form;
}

/**
 * Validation handler for the sponsored result add/edit form.
 */
function sponsored_search_result_form_validate($form, &$form_state) {
  $path = drupal_lookup_path('source', ltrim(parse_url($form_state['values']['nid'], PHP_URL_PATH), '/'));
  if (!$path) {
    form_set_error('nid', t('"@path" was not recognized as a valid node path.', array('@path' => $form_state['values']['nid'])));
  }
  else {
    $form_state['values']['nid'] = preg_replace('/[^0-9]/','', $path);
  }
}

/**
 * Submit handler for the sponsored result add/edit form.
 */
function sponsored_search_result_form_submit($form, &$form_state) {
  $form_state['values']['term'] = strtolower($form_state['values']['term']);
  $entity = entity_ui_form_submit_build_entity($form, $form_state);
  $entity->save();
  drupal_set_message(t('The sponsored search result has been saved.'));
  $form_state['redirect'] = 'admin/config/search/sponsored';
}
