<?php
/**
 * @file
 * wfm_search.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wfm_search_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_search_api_autocomplete_search().
 */
function wfm_search_default_search_api_autocomplete_search() {
  $items = array();
  $items['search_api_page_global_search'] = entity_import('search_api_autocomplete_search', '{
    "machine_name" : "search_api_page_global_search",
    "name" : "Global Search",
    "index_id" : "global",
    "type" : "search_api_page",
    "enabled" : "1",
    "options" : {
      "custom" : { "page_id" : "global_search" },
      "result count" : true,
      "fields" : []
    },
    "metatags" : [],
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_index().
 */
function wfm_search_default_search_api_index() {
  $items = array();
  $items['blogs'] = entity_import('search_api_index', '{
    "name" : "Blogs",
    "machine_name" : "blogs",
    "description" : null,
    "server" : "acquia_search",
    "item_type" : "node",
    "options" : {
      "datasource" : { "bundles" : [ "blog_post" ] },
      "index_directly" : 0,
      "cron_limit" : "50",
      "fields" : {
        "author" : { "type" : "integer", "entity_type" : "user" },
        "body:summary" : { "type" : "text" },
        "body:value" : { "type" : "text" },
        "field_blog" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "node" },
        "search_api_access_node" : { "type" : "list\\u003Cstring\\u003E" },
        "search_api_language" : { "type" : "string" },
        "status" : { "type" : "boolean" },
        "title" : { "type" : "text" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_node_status" : { "status" : 1, "weight" : "-50", "settings" : [] },
        "search_api_alter_node_access" : { "status" : 1, "weight" : "-49", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "-48", "settings" : { "fields" : [] } },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "-47", "settings" : { "mode" : "full" } },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "-46", "settings" : [] },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "-45", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : { "title" : true, "body:value" : true, "body:summary" : true } }
        },
        "search_api_html_filter" : {
          "status" : 1,
          "weight" : "10",
          "settings" : {
            "fields" : { "title" : true, "body:value" : true, "body:summary" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 1,
          "weight" : "20",
          "settings" : {
            "fields" : { "title" : true, "body:value" : true, "body:summary" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[-]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "title" : true, "body:value" : true, "body:summary" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "metatags" : [],
    "rdf_mapping" : []
  }');
  $items['global'] = entity_import('search_api_index', '{
    "name" : "Global",
    "machine_name" : "global",
    "description" : null,
    "server" : "acquia_search",
    "item_type" : "node",
    "options" : {
      "index_directly" : 0,
      "cron_limit" : "50",
      "fields" : {
        "author" : { "type" : "integer", "entity_type" : "user" },
        "body:value" : { "type" : "text" },
        "changed" : { "type" : "date" },
        "created" : { "type" : "date" },
        "is_new" : { "type" : "boolean" },
        "language" : { "type" : "string" },
        "nid" : { "type" : "integer" },
        "search_api_access_node" : { "type" : "list\\u003Cstring\\u003E" },
        "search_api_language" : { "type" : "string" },
        "status" : { "type" : "boolean" },
        "title" : { "type" : "text" },
        "type" : { "type" : "string" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_node_status" : { "status" : 1, "weight" : "-50", "settings" : [] },
        "search_api_alter_bundle_filter" : {
          "status" : 1,
          "weight" : "-49",
          "settings" : {
            "default" : "0",
            "bundles" : {
              "blog" : "blog",
              "blog_post" : "blog_post",
              "core_value" : "core_value",
              "department" : "department",
              "department_article" : "department_article",
              "local_vendor" : "local_vendor",
              "page" : "page",
              "product" : "product",
              "product_line" : "product_line",
              "recipe" : "recipe",
              "service" : "service",
              "special_diet" : "special_diet",
              "store" : "store"
            }
          }
        },
        "search_api_alter_node_access" : { "status" : 1, "weight" : "-48", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "-47", "settings" : { "fields" : [] } },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "-46", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "-45", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "-44", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : { "title" : true, "body:value" : true } }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : { "title" : true, "body:value" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "title" : true, "body:value" : true },
            "spaces" : "[^\\\\p{L}\\\\p{N}^\\u0027]",
            "ignorable" : "[-]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "title" : true, "body:value" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "metatags" : [],
    "rdf_mapping" : []
  }');
  $items['location_search_index'] = entity_import('search_api_index', '{
    "name" : "Location Search Index",
    "machine_name" : "location_search_index",
    "description" : null,
    "server" : "acquia_search",
    "item_type" : "node",
    "options" : {
      "datasource" : { "bundles" : [ "store" ] },
      "index_directly" : 0,
      "cron_limit" : "50",
      "fields" : {
        "author" : { "type" : "integer", "entity_type" : "user" },
        "field_365_store" : { "type" : "boolean" },
        "field_geo_data:latlon" : { "type" : "string", "real_type" : "location" },
        "field_postal_address:administrative_area" : { "type" : "text" },
        "nid" : { "type" : "integer" },
        "search_api_access_node" : { "type" : "list\\u003Cstring\\u003E" },
        "search_api_language" : { "type" : "string" },
        "status" : { "type" : "boolean" },
        "title" : { "type" : "text" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_node_access" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_node_status" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : { "title" : true, "field_postal_address:administrative_area" : true } }
        },
        "search_api_html_filter" : {
          "status" : 1,
          "weight" : "10",
          "settings" : {
            "fields" : { "title" : true, "field_postal_address:administrative_area" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "title" : true, "field_postal_address:administrative_area" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[-]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "title" : true, "field_postal_address:administrative_area" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "metatags" : [],
    "rdf_mapping" : []
  }');
  $items['products'] = entity_import('search_api_index', '{
    "name" : "Products",
    "machine_name" : "products",
    "description" : null,
    "server" : "acquia_search",
    "item_type" : "node",
    "options" : {
      "datasource" : { "bundles" : [ "product" ] },
      "index_directly" : 0,
      "cron_limit" : "50",
      "fields" : {
        "author" : { "type" : "integer", "entity_type" : "user" },
        "body:summary" : { "type" : "text" },
        "body:value" : { "type" : "text" },
        "changed" : { "type" : "date" },
        "created" : { "type" : "date" },
        "field_price" : { "type" : "text" },
        "field_product_certification" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "node" },
        "field_product_line" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "node" },
        "field_recipes" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "node" },
        "field_special_diet" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "node" },
        "nid" : { "type" : "integer" },
        "search_api_access_node" : { "type" : "list\\u003Cstring\\u003E" },
        "search_api_language" : { "type" : "string" },
        "status" : { "type" : "boolean" },
        "title" : { "type" : "text" },
        "type" : { "type" : "string" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_node_access" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_node_status" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : { "title" : true } }
        },
        "search_api_html_filter" : {
          "status" : 1,
          "weight" : "10",
          "settings" : {
            "fields" : { "title" : true },
            "title" : 1,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "title" : true, "body:value" : true, "body:summary" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[-]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "title" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "metatags" : [],
    "rdf_mapping" : []
  }');
  $items['recipes'] = entity_import('search_api_index', '{
    "name" : "Recipes",
    "machine_name" : "recipes",
    "description" : null,
    "server" : "acquia_search",
    "item_type" : "node",
    "options" : {
      "datasource" : { "bundles" : [ "recipe" ] },
      "index_directly" : 0,
      "cron_limit" : "50",
      "fields" : {
        "author" : { "type" : "integer", "entity_type" : "user" },
        "body:value" : { "type" : "text" },
        "field_cook_time" : { "type" : "integer" },
        "field_difficulty" : { "type" : "string" },
        "field_is_featured" : { "type" : "boolean" },
        "field_prep_time" : { "type" : "integer" },
        "field_recipe_category" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_recipe_course" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_recipe_cusine" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_recipe_featured_in" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_recipe_main_ingredient" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_recipe_occasions" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_recipe_rating" : { "type" : "decimal" },
        "field_serve_count" : { "type" : "integer" },
        "field_special_diet" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "node" },
        "nid" : { "type" : "integer" },
        "search_api_access_node" : { "type" : "list\\u003Cstring\\u003E" },
        "search_api_language" : { "type" : "string" },
        "status" : { "type" : "boolean" },
        "title" : { "type" : "text" },
        "type" : { "type" : "string" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_node_status" : { "status" : 1, "weight" : "-50", "settings" : [] },
        "search_api_alter_node_access" : { "status" : 1, "weight" : "-49", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "-48", "settings" : { "fields" : [] } },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "-47", "settings" : { "mode" : "full" } },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "-46", "settings" : [] },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "-45", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : {
              "title" : true,
              "field_special_diet" : true,
              "field_recipe_category" : true,
              "field_recipe_course" : true,
              "field_recipe_cusine" : true,
              "field_recipe_featured_in" : true,
              "field_recipe_main_ingredient" : true,
              "field_recipe_occasions" : true,
              "body:value" : true,
              "field_nutrional_info:value" : true
            }
          }
        },
        "search_api_html_filter" : {
          "status" : 1,
          "weight" : "10",
          "settings" : {
            "fields" : { "title" : true, "body:value" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "title" : true, "body:value" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[-]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "title" : true, "body:value" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "metatags" : [],
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_page().
 */
function wfm_search_default_search_api_page() {
  $items = array();
  $items['blog_pages'] = entity_import('search_api_page', '{
    "index_id" : "blogs",
    "path" : "blog\\/search",
    "name" : "Blog Pages",
    "machine_name" : "blog_pages",
    "description" : "",
    "options" : {
      "mode" : "terms",
      "fields" : { "title" : "title", "body:value" : "body:value" },
      "per_page" : "20",
      "result_page_search_form" : 1,
      "get_per_page" : 1,
      "view_mode" : "search_api_page_result"
    },
    "enabled" : "1",
    "metatags" : [],
    "rdf_mapping" : []
  }');
  $items['global_search'] = entity_import('search_api_page', '{
    "index_id" : "global",
    "path" : "site_search",
    "name" : "Global Search",
    "machine_name" : "global_search",
    "description" : "",
    "options" : {
      "mode" : "terms",
      "fields" : { "title" : "title", "body:value" : "body:value" },
      "per_page" : "20",
      "result_page_search_form" : 1,
      "get_per_page" : 1,
      "view_mode" : "search_api_page_result"
    },
    "enabled" : "1",
    "metatags" : [],
    "rdf_mapping" : []
  }');
  $items['product_search'] = entity_import('search_api_page', '{
    "index_id" : "products",
    "path" : "products\\/search",
    "name" : "Product Search",
    "machine_name" : "product_search",
    "description" : "",
    "options" : {
      "mode" : "terms",
      "fields" : {
        "title" : "title",
        "field_price" : "field_price",
        "body:value" : "body:value",
        "body:summary" : "body:summary"
      },
      "per_page" : "20",
      "result_page_search_form" : 1,
      "get_per_page" : 1,
      "view_mode" : "search_api_page_result"
    },
    "enabled" : "1",
    "metatags" : [],
    "rdf_mapping" : []
  }');
  $items['recipe_search'] = entity_import('search_api_page', '{
    "index_id" : "recipes",
    "path" : "recipe\\/search",
    "name" : "Recipe Search",
    "machine_name" : "recipe_search",
    "description" : "",
    "options" : {
      "mode" : "terms",
      "fields" : [],
      "per_page" : "20",
      "result_page_search_form" : 1,
      "get_per_page" : 1,
      "view_mode" : "search_api_page_result"
    },
    "enabled" : "1",
    "metatags" : [],
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function wfm_search_default_search_api_server() {
  $items = array();
  $items['acquia_search'] = entity_import('search_api_server', '{
    "name" : "Acquia Search",
    "machine_name" : "acquia_search",
    "description" : "",
    "class" : "acquia_search_service",
    "options" : {
      "clean_ids" : true,
      "scheme" : "http",
      "host" : "useast1-c5.acquia-search.com",
      "port" : "80",
      "path" : "\\/solr\\/CDIV-25325",
      "edismax" : 1,
      "modify_acquia_connection" : false,
      "acquia_override_subscription" : {
        "acquia_override_auto_switch" : 1,
        "acquia_override_selector" : "CDIV-25325",
        "acquia_override_subscription_id" : "CDIV-25325",
        "acquia_override_subscription_key" : "294a8837eb17041d598f2626353fae2c",
        "acquia_override_subscription_corename" : "CDIV-25325"
      },
      "http_user" : "",
      "http_pass" : "",
      "excerpt" : 1,
      "retrieve_data" : 1,
      "highlight_data" : 0,
      "http_method" : "AUTO",
      "derived_key" : "87d8b686bec82fdb27185ace87e53d5d690fad4b"
    },
    "enabled" : "1",
    "metatags" : [],
    "rdf_mapping" : []
  }');
  return $items;
}
