<?php

class WFMMigrateEntityFieldQuery extends EntityFieldQuery
{
// Extra added fields to the query
  private $addedFields = array();

  /**
   * Finishes the query.
   *
   * Adds tags, metaData, range and returns the requested list or count.
   *
   * @param SelectQuery $select_query
   *   A SelectQuery which has entity_type, entity_id, revision_id and bundle
   *   fields added.
   * @param $id_key
   *   Which field's values to use as the returned array keys.
   *
   * @return
   *   See EntityFieldQuery::execute().
   */
  function finishQuery($select_query, $id_key = 'entity_id')
  {
    $fields = $select_query->getFields();
    $base_table = $fields['entity_id']['table'];
    foreach ($this->addedFields as $addedField) {
      $table_alias = '';
      if (!empty($addedField['field_name'])) {
        $tables = $select_query->getTables();
        foreach ($tables as $table) {
          if ($table['table'] == 'field_data_' . $addedField['field_name']) {
            $table_alias = $table['alias'];
            break;
          }
        }
        if (empty($table_alias)) {
          $table_alias = 'field_data_' . $addedField['field_name'] . count($tables);
          $select_query->addJoin(
            'LEFT OUTER',
            'field_data_' . $addedField['field_name'],
            $table_alias,
            $table_alias . '.entity_type = ' . $base_table . '.entity_type AND ' . $table_alias . '.entity_id = ' . $base_table . '.entity_id'
          );
        }

        $column = $addedField['field_name'] . '_' . $addedField['column'];
        $column_alias = $addedField['field_name'] . '_' . $addedField['column_alias'];
      } else {
        $column = $addedField['column'];
        $column_alias = $addedField['column_alias'];
      }
      if (empty($table_alias)) {
        $table_alias = $base_table;
      }
      $select_query->addField($table_alias, $column, $column_alias);
    }

    $return = parent::finishQuery($select_query, $id_key);
    if (is_array($this->ordered_results)) {
      foreach ($this->ordered_results as $partial_entity) {
        $return[$partial_entity->entity_type][$partial_entity->$id_key]->extraFields = $partial_entity;
      }
    }
    return $return;
  }

  public function addExtraField($field_name, $column, $column_alias = NULL)
  {
    $this->addedFields[] = array(
      'field_name' => $field_name,
      'column' => $column,
      'column_alias' => $column_alias,
    );
    return $this;
  }
}

