<?php
/**
 * @file
 * Contains the class definitions migrating store content from WFM recipe db
 * to Drupal nodes.
 */

class WFMMigrateRecipesList extends WFMApi {
  const GET_LISTS_URL_FULL = 'recipes/modified/0';
  const GET_LISTS_URL_SIMPLIFIED = 'recipes';
  const GET_LISTS_URL = 'recipes/modified/:time';
  const GET_RECIPE_URL = 'recipes/view/:rid';

  static $recipe_list = array();

  static public function getFoundRecipes() {
    return self::$recipe_list;
  }

  /**
   * Get list of recipes modified starting from $timestamp
   */
  static public function getList($timestamp = NULL) {
    try{
      $params = array(
        'resource' => self::GET_LISTS_URL,
        'format' => 'json',
        'unlimit' => 1,
        'get' => array(),
      );

      $list = self::_curl($params, array(':time' => $timestamp ? $timestamp : (REQUEST_TIME - 604800)));
      $list = (array) $list;

      foreach($list as $item) {
        self::$recipe_list[$item->id] = $item;
      }
    }
    catch (Exception $e) {
      throw $e;
    }

    return $list;
  }

  /**
   */
  static public function getListFull() {
    try{
      $params = array(
        'resource' => self::GET_LISTS_URL_FULL,
        'format' => 'json',
        'unlimit' => 1,
        'get' => array(),
      );
      $list = self::_curl($params, array());
      $list = (array) $list;
    }
    catch (Exception $e) {
      throw $e;
    }

    return $list;
  }

  static public function getRecipe($id) {
    if (array_key_exists($id, self::$recipe_list)) {
      return self::$recipe_list[$id];
    }

    try{
      $params = array(
                      'resource' => self::GET_RECIPE_URL,
                      'format' => 'json',
                      'get' => array(),
                      );
      $list = self::_curl($params, array(':rid' => $id));
    }
    catch (Exception $e) {
      throw $e;
    }

    self::$recipe_list[$id] = $list[0];
    return self::$recipe_list[$id];
  }

  static public function getTotalCount() {
    $list = self::getList();
    return count($list) ;
  }

  static public function getIdList($timestamp = NULL) {
    $list = self::getList($timestamp);
    $id_list = array();
    foreach ($list as $value) {
      $id_list[] = $value->id;
    }

    return $id_list;
  }


  static public function getListSimplified() {
    $items = array();
    try{
      $params = array(
        'resource' => self::GET_LISTS_URL_SIMPLIFIED,
        'format' => 'json',
        'get' => array(
          'page' => 1,
          'limit' => 500, // api was tested even with 2500+ limit. (less pages = less possible problems)
        ),
      );
      do {
        $list = self::_curl($params, array(), $query_info);
        $list = (array) $list;
        $items = array_merge($items, $list);
        $params['get']['page']++;
      } while ($query_info->pages > $query_info->page);

      // check if we get correct number of items
      if (count($items) <> $query_info->count) {
        drupal_set_message('Can\'t fetch recipes full list.', 'error');
        return false;
      }
    }
    catch (Exception $e) {
      throw $e;
    }


    return $items;
  }

  static public function getIdListFull() {
    $list = self::getListSimplified();
    $id_list = array();
    foreach ($list as $value) {
      $id_list[] = $value->id;
    }

    return $id_list;
  }}

define('WFMAPI_FLAG_ID_SPECIAL_DIET', '1');

class RecipeMigrateList extends MigrateListJSON {
  protected function getIDsFromJSON(array $data) {
    $migration = RecipeMigration::currentMigration();
    $filter = $migration->getRecipeList();

    $filtered_list = array();

    if (count($filter)) {
      // we will download all recipes updated after oldest receipe in filter list.
      // using 1 day overlap to avoid problems connected with timezone settings.
      $id_list = WFMMigrateRecipesList::getIdList(max(0, strtotime(min($filter)) - 86400));
      foreach($id_list as $id) {
        if (!empty($filter[$id])) {
          $filtered_list[] = $id;
        }
      }
    }

    return $filtered_list;
  }

  public function getIdList() {
    $data = array();
    return $this->getIDsFromJSON($data);
  }
}

class RecipeMigrateItem extends MigrateItemJSON {
  public function getItem($id) {
    try {
      $list = WFMMigrateRecipesList::getRecipe($id);
    } catch (Exception $e) {
      $migration = Migration::currentMigration();
      $message =  t('Loading of !objecturl failed:', array('!objecturl' => $item_url));
      $migration->getMap()->saveMessage(
                                        array($id), $message, MigrationBase::MESSAGE_ERROR);
      return NULL;
    }
    return $list;
  }
}

class RecipeMigrateSource extends MigrateSourceList {
}

class RecipeMigration extends Migration {
  private $nodes_list = array();
  private $recipe_list = array();

  public function getRecipeList() {
    return $this->recipe_list;
  }

  public function preImport() {
    drupal_set_message("Starting pre import process...");
    parent::preImport();

    // get existing nodes with field_rid & field_recipe_source_modified values
    $query = new WFMMigrateEntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'recipe')
      ->propertyCondition('status', 1)
      ->fieldCondition('field_rid', 'value', 0, '>')
      ->addExtraField('field_rid', 'value', 'value')
      ->addExtraField('field_recipe_source_modified', 'value', 'value')
      ->addMetaData('account', user_load(1));
    $result = $query->execute();
    foreach ($result['node'] as $node_partial) {
      $this->nodes_list[$node_partial->extraFields->field_rid_value] = array(
        'nid' => $node_partial->nid,
        'modified' => $node_partial->extraFields->field_recipe_source_modified_value,
      );
    }

    // get recipes list from api server
    $list = WFMMigrateRecipesList::getListSimplified();
    if (count($list)) {
      foreach($list as $item) {
        $this->recipe_list[$item->id] = $item->modified;
      }

      // build lists: unpublish, update/insert
      $unpublish_list = array();
      $modified_min = NULL;
      foreach ($this->nodes_list as $node_rid => $node_info) {
        // item is present in drupal and not present in api list. need to unpublish node
        if (!isset($this->recipe_list[$node_rid])) {
          $unpublish_list[] = $node_info['nid'];
        }
        // item is present in both lists with equal "modified" field. need to skip this item
        elseif ($node_info['modified'] == $this->recipe_list[$node_rid]) {
          unset($this->recipe_list[$node_rid]);
        }
      }

      // unpublish all nodes from list
      if ($count = count($unpublish_list)) {
        $nodes = node_load_multiple($unpublish_list);
        foreach ($nodes as $node) {
          // set status property to 1
          $node->status = 0;
          // re-save the node
          node_save($node);
        }

        drupal_set_message(format_plural($count, 'Unpublished 1 old node.', 'Unpublished @count old nodes.'), 'completed');
      }
      else {
        drupal_set_message('No old nodes to unpublish.', 'completed');
      }
    }
  }

  /*
   * Ensure that node will have published state after import
   */
  public function prepare($node, stdClass $row) {
    $node->status = 1;
  }

  /**
   * Do anything that needs to happen to a node/row right after migration.
   *
   * Add the node alias from the title.
   *
   * Add old redirects.
   */
  public function complete($node, stdClass $row) {
    if (!isset($node->title))
      return;

    // aliases created automatically, but here we still need to setup redirects
    $redirects = array(
      'recipe/' . $row->id,
      'recipes/' . $row->id,
    );

    foreach ($redirects as $v) {
      $redirect = redirect_load_by_source($v);

      if (is_object($redirect)) {
        $redirect->redirect = 'node/' . $node->nid;
      }
      else {
        $redirect = new stdClass();
        redirect_object_prepare(
          $redirect,
          array(
            'source' => $v,
            'source_options' => array(),
            'redirect' => 'node/' . $node->nid,
            'redirect_options' => array(),
            'language' => LANGUAGE_NONE,
          )
        );
        $redirect->is_new = TRUE;
      }
      redirect_save($redirect);
    }

    watchdog('Recipe Migrate',
             '!title (!rid) has been imported.',
             array('!title' => $node->title, '!rid' => $row->id),
             WATCHDOG_INFO, 'node/' . $node->nid);
    return;
  }

  public function prepareRow($row) {

    $item = $row;

    $row->updated_time = REQUEST_TIME;
    $this->_addBasicInfo($row, $item);
    $this->_addNutritionInfo($row, $item);
    $this->_addIngredientInfo($row, $item);
    $this->_addDirectionsInfo($row, $item);
    $this->_addDietInfo($row, $item);
    $this->_addFlagInfo($row, $item);
    $this->_addPhotoInfo($row, $item);
    $this->_addBeerWineInfo($row, $item);
    $this->_addCopyrightInfo($row, $item);

    return TRUE;
  }

  function _addBasicInfo(&$row, $basic) {
    $row->field_rid = $basic->id;
    $row->title = urldecode($basic->title);

    $serves_count = urldecode($basic->serves);
    $row->field_serves = $serves_count;
    $row->body =  '<p>' . urldecode($basic->description) . '</p>';
    $row->field_prep_time = $this->_parseTime($basic->prep_time);
    $row->field_cook_time = $this->_parseTime($basic->cook_time);
    if ($row->field_prep_time == '') {
      $row->field_prep_time = 0;
    }

    if ($row->field_cook_time == '') {
      $row->field_cook_time = 0;
    }

    $row->changed = strtotime(urldecode($basic->modified));
    $row->created = strtotime(urldecode($basic->created));
    $row->rating = (string) ($basic->weighted_rating) ? $basic->weighted_rating : 0.0;
    if (empty($row->created) || $row->created < 0) {
      $row->created = REQUEST_TIME;
    }

    if (empty($row->changed) || $row->changed < 0) {
      $row->changed = REQUEST_TIME;
    }

    // saving modified field as is
    $row->field_recipe_source_modified = $basic->modified;
  }

  function _addNutritionInfo(&$row, $item) {
    $nutrition =  (array) $item->basicnutrition;
    unset($nutrition['id']);
    unset($nutrition['recipe_id']);

    $html = json_encode($nutrition);

    $row->field_nutritional_info = $html;
  }

  function _addIngredientInfo(&$row, $item) {
    $ingredients = (array) $item->ingredients;

    $html = array();

    foreach ($ingredients as $ing) {
      foreach ($ing as $key => $value) {
        if (gettype($value) == 'string')
          $ing->$key = urldecode($value);

        if ($value == 'n/a')
          $ing->$key = NULL;
      }

      $html[] = json_encode($ing);
    }

    $row->field_ingredients = $html;
  }

  function _addDirectionsInfo(&$row, $item) {
    $directions = (array) $item->steps;

    $html = array();
    foreach ($directions as $step) {
      foreach ($step as $key => $value) {
        if (gettype($value) == 'string')
          $step->$key = urldecode($value);
      }

      $html[] = json_encode($step);
    }

    $row->field_recipe_directions = $html;
  }

  function _addDietInfo(&$row, $item) {
    $flags = $item->flags;

    $items = array();
    if (is_array($flags)) {
      foreach ($flags as $flag) {
        if ($flag->flag_type_id !== WFMAPI_FLAG_ID_SPECIAL_DIET)
          continue;
        $diet_node = $this->_getDietNode(str_replace('-', ' ', urldecode($flag->name)));
        if ($diet_node != NULL) {
          $items[] = array('nid' => $diet_node->nid);
        }
      }
    }

    $row->field_special_diet = $items;
  }

  function _addFlagInfo(&$row, $item) {
    $flag_map = array(
      2 => 'recipe_type_dish',
      3 => 'recipe_cusine',
      4 => 'recipe_featured_in',
      5 => 'recipe_occasions',
      6 => 'recipe_category',
      9 => 'recipe_main_ingredient',
    );

    $results = array();
    $flags = $item->flags;
    $items = array();
    if (is_array($flags)) {
      foreach ($flags as $flag) {
        if (array_key_exists($flag->flag_type_id, $flag_map)) {
          $vocab_name = $flag_map[$flag->flag_type_id];
          $vocab = taxonomy_vocabulary_machine_name_load($vocab_name);

          if ($vocab) {
            $query = new EntityFieldQuery;
            $result = $query
              ->entityCondition('entity_type', 'taxonomy_term')
              ->propertyCondition('name', urldecode($flag->name))
              ->propertyCondition('vid', $vocab->vid)
              ->execute();

            if (empty($result)) {
              $term = new stdClass();
              $term->name = urldecode($flag->name);
              $term->vid = $vocab->vid;
              taxonomy_term_save($term);
              $tid = $term->tid;
            }
            else {
              $term = array_shift(array_shift($result));
              $tid = $term->tid;
            }
            $results[$flag_map[$flag->flag_type_id]][] = $term->tid;
          }
        }
      }
    }

    foreach ($results as $k => $v) {
      $row->{$k} = $v;
    }


  }

  function _addPhotoInfo(&$row, $item) {
    $photos = $item->photos;
    $row->field_hero_image = array();
    if (is_array($photos) && !empty($photos)) {
      foreach ($photos as $photo) {
        if (is_object($photo) && isset($photo->url)) {
          $row->field_hero_image[] = urldecode($photo->url);
        }
      }
      $row->field_hero_image = array_shift($row->field_hero_image);
    }
    else{
      $row->field_hero_image = NULL;
    }
  }

  function _addBeerWineInfo(&$node, $item) {
    // TODO: (msmith) Figure out how this Beer Wine stuff works,
    //                and add a field for it.
    //                Add in whatever it is to the node here.
  }

  function _addCopyrightInfo(&$node, $item) {
    // TODO: (msmith) Add a copyright field to the recipe node and add it here.
  }

  /**
   *
   */
  protected function _parseTime($time) {
    // TODO: (msmith) Parse out minutes our hours if they're in the JSON result.
    //                Not really sure how it'll ever be returned.

    return preg_replace('/\D/', '', $time);
  }


  function _getDietNode($name) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'special_diet')
      ->propertyCondition('title', $name)
      ->range(0, 1)
      ->addMetaData('account', user_load(1));

    $result = $query->execute();

    if (isset($result['node'])) {
      $news_items_nids = array_keys($result['node']);
      $news_items = entity_load('node', $news_items_nids);
      $node = array_pop($news_items);
    }
    else {
      $node = NULL;
      //      $node = $this->_createNewDietNode($name);
    }

    return $node;
  }

  function _createNewDietNode($name) {
    // Create node object.
    $node = new stdClass();
    $node->type = 'special_diet';
    $node->language = LANGUAGE_NONE;
    node_object_prepare($node);
    $node->title = $name;

    node_save($node);
    return $node;
  }

  public function __construct() {
    parent::__construct(MigrateGroup::getInstance('recipes'));

    $migrate_list = new RecipeMigrateList('https://api.wholefoodsmarket.com/v1/recipes.json', array("content-type" => "application/json"));
    $migrate_item = new RecipeMigrateItem('https://api.wholefoodsmarket.com/v1/recipes/:id.json', array("content-type" => "application/json"));
    $migrate_source_list = new MigrateSourceList($migrate_list, $migrate_item);

    $this->map = new MigrateSQLMap(
      $this->machineName,
        array(
          'rid' => array(
            'type' => 'int',
            'unsigned' => TRUE,
            'not null' => TRUE,
          ),
        ),
      MigrateDestinationNode::getKeySchema()
    );

    $this->source = $migrate_source_list;
    $this->destination = new MigrateDestinationNode('recipe');


    $this->addFieldMapping('field_recipe_updated_time', 'updated_time');
    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('body', 'body')->arguments(array('format' => 'full_html'));
    $this->addFieldMapping('field_recipe_created_time', 'created');
    $this->addFieldMapping('field_recipe_rating', 'rating');
    $this->addFieldMapping('field_rid', 'field_rid');
    $this->addFieldMapping('field_recipe_source_modified', 'field_recipe_source_modified');
    $this->addFieldMapping('field_prep_time', 'field_prep_time');
    $this->addFieldMapping('field_cook_time', 'field_cook_time');
    $this->addFieldMapping('field_serves', 'field_serves');
    $this->addFieldMapping('field_nutritional_info', 'field_nutritional_info');
    $this->addFieldMapping('field_ingredients', 'field_ingredients');
    $this->addFieldMapping('field_recipe_directions', 'field_recipe_directions');
    $this->addFieldMapping('field_special_diet', 'field_special_diet');

    $this->addFieldMapping('field_hero_image', 'field_hero_image');
    $this->addFieldMapping('field_hero_image:file_replace')
      ->defaultValue(FILE_EXISTS_REPLACE);

    $this->addFieldMapping('field_recipe_type_dish', 'recipe_type_dish')->arguments(array('source_type' => 'tid'));
    $this->addFieldMapping('field_recipe_cusine', 'recipe_cusine')->arguments(array('source_type' => 'tid'));
    $this->addFieldMapping('field_recipe_occasions', 'recipe_occasions')->arguments(array('source_type' => 'tid'));
    $this->addFieldMapping('field_recipe_category', 'recipe_category')->arguments(array('source_type' => 'tid'));
    $this->addFieldMapping('field_recipe_main_ingredient', 'recipe_main_ingredient')->arguments(array('source_type' => 'tid'));
    $this->addFieldMapping('field_recipe_featured_in', 'recipe_featured_in')->arguments(array('source_type' => 'tid'));
  }
}
