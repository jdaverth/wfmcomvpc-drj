<?php
/**
 * @file
 * Contains the class definitions migrating store content from WFM store db
 * to Drupal nodes.
 */

class MigrateAddressfieldFieldHandler extends MigrateFieldHandler {

  public function __construct() {
    $this->registerTypes(array('addressfield'));
  }

  public function prepare($entity, array $field_info, array $instance, array $values) {
    $migration = Migration::currentMigration();
    if (isset($values['arguments'])) {
      $arguments = $values['arguments'];
      unset($values['arguments']);
    }
    else {
      $arguments = array();
    }

    $language = $this->getFieldLanguage($entity, $field_info, $arguments);

    $delta = 0;
    $return = array();
    $value = $values;
    // Don't save empty references
    if ($value) {
      $states = array(
        'AL' => 'Alabama',
        'AK' => 'Alaska',
        'AZ' => 'Arizona',
        'AR' => 'Arkansas',
        'CA' => 'California',
        'CO' => 'Colorado',
        'CT' => 'Connecticut',
        'DE' => 'Delaware',
        'DC' => 'District Of Columbia',
        'FL' => 'Florida',
        'GA' => 'Georgia',
        'HI' => 'Hawaii',
        'ID' => 'Idaho',
        'IL' => 'Illinois',
        'IN' => 'Indiana',
        'IA' => 'Iowa',
        'KS' => 'Kansas',
        'KY' => 'Kentucky',
        'LA' => 'Louisiana',
        'ME' => 'Maine',
        'MD' => 'Maryland',
        'MA' => 'Massachusetts',
        'MI' => 'Michigan',
        'MN' => 'Minnesota',
        'MS' => 'Mississippi',
        'MO' => 'Missouri',
        'MT' => 'Montana',
        'NE' => 'Nebraska',
        'NV' => 'Nevada',
        'NH' => 'New Hampshire',
        'NJ' => 'New Jersey',
        'NM' => 'New Mexico',
        'NY' => 'New York',
        'NC' => 'North Carolina',
        'ND' => 'North Dakota',
        'OH' => 'Ohio',
        'OK' => 'Oklahoma',
        'OR' => 'Oregon',
        'PA' => 'Pennsylvania',
        'RI' => 'Rhode Island',
        'SC' => 'South Carolina',
        'SD' => 'South Dakota',
        'TN' => 'Tennessee',
        'TX' => 'Texas',
        'UT' => 'Utah',
        'VT' => 'Vermont',
        'VA' => 'Virginia',
        'WA' => 'Washington',
        'WV' => 'West Virginia',
        'WI' => 'Wisconsin',
        'WY' => 'Wyoming',
      );

      if ($value['country'] == 'USA')
        $value['country'] = 'US';
      elseif ($value['country'] == 'CAN')
        $value['country'] = 'CA';
      elseif ($value['country'] == 'United Kingdom')
        $value['country'] = 'UK';

      $value['state'] = array_search($value['state'], $states);

      $return[$language][$delta]['country'] = $value['country'];
      $return[$language][$delta]['administrative_area'] = $value['state'];
      $return[$language][$delta]['locality'] = $value['city'];
      $return[$language][$delta]['postal_code'] = $value['zipchar'];
      $return[$language][$delta]['thoroughfare'] = $value['address'];
      $delta++;
    }
    return $return;
  }
}

class StoresMigrateList extends MigrateListJSON {
  protected function getIDsFromJSON(array $data) {
    $return = array();
    foreach ($data as $k => $v) {
      if (isset($v['Store'])) {
        $return[] = $v['Store']['tlc'];
      }
    }

    return $return;
  }

  public function getIdList() {
    migrate_instrument_start("Retrieve $this->listUrl");
    /*
    if (empty($this->httpOptions)) {
      $json = file_get_contents($this->listUrl);
    }
    else {
      $response = drupal_http_request($this->listUrl, $this->httpOptions);
      $json = $response->data;
    }
    */
    $ch = curl_init($this->listUrl);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $json = curl_exec($ch);
    curl_close($ch);


    migrate_instrument_stop("Retrieve $this->listUrl");
    if ($json) {
      $data = drupal_json_decode($json);
      if ($data) {
        return $this->getIDsFromJSON($data);
      }
    }
    $migration = Migration::currentMigration();
    $migration->showMessage(t('Loading of !listurl failed:',
        array('!listurl' => $this->listUrl)));
    return NULL;
  }
}

class StoresMigrateItem extends MigrateItemJSON {
}

class StoresMigrateSource extends MigrateSourceList {
}

class StoresMigration extends Migration {
  public function prepareRow($row) {

    if ($row->query->count == 0) {
      echo "Skip\n";
      return FALSE;
    }

    $item = 0;
    $item = $row->$item;
    $store = $item->Store;

    $row->title = $store->name;
    $address = array();
    foreach (array('address', 'address2', 'city', 'state', 'country', 'zipchar') as $v) {
      if (drupal_strlen($store->$v) > 0) {
        $address[$v] = $store->$v;
      }
    }
    echo $store->name . "\n";
    //    print_r($address);

    $row->field_postal_address = $address;
    $row->field_phone_number = $store->phone;
    $row->field_fax_number = $store->fax;
    $row->field_store_hours = $store->hours;
    $row->field_store_twitter = $store->twitter;
    $row->field_facebook_url = $store->facebook;
    $row->field_twitter_url = $store->flickr;
    $row->store_name = $store->display_name;

    return TRUE;
  }

  public function __construct() {
    parent::__construct(MigrateGroup::getInstance('stores'));

    $migrate_list = new StoresMigrateList('https://api.wholefoodsmarket.com/v2/stores.json?limit=4000', array('headers' => array("content-type" => "application/json"), 'data' => 'page=1&limit=4000'));
    $migrate_item = new StoresMigrateItem('https://api.wholefoodsmarket.com/v2/stores/:id.json', array('headers' => array("content-type" => "application/json")));
    $migrate_source_list = new MigrateSourceList($migrate_list, $migrate_item);

    $this->map = new MigrateSQLMap($this->machineName,
                                   array(
                                         'tlc' => array(
                                                        'type' => 'varchar',
                                                        'length' => 32)),
                                   MigrateDestinationNode::getKeySchema()
                                   );

    $this->source = $migrate_source_list;
    $this->destination = new MigrateDestinationNode('store');

    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('field_store_name', 'field_store_name');
    $this->addFieldMapping('field_phone_number', 'field_phone_number');
    $this->addFieldMapping('field_fax_number', 'field_fax_number');
    $this->addFieldMapping('field_store_hours', 'field_store_hours');
    $this->addFieldMapping('field_store_twitter', 'field_store_twitter');
    $this->addFieldMapping('field_facebook_url', 'field_facebook_url');
    $this->addFieldMapping('field_flickr_url', 'field_flickr_url');
    $this->addFieldMapping('field_postal_address', 'field_postal_address');

    return;
  }
}