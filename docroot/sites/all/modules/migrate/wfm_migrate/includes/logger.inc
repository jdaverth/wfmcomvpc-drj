<?php
/**
 * @file Class file for logger for mongo mapper one-time process
 */

Class MongoLogger {
  private $tempdir;
  private $tempfile;

  /**
   * Constructor function
   */
  public function __construct() {
    $this->tempdir = sys_get_temp_dir();
    $this->tempfile = $this->tempdir . '/mongologger.log';
  }

  /**
   * Log a message to log file and screen.
   * @param $text message to log
   */
  public function logMe($text, $type = 'notice') {
    $handle = fopen($this->tempfile, 'a');
    fwrite($handle, $type . ': ');
    fwrite($handle, $text . "\n");
    fclose($handle);
    drush_log($text, $type);
  }
}