<?php

/**
 * @file
 * Contains the class definitions for migrating recipes from
 * SAGE 2.0 API (wfmapi) to Drupal nodes.
 *
 * All the details of RecipeV2Migration live here.
 */


use Wfm\Api\SageClient\Recipe;
/**
 * Class RecipeV2Migration
 */
class RecipeV2Migration extends Migration {

  protected $recipeAPI;
  protected $migrateItem;
  protected $migrateItemlist;
  protected $fieldsArray;

  /**
   * Constructor.
   *
   * A Migration constructor takes an array of arguments as its
   * first parameter.  The arguments must be passed through to the parent
   * constructor.
   *
   * @param array $arguments
   *   Arguments
   */
  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->team = array(
      new MigrateTeamMember('Selwyn Polit', 'selwyn.polit@wholefoods.com',
        t('Developer')),
      new MigrateTeamMember('Tom Britton', 'tom.britton@wholefoods.com',
        t('Developer')),
    );

    // Individual mappings in a migration can be linked to a ticket or issue
    // in an external tracking system. Define the URL pattern here in the shared
    // class with ':id:' representing the position of the issue number, then add
    // ->issueNumber(1234) to a mapping.
    $this->issuePattern = 'https://digitalandweb.atlassian.net/browse/:id:';

    $this->description = t('Migrate recipes from WFM SAGE api v2.');

    // Manually define source fields.
    $this->fieldsArray = array(
      'id' => 'ID number for each recipe',
      '_id' => 'Mongo ID for each recipe',
      'title' => 'Title of recipe',
      'modified_at' => 'Datetime recipe was last modified',
      'modified_at2' => 'Datetime recipe was last modified',
      'description' => 'Description',
      'comment_count' => 'Number of comments',
      'number_of_ratings' => 'Number of ratings',
      'created_at' => 'Created',
      'rating' => 'Rating',
      'weighted_rating' => 'Weighted Rating',
      'prep_time' => 'Preparation time',
      'cook_time' => 'Cooking time',
      'serves' => 'Serves',
      'ingredients' => 'Ingredients',
      'directions' => 'Directions',
      'photos' => 'Photographs',
      'special_diet' => 'Special Diet Flag',
      'type_dish' => 'Type of dish',
      'cuisine' => 'Cuisine',
      'occasion' => 'Occasion',
      'category' => 'Category',
      'categories' => 'Categories',
      'main_ingredient' => 'Main Ingredient in dish',
      'cloudinary_images' => 'Cloudinary Images',
      'image_filename' => 'Image Filename',
      'field_hero_image' => 'Main image for the recipe',
      'image_alt_text' => 'Image title text',
      'image_title_text' => 'Image title text',

    );

    // Hash all source rows and compare to hash in migrate_map_recipev2
    // table to see if anything has changed.
    $source_options = array(
      'track_changes' => 1,
    );

    $migrate_list = new RecipeList();
    $this->recipeAPI = $migrate_list->getrecipeAPI();
    $this->migrateItem = new RecipeItem($this->recipeAPI);
    $migrate_source_list = new MigrateSourceList($migrate_list, $this->migrateItem, $this->fieldsArray, $source_options);
    $this->source = $migrate_source_list;

    // Set up destination - nodes of type recipe.
    $this->destination = new MigrateDestinationNode('recipe');

    /* Create a map object for tracking the relationships
    between source rows and their resulting Drupal objects.
    mongo id <-> recipe nid
    */
    $map_options = array(
      // Create source object.
      'track_last_imported' => TRUE,
    );
    $this->map = new MigrateSQLMap(
      $this->machineName,
      array(
        '_id' => array(
          'type' => 'varchar',
          'length' => 255,
          'description' => 'Sage API mongo DB id',
          'not null' => TRUE,
        ),
      ),
      MigrateDestinationNode::getKeySchema(),
      'default',
      $map_options
    );

    // Field Mappings.
    $this->addFieldMapping('field_rid', 'id');
    $this->addFieldMapping('field_mongo_id', '_id');
    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('body', 'description');
    $this->addFieldMapping('body:format')
      ->defaultValue('full_html');

    //$this->addFieldMapping('field_recipe_created_time', 'created_at');
    $this->addFieldMapping('field_recipe_rating', 'weighted_rating');
    $this->addFieldMapping('field_prep_time', 'prep_time');
    $this->addFieldMapping('field_cook_time', 'cook_time');
    $this->addFieldMapping('field_serves', 'serves');

    $this->addFieldMapping('field_recipe_directions', 'directions');
    $this->addFieldMapping('field_recipe_directions:format')
      ->defaultValue('full_html');

    $this->addFieldMapping('field_recipe_course', 'type_dish');
    $this->addFieldMapping('field_recipe_cuisine', 'cuisine');
    $this->addFieldMapping('field_recipe_occasions', 'occasion');
    $this->addFieldMapping('field_recipe_category', 'category');
    $this->addFieldMapping('field_recipe_main_ingredient', 'main_ingredient');
    //$this->addFieldMapping('field_recipe_updated_time', 'modified_at');
    //$this->addFieldMapping('field_recipe_source_modified', 'modified_at2');
    $this->addFieldMapping('field_special_diet', 'special_diet');

    // Images.
    $this->addFieldMapping('field_hero_image', 'field_hero_image');
    $this->addFieldMapping('field_hero_image:file_class')
      ->defaultValue('MigrateFileUri');
    $this->addFieldMapping('field_hero_image:destination_dir')
      ->defaultValue('public://media');
    $this->addFieldMapping('field_hero_image:destination_file', 'image_filename');
    $this->addFieldMapping('field_hero_image:file_replace')
      ->defaultValue(MigrateFile::FILE_EXISTS_REUSE);
    $this->addFieldMapping('field_hero_image:preserve_files')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_hero_image:alt', 'image_alt_text');
    $this->addFieldMapping('field_hero_image:title', 'image_title_text');

    $this->addFieldMapping('field_ingredients', 'ingredients');
    $this->addFieldMapping('field_ingredients:format')
      ->defaultValue('full_html');

    $this->addFieldMapping('field_nutritional_info', 'basic_nutrition');
    $this->addFieldMapping('field_nutritional_info:format')
      ->defaultValue('full_html');

    $this->addFieldMapping('locations_acl')
      ->defaultValue("WFM")
      ->description(t('Do we like global access?'));

    $this->addFieldMapping('uid')
      ->defaultValue(0)
      ->description(t('Do we still want to leave this as anon?'));

    $this->addUnmigratedSources(array(
      'rating',
      'categories',
      'cloudinary_images',
      'comment_count',
      'number_of_ratings',
      'photos',
      ));

    $this->addUnmigratedDestinations(array(
      'created',
      'changed',
      'status',
      'promote',
      'sticky',
      'revision',
      'log',
      'language',
      'tnid',
      'translate',
      'revision_uid',
      'body:summary',
      'field_hero_image:source_dir',
      'field_hero_image:urlencode',
      'field_ingredients:summary',
      'field_is_featured',
      'field_marquee_image',
      'field_marquee_image:file_class',
      'field_marquee_image:preserve_files',
      'is_new',
      ));
  }


  /**
   * Modify source data to get it ready for migrate.
   *
   * @param object $row
   *   Source row.
   *
   * @return bool
   *   if you return false, he will skip this row
   */
  public function prepareRow($row) {

    if (parent::prepareRow($row) === FALSE || $row->status != 'Published') {
      return FALSE;
    }

    // Use modified_at2 here but reformat.
    $date = new DateTime($row->modified_at2);
    $datestring = $date->format('Y-m-d H:i:s');
    $row->modified_at2 = $datestring;

    $this->prepareIngredientInfo($row);
    $this->prepareNutritionInfo($row);
    $this->preparePhotoInfo($row);
    $str = sprintf("id:%s %s %s", $row->_id, $row->title, $row->field_hero_image);
    drush_print_r($str);

    return TRUE;
  }

  /**
   * Retrieve the existing image URI.
   *
   *  Lookup 36.jpg in file_managed
   *  it's uri could be public://media/36_0.jpg or
   *  public://media/36_1.jpg so migrate needs this info
   *  to verify that the image exists.  If the file doesn't exist
   *  migrate will want to write it to file_managed table where
   *  it already exists which will cause integrity violations.
   *
   * @param string $image
   *   $image comes in this form:
   *   http://assets.wholefoodsmarket.com/recipes/30/460/290/30.jpg
   *   which means it is on Amazon s3
   *
   * @return null|string
   *   Return the actual filename on disk or null.
   */
  private function lookupImageFilename($image) {
    if (!strlen($image)) {
      return NULL;
    }
    $dest_filename = basename($image);
    $uri = db_query('SELECT f.uri
      FROM {file_managed} f
      WHERE f.filename = :filename', array(':filename' => $dest_filename))
      ->fetchField();

    if ($uri) {
      $uri = basename($uri);
      return $uri;
    }
    return NULL;
  }


  /**
   * Prepare the photo info.
   *
   * Grab the first image which is correctly sized for
   * display on the recipe node pages
   *
   * @param object $row
   *   Source Row.
   */
  private function preparePhotoInfo($row) {
    $photos = $row->photos;
    if (is_array($photos) && !empty($photos)) {
      $image = array();
      foreach ($photos as $photo) {
        if (is_array($photo)) {
          $image = array_shift($row->photos);
          $row->field_hero_image = $image['url'];

          // $image comes in this form:
          // http://assets.wholefoodsmarket.com/recipes/30/460/290/30.jpg
          $image = basename($image['url']);
          $uri = $this->lookupImageFilename($image);
          $row->image_filename = $uri;

          // Alt & title text use the recipe title.
          $row->image_alt_text = $row->title;
          $row->image_title_text = $row->title;

          // Just return the first one.
          return;
        }
      }
    }
    else {
      $row->field_hero_image = NULL;
      $row->image_filename = NULL;
    }
  }


  /**
   * Grab the ingredients.
   *
   * @param object $row
   *   Source row.
   */
  private function prepareIngredientInfo($row) {
    $ingredients = array();

    foreach ($row->ingredients as $ingredient_row) {
      $ingredient = $this->processIngredientRow($ingredient_row);
      if (strlen($ingredient)) {
        $ingredients[] = $ingredient;
      }
    }
    $row->ingredients = $ingredients;
  }

  /**
   * Grab nutritional information.
   *
   * @param object $row
   *   Row of source data.
   */
  private function prepareNutritionInfo($row) {

    $content = array();
    $uom_g = 'g';
    $uom_mg = 'mg';
    $nutrition = new stdClass();

    // Don't go through all this if there is no nutritional info.
    if (!isset($row->basic_nutrition)){
      return;
    }

    // Convert array to an object.
    foreach ($row->basic_nutrition as $key => $value) {
      $nutrition->$key = $value;
    }

    $content[] = '<strong>' . t('Per Serving:') . '</strong>';

    // Serving size: about 1/4 cup,
    if (!empty($nutrition->serving_size)) {
      $content[] = 'Serving size: <span itemprop="servingSize">' . $nutrition->serving_size . '</span>,';
    }

    // 150 calories (100 from fat),
    if (!empty($nutrition->calories)) {
      $partial_content = '<span itemprop="calories">' . $nutrition->calories .
      ' calories</span> ';
      if (isset($nutrition->calories_from_fat)) {
        $partial_content .= '(' . $nutrition->calories_from_fat . ' from fat),';
      }
      $content[] = $partial_content;
    }

    // 11g total fat,
    if (!empty($nutrition->total_fat)) {
      $content[] = '<span itemprop="fatContent">' . $nutrition->total_fat . $uom_g . ' </span>total fat,';
    }

    // 1g saturated fat,
    if (!empty($nutrition->saturated_fat)) {
      $content[] = '<span itemprop="saturatedFatContent">' . $nutrition->saturated_fat . $uom_g . '</span> saturated fat,';
    }

    // 0mg cholesterol,
    if (!empty($nutrition->cholesterol)) {
      $content[] = '<span itemprop="cholesterolContent">' . $nutrition->cholesterol . $uom_mg . '</span> cholesterol,';
    }

    // 190mg sodium,
    if (!empty($nutrition->sodium)) {
      $content[] = '<span itemprop="sodiumContent">' . $nutrition->sodium . $uom_mg . '</span> sodium,';
    }

    // 10g carbohydrate.
    if (!empty($nutrition->total_carbohydrates)) {
      $content[] = '<span itemprop="carbohydrateContent">' . $nutrition->total_carbohydrates . $uom_g .
        '</span> carbohydrates,';
    }

    // (3g dietary fiber, 2g sugar),
    if (!empty($nutrition->carbohydrates_from_fiber)) {
      $partial_content = '(' . $nutrition->carbohydrates_from_fiber . '</span> <span itemprop="fiberContent">' . $uom_g . '</span> dietary fiber';
      if (!empty($nutrition->carbohydrates_from_sugar)) {
        $partial_content .= ', <span itemprop="sugarContent">' .$nutrition->carbohydrates_from_sugar .
          $uom_g . '</span> sugar';
      }
      $partial_content .= '),';
      $content[] = $partial_content;
    }
    // 4g protein.
    if (!empty($nutrition->protein)) {
      $content[] = '<span itemprop="proteinContent">' . $nutrition->protein . $uom_g . '</span> protein';
    }

    // Put them all together into a nice string.
    $string_ver = implode(' ', $content);

    // Remove any trialing whitespace & comma.
    $string_ver = rtrim($string_ver, ', ');
    $string_ver .= '.';
    $row->basic_nutrition = $string_ver;

  }

  /**
   * Process one ingredient row.
   *
   * @param array $row
   *   One of the ingredients for this recipe.
   *
   * @return string
   *   A nice neat string for display like: "1 large yellow chopped onion"
   */
  private function processIngredientRow($row) {
    $item = new stdClass();
    $content = array();

    // Convert array to an object.
    foreach ($row as $key => $value) {
      $item->$key = $value;
    }

    if (!empty($item->amount)) {
      if (is_float($item->amount)) {
        $item->amount = $this->decToFraction($item->amount);
      }
      $content[] = $item->amount . ' ';
    }

    if (!empty($item->package_size_amount)) {
      $content[] = '(' . $item->package_size_amount . '-' .  $item->package_size_uom . ') ';
    }

    if (!empty($item->uom)) {
      if (is_numeric($item->amount)) {
        if ($item->amount == 1) {
          // E.g. Tablespoon.
          $content[] = $item->uom . ' ';
        }
        else {
          // E.g. Tablespoons.
          $content[] = $item->uom_plural . ' ';
        }
      }
      else {
        // $item->Amount is a string like "1/2"
        $content[] = $item->uom . ' ';
      }
    }
    $content[] = $item->description;

    if (!empty($item->extra_info)) {
      $content[] = ', ' . $item->extra_info;
    }

    // Put them all together into a nice string.
    $string_ver = implode('', $content);
    return $string_ver;
  }

  /**
   * Convert Decimal to Fraction.
   *
   * @param float $float
   *   a float like 0.5 or 0.75
   *
   * @return float|string
   *   a string like 1/2  or 3/4
   */
  public function decToFraction($float) {
    // 1/2, 1/4, 1/8, 1/16, 1/3 ,2/3, 3/4, 3/8, 5/8, 7/8, 3/16, 5/16, 7/16,
    // 9/16, 11/16, 13/16, 15/16
    $whole = floor($float);
    $decimal = $float - $whole;
    // 16 * 3;
    $least_common_denom = 48;
    $denominators = array(2, 3, 4, 8, 16, 24, 48);
    $rounded_decimal = round($decimal * $least_common_denom) / $least_common_denom;
    if ($rounded_decimal == 0) {
      return $whole;
    }
    if ($rounded_decimal == 1) {
      return $whole + 1;
    }

    foreach ($denominators as $d) {
      if ($rounded_decimal * $d == floor($rounded_decimal * $d)) {
        $denom = $d;
        break;
      }
    }
    return ($whole == 0 ? '' : $whole) . " " . ($rounded_decimal * $denom) . "/" . $denom;
  }
}

class RecipeList extends MigrateList {

  protected $recipeAPI;
  protected $apiKey;
  protected $apiSecret;
  protected $apiUrl;
  protected $apiVersion;
  protected $cachingMethod;
  protected $cachingOptions;
  /**
   * Constructor.
   */
  public function __construct() {

    $cache = new WfmApiDrupalCache(array('cache_bin' => 'cache_wfm_api'));

    $this->recipeAPI = new Recipe(
      $this->apiKey = WFMVariables::get('wfmapi_key'),
      $this->apiSecret = WFMVariables::get('wfmapi_secret'),
      $this->apiUrl = 'https://' . WFMVariables::get('wfmapi_host'),
      $this->apiVersion = WFMVariables::get('wfmapi_version'),
      $cache
    );

    // Get all the recipe ID's
    migrate_instrument_start("Retrieve IDs for recipes modified in last 7 days");
    $this->recipe_id_array = $this->recipeAPI->getRecipesModifiedSince(strtotime('-7 days'));
    migrate_instrument_stop("Retrieve IDs for all recipes");

    /* This is in the form array[0]['id']['_id']
    ideally this would make an array of _id's only
    */
    // Remove all the id's
    $new_array = array();
    for ($counter = 0; $counter < count($this->recipe_id_array); $counter++) {
      $new_array[] = $this->recipe_id_array[$counter]['_id'];
    }
    $this->recipe_id_array = $new_array;

  }

  /**
   * Return listing source.
   *
   * Return a string representing where the listing
   * is obtained from (a URL, file directory, etc.)
   *
   * @return string
   *   The endpoint.
   */
  public function __toString() {
    return 'https://' . WFMVariables::get('wfmapi_host') ;
  }

  /**
   * Get list of Recipes.
   *
   * Return an array of unique IDs, suitable for
   * passing to the MigrateItem class to retrieve
   * the data for a single item.
   *
   * @return Mixed
   *   iterator or array
   */
  public function getIdList() {
    return (object) $this->recipe_id_array;
  }

  /**
   * Return a count of IDs available to be migrated.
   *
   * @return int
   *   The count.
   */
  public function computeCount() {
    return count($this->recipe_id_array);

  }

  /**
   * Gets the recipe api object.
   *
   * @return object
   *   recipeAPI
   */
  public function getrecipeAPI() {
    return $this->recipeAPI;
  }
}

/**
 * Implementation of MigrateItem, for retrieving a Recipe object given
 * an ID provided by a MigrateList class.
 */
class RecipeItem extends MigrateItem {

  protected $recipeAPI;

  /**
   * Constructor for MigrateItem.
   *
   * @param object $recipe_api
   *   recipeAPI
   */
  public function __construct($recipe_api) {
    $this->recipeAPI = $recipe_api;
  }

  /**
   * Retrieve recipe from sage API.
   *
   * Retrieve the entire recipe from the Sage 2.x API and
   * process fields accordingly.
   *
   * @param string $id
   *   mongo_id
   *
   * @return object
   *   recipe or null if error
   */
  public function getItem($id) {
    $this->recipeAPI->resetSelectFields();
    $item = $this->recipeAPI->getRecipe($id);
    if ($item) {
      // $this->title = $item['title'];
      $item['modified_at2'] = $item['modified_at'];

      // Get flag types.
      // $flagtypes = $this->recipeAPI->getRecipeFlagTypes();
      for ($i = 0; $i < count($item['flag_types']); $i++) {
        switch ($item['flag_types'][$i]['type']) {
          case 'Categories':
            $item['category'] = $item['flag_types'][$i]['flags'];
            break;

          case 'Cuisine':
            $item['cuisine'] = $item['flag_types'][$i]['flags'];
            break;

          case 'Main Ingredient':
            $item['main_ingredient'] = $item['flag_types'][$i]['flags'];
            break;

          case 'Type of Dish':
            $item['type_dish'] = $item['flag_types'][$i]['flags'];
            break;

          case 'Special Diets':
            // This is special as it is a node-reference field.
            $special_items = $item['flag_types'][$i]['flags'];
            $item['special_diet'] = $this->getNidFromTitle($special_items,
              'special_diet', $id);
            break;

          case 'Occasions':
            $item['occasion'] = $item['flag_types'][$i]['flags'];
            break;

        }
      }
      return (object) $item;
    }
    // Failed.
    $migration = Migration::currentMigration();
    $message = t('Loading of Recipe !id failed:', array('!id' => $id));
    $migration->getMap()->saveMessage(
      array($id), $message, MigrationBase::MESSAGE_ERROR);
    return NULL;
  }


  /**
   * Lookup the nids for this array of titles for content type $type.
   *
   * This is used for looking up the nid for special diet
   * content
   *
   * @param array $titles
   *   array of titles
   * @param string $type
   *   node type
   * @param string $id
   *   mongo id for error message handling
   *
   * @return array
   *   of matching nids
   */
  private function getNidFromTitle($titles, $type, $id) {
    $nid_array = array();
    foreach ($titles as $title) {
      $row = db_query('SELECT nid
        FROM {node} WHERE title = :mytitle and type = :type and status = 1',
        array(
          ':mytitle' => $title,
          ':type' => $type,))
        ->fetchField();
      if (isset($row)) {
        if ($row > 0 ){
          // Found a valid nid.
          $nid_array[] = intval($row) ;
        }
        else {
          // There is no node of type special item with this title
          // so mark this as error and skip this item.
          if (false) {
            $migration = Migration::currentMigration();
            $message =
              t('Unable to find matching special diet with title @title ',
                array('@title' => $title));
            $migration->getMap()->saveMessage(
              array($id), $message, MigrationBase::MESSAGE_ERROR);
          }
          //drush_print_r("Can't find unpublished special diet node: " . $title );
        }
      }
    }
    return $nid_array;
  }
}
