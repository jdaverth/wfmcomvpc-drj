<?php

/**
 * @file
 * Process theme data.
 *
 * Use this file to run your theme specific implimentations of theme functions,
 * such preprocess, process, alters, and theme function overrides.
 *
 * Preprocess and process functions are used to modify or create variables for
 * templates and theme functions. They are a common theming tool in Drupal, often
 * used as an alternative to directly editing or adding code to templates. Its
 * worth spending some time to learn more about these functions - they are a
 * powerful way to easily modify the output of any template variable.
 *
 * Preprocess and Process Functions SEE: http://drupal.org/node/254940#variables-processor
 * 1. Rename each function and instance of "adaptivetheme_subtheme" to match
 *    your subthemes name, e.g. if your theme name is "footheme" then the function
 *    name will be "footheme_preprocess_hook". Tip - you can search/replace
 *    on "adaptivetheme_subtheme".
 * 2. Uncomment the required function to use.
 */


/**
 * Preprocess variables for the html template.
 */
function brand_camp_theme_preprocess_html(&$vars) {
  $node = menu_get_object();
  $meta = array();
  if ($node) {
    if ($node->field_bc_cover_image) {
      $meta['og:image'] = file_create_url($node->field_bc_cover_image['und'][0]['uri']);
    }
    if ($node->body && empty($node->body[LANGUAGE_NONE][0]['value']) && !empty($node->body[LANGUAGE_NONE][0]['summary'])) {
      $meta['og:description'] = strip_tags($node->body[LANGUAGE_NONE][0]['summary']);
    }
  }

  $i = 0;
  foreach ($meta as $key => $value) {
    if (!is_array($value)) {
      $og['#type'] = 'markup';
      $og['#markup'] = '<meta property="' . $key . '" content="' . $value . '"/>';
      drupal_add_html_head($og, 'og' . ++$i);
    }
    else {
      foreach ($value as $property) {
        $og['#type'] = 'markup';
        $og['#markup'] = '<meta property="' . $key . '" content="' . $property . '"/>';
        drupal_add_html_head($og, 'og' . ++$i);
      }
    }
  }
}

/**
 * Forms summary.
 */
function _og_description($text) {
  return str_replace('"', '&quot;', trim(text_summary(filter_xss($text, array()))));
}

/* -- Delete this line to enable.
function adaptivetheme_subtheme_preprocess_html(&$vars) {
  global $theme_key;

  // Two examples of adding custom classes to the body.

  // Add a body class for the active theme name.
  // $vars['classes_array'][] = drupal_html_class($theme_key);

  // Browser/platform sniff - adds body classes such as ipad, webkit, chrome etc.
  // $vars['classes_array'][] = css_browser_selector();

}
// */


/**
 * Process variables for the html template.
 */
/* -- Delete this line if you want to use this function
function adaptivetheme_subtheme_process_html(&$vars) {
}
// */


/**
 * Override or insert variables for the page templates.
 */
function brand_camp_theme_preprocess_page(&$vars) {
  $all_topics_link = '/values-matter';
  if (isset($vars['node'])) {
    if ($vars['node']->type == 'bc_ugc_image' && $_SERVER['HTTP_REFERER']) {
      $all_topics_link = htmlspecialchars($_SERVER['HTTP_REFERER']);
    }
  }
  $vars['all_topics_link'] = $all_topics_link;
}
/* -- Delete this line if you want to use these functions
function adaptivetheme_subtheme_process_page(&$vars) {
}
// */


/**
 * Override or insert variables into the node templates.
 */
/* -- Delete this line if you want to use these functions
function adaptivetheme_subtheme_preprocess_node(&$vars) {
}
function adaptivetheme_subtheme_process_node(&$vars) {
}
// */


/**
 * Override or insert variables into the comment templates.
 */
/* -- Delete this line if you want to use these functions
function adaptivetheme_subtheme_preprocess_comment(&$vars) {
}
function adaptivetheme_subtheme_process_comment(&$vars) {
}
// */


/**
 * Override or insert variables into the block templates.
 */
/* -- Delete this line if you want to use these functions
function adaptivetheme_subtheme_preprocess_block(&$vars) {
}
function adaptivetheme_subtheme_process_block(&$vars) {
}
// */


/**
 * Implements hook_form_FORM_ID_alter().
 */
function brand_camp_theme_form_views_exposed_form_alter(&$form, &$form_state, $form_id) {
  if ($form_state['view']->name == 'brand_camp_list' && $form_state['view']->current_display == 'page') {
    foreach ($form['#info'] as $filter) {
      $all_option = $form[$filter['value']]['#options']['All'];
      unset($form[$filter['value']]['#options']['All']);
      $form[$filter['value']]['#options']['All'] = $all_option;
    }
  }
}


/**
 * Implements theme_preprocess_node().
 */
function brand_camp_theme_preprocess_node(&$vars) {
  $bundles = _brand_camp_get_bundles();
  if (in_array($vars['type'], $bundles)) {
    $vars['theme_hook_suggestions'][] = 'node__brand_camp__' . $vars['view_mode'];
  }
}

/**
 * Implements theme_preprocess_field().
 */
function brand_camp_theme_preprocess_field(&$vars, $hook) {
  if($vars['element']['#field_name'] == 'field_bc_showme') {
    foreach($vars['element']['#items'] as $key => $item) {
      $vars['item_classes'][$key] = 'field-key-'. $item['value'];
      $vars['item_attributes_array'][$key]['title'] = $vars['items'][$key]['#markup'];
    }
  }
  elseif ($vars['element']['#field_name'] == 'field_bc_timing') {
    foreach ($vars['items'] as $key => $value) {
      if ($value['#markup'] > 90 && $value['#markup'] < 3599) {
        $minutes = round($value['#markup'] / 60);
        $vars['items'][$key]['#markup'] = $minutes . ' ' . format_plural($minutes, t('minute'), t('minutes'));;
      }
      elseif ($value['#markup'] > 3599) {
        $hours = round($value['#markup'] / 3600);
        $vars['items'][$key]['#markup'] = $hours . ' ' . format_plural($hours, t('hour'), t('hours'));
      }
      else {
        $vars['items'][$key]['#markup'] .= ' ' . format_plural($value['#markup'], t('second'), t('seconds'));
      }
    }
  }
}

/**
 * Implements hook_js_alter().
 */
function brand_camp_theme_js_alter(&$javascript) {
  unset($javascript['sites/all/libraries/jail/jquery.jail.js']);
  unset($javascript['sites/all/modules/custom/wfm_blocks/wfm_blocks.js']);
}