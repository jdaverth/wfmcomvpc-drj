function janrainReturnExperience() {
    var span = document.getElementById('traditionalWelcomeName');
    var name = janrain.capture.ui.getReturnExperienceData("displayName");
    if (span && name) {
        span.innerHTML = "Welcome back, " + name + "!";
    }
}

function janrainCaptureWidgetOnLoad() {
  var $logout_link = jQuery('a[href="/user/logout"]');
  $logout_link.addClass('capture_end_session');

  function handleCaptureLogin(result) {
    console && console.log ("exchanging code for token...");

    jQuery.ajax({
      url: Drupal.settings.basePath + 'janrain_capture/oauth?code=' + result.authorizationCode,
      success: function(token) {
        console.log('code for token exchange completed');
        handleDatalayerProcess(result);
        window.location.reload();
      },
      async: false
    });
  }
  janrain.events.onCaptureSessionFound.addHandler(function(result){
    //console.log ("capture session found");
  });

  janrain.events.onCaptureSessionNotFound.addHandler(function(result){
    //console.log ("capture session not found");
    if (typeof(Backplane) != 'undefined' && typeof(Backplane.getChannelID()) == 'undefined') {
      //console.log ("reset backplane channel");
      Backplane.resetCookieChannel();
    }
  });

  function handleDatalayerProcess(result) {
    //Push dataLayer object for login or register via Janrain.
    if (result.action == "traditionalRegister" ||
        result.action == "socialRegister"){
        window.dataLayer.push({
          'event': 'account-created',
          'sign-up-method': result.authProvider
        });
    } else if (result.action == "traditionalSignin" ||
               result.action == "socialSignin") {
        window.dataLayer.push({
          'event': 'account-sign-in',
          'sign-in-method': result.authProvider
        });
    }
  }

  janrain.events.onCaptureLoginSuccess.addHandler(handleCaptureLogin);
  janrain.events.onCaptureSessionEnded.addHandler(function() {
    window.location.href = '/user/logout';
  });
  janrain.events.onCaptureRegistrationSuccess.addHandler(handleCaptureLogin);
  janrain.events.onCaptureScreenShow.addHandler(function(result) {
      if (result.screen == "returnTraditional") {
          janrainReturnExperience();
      }
  });

  janrain.capture.ui.start();
}
