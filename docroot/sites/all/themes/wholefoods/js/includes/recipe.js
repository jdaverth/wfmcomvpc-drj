/**
 * @file
 * Recipe pages js code.
 */

(function ($) {

Drupal.WholeFoods.Recipes = {};

Drupal.WholeFoods.Recipes.pageLoad = function() {
  var $new_list_fieldset = $('#edit-new-list');
  if ($new_list_fieldset.length <= 0) {
    return;
  }

  var $form = $('#healthy-eating-add-recipe-ingredients-form.disabled-form');
  $('input', $form).attr("onclick", "javascript:return false;");
  $('select', $form).attr('disabled', 'disabled');

  $new_list_fieldset.hide();

  var $select_list = $('#edit-shopping-list');
  $select_list.change(function() {
    var $selected = $('option:selected', $(this));
    if ('new' == $selected.val()) {
      $new_list_fieldset.css("position","absolute");
      $new_list_fieldset.css("top", (Math.max(0, (($(window).height() - $new_list_fieldset.outerHeight()) / 2) +
                                              $(window).scrollTop()) - 50) + "px");
      $new_list_fieldset.css("left", Math.max(0, (($(window).width() - $new_list_fieldset.outerWidth()) / 2) +
                                                $(window).scrollLeft()) + "px");
      $('#healthy-eating-add-recipe-ingredients-form select').attr('disabled', 'disabled');
      $('#healthy-eating-add-recipe-ingredients-form input[type="submit"]').attr("onclick", "javascript:return false;");
      $('#healthy-eating-add-recipe-ingredients-form input[name="new_list_name"]').removeAttr('disabled');
      $('#edit-new-list-button').removeAttr('disabled');
      $new_list_fieldset.show();
    }
    else if (0 == $selected.val()) {
      $('#healthy-eating-add-recipe-ingredients-form input').attr("onclick", "javascript:return false;");
      if ($form.length <= 0) {
      $('#healthy-eating-add-recipe-ingredients-form select').removeAttr('disabled');
      }
    }
    else {
      $('#healthy-eating-add-recipe-ingredients-form input').removeAttr("onclick", "javascript:return false;");
    }

  });

  $('#edit-new-list-button').click(function(e) {
    var $selected = $('option[value="new"]', $select_list);
    var $name = $('#edit-new-list-name');
    var name_string = $name.attr('value');
    $selected.attr('value', name_string);
    $selected.html(name_string);
    $new_list_fieldset.hide();
    $('#healthy-eating-add-recipe-ingredients-form select').removeAttr('disabled');
    $('#healthy-eating-add-recipe-ingredients-form input[type="submit"]').removeAttr("onclick", "javascript:return false;");
    e.preventDefault();
    return false;
  });

  $('#healthy-eating-add-recipe-ingredients-form .cancel-button').click(function() {
    $new_list_fieldset.hide();
    $('option', $select_list).removeAttr("selected");
    $('option[value="0"]', $select_list).attr("selected", "selected");
    $select_list.change();
    return false;
  });

  $('#edit-new-list-name').keydown(function(e){
	  if (e.keyCode == 13) {
      e.preventDefault();
		  $("#edit-new-list-button").click();
	  }
  });

  $('#healthy-eating-add-recipe-ingredients-form').submit(function(e) {
    e.preventDefault();
    var list = null;
    var listName = null;
    var user = null;
    var rid = null;

    var $selected = $('option:selected', $select_list);
    var $user = $('input[name="user"]', $(this));
    var $rid = $('input[name="rid"]', $(this));

    list = $selected.val();
    listName = $selected.html();
    user = $user.val();
    rid = $rid.val();
    if (list == listName) {
      list = 'new';
    }

    if (user == 0 || user == null || user == ''){
      Drupal.WholeFoods.Recipes._setValidateMessage(Drupal.t("Please sign in or create an account to use the shopping list feature."));
      return false;
    }
    else if (0 == list) {
      Drupal.WholeFoods.Recipes._setValidateMessage(Drupal.t("Please select a shopping list to add this recipe to."));
      return false;
    }

    var url = '/action/add_recipe_to_shopping_list/' + list + '/' + rid + '/' + user + '/' + encodeURIComponent(listName);
    // Disable the form so you can't do it again.
    $('#healthy-eating-add-recipe-ingredients-form select').attr('disabled', 'disabled');
    $('#healthy-eating-add-recipe-ingredients-form input[type="submit"]').attr("onclick", "javascript:return false;");
    $.ajax({
      url: url,
      success: function(data) {
        Drupal.WholeFoods.Recipes._setMessage(Drupal.t('Ingredients have been added to !recipe', {'!recipe' : listName}));
        Drupal.WholeFoods.Recipes.setDataLayer();
      },
      error: function(data) {
        Drupal.WholeFoods.Recipes._setMessage(Drupal.t('There was a problem adding ingredients to !recipe', {'!recipe' : listName}));
      }
    });

    return false;
  });

  $select_list.change();
}


Drupal.WholeFoods.Recipes._setValidateMessage = function(msg) {
  alert(msg);
}

Drupal.WholeFoods.Recipes._setMessage = function(msg) {
  var $msg = $('#extra-msg');
  $msg.html(msg + ' |&nbsp');
}

Drupal.WholeFoods.Recipes.setDataLayer = function() {
  if (window.dataLayer) {
    window.dataLayer.push({
      'event': 'shoplist-add-recipe',
      'recipe-name': $('.view-recipe-header .torn-pod-content .views-field-title h1').text(),
    });
  }
}

$(window).load(function() {
  Drupal.WholeFoods.Recipes.pageLoad();
});

})(jQuery);
