/**
 * @file
 * TODO (msmith): Add file docs!
 */

(function ($) {

Drupal.WholeFoods.Responsive = {};
Drupal.WholeFoods.Responsive.BREAKPOINT_BROWSER = '1220';
Drupal.WholeFoods.Responsive.BREAKPOINT_TABLET = '1219';
Drupal.WholeFoods.Responsive.BREAKPOINT_PHONE = '999';
Drupal.WholeFoods.Responsive.TYPE_TABLET = 'tablet';

if (navigator.appName == 'Microsoft Internet Explorer') {
	var ua = navigator.userAgent;
	var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
  var rv = -1;
	if (re.exec(ua) != null){
		rv = parseFloat( RegExp.$1 );
	}

  if (rv - 9.0) {
    Drupal.WholeFoods.Responsive.BREAKPOINT_BROWSER = '1372';
    Drupal.WholeFoods.Responsive.BREAKPOINT_TABLET = '1371';
  }
}

Drupal.WholeFoods.Responsive.settings = {
  width: null,
  height: null,
  device: '',
  deviceType: 'browser',
  responsiveChange: false,
  breakPoint: '',
};

/**
 * Method to be ran on $(window).load.
 */
Drupal.WholeFoods.Responsive.pageLoad = function() {
  $(window).resize(Drupal.WholeFoods.Responsive._pageResize);
  Drupal.WholeFoods.Responsive._fixUnderNavOpenIssue();

}

/**
 * Method to be ran on $(document).ready.
 */
Drupal.WholeFoods.Responsive.documentReady = function() {
  // initialize the nav tab regardless of viewability, because
  // sometimes the tab will be shown but the listener will not be added
  // in _pageResize.
  var $monsterNav = Drupal.WholeFoods.Responsive._getMonsterNav();
  if (Drupal.WholeFoods.Responsive._hasSlidingMonsterNav() == false) {
    $monsterNav.addClass('initialized');
    
    $('.nav-tab').click(Drupal.WholeFoods.Responsive._toggleMonsterNav);
  }
  
  Drupal.WholeFoods.Responsive._pageResize();
}

Drupal.WholeFoods.Responsive._toggleMonsterNav = function() {
  $monsterNav = Drupal.WholeFoods.Responsive._getMonsterNav();

	if (window.innerWidth > 999) {
  	if ($('.nav').hasClass('nav-close')) {
	  	//open
    	$('#block-views-monster-nav-top').animate({'left' : 0+'px'}, 500);
	  	$('.nav-close').animate({'left' : 0+'px'}, 500);
    	$('.nav').removeClass('nav-close');
  	}
  	else {
	  	//close
    	$('.nav').addClass('nav-close');
    	$('.nav-close').animate({'left' : -200+'px'}, 500);
		//	  $('#block-views-monster-nav-top').animate({'left' : -20+'px'}, 500);
  	}
	}
}

Drupal.WholeFoods.Responsive._getMonsterNav = function() {
  if (this.settings.monsterNav == null) {
    this.settings.monsterNav = $('#block-views-monster-nav-top');
  }

  return this.settings.monsterNav;
}

/**
 * Method to run on resize of the window to handle moving DOM elements around.
 *
 * In addition to being called every resize event, this method is also called
 * once on DocumentReady.
 */
Drupal.WholeFoods.Responsive._pageResize = function() {
  var width = null;

  Drupal.WholeFoods.Responsive._detectResponsiveChange();

  if (Drupal.WholeFoods.Responsive.hasChanged() == true) {
    var breakPoint = Drupal.WholeFoods.Responsive.breakPoint();
    Drupal.WholeFoods.Responsive._resetEverything();

    if (breakPoint != Drupal.WholeFoods.Responsive.BREAKPOINT_BROWSER) {
      Drupal.WholeFoods.Responsive._initSlidingMonsterNav();
    }
    else {
      $('.nav').css('left', '0px');
      $('.nav').removeClass('nav-close');
    }

    if (breakPoint == Drupal.WholeFoods.Responsive.BREAKPOINT_PHONE) {
      Drupal.WholeFoods.Responsive._initUnderMonsterNav();
    }

    var $navTab = $('.nav-tab');
    if ($navTab.css('z-index') == '20') {
      $navTab.css('z-index', '19');
    }
    else {
      $navTab.css('z-index', '20');
    }
  }
  Drupal.WholeFoods.Responsive._fixIeHandleIssue();
  Drupal.WholeFoods.Responsive._fixUnderNavOpenIssue();
}

Drupal.WholeFoods.Responsive._fixUnderNavOpenIssue = function() {
  if ($('.undernav-nav').length) {
    $('#navigation').css('left','0');
  }
}

Drupal.WholeFoods.Responsive._initSlidingMonsterNav = function() {
  var $monsterNav = Drupal.WholeFoods.Responsive._getMonsterNav(),
      navTab = $('.nav-tab');
  if (Drupal.WholeFoods.Responsive._hasSlidingMonsterNav() == false) {
    $monsterNav.addClass('initialized');

    // prevent multiple of the same listener from being attached
    navTab.off('click');
    navTab.click(Drupal.WholeFoods.Responsive._toggleMonsterNav);
  }

  if (!$('.nav').hasClass('nav-close')) {
    Drupal.WholeFoods.Responsive._toggleMonsterNav();
  }
}

Drupal.WholeFoods.Responsive._hasSlidingMonsterNav = function() {
  var $monsterNav = Drupal.WholeFoods.Responsive._getMonsterNav();
  return $monsterNav.hasClass('initialized');
}


Drupal.WholeFoods.Responsive._initUnderMonsterNav = function() {
  if ($('html').hasClass('undernav-active')) {
    return;
  }

  $('#header, #navigation').underNav({
    content_container: '#page-wrapper',
    distance: '235px',
    width: 999,
    plugin_loaded : function() {
      Drupal.WholeFoods.Responsive._monsterUnderNavCallback();
    }
  });
}

Drupal.WholeFoods.Responsive._monsterUnderNavCallback = function() {
  $('.main-links-container').removeClass('desktop');
  $(document).delegate('.main-links-container a.primary-link', 'click', function(e){
    e.preventDefault();
    var primary_link = $(this);

    if (!primary_link.hasClass('undernav-link-open')) {
      primary_link.closest('.menu-drop-container').show();
      primary_link.addClass('undernav-link-open');
    }
    else if (primary_link.hasClass('undernav-link-open')) {
      primary_link.closest('.menu-drop-container').hide();
      primary_link.removeClass('undernav-link-open');
    }
  });
}

Drupal.WholeFoods.Responsive._resetEverything = function() {
}

Drupal.WholeFoods.Responsive._detectResponsiveChange = function() {
  var width = null;
  var height = null;
  var device = null;
  var deviceType = null;
  var breakPoint = this.BREAKPOINT_BROWSER;

  this.settings.responsiveChange = false;

  this._measurePage();
  width = this.width();
  height = this.height();
  device = this.device();
  device_type = this.deviceType();

  if (width <= this.BREAKPOINT_PHONE) {
    breakPoint = this.BREAKPOINT_PHONE;
  }
  else if (width <= this.BREAKPOINT_TABLET || deviceType  == this.TYPE_TABLET) {
    breakPoint = this.BREAKPOINT_TABLET;
  }

  if (this.breakPoint() != breakPoint) {
    console.log("Responsive Breakpoint Changed " + breakPoint);
    this.settings.responsiveChange = true;
  }
  this.settings.breakPoint = breakPoint;
}

Drupal.WholeFoods.Responsive._fixIeHandleIssue = function() {
  var $nav_tab = $('.nav-tab');
  // check for IE8 to add nav handle for closing menu when its overlapping content
  if (navigator.appName == 'Microsoft Internet Explorer') {
    var width = this.width();
//    alert(width);

    if (width > '999' && width < '1367' && document.querySelector && !document.addEventListener) {
      $nav_tab.addClass('handle');
    }
    else {
      $nav_tab.removeClass('handle');
    }
  }
}

Drupal.WholeFoods.Responsive._measurePage = function() {
  // Setup page measurements so we're never doing it more than once.
  this.settings.width = $(window).width();
  this.settings.height = $(window).height();
}

Drupal.WholeFoods.Responsive.width = function() {
  return this.settings.width;
}

Drupal.WholeFoods.Responsive.height = function() {
  return this.settings.height;
}

Drupal.WholeFoods.Responsive.device = function() {
  return '';
  //navigator.userAgent.match(/Android/i)
}

Drupal.WholeFoods.Responsive.deviceType = function() {
  return '';
  //navigator.userAgent.match(/Android/i)
}

Drupal.WholeFoods.Responsive.hasChanged = function() {
  return this.settings.responsiveChange;
}

Drupal.WholeFoods.Responsive.breakPoint = function() {
  return this.settings.breakPoint;
}


// Add method to window.load
$(window).load(Drupal.WholeFoods.Responsive.pageLoad);

// Add method to documentReady
$(document).ready(Drupal.WholeFoods.Responsive.documentReady);


})(jQuery);
