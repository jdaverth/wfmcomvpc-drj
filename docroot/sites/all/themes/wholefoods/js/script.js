var $_GET = {};

function wfm_getCookie(c_name)
{
  var i,x,y,ARRcookies=document.cookie.split(";");
  for (i=0;i<ARRcookies.length;i++) {
    x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
    y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
    x=x.replace(/^\s+|\s+$/g,"");
    if (x==c_name)
    {
      return unescape(y);
    }
  }

  return null;
}


document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
    function decode(s) {
        return decodeURIComponent(s.split("+").join(" "));
    }

    $_GET[decode(arguments[1])] = decode(arguments[2]);
});

function _unhideFrontpageContent($) {
}

//document.domain = 'wholefoodsmarket.com';
function closeProfileEditor() {
  location.reload();
}

function closeRecoverPassword (test) {
  jQuery('#fancybox-frame').attr('src', '/janrain_capture/forgot_pass_final');
}


var expanded = null;
if (typeof Drupal.openlayers !== 'undefined') {
  /**
   * OpenLayers Popup Behavior
   */
  Drupal.openlayers.addBehavior('openlayers_behavior_popup', function (data, options) {
    var map = data.openlayers;
    var layers = [];

    // For backwards compatiability, if layers is not
    // defined, then include all vector layers
    if (typeof options.layers === 'undefined' || options.layers.length === 0) {
      layers = map.getLayersByClass('OpenLayers.Layer.Vector');
    }
    else {
      for (var i in options.layers) {
        var selectedLayer = map.getLayersBy('drupalID', options.layers[i]);
        if (typeof selectedLayer[0] !== 'undefined') {
          layers.push(selectedLayer[0]);
        }
      }
    }

    var popupSelect = new OpenLayers.Control.SelectFeature(layers,
    {
     onSelect: function(feature) {
       // Create FramedCloud popup.
       popup = new OpenLayers.Popup.FramedCloud(
         'popup',
         feature.geometry.getBounds().getCenterLonLat(),
         new OpenLayers.Size(400, 185)/*        null*/,
         Drupal.theme('openlayersPopup', feature),
         null,
         true,
         function(evt) {
           Drupal.openlayers.popup.popupSelect.unselect(
             Drupal.openlayers.popup.selectedFeature
           );
         }
       );
       popup.contentSize = new OpenLayers.Size(400, 185);
       popup.autoSize = false;

       // Assign popup to feature and map.
       feature.popup = popup;
       feature.layer.map.addPopup(popup);
       Drupal.attachBehaviors();
       Drupal.openlayers.popup.selectedFeature = feature;
     },
     onUnselect: function(feature) {
       // Remove popup if feature is unselected.
       feature.layer.map.removePopup(feature.popup);
       feature.popup.destroy();
       feature.popup = null;
     }
    }
    );

    map.addControl(popupSelect);
    popupSelect.activate();
    Drupal.openlayers.popup.popupSelect = popupSelect;
  });
}

var parts = window.location.search.substr(1).split("&");
var $_GET = {};
for (var i = 0; i < parts.length; i++) {
    var temp = parts[i].split("=");
    $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
}


/**
 * On a search page, hide the default search form and copy it's value to the
 * search form block that should be in content-top.
 *
 * A search page by default will add a search form to the top of content,
 * which is the wrong region.
 */
var _initSearchPage = function($) {
  if ($('#search-api-page-search-form').length === 0)
    return;

  $('#search-api-page-search-form').hide();
  $('#search-api-page-search-form-product-search .form-text').val(
    $('#search-api-page-search-form .form-text').val());
};

/**
 * Redraw the vendor map once on page load. For some reason, this
 * doesn't seem to happen by default on the dev server. We force it here.
 */
var _initVendorMap = function($) {

  if (typeof Drupal.settings.openlayers === 'undefined')
    return

  for (map in Drupal.settings.openlayers.maps) {
    var control = $('.olControlLayerSwitcher');

    map = jQuery('#' + map);
    control = map.data('openlayers').openlayers.getControl(control.attr('id'));
    control.layerStates = [];
    control.redraw();
  }
};

/**
 * Equalize the height of center and right columns so they are more visually
 * appealing.
 */
var _columnEqualization = function($) {
  $('#breadcrumb:first').addClass('shown');

  if ($('body').hasClass('front') || $('body').hasClass('page-user') || $('body').hasClass('page-node-86'))
    return;

  var node = $('.node');
  if (node.height() <= 20 && !node.hasClass('node-webform'))
    node.hide();
  else
    node.show();

  $('#content .contentbox .block').not('#block-views-ab14fd5ea913ef070def3887cd05d050').each(function() {
    $(this).show();
  });

  $('#content .contentbox .block').not('#block-views-ab14fd5ea913ef070def3887cd05d050').each(function() {
    if ($(this).height() <= 20)
      $(this).hide();
    else
      $(this).show();
  });

  var centerContent = $('#content .contentbox:first');
  var nearby = $('.view-display-id-nearby');  // Preventing nearby events view block from being hidden.
  if (centerContent.height() <= 20 && nearby.length == 0) {
    $('#content').hide();
    $('#content-bottom').css('margin-top', '-10px');
  } else {
    $('#content').show();
    $('#content').css('display', 'inherit');
  }

  centerContent = $('#block-views-promo-blocks-block .content:first');
  if (centerContent.height() === 0) {
    $('#block-views-promo-blocks-block').css('display', 'none');
  }

  centerContent = $('.column.with-sidebar');
  if (centerContent.length !== 0) {
    var rightContent = $('.column.sidebar');
    var rightHeight = $('.torn-pod-content:first', rightContent).height() + 0;
    var rightHeight = rightContent.height();
    var centerHeight = $('.torn-pod-content:first', centerContent).height() - 0;
    var centerHeight = centerContent.height();
    if (rightHeight > centerHeight) {
   //   centerContent.css('min-height', rightHeight);
    }
    else {
     // rightContent.css('min-height', centerHeight);
    }
  }

  setTimeout(function() {
    var centerContent = $('.column.with-sidebar');
    if (centerContent.length !== 0) {
      var rightContent = $('.column.sidebar');
      var rightHeight = $('.torn-pod-content:first', rightContent).height()/* + 28*/;
      var centerHeight = $('.torn-pod-content:first', centerContent).height() - 28;
      if (rightHeight > centerHeight) {
        $('.torn-pod-content:first', centerContent).css('min-height', rightHeight);
      }
      else {
        // don't equalize content in mobile view
        $('#sidebar-second .torn-pod-content').css('min-height', centerHeight - 2000);
        var body = $('body');
        if (body.width() > 999) {
          $('.torn-pod-content:first', rightContent).css('min-height', centerHeight + 48);
          $('.node-type-blog-post #sidebar-second .torn-pod-content:first').css('min-height', centerHeight - 14);
        }
      }
    }
  }, 200);
};

var _initVideoPage = function($) {
  _init_video();
  var related = $('.view-display-id-featured_header .views-field-field-video-related-content');
  if (related.length > 0) {
    if (related.height() <= 0)
      return;
    var new_link =
      '<div class="show_more_content_pod"><a href="" class="show_more_featured">' +
      Drupal.t('Show More') +
      '</a></div>';

    related.before(new_link);
    related.hide();

    $('a.show_more_featured').click(function() {
      var view = $('.view-display-id-featured_header .views-field-field-video-related-content');
      $('.show_more_featured').hide();
      $(view).show();
      return false;
    });
  }
};

var _initProfile = function($) {
  setTimeout(function() {
    _initProfileHeader($);
    _initProfileShoppingList($);
  }, 200);
};

var _initProfileHeader = function($) {
  var store_header = $('#user-header-store');
  var name = $('.PersonaHeader_Title');
  name.detach();
  store_header.detach();
  $('.PersonaHeader_PublicContent').detach();
  store_header.appendTo($('.PersonaHeader_TableRight'));
  $('.PersonaHeader_Info:first').before(name);
};

var _initProfileShoppingList = function($) {
  var list = $('ul.list_links');

  if (list.length < 1) {
    return;
  }

  var add_item_form = $('#users-add-shopping-list-item-form');

  $('a.list_link').click(function() {
    var card = $('.card');
    $('.content', card).hide();
    $('.edit-link-subtitle .title').hide();

    $('#list-' + $(this).attr('ref') + ', #list-title-' + $(this).attr('ref'), card).show();
    add_item_form.appendTo($('.list_header', $('#list-' + $(this).attr('ref'), card)));

    $('input[name="list_id"]', add_item_form).val($(this).attr('ref'));
    $('.email_link a').attr('href', $('.email_link a').attr('ref').replace('_id', $(this).attr('ref')));
    $('.print_link a').attr('href', $('.print_link a').attr('ref').replace('_id', $(this).attr('ref')));
    //$('.sms_link a').attr('href', $('.sms_link a').attr('ref').replace('_id', $(this).attr('ref')));
    //$('.share_link a').attr('href', $('.share_link a').attr('ref').replace('_id', $(this).attr('ref')));

    $('.card .top .right-header').show();
    $('.list-add-item').show();
      Drupal.settings.WholeFoods.ShoppingList.current  = $(this).attr('ref');
    return false;
  });

  $('a.add_item_to_list').click(function() {
    add_item_form.slideToggle();
    return false;
  });

  var default_card = $('.card div.default');

  if (typeof default_card.attr('ref') !== 'undefined') {
    $('a.list_link[ref="' + default_card.attr('ref') + '"]').click();
  }
};

var cs_page_init = false;
var cs_options = new Array();
var cs_store = null;

var _doStateSelectorChange = function($, store_selector, state_selector) {
  $("optgroup", store_selector).each(function () {
    $(this).detach();
  });
  $("option", store_selector).each(function () {
    $(this).detach();
  });
  $("option:selected", state_selector).each(function () {
    store_selector.append("<option value='undefined'>" + Drupal.t('Select a Store') + "</option>");

    for (key in cs_options[$(this).val()]) {
      store_selector.append(cs_options[$(this).val()][key]);
    }
  });
  if ($("option[value='" + cs_store + "']", store_selector).length == 0) {
    var first = $('option', store_selector).first();
    first.attr('selected', 'selected');
  } else {
    $("option[value='" + cs_store + "']", store_selector).attr('selected', 'selected');
  }
  store_selector.trigger('change');
}


var _initForums = function($) {
  var dest = $('#forumDest.recipe_forums');
  if (dest.length <= 0) {
    var dest = $('#forumDest.all_forums');
    if (dest.length <= 0) {
      return;
    } else {
      var loc = '/forums';
    }
  }
  else {
    var loc = '/recipe-forums';
  }

  dest.bind('DOMNodeInserted', function(e) {
    var links = $('.ForumList_ForumName a');
    links.each(function() {
      var href = $(this).attr('href');
      var id = href.split('?')[1];
      $(this).attr('href', loc + '?' + id);
    });
    var links = $('#forumDest a');

    links.each(function() {
      href = $(this).attr('href');
      if (typeof href !== 'undefined') {
        if (href.substr(0,1) !== '#') {
          var id = href.split('?')[1];
          $(this).attr('href', loc + '?' + id);
        }
      }
    });
    $('#content').show();
  });
};

var _initRecipeSearch = function($) {
  var form = $('#healthy-eating-recipe-search');
  if (form.length <= 0)
    return;

  $('.advanced-search-link').click(function() {
    $('#edit-advanced').slideToggle('slow');
    return false;
  });
};

var _initShoppingListModal = function($) {
  var form = $('#modal-content #users-add-to-shopping-list-form');
  if (form.length <= 0)
    return;

  $('.shopping_list', form).hide();
  $('#edit-list-id', form).change(function() {
    $('option:selected', $(this)).each(function() {
      var list = $('#shopping_list_' + $(this).val());
      $('.shopping_list', form).hide();
      list.show();
    });
  });

  $('#edit-list-id').change();
};

var _initEvents = function($) {
  var events = $('.events');
  if (events.length < 0)
    return;

  $('.events .event').each(function() {
    var event = $(this);
    $('.expand a', $(this)).click(function() {
      $('.events .event .detail').hide();
      $('.events .event .brief').show();
      $('.events .event').removeClass('expanded');

      event.addClass('expanded');
      $('.brief', event).hide();
      $('.detail', event).show();
      _columnEqualization($);
      return false;
    });

    $('.collapse a', $(this)).click(function() {
      $('.events .event .detail').hide();
      $('.events .event .brief').show();
      $('.events .event').removeClass('expanded');
      _columnEqualization($);
      return false;
    });
  });

  $('.view-display-id-featured .views-field-nothing a').click(function() {
    var parent = $(this).parent('div');
    var class_list = parent.attr('class').split(' ');
    var node_class = class_list[class_list.length - 1];

    $('.events .' + node_class + ' .expand a').click();
  });
};

var _initNewsletter = function($) {
  var newsletter_form = $('#newsletters-form');
  if (newsletter_form.length <= 0)
    return;

  var select_all = $('#edit-selectall');
  select_all.change(function() {
    var options = $('input.newsletter_checkbox');
    if ($(this).is(':checked')) {
      options.not(':checked').click();
    } else {
      options.not(':not(:checked)').click();
    }
  });
};

var sale_page_init = false;
var state_options = new Array();
var selected_store = null;

var _initGenericPageList = function($) {
  //var list_div = $('#block-views-generic-page-items-list-copy');

  /*if (list_div.length < 1)
    return;*/

  $('a.expander').click(function() {
    var expanded = $('.expanded', $(this).parents('.list-row'));
    //$(this).addClass('collapse');
    $('.brief', $(this).parents('.list-row')).hide();
    expanded.show();
    _columnEqualization($);
    return false;
  });

  $('a.collapser').click(function() {
    var brief = $('.brief', $(this).parents('.list-row'));
    $('.expanded', $(this).parents('.list-row')).hide();
    brief.show();
    _columnEqualization($);
    return false;
  });
};

var _initGenericPage = function($) {
  _initGenericPageList($);
};

var _initTwitterStateSelector = function($) {
  $('#twitter_state').change(function(){
    $('#edit-field-postal-address-administrative-area').val($(this).val());
  });
  $('#edit-field-postal-address-administrative-area').change(function(){
    $('#twitter_state').val($(this).val());
  });

};

var _initTwitterFeed = function($){
  $('.tweet-actions').delegate('a', 'click', function(){
    $(this).addClass('clicked');
  });
};

var _remove_modal = function(){
  jQuery('.store-box a.ctools-use-modal').each(function() {
    jQuery(this).removeClass('ctools-use-modal');
    jQuery(this).attr('href', '/stores/list');
    jQuery(this).removeClass('ctools-use-modal-processed');
    jQuery(this).unbind('click');
  });
  jQuery('a.iframe.fancy').each(function() {
    if (jQuery(this).attr('href') === '/user/janrain_register') {
      jQuery(this).removeClass('iframe');
      jQuery(this).removeClass('fancy');
      jQuery(this).unbind('click');
    }
  });
}

/**
 * Add class "current-page-item" to current department article nav item
 */
var _initDepartmentPage = function($){
  if ($('body').hasClass('node-type-department-article')) {
    var nid = Drupal.settings.nid;
    $('.view-id-store_departments a.node-' + nid).addClass('current-page-item');
  }
}

jQuery(function($) {
  _initSearchPage($);
  _initVendorMap($);
  _initVideoPage($);
  _initProfile($);
  _initForums($);
  _initEvents($);
  _initNewsletter($);
  _initRecipeSearch($);
  _initGenericPage($);
  _initDepartmentPage($);

  var promo_sorter = $('.promo-sorter-button');
  if (promo_sorter.length > 0) {
    var content = $('.view-display-id-sorter .view-content');
    content.hide();

    promo_sorter.click(function() {
      var content = $('.view-display-id-sorter .view-content');
      var display = content.css('display');
      content.slideToggle();
      if (display === 'none') {
        $(this).html(Drupal.t('Hide Promo Sequence'));
      }
      else {
        $(this).html(Drupal.t('Show Promo Sequence'));
      }

      return false;
    });
  }

  /*
    Shopping list functions
  */
  //Click on item name
  $('a.list_item_edit').click(function() {
    $('.list_item_edit_form').hide();
    $('.list_item_edit_form[ref="' + $(this).attr('ref') + '"]').toggle();
    return false;
  });

  //Clicking the delete icon
  $('.delete-item-icon a').click(function() {
    $('.list_item_edit_form[ref="' + $(this).attr('ref') + '"] input.delete-list-item').click();
    return false;
  });

  $('.rename_link').click(function() {
    var list_id = $(this).attr('ref');
    var field = $('#rename_list_' + list_id);

    if ($(this).attr('go') === 'true') {
      var text = field.val();

      if (text.length <= 0) {
        alert(Drupal.t("You can't rename a list to nothing!"));
      }
      else {
        window.location = encodeURI('/action/shopping_list/rename/' + list_id + '/' + text + '?destination=user/shopping_lists?show_list=' + list_id);
      }
    }
    else {
      $(this).html('' + Drupal.t('Save Rename') + '');
      $(this).attr('go', 'true');
      field.show('slide');
    }
    return false;
  });

  $('.social-feed-container.twitter_').each(function() {
    var content = $('.tweet-content', $(this));
    var sub_list = content.slice(3);
    sub_list.hide();

    var new_link =
      '<a href="" class="show_more_twitter" ref="'+$(this).attr('ref')+'">' +
      Drupal.t('Load More Tweets') +
      '</a>';

    $(this).append(new_link);
    var box = $(this);

    $('a.show_more_twitter').click(function() {
      $(this).hide();
      var box = $('.social-feed-container.twitter_' + $(this).attr('ref'));
      $('.tweet-content', box).show();
      _columnEqualization($);
      return false;
    });

  });

  $('.view-promo-blocks a').each(function () {
    $(this).click(function() {
      //var nid = $(this).parents('.promo-ref').attr('ref');
      var title = $(this).parents('.promo-ref').attr('title');
    });
  });

  var recipe = $('#block-healthy-eating-recipe-individual-page-header');
  if (recipe.length > 0) {
    $('.views-field.photos ul.ad-thumb-list', recipe).hide();

    $('.user-submitted-links.view-photos', recipe).click(function() {
      $('.views-field.photos ul.ad-thumb-list', recipe).slideToggle();
      return false;
    });

    $('.user-submitted-links.upload-photo', recipe).click(function() {
      $('.upload-photo-form', recipe).slideToggle();
      return false;
    });

  }

  if (typeof $_GET['forgot_code'] !== 'undefined') {
    $('#main-wrapper').append('<a href="/user/janrain_register?code='+$_GET['forgot_code']+'" class="iframe fancy" id="forgot_link">Click it!</a>');
  }

  _columnEqualization($);

  Drupal.ajax.prototype.commands.reload = Drupal.CTools.Modal.reload;

  Drupal.behaviors.core = {
    attach: function (context, settings) {
      _initShoppingListModal($);
      _initEvents($);
      _initTwitterStateSelector($);
      _columnEqualization($);
      _init_flexslider();
      _hide_extra_pods($);
      _change_modal();
      _initTwitterFeed($);
      _initDepartmentPage($); //No .once() because Department Article content loads in AJAX callback.
      $('.pod_blogs, .pod_facebook_feed, .pod_twitter_feed', context).addClass('social');
      $('.view-promo-blocks .col-width-one:first-child .views-col', context).addClass('promos');
      equalHeight($, $(".social .torn-pod-content", context));
      equalHeight($, $(".promos .torn-pod-content", context));
    }
  };

  // Watch the form containers for changes. If anything changes, re-call columnEqualizatio
  // so they're not hidden.
  if ($('#forumDest').length > 0) {
    $('#forumDest').bind('DOMNodeInserted', function(e) {
      _columnEqualization($);
    });
  }

  // Remove modals if the body width is less than 1000px across.
  var body = $('body');
  if (body.width() < 1000) {
    _remove_modal();
    setTimeout(_remove_modal, 2000);
    setTimeout(_remove_modal, 4000);
    setTimeout(_remove_modal, 6000);
  }

  $('#content').bind('DOMSubtreeModified', function(e) {
   // _columnEqualization($);
  });

  if (typeof Drupal.ajax.prototype.commands.viewsScrollTop != 'undefined') {
    Drupal.ajax.prototype.commands.viewsScrollTop = function (ajax, response, status) {
      // Scroll to the top of the view. This will allow users
      // to browse newly loaded content after e.g. clicking a pager
      // link.
      var offset = $(response.selector).offset();
      if (typeof offset == 'undefined' || offset == null)
        return;

      // We can't guarantee that the scrollable object should be
      // the body, as the view could be embedded in something
      // more complex such as a modal popup. Recurse up the DOM
      // and scroll the first element that has a non-zero top.
      var scrollTarget = response.selector;
      while ($(scrollTarget).scrollTop() == 0 && $(scrollTarget).parent()) {
        scrollTarget = $(scrollTarget).parent();
      }
      // Only scroll upward
      if (offset.top - 10 < $(scrollTarget).scrollTop()) {
        $(scrollTarget).animate({scrollTop: (offset.top - 10)}, 500);
      }
    };
  }

  // Disable scrollTop for views with load more pager.
  var views = [
    '.view-store-lookup-by-address.view-display-id-store_locations_block',
  ];
  $(views.join(',')).length && (Drupal.ajax.prototype.commands.viewsScrollTop = function() {});


  Drupal.behaviors.ModalClickCloseReload = {
    attach: function() {
      $('.close').bind('click', function() {
        location.reload();
      });
    }
  };

  Drupal.behaviors.detectUserStore = {
    attach: function() {
      var userStore = $.cookie('local_store_id');
      if (userStore != null) {
        $('body').addClass('store-selected').addClass('store-selected-' + userStore);
      }
    }
  };

/*
      var tag = document.createElement('script');
      tag.src = "http://www.youtube.com/player_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  jQuery('iframe').each(function() {
    var domain = jQuery(this).attr('src').split('/');
    if (domain[2] == 'www.youtube.com') {
      var str = domain[4].split('?')[0];
      console.log(str);
      var div = document.createElement('div');
      div.setAttribute('id', 'vid-' + str);
      div.setAttribute('class', 'youtube-player');
      jQuery(this).replaceWith(div);
    }
  });

  var players;
  function onYouTubePlayerAPIReady() {
    jQuery('.youtube-player').each(function() {
      var the_id = jQuery(this).attr('id');
      var the_vid_id = the_id.split('-')[1];
      var the_player = new YT.Player(the_id, {
          height: '433',
          width: '731',
          videoId: the_vid_id
        });
//      players[the_id] = the_player;

      the_player.addEventListener("onStateChange", "onytplayerStateChange");
    });

  }

function onytplayerStateChange(newState) {
   alert("Player's new state: " + newState);
}

window.addEventListener("message", function(a) {
console.log("Got " + a.data);
},!1);

*/

});

/**
 * AJAX responder command to dismiss the modal.
 */
Drupal.CTools.Modal.reload = function(command) {
  location.reload();
};

jQuery(document).ready(function(){
  jQuery.each(jQuery('.more-posts-by-link'), function(k, v){
    var text = jQuery(v).children('a').text();
    var data = text.split(' ');
    data.pop();
    jQuery(v).children('a').text(data.join(' '));
  });
});

jQuery(document).ready(function(){
  jQuery('.view-photos').hide();
  var ul = jQuery('.view-photos').parent().find('ul.ad-thumb-list');
  ul.bind('DOMSubtreeModified', function(e) {
    if (e.target.innerHTML.length > 0) {
      jQuery('.view-photos').show();
    }
    else {
    }
  });
});

jQuery(document).ready(function() {
    jQuery(".list-row").click(function(){
         window.location=jQuery(this).find("a").attr("href");
         return false;
    });
});


/**
 * Custom validation for store selector form on 'custom-service' page
 */
(function ($) {
  Drupal.behaviors.customServiceProcessing = {
    attach: function(context, settings) {
      $('#edit-submitted-choose-topic').find('option').eq(0).text(Drupal.t('Select a Topic'));
      var form = $('#webform-client-form-7477');
      if (typeof($().wfmvalidate) === 'function') {
        form.wfmvalidate({
          '#phone_number_store_list': 'not_undefined',
          '#edit-submitted-name': 'required',
  //        '#edit-submitted-email': 'required',
          '#edit-submitted-email': 'email',
          '#edit-submitted-body': 'required',
        });
      }
    }
  };
})(jQuery);
