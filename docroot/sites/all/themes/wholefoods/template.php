<?php

/**
 * Implements hook_form_alter().
 */
function wholefoods_form_alter(&$form, &$form_state, $form_id) {
  // Modify search box using hook form alter
  if ($form_id == 'search_block_form') {
    $form['search_block_form']['#size'] = 25;  // define size of the textfield
    $form_default = t('Search');
    $form['search_block_form']['#default_value'] = $form_default; // Set a default value for the textfield
    $form['actions']['submit']['#value'] = t('Search'); // Change the text on the submit button

    // Add extra attributes to the text box
    $form['search_block_form']['#attributes'] = array('onblur' => "if (this.value == '') {this.value = '{$form_default}';}", 'onfocus' => "if (this.value == '{$form_default}') {this.value = '';}" );
  }
}

/**
 * Implements hook_preprocess_html().
 */
function wholefoods_preprocess_html(&$vars) {
  // Detect Marquee region
  if (!isset($vars['page']['marquee'])) {
    $vars['classes_array'][] = 'no-marquee';
  }
  // Detect Marquee Heading
  else {
    $marquee = isset($vars['page']['marquee']['views_generic_page_items-marqee'])
      ? $vars['page']['marquee']['views_generic_page_items-marqee'] : '';
    if ($marquee && (isset($marquee['content']['#markup']) || isset($marquee['#markup']))) {
      $marquee = isset($marquee['#markup']) ? $marquee['#markup'] : $marquee['content']['#markup'];
      preg_match('/views-field-field-page-marquee-heading.*?field-content">(.*?)<\/div>/', $marquee, $heading);
      preg_match('/views-field views-field-field-page-marquee-copy.*?field-content">(.*?)<\/div>/', $marquee, $copy);
      if ($heading && trim($heading[1]) || $copy && trim($copy[1])) {
        $vars['classes_array'][] = 'marquee-heading';
      }
    }
    // Detect Marquee Promo
    $promo = isset($vars['page']['marquee']['views_recipe_promo-block'])
      ? $vars['page']['marquee']['views_recipe_promo-block'] : '';
    if ($promo && (isset($promo['content']['#markup']) || isset($promo['#markup']))) {
      $promo = isset($promo['#markup']) ? $promo['#markup'] : $promo['content']['#markup'];
      if (trim($promo)) {
        $vars['classes_array'][] = 'marquee-promo';
      }
    }
  }
  // Detect Free Form
  $node = menu_get_object();
  if ($node && 'page' == $node->type && $node->field_page_body_layout
      && 'free_form' == $node->field_page_body_layout[LANGUAGE_NONE][0]['value']) {
    $vars['classes_array'][] = 'free-form';
  }
  // Add conditional stylesheets for IE
  drupal_add_css(path_to_theme() . '/css/ie6.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 6', '!IE' => FALSE), 'preprocess' => FALSE));
  drupal_add_css(path_to_theme() . '/css/ie7.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'IE 7', '!IE' => FALSE), 'preprocess' => FALSE));
  drupal_add_css(path_to_theme() . '/css/ie8.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'IE 8', '!IE' => FALSE), 'preprocess' => FALSE));
  drupal_add_js(libraries_get_path('jquery.cycle') . '/jquery.cycle.all.min.js');
  // Add <link> to <head> for RSS feed on blog and blog_post node types
  blogs_rss_header();
  wfm_social_open_graph();
}

/**
 * Implements hook_preprocess_page().
 */
function wholefoods_preprocess_page(&$vars) {
  global $base_path;

	if (isset($vars['print']) && isset($_GET['css'])) {
    $css = drupal_get_path('theme', 'wholefoods') . '/css/print/' . filter_xss($_GET['css'], array()) . '.css';
    if (file_exists($css)) {
      $vars['print']['css'] .= '<link rel="stylesheet" href="' . $base_path . $css . '" media="all" />';
    }
	}
  // Do we have a node?
  if (isset($vars['node'])) {
		$node = $vars['node'];
		if (isset($node->field_page_body_layout[LANGUAGE_NONE][0])) {
			$vars['theme_hook_suggestions'][] = 'page__' . $node->type . '__' . $node->field_page_body_layout[LANGUAGE_NONE][0]['value'];
		}

    // Ref suggestions cuz it's stupid long.
    $suggests = &$vars['theme_hook_suggestions'];

    // Get path arguments.
    $args = arg();
    // Remove first argument of "node".
    unset($args[0]);

    // Set type.
    if ($vars['node']) {
      $type = "page__type_{$vars['node']->type}";
    }

    // Bring it all together.
    $suggests = array_merge(
      $suggests,
      array($type),
      theme_get_suggestions($args, $type)
    );

  }
  //And now for something completely unrelated... Add support for page--content-type.tpl.php
  if (isset($vars['node']->type)) {
    $vars['theme_hook_suggestions'][] = 'page__' . $vars['node']->type;
  }
}

/**
 * Implements hook_preprocess_node().
 */
function wholefoods_preprocess_node(&$variables) {
  $node = $variables['node'];

  hide($variables['content']['links']['print_html']);
  if ($blocks = block_get_blocks_by_region('content_sidebar')) {
    $variables['content_sidebar'] = $blocks;
  }

	if (isset($node->field_page_layout[LANGUAGE_NONE][0])) {
    $variables['theme_hook_suggestions'][] = 'node__' . $node->type . '__' . $node->field_page_layout[LANGUAGE_NONE][0]['value'];
  }

  if ($node->type == 'blog_post') {
    $variables['author_name'] = 'Anonymous';
    if (!empty($node->field_blog_author)) {
      $author_node = node_load($node->field_blog_author[LANGUAGE_NONE][0]['nid']);
      $variables['author_name'] = $author_node->title;
      $variables['author_path'] = drupal_get_path_alias('node/' . $author_node->nid);
    }
  }
}

/**
 * Implements template_preprocess_comment_wrapper().
 */
function wholefoods_preprocess_comment_wrapper(&$variables) {
  $result = db_select('comment', 'c')
    ->fields('c')
    ->condition('nid', $variables['node']->nid)
    ->condition('status', 1)
    ->execute();
  $variables['comment_count'] = $result->rowCount();

  $variables['blog_comment_guidelines'] = wfm_template_text_get_text('wfm_template_text_blog_comment_guidelines');
}

/**
 * Implements template_preprocess_comment().
 */
function wholefoods_preprocess_comment(&$variables) {
  // If comment was left by an anonymous user.
  $variables['author'] = check_plain($variables['comment']->name);

  if ($variables['comment']->uid > 0) {
    // If comment was left by an authenticated user.
    $result = db_select('field_data_field_display_name', 'n')
      ->fields('n', array('field_display_name_value'))
      ->condition('entity_id', $variables['comment']->uid)
      ->execute()
      ->fetchAssoc();
    $variables['author'] = check_plain($result['field_display_name_value']);
  }
}

/**
 * Implements hook_theme().
 */
function wholefoods_theme() {
  return array(
    '_wholefoods_search_api_page_results' => array(
      'template' => 'templates/search/search-results',
      'variables' => array('renderd_items' => NULL),
    ),
  );
}

/**
 * Implemennts hook_search_api_page_results().
 */
function wholefoods_search_api_page_results(array $variables) {
  $index = $variables['index'];
  $results = $variables['results'];
  $items = $variables['items'];
  $keys = $variables['keys'];
  $variables['facets'] = array();
  $per_page = 20;
  $page = (array_key_exists('page', $_GET)) ? $_GET['page'] : 0;

  if (arg(0) == 'recipe' || TRUE) {
    $filters = array(
      'special_diet',
      'recipe_occasions',
      'recipe_course',
      'recipe_main_ingredient',
      'recipe_cusine',
      'recipe_category',
      'recipe_featured_in',
    );
    foreach ($filters as $field) {
      $default = _get_default_options('field_' . $field);
      if (count($default) == 0) continue;

      if ($field == 'special_diet') {
        $options = array();
        $query = new EntityFieldQuery;
        $query->entityCondition('entity_type', 'node')
            ->entityCondition('bundle', 'special_diet')
            ->propertyOrderBy('title', 'ASC')
            ->addMetaData('account', user_load(1));

        $result = $query->execute();
        foreach ($result['node'] as $k => $value) {
          $term = node_load($k);
          if (_get_show_option($term) == FALSE) {
            continue;
          }
          if (array_key_exists($term->nid, $default)) {
            $variables['facets'][] = $term->title;
          }
        }
      }
      else {
        $options = _get_options_by_vocab($field);
        foreach ($default as $d) {
          $variables['facets'][] = $options[$d];
        }
      }
    }
  }

  $rendered_items = array();
  $count = 1;
  foreach ($results['results'] as $item) {
    $rendered_items[] = theme('_wholefoods_search_api_page_results', array(
      'index' => $index,
      'result' => $item,
      'item' => isset($items[$item['id']]) ? $items[$item['id']] : NULL, 'keys' => $keys,
      'count' => $count,
    ));
    if ($count == 3) $count = 1;
    else ++$count;
  }

  $results_count = count($results['results']);
  $variables['rendered_items'] = $rendered_items;
  $variables['term'] = check_plain(trim(str_replace('+', ' ', $keys)));
  $variables['start'] = $results_count ? ($per_page * $page) + 1 : 0;
  $variables['end'] = $results_count ? $variables['start'] + ($results_count - 1) : 0;

  return theme('_wholefoods_search_api_page_results', $variables);
}

/**
 * Implements hook_breadcrumb().
 */
function wholefoods_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  if (request_path() == 'sales-flyer' && is_numeric(arg(1)) && isset($_GET['store'])) {
    $node = node_load((int)($_GET['store']));
    $breadcrumb[] = l($node->title, 'node/' . $node->nid);
  }
  if (!empty($breadcrumb)) {
    $bc = array_pop($breadcrumb);
    $strip = strip_tags($bc);

    $title_from_drupal = drupal_get_title();
    if ($strip != $title_from_drupal) {
      array_push($breadcrumb, $bc);
    }
    if (strlen(trim($title_from_drupal)) > 0)
      $breadcrumb[] = $title_from_drupal;

    $output = '<div class="breadcrumb">' . implode(' » ', $breadcrumb) . '</div>';

    return $output;
  }
}

/**
 * Determines whether to generate location info from node reference or match on TLC
 *
 * @param object $item
 *   Service node object.
 * @return string
 *   Applicable location information for service.
 */
function wholefoods_service_render_search_results($item){
  $language = $item->language;
  $html = '';
  //Get node ids of stores attached to service
  $query = 'SELECT entity_id FROM {field_data_field_service} WHERE field_service_nid = :nid';
  $conditions = array(':nid' => $item->nid);
  $results = db_query($query, $conditions);
  $fetched = $results->fetchCol();
  if (count($fetched) > 0) {
    $html = wholefoods_services_search_get_text($fetched, $item);
  }
  elseif (isset($item->field_service_tlc[$language][0]['value'])) {
    $vocabulary = taxonomy_vocabulary_machine_name_load('locations_acl');
    $tlc_array = taxonomy_get_term_by_name($item->field_service_tlc[$language][0]['value'], $vocabulary->vid);
    if (count($tlc_array)) {
      $term = array_shift($tlc_array);
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'node')
          ->entityCondition('bundle', array('store', 'region', 'metro', 'national offices', 'IN'))
          ->fieldCondition('locations_acl', 'tid', $term->tid)
          ->range(0, 1)
          ->addMetaData('account', user_load(1)); // run the query as user 1
      $result = $query->execute();
      if (isset($result['node'])) {
        $html = wholefoods_services_search_get_text(array_keys($result['node']), $item);
      }
    }
  }
  return $html;
}

/**
 * Generates location info for service content type search results.
 *
 * @param array $fetched
 *   Array of entity ids.
 * @param object $item
 *   Node object from search result item.
 * @return string
 *   Applicable location information for service.
 */
function wholefoods_services_search_get_text($fetched, $item){
  if ($item->nid == 43136) {
    return FALSE;
  }
  if ($item->nid == 53871) {
    return t('This information is valid only in our Southern California stores.');
  }
  $text = '';
  if($results_count = count($fetched)) {
    $entity_ids = array();
    foreach ($fetched as $result) {
      $entity_ids[] = $result->entity_id;
    }
    $loaded_entities = entity_load('node', $entity_ids);
    $text = t('This information is valid at our ');
    $count = count($entity_ids);
    $i = 1;
    foreach ($loaded_entities as $store) {
      $storename = $store->title;
      $language = $store->language;
      $city = $store->field_postal_address[$language][0]['locality'];
      $state = $store->field_postal_address[$language][0]['administrative_area'];
      if ($i > 1 && $i === $results_count) {
        $text = trim($text, ' \t\n\r\0\x0B,');
        $text .= ' and our ' . $store->title . t(' store in ' . $city . ', ' . $state . ', ');
      }
      else if ($i > 1) {
        $text = substr_replace(trim($text) ,'',-1);
        $text .= ', our ' . $store->title . t(' store in ' . $city . ', ' . $state . ', ');
      }
      else {
        $text .= $store->title . t(' store in ' . $city . ', ' . $state . ', ');
      }
      $i++;
    }
    $text = trim($text, ' \t\n\r\0\x0B,') . '.';
    return $text;
  }
  return FALSE;
}

/**
 * Generates location info for department article content type search results.
 *
 * @param object $item
 *   Deparment article node object.
 * @return string
 *   Applicable location information for department article.
 */
function wholefoods_department_article_render_search_results($item){
  $nids = array();
  $language = $item->language;
  if (count($item->field_locations) > 0) {
    $html = '';
    foreach ($item->field_locations[$language] as $tid) {
      $node = store_get_store_from_tid($tid, array('store', 'national_offices'));
      if ($node->type === 'store') {
        $storelang = $node->language;
        $storename = $node->title;
        $city = $node->field_postal_address[$storelang][0]['locality'];
        $state = $node->field_postal_address[$storelang][0]['administrative_area'];
        $html .= t('This information is valid at our ' . $storename . ' store in ' . $city . ', ' . $state);
      }
      elseif($node->type == 'national_offices') {
        switch($node->nid){
          case 95:
            $html = t('This information is valid for our United States stores');
            break;
          case 94296:
            $html = t('This information is valid for our Canadian stores');
            break;
          case 93096:
            $html = t('This information is valid for our United Kingdom stores');
            break;
        }
      }
    }
    return $html;
  }
}

/**
 * Override theme_views_infinite_scroll_pager().
 */
function wholefoods_views_infinite_scroll_pager($variables) {
  $tags = $variables['tags'];
  $limit = isset($variables['limit']) ? $variables['limit'] : 10;
  $view_name = $variables['view_name'];
  $element = isset($variables['element']) ? $variables['element'] : 0;
  $current_display = $variables['current_display'];
  $content_selector = isset($variables['content_selector']) ? $variables['content_selector'] : 'div.view-content';
  $items_selector = isset($variables['items_selector']) ? $variables['items_selector'] : 'div.view-content .views-row';
  $img_path = $variables['img_path'];
  $parameters = $variables['parameters'];
  $quantity = $variables['quantity'];

  $PAGER_CLASS = 'pager';
  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  $li_previous = theme('pager_previous',
    array(
      'text' => (isset($tags[3]) ? $tags[3] : t('more results')),
      'limit' => $limit,
      'element' => $element,
      'interval' => 1,
      'parameters' => $parameters,
    )
  );
  if (empty($li_previous)) {
    $li_previous = "&nbsp;";
  }

  $li_next = theme('pager_next',
    array(
      'text' => (isset($tags[3]) ? $tags[3] : t('››')),
      'limit' => $limit,
      'element' => $element,
      'interval' => 1,
      'parameters' => $parameters,
    )
  );
  if (empty($li_next)) {
    $li_next = "&nbsp;";
  }


  if ($pager_total[$element] > 1) {
    $items[] = array(
      'class' => array('pager-previous'),
      'data' => $li_previous,
    );

    $items[] = array(
      'class' => array('pager-current'),
      'data' => t('@current of @max', array('@current' => $pager_current, '@max' => $pager_max)),
    );

    $items[] = array(
      'class' => array('pager-next'),
      'data' => $li_next,
    );

    $settings = array(
      'views_infinite_scroll' => array(
        array(
          'view_name' => $view_name,
          'display' => $current_display,
          'pager_selector' => 'ul.' . $PAGER_CLASS,
          'next_selector' => 'li.pager-next a:first',
          'content_selector' => $content_selector,
          'items_selector' => $items_selector,
          'img_path' => $img_path,
        ),
      ),
    );
    drupal_add_css(drupal_get_path('module', 'views_infinite_scroll') . '/css/views_infinite_scroll.css');
    drupal_add_js($settings, array('type' => 'setting', 'scope' => JS_DEFAULT));

    // Add Autopager jQuery plugin
    // If libraries module is installed, check for the plugin in libraries dir.
    if (module_exists('libraries') && file_exists(libraries_get_path('autopager') .'/jquery.autopager-1.0.0.js')) {
      drupal_add_js(libraries_get_path('autopager') .'/jquery.autopager-1.0.0.js');
    }
    // else, load it from views_infinite_scroll/js dir.
    else {
      $autopager_module_path = drupal_get_path('module', 'views_infinite_scroll') . '/js/jquery.autopager-1.0.0.js';
      drupal_add_js($autopager_module_path);
    }

    // drupal_add_js(drupal_get_path('module', 'views_infinite_scroll') . '/js/views_infinite_scroll.js');

    return theme('item_list', array('items' => $items, 'title' => NULL, 'type' => 'ul', 'attributes' => array('class' => array($PAGER_CLASS))));
  }
}

/**
 * Implements hook_preprocess_search_api_page_result().
 * Gets various values ready for the $variables for search result template.
 */
function wholefoods_preprocess_search_api_page_result(&$vars) {
  $language = $vars['item']->language;
  $vars['path_alias'] = drupal_get_path_alias($vars['url']['path']);
  $vars['post_info'] = node_type_get_name($vars['item']->type);
  //Add publication date if is a blog post
  if ($vars['item']->type === 'blog_post') {
    $date = format_date(strtotime($vars['info_split']['date']), 'custom', 'F j, Y');
    $vars['post_info'] .= ': ' . $date;
    $blog_author = _wholefoods_get_blog_author($vars['item']->field_blog_author[$language][0]['nid']);
  }
  if ($vars['item']->type === 'service') {
    $stores_info_html = wholefoods_service_render_search_results($vars['item']);
    if (!empty($stores_info_html)) {
      $vars['post_info'] .= ": " . $stores_info_html;
    }
  }
  if ($vars['item']->type === 'department_article') {
    $stores_info_html = wholefoods_department_article_render_search_results($vars['item']);
    if (!empty($stores_info_html)) {
      $vars['post_info'] .= ": " . $stores_info_html;
    }
  }
  //Get Rating
  if (!empty($vars['item']->field_recipe_rating)) {
    $vars['recipe_rating'] = $vars['item']->field_recipe_rating[$language][0]['value'];
    $vars['recipe_rating_percent'] = ($vars['recipe_rating'] / 5) * 100;
  }
  //Get image from node
  if (!empty($vars['item']->field_image)) {
    $vars['media'] = _wholefoods_get_media_path($vars['item']->field_image);
  }
  elseif (!empty($vars['item']->field_hero_image)) {
    $vars['media'] = _wholefoods_get_media_path($vars['item']->field_hero_image);
  }
  elseif (!empty($vars['item']->field_blog_media)) {
    $vars['media'] = _wholefoods_get_media_path($vars['item']->field_blog_media);
  }

  //Get Recipe Course
  $category_ids = array();
  if (!empty($vars['item']->field_recipe_course)) {
    $category_ids['field_recipe_course'] = array();
    foreach ($vars['item']->field_recipe_course[$language] as $tag) {
      $category_ids['field_recipe_course'][] = $tag['tid'];
    }
  }
  // Get Recipe category field.
  if (!empty($vars['item']->field_recipe_category)) {
    //Special diet fields also appear on products
    if ($vars['item']->type === 'recipe') {
      $category_ids['field_recipe_category'] = array();
      foreach ($vars['item']->field_recipe_category[$language] as $tag) {
        $category_ids['field_recipe_category'][] = $tag['tid'];
      }
    }
  }

  // Get Special Diet field.
  if (!empty($vars['item']->field_special_diet)) {
    //Special diet fields also appear on products
    if ($vars['item']->type === 'recipe') {
      $category_ids['field_special_diet'] = array();
      foreach ($vars['item']->field_special_diet[$language] as $tag) {
        $category_ids['field_special_diet'][] = $tag['nid'];
      }
    }
  }

  // Get Blog taxonomy field
  if (!empty($vars['item']->field_blog_taxonomy)) {
    $blogid = $vars['item']->field_blog[$language][0]['nid'];
    //We have to pass blog nid to _wholefoods_get_search_categories to construct category url.
    $category_ids['field_blog_taxonomy'] = array();
    $category_ids['field_blog_taxonomy'][$blogid] = array();
    foreach ($vars['item']->field_blog_taxonomy[$language] as $tag) {
      $category_ids['field_blog_taxonomy'][$blogid][] = $tag['tid'];
    }
  }

  if (!empty($category_ids)) {
    $vars['search_categories'] = _wholefoods_get_search_categories($category_ids);
    if (!empty($blog_author)) {
      $vars['search_categories'] .= $blog_author;
    }
  }

  //Use body summary as snippet, if available
  if (!empty($vars['item']->body[$language][0]['summary'])) {
    $vars['snippet'] = $vars['item']->body[$language][0]['summary'];
  }

  //Strip HTML from snippet
  $allowed_tags = array('a', 'em', 'strong', 'b');
  $vars['snippet'] = filter_xss($vars['snippet'], $allowed_tags);
}

/**
 * Gets path of image for search result in correct image style
 *
 * @param array $field
 *   Image/media field from node object.
 * @return string
 *   Path to image style derivative.
 */
function _wholefoods_get_media_path($field) {
  $fid = $field[LANGUAGE_NONE][0]['fid'];
  $file = file_load($fid);
  $path = image_style_url('search_result', $file->uri);
  return $path;
}

/**
 * Returns formatted html for display from array of term ids / node ids.
 *
 * @param array $category_ids
 *   Array of entity ids for terms and node references of search results.
 * @return string
 *   Links to category pages.
 */
function _wholefoods_get_search_categories($category_ids) {
  $out = t('Categories') . ': ';
  foreach ($category_ids as $origin_field => $entity_ids) {
    switch($origin_field) {
      case 'field_recipe_course':
      case 'field_recipe_category':
        $url = 'recipe/search/%20';
        $entities = entity_load('taxonomy_term', $entity_ids);
        foreach ($entities as $term) {
          $link_options = array(
            'query' => array(
              'f[0]' => $origin_field . ':' . $term->tid,
            )
          );
          $out .= l($term->name, $url, $link_options) . ', ';
        }
        break;
      case 'field_special_diet':
        $url = 'recipe/search/%20';
        $entities = entity_load('node', $entity_ids);
        foreach ($entities as $node) {
          $link_options = array(
            'query' => array(
              'f[0]' => $origin_field . ':' . $node->nid,
            )
          );
          $out .= l($node->title, $url, $link_options) . ', ';
        }
        break;
      case 'field_blog_taxonomy':
        $blog_id = key($entity_ids);
        $terms = entity_load('taxonomy_term', $entity_ids[$blog_id]);
        foreach ($terms as $term) {
          $url = 'blog/category/' . $blog_id . '/' . $term->name;
          $out .= l($term->name, $url) . ', ';
        }
        break;
    }
  }
  //Strip trailing spaces and comma
  return trim($out, ' \t\n\r\0\x0B,');
}

/**
 * Gets path of person node from blog author field
 *
 * @param int $nid
 *   Node id of person node.
 * @return string
 *   Link to person node.
 */
function _wholefoods_get_blog_author($nid) {
  $person = node_load($nid);
  $url = drupal_get_path_alias('node/' . $person->nid);
  $out = ', <a href="/' . $url . '">' . $person->title . '</a>';
  return $out;
}

/**
 * Helper function to build a Load More pager.
 */
function _wholefoods_load_more_pager($vars, $text = 'Load More') {
  $more = theme('pager_next', array(
    'text' => (t($text)),
    'element' => $vars['element'],
    'interval' => 1,
    'parameters' => $vars['parameters'],
  ));
  $output = theme('item_list', array(
    'items' => array($more),
    'attributes' => array('class' => array('pager')),
  ));
  $output = str_replace('page=1&amp;', '', $output);
  return $output;
}

/**
 * Overrides theme_pager__VIEW__DISPLAY().
 */
function wholefoods_pager__store_lookup_by_address__store_locations_block($vars) {
  global $wfm_mobile_store_list_hide_pager;
  if (!$wfm_mobile_store_list_hide_pager) {
    //return _wholefoods_load_more_pager($vars, 'Load more stores');
    $vars['custom_pager_class'] = 'custom-pager';
    return theme('pager', $vars, 5);
  }
}

/*
 * theme_pager();
 */
function wholefoods_pager ($variables, $quantity = 7) {
  $tags = $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  global $pager_page_array, $pager_total, $theme;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  $pager_pre_max = $pager_max - 1;
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  $li_previous = theme('pager_previous', array('text' => t('« previous'), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_next = theme('pager_next', array('text' => t('» next'), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_first = theme('pager_first', array('text' => 1, 'element' => $element, 'parameters' => $parameters));
  $li_last = theme('pager_last', array('text' => $pager_max, 'element' => $element, 'parameters' => $parameters));
  $pager_pre_max = theme('pager_last', array('text' => $pager_pre_max, 'element' => $element, 'parameters' => $parameters));

  // First-page link display condition
  $show_first = ($i > 1) ? true : false;

  if ($pager_total[$element] > 1) {
    if ($li_previous) {
      $items[] = array(
        'class' => array('pager-previous'),
        'data' => $li_previous,
      );
    }

    if ($show_first && $li_first) {
      $items[] = array(
        'class' => array('pager-first'),
        'data' => $li_first,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 2) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_previous', array('text' => $i, 'element' => $element, 'interval' => ($pager_current - $i), 'parameters' => $parameters)),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => array('pager-current'),
            'data' => '<span>' . $i . '</span>',
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_next', array('text' => $i, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
    }

    // Last-page link display condition
    $show_last = ($pager_max > ($i-1)) ? true : false;

    // End generation.
    if ($show_last && $li_last && $pager_pre_max) {
      $items[] = array(
        'class' => 'pager-item',
        'data' => $pager_pre_max,
      );
      $items[] = array(
        'class' => 'pager-last',
        'data' => $li_last,
      );
    }

    if ($li_next) {
      $items[] = array(
        'class' => array('pager-next'),
        'data' => $li_next,
      );
    }
    $custom_pager_classes = array('pager');
    if (isset($variables['custom_pager_class'])) {
      $custom_pager_classes[] = $variables['custom_pager_class'];
    }
    return '<h2 class="element-invisible">' . t('Pages') . '</h2>' . theme('item_list', array(
      'items' => $items,
      'attributes' => array('class' => $custom_pager_classes),
    ));
  }
}

/*
 * theme_pager_previous()
 */
function wholefoods_pager_previous($variables) {
  $text = $variables['text'];
  $element = $variables['element'];
  $interval = $variables['interval'];
  $parameters = $variables['parameters'];
  global $pager_page_array;
  $output = '';

  // If we are anywhere but the first page
  if ($pager_page_array[$element] > 0) {
    $page_new = pager_load_array($pager_page_array[$element] - $interval, $element, $pager_page_array);
    // If the previous page is the first page, mark the link as such.
    if ($page_new[$element] == 0) {
      $output = theme('pager_first', array('text' => $text, 'element' => $element, 'parameters' => $parameters));
    }
    // The previous page is not the first page.
    else {
      $output = theme('pager_link', array('text' => $text, 'page_new' => $page_new, 'element' => $element, 'parameters' => $parameters));
    }
  } elseif ($pager_page_array[$element] == 0) {
    $output = '<span  class="pager-inactive">' . t('« previous') . '</span>';
  }
  return $output;
}

/*
 * theme_pager_next()
 */
function wholefoods_pager_next($variables) {
  $text = $variables['text'];
  $element = $variables['element'];
  $interval = $variables['interval'];
  $parameters = $variables['parameters'];
  global $pager_page_array, $pager_total;
  $output = '';

  // If we are anywhere but the last page
  if ($pager_page_array[$element] < ($pager_total[$element] - 1)) {
    $page_new = pager_load_array($pager_page_array[$element] + $interval, $element, $pager_page_array);
    // If the next page is the last page, mark the link as such.
    if ($page_new[$element] == ($pager_total[$element] - 1)) {
      $output = theme('pager_last', array('text' => $text, 'element' => $element, 'parameters' => $parameters));
    }
    // The next page is not the last page.
    else {
      $output = theme('pager_link', array('text' => $text, 'page_new' => $page_new, 'element' => $element, 'parameters' => $parameters));
    }
  } elseif ($pager_page_array[$element] == $pager_total[$element] - 1) {
      $output = '<span  class="pager-inactive">' . t('next »') . '</span>';
  }

  return $output;
}

/**
 * Construct a spelling suggestion for search.
 *
 * @param string $term
 *   Term being searched for.
 * @param array $results
 *   Array of search results from Solr
 * @return string
 *   HTML for search term spelling suggestion
 */
function wholefoods_search_spelling_suggestion($term, $results) {
  if(!$results['search_api_solr_response']->spellcheck->suggestions->correctlySpelled) {
    $out = '<br />';
    //If no results are found and we have a spellcheck suggestion
    if ($results['search_api_solr_response']->response->numFound == 0) {
      $out .= t('You typed !term but we’re going to go out on a limb and guess
        that you meant to search for !suggestion. How about these results? ',
        array(
          '!term' => '<em><strong>' . $term . '</strong></em>',
          '!suggestion' =>'<i><strong>' . $results['search_api_solr_response']->spellcheck->
            suggestions->{$term}->suggestion[0]->word . '</strong></i>',
        )
      );
      $link_text = t('Search for <em>!term</em> instead', array('!term' => $term));
      $out .= l($link_text, current_path(), array('html' => true, 'query' => array('spellcheck' => 'false')));
    }
    //If results are found and we have a spellcheck suggestion
    else {
      //Why parse URL? Because of our various different search forms.
      //Recipe search, product search, etc...
      $current_search = current_path();
      $url_segments = explode('/', $current_search);
      $spelling_suggestion = $results['search_api_solr_response']->spellcheck->
        suggestions->{$term}->suggestion[0]->word;
      if (!empty($spelling_suggestion)) {
        $url_segments[count($url_segments) - 1] = $spelling_suggestion;
        $new_url = '';
        foreach ($url_segments as $url_segment) {
          $new_url .= '/' . $url_segment;
        }
        $out .= t('Did you mean to search for !spellcheck instead?',
          array(
            '!spellcheck' => l($spelling_suggestion, trim($new_url, '/')),
          )
        );
      }
    }
  }
  return $out;
}


function wholefoods_preprocess_views_view(&$vars) {
    if ($vars['view']->name == 'recipe_header') {
        drupal_add_js(array(
            'CToolsModal' => array(
                'modalSize' => array(
                    'height' => 350,
                ),
            ),
        ), 'setting');
    }
}
