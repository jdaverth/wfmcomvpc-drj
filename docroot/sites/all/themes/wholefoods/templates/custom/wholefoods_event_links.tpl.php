<?php
global $user;
$node = node_load($nid);
$query_string = 'subject=' . htmlentities(rawurlencode($node->title) . '&body=' . urlencode(url('node/' . $nid, array('absolute' => TRUE))));
?>

<ul>
  <li>
    <?php print l(t('print'), 'print/' . drupal_lookup_path('alias', 'node/' . $nid), array('attributes' => array('class' => array('print')))); ?>
  </li>
  <li>
    <a class="mailto" href="mailto:?<?php print $query_string; ?>"></a>
  </li>
</ul>