<?php

/**
 * @file
 * Default module/theme implementation to display Facebook wall posts.
 *
 * Available variables:
 * - $posts: Object containing an array of Facebook posts and paging URLs.
 *
 */
?>
<?php $TRIM_LEN = 150; ?>
<h2>Facebook</h2>
<div id="social-feed-container">
  <?php foreach ($posts->data as $count => $post): ?>
  <div class="facebook-post-fetcher-item">
    <?php // Print user picture ?>
      <div class="fb-user-img">
        <img src="<?php print $post->from->picture?>" />
      </div>
    <?php // Link to Facebook Profile. ?>
    <h3><a href="https://facebook.com/profile.php?id=<?php echo $post->from->id; ?>"><?php echo $post->from->name; ?></a></h3>
    <?php // Display message. ?>
    <?php if (isset($post->message)): ?>
      <p class="facebook-post-fetcher-message"><?php print $post->message; ?></p>
    <?php endif; ?>
  </div>
  <?php endforeach;?>
  <a href="https://facebook.com/profile.php?id=<?php echo $posts->data[0]->from->id; ?>">View <?php echo $posts->data[0]->from->name; ?> on Facebook</a>
</div>
