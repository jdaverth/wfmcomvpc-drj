<?php
  global $user;
  $the_node = node_load($nid);
  $recipeListApi = new WFMApiRecipeList();
?>

<ul>
  <li class="user_recipe_links">
  <?php if ($user->uid > 0): ?>
  <?php if ($recipeListApi->isInList($user->uid, $nid) === FALSE): ?>
    <?php print l(t('add to recipe box'), 'add_to_recipe_box/' . $nid, array(
      'attributes' => array('class' => array('add-to-shopping-list'))
    )); ?>
  <?php else: ?>
    <?php print l(t('remove from recipe box'), 'action/recipe/remove/' . $nid, array(
      'attributes' => array('class' => array('add-to-shopping-list')),
      'query' => array('destination' => 'node/' . $nid)
    )); ?>
  <?php endif; ?>
  <?php print l(t('view saved recipes'), 'user/recipe_box', array(
    'attributes' => array('class' => array('view-saved-recipes'))
  )); ?>
  <?php else: ?>
    <!-- @TODO: Per QD-4178, temporarily remove Janrain modal capability until site-wide SSL is implemented. Add this class back in to the link: capture_modal_open -->
  <?php print l(t('Log in or create an account to save favorite recipes to your Recipe Box'), 'signin', array(
    'attributes' => array('class' => array('shopping-list-login'))
  )); ?>
  <?php endif; ?>
  </li>
  <li class="user_recipe_print">
    <?php print l(t('print'), 'modals/nojs/print/' . $nid, array(
      'attributes' => array(
        'class' => array('print', 'ctools-use-modal', 'recipe-print'),
        'data-title' => $the_node->title,
        'title' => t('Print recipe'),
        'rid' => $the_node->field_rid[LANGUAGE_NONE][0]['value'],
      )));
    ?>
  </li>
  <li class="user_recipe_email">
  <?php
    $recipe_email_text = "I thought you might like this recipe from Whole Foods Market at ";
    $recipe_email_text .= url("node/" . $nid, array('absolute' => TRUE));
    $recipe_email_text .= "\n\nSign up for our newsletters for even more Whole Foods Market recipes: ";
    $recipe_email_text .= url("newsletters/", array('absolute' => TRUE, 'query' => array('promo' => 'recipes')));
    print l(t('email'), 'mailto:', array(
      'absolute' => true,
      'attributes' => array(
        'class' => array('email'),
        'title' => t('Email recipe'),
      ),
      'query' => array(
        'subject' => "A recipe for you: "  . ($the_node->title) . " | WholeFoodsMarket.com",
        'body' =>  $recipe_email_text,
      ),
    ));
  ?>
  </li>
</ul>
