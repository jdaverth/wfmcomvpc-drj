<?php

/**
 * @file
 * Default theme implementation for field collection items.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) field collection item label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-field-collection-item
 *   - field-collection-item-{field_name}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
$new_content = array_shift($content);
if ($new_content['#field_name'] == 'field_related_content_node') {
	$view_elements = array('related_content', 'node', $new_content['#items'][0]['nid']);
	$content = views_embed_view($view_elements[0], $view_elements[1], $view_elements[2]);
} else {
	$view_id = $new_content['#items'][0]['view_id'];
	$view_elements = explode(':', $view_id);
  $content = views_embed_view($view_elements[0], $view_elements[1]);
}
// Decide if we want to print a view or a node.

?>
<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="content"<?php print $content_attributes; ?>>
    <?php
      print $content;
    ?>
  </div>
</div>
