<?php
// $Id: page.tpl.php,v 1.47 2010/11/05 01:25:33 dries Exp $

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 */
$breadcrumb = wholefoods_display_breadcrumb();
global $has_heading_copy;
$extra_class= "";
if (store_get_user_store()){$extra_class="has_store";}
$language = $variables['node']->language;
$state = $variables['node']->field_postal_address[$language]['0']['administrative_area'];
$state_list = _local_get_state_list(TRUE);
$state_name = $state_list[$state];
?>
  <div id="page-wrapper" class="layout_one <?php print $extra_class; ?>"><div id="page">

    <div id="header" class="header"><div class="section clearfix">

      <!--<?php //if ($logo): ?>
        <a href="<?php //print $front_page; ?>" title="<?php //print t('Home'); ?>" rel="home" id="logo">
          <img src="<?php //print $logo; ?>" alt="<?php //print t('Home'); ?>" />
        </a>
      <?php //endif; ?>-->

      <?php if ($site_name || $site_slogan): ?>
        <div id="name-and-slogan">
          <?php if ($site_name): ?>
            <?php if ($title): ?>
              <div id="site-name"><strong>
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
              </strong></div>
            <?php else: /* Use h1 when the content title is empty */ ?>
              <h1 id="site-name">
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
              </h1>
            <?php endif; ?>
          <?php endif; ?>

          <?php if ($site_slogan): ?>
            <div id="site-slogan"><?php print $site_slogan; ?></div>
          <?php endif; ?>
        </div> <!-- /#name-and-slogan -->
      <?php endif; ?>

      <?php print render($page['header']); ?>

    </div></div> <!-- /.section, /#header -->

    <div id="navigation" class="nav">
      <div class="nav-tab"><div class="nav-tab-copy"></div><div class="nav-tab-arrow"></div></div>
      <?php if ($logo): ?>
         <!-- <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
          </a>-->
        <?php endif; ?>
      <?php print render($page['navigation']); ?>
    </div>

     <?php if ($page['marquee']): ?>
     <?php print render($page['marquee']); ?>
     <?php endif; ?>



    <div id="main-wrapper" class="<?php if ($has_heading_copy) { print 'push-down'; } ?>"><div id="main" class="clearfix">
        <?php print $messages; ?>

        <?php if (FALSE): ?>
        <?php print render($title_prefix); ?>
        <?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
        <?php print render($title_suffix); ?>
        <?php endif; ?>
        <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>

     <?php if ($page['content_top']): ?>
      <div id="content-top"><?php print render($page['content_top']); ?></div>
     <?php endif; ?>

      <div id="content" class="column with-sidebar"><div class="section">
        <?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
        <a id="main-content"></a>
        <div class="torn-pod-header"></div>
        <div class="torn-pod-content">
          <?php if ($page['content_sidebar']): ?>
            <div class="content-sidebar">
              <?php print render($page['content_sidebar']); ?>
            </div>
          <?php endif; ?>
          <?php if ($breadcrumb): ?>
          <?php print $breadcrumb; ?>
          <?php endif; ?>

          <div class="contentbox <?php if ($page['content_sidebar']) { print ' with-inner-sidebar'; }?>">
          <?php print render($page['content']); ?>
          <?php print $feed_icons; ?></div>

        </div>
        <div class="torn-pod-footer"></div>

      </div></div> <!-- /.section, /#content -->

      <?php if ($page['sidebar_first']): ?>
        <div id="sidebar-first" class="column sidebar"><div class="section">
          <div class="torn-pod-header"></div>
          <div class="torn-pod-content">
            <h3>Sidebar First</h3>
            <?php print render($page['sidebar_first']); ?>
          </div>
          <div class="torn-pod-footer"></div>
        </div></div> <!-- /.section, /#sidebar-first -->

      <?php endif; ?>

      <div id="sidebar-second" class="column sidebar">

         <div class="section">
          <div class="torn-pod-header"></div>
          <div class="torn-pod-content">
            <div class="sidebar_vendors">
              <h3>Vendors in <?php echo $state_name; ?></h3>
              <?php 
                $view = views_embed_view('local_vendor_profiles', 'local_vendors_sidebar', $state);
                echo $view;
              ?>
            </div><!--sidebar_vendors-->
            <?php print render($page['sidebar_second']); ?>
          </div><!--torn-pod-content-->
          <div class="torn-pod-footer"></div>
        </div>

      </div> <!-- /.section, /#sidebar-second -->
      <div class="clearfix"></div>

     <?php if ($page['content_bottom']): ?>
      <div id="content-bottom"><?php print render($page['content_bottom']); ?></div>
     <?php endif; ?>
    </div></div> <!-- /#main, /#main-wrapper -->




  </div><div class="push"></div></div> <!-- /#page, /#page-wrapper -->
<div id="footer"><div class="section">
      <?php print render($page['footer']); ?>
    </div></div> <!-- /.section, /#footer -->
  <!-- force install of chrome frame if browser is less than ie7 -->
  <!--[if lt IE 7 ]>
    <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
    <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
  <![endif]-->
