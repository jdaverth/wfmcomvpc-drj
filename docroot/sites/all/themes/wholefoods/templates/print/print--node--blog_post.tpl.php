<?php

/**
 * @file
 * Default print module template
 *
 * @ingroup print
 */
	$print['content']  = '<div id="node-' . $node->nid . '" class="node node-blog clearfix"><h2 property="dc:title" datatype="">' . $node->title . '</h2>';
	$print['content'] .= '<div class="content">';
	$print['content'] .= drupal_render(field_view_field('node', $node, 'body'));
	$print['content'] .= '</div></div>';

$params = drupal_get_query_parameters();
if ($params['css']) {
  $classes .= ' ' . drupal_clean_css_identifier(check_plain($params['css']));
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $print['language']; ?>" xml:lang="<?php print $print['language']; ?>">
  <head>
    <?php print $head; ?>
    <base href='<?php print $url ?>' />
    <title><?php print $print_title; ?></title>
    <?php print $scripts; ?>
    <?php if (isset($sendtoprinter)) print $sendtoprinter; ?>
    <?php print $robots_meta; ?>
    <?php if (theme_get_setting('toggle_favicon')): ?>
      <link rel='shortcut icon' href='<?php print theme_get_setting('favicon') ?>' type='image/x-icon' />
    <?php endif; ?>
    <?php print $css; ?>
  </head>
  <body class="print-theme <?php print $classes; ?>">
    <?php if ($print_logo): ?>
      <div class="print-logo"><?php print $print_logo; ?></div>
    <?php endif; ?>
    <?php if (!empty($print['message'])) {
      print '<div class="print-message">'. $print['message'] .'</div><p />';
    } ?>
    <p />
    <hr class="print-hr" />
    <div class="print-content"><?php print $print['content']; ?></div>
    <hr class="print-hr" />
    <?php if ($sourceurl_enabled): ?>
      <div class="print-source_url">
        <?php print theme('print_sourceurl', array('url' => $source_url, 'node' => $node, 'cid' => $cid)); ?>
      </div>
    <?php endif; ?>
    <div class="print-links"><?php print theme('print_url_list'); ?></div>
    <?php print $footer_scripts; ?>
  </body>
</html>
