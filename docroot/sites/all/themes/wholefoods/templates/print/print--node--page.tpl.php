<?php

/**
 * @file
 * Default print module template
 *
 * @ingroup print
 */
if ($node->nid == 18550) {

  $print['content'] = '<div id="node-18550" class="node node-page clearfix" about="/sales-flyer" typeof="foaf:Document"><h2 property="dc:title" datatype="">Sales Flyer</h2><div class="content">';

  // Add in the block
  $print['content'] .= _products_sale_items(); 

  $print['content'] .= '</div></div>';
	
	$print['footer_message'] = "";
	
} elseif ($node->type == 'blog_post') {
	$print['content']  = '<div id="node-' . $node->nid . '" class="node node-blog clearfix"><h2 property="dc:title" datatype="">' . $node->title . '</h2>';
	$print['content'] .= '<div class="content">';
	$print['content'] .= drupal_render(field_view_field('node', $node, 'body'));
	$print['content'] .= '</div></div>';
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $print['language']; ?>" xml:lang="<?php print $print['language']; ?>">
  <head>
    <?php print $print['head']; ?>
    <?php print $print['base_href']; ?>
    <title><?php print $print['title']; ?></title>
    <?php print $print['scripts']; ?>
    <?php print $print['sendtoprinter']; ?>
    <?php print $print['robots_meta']; ?>
    <?php print $print['favicon']; ?>
    <?php print $print['css']; ?>
  </head>
  <body>
    <?php if (!empty($print['message'])) {
      print '<div class="print-message">'. $print['message'] .'</div><p />';
    } ?>
    <div class="print-logo"><?php print $print['logo']; ?></div>
    <div class="print-site_name"><?php print $print['site_name']; ?></div>
    <p />
    <div class="print-breadcrumb"><?php print $print['breadcrumb']; ?></div>
    <hr class="print-hr" />
    <div class="print-content"><?php print $print['content']; ?></div>
    <hr class="print-hr" />
    <div class="print-source_url"><?php print $print['source_url']; ?></div>
    <div class="print-links"><?php print $print['pfp_links']; ?></div>
    <?php //print $print['footer_scripts']; ?>
  </body>
</html>
