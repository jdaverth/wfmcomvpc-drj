<?php

/**
 * @file
 * Default theme implementation for displaying search results.
 *
 * This template collects each invocation of theme_search_result(). This and
 * the child template are dependent to one another sharing the markup for
 * definition lists.
 *
 * Note that modules may implement their own search type and theme function
 * completely bypassing this template.
 *
 * Available variables:
 * - $search_results: All results as it is rendered through
 *   search-result.tpl.php
 * - $module: The machine-readable name of the module (tab) being searched, such
 *   as "node" or "user".
 *
 *
 * @see template_preprocess_search_results()
 *
 * @ingroup themeable
 */
?>
<?php if ($search_results): ?>
  <p class="search_description">
    <?php
    print t('Showing results !start - !end of !total', array(
      '!start' => $start,
      '!end' => $end,
      '!total' => $results['result count'],
    ));
    if ($term != '' && $term != '%20') {
      print t(' for !term', array(
        '!term' => '<span class="search_term">' . check_plain($term) . '</span>'
      ));
    }
    print wholefoods_search_spelling_suggestion($term, $results);
    ?>
  </p>
  <?php print render($spellcheck); ?>
  <h2><?php print t('Search results');?></h2>
  <ol class="search-results">
    <?php print render($search_results); ?>
  </ol>
  <?php print render($pager); ?>
<?php else : ?>

  <h2><?php print t('Search Results for') . ' "' . check_plain($term) . '"'; ?></h2>
  <?php
    $block = block_load('wfm_blocks', 'wfm_blocks_no_search_results');
    $block = _block_get_renderable_array(_block_render_blocks(array($block)));
    echo drupal_render($block);
  ?>
<?php endif; ?>
