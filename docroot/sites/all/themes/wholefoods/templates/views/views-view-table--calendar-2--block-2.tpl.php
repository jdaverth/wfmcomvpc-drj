<div class="events">
  <div class="torn-pod-header"></div>
  <div class="torn-pod-content">
  <?php foreach ($result as $id => $event): ?>
  <?php $row = $rows[$id]; ?>
  <?php $date = $event->field_field_event_date[0]['raw']; ?>
  <div class="event">
    <div class="date-block">
      <div class="weekday">
        <?php print date('D', $date['value']); ?>
      </div>
      <div class="monthday">
        <?php print date('d', $date['value']); ?>
      </div>
    </div>
    <div class="detail-block">
      <div class="brief">
        <div class="title">
          <?php print $row['title']; ?>
        </div>

        <div class="time-loc">
   <?php print date('g:i A', $date['value']); ?> - <?php print date('g:i A', $date['value2']); ?> @ <?php print $row['field_location_name']; ?>
        </div>

        <div class="expand">
          <a href="">+</a>
        </div>

      </div>


      <div class="detail">
        <div class="body">
          <div class="title">
            <?php print $row['title']; ?>
          </div>

          <div class="time-loc">
            <div>
              <?php print date('l, F jS', $date['value']); ?>
            </div>

            <?php print date('g:i A', $date['value']); ?> -
            <?php print date('g:i A', $date['value2']); ?><br>
            <?php print $row['field_location_name']; ?>
            <?php print $row['field_price']; ?>
          </div>
          <div class="clearfix"></div>
          <div class="description">
            <?php print $row['body']; ?>
          </div>

        </div>
        <div class="image">
          <?php print $row['field_event_image']; ?>
        </div>
        <div class="clearfix"></div>
        <div class="collapse">
          <a href="">-</a>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <?php endforeach; ?>
  </div>
  <div class="torn-pod-footer"></div>
</div>
