<?php

$grid = array();
$grid_row = 1;
$grid_col = 1;

foreach ($rows as $k => $row) {
  if (array_key_exists('field_freeform', $row) && strlen($row['field_freeform']) !== 0) {
    if ($grid_col != 1) {
      $grid_row++;
    }
    $grid[$grid_row][1] = array('id' => $k, 'content' => $row, 'class' => 'freeform');

    $grid_row++;
    $grid_col = 1;
  } else {
    $grid[$grid_row][$grid_col] = array('id' => $k, 'content' => $row, 'class' => '');

    if ($grid_col == 3) {
      $grid_row++;
      $grid_col = 1;
    }
    else {
      $grid_col++;
    }
  }
}

?>

<?php foreach ($grid as $row): ?>
	<?php $classes = array() ?>
		<?php
			if (count($row) == 3) {
				$classes[] = 'col-width-one';
        $style_name = 'small_pod';
			}
			elseif (count($row) == 2) {
				$classes[] = 'col-width-two';
        $style_name = 'promo_pod_large';
			}
			else { $classes[] = 'col-width-three';
        $style_name = 'promo_pod_large';
			}
		?>
		<div class="views-row <?php print implode(' ', $classes); ?>">
			<?php foreach ($row as $index => $item): ?>

			<div class="views-col <?php if ($index == 1) { print ' views-row-first'; } if ($index == count($row)) { print ' views-row-last'; } print ' ' . $item['class']; ?>">


				<?php if ($item['class'] == 'freeform'): ?>
					<div class="<?php print $field_classes['field_freeform'][$item['id']]; ?>">
						<?php print $item['content']['field_freeform']; ?>
 					</div>
				<?php else: ?>
					<div class="torn-pod-header"></div><?php //torn pod header?>
        <div class="torn-pod-content"><?php //torn pod content?>
							<?php foreach ($item['content'] as $field => $content): ?>

        <?php if ($field == 'field_event_image'): ?>
        <?php $image = file_load($content); ?>
        <?php $content = theme_image_style(array('style_name' => $style_name, 'path' => $image->uri, 'height' => NULL, 'width' => NULL)); ?>
        <?php endif; ?>

	  							<div class="<?php print $field_classes[$field][$item['id']]; ?>"><?php print $content; ?></div>
							<?php endforeach; ?>
						</div>
					<div class="torn-pod-footer"></div><?php //torn pod footer?>
  					<div class="clearfix"></div>
				<?php endif; ?>
			</div>
			<?php endforeach; ?>
		</div><?php //end of views-row div?>

<?php endforeach; ?>
