<h3><?php print t('Events At Nearby Stores'); ?></h3>
<?php $date_format_string = 'g:i A'; ?>
<div class="events">
<?php foreach ($result as $id => $event): ?>
<?php $row = $rows[$id]; ?>
<?php $date = $event->field_field_event_date[0]['raw']; ?>

<div class="event">
  <div class="left">
    <div class="image">
      <?php print $row['field_event_image']; ?>
    </div>

    <div class="links detail">
<?php
  $date = date('Y-m', (int) $event->field_field_event_date[0]['raw']['value']);
?>
      <a href="/events?store=<?php print $event->field_locations_acl[0]['raw']['tid']; ?>&date=<?php
      print $date; ?>#node-<?php print $event->nid . '-' . $event->field_field_event_date[0]['raw']['value']; ?>"
      class="button"><?php print t('View Store Calendar'); ?></a>
    </div>
  </div>

  <div class="right">
    <div class="title">
      <?php print $row['title']; ?>
    </div>
    <div class="clearfix"></div>
		<div class="store">
		  <?php print $row['title_1']; ?>
		</div>

    <div class="time-loc detail">
		<?php $date = $event->field_field_event_date[0]['raw']; ?>
      <div>
        <?php print date('l, F jS', $date['value']); ?>
      </div>
   <?php $date_string = date($date_format_string, $date['value']); ?>
   <?php if ($date['value'] != $date['value2']): ?>
   <?php $date_string .= ' - ' . date($date_format_string, $date['value2']); ?>
   <?php endif; ?>

   <?php print $date_string; ?><br>
      <?php print $row['field_location_name']; ?>,
      <?php print $row['field_price']; ?>
    </div>

   <div class="clearfix"></div>
    <div class="description detail">
      <?php print $row['body']; ?>
    </div>

    <div class="description brief">
      <?php print $row['body_1']; ?>
    </div>


    <div class="link brief">
      <div class="expand"><a href="" class="button"><?php print t('View Event'); ?></a></div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>
<div class="clearfix"></div>
<?php endforeach; ?>
</div>
