<?php foreach ($rows as $row): ?>


<div class="list-row">
  <div class="brief">
    <div class="image">
     <?php print $row['field_body_list_image']; ?>
    </div>
    <div class="float-right">
      <div class="title">
        <?php print $row['field_body_list_title']; ?>
      </div>

      <div class="copy">
        <?php print $row['field_body_list_short_copy']; ?>
      </div>

      <div class="CTA">
        <?php
          // Checks to see if the expand/collapse checkbox is checked
          if($row['field_body_list_expand'])
          {
            // if checkbox is checked, make <a> expand/collapse button
            print "<a class='expander' href='#'>". $row['field_body_list_cta_copy'] . "</a>";
          }
          else {
            // makes <a> a link if checkbox isn't checked
            if($row['field_body_list_cta_link_node'] != '')
            {
              print "<a href='".$row['field_body_list_cta_link_node']."'>".$row['field_body_list_cta_copy']."</a>";
            }
            else {
              print "<a href='".$row['field_body_list_cta_link_ext']."'>".$row['field_body_list_cta_copy']."</a>";
            }
          }
        ?>
      </div>
    </div>
  </div>

  <div class="expanded">
    <div>
      <a href="#" class="collapser">Collapse</a>
    </div>

    <div class="title">
      <?php print $row['field_body_list_title']; ?>
    </div>

    <div class="image">
      <?php print $row['field_body_list_image']; ?>
    </div>

    <div class="copy">
      <?php print $row['field_body_list_expanded_copy']; ?>
    </div>
  </div>
  <div class="clearfix"></div>
</div>
<?php endforeach; ?>