<?php
// Rows include...
//   title
//   field_marquee_media
//   field_marquee_url
//   field_heading_copy
//
// Uncomment to see the data structure...
// dsm($rows);
global $profile;
$store = $_COOKIE['local_store'];
$store_id = $_COOKIE['local_store_id'];
// watchdog('views-view-table--marquee-scroller--block.tpl.php', 'local_store_id = @store_id', array('@store_id' => $store_id));
?>
<div class="flex-container">
  <div class="flexslider">
    <ul class="slides">
      <?php foreach ($rows as $row): ?>
        <li class="marquee-container">

            <div class="marquee-content">
              <?php print $row['field_heading_copy']; ?>
              <div class="learn-more"> <?php print wfm_marquee_destination_url($row); ?> </div>
              <div class="marquee-nav">
                <a class="prev1" href="javascript:;">Prev</a> <a class="next1" href="javascript:;">Next</a>
                <div class="pager-nav"></div>
              </div>
            </div>
            <img src="<?php print image_style_url('marqee', $row['field_marquee_media_1']); ?>">
        </li>
      <?php endforeach; ?>
    </ul>
  </div>
</div>
