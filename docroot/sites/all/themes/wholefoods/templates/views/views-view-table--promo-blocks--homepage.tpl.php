<?php
$grid = array();
$grid_row = 1;
$grid_col = 1;

foreach ($rows as $k => $row) {
  $res = $view->result[$k];
  if (strlen($row['field_freeform']) !== 0) {
    if ($grid_col != 1) {
      $grid_row++;
    }
    $grid[$grid_row][1] = array('res' => $res, 'id' => $k, 'content' => $row, 'class' => 'freeform');

    $grid_row++;
    $grid_col = 1;
  }
}
?>

<?php foreach ($grid as $row): ?>
	<?php $classes = array() ?>
		<?php
			if (count($row) == 3) {
				$classes[] = 'col-width-one';
        $style_name = 'small_pod';
			}
			elseif (count($row) == 2) {
				$classes[] = 'col-width-two';
        $style_name = 'promo_pod_large';
			}
			else { $classes[] = 'col-width-three';
        $style_name = 'promo_pod_large';
			}
		?>
		<div class="views-row <?php print implode(' ', $classes); ?>">
			<?php foreach ($row as $index => $item): ?>

        <?php $title_class = 'pod_' . strtolower(str_replace(' ', '_', $item['res']->node_title)); ?>

        <?php if (!isset($item['content']['nid'])) { $item['content']['nid'] = '-1'; } ?>
			<div class="views-col <?php print $title_class; ?> <?php if ($index == 1) { print ' views-row-first'; } if ($index == count($row)) { print ' views-row-last'; } print ' ' . $item['class']; ?>">

				<?php if ($item['class'] == 'freeform'): ?>
					<div class="<?php print $field_classes['field_freeform'][$item['id']]; ?>">
						<?php print $item['content']['field_freeform']; ?>
 					</div>
				<?php else: ?>
					<div class="torn-pod-header"></div><?php //torn pod header?>
						  <div class="torn-pod-content promo-ref" ref="<?php print $item['content']['nid']; ?>" title="<?php print $item['res']->node_title; ?>"><?php //torn pod content?>
						<?php unset($item['content']['nid']); ?>
							<?php foreach ($item['content'] as $field => $content): ?>
        <?php if ($field == 'field_promo_media'): ?>
        <?php if (strlen($content) == 0) { continue; } ?>
        <?php $image = file_load($content); ?>
        <?php $content = theme_image_style(array('style_name' => $style_name, 'path' => $image->uri, 'height' => NULL, 'width' => NULL)); ?>
        <?php endif; ?>

	  							<div class="<?php print $field_classes[$field][$item['id']]; ?>"><?php print $content; ?></div>
							<?php endforeach; ?>
						</div>
					<div class="torn-pod-footer"></div><?php //torn pod footer?>
  					<div class="clearfix"></div>
				<?php endif; ?>
			</div>
			<?php endforeach; ?>
      <div class="clearfix"></div>
		</div><?php //end of views-row div?>

<?php endforeach; ?>
