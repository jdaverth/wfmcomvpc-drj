<?php

$list = array();
$state_list = _local_get_state_list(TRUE);

foreach ($rows as $row) {
  if ($row['nid'] == 0) {
    continue;
  }

  if ($row['field_postal_address_administrative_area'] == '') {
    $row['field_postal_address_administrative_area'] = 'ZOther';
  } elseif ($row['field_postal_address_administrative_area'] == 'GB') {
		$row['field_postal_address_administrative_area'] = 'UK';
  }
  $options['list'][strtoupper($row['field_postal_address_administrative_area'])][] = $row;
}

foreach ($options['list'] as $k => $state) {
  usort($state, '_store_usort_customer_service');
  $options['list'][$k] = $state;
}

?>
<div class="torn-pod-header"></div>
<div class="torn-pod-content">
  <div class="header-content">
  <?php
    print t("Individual stores are responsible for, respond to and correct the vast majority of issues. If you have a concern with a product or service, please contact a Shift Leader at the store where the problem arose.");
  ?>
  </div>
  <div class="store-select-box">
    <h4><?php print t('Choose your state:') ?></h4>
    <select id="phone_number_state_list">
    <option value="undefined">Select State/Province</option>
    <?php foreach ($state_list as $k => $v): ?>
    <?php if (!array_key_exists($k, $options['list'])) { continue; } ?>
     <option value="<?php print $k ?>"><?php print $v ?></option>
    <?php endforeach; ?>
    </select>
    <br /><br />
    <h4><?php print t('Choose your store:') ?></h4>
    <select id="phone_number_store_list">

    <option number="0" value="empty">Select Store</option>
    <?php foreach ($options['list'] as $state => $stores): ?>
     <optgroup label="<?php print $state_list[$state] ?>">
     <?php foreach ($stores as $row):
             if ( !$row['field_opening_soon'] ) :

               $val = $state . '__' . $row['field_postal_address_locality'] . '__' . $row['nid']; ?>
               <?php $tlc = store_get_tlc_by_acl_name($row['locations_acl']);?>
      <option number="<?php print $row['field_phone_number'] ?>" tlc="<?php print $tlc ?>" value="<?php print $val ?>"><?php print ucfirst(strip_tags($row['title'])) .' - '. $row['field_postal_address_thoroughfare']; ?></option>
       <?php endif;
          endforeach; ?>
     </optgroup>
    <?php endforeach; ?>
    </select>
  </div>

  <div id="phone-number-box-wrapper">
    <div id="phone-number-box">
      <?php print t("Many issues can be solved by a quick call to your local store. Please select your store from the above menu to see phone number."); ?>
    </div>
    <div class="phone-number"></div>
  </div>

</div>
<div class="torn-pod-footer"></div>
