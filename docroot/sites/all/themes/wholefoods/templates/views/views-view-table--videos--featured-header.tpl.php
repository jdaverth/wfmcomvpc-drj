<?php
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<div class="torn-pod-header"></div>
<div class="torn-pod-content">
<?php
foreach ($variables['view']->result as $k => $v) {
  $vid = $v->field_field_video[0]['raw']['file']->uri;
  $vid = explode('/', $vid);
  $vid = array_pop($vid);
  $rows[$k]['field_video'] = '<embed id="flvvideo" height="100%" width="100%" allowfullscreen="true" flashvars="fs=1" wmode="transparent" quality="high" bgcolor="#ffffff" name="flvvideo" src="//www.youtube.com/v/'.$vid.'" type="application/x-shockwave-flash">';
}
?>
<?php foreach ($rows as $id => $row): ?>
  <div class="<?php print $classes_array[$id]; ?>">
   <?php foreach ($row as $field => $content): ?>
     <div class="<?php print $field_classes[$field][$id]; ?>">
   <?php print $content; ?>
     </div>
   <?php endforeach; ?>
  </div>
<?php endforeach; ?>
</div>
<div class="torn-pod-footer"></div>
