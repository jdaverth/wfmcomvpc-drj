<?php
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
   <?php if (strstr($row, 'ul') == FALSE): ?>
     <?php if (strstr($row, 'view view-monster-nav-sub-views') == FALSE): ?>
       <?php continue; ?>
		 <?php elseif (strlen(trim(strip_tags(strstr($row, '<div class="view view-monster-nav-sub-views')))) == 0 ): ?>
		   <?php continue; ?>
     <?php endif; ?>
   <?php endif; ?>
  <div class="<?php print $classes_array[$id]; ?>">
    <?php print $row; ?>
  </div>
<?php endforeach; ?>
