
<?php
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div class="<?php print $classes_array[$id]; ?>">
<div class="torn-pod-header"></div><?php //torn pod header?>
						<div class="torn-pod-content"><?php //torn pod content?>
  <?php print $row; ?>
  </div>
						<div class="torn-pod-footer"></div><?php //torn pod footer?>
						<div class="clearfix"></div>
</div>
<?php endforeach; ?>
