
<?php
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php
$caption = '';
$cnt = 0;
$captions = array(t('Closest store'), t('More nearby stores'));
foreach ($rows as $id => $row):
  if (isset($captions[$cnt++]) && $caption == $captions[$cnt++]) {
    print '<h3>' . $caption . '</h3>';
  }
  ?>
  <?php if (!empty($row)):?>
  <div class="<?php print (isset($classes_array[$id]) ? $classes_array[$id] : ''); ?>">
<div class="torn-pod-header"></div><?php //torn pod header?>
						<div class="torn-pod-content"><?php //torn pod content?>
  <?php print $row; ?>
  </div>
						<div class="torn-pod-footer"></div><?php //torn pod footer?>
						<div class="clearfix"></div>
</div>
  <?php endif; ?>
<?php endforeach; ?>
