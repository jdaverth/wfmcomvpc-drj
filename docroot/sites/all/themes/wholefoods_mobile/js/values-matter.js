/**
 * @file
 * JS for /values-matter page
 */
(function($){
  'use strict';
  
  /**
   * Container object for custom form filtering behavior
   */
  var vmforms = {
    /**
     * Properties
     */ 
    vars: {
      form: '#views-exposed-form-brand-camp-list-mobile',
      filters: '.isoFilters-widget',
      filter_height: 75,
    },

    /**
     * Attach event listeners
     */
    addListeners: function() {
      $('body').delegate(vmforms.vars.filters, 'click', function clickDelegateCallback(){
        vmforms.filterClickCallback($(this));
      });
    },

    /**
     * Click callback function
     *
     * @param element object
     *   jQuery obkect of clicked element
     */
    filterClickCallback: function(element) {
      var height = vmforms.getLabelsHeight(element);
      if (!element.hasClass('expanded')) {
        $('.isoFilters > .expanded').removeClass('expanded');
        element.addClass('expanded');
      }
      else {
        element.removeClass('expanded');
      }
    },

    /**
     * Get height of child label elements
     *
     * @param element object
     *   jQuery object of element to find child labels
     * @return int
     *   Height of child labels
     */
    getLabelsHeight: function(element) {
      var labels = element.find('.views-widget label, .filter'),
          height = labels.height();
      return height * (labels.length + 1);
    },

    /**
     * Start functionality
     */
    init: function(){
      vmforms.addListeners();
    },
  };

  /**
   * Container object for animated text functionality
   */
  var vmanimate = {

    /**
     * Animates the next slide
     *
     * @param outId string
     *   Element selector to animate out
     * @param inId string
     *   Element selector to animate in
     */
    animateNext: function(outId, inId) {
      $(outId).removeClass('slideInRight').addClass('slideOutLeft');
      $(inId).removeClass('slideOutLeft').addClass('slideInRight');
    },  

    /**
     * Animates slides
     * 
     * @param slides array
     *   Array of element ids to animate
     */
    animateSlides: function(slides) {
      var currentSlide = 0;
      slides.forEach(function (slide) { 
        $(slide).removeClass('invisible');
      });
      setInterval(function() {
        if (currentSlide == slides.length-1) {
          vmanimate.animateNext(slides[currentSlide], slides[0]);
          currentSlide = 0;
        } else {
          vmanimate.animateNext(slides[currentSlide], slides[currentSlide+1]);
          currentSlide++;
        }
      }, 5000);
    },

    /**
     * Start functionality
     */
    init: function() {
      setTimeout(function() {
        vmanimate.animateSlides(['#header1', '#header2', '#header3', '#header4']);
      }, 1000);
    },
  };

  var vmarticle = {
    
    addScrollTopButton: function() {
      $('body').append(
        $('<div></div>')
          .attr({ class: 'scrollTop', id: 'scrollTop' })
          .click(function() {
            window.scrollTo(0, 0);
          })
          .hide()
      );
      $(window).scroll(function() {
        if ($(this).scrollTop() > 1000 ) {
          $('#scrollTop').fadeIn(400);
        } else {
          $('#scrollTop').fadeOut(400);
        }
      });
    },

    clickNodeCallback: function(article) {
      var id = '';
      if ($(article).hasClass('node-bc-ugc-image')){
        vmarticle.ugcClickCallback(article);
      } 
      else {
        window.location.href = $(article).find('.no-colorbox-node').attr('href');
      }
    },

    /**
     * Attach event listeners
     */
    init: function() {
      var isTouchDevice = 'ontouchstart' in document.documentElement;

      $('body').delegate('.link-to-content a', 'click', function(event){
        event.stopPropagation();
      });
      $('body').delegate('.field-name-field-bc-src-username a', 'click', function(event){
        event.stopPropagation();
      });  
      $('#boxes-box-bc_mobile_back_to_top').click(function(){
        location.href = '#page-wrapper';
      });
      $('.pager-load-more').click(function(){
        $(this).find('a').trigger('click');
      });
      vmarticle.addScrollTopButton();
    },
    
    ugcClickCallback: function(article) {
      var id = $(article).attr('id'),
          href = $(article).find('.field-name-field-bc-cover-image a').attr('href'),
          content_page = false;
      
      if ($('#' + id).parents('#block-system-main').length && $('body').hasClass('node-type-bc-ugc-image')) {
        content_page = true;
      }

      if ($('#' + id).hasClass('overlay-visible')) {
        if (content_page) {
          $('#' + id).removeClass('overlay-visible');
        } 
        else {
          window.location.href = href;
        }
      }
      else {
        $('#' + id).addClass('overlay-visible');
      }
    },
  };

  /**
   * Attach initialization functions
   */
  Drupal.behaviors.valuesMatter = {
    attach: function valuesMatterAttach() {
      vmforms.init(); //No .once because the listeners have to reattach after AJAX form refresh
      $('#block-system-main').once('articleColorbox', function() {
        vmarticle.init();
      });      
      $('#block-boxes-bc-mobile-header').once('animateText', function() {
        vmanimate.init();
      });
    }
  };

})(jQuery);