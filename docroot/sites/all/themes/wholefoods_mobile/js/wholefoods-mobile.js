/**
 * @file
 * Mobile theme js
 */

(function($) {
  $(function() {

    function setThrobber(target) {
      $('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div>')
        .insertAfter(target);
    }


    function removeThrobber(target) {
      target.siblings('.ajax-progress-throbber').remove();
    }


    function MobileStoreMap() {
      if ('mobileStore' in Drupal.settings) {
        this.current = null;
        this.map = null;
        this.canvas = '#mapCanvas';
      }
    }
    $.extend(MobileStoreMap.prototype, {
      init: function() {
        if ('current' in this) {
          this.initCanvas();
          this.initMap();
          this.setCurrent();
        }
      },
      initCanvas: function() {
        this.canvas = $(this.canvas);
        this.canvas.height(this.canvas.width() * .8);
      },
      initMap: function() {
        this.current = Drupal.settings.mobileStore.center;
        var options = {
          center: new google.maps.LatLng(this.current.lat, this.current.lon),
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          zoom: 14,
          draggable: false
        };
        this.map = new google.maps.Map(this.canvas[0], options);
      },
      setCurrent: function() {
        var marker = {
          map: this.map,
          position: new google.maps.LatLng(this.current.lat, this.current.lon),
          icon: new google.maps.MarkerImage('http://' + location.host
            + '/sites/all/themes/wholefoods/images/store.png')
        };
        new google.maps.Marker(marker);
      }
    });


    // Menu
    $('nav.navigation > ul > li').each(function() {
      var subMenu = $(this).find('ul');
      var parentLink = $(this).children('a');
      var parentName = parentLink.text();
      if (subMenu.length && parentLink.attr('id')) {
        subMenu.addClass('submenu submenu-' + parentLink.attr('id'));
        subMenu.appendTo('nav.navigation');
        parentLink.click(function() {
          var self = $(this);
          $('#region-menu .submenu-' + $(this).attr('id')).slideToggle(function() {
            self.hasClass('expanded')
              ? self.text(parentName).removeClass('expanded')
              : self.text(Drupal.t('less')).addClass('expanded');
          });
          return false;
        });
      }
    });


    // Select A Store
    var storeSelectForm = $('#store-mobile-select-store-form');
    if (storeSelectForm.length) {
      var path = Drupal.settings.basePath + 'mobile/stores';
      // To prevent submit additional values in GET query
      // and avoid extra drupal page load, locate browser directly to
      // mobile/store page
      storeSelectForm.submit(function() {
        var address = storeSelectForm.find('#edit-address').val();
        if (address.length) {
          path += '?address=' + encodeURI(address);
        }
        window.location.href = path;
        return false;
      });
      // Use GPS handler
      $('#edit-use-gps').click(function() {
        var self = $(this);
        setThrobber(self);
        if (!$.cookie('geo_location')) {
          if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(pos) {
              $.cookie('geo_location', pos.coords.latitude + ',' + pos.coords.longitude);
              window.location.href = path;
            }, function() {
              removeThrobber(self);
              $.cookie('geo_location', '');
              window.location.href = path;
            }, {
              timeout: 5000,
              enableHighAccuracy: true
            });
            return false;
          }
        }
      });
    }


    // Addds query parmeter for back to search results
    $('.view-id-store_page_content .back-to-results a').each(function() {
      if ($.cookie('store_last_search_address')) {
        $(this).attr('href', $(this).attr('href') + '?address=' + $.cookie('store_last_search_address'));
      }
    });


    // Makes Featured list on home page collapsible
    $('.pane-mobile-marquee-mobile-marquee-featured-items .views-row:gt(0) .pane-title').each(function() {
      var title = $(this).addClass('collapsed');
      var target = title.parent();
      $('<div class="views-row-container"/>').append($('> div', target).show()).appendTo(target);
      var button = $('<a/>').attr('href', '').addClass('toggle').text('-');
      title.click(function() {
        button.click();
      });
      button.click(function() {
        var target = $(this)
          , title = target.parent()
          , content = $('.views-row-container', target.closest('.views-row'))
          , collapsed = title.hasClass('collapsed');
        collapsed ? content.slideDown(function() {
          target.text('−');
          title.removeClass('collapsed');
        }) : content.slideUp(function() {
          target.text('+');
          title.addClass('collapsed');
        });
        return false;
      }).appendTo(title);
    });


    // Store Details
    Drupal.behaviors.refreshStoreMap = {
      attach: function() {
        $('.pane-store-page-content-mobile-detailed').length && new MobileStoreMap().init();
      }
    };
    Drupal.behaviors.refreshStoreMap.attach();


    // Make This My Store
    Drupal.behaviors.makeThisMyStore = {
      attach: function() {
        var context = $('.my-store');
        if (context.length) {
          var link = $('.make-this-my-store', context)
            , text = $('.this-is-your-store', context);
          (link.attr('store') != $.cookie('local_store') ? link : text).css('display', 'block');
        }
      }
    };
    Drupal.behaviors.makeThisMyStore.attach();


    // Store Events
  Drupal.behaviors.storeEventsToggle = {
    attach: function() {
      function close_accordion_section() {
        $('.events .detail').hide();
        $('.events .event').removeClass('expanded');
        $('.expand a').text('+');
      }

      $('.brief').click(function() {
        $('.expand a', this).click();
      });

      $('.expand a').click(function() {
        var text = $(this).text();
        var currentAttrValue = $(this).attr('href');
        if($(currentAttrValue).is('.expanded')) {
          close_accordion_section();
        }else {
          close_accordion_section();
          $('.events ' + currentAttrValue).addClass('expanded');
          $('.events ' + currentAttrValue + ' .detail').show();
          $(this).text(text == "+" ? "-" : "+");
        }
        return false;
      });
    },

  };
  Drupal.behaviors.storeEventsToggle.attach();


    // Recipe Search
    Drupal.behaviors.recipeSearchClickable = {
      attach: function() {
        if ($('.pane-search-global').length) {
          $('.pane-search-global .views-row').each(function() {
            $(this).unbind('click').click(function() {
              window.location = $(this).find('a:eq(0)').attr('href');
            });
          });
        }
      }
    };


    // Makes Special Diets on recipe page collapsible
    (function() {
      var target = $('.view-recipe-details-mobile .views-field-field-special-diet');
      if (target.length) {
        target.addClass('collapsed');
        var toggle = $('<a/>', { class: 'toggle', href: '', text: '+' })
          .appendTo($('.views-label-field-special-diet', target));
        var content = $('.special-diet-info-block', target);
        target.add(toggle).click(function() {
          target.hasClass('collapsed') ? content.slideDown(function() {
            toggle.text('−');
            target.removeClass('collapsed');
          }) : content.slideUp(function() {
            toggle.text('+');
            target.addClass('collapsed');
          });
          return false;
        });
      }
    })();


    // Collapsible Cooking & Entertaining Guides
    Drupal.behaviors.collapsibleGuides = {
      attach: function() {
        var target = $('.page-mobile-recipes .pane-generic-page-items-mobile-list-copy');
        if (target.length && !$('>h2.pane-title', target).length) {
          var collapsed = target.hasClass('collapsed')
            , title = $('<h2/>').addClass('pane-title').text($('.page-title', target)
                .detach().text()).insertBefore($('.pane-content', target))
            , height = title.outerHeight()
            , button = button = $('<a/>').attr('href', '').addClass('toggle');
          height && button.css('lineHeight', height + 'px');
          collapsed ? button.text('+') : button.text('−');
          title.click(function() {
            $(this).next().click();
          });
          button.click(function() {
            var target = $(this)
              , content = target.next()
              , collapsed = target.parent().hasClass('collapsed');
            collapsed ? content.slideDown(function() {
              target.text('−').parent().removeClass('collapsed');
            }) : content.slideUp(function() {
              target.text('+').parent().addClass('collapsed');
            });
            return false;
          }).insertAfter(title);
        }
      }
    };
    Drupal.behaviors.collapsibleGuides.attach();


    // Unbind autosubmit from textfields on search pages
    Drupal.behaviors.unbindAutosubmit = {
      attach: function() {
        var context = $('.pane-search-global, .pane-recipe-search-mobile');
        context.length && $('.views-exposed-widgets .form-text', context).unbind();
      }
    };
    Drupal.behaviors.unbindAutosubmit.attach();


    // Disable scrollTop for views with load more pager.
    var views = [
      '.view-id-blogs.view-display-id-mobile_blog_pages',
      '.view-id-blogs.view-display-id-mobile_blog_post_comments',
      '.view-id-recipe_search.view-display-id-mobile',
      '.view-store-lookup-by-address.view-display-id-mobile',
      '.view-search.view-display-id-global',
      '.view-blog-posts-by-author.view-display-id-mobile_blog_posts_by_author'
    ];
    $(views.join(',')).length && (Drupal.ajax.prototype.commands.viewsScrollTop = function() {});


    // Hide/show Blog post comments and comment form
    (function() {
      var form = $('.pane-node-comment-form')
        , count = $('.pane-blogs-mobile-blog-post .views-field-comment-count')
        , comments = $('.view-blogs.view-display-id-mobile_blog_post_comments').hide();
      count.click(function() {
        $(this).add(form).add(comments).slideToggle(400);
      });
      $('.error', form).length && count.add(form.find('a.toggle')).click();
    })();


    //
    var containers = $('.view-id-coupons .views-row');
    if (containers.length) {
      $('.form-item-select-all-coupons label').click(function() {
        setTimeout(function(){ //hacky, but we're in a hurry
          containers.each(function(){
            if ($(this).find('input').attr('checked')) {
              $(this).addClass('checked');
            }
            else {
              $(this).removeClass('checked');
            }
          });
        }, 100);
      });
      containers.click(function() {
        var target = $(this).find('input')
          , self = $(this);
        if (target.attr('checked')) {
          self.removeClass('checked');
          target.removeAttr('checked');
        }
        else {
          self.addClass('checked');
          target.attr('checked', 'checked');
        }
      });
    }


    function osAndroid() {
      return /android/i.test(navigator.userAgent);
    }


    // on Gift Cards page add iframe in a different way to be scrollable on iPhone and Droid
    // iPhone scrolls frame but can't scroll div with oveflow auto containing frame
    // Droid can't scroll frame but can scroll div...
    // Thank you Lord we don't have to support Blackberry
    var scrollWrapper = $('div.view-generic-page-items div.scroll-wrapper');
    if (scrollWrapper && osAndroid()) {
      // for old Droids
      $('#page-wrapper').css({ width: 640 });
      scrollWrapper.css({
        width: 600,
        overflow: 'auto'
      });
      scrollWrapper.html('');
      scrollWrapper.html('<iframe frameborder="0" src="https://app.giftango.com/GiftCardPortal/WholeFoods/GiftCardPortal.aspx" style="border:none;height:805px;width:940px"></iframe>');
    }


    // Blog post back buttons
    $('.pane-blogs-mobile-blog-post').length && $('.back a, .back-to-blog-home .pane-content a')
      .click(function() {
        window.history.go(-1);
        return false;
      });

    // Global search state
    (function() {
      var target = $('.pane-search-global #edit-submit-search');
      if (target.length) {
        Drupal.behaviors.globalSearchState = {
          attach: function() {
            $('.pane-search-global #edit-submit-search').click(function() {
              $.cookie('globalSearch', JSON.stringify({
                q: $('#edit-search-api-views-fulltext').val(),
                c: $('#edit-type').val()
              }), { path: '/' });

              // Send search term to google_analytics
              if (typeof(dataLayer) !== 'undefined') {
                var searchTerm = $('#edit-search-api-views-fulltext').val();
                if (searchTerm) {
                  dataLayer.push({
                    'searchTerm': searchTerm,
                    'event': 'ajaxMobileSearch'
                  });
                }
              }
            });
          }
        };
        Drupal.behaviors.globalSearchState.attach();
        var data = 1 < $.cookie('globalSearchMoves') ? 0 : $.cookie('globalSearch');
        $.cookie('globalSearchMoves', 0, { path: '/' });
        if (data) {
          data = JSON.parse(data);
          $('#edit-search-api-views-fulltext').val(data.q);
          $('#edit-type').val(data.c);
          target.click();
        }
      }
      else {
        $.cookie('globalSearchMoves', +$.cookie('globalSearchMoves') + 1, { path: '/' });
      }
    })();


    // Recipe search state
    (function() {
      var target = $('.pane-recipe-search-mobile #edit-submit-recipe-search');
      if (target.length) {
        Drupal.behaviors.recipeSearchState = {
          attach: function() {
            $('.pane-recipe-search-mobile #edit-submit-recipe-search').click(function() {
              $.cookie('recipeSearch', JSON.stringify({
                q: $('#edit-search-api-views-fulltext').val(),
                c1: $('#edit-field-recipe-category').val(),
                c2: $('#edit-field-recipe-course').val(),
                c3: $('#edit-field-special-diet').val(),
                s: $('#edit-sort-by').val()
              }), { path: '/' });
            });
          }
        };
        Drupal.behaviors.recipeSearchState.attach();
        var data = 1 < $.cookie('recipeSearchMoves') ? 0 : $.cookie('recipeSearch');
        $.cookie('recipeSearchMoves', 0, { path: '/' });
        if (data) {
          data = JSON.parse(data);
          $('#edit-search-api-views-fulltext').val(data.q);
          $('#edit-field-recipe-category').val(data.c1);
          $('#edit-field-recipe-course').val(data.c2);
          $('#edit-field-special-diet').val(data.c3);
          $('#edit-sort-by').val(data.s);
          target.click();
        }
      }
      else {
        $.cookie('recipeSearchMoves', +$.cookie('recipeSearchMoves') + 1, { path: '/' });
      }
    })();


    // State Tracker
    (function(win, doc, target) {
      if (target.length) {
        var data = $.cookie('pageStates');
        data = data ? JSON.parse(data) : {};
        var path = win.location.pathname + win.location.search;
        if (data && path in data) {
          var states = data[path].split(',');
          var pos = states.shift();
          target.each(function() {
            !+states.shift() !== $(this).parent().hasClass('collapsed') && $(this).click();
          });
          // Timeout for expanding/collapsing to set an accurate scroll position
          setTimeout(function() {
            doc.scrollTop(pos);
          }, 400);
        }
        // We could use onunload here, but behavior of this event is different for mobile safari
        doc.click(function() {
          var states = [];
          states.push(doc.scrollTop());
          target.each(function() {
            states.push($(this).parent().hasClass('collapsed') ? 0 : 1);
          });
          data[path] = states.join();
          $.cookie('pageStates', JSON.stringify(data), { path: '/' });
        });
      }
    })(window, $(document), $('a[href=""].toggle'));


    function onMobile() {
      var a = navigator.userAgent || navigator.vendor || window.opera;
      if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) {
        return true;
      }
      return false;
    }


    // Disable contextual links on touch devices
    if (onMobile()) {
      Drupal.behaviors.contextualLinksDisable = {
        attach: function (context) {
          var wrapper = $('div.contextual-links-wrapper', context);
          wrapper.closest('.contextual-links-region').unbind('mouseleave click');
          wrapper.find('ul.contextual-links').add('a.contextual-links-trigger').remove();
        }
      };
      Drupal.behaviors.contextualLinksDisable.attach();
    }


    //Return a cookie value if set.
    function wfm_getCookie(c_name) {
      var i, x, y, ARRcookies = document.cookie.split(";");
      for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
          return unescape(y);
        }
      }
      return null;
    }

    //Expire a cookie if it is set.
    function wfm_deleteCookie(c_name, c_path, c_domain) {
      if (!wfm_getCookie(c_name)) { return false; }    
      document.cookie = encodeURIComponent(c_name) + 
        "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + 
        (c_domain ? "; domain=" + c_domain : "; domain=/") + 
        (c_path ? "; path=" + c_path : "; path=.wholefoodsmarket.com");
      return true;
    }

    //Expires user authentication cookies.
    function wfm_deleteAuthCookies() {
      wfm_deleteCookie('HD');
      wfm_deleteCookie('at');
      wfm_deleteCookie('gtc');
      wfm_deleteCookie('gtct');
      wfm_deleteCookie('GiftCardPortalPartnerAuth');
    }

    //Remove left over authentication cookies if graceless log out
    (function(){
      var cookie = wfm_getCookie('HD');
      if (cookie && $("body").hasClass("not-logged-in")) { wfm_deleteAuthCookies(); }
    })();

  });
})(jQuery);
