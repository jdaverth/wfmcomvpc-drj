<?php


/**
 * Implements hook_preprocess_html().
 */
function wholefoods_mobile_preprocess_html(&$vars) {
  $vars['rdf'] = new stdClass;

  // Set default title Home, if store is not selected
  if (arg(0) == 'mobile' && arg(1) == 'home') {
    drupal_set_title(t('Home'));
  }

  // Construct page title.
  if (drupal_get_title()) {
    $head_title = array(
      'title' => strip_tags(drupal_get_title()),
      'name' => check_plain(variable_get('site_name', 'Drupal')),
    );
  }
  else {
    $head_title = array('name' => check_plain(variable_get('site_name', 'Drupal')));
    if (variable_get('site_slogan', '')) {
      $head_title['slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
    }
  }
  $vars['head_title_array'] = $head_title;
  $vars['head_title'] = implode(' | ', $head_title);

  if (module_exists('rdf')) {
    $vars['doctype'] = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML+RDFa 1.1//EN">' . "\n";
    $vars['rdf']->version = ' version="HTML+RDFa 1.1"';
    $vars['rdf']->namespaces = $vars['rdf_namespaces'];
    $vars['rdf']->profile = ' profile="' . $vars['grddl_profile'] . '"';
  }
  else {
    $vars['doctype'] = '<!DOCTYPE html>' . "\n";
    $vars['rdf']->version = '';
    $vars['rdf']->namespaces = '';
    $vars['rdf']->profile = '';
  }
  
  // see function _wholefoods_mobile_render_apple_touch_icons
  $vars['apple_touch_icons'] = array(
    '60x60' => 'touch-icon-iphone.png',
    '76x76' => 'touch-icon-ipad.png',
    '120x120' => 'touch-icon-iphone-retina.png',
    '152x152' => 'touch-icon-ipad-retina.png'
  );
 
  wfm_social_open_graph();

  // This is depcrecated for now. user scalable enabled to all devices according
  // to the ticket QD-1300.
  // $vars['viewport'] = '<script src="/' . path_to_theme() . '/js/viewport.js"></script>';
}


/**
 * Override or insert variables into the block template.
 */
function wholefoods_mobile_preprocess_block(&$variables) {
  // In the header region visually hide block titles.
  if ($variables['block']->region == 'header') {
    $variables['title_attributes_array']['class'][] = 'element-invisible';
  }
}


/**
 * Implements theme_menu_link().
 */
function wholefoods_mobile_menu_link($vars) {
  if ('user-register' == $vars['element']['#original_link']['link_path'] && !user_register_access()) {
    return FALSE;
  }
  // create class 'active' for blog pages.
  if (0 === strpos(drupal_get_path_alias(current_path()), 'blog')
      && 'blogs' == drupal_get_path_alias($vars['element']['#original_link']['link_path'])) {
    $vars['element']['#attributes']['class'][] = 'active-trail';
    $vars['element']['#localized_options']['attributes']['class'][] = 'active';
  }
  switch ($vars['element']['#original_link']['menu_name']) {
    case 'menu-mobile-header-menu':
      $vars['element']['#title'] = '<i class="icon"></i><span class="text">'
          . $vars['element']['#title'] . '</span>';
      $vars['element']['#localized_options']['html'] = TRUE;
      break;

    case 'menu-mobile-footer':
      $function = '_wfm_mobile_core_store_social_link';
      if (function_exists($function)) {
        $href = $function($vars['element']['#original_link']['link_title']);
        if ($href) {
          $vars['element']['#href'] = $href;
        }
      }
      break;
  }
  return theme_menu_link($vars);
}


/**
 * Implements hook_page_alter().
 */
function wholefoods_mobile_page_alter(&$page) {
  // Force render menu and header regions, even they don't have content
  if(!isset($page['header'])) {
    $page['header'] = array(
      '#region' => 'header',
      '#theme_wrappers' => array('region'),
    );
  }
  $page['menu'] = array(
    '#region' => 'menu',
    '#theme_wrappers' => array('region'),
  );
}


/**
 * Implements hook_process_region().
 */
function wholefoods_mobile_process_region(&$vars) {
  // Load default mobile-menu
  if ($vars['elements']['#region'] == 'menu') {
    $tree = menu_tree_page_data('menu-mobile-menu');
    $vars['main_menu'] = menu_tree_output($tree);
  }

  // Init default vars for header region
  if ($vars['region'] == 'header') {
    $vars['linked_site_name'] = l(variable_get('site_name', ''), '<front>');
  }
}


/**
 * Helper function to build a Load More pager.
 */
function _wholefoods_mobile_load_more_pager($vars, $text = 'Load More') {
  $more = theme('pager_next', array(
    'text' => (t($text)),
    'element' => $vars['element'],
    'interval' => 1,
    'parameters' => $vars['parameters'],
  ));
  $output = theme('item_list', array(
    'items' => array($more),
    'attributes' => array('class' => array('pager')),
  ));
  $output = str_replace('page=1&amp;', '', $output);
  return $output;
}


/**
 * Helper function to build a Clean mini pager.
 */
function _wholefoods_mobile_clean_pager($vars) {
  global $pager_page_array, $pager_total;
  $element = $vars['element'];
  $pager_current = $pager_page_array[$element] + 1;
  $pager_max = $pager_total[$element];
  if ($pager_total[$element] > 1) {
    $li_prev = theme('pager_previous', array(
      'text' => t('« newer'),
      'element' => $element,
      'interval' => 1,
      'parameters' => $vars['parameters'],
    ));
    $li_next = theme('pager_next', array(
      'text' => t('older »'),
      'element' => $element,
      'interval' => 1,
      'parameters' => $vars['parameters'],
    ));
    $items[] = array(
      'class' => array('pager-previous'),
      'data' => $li_prev,
    );
    $items[] = array(
      'class' => array('pager-current'),
      'data' => t('page @current of @max', array('@current' => $pager_current, '@max' => $pager_max)),
    );
    $items[] = array(
      'class' => array('pager-next'),
      'data' => $li_next,
    );
    if ($items) {
      return theme('item_list', array(
        'items' => $items,
        'title' => NULL,
        'type' => 'ul',
        'attributes' => array('class' => array('pager')),
      ));
    }
  }
}

/**
 * Helper function to build a Store list mini pager.
 */
function _wholefoods_mobile_store_list_pager($variables, $quantity = 3) {
  $tags = $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  global $pager_page_array, $pager_total, $theme;
  
  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.
    
  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  $li_previous = theme('pager_previous', array('text' => t('«'), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_next = theme('pager_next', array('text' => t('»'), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_first = theme('pager_first', array('text' => 1, 'element' => $element, 'parameters' => $parameters));
  $li_last = theme('pager_last', array('text' => $pager_max, 'element' => $element, 'parameters' => $parameters));

  // First-page link display condition
  $show_first = ($i > 1) ? true : false;

  if ($pager_total[$element] > 1) {
    if ($li_previous) {
      $items[] = array(
        'class' => array('pager-previous'),
        'data' => $li_previous,
      );
    }

    if ($show_first && $li_first) {
      $items[] = array(
        'class' => array('pager-first'),
        'data' => $li_first,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 2) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_previous', array('text' => $i, 'element' => $element, 'interval' => ($pager_current - $i), 'parameters' => $parameters)),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => array('pager-current'),
            'data' => '<span>' . $i . '</span>',
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_next', array('text' => $i, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
    }

    // Last-page link display condition
    $show_last = ($pager_max > ($i-1)) ? true : false;

    // End generation.
    if ($show_last && $li_last) {
      $items[] = array(
        'class' => 'pager-last',
        'data' => $li_last,
      );
    }

    if ($li_next) {
      $items[] = array(
        'class' => array('pager-next'),
        'data' => $li_next,
      );
    }
    return '<h2 class="element-invisible">' . t('Pages') . '</h2>' . theme('item_list', array(
      'items' => $items,
      'attributes' => array('class' => array('pager')),
    ));
  }
}

/*
 * theme_pager_previous()
 */
function wholefoods_mobile_pager_previous($variables) {
  $text = $variables['text'];
  $element = $variables['element'];
  $interval = $variables['interval'];
  $parameters = $variables['parameters'];
  global $pager_page_array;
  $output = '';

  // If we are anywhere but the first page
  if ($pager_page_array[$element] > 0) {
    $page_new = pager_load_array($pager_page_array[$element] - $interval, $element, $pager_page_array);
    // If the previous page is the first page, mark the link as such.
    if ($page_new[$element] == 0) {
      $output = theme('pager_first', array('text' => $text, 'element' => $element, 'parameters' => $parameters));
    }
    // The previous page is not the first page.
    else {
      $output = theme('pager_link', array('text' => $text, 'page_new' => $page_new, 'element' => $element, 'parameters' => $parameters));
    }
  } elseif ($pager_page_array[$element] == 0) {
    $output = '<span  class="pager-inactive">' . t('«') . '</span>';
  }
  return $output;
}

/*
 * theme_pager_next()
 */
function wholefoods_mobile_pager_next($variables) {
  $text = $variables['text'];
  $element = $variables['element'];
  $interval = $variables['interval'];
  $parameters = $variables['parameters'];
  global $pager_page_array, $pager_total;
  $output = '';

  // If we are anywhere but the last page
  if ($pager_page_array[$element] < ($pager_total[$element] - 1)) {
    $page_new = pager_load_array($pager_page_array[$element] + $interval, $element, $pager_page_array);
    // If the next page is the last page, mark the link as such.
    if ($page_new[$element] == ($pager_total[$element] - 1)) {
      $output = theme('pager_last', array('text' => $text, 'element' => $element, 'parameters' => $parameters));
    }
    // The next page is not the last page.
    else {
      $output = theme('pager_link', array('text' => $text, 'page_new' => $page_new, 'element' => $element, 'parameters' => $parameters));
    }
  } elseif ($pager_page_array[$element] == $pager_total[$element] - 1) {
      $output = '<span  class="pager-inactive">' . t('»') . '</span>';
  }

  return $output;
}

/**
 * Overrides theme_pager__VIEW__DISPLAY().
 */
function wholefoods_mobile_pager__store_lookup_by_address__mobile($vars) {
  global $wfm_mobile_store_list_hide_pager;
  if (!$wfm_mobile_store_list_hide_pager) {
    return _wholefoods_mobile_store_list_pager($vars);
  }
}


/**
 * Overrides theme_pager__VIEW__DISPLAY().
 */
function wholefoods_mobile_pager__recipe_search__mobile($vars) {
  return _wholefoods_mobile_load_more_pager($vars, 'Load More Recipes');
}


/**
 * Overrides theme_pager__VIEW__DISPLAY().
 */
function wholefoods_mobile_pager__search__global($vars) {
  return _wholefoods_mobile_load_more_pager($vars, 'Load More Results');
}


/**
 * Overrides theme_pager__VIEW__DISPLAY().
 */
function wholefoods_mobile_pager__blogs__mobile_blog_post_comments($vars) {
  return _wholefoods_mobile_load_more_pager($vars, 'Load More Comments');
}


/**
 * Overrides theme_pager__VIEW__DISPLAY().
 */
function wholefoods_mobile_pager__blogs__mobile_blog_pages($vars) {
  return _wholefoods_mobile_clean_pager($vars);
}


/**
 * Overrides theme_pager__VIEW__DISPLAY().
 */
function wholefoods_mobile_pager__blog_posts_by_author__mobile_blog_posts_by_author($vars) {
  return _wholefoods_mobile_clean_pager($vars);
}


/**
 * Implements hook_preprocess_node().
 */
function wholefoods_mobile_preprocess_node(&$vars) {
  // Clear content for regular recipe node, instead there displays quicktab block with all details
  if ($vars['node']->type == 'recipe') {
    $vars['content'] = array(
      'page-title' => array(
        '#markup' => '<h2 class="page-title">Recipes</h2>',
      ),
      'node-title' => array(
        '#markup' => '<h2 class="node-title">' . $vars['node']->title . '</h2>',
      ),
    );
  }
  if ($vars['node']->type == 'bc_article' || 
      $vars['node']->type == 'bc_article_external' ||
      $vars['node']->type == 'bc_connect' ||
      $vars['node']->type == 'bc_coupon' ||
      $vars['node']->type == 'bc_image' ||
      $vars['node']->type == 'bc_quote' ||
      $vars['node']->type == 'bc_video') {
    
    $vars['theme_hook_suggestion'] = 'node__bc_all';
  }
}


/**
 * Implements hook_preprocess_views_view_fields().
 */
function wholefoods_mobile_preprocess_views_view_fields(&$vars) {
  $view = $vars['view'];

  if ($view->name == 'recipe_details_mobile') {
    // Summary tab
    if ($view->current_display == 'default') {
      // Process rating field
      $vars['fields']['field_recipe_rating']->content = '<div class="field-content"><div class="stars" style="width: '
        . $vars['row']->field_field_recipe_rating[0]['raw']['value'] * 20 . '%"></div></div>';
      // Process Special Diets field
      $special_diets_values = array();
      foreach ($vars['row']->field_field_special_diet as $field_option) {
        $special_diets_values[$field_option['raw']['nid']] = TRUE;
      }

      $special_diets = array(
        '138' => 'Dairy Free',
        '25068' => 'Fat Free',
        '139' => 'Gluten Free',
        '25066' => 'High Fiber',
        '21' => 'Low Fat',
        '22' => 'Low Sodium',
        '137' => 'Sugar Conscious',
        '158786' => 'Vegan',
        '25' => 'Vegetarian',
        '828' => 'Wheat Free',
      );
      $special_diets_content = '';
      $special_diets_info_block = '';
      foreach ($special_diets as $nid => $title) {
        $active = isset($special_diets_values[$nid]) ? ' active' : '';
        $special_diets_content .= "<div class=\"special-diet-icon nid-{$nid}{$active}\"></div>";
        $special_diets_info_block .= "<div class=\"special-diet-item\"><div class=\"special-diet-icon nid-{$nid} active\"></div> {$title}</div>";
      }

      $vars['fields']['field_special_diet']->content = "<div class=\"field-content\">{$special_diets_content}</div><div class=\"special-diet-info-block\">{$special_diets_info_block}</div>";
    }
    // Ingredients tab
    elseif ($view->current_display == 'mobile_recipe_ingredients') {
      $items = array();

      foreach ($vars['row']->field_field_ingredients as $ingredient) {
        $items[] = $ingredient['rendered']['#markup'];
      }

      $ingredients_content = array(
        '#prefix' => '<div class="field-content">',
        '#suffix' => '</div>',
        '#markup' => theme('item_list', array('items' => $items)),
      );
      $vars['fields']['field_ingredients']->content = render($ingredients_content);

    }
  }
}

/**
 * Implements theme_preprocess_field().
 */
function wholefoods_mobile_preprocess_field(&$vars, $hook) {
  if($vars['element']['#field_name'] == 'field_bc_showme') {
    foreach($vars['element']['#items'] as $key => $item) {
      $vars['item_classes'][$key] = 'field-key-'. $item['value'];
      $vars['item_attributes_array'][$key]['title'] = $vars['items'][$key]['#markup'];
    }
  }
  elseif ($vars['element']['#field_name'] == 'field_bc_timing') {
    foreach ($vars['items'] as $key => $value) {
      if ($value['#markup'] >= 60) {
        $minutes = round($value['#markup'] / 60);
        $vars['items'][$key]['#markup'] = $minutes . ' ' . format_plural($minutes, t('minute'), t('minutes'));;
      }
      else {
        $vars['items'][$key]['#markup'] .= ' ' . format_plural($value['#markup'], t('second'), t('seconds'));
      }
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function wholefoods_mobile_form_views_exposed_form_alter(&$form, &$form_state, $form_id) {
  if ($form_state['view']->name == 'brand_camp_list' && $form_state['view']->current_display == 'mobile') {
    foreach ($form['#info'] as $filter) {
      if (isset($form[$filter['value']]['#options']['All'])) {
        $all_option = $form[$filter['value']]['#options']['All'];
        unset($form[$filter['value']]['#options']['All']);
        $form[$filter['value']]['#options']['All'] = $all_option;
      }
    }
  }
}

/**
 * Renders HTML for apple touch icons from array in page variables.
 *
 *   @param array $vars
 *     Drupal page variables array.
 *   @return string $html or FALSE
 *     Returns html <link> tags ready for display.
 *     Or returns FALSE if $vars['apple_touch_icons'] is not set.
 */
function _wholefoods_mobile_render_apple_touch_icons($vars) {
  if(empty($vars['apple_touch_icons'])) {
    return FALSE;
  }
  $html = '';
  foreach ($vars['apple_touch_icons'] as $k => $v) {
    $html .= '<link rel="apple-touch-icon" sizes="' . $k .'" href="/' . path_to_theme() . '/images/' . $v . '" />';
  }
  return $html;
}
