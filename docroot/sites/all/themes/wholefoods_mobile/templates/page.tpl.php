<?php 
/**
 * @file
 * Implementation to display a single Drupal page.
 */
?>
<div id="page-wrapper">
  <div id="page">
    <?php if (isset($page['userzone'])) : ?>
      <?php print render($page['userzone']); ?>
    <?php endif; ?>

    <header id="section-header">
    <?php if (isset($page['header'])) : ?>
      <?php print render($page['header']); ?>
    <?php endif; ?>

    <?php if (isset($page['menu'])) : ?>
      <?php print render($page['menu']); ?>
    <?php endif; ?>
    </header>

    <?php if ($messages) : ?>
      <div class="region-message">
        <?php print $messages; ?>
      </div>
    <?php endif; ?>
    <?php if (!empty($tabs["#primary"])):  ?>
      <div class="tabs">
        <?php print render($tabs); ?>
      </div>
    <?php endif; ?>
    <?php if (isset($page['content'])) : ?>
      <?php print render($page['content']); ?>
    <?php endif; ?>
  </div>
  <div class="push"></div>
</div>

<?php if (isset($page['footer'])) : ?>
  <?php print render($page['footer']); ?>
<?php endif; ?>