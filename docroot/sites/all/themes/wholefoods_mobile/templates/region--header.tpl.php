<div class="<?php print $classes ?>" id="region-<?php print $region ?>">
  <div class="region-inner region-<?php print $region ?>-inner">
    <hgroup class="site-logo clearfix">
      <?php if ($is_front): ?>
      <h1 id="site-logo"><?php print $linked_site_name; ?></h1>
      <?php else: ?>
      <h2 id="site-logo"><?php print $linked_site_name; ?></h2>
      <?php endif; ?>
      <?php if ($content) : ?>
        <?php print $content; ?>
      <?php endif; ?>
    </hgroup>
  </div>
</div>