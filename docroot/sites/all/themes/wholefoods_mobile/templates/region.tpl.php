<?php 
/**
 * @file
 * Implementation to display a region.
 */
?>
<div class="<?php print $classes ?>" id="region-<?php print $region ?>">
  <div class="region-inner region-<?php print $region ?>-inner">
    <?php print $content; ?>
  </div>
</div>