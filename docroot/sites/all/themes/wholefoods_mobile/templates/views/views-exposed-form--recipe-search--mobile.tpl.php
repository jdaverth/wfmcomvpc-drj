<?php
/**
 * @file
 * This template handles the layout of the views exposed filter form.
 *
 * Variables available:
 * - $widgets: An array of exposed form widgets. Each widget contains:
 * - $widget->label: The visible label to print. May be optional.
 * - $widget->operator: The operator for the widget. May be optional.
 * - $widget->widget: The widget itself.
 * - $sort_by: The select box to sort the view using an exposed form.
 * - $sort_order: The select box with the ASC, DESC options to define order. May be optional.
 * - $items_per_page: The select box with the available items per page. May be optional.
 * - $offset: A textfield to define the offset of the view. May be optional.
 * - $reset_button: A button to reset the exposed filter applied. May be optional.
 * - $button: The submit button for the form.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($q)): ?>
  <?php
  // This ensures that, if clean URLs are off, the 'q' is added first so that
  // it shows up first in the URL.
  print $q;
  ?>
<?php endif; ?>
<div class="views-exposed-form">
  <div class="views-exposed-widgets clearfix">
    <?php foreach ($widgets as $id => $widget): ?>
      <?php if ($id == 'filter-search_api_views_fulltext'): ?>
        <div class="main-controls">
        <?php endif; ?>
        <div id="<?php print $widget->id; ?>-wrapper" class="views-exposed-widget views-widget-<?php print $id; ?>">
          <?php if (!empty($widget->label)): ?>
            <label for="<?php print $widget->id; ?>">
              <?php print $widget->label; ?>
            </label>
          <?php endif; ?>
          <div class="views-widget">
            <?php print $widget->widget; ?>
          </div>
        </div>
        <?php if ($id == 'filter-search_api_views_fulltext'): ?>
          <div class="views-exposed-widget views-submit-button">
            <?php print $button; ?>
          </div>
        </div><div class="filter-controls">
        <?php endif; ?>
      <?php endforeach; ?>
    </div>
    <div class="sort-controls">
      <?php if (!empty($sort_by)): ?>
        <div class="views-exposed-widget views-widget-sort-by">
          <?php print $sort_by; ?>
        </div>
      <?php endif; ?>
    </div>
  </div>
</div>
