<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>

<?php
$heading = t('Holiday Hours');
if ($row->_entity_properties['entity object']->field_holiday_hours_heading) {
  $heading = check_plain($row->_entity_properties['entity object']->field_holiday_hours_heading[LANGUAGE_NONE][0]['safe_value']);
}
?>

<?php if ($row->_entity_properties['entity object']->field_has_holiday_hours): ?>
  <?php if ($row->_entity_properties['entity object']->field_has_holiday_hours[LANGUAGE_NONE][0]['value'] > 0): ?>
    <h4><?php print $heading ?></h4>
    <?php print $output; ?>
  <?php endif; ?>
<?php endif; ?>