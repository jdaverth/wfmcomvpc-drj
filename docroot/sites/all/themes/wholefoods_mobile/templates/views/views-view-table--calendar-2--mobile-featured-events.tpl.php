<?php $date_format_string = 'F y g:iA'; ?>
<div class="events">
  <?php foreach ($result as $id => $event): ?>
  <?php $row = $rows[$id]; ?>
  <?php $date = $event->field_field_event_date[0]['raw']; ?>
  <div id="node-<?php print $event->nid; ?>" class="event node-<?php print $event->nid; ?>">
    <div class="detail-block">
      <div class="brief">
        <div class="image">
          <?php print $row['field_event_image']; ?>
        </div>
        <div class="title">
          <?php print $row['title']; ?>
        </div>

        <div class="time-loc">
          <?php
          //Manage dates with no time indicating all day events.
          if (strpos($date['db']['value']->originalTime, "00:00:00")) {
            $date_string = "All Day";
          } else {
            $date_string = date($date_format_string, $date['value']);
          }
          ?>
          <?php print $date_string; ?>
        </div>

        <div class="expand">
          <a href="#node-<?php print $event->nid; ?>">+</a>
        </div>
      </div>


      <div class="detail">
        <div class="email-event">
           <?php print theme("wholefoods_event_links", array('nid' => $event->nid)); ?>
        </div>
        <div class="body">
          <div class="clearfix"></div>
          <div class="description">
            <?php print $row['body']; ?>
          </div>
          <div class="links">
            <?php print theme('wholefoods_sharelinks'); ?>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <?php endforeach; ?>
</div>
