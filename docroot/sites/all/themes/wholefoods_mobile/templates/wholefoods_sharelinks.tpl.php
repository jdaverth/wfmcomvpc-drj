<?php

// code for pinit to get image and description from node
$node = node_load(arg(1));
$pin_img = NULL;

if ($node && $node->type == 'recipe') {
  $url = url('node/' . $node->nid, array('absolute' => TRUE));
  $pin_img = file_create_url($node->field_hero_image[LANGUAGE_NONE][0]['uri']);
  $pin_img_parts = parse_url($pin_img);
  $pin_img = $pin_img_parts['scheme'] . '://www.wholefoodsmarket.com' . $pin_img_parts['path'];
  $pin_descr = strip_tags($node->body[LANGUAGE_NONE][0]['value']);
}
?>
<div class="social-links clearfix">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#version=v2.0&xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="recipe-fb socfl">
  <fb:like send="false" layout="button_count" width="450" show_faces="false";></fb:like>
</div>

<div class="recipe-twitter socfl">
  <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
  <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</div>

<!-- added a Google+ button -->
<div class="socfl google-plus">
 <div class="g-plusone" data-size="medium"></div>
 <script src="https://apis.google.com/js/platform.js" async defer></script>
</div>

<?php if ($pin_img): ?>
<div class="pinit socfl">
  <a data-pin-config="beside"
     href="//pinterest.com/pin/create/button/?url=<?php print urlencode($url); ?>&media=<?php print urlencode($pin_img); ?>&description=<?php print urlencode($pin_descr); ?>"
     data-pin-do="buttonPin" ><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" /></a>
</div>
<?php endif; ?>
</div>
