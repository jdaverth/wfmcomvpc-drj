<?php

// Load require file to ensure environment variables are available immediately.
if (file_exists('/var/www/site-php')) {
  require('/var/www/site-php/wfmcom/wfmcom-settings.inc');
}

// Set Acquia site name for all environments, including local
if (!isset($_ENV['AH_SITE_NAME'])) {
  $_ENV['AH_SITE_NAME'] = 'wholefoods';
}

ini_set('session.cookie_httponly', '0');

$conf['maintenance_theme'] = 'wholefoods';
/**
 * @file
 * Drupal site-specific configuration file.
 *
 * IMPORTANT NOTE:
 * This file may have been set to read-only by the Drupal installation
 * program. If you make changes to this file, be sure to protect it again
 * after making your modifications. Failure to remove write permissions
 * to this file is a security risk.
 *
 * The configuration file to be loaded is based upon the rules below.
 *
 * The configuration directory will be discovered by stripping the
 * website's hostname from left to right and pathname from right to
 * left. The first configuration file found will be used and any
 * others will be ignored. If no other configuration file is found
 * then the default configuration file at 'sites/default' will be used.
 *
 * For example, for a fictitious site installed at
 * http://www.drupal.org/mysite/test/, the 'settings.php'
 * is searched in the following directories:
 *
 * - sites/www.drupal.org.mysite.test
 * - sites/drupal.org.mysite.test
 * - sites/org.mysite.test
 *
 * - sites/www.drupal.org.mysite
 * - sites/drupal.org.mysite
 * - sites/org.mysite
 *
 * - sites/www.drupal.org
 * - sites/drupal.org
 * - sites/org
 *
 * - sites/default
 *
 * If you are installing on a non-standard port number, prefix the
 * hostname with that number. For example,
 * http://www.drupal.org:8080/mysite/test/ could be loaded from
 * sites/8080.www.drupal.org.mysite.test/.
 */

/**
 * Database settings:
 *
 * The $databases array specifies the database connection or
 * connections that Drupal may use.  Drupal is able to connect
 * to multiple databases, including multiple types of databases,
 * during the same request.
 *
 * Each database connection is specified as an array of settings,
 * similar to the following:
 * @code
 * array(
 *   'driver' => 'mysql',
 *   'database' => 'databasename',
 *   'username' => 'username',
 *   'password' => 'password',
 *   'host' => 'localhost',
 *   'port' => 3306,
 *   'prefix' => 'myprefix_',
 *   'collation' => 'utf8_general_ci',
 * );
 * @endcode
 *
 * The "driver" property indicates what Drupal database driver the
 * connection should use.  This is usually the same as the name of the
 * database type, such as mysql or sqlite, but not always.  The other
 * properties will vary depending on the driver.  For SQLite, you must
 * specify a database file name in a directory that is writable by the
 * webserver.  For most other drivers, you must specify a
 * username, password, host, and database name.
 *
 * Some database engines support transactions.  In order to enable
 * transaction support for a given database, set the 'transactions' key
 * to TRUE.  To disable it, set it to FALSE.  Note that the default value
 * varies by driver.  For MySQL, the default is FALSE since MyISAM tables
 * do not support transactions.
 *
 * For each database, you may optionally specify multiple "target" databases.
 * A target database allows Drupal to try to send certain queries to a
 * different database if it can but fall back to the default connection if not.
 * That is useful for master/slave replication, as Drupal may try to connect
 * to a slave server when appropriate and if one is not available will simply
 * fall back to the single master server.
 *
 * The general format for the $databases array is as follows:
 * @code
 * $databases['default']['default'] = $info_array;
 * $databases['default']['slave'][] = $info_array;
 * $databases['default']['slave'][] = $info_array;
 * $databases['extra']['default'] = $info_array;
 * @endcode
 *
 * In the above example, $info_array is an array of settings described above.
 * The first line sets a "default" database that has one master database
 * (the second level default).  The second and third lines create an array
 * of potential slave databases.  Drupal will select one at random for a given
 * request as needed.  The fourth line creates a new database with a name of
 * "extra".
 *
 * For a single database configuration, the following is sufficient:
 * @code
 * $databases['default']['default'] = array(
 *   'driver' => 'mysql',
 *   'database' => 'databasename',
 *   'username' => 'username',
 *   'password' => 'password',
 *   'host' => 'localhost',
 *   'prefix' => 'main_',
 *   'collation' => 'utf8_general_ci',
 * );
 * @endcode
 *
 * You can optionally set prefixes for some or all database table names
 * by using the 'prefix' setting. If a prefix is specified, the table
 * name will be prepended with its value. Be sure to use valid database
 * characters only, usually alphanumeric and underscore. If no prefixes
 * are desired, leave it as an empty string ''.
 *
 * To have all database names prefixed, set 'prefix' as a string:
 * @code
 *   'prefix' => 'main_',
 * @endcode
 * To provide prefixes for specific tables, set 'prefix' as an array.
 * The array's keys are the table names and the values are the prefixes.
 * The 'default' element is mandatory and holds the prefix for any tables
 * not specified elsewhere in the array. Example:
 * @code
 *   'prefix' => array(
 *     'default'   => 'main_',
 *     'users'     => 'shared_',
 *     'sessions'  => 'shared_',
 *     'role'      => 'shared_',
 *     'authmap'   => 'shared_',
 *   ),
 * @endcode
 * You can also use a reference to a schema/database as a prefix. This maybe
 * useful if your Drupal installation exists in a schema that is not the default
 * or you want to access several databases from the same code base at the same
 * time.
 * Example:
 * @code
 *   'prefix' => array(
 *     'default'   => 'main.',
 *     'users'     => 'shared.',
 *     'sessions'  => 'shared.',
 *     'role'      => 'shared.',
 *     'authmap'   => 'shared.',
 *   );
 * @endcode
 * NOTE: MySQL and SQLite's definition of a schema is a database.
 *
 * Database configuration format:
 * @code
 *   $databases['default']['default'] = array(
 *     'driver' => 'mysql',
 *     'database' => 'databasename',
 *     'username' => 'username',
 *     'password' => 'password',
 *     'host' => 'localhost',
 *     'prefix' => '',
 *   );
 *   $databases['default']['default'] = array(
 *     'driver' => 'pgsql',
 *     'database' => 'databasename',
 *     'username' => 'username',
 *     'password' => 'password',
 *     'host' => 'localhost',
 *     'prefix' => '',
 *   );
 *   $databases['default']['default'] = array(
 *     'driver' => 'sqlite',
 *     'database' => '/path/to/databasefilename',
 *   );
 * @endcode
 */

/**
 * Access control for update.php script.
 *
 * If you are updating your Drupal installation using the update.php script but
 * are not logged in using either an account with the "Administer software
 * updates" permission or the site maintenance account (the account that was
 * created during installation), you will need to modify the access check
 * statement below. Change the FALSE to a TRUE to disable the access check.
 * After finishing the upgrade, be sure to open this file again and change the
 * TRUE back to a FALSE!
 */
$update_free_access = FALSE;

/**
 * Salt for one-time login links and cancel links, form tokens, etc.
 *
 * This variable will be set to a random value by the installer. All one-time
 * login links will be invalidated if the value is changed.  Note that this
 * variable must have the same value on every web server.  If this variable is
 * empty, a hash of the serialized database credentials will be used as a
 * fallback salt.
 *
 * For enhanced security, you may set this variable to a value using the
 * contents of a file outside your docroot that is never saved together
 * with any backups of your Drupal files and database.
 *
 * Example:
 *   $drupal_hash_salt = file_get_contents('/home/example/salt.txt');
 *
 */
$drupal_hash_salt = 'lcn5wqtmp-MM13N7Bg6JcdutknT5-ExobajHj0ku6kI';

/**
 * Base URL (optional).
 *
 * If Drupal is generating incorrect URLs on your site, which could
 * be in HTML headers (links to CSS and JS files) or visible links on pages
 * (such as in menus), uncomment the Base URL statement below (remove the
 * leading hash sign) and fill in the absolute URL to your Drupal installation.
 *
 * You might also want to force users to use a given domain.
 * See the .htaccess file for more information.
 *
 * Examples:
 *   $base_url = 'http://www.example.com';
 *   $base_url = 'http://www.example.com:8888';
 *   $base_url = 'http://www.example.com/drupal';
 *   $base_url = 'https://www.example.com:8888/drupal';
 *
 * It is not allowed to have a trailing slash; Drupal will add it
 * for you.
 */
# $base_url = 'http://www.example.com';  // NO trailing slash!

/**
 * PHP settings:
 *
 * To see what PHP settings are possible, including whether they can be set at
 * runtime (by using ini_set()), read the PHP documentation:
 * http://www.php.net/manual/en/ini.list.php
 * See drupal_initialize_variables() in includes/bootstrap.inc for required
 * runtime settings and the .htaccess file for non-runtime settings. Settings
 * defined there should not be duplicated here so as to avoid conflict issues.
 */

/**
 * Some distributions of Linux (most notably Debian) ship their PHP
 * installations with garbage collection (gc) disabled. Since Drupal depends on
 * PHP's garbage collection for clearing sessions, ensure that garbage
 * collection occurs by using the most common settings.
 */
ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 100);

/**
 * Set session lifetime (in seconds), i.e. the time from the user's last visit
 * to the active session may be deleted by the session garbage collector. When
 * a session is deleted, authenticated users are logged out, and the contents
 * of the user's $_SESSION variable is discarded.
 */
ini_set('session.gc_maxlifetime', 200000);

/**
 * Set session cookie lifetime (in seconds), i.e. the time from the session is
 * created to the cookie expires, i.e. when the browser is expected to discard
 * the cookie. The value 0 means "until the browser is closed".
 */
ini_set('session.cookie_lifetime', 2000000);

/**
 * If you encounter a situation where users post a large amount of text, and
 * the result is stripped out upon viewing but can still be edited, Drupal's
 * output filter may not have sufficient memory to process it.  If you
 * experience this issue, you may wish to uncomment the following two lines
 * and increase the limits of these variables.  For more information, see
 * http://php.net/manual/en/pcre.configuration.php.
 */
# ini_set('pcre.backtrack_limit', 200000);
# ini_set('pcre.recursion_limit', 200000);

/**
 * Drupal automatically generates a unique session cookie name for each site
 * based on its full domain name. If you have multiple domains pointing at the
 * same Drupal site, you can either redirect them all to a single domain (see
 * comment in .htaccess), or uncomment the line below and specify their shared
 * base domain. Doing so assures that users remain logged in as they cross
 * between your various domains. Make sure to always start the $cookie_domain
 * with a leading dot, as per RFC 2109.
 */
# $cookie_domain = '.example.com';

/**
 * Variable overrides:
 *
 * To override specific entries in the 'variable' table for this site,
 * set them here. You usually don't need to use this feature. This is
 * useful in a configuration file for a vhost or directory, rather than
 * the default settings.php. Any configuration setting from the 'variable'
 * table can be given a new value. Note that any values you provide in
 * these variable overrides will not be modifiable from the Drupal
 * administration interface.
 *
 * The following overrides are examples:
 * - site_name: Defines the site's name.
 * - theme_default: Defines the default theme for this site.
 * - anonymous: Defines the human-readable name of anonymous users.
 * Remove the leading hash signs to enable.
 */
# $conf['site_name'] = 'My Drupal site';
# $conf['theme_default'] = 'garland';
# $conf['anonymous'] = 'Visitor';

/**
 * A custom theme can be set for the offline page. This applies when the site
 * is explicitly set to maintenance mode through the administration page or when
 * the database is inactive due to an error. It can be set through the
 * 'maintenance_theme' key. The template file should also be copied into the
 * theme. It is located inside 'modules/system/maintenance-page.tpl.php'.
 * Note: This setting does not apply to installation and update pages.
 */
# $conf['maintenance_theme'] = 'bartik';

/**
 * Reverse Proxy Configuration:
 *
 * Reverse proxy servers are often used to enhance the performance
 * of heavily visited sites and may also provide other site caching,
 * security, or encryption benefits. In an environment where Drupal
 * is behind a reverse proxy, the real IP address of the client should
 * be determined such that the correct client IP address is available
 * to Drupal's logging, statistics, and access management systems. In
 * the most simple scenario, the proxy server will add an
 * X-Forwarded-For header to the request that contains the client IP
 * address. However, HTTP headers are vulnerable to spoofing, where a
 * malicious client could bypass restrictions by setting the
 * X-Forwarded-For header directly. Therefore, Drupal's proxy
 * configuration requires the IP addresses of all remote proxies to be
 * specified in $conf['reverse_proxy_addresses'] to work correctly.
 *
 * Enable this setting to get Drupal to determine the client IP from
 * the X-Forwarded-For header (or $conf['reverse_proxy_header'] if set).
 * If you are unsure about this setting, do not have a reverse proxy,
 * or Drupal operates in a shared hosting environment, this setting
 * should remain commented out.
 *
 * In order for this setting to be used you must specify every possible
 * reverse proxy IP address in $conf['reverse_proxy_addresses'].
 * If a complete list of reverse proxies is not available in your
 * environment (for example, if you use a CDN) you may set the
 * $_SERVER['REMOTE_ADDR'] variable directly in settings.php.
 * Be aware, however, that it is likely that this would allow IP
 * address spoofing unless more advanced precautions are taken.
 */
$conf['reverse_proxy'] = TRUE;

/**
 * Specify every reverse proxy IP address in your environment.
 * This setting is required if $conf['reverse_proxy'] is TRUE.
 */
if ($conf['reverse_proxy'] && isset($_ENV['AH_SITE_ENVIRONMENT'])) {
  $filepath = '/mnt/gfs/wfmcom.' .  $_ENV['AH_SITE_ENVIRONMENT'] . '/sites/default/files/private/ip_addresses.txt';
  if (file_exists($filepath)) {
    $content = unserialize(file_get_contents($filepath));
    if (is_array($content)) {
      $conf['reverse_proxy_addresses'] = $content;
    }
  }
}

/**
 * Set this value if your proxy server sends the client IP in a header
 * other than X-Forwarded-For.
 */
# $conf['reverse_proxy_header'] = 'HTTP_X_CLUSTER_CLIENT_IP';

/**
 * Page caching:
 *
 * By default, Drupal sends a "Vary: Cookie" HTTP header for anonymous page
 * views. This tells a HTTP proxy that it may return a page from its local
 * cache without contacting the web server, if the user sends the same Cookie
 * header as the user who originally requested the cached page. Without "Vary:
 * Cookie", authenticated users would also be served the anonymous page from
 * the cache. If the site has mostly anonymous users except a few known
 * editors/administrators, the Vary header can be omitted. This allows for
 * better caching in HTTP proxies (including reverse proxies), i.e. even if
 * clients send different cookies, they still get content served from the cache.
 * However, authenticated users should access the site directly (i.e. not use an
 * HTTP proxy, and bypass the reverse proxy if one is used) in order to avoid
 * getting cached pages from the proxy.
 */
# $conf['omit_vary_cookie'] = TRUE;

/**
 * CSS/JS aggregated file gzip compression:
 *
 * By default, when CSS or JS aggregation and clean URLs are enabled Drupal will
 * store a gzip compressed (.gz) copy of the aggregated files. If this file is
 * available then rewrite rules in the default .htaccess file will serve these
 * files to browsers that accept gzip encoded content. This allows pages to load
 * faster for these users and has minimal impact on server load. If you are
 * using a webserver other than Apache httpd, or a caching reverse proxy that is
 * configured to cache and compress these files itself you may want to uncomment
 * one or both of the below lines, which will prevent gzip files being stored.
 */
# $conf['css_gzip_compression'] = FALSE;
# $conf['js_gzip_compression'] = FALSE;

/**
 * String overrides:
 *
 * To override specific strings on your site with or without enabling locale
 * module, add an entry to this list. This functionality allows you to change
 * a small number of your site's default English language interface strings.
 *
 * Remove the leading hash signs to enable.
 */
# $conf['locale_custom_strings_en'][''] = array(
#   'forum'      => 'Discussion board',
#   '@count min' => '@count minutes',
# );

/**
 *
 * IP blocking:
 *
 * To bypass database queries for denied IP addresses, use this setting.
 * Drupal queries the {blocked_ips} table by default on every page request
 * for both authenticated and anonymous users. This allows the system to
 * block IP addresses from within the administrative interface and before any
 * modules are loaded. However on high traffic websites you may want to avoid
 * this query, allowing you to bypass database access altogether for anonymous
 * users under certain caching configurations.
 *
 * If using this setting, you will need to add back any IP addresses which
 * you may have blocked via the administrative interface. Each element of this
 * array represents a blocked IP address. Uncommenting the array and leaving it
 * empty will have the effect of disabling IP blocking on your site.
 *
 * Remove the leading hash signs to enable.
 */
# $conf['blocked_ips'] = array(
#   'a.b.c.d',
# );

/**
 * Fast 404 pages:
 *
 * Drupal can generate fully themed 404 pages. However, some of these responses
 * are for images or other resource files that are not displayed to the user.
 * This can waste bandwidth, and also generate server load.
 *
 * The options below return a simple, fast 404 page for URLs matching a
 * specific pattern:
 * - 404_fast_paths_exclude: A regular expression to match paths to exclude,
 *   such as images generated by image styles, or dynamically-resized images.
 *   If you need to add more paths, you can add '|path' to the expression.
 * - 404_fast_paths: A regular expression to match paths that should return a
 *   simple 404 page, rather than the fully themed 404 page. If you don't have
 *   any aliases ending in htm or html you can add '|s?html?' to the expression.
 * - 404_fast_html: The html to return for simple 404 pages.
 *
 * Add leading hash signs if you would like to disable this functionality.
 */

/**
 * By default, fast 404s are returned as part of the normal page request
 * process, which will properly serve valid pages that happen to match and will
 * also log actual 404s to the Drupal log. Alternatively you can choose to
 * return a 404 now by uncommenting the following line. This will reduce server
 * load, but will cause even valid pages that happen to match the pattern to
 * return 404s, rather than the actual page. It will also prevent the Drupal
 * system log entry. Ensure you understand the effects of this before enabling.
 *
 * To enable this functionality, remove the leading hash sign below.
 */
# drupal_fast_404();

/**
 * Authorized file system operations:
 *
 * The Update manager module included with Drupal provides a mechanism for
 * site administrators to securely install missing updates for the site
 * directly through the web user interface by providing either SSH or FTP
 * credentials. This allows the site to update the new files as the user who
 * owns all the Drupal files, instead of as the user the webserver is running
 * as. However, some sites might wish to disable this functionality, and only
 * update the code directly via SSH or FTP themselves. This setting completely
 * disables all functionality related to these authorized file operations.
 *
 * Remove the leading hash signs to disable.
 */
# $conf['allow_authorize_operations'] = FALSE;

// Ensure that cron can only ever run from an external trigger
$conf['cron_safe_threshold'] = 0;

// Disable core search indexing
$conf['search_cron_limit'] = 0;

// OpenGraph metadata generation will be skipped for these nodes.
$conf['og_metadata_skip_nids'] = array('6850');

/**
 * Special configuration for John Mackey's blog
 */
$conf['blog_comment_posting_guidelines'] = array(
  'default' => array(
    'nid' => 24079,
    'extra' => '',
  ),
  '6861' => array(
    'nid' => 21637,
    'extra' => ' This blog is intended to discuss only the specific topics that '
      . 'John Mackey writes about. You are welcome to address other topics in our'
      . ' <a href="http://www.wholefoodsmarket.com/forums/index.php">forums</a>.',
  ),
);

// Enables CDN far future expiration
#$conf['cdn_farfuture_status'] = 1;

// TODO: Uncomment lines below for upgraded smart_ip module
// Use external service. Omits request to empty table.
$conf['smart_ip_source'] = 'ipinfodb_service';
// Skip updating of local Smart IP database. Cause of cron run fail.
$conf['smart_ip_auto_update'] = 0;
// Store geolocation data to session for authenticated users.
$conf['smart_ip_roles_to_geolocate'] = array(2);

// Make sure drush keeps working.
// Modified from function drush_verify_cli()
$cli = (php_sapi_name() == 'cli');

# Pinterest API
# TODO - Is this an override for a specific AH_ENV?  Should move elsewhere, maybe wfmvariables?
$conf['wfm_variable__social_pinterest_client_id'] = '1433528';
$conf['wfm_variable__social_pinterest_client_secret'] = 'cf59dbdab157a03ab6ed08026844432e347a9536';

# Global Composer Manager configs
$conf['composer_manager_file_dir'] = '../';
$conf['composer_manager_vendor_dir'] = '../vendor';

// Allows Block to be cached on sites with hook_node_grant() implementations.
// This will prevent disabling of a block caching on all environments.
$conf['block_cache_bypass_node_grants'] = TRUE;

/**
 * Harmony Email Service Provider API configs.
 */
$conf['harmony'] = array(
  'client_id' => 'harmony_WFM_RTM',
  'secret_key' => 'mk@pX4r@qT',
  'user_name' => 'WFM_RTM_API',
  'password' => 'p8athu&ecH',
  'auth_url' => 'https://api-public.epsilon.com/Epsilon/oauth2/access_token',
  'base_url' => 'https://api.harmony.epsilon.com/',
  'scope' => 'cn mail sn givenname uid employeeNumber',
  'uid' => '48267c46-076d-41e7-b370-8f1f6ae94c3b',
);

/**
 * Acquia Configuration
 *
 * Lines in this section are specific to Acquia environments.
 */
if (isset($_ENV['AH_SITE_ENVIRONMENT'])) {
  /**************************
   * Acquia Host: ALL        *
   **************************/

  $conf['https'] = TRUE;

  //Increase php memory from 128MB to 512MB for all admin paths.
  if (isset($_GET['q']) && strpos($_GET['q'], 'admin') === 0) {
    ini_set('memory_limit', '512M');
  }

  # mobile settings
  if (($_SERVER['HTTP_HOST'] == 'wholefoodsmdev.prod.acquia-sites.com') ||
    ($_SERVER['HTTP_HOST'] == 'm.stage.wholefoodsmarket.com') ||
    ($_SERVER['HTTP_HOST'] == 'm.preview.wholefoodsmarket.com') ||
    ($_SERVER['HTTP_HOST'] == 'm.wholefoodsmarket.com') ||
    ($_SERVER['HTTP_HOST'] == 'm.d.wholefoodsmarket.com') ||
    ($_SERVER['HTTP_HOST'] == 'm.d2.wholefoodsmarket.com') ||
    ($_SERVER['HTTP_HOST'] == 'm.d3.wholefoodsmarket.com') ||
    ($_SERVER['HTTP_HOST'] == 'm.d4.wholefoodsmarket.com') ||
    ($_SERVER['HTTP_HOST'] == 'm.d5.wholefoodsmarket.com') ||
    ($_SERVER['HTTP_HOST'] == 'm.mdev.wholefoodsmarket.com') ||
    ($_SERVER['HTTP_HOST'] == 'm.local.wholefoodsmarket.com')
  ) {
    $conf['theme_default'] = 'wholefoods_mobile';
    // $conf['site_frontpage'] = '/';
  }

  // Set Drupal cache backends and cache classes
  $conf['cache_backends'][] = 'includes/cache-install.inc';
  $conf['cache_backends'][] = 'sites/all/modules/contrib/memcache/memcache.inc';
  $conf['cache_default_class'] = 'MemCacheDrupal';
  $conf['cache_class_cache_page'] = 'DrupalFakeCache';

  // Don't bootstrap the database when serving pages from the cache.
  $conf['page_cache_without_database'] = TRUE;
  $conf['page_cache_invoke_hooks'] = FALSE;

  // Allow redirects to be saved into the page cache
  $conf['redirect_page_cache'] = TRUE;

  // The 'cache_form' bin must be assigned to non-volatile storage.
  $conf['cache_class_cache_form'] = 'DrupalDatabaseCache';

  // Cache bins that often grow too large to keep in memcache
  // $conf['cache_class_cache_entity_bean'] = 'DrupalDatabaseCache';
  // $conf['cache_class_cache_entity_comment'] = 'DrupalDatabaseCache';
  // $conf['cache_class_cache_entity_file'] = 'DrupalDatabaseCache';
  // $conf['cache_class_cache_entity_node'] = 'DrupalDatabaseCache';

  // Move locks to memcache to bypass a semaphore stampede -- needs testing
  $conf['lock_inc'] = 'sites/all/modules/contrib/memcache/memcache-lock.inc';
  $conf['memcache_stampede_protection'] = TRUE;

  $conf['memcache_stampede_protection_ignore'] = array(
    // Ignore stampede protection for the entire bin due to delayed cache_set in
    // drupal_lookup_path() until drupal_cache_system_paths().
    'cache_path',
    // Ignore stampede protection for cache_views because of delayed cache_set.
    'cache_views',
    // Ignore some cids in 'cache_bootstrap'.
    'cache_bootstrap' => array(
      'module_implements',
      'variables',
      'schema:runtime:*',
      'theme_registry:runtime:*',
    ),
  );

  //Allow writes to search indexes
  $conf['search_api_mode'] = 'write-all';


  /**
   * Composer Manager Settings. Don't rebuild packages on any Acquia server.
   */
  $conf['composer_manager_autobuild_file'] = 0;
  $conf['composer_manager_autobuild_packages'] = 0;

  // Acquia Purge settings
   $conf['acquia_purge_cron'] = TRUE; // queue processing entirely to cron
   $conf['acquia_purge_http'] = TRUE; // purge http paths
   $conf['acquia_purge_https'] = TRUE; // purge https paths

  /**************************
   * Acquia Host: PRODUCTION *
   **************************/
  if (isset($_ENV['AH_PRODUCTION'])) {
    $conf['site_env'] = 'prod';

    // Enforce caching in production.
    $conf['cache'] = TRUE;

    // When using varnish, set cache_lifetime to 0.
    // Setting this to 0 allows direct cache clears to clear caches that
    // have not yet reached expiration.
    // Sites with large amounts of content creation/edits could benifit from
    // setting this to a non-zero value as it impacts the block cache as well.
    // $conf['cache_lifetime'] = 0;

    // Set default cache expiration to 30 minutes.
    // $conf['page_cache_maximum_age'] = 1800;

    // $conf['block_cache'] = TRUE;

    // Enforce aggregation and compression.
    $conf['page_compression'] = TRUE;
    $conf['preprocess_css'] = TRUE;
    $conf['preprocess_js'] = TRUE;

    // Allow writes to search indexes
    $conf['search_api_mode'] = 'write-all';

    // Override domain detection in Acquia Purge
    $conf['acquia_purge_domains'] = array(
      'www.wholefoodsmarket.com',
      'm.wholefoodsmarket.com',
    );

    // Fast 404 pages
    $conf['404_fast_paths_exclude'] = '/\/(?:styles|media_crop)\//';
    $conf['404_fast_paths'] = '/\.(?:txt|png|gif|jpe?g|css|js|ico|swf|flv|cgi|bat|pl|dll|exe|asp)$/i';
    $conf['404_fast_html'] = '<html xmlns="http://www.w3.org/1999/xhtml"><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL "@path" was not found on this server.</p></body></html>';

    // Whitelist advagg cache.
    $conf['fast_404_string_whitelisting'][] = '/advagg_';
  }

  /**************************
   * Acquia Host: OTHERS     *
   **************************/
  elseif (isset($_ENV['AH_NON_PRODUCTION'])) {
    $conf['site_env'] = 'dev';

    /**
     * Fast 404 pages
     */
    $conf['404_fast_paths_exclude'] = '/\/(?:styles|media_crop)\//';
    $conf['404_fast_html'] = '<html xmlns="http://www.w3.org/1999/xhtml"><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL "@path" was not found on this server.</p></body></html>';

    // Whitelist advagg cache.
    $conf['fast_404_string_whitelisting'][] = '/advagg_';

  }

  // Additional tweaks for specific Acquia environments based on AH_SITE_ENVIRONMENT
  // To add more, use case variables available from /admin/reports/status/php
  switch ($_ENV['AH_SITE_ENVIRONMENT']) {
    case 'dev':
      // Map estore destination to Spinternet dev
      $conf['wfm_estore_url'] = 'shop-dev-rs.wholefoodsmarket.com';
      $conf['googleanalytics_account'] = 'UA-190385-13';

      // Override domain detection in Acquia Purge
      $conf['acquia_purge_domains'] = array(
        'd.wholefoodsmarket.com',
        'm.d.wholefoodsmarket.com',
      );
      break;

    case 'dev2':
      // Override domain detection in Acquia Purge
      $conf['acquia_purge_domains'] = array(
        'd2.wholefoodsmarket.com',
        'm.d2.wholefoodsmarket.com',
      );
      break;

    case 'dev3':
      // Override domain detection in Acquia Purge
      $conf['acquia_purge_domains'] = array(
        'd3.wholefoodsmarket.com',
        'm.d3.wholefoodsmarket.com',
      );
      break;

    case 'dev4':
      // Override domain detection in Acquia Purge
      $conf['acquia_purge_domains'] = array(
        'd4.wholefoodsmarket.com',
        'm.d4.wholefoodsmarket.com',
      );
      break;

    case 'dev5':
      // Override domain detection in Acquia Purge
      $conf['acquia_purge_domains'] = array(
        'd5.wholefoodsmarket.com',
        'm.d5.wholefoodsmarket.com',
      );
      break;

    case 'mdev':
      // Override domain detection in Acquia Purge
      $conf['acquia_purge_domains'] = array(
        'mdev.wholefoodsmarket.com',
        'm.mdev.wholefoodsmarket.com',
      );
      break;

    case 'test':
      // Map estore destination to Sprinternet test
      $conf['wfm_estore_url'] = 'shop-qa-rs.wholefoodsmarket.com';
      $conf['googleanalytics_account'] = 'UA-190385-13';

      // Override domain detection in Acquia Purge
      $conf['acquia_purge_domains'] = array(
        'stage.wholefoodsmarket.com',
        'm.stage.wholefoodsmarket.com',
      );
      break;

    case 'test2':
      // Override domain detection in Acquia Purge
      $conf['acquia_purge_domains'] = array(
        'preview.wholefoodsmarket.com',
        'm.preview.wholefoodsmarket.com',
      );
      break;
  }
}
else {
  /**
   * Local and Developer Configuration
   *
   * Lines in this section are specific to running on Local environments.
   *
   * Don't hack this file for local development. Create a file named
   * settings.local.php in this directory, and add any needed customization there.
   */

  // Default local search environments to read only
  $conf['search_api_mode'] = 'readonly';
}

// Settings for ALL NON-PRODUCTION environments including locals
if ((isset($_ENV['AH_NON_PRODUCTION'])) || (!isset($_ENV['AH_SITE_ENVIRONMENT']))) {

  /**
   * Limit customer service forms to communicate with salesforce QA environment only.
   */
  $conf['wfm_variable__sf_org_id'] = '00D540000008mEf';
  $conf['wfm_variable__sf_post_url'] = 'https://wholefoods--QA.cs40.my.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8';
  $conf['wfm_variable__sf_category_field'] = '00NF000000CThMH';
  $conf['wfm_variable__sf_store_field'] = '00NF000000CThMY';
  $conf['wfm_variable__sf_upc_field'] = '00NF000000CThMc';
  $conf['wfm_variable__sf_zip_field'] = '00NF000000CThMZ';
  $conf['wfm_variable__sf_tlc_field'] = '00NF000000D0Eps';
  $conf['wfm_variable__sf_ua_field'] = '00NF000000D0G5v';

  /**
   * Include a local settings file if it exists.
   */
  $local_settings = dirname(__FILE__) . '/settings.local.php';
  if (file_exists($local_settings)) {
    include $local_settings;
    $conf['site_env'] = 'local';
  }
}

/**
 * SAGE config settings for eStore
 *
 * New dev / qa end points:
 * api-qa.wholefoodsmarket.com
 * api-integration.wholefoodsmarket.com
 */
if (isset($_ENV['AH_PRODUCTION'])) {
  // PROD
  $conf['sage_config'] = array(
    'sage_get_token' => 'https://api.wholefoodsmarket.com/oauth20/token',
    'sage_refresh_token' => 'https://api.wholefoodsmarket.com/oauth20/token',
    'spin' => 'https://api.wholefoodsmarket.com/v1/estore?access_token=',
    'stores' => 'https://api.wholefoodsmarket.com/v2/stores/',
    'discount' => 'https://api.wholefoodsmarket.com/v1/card/tm/view/',
    'always_error' => FALSE,
    'client_id' => 'QI77A4tufexEgOKMJch1G8KgcdxHkENp',
    'client_secret' => 'n0RTAK7GotU84Da4',
    'litle_host' => 'https://request.securepaypage-litle.com/LitlePayPage/js/payframe-client.min.js',
    'payPageIdUK' => '4vMh3Xs2AKp9c3eF',
    'payPageIdCAN' => 'cCmwa4zwr2VXNDHQ',
    'payPageIdUSA' => '22TEVtpcSocbT6fN',
  );
} elseif (isset($_ENV['AH_SITE_ENVIRONMENT'])
  && strstr($_ENV['AH_SITE_ENVIRONMENT'],'dev')) {
  // DEV
  $conf['sage_config'] = array(
    'sage_get_token' => 'https://api-test.wholefoodsmarket.com/oauth20/token',
    'sage_refresh_token' => 'https://api-test.wholefoodsmarket.com/oauth20/token',
    'spin' => 'https://api-test.wholefoodsmarket.com/v1/dev-estore?access_token=', // Dev Spinternet API
    'stores' => 'https://api-test.wholefoodsmarket.com/v2/stores/',
    'discount' => 'https://api-test.wholefoodsmarket.com/v1/card/tm/view/',
    'always_error' => FALSE,
    'client_id' => 'QI77A4tufexEgOKMJch1G8KgcdxHkENp',
    'client_secret' => 'n0RTAK7GotU84Da4',
    'litle_host' => 'https://request-prelive.np-securepaypage-litle.com/LitlePayPage/js/payframe-client.min.js',
    'payPageIdUK' => '4vMh3Xs2AKp9c3eF',
    'payPageIdCAN' => 'cCmwa4zwr2VXNDHQ',
    'payPageIdUSA' => '22TEVtpcSocbT6fN',
  );
} else {
  // QA
  $conf['sage_config'] = array(
    'sage_get_token' => 'https://api-test.wholefoodsmarket.com/oauth20/token',
    'sage_refresh_token' => 'https://api-test.wholefoodsmarket.com/oauth20/token',
    'spin' => 'https://api-test.wholefoodsmarket.com/v1/estore?access_token=', // QA, Litle feature branch
    'stores' => 'https://api-test.wholefoodsmarket.com/v2/stores/',
    'discount' => 'https://api-test.wholefoodsmarket.com/v1/card/tm/view/',
    'always_error' => FALSE,
    'client_id' => 'QI77A4tufexEgOKMJch1G8KgcdxHkENp',
    'client_secret' => 'n0RTAK7GotU84Da4',
    'litle_host' => 'https://request-prelive.np-securepaypage-litle.com/LitlePayPage/js/payframe-client.min.js',
    'payPageIdUK' => '4vMh3Xs2AKp9c3eF',
    'payPageIdCAN' => 'cCmwa4zwr2VXNDHQ',
    'payPageIdUSA' => '22TEVtpcSocbT6fN',
  );
}

/**
 * eStore Cookie Salt
 */
$conf['estore_cookie_salt'] = '@#$KL@J$NLKDSJFL:I$R!@#:k';


// Settings for ALL NON-PRODUCTION environments including locals
if ((isset($_ENV['AH_NON_PRODUCTION'])) || (!isset($_ENV['AH_SITE_ENVIRONMENT']))) {
  /**
   * stage file proxy module will get images from prod
   */
  $conf['stage_file_proxy_origin'] = 'http://www.wholefoodsmarket.com';
  // needed for fast_404 module NOT to serve image 404s before stage_file_proxy
  $conf['404_fast_paths'] = '/\.(?:txt|css|js|ico|swf|flv|cgi|bat|pl|dll|exe|asp)$/i';
  $conf['fast_404_exts'] = '/\.(txt|js|ico|swf|flv|cgi|bat|pl|dll|exe|asp|)$/i';

  // Whitelist advagg cache.
  $conf['fast_404_string_whitelisting'][] = '/advagg_';
}

/**
 * Acquia Search overrides for Search API Acquia
 * NOTE: THIS SHOULD GO BELOW the Acquia include line.
 *
 * QD-2802
 */
$acquia_search_server_id = "acquia_search";
$core_set = false;
if (isset($_ENV["AH_SITE_ENVIRONMENT"])) {

  // Entry for DLMT-119294.prod.default - wfmcom
  if ($_ENV["AH_SITE_ENVIRONMENT"] == "prod") {
      // New Production core.
      $conf["search_api_acquia_overrides"][$acquia_search_server_id] = array(
        "path" => "/solr/DLMT-119294.prod.alt2",
        "host" => "useast1-c70.acquia-search.com",
        "derived_key" => "161b35e4e8a4538d2357044b933c38e78e4b71a5",
      );
    }
    $core_set = true;

  // Entry for DLMT-119294.dev.default - wfmcom
  if ($_ENV["AH_SITE_ENVIRONMENT"] == "dev") {
    $conf["search_api_acquia_overrides"][$acquia_search_server_id] = array(
      "path" => "/solr/DLMT-119294.dev.default",
      "host" => "useast1-c1.acquia-search.com",
      "derived_key" => "40e5b5f62e3469dbc603e2b6d6cb3f4463789f88",
    );
    $core_set = true;
  }

  // Entry for DLMT-119294.dev2.default - wfmcom
  if ($_ENV["AH_SITE_ENVIRONMENT"] == "dev2") {
    $conf["search_api_acquia_overrides"][$acquia_search_server_id] = array(
      "path" => "/solr/DLMT-119294.dev2.default",
      "host" => "useast1-c1.acquia-search.com",
      "derived_key" => "fef4ce645018bc341dfdfa091263345df31a8c19",
    );
    $core_set = true;
  }

  // Entry for DLMT-119294.dev3.default - wfmcom
  if ($_ENV["AH_SITE_ENVIRONMENT"] == "dev3") {
    $conf["search_api_acquia_overrides"][$acquia_search_server_id] = array(
      "path" => "/solr/DLMT-119294.dev3.default",
      "host" => "useast1-c1.acquia-search.com",
      "derived_key" => "967c2e81534db64fc50720c9abce22311c57b896",
    );
    $core_set = true;
  }

  // Entry for DLMT-119294.dev4.default - wfmcom
  if ($_ENV["AH_SITE_ENVIRONMENT"] == "dev4") {
    $conf["search_api_acquia_overrides"][$acquia_search_server_id] = array(
      "path" => "/solr/DLMT-119294.dev4.default",
      "host" => "useast1-c1.acquia-search.com",
      "derived_key" => "4d389c6ff2b2331e1aec3dc8c0bbed6599241193",
    );
    $core_set = true;
  }

  // Entry for DLMT-119294.dev5.default - wfmcom
  if ($_ENV["AH_SITE_ENVIRONMENT"] == "dev5") {
    $conf["search_api_acquia_overrides"][$acquia_search_server_id] = array(
      "path" => "/solr/DLMT-119294.dev5.default",
      "host" => "useast1-c1.acquia-search.com",
      "derived_key" => "363aec2204792fb3973446fbca2a131903bc8123",
    );
    $core_set = true;
  }

  // Entry for DLMT-119294.mdev.default - wfmcom
  if ($_ENV["AH_SITE_ENVIRONMENT"] == "mdev") {
    $conf["search_api_acquia_overrides"][$acquia_search_server_id] = array(
      "path" => "/solr/DLMT-119294.mdev.default",
      "host" => "useast1-c1.acquia-search.com",
      "derived_key" => "a97751038e4be51cbf8414d11efd4be4b6030d17",
    );
    $core_set = true;
  }

  // Entry for DLMT-119294.test.default - wfmcom
  if ($_ENV["AH_SITE_ENVIRONMENT"] == "test") {
    $conf["search_api_acquia_overrides"][$acquia_search_server_id] = array(
      "path" => "/solr/DLMT-119294.test.default",
      "host" => "useast1-c1.acquia-search.com",
      "derived_key" => "454493bc8fc15e4ecf8fb2d3a4c8a34c35cf233f",
    );
    $core_set = true;
  }

  // Entry for DLMT-119294.test2.default - wfmcom
  if ($_ENV["AH_SITE_ENVIRONMENT"] == "test2") {
    $conf["search_api_acquia_overrides"][$acquia_search_server_id] = array(
      "path" => "/solr/DLMT-119294.test2.default",
      "host" => "useast1-c1.acquia-search.com",
      "derived_key" => "9ed35c2f080787426618c2887fb2ffbcd143b59c",
    );
    $core_set = true;
  }
}

if (! $core_set) {
  // Found no matching case, or not currently in Acquia Cloud.
  // Define a fallback core below.
  $conf["search_api_acquia_overrides"][$acquia_search_server_id] = array(
    "path" => "/solr/DLMT-119294.dev.default",
    "host" => "useast1-c1.acquia-search.com",
    "derived_key" => "40e5b5f62e3469dbc603e2b6d6cb3f4463789f88",
  );
}
// End Acquia Search overrides for Search API Acquia.
