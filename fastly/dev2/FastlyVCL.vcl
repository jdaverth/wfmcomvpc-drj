sub vcl_recv {

#FASTLY recv

    if (!req.http.Fastly-FF) {
        set req.http.Fastly-Country-Code = geoip.country_code;
        set req.http.Fastly-City = geoip.city;
        set req.http.Fastly-Latitude = geoip.latitude;
        set req.http.Fastly-Longitude = geoip.longitude;
        set req.http.Fastly-Country = geoip.country_name;
        set req.http.Fastly-Metro = geoip.metro_code;
    }

    # bypass all caching
    if(req.url){
      return(pass);
    }

    if (req.request != "HEAD" && req.request != "GET" && req.request != "PURGE") {
      return(pass);
    }


    return(lookup);
}

sub vcl_fetch {

#FASTLY fetch

  if ((beresp.status == 500 || beresp.status == 503) && req.restarts < 1 && (req.request == "GET" || req.request == "HEAD")) {
    restart;
  }

  if(req.restarts > 0 ) {
    set beresp.http.Fastly-Restarts = req.restarts;
  }

  if (beresp.http.Set-Cookie) {
    set req.http.Fastly-Cachetype = "SETCOOKIE";
    return (pass);
  }

  if (beresp.http.Cache-Control ~ "private") {
    set req.http.Fastly-Cachetype = "PRIVATE";
    return (pass);
  }

  if (beresp.status == 500 || beresp.status == 503) {
    set req.http.Fastly-Cachetype = "ERROR";
    set beresp.ttl = 1s;
    set beresp.grace = 5s;
    return (deliver);
  }


  if (beresp.http.Expires || beresp.http.Surrogate-Control ~ "max-age" || beresp.http.Cache-Control ~"(s-maxage|max-age)") {
    # keep the ttl here
  } else {
    # apply the default ttl
    set beresp.ttl = 0s;
  }

  return(deliver);
}

sub vcl_hit {
#FASTLY hit

  if (!obj.cacheable) {
    return(pass);
  }
  return(deliver);
}

sub vcl_miss {
#FASTLY miss
  return(fetch);
}

sub vcl_deliver {
#FASTLY deliver
  return(deliver);
}

sub vcl_error {
#FASTLY error
}

sub vcl_pass {
#FASTLY pass
}
