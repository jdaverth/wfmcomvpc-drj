sub vcl_recv {

#FASTLY recv

    # Add the fastly country code header.
    # geoip will only be captured on the edge servers from the user.
    # This will work fine for shielding or non-shielding (as in production)
    if (!req.http.Fastly-FF) {
        set req.http.Fastly-Country-Code = geoip.country_code;
        set req.http.Fastly-City = geoip.city;
        set req.http.Fastly-Latitude = geoip.latitude;
        set req.http.Fastly-Longitude = geoip.longitude;
        set req.http.Fastly-Country = geoip.country_name;
        set req.http.Fastly-Metro = geoip.metro_code;
    }

    # Add X-Local-Store vary header if fastly detects a cookie
    if (req.http.Cookie:local_store) {
        # Set store id value to a header called X-Local-Store
        set req.http.X-Local-Store = req.http.Cookie:local_store ;
    }

    # strip out certain querystring params that varnish should not vary cache by
    call normalize_req_url;

    # Detect the device and set the req.http.X-UA-Device
    # and req.http.X-UA-Vendor headers
    # Note. device can be mobile/tablet/smartphone.
    call detect_device;


    # Simplified mobile detection.
    if ((req.http.X-UA-Device == "mobile" || req.http.X-UA-Device == "tablet" || req.http.X-UA-Device == "smartphone") && req.http.User-agent !~ "(?i)iPad") {
        set req.http.is-wfm-mobile-device = "true";
    } else {
        set req.http.is-wfm-mobile-device = "false";
    }

    if (req.http.X-UA-Device == "tablet" && req.http.User-agent ~ "(?i)iPad") {
        set req.http.is-wfm-ipad-device = "true";
    } else {
        set req.http.is-wfm-ipad-device = "false";
    }

    # Redirection Strategy:
    #  1. mobile/tablet/smartphone + !ipad www->m.
    #  2. mobile/tablet/smartphone + !ipad (mdev., d., d2, d3, d4, d5, stage, preview) -> m.mdev., m.d., m.d2., m.d3. and so on
    #  3. iPad only m.->www
    #  4. ipad only (m.mdev., m.d., m.d2, m.d3, m.d4, m.d5, m.stage, m.preview) -> mdev., d., d2., d3. and so on
    #  5. desktop only (m.mdev., m.d., m.d2, m.d3, m.d4, m.d5, m.stage, m.preview) -> mdev., d., d2., d3. and so on
    #  6. desktop only (m.wholefoodsmarket.com -> www.wholefoodsmarket.com)


    # 1. mobile/tablet/smartphone + !ipad www->m.
    # Redirect to m. for mobile devices except ipad.
    # for main domain ie. www.
    if (req.http.is-wfm-mobile-device == "true") {
        if (req.http.host ~ "^www.wholefoodsmarket.com") {
            set req.http.newhost = regsub(req.http.host, "^(www\.)(.*)", if(req.is_ssl, "https://m.\2", "http://m.\2"));
            error 750 req.http.newhost;
        }
    }

    #  2. mobile/tablet/smartphone + !ipad (mdev., d., d2, d3, d4, d5, stage, preview) -> m.mdev., m.d., m.d2., m.d3. and so on
    # Redirect to m. for mobile devices except ipad.
    # for all lower lifecycle enviros.
    # eg. mdev.wholefoodsmarket.com
    if (req.http.is-wfm-mobile-device == "true") {
        if (req.http.host ~ "^(\w+\.)(wholefoodsmarket.com+.*)") {
            # Don't re-redirect if we've already redirected to m. from condition 1.
            if (req.http.host !~ "m.wholefoodsmarket.com") {
                set req.http.newhost = regsub(req.http.host, "^(\w+\.)(wholefoodsmarket.com+.*)", if(req.is_ssl, "https://m.\1\2", "http://m.\1\2"));
                error 750 req.http.newhost;
            }
        }
    }

    # 3. iPad only m.->www
    # Redirect to desktop if user types m.wholefoodsmarket.com on an ipad
    # ie. main domain
    if (req.http.is-wfm-ipad-device == "true") {
        if (req.http.host ~ "^m.wholefoodsmarket.com") {
            set req.http.newhost = regsub(req.http.host, "^(m\.)(.*)", if(req.is_ssl, "https://www.\2", "http://www.\2"));
            error 750 req.http.newhost;
        }
    }

    # 4. ipad only (m.mdev., m.d., m.d2, m.d3, m.d4, m.d5, m.stage, m.preview) -> d., d2., d3. and so on
    # Redirect to desktop if user types m.d.wholefoodsmarket or m.mdev.wholefoodsmarket
    # ie all lower lifecycle environments
    if (req.http.is-wfm-ipad-device == "true") {
        if (req.http.host ~ "^m\.(\w+\.)(wholefoodsmarket.com+.*)") {
            set req.http.newhost = regsub(req.http.host, "^m\.(\w+\.)(wholefoodsmarket.com+.*)", if(req.is_ssl, "https://\1\2", "http://\1\2"));
            error 750 req.http.newhost;
        }
    }

    #  5. desktop only (m.mdev., m.d., m.d2, m.d3, m.d4, m.d5, m.stage, m.preview) -> mdev., d., d2., d3. and so on
    # Force user to desktop even if they type m.d.wholefoodsmarket
    # on a desktop device
    # ie all lower lifecycle environments
    if (req.http.X-UA-Device == "desktop") {
        if (req.http.host ~ "^m\.(\w+\.)(wholefoodsmarket.com+.*)") {
            set req.http.newhost = regsub(req.http.host, "^m\.(\w+\.)(wholefoodsmarket.com+.*)", if(req.is_ssl, "https://\1\2", "http://\1\2"));
            error 750 req.http.newhost;
        }
    }

    #  6. desktop only (m.wholefoodsmarket.com -> www.wholefoodsmarket.com)
    # Redirect to desktop if user types m.wholefoodsmarket.com into a desktop browser.
    # ie. main domain
    if (req.http.X-UA-Device == "desktop") {
        if (req.http.host ~ "^m.wholefoodsmarket.com") {
            set req.http.newhost = regsub(req.http.host, "^(m\.)(.*)", if(req.is_ssl, "https://www.\2", "http://www.\2"));
            error 750 req.http.newhost;
        }
    }

    # Bypass all fastly caching for now.
    if(req.url){
      return(pass);
    }

    # Cache Only HEAD, GET OR PURGE requests.
    if (req.request != "HEAD" && req.request != "GET" && req.request != "PURGE") {
      return(pass);
    }


    return(lookup);
}

sub vcl_fetch {

#FASTLY fetch

  if ((beresp.status == 500 || beresp.status == 503) && req.restarts < 1 && (req.request == "GET" || req.request == "HEAD")) {
    restart;
  }

  if(req.restarts > 0 ) {
    set beresp.http.Fastly-Restarts = req.restarts;
  }

  if (beresp.http.Set-Cookie) {
    set req.http.Fastly-Cachetype = "SETCOOKIE";
    return (pass);
  }

  if (beresp.http.Cache-Control ~ "private") {
    set req.http.Fastly-Cachetype = "PRIVATE";
    return (pass);
  }

  if (beresp.status == 500 || beresp.status == 503) {
    set req.http.Fastly-Cachetype = "ERROR";
    set beresp.ttl = 1s;
    set beresp.grace = 5s;
    return (deliver);
  }


  if (beresp.http.Expires || beresp.http.Surrogate-Control ~ "max-age" || beresp.http.Cache-Control ~"(s-maxage|max-age)") {
    # keep the ttl here
  } else {
    # apply the default ttl
    set beresp.ttl = 0s;
  }

  return(deliver);
}

sub vcl_hit {
#FASTLY hit

  if (!obj.cacheable) {
    return(pass);
  }
  return(deliver);
}

sub vcl_miss {
#FASTLY miss
  return(fetch);
}

sub vcl_deliver {
#FASTLY deliver
  return(deliver);
}

# Backend down: Error page returned when all backend servers are down
sub vcl_error {
#FASTLY error


    # Handle our brilliant browser redirection code
    if (obj.status == 750) {
    set obj.http.Location = obj.response + req.url;
    set obj.status = 302;
    set obj.response = "Found";
    synthetic {""};
    return(deliver);
    }

  return(deliver);
}

sub vcl_pass {
#FASTLY pass
}

# based on https://github.com/varnish/varnish-devicedetect/blob/master/devicedetect.vcl
sub detect_device {
  unset req.http.X-UA-Device;
  unset req.http.X-UA-Vendor;

  set req.http.X-UA-Device = "desktop";
  set req.http.X-UA-Vendor = "generic";

  # Handle that a cookie or url param may override the detection altogether
  if (req.url ~ "[&|?]device_force=([^&\s]+)") {
    set req.http.X-UA-Device = regsub(req.url, ".*[&|?]device_force=([^&\s]+).*", "\1");
  } elsif (req.http.Cookie ~ "(?i)X-UA-Device-force") {
    # ;?? means zero or one ;, non-greedy to match the first
    set req.http.X-UA-Device = regsub(req.http.Cookie, "(?i).*X-UA-Device-force=([^;]+);??.*", "\1");
    # Clean up our mess in the cookie header
    set req.http.Cookie = regsuball(req.http.Cookie, "(^|; ) *X-UA-Device-force=[^;]+;? *", "\1");
    # If the cookie header is now empty, or just whitespace, unset it
    if (req.http.Cookie ~ "^ *$") { unset req.http.Cookie; } # "$ # stupid syntax highlighter
  } else {
    if (req.http.User-Agent ~ "(?i)(ads|google|bing|msn|yandex|baidu|ro|career|)bot" ||
      req.http.User-Agent ~ "(?i)(baidu|jike|symantec)spider" ||
      req.http.User-Agent ~ "(?i)scanner" ||
      req.http.User-Agent ~ "(?i)(web)crawler") {
      set req.http.X-UA-Device = "bot";
    } elsif (req.http.User-Agent ~ "(?i)ipad") {
      set req.http.X-UA-Device = "tablet";
      set req.http.X-UA-Vendor = "apple";
    } elsif (req.http.User-Agent ~ "(?i)ip(hone|od)") {
      set req.http.X-UA-Device = "smartphone";
      set req.http.X-UA-Vendor = "apple";
    # how do we differ between an android phone and an android tablet?
    #   http://stackoverflow.com/questions/5341637/how-do-detect-android-tablets-in-general-useragent
    #   http://googlewebmastercentral.blogspot.com/2011/03/mo-better-to-also-detect-mobile-user.html
    } elsif (req.http.User-Agent ~ "(?i)android.*(mobile|mini)") {
      set req.http.X-UA-Device = "smartphone";
      set req.http.X-UA-Vendor = "android";
    # android 3/honeycomb was just about tablet-only, and any phones will probably handle a bigger page layout
    } elsif (req.http.User-Agent ~ "(?i)android") {
      set req.http.X-UA-Device = "tablet";
      set req.http.X-UA-Vendor = "android";
    # see http://my.opera.com/community/openweb/idopera/
    } elsif (req.http.User-Agent ~ "Opera Mobi") {
      set req.http.X-UA-Device = "smartphone";
      set req.http.X-UA-Vendor = "android";
    } elsif (req.http.User-Agent ~ "PlayBook; U; RIM Tablet") {
      set req.http.X-UA-Device = "tablet";
      set req.http.X-UA-Vendor = "blackberry";
    } elsif (req.http.User-Agent ~ "hp-tablet.*TouchPad") {
      set req.http.X-UA-Device = "tablet";
      set req.http.X-UA-Vendor = "hp";
    } elsif (req.http.User-Agent ~ "Kindle/3") {
      set req.http.X-UA-Device = "tablet";
      set req.http.X-UA-Vendor = "kindle";
    } elsif (req.http.User-Agent ~ "Mobile.+Firefox") {
      set req.http.X-UA-Device = "mobile";
      set req.http.X-UA-Vendor = "firefoxos";
    } elsif (req.http.User-Agent ~ "^HTC") {
      set req.http.X-UA-Device = "smartphone";
      set req.http.X-UA-Vendor = "htc";
    } elsif (req.http.User-Agent ~ "Fennec") {
      set req.http.X-UA-Device = "smartphone";
      set req.http.X-UA-Vendor = "fennec";
    } elsif (req.http.User-Agent ~ "IEMobile") {
      set req.http.X-UA-Device = "smartphone";
      set req.http.X-UA-Vendor = "microsoft";
    } elsif (req.http.User-Agent ~ "BlackBerry" || req.http.User-Agent ~ "BB10.*Mobile") {
      set req.http.X-UA-Device = "smartphone";
      set req.http.X-UA-Vendor = "blackberry";
    } elsif (req.http.User-Agent ~ "GT-.*Build/GINGERBREAD") {
      set req.http.X-UA-Device = "smartphone";
      set req.http.X-UA-Vendor = "android";
    } elsif (req.http.User-Agent ~ "SymbianOS.*AppleWebKit") {
      set req.http.X-UA-Device = "smartphone";
      set req.http.X-UA-Vendor = "symbian";
    } elsif (req.http.User-Agent ~ "(?i)symbian" ||
      req.http.User-Agent ~ "(?i)^sonyericsson" ||
      req.http.User-Agent ~ "(?i)^nokia" ||
      req.http.User-Agent ~ "(?i)^samsung" ||
      req.http.User-Agent ~ "(?i)^lg" ||
      req.http.User-Agent ~ "(?i)bada" ||
      req.http.User-Agent ~ "(?i)blazer" ||
      req.http.User-Agent ~ "(?i)cellphone" ||
      req.http.User-Agent ~ "(?i)iemobile" ||
      req.http.User-Agent ~ "(?i)midp-2.0" ||
      req.http.User-Agent ~ "(?i)u990" ||
      req.http.User-Agent ~ "(?i)netfront" ||
      req.http.User-Agent ~ "(?i)opera mini" ||
      req.http.User-Agent ~ "(?i)palm" ||
      req.http.User-Agent ~ "(?i)nintendo wii" ||
      req.http.User-Agent ~ "(?i)playstation portable" ||
      req.http.User-Agent ~ "(?i)portalmmm" ||
      req.http.User-Agent ~ "(?i)proxinet" ||
      req.http.User-Agent ~ "(?i)sonyericsson" ||
      req.http.User-Agent ~ "(?i)symbian" ||
      req.http.User-Agent ~ "(?i)windows\ ?ce" ||
      req.http.User-Agent ~ "(?i)winwap" ||
      req.http.User-Agent ~ "(?i)eudoraweb" ||
      req.http.User-Agent ~ "(?i)htc" ||
      req.http.User-Agent ~ "(?i)240x320" ||
      req.http.User-Agent ~ "(?i)avantgo") {
      set req.http.X-UA-Device = "mobile";
    }
  }
}


sub normalize_req_url {
 # Strip out Google Analytics campaign variables. They are only needed
 # by the javascript running on the page
 # utm_source, utm_medium, utm_campaign, gclid, ...

 set req.url = querystring.filter(req.url, "gclid"
                   + querystring.filtersep() + "dclid"
                   + querystring.filtersep() + "cx"
                   + querystring.filtersep() + "ie"
                   + querystring.filtersep() + "cof"
                   + querystring.filtersep() + "siteurl"
                   + querystring.filtersep() + "gclsrc"
                   + querystring.filtersep() + "zanpid"
                   + querystring.filtersep() + "os_ehash"
                   + querystring.filtersep() + "origin"
                   + querystring.filtersep() + "hConversionEventId"
                   );
 set req.url = querystring.globfilter(req.url, "utm_*");
}
