#!/bin/sh
#
# Cloud Hook: post-code-update
#
# The post-code-update hook runs in response to code commits.
# When you push commits to a Git branch, the post-code-update
# hooks runs for each environment that is currently running that branch.
#
# The arguments for post-code-update are the same as for post-code-deploy,
# with the source-branch and deployed-tag arguments both set to the name
# of the environment receiving the new code.
#
# post-code-update only runs if your site is using a Git repository.
# It does not support SVN.
#
# See ../README.md for details.
#
# Usage: post-code-update site target-env source-branch deployed-tag repo-url
#                         repo-type

site="$1"
target_env="$2"
source_branch="$3"
deployed_tag="$4"
repo_url="$5"
repo_type="$6"


### Common steps

# Update sites and revert features.
echo "Running drush @$site.$target_env updb"
drush @$site.$target_env updb -y

# Clear drush cache to pick up new includes
echo "Running drush @$site.$target_env cc drush"
drush @$site.$target_env cc drush

echo "Running drush @$site.$target_env features-revert-all"
drush @$site.$target_env features-revert-all -y

case $target_env in
    "prod" )
        # Ensure new hooks and template files are picked up
        echo "Running drush @$site.$target_env cc registry"
        drush @$site.$target_env cc registry

        echo "Running drush @$site.$target_env cc theme-registry"
        drush @$site.$target_env cc theme-registry

        echo "Running drush @$site.$target_env cc css-js"
        drush @$site.$target_env cc css-js

        echo "Running drush @$site.$target_env advagg-clear-all-files"
        drush @$site.$target_env advagg-clear-all-files
        ;;
    * )
        # Ensure new hooks and template files are picked up
        echo "Running drush @$site.$target_env cc registry"
        drush @$site.$target_env cc registry

        echo "Running drush @$site.$target_env cc theme-registry"
        drush @$site.$target_env cc theme-registry

        echo "Running drush @$site.$target_env cc css-js"
        drush @$site.$target_env cc css-js

        echo "Running drush @$site.$target_env advagg-clear-all-files"
        drush @$site.$target_env advagg-clear-all-files

        # Enable and disable non-prod modules
        echo "Running drush @$site.$target_env en stage_file_proxy"
        drush @$site.$target_env en stage_file_proxy -y

        echo "Running drush @$site.$target_env dis janrain_capture_screens"
        drush @$site.$target_env dis janrain_capture_screens -y
        ;;
esac

# Clear views cache to force views to revert to code.
echo "Running drush @$site.$target_env cc views"
drush @$site.$target_env cc views