#!/bin/sh
#
# Cloud Hook: post-db-copy
#
# The post-db-copy hook is run whenever you use the Workflow page to copy a
# database from one environment to another. See ../README.md for
# details.
#
# Usage: post-db-copy site target-env db-name source-env

site="$1"
target_env="$2"
db_name="$3"
source_env="$4"


### Common steps

# Update sites and revert features.
echo "Running drush @$site.$target_env updb"
drush @$site.$target_env updb -y

# Clear drush cache to pick up new includes
echo "Running drush @$site.$target_env cc drush"
drush @$site.$target_env cc drush

echo "Running drush @$site.$target_env features-revert-all"
drush @$site.$target_env features-revert-all -y

case $target_env in
    "prod" )
        # Ensure new hooks and template files are picked up
        echo "Running drush @$site.$target_env cc registry"
        drush @$site.$target_env cc registry

        echo "Running drush @$site.$target_env cc theme-registry"
        drush @$site.$target_env cc theme-registry

        echo "Running drush @$site.$target_env cc css-js"
        drush @$site.$target_env cc css-js

        echo "Running drush @$site.$target_env advagg-clear-all-files"
        drush @$site.$target_env advagg-clear-all-files
        ;;
    * )
        # Ensure new hooks and template files are picked up
        echo "Running drush @$site.$target_env cc registry"
        drush @$site.$target_env cc registry

        echo "Running drush @$site.$target_env cc theme-registry"
        drush @$site.$target_env cc theme-registry

        echo "Running drush @$site.$target_env cc css-js"
        drush @$site.$target_env cc css-js

        echo "Running drush @$site.$target_env advagg-clear-all-files"
        drush @$site.$target_env advagg-clear-all-files

        # Enable and disable non-prod modules
        echo "Running drush @$site.$target_env en stage_file_proxy"
        drush @$site.$target_env en stage_file_proxy -y

        echo "Running drush @$site.$target_env dis janrain_capture_screens"
        drush @$site.$target_env dis janrain_capture_screens -y
        ;;
esac

# Clear views cache to force views to revert to code.
echo "Running drush @$site.$target_env cc views"
drush @$site.$target_env cc views