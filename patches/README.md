# Patches on Drupal Core #
--------------------------

## JS ##

	d7core-js-drupal-log-1232416-100-D7.patch

Suppresses AJAX error messaging in instances where ajax is interrupted during a page load.
run from wfmcom
```patch -p1 < patches/d7core_js-drupal-log-1232416-100-D7.patch```

This patch was made for older version of Core but it can still work with newer ones (tested on 7.39).
For all files assume reversed - no, apply anyway - yes.
```Reversed (or previously applied) patch detected!  Assume -R? [n]
Apply anyway? [n] y```
Remove *.rej files afterwards.

## Secure Pages ##

    d7core_securepages_471970-100-fix-simpletest-https-d7.patch
    **NOTE this one doesn't appear necessary any more ** d7core_securepages_961508-232.patch

These patches are required to make the secure pages module work but make changes to the core so are listed here for tracking purposes.  Before applying, please check https://drupal.org/project/securepages to confirm that patches remain relevant.
run from wfmcom
patch -p1 <patches/d7core_securepages_471970-100-fix-simpletest-https-d7.patch

## Overlay ##

  d7core_1116326-overlay-method-get-forms-56-D7.patch

When defining a page view in the administration area using exposed forms, the overlay closes each time you submit the form.
https://www.drupal.org/node/1116326

# Patches on Contrib Modules #
--------------------------

Some community contributed modules have been patched to fix specific bugs or to address specific issues faced by Whole Foods Market. This file should contain a record of all patches on contrib modules, an explanation of why and how the module was patched, and any additional instructions that may help a future developer or developers who will be upgrading these contrib modules.

## CDN ##

	cdn-css_hash_in_aggregate-1514182-25.patch

In cases where files are replaced by an asset of the same name (e.g. social_icon.png), the CDN module is iterated with a unix timestamp as a query string in the aggregated CSS file.  However, this query is prepended by a '#' instead of a '?' and the asset will not load.

## Fastly ##
    patches/fastly_vary_header.patch
    
This patch modifies the call to drupal_add_http_header('Vary', 'Cookie,fastly-ssl', TRUE); to add the TRUE parameter.  This ensures that Drupal does not overwrite the value of the vary header, but rather appends to it.
  Apply it by navigating to git root (eg. ~/Sites/wfmcom) and using the following command:
  $ patch -p1 < patches/fastly_vary_header.patch

## Features ##

    patches/features-mtime-1x-2381739-14.patch

Remove modification timestamp from exported features (added in Drupal 7.33)

## Feeds ##

	FeedsJSONPathParser-invalidcharacter-1824506-5.patch

Strips unicode extended characters from JSON data. These characters does not fit into MySQL's "UTF-8" charset and lead to DB errors when Feeds tries to insert them into database

## Field Collection ##

	field_collection_empty_lists.diff

Field Collections save a new field collection when it appears there is new data in that field collection. Anytime a boolean or a list is present, there is always non empty data to be saved resulting in a new field collection being saved on every node save that contains the collection.

We added a patch to consider bools and lists empty if the option 'This field is always considered empty when saving' is selected on those fields.

    field-collection-drupal-7.27-2242751-1.patch

Modules which use custom Ajax form page callbacks require updates for Drupal 7.27 -- https://www.drupal.org/node/2242663. This affects field collection widgets when they are displayed to anonymous users on cached pages (possibly only in the middle of a multi-step form, although not positive).

## Google Analytics ##

	google_analytics-search_fix-1.3.patch

This patch adds the ability to define specific search pages for GA tracking from the admin UI.

## Janrain Capture ##

	janrain_capture-entire_site_ssl.patch

This patch replaces a css file import from a standard http protocol only host with the contents of the file instead, since the import fails over secure https. This will remove a mixed content error if you load a page securely.

  janrain_capture_async_js_2696959_1.patch
  
This patch add async attribute for external script janrain_capture.js.

## Media ##

	media-fix_insertion_of_non_image_content-1283844-13.patch

Per https://drupal.org/node/1283844, this patch provides basic handling for non-image (e.g. pdf) content through the WYSIWYG module.  This is patched against the development branch for 2.x which now has an alpha release and should be re-evaluated when updating.

## Node Export ##

	node_export-field_collection-1670740-16.patch

This provides handling for exporting field collection data.  I'm not aware of anyplace that we use Node Export on the site though the module is enabled in production so I'm disinclined to remove this without investigation.

## Optimizely ##

  optimizely_add_async_js_load.patch
  
This patch add async attribute for external script of the optimizely module.

## Print ##

  print-disable_update_to_database_on_print.patch

The print module makes a database update to track visits to the print pages. The path row in the table that holds this data has a max size of 128 characters. This patch removed the query that updates this table for two reasons:

1. Printing our coupons can create a path longer than 128 characters
2. We don't need to be making database updates with every visit to the print pages. We have Google Analytics for tracking.

## Search API ##

## Search API Autocomplete ##
This patch stops the autocomplete from displaying an Ajax 500 error.  See QD-3838 for details.
  Apply it by navigating to git root (eg. ~/Sites/wfmcom) and using the following command:
  $ patch -p1 < patches/search_api_autocomplete_ajax500-2770229-001.patch


## Search API Pages ##

	search_api_pages-cache_page_forms_1664706.patch

This deals with a performance issue encountered when tuning the site. search_api_page_forms() returns the exact same array regardless of input parameters, so there is no reason to re-query the search_api_page table every time the form is built. On locations pages, we run this method over 300 times. We have written a patch to cache the output of this form so every subsequent run after the first will return faster. This issue has been reported to the maintainer of the module and is documented at http://drupal.org/node/1664706. That patch is in the patches directory:

	search_api_pages-form_redirect_does_not_work_with_get_destination.patch

## Search API Solr ##

	search_api_solr-compatibility-with-search_api_location.patch

The module 'Search API Location' requires that a small patch be added to Search API Solr in order for the later module to be able to run location based Solr queries. This is a commonly accepted patch that is distributed with the Search API Location module and is located in the Search AP Location directory.

Future updates to the Search API Location module should contain an updated patch file that will need to be applied to the Search API Solr module.

    search_api_solr-schema.xml.patch

This patch represents our deviation from the default schema.xml which currently removes SnowballPorter in favor of Edge N-Gram indexing to accomodate fuzzy search, spell checking, porter-stemmer, etc.  This patch can be applied for tracking purposes but has no impact on our code base.  Rather, schema.xml is used by our Solr instance at Acquia Search and is loaded directly on their server(s).  Future changes will need to be filed as support requests with them.

  search_api_solr-autocomplete-2370433-13.patch
  
Per https://www.drupal.org/node/2370433 It looks like there is a slightly new response.

## Views ##

	add-group_concat-aggregate-function-1362524-42.patch

Adds a GROUP_CONCAT available as an aggregate function.

	views-1249684-dependency-exposed-filter.patch

Exposed filter operator "is empty" causes exposed filter operator choices to disappear

## Views Infinite Scroll ##

    views_infinite_scroll-patch.patch

This patch is NOT for the views module, but for the Views Infinite Scroll module.  It is in production although CIA folks believe there are no views with infinite scrolling.

## Node Connect ##

	nodeconnect_miss_edit_button.patch

Change javascript function "attr" to "prop" that don't work in current jQuery version.

## Media: YouTube ##

	media_youtube-work-with-https-fix.patch

Change javascript function "insertEmbed", remove protocol for www.youtube.com.
