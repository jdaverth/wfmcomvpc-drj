<?php
// Deployment Job

// Timeout length for an acquia task to be
// considered 'hung'
define('ACQUIA_TASK_TIMEOUT', 120);

$steps = array(
  'Code Deployment' => 'code_deployment',
  'Database Update' => 'drush_updb',
  'Revert Features' => 'drush_fra',
  'Clear Drupal Cache' => 'drush_cc_all',
  'Clear Varnish' => 'varnish_flush',
);

/*
 * Helper Functions
 */

function acquia_api_curl($path, $user, $pass, $method = 'GET', $fields = null) {
  $ch = curl_init();
  $url = 'https://cloudapi.acquia.com/' . $path;
  curl_setopt($ch, CURLOPT_URL, $url);
  // Tell curl to pass results back
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  // Set time to 30 seconds
  curl_setopt($ch, CURLOPT_TIMEOUT, 30);
  // Set user and pass
  curl_setopt($ch, CURLOPT_USERPWD, $user . ':' . $pass);

  switch ($method) {
    case 'GET':
      break;

    case 'POST':
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
      break;

    case 'DELETE':
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
      break;
  }

  $data = curl_exec($ch);
  
  if(!$data) {
    print_date("*** Acquia API Curl request failed - $path");
    print_date("debug:\n\r");
    print_date(curl_errno($ch));
    print_date(curl_error($ch));
    curl_close($ch);
   return FALSE;
  }
  else {
    curl_close($ch);
    $data = json_decode($data);
    return $data;
  }
}

function acquia_task_loop($task_id) {
  $complete = false;
  // Counter used to try to catch hung tasks
  $counter = 0;
  // Check for task completion
  while (!$complete && $counter < ACQUIA_TASK_TIMEOUT) {
    $task_status = acquia_api_curl("v1/sites/" . $_SERVER["AH_SITE"] . "/tasks/$task_id.json", $_SERVER["AH_API_USER"], $_SERVER["AH_API_PASS"]);
    if ($task_status) {
      $complete = (isset($task_status->completed) ? true : false);
      $counter++;
      print '.';
      sleep(1);
    }
  }
  if (!$complete) {
    print_date("*** The task $task_id appears to be hung.");
    return false;
  }
  return true;
}

function print_date($message) {
  print("\r\n" . time() . ' - ');
  print($message);
}

function acquia_ssh($command) {
  static $ssh_login;

  // If we don't know user and host, get from Acquia.
  if (!isset($ssh_login)) {
    $site_info = acquia_api_curl("v1/sites/" . $_SERVER["AH_SITE"] . ".json", $_SERVER["AH_API_USER"], $_SERVER["AH_API_PASS"]);
    $server_info = acquia_api_curl("v1/sites/" . $_SERVER["AH_SITE"] . "/envs/" . $_SERVER["AH_ENV"] . ".json", $_SERVER["AH_API_USER"], $_SERVER["AH_API_PASS"]);
    if($site_info && $server_info) { 
      $ssh_login = $site_info->unix_username . '@' . $server_info->ssh_host;
      print_date('SSH connection discovered as: ' . $ssh_login);
    }
    else {
      print_date('*** Failed to discover SSH connection information.');
      return FALSE;
    }
  }

  $ssh_command = "ssh -o StrictHostKeyChecking=no -o PasswordAuthentication=no -C $ssh_login \"$command\"";
  $ssh_output = '';
  exec($ssh_command, $ssh_output, $ssh_success);

  if ($ssh_success == 0) {
    return TRUE;
  }
  elseif ($ssh_success == 255) {
    print_date("An error occured while connecting to the remote server via SSH");
    return FALSE;
  }
  else {
    print_date("Remote command '$command' did not return successfully");
    print_date("  returned: $ssh_output");
    return FALSE;
  }
}


/*
 * Step Callbacks
 */

// Deploy Code
function code_deployment() {
  if (!isset($_SERVER["AH_GIT_TAG"])) {
    print_date("No tag to deploy. Skipping this step.");
    return true;
  }

  $fields = array(
    'path' => $_SERVER["AH_GIT_TAG"],
  );
  $code_deploy = acquia_api_curl("v1/sites/" . $_SERVER["AH_SITE"] . "/envs/" . $_SERVER["AH_ENV"] . "/code-deploy.json", $_SERVER["AH_API_USER"], $_SERVER["AH_API_PASS"], 'POST', $fields);

  // Evaluate deployment request
  if ($code_deploy && ($code_deploy->state == 'waiting' || $code_deploy->state == 'received')) {
    print_date("Code deploying.");
    $acquia_task_id = $code_deploy->id;
    sleep(1);
    $complete = acquia_task_loop($acquia_task_id);
    if ($complete) {
      print_date("Tag '" . $_SERVER["AH_GIT_TAG"] . "' deployed to " . $_SERVER["AH_SITE"] . "." . $_SERVER["AH_ENV"] . ".");
    }
    else {
      print_date("*** There was an error deploying the code.");
      return false;
    }
  }
  else {
    print_date("*** There was an error deploying code.");
    return false;
  }

  return true;
}


function drush_updb() {
  return acquia_ssh("/usr/bin/env drush @" . $_SERVER["AH_SITE"] . "." . $_SERVER["AH_ENV"] . " updb -y");
}

function drush_fra() {
  return acquia_ssh("/usr/bin/env drush @" . $_SERVER["AH_SITE"] . "." . $_SERVER["AH_ENV"] . " fr-all -y");
}

function drush_cc_all() {
  return acquia_ssh("/usr/bin/env drush @" . $_SERVER["AH_SITE"] . "." . $_SERVER["AH_ENV"] . " cc all -y");
}

function varnish_flush() {
  $success = true; 
  $domains = acquia_api_curl("v1/sites/" . $_SERVER["AH_SITE"] . "/envs/" . $_SERVER["AH_ENV"] . "/domains.json", $_SERVER["AH_API_USER"], $_SERVER["AH_API_PASS"]);

  if (!$domains) {
    print_date("*** Failed to find domains listed on " . $_SERVER["AH_ENV"]);
    $success = false;
  }

  foreach ($domains as $domain) {
    $domain = $domain->name;
    print_date("Clearing Varnish cache for $domain.");
    $varnish = acquia_api_curl("v1/sites/" . $_SERVER["AH_SITE"] . "/envs/" . $_SERVER["AH_ENV"] . "/domains/$domain/cache.json", $_SERVER["AH_API_USER"], $_SERVER["AH_API_PASS"], 'DELETE');
    if ($varnish) {
      $acquia_task_id = $varnish->id;
      if (acquia_task_loop($acquia_task_id)) {
        print_date($domain . " Varnish cache cleared.");
      }
      else {
        $success = false;
        print_date("*** Failed to clear Varnish cache for $domain.");
      }
    }
    else {
      print_date("*** Failed to clear Varnish cache for $domain.");
      $success = false;
    }
  }

  if ($success) {
    return true;
  }
  else {
    return false;
  }
}

/*
 * Main Loop
 */
if (!isset($_SERVER["AH_ENV"])) {
  print_date("*** No environment defined. Aborting.");
  exit(1);
}

print_date("Starting auto deployment with values: ");
print_date(" - Site: " . $_SERVER["AH_SITE"]);
print_date(" - Environment: " . $_SERVER["AH_ENV"]);
if (!isset($_SERVER["AH_GIT_TAG"])) {
  print_date(" - Git Tag: *NONE*");
}
else {
  print_date(" - Git Tag: " . $_SERVER["AH_GIT_TAG"]);
}

foreach ($steps as $name => $step) {
  print_date('Starting ' . $name . '.');
  if(!call_user_func($step)) {
    print_date($name . ' failed!');
    // we should print remaining steps
    exit(1);
  }
  print_date($name . ' completed.');
}
print_date("Complete.\n\r");
// Report we finished with no errors
exit(0);
