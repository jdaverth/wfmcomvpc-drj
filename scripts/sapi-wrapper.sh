#!/usr/bin/env bash
export PATH="$PATH:/usr/local/bin"
export PHPRC="/var/www/site-php/${1}/php.ini"
LOG=/mnt/tmp/${1}/sapi-s-$(date +%F).log

if [ -n "${2}" ]; then
URI=${2}
else
URI=${1}.prod.acquia-sites.com
fi

echo -e "*\n**\n***\nsapi-s Started: $(date)\n***\n**\n*" >> ${LOG}
drush --root=/var/www/html/${1}/docroot/ --uri=${URI} -vd sapi-s >> ${LOG} 2>&1
echo -e "*\n**\n***\nsapi-s Completed: $(date)\n***\n**\n*\n" >> ${LOG}
