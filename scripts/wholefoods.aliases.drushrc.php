<?php
// Site wholefoods, environment dev
$aliases['dev'] = array(
  'parent' => '@parent',
  'site' => 'wholefoods',
  'env' => 'dev',
  'root' => '/var/www/html/wholefoods.dev/docroot',
  'remote-host' => 'staging-2055.prod.hosting.acquia.com',
  'remote-user' => 'wholefoods',
);
// Site wholefoods, environment dev2
$aliases['dev2'] = array(
  'parent' => '@parent',
  'site' => 'wholefoods',
  'env' => 'dev2',
  'root' => '/var/www/html/wholefoods.dev2/docroot',
  'remote-host' => 'staging-2055.prod.hosting.acquia.com',
  'remote-user' => 'wholefoods',
);
// Site wholefoods, environment dev3
$aliases['dev3'] = array(
  'parent' => '@parent',
  'site' => 'wholefoods',
  'env' => 'dev3',
  'root' => '/var/www/html/wholefoods.dev3/docroot',
  'remote-host' => 'staging-2055.prod.hosting.acquia.com',
  'remote-user' => 'wholefoods',
);
// Site wholefoods, environment test
$aliases['test'] = array(
  'parent' => '@parent',
  'site' => 'wholefoods',
  'env' => 'test',
  'root' => '/var/www/html/wholefoods.test/docroot',
  'remote-host' => 'staging-2055.prod.hosting.acquia.com',
  'remote-user' => 'wholefoods',
);
// Site wholefoods, environment test2
$aliases['test2'] = array(
  'parent' => '@parent',
  'site' => 'wholefoods',
  'env' => 'test2',
  'root' => '/var/www/html/wholefoods.test2/docroot',
  'remote-host' => 'staging-2055.prod.hosting.acquia.com',
  'remote-user' => 'wholefoods',
);
// Site wholefoods, environment prod
$aliases['prod'] = array(
  'parent' => '@parent',
  'site' => 'wholefoods',
  'env' => 'prod',
  'root' => '/var/www/html/wholefoods.prod/docroot',
  'remote-host' => 'web-1667.prod.hosting.acquia.com',
  'remote-user' => 'wholefoods',
);
// Site wholefoods, environment mdev
$aliases['mdev'] = array(
  'parent' => '@parent',
  'site' => 'wholefoods',
  'env' => 'mdev',
  'root' => '/var/www/html/wholefoods.mdev/docroot',
  'remote-host' => 'staging-2055.prod.hosting.acquia.com',
  'remote-user' => 'wholefoods',
);
