<?php
/**
 * Created by PhpStorm.
 * User: paul_cooper
 * Date: 8/25/2015
 * Time: 7:49 AM
 */

use Behat\Behat\Context\BehatContext;
use Drupal\DrupalExtension\Context\DrupalSubContextInterface;

define('DEBUG',0);

/*-----------------------------------------------------------------------------
 * Subcontext for Desktop Brand Camp steps, utilities, and constants
 -----------------------------------------------------------------------------*/
class BrandCampDesktopSubContext extends BehatContext implements DrupalSubContextInterface
{

  public function __construct(array $parameters = null) {/*do subcontext initialization*/}

  public static function getAlias() {return 'BrandCampDesktop';}

  #----------------------------------------------------------------------------
  # Shell functions to access functions in the FeatureContext space
  #----------------------------------------------------------------------------
  public function getSession(){return parent::getMainContext()->getSession();}

  public function shouldBeVisible($css_selector){$this->getMainContext()->shouldBeVisible($css_selector);}

  public function iWaitForAjaxCallsToFinish($timeout=5000){$this->getMainContext()->iWaitForAjaxCallsToFinish();}

  public function setSelector($el_selector, $value = null, $container_selector = null)
  {
    return $this->getMainContext()->setSelector($el_selector, $value, $container_selector);
  }

  public function resolveSelector($el_selector, $container_selector = null, $multiple = FALSE)
  {
    return $this->getMainContext()->resolveSelector($el_selector, $container_selector, $multiple);
  }

  private function putScreenshot( $short_msg="", $path=null)
  {
    return $this->getMainContext()->putScreenshot( $short_msg, $path);
  }

  #####################################################################################################################
  #         S T E P   F U N C T I O N S  F O R   B E H A T
  #####################################################################################################################
  const BC_FIND_A_STORE = "nav a[href='/stores/list']";
  const BC_ON_SALE = "nav a[href='/sales-flyer']";
  const BC_RECIPIES = "nav a[href='/recipes']";
  const BC_SHOP_ONLINE = "nav a[href='/online-ordering']";
  const BC_SIGN_IN = "nav a[href='/signin']";
  /**
   * @Then /^I verify upper menu bar$/
   */
  public function iVerifyUpperMenuBar() {
    $this->resolveSelector(self::BC_FIND_A_STORE);
    $this->resolveSelector(self::BC_ON_SALE);
    $this->resolveSelector(self::BC_RECIPIES);
    $this->resolveSelector(self::BC_SHOP_ONLINE);
    $this->resolveSelector(self::BC_SIGN_IN);
  }

  /**
   * @When /^I set the Brand Camp SHOW ME filter to "([^"]*)"$/
   * REQUIREMENTS: The current url is the homepage.
   * @param $filter one of (FOOD, PEOPLE, PLANET or ALL)
   * @throws Exception Invalid show me filter
   */
  public function iSetTheBrandCampShowMeFilterTo($filter) {
    #Activate the filter via setSelector and wait for things to settle down
    $this->setBrandCampShowMe($filter);
  }

  /**
   * @Then /^Verify Brand Camp SHOW ME "([^"]*)" is active$/
   * REQUIREMENTS: The current url is the homepage.
   * @param $filter one of (FOOD, PEOPLE, PLANET or ALL)
   * @throws Exception Filter fail
   */
  public function verifyBrandCampShowMeIsActive($filter) {
    $active_filters = $this->getActiveBrandCampFilters();
    try{
      $active_filter = $active_filters["SHOW ME"];
      if ($active_filter == $filter) return;
      throw new Exception("Filter fail: Filter '$filter' is not active. \n" .
                          ".............Filter '$active_filter' is active instead");
    }
    catch (Exception $e){
      throw new Exception("Filter fail: Filter '$filter' is not present on the page");
    }
  }

  /**
   * @Then /^Verify no other Show Me tile count exceeds "([^"]*)" tile count$/
   * REQUIREMENTS: The current url is the homepage.
   * @param $filter One of the following filters "FOOD", "PEOPLE", or "PLANET"
   * @return bool TRUE if no fails, otherwise exception is thrown
   * @throws Exception Show Me filter fail.
   */
  public function verifyNoOtherShowMeTileCountExceedsTileCount($filter) {
    #Count the various Show Me tiles
    $show_me_counts = $this->getTileTypeCounts();

    #If the filtered count is zero throw an exception
    if ( $this->isShowMeFilter($filter) and $show_me_counts[$filter] < 1){
      throw new Exception("Show Me '$filter' fail: '$filter' tile count should be greater than zero\n" .
        "FOOD   = " .$show_me_counts["FOOD"].   "\n" .
        "PEOPLE = " .$show_me_counts["PEOPLE"]. "\n" .
        "PLANET = " .$show_me_counts["PLANET"]       );
    }

    #Compare filtered counts in order to determine if filtering works
    switch($filter){
      case "FOOD":
        if($show_me_counts["FOOD"] >= $show_me_counts["PEOPLE"] and
           $show_me_counts["FOOD"] >= $show_me_counts["PLANET"]  ) return TRUE;
        break;

      case "PEOPLE":
        if($show_me_counts["PEOPLE"] >= $show_me_counts["FOOD"] and
           $show_me_counts["PEOPLE"] >= $show_me_counts["PLANET"]  ) return TRUE;
        break;

      case "PLANET":
        if($show_me_counts["PLANET"] >= $show_me_counts["FOOD"] and
           $show_me_counts["PLANET"] >= $show_me_counts["PEOPLE"]  ) return TRUE;
        break;

      default:
        #The given filter is not handled by this function, throw an exception
        throw new Exception("Show Me '$filter' fail: The filter '$filter' is not testable by Step Function verifyBrandCampShowMe" );
    }
    #A non filtered tile count was higher that the filtered count, throw an exception.
    throw new Exception("Show Me '$filter' fail: No other Show Me tile count should be greater than '$filter'\n" .
      "FOOD   = " .$show_me_counts["FOOD"].   "\n" .
      "PEOPLE = " .$show_me_counts["PEOPLE"]. "\n" .
      "PLANET = " .$show_me_counts["PLANET"]       );
  }


  /**
   * @Then /^Verify Brand Camp I WANT TO "([^"]*)" differs from "([^"]*)"$/
   * REQUIREMENTS: The current url is the homepage.
   * @param $filter1 one of (LEARN, DO, or BOTH)
   * @param $filter2 one of (LEARN, DO, or BOTH) but not the same as filter1
   * @throws Exception Filter fail
   */
  public function verifyBrandCampIWantTo($filter1, $filter2) {
    #Make sure at least one Brand Camp tile has loaded
    $this->resolveSelector(self::BRAND_CAMP_VISIBLE_TILES, null, TRUE);

    #Set filter to filter1 and store its tile hrefs
    $this->setBrandCampIWantTo($filter1);
    #Get the header hrefs from the "filter1" filtered  tiles
    for( $i=0; $i<12; $i++){
      $tile_href = "[weight='$i'] header a[href]";
      $href_f1 = $this->resolveSelector($tile_href);
      $hrefs_f1[] = $href_f1->getAttribute("href");
    }


    #Set filter to filter2 and store its tile hrefs
    $this->setBrandCampIWantTo($filter2);
    #Get the header hrefs from the "filter2" filtered  tiles
    for( $i=0; $i<12; $i++){
      $tile_href_selector = "[weight='$i'] header a[href]";
      $href_f2 = $this->resolveSelector($tile_href);
      $hrefs_f2[] = $href_f2->getAttribute("href");
    }


    #Count the differences between the filtered tile presentations
    $diff_f1_f2 = 0;
    for ($i=0; $i<count($hrefs_f1); $i++) {
      if ($hrefs_f1[$i] != $hrefs_f2[$i]) $diff_f1_f2++;
    }

    #Compare the count differences to determine if filtering is working
    if ($diff_f1_f2 == 0) throw new Exception("Filter fail: I WANT TO  Filters '$filter1' " .
      "and '$filter2' produce identical results. ");
  }

  /**
   * @Then /^Verify Brand Camp I WANT TO "([^"]*)" is active$/
   * REQUIREMENTS: The current url is the homepage.
   * @param $filter one of (LEARN, DO, or BOTH)
   * @throws Exception Filter fail
   */
  public function verifyBrandCampIWantToIsActive($filter) {
    try{
      $active_filters = $this->getActiveBrandCampFilters();
      $active_IWT = $active_filters["I WANT TO"];
    }
    catch (Exception $e){
      throw new Exception("Filter fail: Filter '$filter' is not present on the page");
    }

    if ($active_IWT == $filter) return;
    throw new Exception("Filter fail: Filter '$filter' is not active. \n" .
      "Filter '" .$active_filters["I WANT TO"]. "' is active instead");
  }


  /**
   * @Then /^Verify Brand Camp VIEW "([^"]*)" filter$/
   * REQUIREMENTS: The current url is the homepage or it is the ../values-matter/your-stories page.
   * @param $filter one of (OUR STORIES, or YOUR STORIES)
   * @throws Exception Filter fail
   */
  public function verifyBrandCampView($filter) {
    #Make sure at least one Brand Camp tile has loaded
    $this->resolveSelector(self::BRAND_CAMP_VISIBLE_TILES, null, TRUE);

    #Set filter to filter1 and store the tile hrefs
    $this->setBrandCampView($filter);

    if ($filter == "YOUR STORIES") {
      $this->getMainContext()->assertPageAddress(self::BRAND_CAMP_YOUR_STORIES_PAGE);
      $active_filters = $this->getActiveBrandCampFilters();
      if ($active_filters["VIEW"] != $filter) {
        throw new Exception('Filter fail: Expected VIEW filter "YOUR STORIES" is not active');
      }
    }
    else {
      $this->getMainContext()->assertHomePage();
      $active_filters =  $this->getActiveBrandCampFilters();
      if ($active_filters["VIEW"]      != "OUR STORIES" and
          $active_filters["SHOW ME"]   != "ALL"         and
          $active_filters["I WANT TO"] != "BOTH"           ){
        throw new Exception("Filter fail: Expected       Actual \n" .
                             "  SHOW ME    'ALL'         '" .  $active_filters["SHOW ME"]   . "'\n" .
                             "I WANT TO   'BOTH'         '" .  $active_filters["I WANT TO"] . "'\n" .
                             "     VIEW  'OUR STORIES'   '" .  $active_filters["VIEW"]              );
      }
    }
    $this->getActiveBrandCampFilters();
  }


  /**
   * @Then /^Verify Brand Camp tile count is "([^"]*)"$/
   * REQUIREMENTS: The current url is the Brand Camp homepage.
   * @param $exp_count Expected number of loaded Brand Camp tiles
   * @throws Exception Tile count fail
   */
  public function verifyBrandCampTileCountIs($exp_count) {
    $act_count = $this->getTileCount();
    if ($act_count != $exp_count) {
      throw new Exception("Tile count fail: Expected=$exp_count  Actual=$act_count");
    }
  }

  /**
   * @When /^Load More is Pressed "([^"]*)" time\(s\)$/
   * REQUIREMENTS: The current url is the Brand Camp homepage.
   * @param $count The number of times to press LOAD MORE
   */
  public function loadMoreIsPressedTimeS($count) {
    for ($i=0; $i<$count; $i++){
      try {
        $this->setSelector(self::BRAND_CAMP_LOAD_MORE);
        $this->iWaitForAjaxCallsToFinish();
      } catch (Exception $e) {throw new Exception ("LOAD MORE Fail: Could not load iteration $i." );}
    }
  }

  /**
   * @When /^LOAD MORE Brand Camp is exhausted$/
   * REQUIREMENTS: The current url is the Brand Camp homepage.
   * Keep loading tiles until there are no more left.
   */
  public function loadMoreBrandCampUntilExhausted()
  {
    $load_more = $this->resolveSelector(self::BRAND_CAMP_LOAD_MORE);
    while( $load_more->isVisible()) $load_more->press();
  }

  const BC_SCROLL_TOP = "#scrollTop";

  /**
   * @When /^Scroll Top is pressed$/
   * REQUIREMENTS: The current url is the Brand Camp homepage and the Scroll to Top button is visible.
   */
  public function scrollTopIsPressed() {
    #Use setSelector to act upon the element
    $this->setSelector(self::BC_SCROLL_TOP);
  }

  /**
   * @When /^Verify Brand Camp Special Tiles are in place$/
   * Verify the Special tiles are at positions 2 and 6 with in each group of 12.
   * Also verify elements of the loaded Special tiles
   * REQUIREMENTS: The current url is the Brand Camp homepage and that at least 48 tiles have been loaded to ensure
   * all eight Special tiles are present.
   * @throws Exception Tile position fail and others
   */
  public function verifyBrandCampSpecialTilesAreInPlace()
  {
    #First confirm there is at least one Special Tile
    try{
      $this->resolveSelector(self::BAND_CAMP_SPECIAL_TILE_TYPE, null, true );
    }
    catch (Exception $e){
      throw new exception("Fail verifyBrandCampSpecialTilesAreInPlace: No Special Tiles found");
    }

    #Anticipated exceptions are stored so that tile testing can continue
    #Iterate through the tiles and verify the special ones
    $exceptions = [];
    $tiles = $this->getBrandCampTiles();
    foreach( $tiles as $tile ){
      $tile_errors = null;
      #Only Special tiles are verified
      if ($this->isTileType($tile, self::BAND_CAMP_SPECIAL_TILE_TYPE)){
        #Give the special tile focus and verify its elements
        $tile->focus();
        $tile_errors = $this->specialTileVerification($tile);
        }
      #Capture tile verification error, if any
      if ($tile_errors) $exceptions[] = $tile_errors;
    } #foreach

    #Report exceptions if any
    if ($exceptions) throw new Exception( implode("\n", $exceptions) );
  }

  /**
   * @When /^Verify Brand Camp UGC Tiles are in place$/
   * REQUIREMENTS: The current url is the Brand Camp homepage.
   * Verify UGC tiles are at positions 1 and 8 within each group of 12 tiles.
   * Also verify elements of each loaded UGC tile
   * REQUIREMENTS: The current url is the Brand Camp homepage.
   */
  public function verifyBrandCampUGCTilesAreInPlace()
  {
    #First confirm there is at least one Special Tile
    try{
      $this->resolveSelector(self::BAND_CAMP_UGC_TILE_TYPE, null, true );
    }
    catch (Exception $e){
      throw new exception("Fail verifyBrandCampUGCTilesAreInPlace: No UGC Tiles found");
    }

    #Anticipated exceptions are stored so that tile testing can continue
    $exceptions = [];
    $tiles = $this->getBrandCampTiles();
    foreach( $tiles as $tile ){
      $tile_errors = null;

      #Verify only UGC tiles
      if ($this->isTileType($tile, self::BAND_CAMP_UGC_TILE_TYPE)){
        $tile->focus();
        $tile_errors = $this->ugcTileVerification($tile);
      }
      #Capture tile verification error, if any
      if ($tile_errors) {
        $exceptions[] = $tile_errors;
      }
    } #foreach

    #Report any exceptions
    if (count($exceptions) != 0 ) throw new Exception( implode("\n", $exceptions) );
  }

  /**
   * @When /^Verify Brand Camp Article Tiles are in place$/
   * REQUIREMENTS: The current url is the Brand Camp homepage.
   */
  public function verifyBrandCampArticleTilesAreInPlace()
  {
    #First confirm there is at least one Article Tile
    try {$this->resolveSelector(self::BAND_CAMP_IMAGE_TILE_TYPE, null, true);}
    catch (Exception $e) {
      try {$this->resolveSelector(self::BAND_CAMP_ARTICLE_TILE_TYPE, null, true);}
      catch (Exception $e) {
        try {$this->resolveSelector(self::BAND_CAMP_VIDEO_TILE_TYPE, null, true);}
        catch (Exception $e) {
          throw new exception("Fail verifyBrandCampArticleTilesAreInPlace: No Article Tiles found");
        }
      }
    }
    #Anticipated exceptions are stored so that tile testing can continue
    $exceptions = [];

    #Get all the loaded tiles and iterate through them
    $tiles = $this->getBrandCampTiles();
    foreach( $tiles as $tile ){
      $tile_errors = null;

      #Determine if the tile is an "article" tile
      $is_article = ($this->isTileType($tile, self::BAND_CAMP_ARTICLE_TILE_TYPE) or
                     $this->isTileType($tile, self::BAND_CAMP_VIDEO_TILE_TYPE)   or
                     $this->isTileType($tile, self::BAND_CAMP_IMAGE_TILE_TYPE)     );

      #Only Article tiles are verified.
      if ($is_article) {
        $tile->focus();
        $tile_errors = $this->articleTileVerification( $tile);
      }
      #Capture tile verification error, if any
      if ($tile_errors) $exceptions[] = $tile_errors;
    } #foreach tile

    #Report any anticipated exceptions
    if (count($exceptions) != 0 ) {
      $e = implode("\n", $exceptions);
      throw new Exception( $e );
    }
  }

  const BRAND_CAMP_MENU_BUTTON  = "div.bc-menu-btn";
  const BRAND_CAMP_MENU_TAB     = "div.nav-tab";
  const BRAND_CAMP_SIDEBAR      = "#block-wfm-monster-nav-wfm-monster-navigation";
  const BRAND_CAMP_SIDEBAR_OPEN = ".displayed";

  /**
   * @When /^I press the Brand Camp Menu "([^"]*)"$/
   * REQUIREMENTS: The current url is the Brand Camp homepage.
   * @param $control Either menu "SIDEBAR" on the left of page or menu "LINK" at top of page.
   * @throws Exception
   */
  public function iPressTheBrandCampMenu($control) {
    switch (strtoupper($control) ) {
      case "LINK":
        $this->setSelector(self::BRAND_CAMP_MENU_BUTTON);
        break;
      case "SIDEBAR":
        $this->setSelector(self::BRAND_CAMP_MENU_TAB);
        break;
      default:
        throw new Exception("Test Case Error: Unrecognized control '$control'");
    }
  }

  /**
   * @Then /^Sidebar is open$/
   * REQUIREMENTS: The current url is the Brand Camp homepage.
   */
  public function sidebarIsOpen() {
    $sidebar = $this->resolveSelector(self::BRAND_CAMP_SIDEBAR);
    if( $sidebar->find('css', self::BRAND_CAMP_SIDEBAR_OPEN)) return;
    throw new Exception("Sidebar Menu is not visible/open");
  }

  /**
   * @Then /^Sidebar is closed$/
   * REQUIREMENTS: The current url is the Brand Camp homepage.
   */
  public function sidebarIsClosed() {
    $sidebar = $this->resolveSelector(self::BRAND_CAMP_SIDEBAR);
    if( !$sidebar->find('css', self::BRAND_CAMP_SIDEBAR_OPEN)) return;
    throw new Exception("Sidebar Menu is not hidden/closed");
  }

  static $SPECIAL_TILE_LANDINGS = [
    "ARE YOU ON THE LIST"           => "/newsletters",
    "FIND A STORE"                  => "/stores/list",
    "ORDER ONLINE PICK UP IN STORE" => "/values-matter/order-online-pick-store",
    "JOIN THE CONVERSATION"         => "/values-matter/your-stories",
    "SEND A VIRTUAL GIFT CARD"      => "/custom/wholefoods/",
    "CALLING ALL YAY-SAYERS"        => "/careers/find-and-apply-jobs",
    "GROCERY DELIVERY"              => "/values-matter/instacart-grocery-delivery",
    "SAVE WITH DIGITAL COUPONS"     => "/values-matter/digital-coupons-your-phone",
  ];

  static $SPECIAL_TILE_URLS = [
    "ARE YOU ON THE LIST"           => "are-you-list",
    "FIND A STORE"                  => "find-our-stores",
    "ORDER ONLINE PICK UP IN STORE" => "order-online-pick-store",
    "JOIN THE CONVERSATION"         => "join-conversation-foods4thought",
    "SEND A VIRTUAL GIFT CARD"      => "send-virtual-gift-card",
    "CALLING ALL YAY-SAYERS"        => "calling-all-yay-sayers",
    "GROCERY DELIVERY"              => "instacart-grocery-delivery",
    "SAVE WITH DIGITAL COUPONS"     => "digital-coupons-your-phone",
  ];

  // Note these links need to be exactly the same as Erin puts on the website.
  static $SPECIAL_TILE_ERIN_URLS = [
    "ARE YOU ON THE LIST"           => "/values-matter/are-you-list",
    "FIND A STORE"                  => "/values-matter/find-our-stores",
    "ORDER ONLINE PICK UP IN STORE" => "/values-matter/order-online-pick-store",
    "JOIN THE CONVERSATION"         => "/values-matter/join-conversation-foods4thought",
    "SEND A VIRTUAL GIFT CARD"      => "/values-matter/send-virtual-gift-card",
    "CALLING ALL YAY-SAYERS"        => "/values-matter/calling-all-yay-sayers",
    "GROCERY DELIVERY"              => "/values-matter/instacart-grocery-delivery",
    "SAVE WITH DIGITAL COUPONS"     => "/values-matter/digital-coupons-your-phone",
  ];
  //Note. These url's are the actual URL's that appear when you click on the tile.
  static $SPECIAL_TILE_REAL_URLS = [
    "ARE YOU ON THE LIST"           => "/newsletters",
    "FIND A STORE"                  => "/stores/list",
    "ORDER ONLINE PICK UP IN STORE" => "/shop/choose",
    "JOIN THE CONVERSATION"         => "/values-matter/your-stories",
    "SEND A VIRTUAL GIFT CARD"      => "https://wholefoods.buyatab.com/custom/wholefoods/",
    "CALLING ALL YAY-SAYERS"        => "/careers",
    "GROCERY DELIVERY"              => "https://delivery.wholefoodsmarket.com/?utm_source=partner_wholefoodsmarket.com&utm_medium=referral&utm_campaign=wholefoodsmarket_homepage_seasonaltile",
    "SAVE WITH DIGITAL COUPONS"     => "/apps",
  ];


  /**
   * @Then /^Verify all Special tiles are present$/
   * REQUIREMENTS: The current url is the Brand Camp homepage and at least 48 tiles have been loaded so that all eight
   * special tiles should be present.
   */
  public function verifyAllSpecialTilesArePresent() {
    $exceptions = null;
    #Create an array with the URLs as keys and zero for values.
    foreach(self::$SPECIAL_TILE_URLS as $key  ) $special_count[$key] = 0;

    $tiles = $this->getBrandCampTiles();

    #Count how often each special tile is present
    foreach ($tiles as $tile) {
      #Ignore non-Special tiles
      if( !$this->isTileType($tile, self::BAND_CAMP_SPECIAL_TILE_TYPE)) continue;

      $lnk = $tile->find('css',self::BC_TILE_FIGURE_LINK)->getAttribute('href');
      $key = pathinfo($lnk, PATHINFO_FILENAME);
      try{
        $special_count[$key] += 1;
      } catch (Exception $e) {$exceptions[] = "Unknown Special Tile url='($lnk)'";}
    }

    #Go through the tile counts and note zero counts as exceptions.
    foreach(self::$SPECIAL_TILE_URLS as $tile_name => $url){
      if (0 == $special_count[$url]) {
        $exceptions[] = "Missing Special Tile [$tile_name] url=[$url";
      }
    }
    if ($exceptions) throw new Exception(implode("\n", $exceptions ));
  }

  /**
   * @Then /^Verify all Special tiles are redirected$/
   * Cycle thru all special tile urls and verify that they match what where we
   *  send them.
   */
  public function verifyAllSpecialTilesAreRedirected() {
    $exceptions = null;

    //Derive the actual base url
    $base_url = $this->getSession()->getCurrentUrl();
    $base_url = parse_url($base_url);
    $base_url = $base_url['host'];

    //Create an array of special tiles from the static array above and
    //build complete URLs for erin urls and real urls that you see when
    //you click on the tiles.
    foreach (self::$SPECIAL_TILE_ERIN_URLS as $key => $url ) {
      if (substr(self::$SPECIAL_TILE_REAL_URLS[$key], 0, 5) == "https") {
        $real_url = self::$SPECIAL_TILE_REAL_URLS[$key];
      } else if ($key == "ORDER ONLINE PICK UP IN STORE") {
        //Secure.
        $real_url = "https://" . $base_url . self::$SPECIAL_TILE_REAL_URLS[$key];
      } else {
        $real_url = "http://" . $base_url . self::$SPECIAL_TILE_REAL_URLS[$key];
      }
      $erin_url = "http://" . $base_url . $url;
      $special_tiles[$key] = array($erin_url, $real_url);
    }

    //Build a list of special tiles from the website
    $special_tiles_from_website = array();
    $tiles = $this->getBrandCampTiles();
    foreach ($tiles as $tile) {
      //Ignore non-Special tiles
      if ($this->isTileType($tile, self::BAND_CAMP_SPECIAL_TILE_TYPE)) {
        //Capture each special tile's link.
        $lnk = $tile->find('css',self::BC_TILE_FIGURE_LINK)->getAttribute('href');
        //Grab the filename part of the url for use as a key.
        $key = pathinfo($lnk, PATHINFO_FILENAME);
        $special_tiles_from_website[$key] = $lnk;
      }
    }

    //Navigate to each special tile url and confirm it is the real_url.
    foreach ($special_tiles_from_website as $key => $lnk) {

      $this->getSession()->visit($lnk);
      sleep(1);

      //Lookup the real url it should land on.. It isn't what you might expect.
      foreach( $special_tiles as $tile_name => $url_array) {
        // find the $lnk
        if ($url_array[0] == $lnk) {
          $expected = $url_array[1];
          break;
        }
      }

      //Sorry no match.
      if (empty($expected)) {
        $exceptions[] = "URL not found for Special Tile [$tile_name] searched for url:[$lnk]";
        continue;
      }

      $landing  = $this->getSession()->getCurrentUrl();
      if (!stristr($landing, $expected)) {
        $exceptions[] = "Bad redirection for Special Tile [$tile_name] erin_url=[$lnk], expected url[$expected]";
      }
      else {
        if (DEBUG == 1) {
          echo "Confirmed [$tile_name] url=[$lnk], expected url[$expected]\n";
        }
      }
    }

    if ($exceptions) throw new Exception(implode("\n", $exceptions ));
  }

  /**
   * @When /^I filter Brand Camp by "([^"]*)"$/
   * REQUIREMENTS: The current url is the Brand Camp homepage.
   * @param $filter_name One of the following (FOOD, PEOPLE, PLANET, ALL, LEARN,DO, BOTH, OUR STORIES, YOUR STORIES)
   * @throws Exception
   */
  public function iFilterBrandCampBy($filter_name) {
    if( $this->isShowMeFilter($filter_name)        )  $this->setBrandCampShowMe($filter_name);
    else if ( $this->isIWantToFilter($filter_name) )  $this->setBrandCampIWantTo($filter_name);
    else if ($this->isViewFilter($filter_name)     ) $this->setBrandCampView($filter_name);
    else throw new Exception( "Invalid Brand Camp filter '$filter_name' ");
    #Wait for the tiles to show up
    try {
      $this->resolveSelector(self::BC_TILE_FIGURE_IMAGE, null, true);
    }
    catch (Exception $e) {throw new Exception("After filtering by '$filter_name' no tiles appeared\n" . $e);}
  }

  const BC_UGC_SEARCH_INPUT  = "#ugc-search-value" ;
  const BC_UGC_SEARCH_SUBMIT = "#ugc-search-submit";

  /**
   * @Given /^I Search curated tiles for "([^"]*)"$/
   * REQUIREMENTS: The current url is the Your Stories page
   * @param $search_text The text to search for amoung the UGC author names.
   */
  public function iSearchCuratedTilesFor($search_text) {
    $this->setSelector(self::BC_UGC_SEARCH_INPUT, $search_text);
    $this->setSelector(self::BC_UGC_SEARCH_SUBMIT);
    #wait for the tiles to disappear
    while ($this->getSession()->getPage()->has('css',self::BAND_CAMP_UGC_TILE_TYPE) )
    {
      null;
    }
  }

  const BC_UGC_SEARCH_NO_MATCH = ".no-matches";

  /**
   * @Then /^I should see "([^"]*)" has "([^"]*)" curated tiles?$/
   * REQUIREMENTS: The current url is the Your Stories page, most likely after a search has been issued.
   * @param $ugc_name The string used to initiate the search
   * @param $amount A positive integer (0,1,2,...) or "Multipe" or "At least 1"
   * @throws Exception
   */
  public function iShouldSeeHasCuratedTiles($ugc_name, $amount ) {
    if ( is_numeric($amount) ) $count = strval($amount);
    else                       $count = -1;

    #Handle case of no tiles expected (0 tiles and message 'No matches').
    if (0 == $count) {
      $tiles_exist = $this->getSession()->getPage()->has('css',self::BAND_CAMP_UGC_TILE_TYPE);
      if ($tiles_exist) throw new Exception("UGC Search: Tiles are present when none are expected");
      $no_matches = $this->resolveSelector(self::BC_UGC_SEARCH_NO_MATCH);
      if ("NO MATCHES FOUND" != $no_matches->getText()) {
        throw new Exception("UGC Search: Expected 'No matches' string was not found");
      }
      return; #Test passed
    }


    #$ugc_selector = '[name="' . preg_replace('@','',$ucg_name) . '"]' ;
    $ugc_tiles = $this->resolveSelector(self::BC_UGC_TILE_CONTRIBUTER, null, true);
    $ugc_count = count($ugc_tiles);

    #A specified count of tiles expected.
    if ($count > 0) {
      if ($ugc_count != $count) throw new Exception("UGC Search: Tile count=$ugc_count does not equal expected=$count.");
      else return; #Test passed
    }

    switch ( strtolower($amount) ){
      case "multiple":
        if ($ugc_count < 2) throw new Exception("Expected multiple UGC tiles but only got count=$ugc_count.");
        break;
      case "at least 1":
        if ($ugc_count < 1) throw new Exception("Expected at least one UGC tile but got none");
        break;

      default:
        throw new Exception("Bad testcase variable \$amount='$amount'.");
    }

    #Wait if the last tile name is not available
    # (this is bad but i have not found an alternate way to sync)
    if ($ugc_tiles[$ugc_count-1]->getText() == '') sleep(5);

    #The counts have been verified now verify the correct search name was returned
    $cc=0;
    foreach ($ugc_tiles as $ugc_tile){
      $ugc_text = $ugc_tile->getText();
      $cc+=1;
      if( FALSE == stristr($ugc_text, $ugc_name) ) {
        throw new Exception("$cc Expected UGC contributor name '$ugc_name' not found in '$ugc_text'" .
                            " See screenshot at " . $this->putScreenshot( $text=$ugc_name . "_NotFound") );
      }
    }
  }


  #####################################################################################################################
  #####################################################################################################################
  #         S U P P O R T   F U N C T I O N S
  #####################################################################################################################
  #####################################################################################################################

  const BRAND_CAMP_CONTAINER = ".view-brand-camp-list";
  const BRAND_CAMP_SEARCH_FORM = "#ugc-search-form";
  const BRAND_CAMP_SEARCH_TEXT_INPUT = "#ugc-search-value";
  const BRAND_CAMP_SEARCH_SUBMIT = "#ugc-search-submit";
  const BRAND_CAMP_SHOW_ME_ALL = ".showme [data-filter=\"0\"]";
  const BRAND_CAMP_SHOW_ME_FOOD = ".showme [data-filter=\"1\"]";
  const BRAND_CAMP_SHOW_ME_PEOPLE = ".showme [data-filter=\"2\"]";
  const BRAND_CAMP_SHOW_ME_PLANET = ".showme [data-filter=\"3\"]";
  const BRAND_CAMP_I_WANT_TO_BOTH = ".iwantto [data-filter=\"0\"]";
  const BRAND_CAMP_I_WANT_TO_LEARN = ".iwantto [data-filter=\"1\"]";
  const BRAND_CAMP_I_WANT_TO_DO = ".iwantto [data-filter=\"2\"]";
  const BRAND_CAMP_VIEW_OURS = ".ugc [data-filter=\"0\"]";
  const BRAND_CAMP_VEIW_YOURS = ".ugc [data-filter=\"1\"]";
  const BRAND_CAMP_LOAD_MORE = "#isopager";
  const BRAND_CAMP_CONTENT_CONTAINER = ".view-content";
  const BRAND_CAMP_VISIBLE_TILES = "[weight]";
  const BRAND_CAMP_VISIBLE_HEADER_LINKS = "[weight] header a[href]";
  const BRAND_CAMP_YOUR_STORIES_PAGE = "values-matter/your-stories";



  #Functions that determine the type (SHOW ME, I WANT TO, VIEW) filter
  private $show_me_filters = [ "FOOD"   => self::BRAND_CAMP_SHOW_ME_FOOD,
                               "PEOPLE" => self::BRAND_CAMP_SHOW_ME_PEOPLE,
                               "PLANET" => self::BRAND_CAMP_SHOW_ME_PLANET,
                               "ALL"    => self::BRAND_CAMP_SHOW_ME_ALL   ];

  private $i_want_to_fillters = ["LEARN" => self::BRAND_CAMP_I_WANT_TO_LEARN,
                                 "DO"    => self::BRAND_CAMP_I_WANT_TO_DO   ,
                                 "BOTH"  => self::BRAND_CAMP_I_WANT_TO_BOTH ];

  private $view_filters = ["OUR STORIES"  => self::BRAND_CAMP_VIEW_OURS  ,
                           "YOUR STORIES" => self::BRAND_CAMP_VEIW_YOURS ];


  private function isShowMeFilter( $filter )  {return array_key_exists($filter, $this->show_me_filters);}
  private function isIWantToFilter( $filter ) {return array_key_exists($filter, $this->i_want_to_fillters);}
  private function isViewFilter( $filter )    {return array_key_exists($filter, $this->view_filters);}


  /**
   * Set the Brand Camp "Show Me" filter
   * @param string $filter one of the following- ALL,FOOD,PEOPLE,PLANET
   * @throws Exception Invalid filter
   */
  public function setBrandCampShowMe($filter="ALL"){
    if( $this->isShowMeFilter($filter) ){
      #Set the "SHOW ME" filter to the given filter value
      #Then wait for Ajax to settle down
      $this->setSelector($this->show_me_filters[$filter]);
      $this->iWaitForAjaxCallsToFinish();
      return;
    }

    #The given filter is not handled by this function, throw an exception
    throw new Exception(
      "Invalid Show Me filter $filter. Please retry with one of the following " .
      implode(",", array_keys($this->show_me_filters)));
  }

  /**
   * Set the Brand Camp "I Want To" filter
   * @param string $filter One of the following- BOTH,DO,LEARN
   * @throws Exception Invalid filter
   */
  public function setBrandCampIWantTo($filter="BOTH"){
    if( $this->isIWantToFilter($filter) ) {
      #Set the "I WANT TO" filter to the given filter value
      #Then wait for Ajax to settle down
      $this->setSelector($this->i_want_to_fillters[$filter]);
      $this->iWaitForAjaxCallsToFinish();
      return;
    }

    #The given filter is not handled by this function, throw an exception
    throw new Exception(
      "Invalid I Want To filter $filter. Please retry with one of the following"     .
      implode(",", array_keys($this->i_want_to_filters)));

  }

  /**
   * Set the Brand Camp View filter
   * @param string $filter One of the following- Our Stories,Your Stories
   * @throws Exception
   */
  public function setBrandCampView($filter="OUR STORIES"){
    if( $this->isViewFilter($filter) ){
      #Set the "VIEW" filter to the given filter value
      #Then wait for Ajax to settle down
      $this->setSelector($this->view_filters[$filter]);
      $this->iWaitForAjaxCallsToFinish();
      return;
    }
    #The given filter is not handled by this function, throw an exception
    throw new Exception(
      "Invalid Show Me filter $filter. Please retry with one of the following" .
      implode(",", array_keys($this->view_filters)));
  }


  const BRAND_CAMP_VISIBLE_PLANET_TILES = "[weight] [title=\"Planet\"]";
  const BRAND_CAMP_VISIBLE_PEOPLE_TILES = "[weight] [title=\"People\"]";
  const BRAND_CAMP_VISIBLE_FOOD_TILES   = "[weight] [title=\"Food\"]";
  /**
   * Get the counts for the currently loaded number of FOOD, PEOPLE and PLANET tiles
   * @return array of counts indexed by the SHOW ME filters FOOD, PEOPLE, and PLANET
   */
  private function getTileTypeCounts(){
    #Make sure at least one Brand Camp tiles has loaded
    $this->resolveSelector(self::BRAND_CAMP_VISIBLE_TILES, null, TRUE);
    $brand_camp_page = $this->getSession()->getPage();
    try{
      $food_tiles = $brand_camp_page->findAll("css",self::BRAND_CAMP_VISIBLE_FOOD_TILES);
    } catch (Exception $e) {$food_tiles = [];}

    try{
      $people_tiles = $brand_camp_page->findAll("css",self::BRAND_CAMP_VISIBLE_PEOPLE_TILES);
    } catch (Exception $e) {$people_tiles = [];}

    try{
      $planet_tiles = $brand_camp_page->findAll("css",self::BRAND_CAMP_VISIBLE_PLANET_TILES);
    } catch (Exception $e) {$planet_tiles = [];}


    $counts = ["FOOD" => count($food_tiles), "PEOPLE" => count($people_tiles),"PLANET" => count($planet_tiles)];
    return $counts;
  }

  const BRAND_CAMP_ACTIVE_FILTERS = ".filter.selected[data-filter]";
  /**
   * Get the active "Show Me", "I Want To", and "View" filter settings
   * @return array key,value ["filter category" => "active filter",...]
   */
  public function getActiveBrandCampFilters(){
    #Make sure at least one Brand Camp tiles has loaded
    $this->resolveSelector(self::BRAND_CAMP_VISIBLE_TILES, null, TRUE);

    //get the active filters
    $active_filters = $this->resolveSelector(self::BRAND_CAMP_ACTIVE_FILTERS, null, true);

    foreach ($active_filters as $filter){
      $active_text = $filter->getText();
      $filter_group = $filter->getParent()->getParent()->find("css",".title")->getText();
      $results[$filter_group] = $active_text;
    }
    return $results;
  }


  /**
   * Wait for Brand Camp tiles to load and return them
   * @return Array of tile elements
   */
  public function getBrandCampTiles(){
    $tiles = $this->resolveSelector(self::BRAND_CAMP_VISIBLE_TILES, null, true);
    return $tiles;
  }

  /**
   * Get the number of tiles loaded into the Brand Camp page
   * @return int The number of tiles
   */
  public function getTileCount(){ return count($this->getBrandCampTiles());  }

  #Tile positions are determined by the value of the tile's weight attribute. Weights may or may not appear in ascending
  #order, but for placement, weight=0 is first weight=1 is second and so on. Tiles are loaded in groups of 12.
  #Tile locations within each group are constant. The following constants are the weight values within a group of 12
  # tiles, where the Special tiles and the UGC tiles are positioned relative to the weight=0 tile for that group.
  #Note The final tile group loaded may have less than 12 tiles.
  #.
  const BC_SPECIAL_POSITION_A = 2;
  const BC_SPECIAL_POSITION_B = 6;
  const BC_UGC_POSITION_A = 1;
  const BC_UGC_POSITION_B = 8;

  const BAND_CAMP_SPECIAL_TILE_TYPE = "article.node-bc-connect";    #Special tile type
  const BAND_CAMP_UGC_TILE_TYPE     = "article.node-bc-ugc-image";  #User Generated Content type
  const BAND_CAMP_ARTICLE_TILE_TYPE = "article.node-bc-article";    #Article type
  const BAND_CAMP_IMAGE_TILE_TYPE   = "article.node-bc-image";      #Another article type
  const BAND_CAMP_VIDEO_TILE_TYPE   = "article.node-bc-video";      #And another article type
  /**
   * @param $tile An HTML element that can be selected by the BRAND_CAMP_VISIBLE_TILES constant
   * @return TRUE if it is a tile matching type, FALSE otherwise
   */
  public function isTileType($tile, $type){return (null != $tile->find("css",$type) ); }

  #------------------
  #Special Tile type
  #------------------
  const BC_TILE_FIGURE_LINK  = "figure a";   #tile link
  const BC_TILE_FIGURE_IMAGE = "figure img"; #tile picture
  /**
   * Verify tile position and that link, and image are populated
   * @param $tile
   * @param null $tileValues
   * @return null|string
   */
  function specialTileVerification( $tile, $tileValues=null){
    $exceptions = null;
    #Normalizing is the tile's position within its group of 12 tiles
    $weight = $tile->getAttribute("weight");
    $normalized = $weight % 12;

    #Special tiles are expected in positions 2 and 6 within each group of 12 tiles
    $out_of_position = (($normalized != self::BC_SPECIAL_POSITION_A) and ($normalized != self::BC_SPECIAL_POSITION_B));

    #Get the various tile elements
    $img = $tile->find('css',self::BC_TILE_FIGURE_IMAGE);
    $lnk = $tile->find('css',self::BC_TILE_FIGURE_LINK);

    if ($out_of_position) $exceptions[] = "Tile out of position ";
    if (null == $img) $exceptions[] = "Tile image is missing.";
    if (null == $lnk) $exceptions[] = "Tile link is missing.";

    #if (2 == strval($tile->getAttribute('weight'))) $exceptions[] = "TEST SPECIAL EXCEPTION";

    if (!$exceptions) return null; #No error leave

    #There was an exception, try to add as much info as possible to the exception
    #Title,subtitle,summary, and icons all in one.
    $info = "\tTile text='" . $tile->getText()                        . "'";
    if ($lnk )  $info .= "\tTile Link='" . $lnk->getAttribute("href") . "'";
    if ($img)   $info .= "\timg src=" . $img->getAttribute("src")     . "'";
    $exceptions[]= $info;

    #There was an error, create an exception string.
    #Note For reporting purposes rows and columns begin with 1 not zero.
    $row = strval(floor(1+$weight/3));
    $col = strval(1+$weight%3);
    $tile_report = implode("\n\t",$exceptions);
    return "Fail: Special Tile verify error on row $row column $col\n\t$tile_report";
  }


  #--------------
  #UGC Tile Type
  #--------------
  const BC_UGC_TILE_ALBUM       = ".field-name-field-bc-album-name .field-item"; #Hash tag upper left
  const BC_UGC_TILE_CONTRIBUTER = ".field-name-field-bc-src-username a";         #@nnn tag upper right
  const BC_UGC_TILE_LIKES_COUNT = ".field-name-field-bc-src-likes-count";        #likes count
  /**
   * Verify tile position and that link, image, like counts and contributor are populated
   * @param $tile The tile to verify
   * @param null $tileValues Not used at this time
   * @return null|string
   */
  function ugcTileVerification( $tile, $tileValues=null){
    $exceptions = null;
    #Normalizing is the tile's position within its group of 12 tiles
    $weight = $tile->getAttribute("weight");
    $normalized = $weight % 12;

    #UGC tiles are expected in positions 1 and 8 within each normalized group of 12 tiles
    $out_of_position = (($normalized != self::BC_UGC_POSITION_A) and ($normalized != self::BC_UGC_POSITION_B));

    #Get the various tile elements
    $img   = $tile->find('css',self::BC_TILE_FIGURE_IMAGE);
    $lnk   = $tile->find('css',self::BC_TILE_FIGURE_LINK);
    $album = $tile->find('css',self::BC_UGC_TILE_ALBUM);
    $contr = $tile->find('css',self::BC_UGC_TILE_CONTRIBUTER);
    $likes = $tile->find('css',self::BC_UGC_TILE_LIKES_COUNT);

    if ( $out_of_position ) $exceptions[] = "Tile out of position ";
    #Verify the various elements exist
    if (null == $img )  $exceptions[] = "Image is missing.";
    if (null == $lnk )  $exceptions[] = "Link is missing.";
    if (null == $album) $exceptions[] = "Hash tag missing.";
    if (null == $contr) $exceptions[] = "@contributor missing.";
    if (null == $likes) $exceptions[] = "Likes missing.";

    #if (1 == strval($tile->getAttribute('weight'))) $exceptions[] = "TEST UGC EXCEPTION";

    #If no exceptions return null
    if (!$exceptions) return null;

    #There was an exception, try to add as much info as possible to the exception
    #Title,subtitle,summary, and icons all in one.
    $info = "\tTile text='" . $tile->getText()                        . "'";
    if ($lnk )  $info .= "\tTile Link='" . $lnk->getAttribute("href") . "'";
    if ($img)   $info .= "\timg src=" . $img->getAttribute("src")     . "'";
    $exceptions[]= $info;

    #Note For reporting purposes rows and columns begin with 1 not zero.
    $row = strval(floor(1+$weight/3));
    $col = strval(1+$weight%3);
    $tile_report = implode("\n\t",$exceptions);
    return "Fail: UGC Tile verify error on row $row column $col\n\t$tile_report";
  }

  #--------------
  #Article Tile
  #--------------
  const BC_ARTICLE_TILE_TITLE    = "article header a";                        #title under image
  const BC_ARTICLE_TILE_SUBTITLE = "article header .sub-title";               #to right of title
  const BC_ARTICLE_TILE_ICON     = ".field-name-field-bc-showme .field-item"; #small 1-3 icons upper right
  const BC_ARTICLE_TILE_SUMMARY  = ".field-name-field-bc-summary";            #under title
  /**
   * Verify tile position and link, image, and associated icons
   * @param $tile The tile to verify
   * @param null $tileValues No used at this time
   * @return array|null|string
   */
  function articleTileVerification( $tile, $tileValues=null){
    $exceptions = null;

    #Normalizing is the tile's position within its group of 12 tiles
    $weight = $tile->getAttribute("weight");
    $normalized  = $weight % 12;

    #Determine if normalized position is that expected of an article tile (0,3,4,5,7,9,10,11)
    $in_position = (($normalized!=self::BC_SPECIAL_POSITION_A) and ($normalized!=self::BC_SPECIAL_POSITION_B) and
                    ($normalized!=self::BC_UGC_POSITION_A)     and ($normalized!=self::BC_UGC_POSITION_B)         );

    #Get the various tile elements
    $img   = $tile->find('css',self::BC_TILE_FIGURE_IMAGE);
    $lnk   = $tile->find('css',self::BC_TILE_FIGURE_LINK);
    $title = $tile->find('css',self::BC_ARTICLE_TILE_TITLE);
    $sub   = $tile->find('css',self::BC_ARTICLE_TILE_SUBTITLE);
    $sum   = $tile->find('css',self::BC_ARTICLE_TILE_SUMMARY);
    $icon  = $tile->findAll('css',self::BC_ARTICLE_TILE_ICON);

    #Verify the various elements exist
    if (!$in_position ) $exceptions[] = "Tile out of position ";
    if (null == $img )  $exceptions[] = "Image is missing.";
    if (null == $lnk )  $exceptions[] = "Link is missing.";
    if (null == $icon)  $exceptions[] = "Icon(s) missing.";
    if (null == $title) $exceptions[] = "Title missing.";
    if (null == $sub)   $exceptions[] = "Sub-Title missing.";
    if (null == $sum)   $exceptions[] = "Summary missing.";

    #if (0 == strval($tile->getAttribute('weight'))) $exceptions[] = "TEST ARTICLE EXCEPTION";

    #If no exceptions return null
    if (!$exceptions) return null;

    #There was an exception, try to add as much info as possible to the exception
    #Title,subtitle,summary, and icons all in one.
    $info = "\tTile text='" . $tile->getText()                        . "'";
    if ($lnk )  $info .= "\tTile Link='" . $lnk->getAttribute("href") . "'";
    if ($img)   $info .= "\timg src=" . $img->getAttribute("src")     . "'";
    $exceptions[]= $info;

    #Note For reporting purposes rows and columns begin with 1 not zero.
    $row = strval(floor(1+$weight/3));
    $col = strval(1+$weight%3);
    $tile_report = implode("\n\t",$exceptions);
    return "Fail: Article Tile verify error on row $row column $col\n\t$tile_report";
  }


} #end BrandCampDesktop0