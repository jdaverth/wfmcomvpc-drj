<?php
//require_once 'FeatureContext.php';

use Behat\Behat\Context\BehatContext;
use Drupal\DrupalExtension\Context\DrupalSubContextInterface;

class DesktopSubContext extends BehatContext implements DrupalSubContextInterface {

  const REGION_CONTENT = 'content';
  const COUPONS_PRINT_BUTTON_SELECTOR = "#edit-form-submit";

  public function __construct(array $parameters = NULL) {
    // do subcontext initialization
  }

  public static function getAlias() {
    return 'desktop';
  }

  public function getSession() {
    return $this->getMainContext()->getSession();
  }

  public function getRegion($region) {
    return $this->getMainContext()->getRegion($region);
  }

  /**
   * Find elements by CSS selector.
   * @param string $selector
   *   Valid CSS selector
   * @return object
   *   Results of findAll on selector.
   */
  public function getElements($selector) {
    $page = $this->getSession()->getPage();
    return $page->findAll('css', $selector);
  }

  /**
   * @Given /^That the Window is Full Screen$/
   */
  public function thatTheWindowIsFullScreen() {
    $this->getSession()->resizeWindow(1280, 800);
  }

  /**
   * @Then /^I should see the Global Marquees$/
   */
  public function iShouldSeeTheGlobalMarquees() {
    $element = $this->getSession()->getPage();
    $find = $element->findall('css', '#block-views-marquee-scroller-block .marquee-container');
    if (empty($find)) {
      throw new Exception("No Marquees were found on the home page.");
    }
    if (count($find) <= 1) {
      throw new Exception("There is only one marquee on the home page.");
    }
  }

  /**
   * @Given /^I should see the Go Local block$/
   */
  public function iShouldSeeTheGoLocalBlock() {
    $element = $this->getSession()->getPage();
    $find = $element->find('css', '.view-store-lookup-by-address.view-display-id-front_page');
    if (empty($find)) {
      throw new Exception('Go Local pod was not found');
    }
  }

  /**
   * @Then /^I should see the Pluck forum$/
   */
  public function iShouldSeeThePluckForum() {
    $element = $this->getSession()->getPage();
    $find = $element->find('css', '.Forums_MainContainer');
    if (empty($find)) {
      throw new Exception('Forum does not display');
    }
  }

  /**
   * @Given /^I should see suggested stores$/
   */
  public function iShouldSeeSuggestedStores() {
    $this->getSession()->wait(5000, '(0 === jQuery.active)');
    $element = $this->getSession()->getPage();
    $find = $element->findall('css', '.view-store-lookup-by-address.jquery-once-1-processed .views-row');
    if (empty($find)) {
      throw new Exception("No suggested stores.");
    }
    if (count($find) <= 3) {
      throw new Exception("There are three or fewer suggested stores.");
    }
  }

  /**
   * @Given /^I should see tabs to filter recipes$/
   */
  public function iShouldSeeTabsToFilterRecipes() {
    $element = $this->getSession()->getPage();
    $element->findById('quicktabs-recipes');
    if (empty ($element)) {
      throw new Exception("Recipe search form wasn\'t found.");
    }
    //check for a list of tabs
    $quickTabs = $element->find('css', '#quicktabs-recipes .quicktabs-tabs');
    if (empty($quickTabs)) {
      throw new Exception("Unable to find container: #quicktabs-recipes .quicktabs-tabs");
    }
    $html = $quickTabs->getHtml();
    //count the number of li children
    $num_items = substr_count($html, "<li");
    if (!$num_items) {
      throw new Exception("Didn\'t find all the recipe tabs/");
    }
    //look through each tab and check that it has content
    for ($i = 0; $i < $num_items; $i++) {
      if ($element->findById('quicktabs-tabpage-recipes-' . $i) == NULL) {
        throw new Exception("Recipe tab $i is missing content");
      }
    }
  }

  /**
   * @Then /^I should see the Facebook Like Buttom$/
   */
  public function iShouldSeeTheFacebookLikeButtom() {
    $sc = $this->getSocialContainer();
    if (!$sc->has('css', '#fb-root')) {
      throw new Exception('Social: Facebook #fb-root not loaded');
    }
    if (!$sc->has('css', '.recipe-fb')) {
      throw new Exception('Social: Facebook .recipe-fb not loaded');
    }
  }

  /**
   * @Then /^I should see the Twitter Tweet Button$/
   */
  public function iShouldSeeTheTwitterTweetButton() {
    $sc = $this->getSocialContainer();
    if (!$sc->has('css', '.recipe-twitter')) {
      throw new Exception('Social: Twitter .recipe-twitter not loaded');
    }
  }

  /**
   * @Then /^I should see the Pin It Button$/
   */
  public function iShouldSeeThePinItButton() {
    $sc = $this->getSocialContainer();
    if (!$sc->has('css', '.pinit')) {
      throw new Exception('Social: Pinterest .pinit not loaded');
    }
  }

  /**
   * Returns the div container that WFM's social links reside in
   */
  private function getSocialContainer() {
    $element = $this->getSession()->getPage();
    $social_node = $element->find('css', '.social-links');
    if (!$social_node) {
      throw new Exception('Social links container not loaded');
    }
    return $social_node;
  }

  /**
   * @Given /^I check the first checkbox under occasions$/
   */
  public function iCheckTheFirstCheckboxUnderOccasions() {
    $element = $this->getSession()->getPage()->find('css', '#edit-field-recipe-occasions');
    $checkbox = $element->find('css', '.form-checkbox');
    $checkbox->check();
  }

  /**
   * @Then /^I should be on a recipe search result page matching "([^"]*)"$/
   */
  public function iShouldBeOnARecipeSearchResultPageMatching($regex) {
    $url = $this->getSession()->getCurrentUrl();
    if (!preg_match($regex, $url)) {
      throw new Exception("Page url ($url) doesn't match the pattern $regex");
    }
  }

  /**
   * @Given /^I should see relevant search results for "([^"]*)" identified by "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeRelevantSearchResultsForIdentifiedByInTheRegion($search_term, $selector, $region) {
    $region_element = $this->getRegion($region);

    $results = $region_element->findAll('css', $selector);
    if (empty($results)) {
      throw new Exception("Unable to find '" . $selector . "'");
    }
    foreach ($results as $result) {
      if (stripos($result->getText(), $search_term) !== FALSE) {
        return;
      }
    }
    throw new Exception("Didn't find a search result title that contained the query.");
  }

  /**
   * @Then /^I should see the link "([^"]*)" with href "([^"]*)" under the "([^"]*)" heading link with href "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeTheLinkWithHrefUnderTheHeadingLinkWithHrefInTheRegion($menu_item_text, $menu_item_href, $heading_text, $heading_href, $menu_region) {
    $menu_region_node = $this->getRegion($menu_region);
    $menu_heading_nodes = $menu_region_node->findAll('css', 'ul.menu li a.secondary-link');
    $found_heading_text = FALSE;
    foreach ($menu_heading_nodes as $menu_heading_node) {
      $menu_heading_text = $menu_heading_node->getText();
      if (empty($menu_heading_text)) {
        $menu_heading_text = $menu_heading_node->getHtml();
      }
      if ($menu_heading_text == $heading_text) {
        $found_heading_text = TRUE;
        break;
      }
    }
    if (!$found_heading_text) {
      throw new Exception("Menu heading " . $heading_text . " was not found in the " . $menu_region . " region");
    }
    if (strpos($menu_heading_node->getAttribute('href'), $heading_href) === FALSE) {
      throw new Exception("Menu heading " . $heading_text . " found but link " . $heading_href . " is incorrect in the " . $menu_region . " region");
    }
    $heading_container_selector = $this->textToSelector($heading_text);
    //$sub_menu_items = $menu_region_node->findAll('css', 'ul.menu ul.menu li.leaf a');
    $sub_menu_items = $menu_region_node->findAll('css', 'div#monster-sub-nav-' . $heading_container_selector . ' a');
    if (!(is_array($sub_menu_items) && count($sub_menu_items) > 0)) {
      throw new Exception("Menu items under parent heading: " . $heading_text . " not found in the " . $menu_region . " region.");
    }
    $found = FALSE;
    foreach ($sub_menu_items as $href_node) {
      $link_text = $href_node->getText();
      if (empty($link_text)) {
        $link_text = $href_node->getHtml();
      }
      if ($menu_item_text == $link_text) {
        $found = TRUE;
        break;
      }
    }
    if (!$found) {
      throw new Exception("Menu item " . $menu_item_text . " under parent heading: " . $heading_text . " not found in the " . $menu_region . " region.");
    }
    if (strpos($href_node->getAttribute('href'), $menu_item_href) === FALSE) {
      throw new Exception("Menu item " . $menu_item_text . " under parent heading: " . $heading_text . " was found but its link " . $menu_item_href . " is incorrect, in the " . $menu_region . " region.");
    }
  }

  /**
   * For matching monster nav container identifiers
   */
  private function textToSelector($text) {
    $text = preg_replace('/[^\da-z\s]/i', '', $text);
    return strtolower(str_replace(' ', '-', $text));
  }

  /**
   * @Then /^I should see the disabled continue button in the "([^"]*)" container$/
   */
  public function iShouldSeeTheDisabledContinueButtonInTheContainer($container) {
    $container_node = $this->getSession()->getPage()->find('css', $container);
    if (empty($container_node)) {
      throw new Exception("Unable to find container " . $container);
    }
    $continue_button = $container_node->find('css', $container . " .form-submit.disabled");
    if (empty($continue_button)) {
      throw new Exception("Unable to find 'Continue' button in the " . $container . " region.");
    }
  }

  /**
   * @Then /^I should see the continue button in the "([^"]*)" container$/
   */
  public function iShouldSeeTheContinueButtonInTheContainer($container) {
    $container_node = $this->getSession()->getPage()->find('css', $container);
    if (empty($container_node)) {
      throw new Exception("Unable to find container " . $container);
    }
    /*$disabled = $contianer_node->find('css', $container ." .form-submit.disabled");
    if ($disabled) {
      throw new Exception("Continue Button found is Disabled.");
    }*/
    $continue_button = $container_node->find('css', $container . " .form-submit");
    if (empty($continue_button)) {
      throw new Exception("Unable to find 'Continue' button in the " . $container . " region.");
    }
  }

  /**
   * @Then /^I should see the select button in the "([^"]*)" region$/
   */
  public function iShouldSeeTheSelectButtonInTheRegion($region) {
    $region_node = $this->getRegion($region);
    $select_store_link = $region_node->findLink('Select');
    if (empty($select_store_link)) {
      throw new Exception("Unable to find 'Select' button in the " . $region . " region.");
    }
  }

  /**
   * @Given /^I have a store selected$/
   */
  public function iHaveAStoreSelected() {
    $store_list_path = 'stores/list';
    $url = $this->getSession()->getCurrentUrl();
    //this code block works around an IE feature that puts ?reloaded=true in the url
    $querystring_position = strpos($url, '?');
    if ($querystring_position !== FALSE) {
      $url = substr($url, 0, $querystring_position);
      $querystring = substr($url, $querystring_position, strlen($url));
      $url .= $store_list_path . $querystring;
    }
    else {
      $url .= $store_list_path;
    }
    $this->getSession()->visit($url);
    $this->getMainContext()->iWaitForMilliseconds('5000');
    $inputNode = $this->getSession()->getPage()->find("css", 'input#edit-field-geo-data-latlon-address');
    if (empty($inputNode)) {
      throw new Exception('Unable to find input field: #edit-field-geo-data-latlon-address on page /stores/list');
    }
    $state = $this->getMainContext()->getRandomState();
    $inputNode->setValue($state);
    $submitNode = $this->getSession()->getPage()->find("css", 'input#edit-submit-store-lookup-by-address');
    if (empty($submitNode)) {
      throw new Exception('Unable to find submit button: #edit-submit-store-lookup-by-address on page /stores/list');
    }
    $submitNode->click();
    $this->getMainContext()->iWaitForMilliseconds('5000');
  }

  /**
   * @Given /^That my desktop recipe box is empty$/
   * @Given /^that my desktop recipe box is empty$/
   *
   * Removes all recipes from a user's recipe box.
   * Assumes the user is logged in.
   * Will leave the session on /user/recipe_box
   */
  public function thatMyDesktopRecipeBoxIsEmpty() {

    // Are we on the recipe box page already?
    if (strpos($this->getSession()
        ->getCurrentUrl(), "user/recipe_box") === FALSE
    ) {
      // We're not on the recipe box page so we need to go there
      $this->getSession()->visit($this->getMainContext()
        ->locatePath("/user/recipe_box"));
    }

    // Verify the page has loaded
    $this->getMainContext()->waitForCssSelectorToBeAvailable('.content h3');

    // Get list of all "remove" links on recipes, if any
    $region_element = $this->getRegion(self::REGION_CONTENT);
    $remove_recipe_links = $region_element->findAll('css', 'li .remove_link a');
    if (is_array($remove_recipe_links) && count($remove_recipe_links) > 0) {
      // Remove first link from the list
      $remove_recipe_link = reset($remove_recipe_links);
      $remove_recipe_link->click();

      // Call ourselves recursively since the page refreshes (and our DOM references are invalidated)
      // This also allows us to verify the page has finished loading after a remove link is clicked
      $this->thatMyDesktopRecipeBoxIsEmpty();
    }
  }

  /**
   * @Given /^I select a random store$/
   */
  public function iSelectARandomStore() {
    $page_element = $this->getSession()->getPage();
    $make_this_my_store_buttons = $page_element->findAll('css', '.set_store input[type="submit"]');
    $clickable_mtmsbs = array();
    foreach ($make_this_my_store_buttons as $mtmsb) {
      if ($mtmsb->getValue() == 'Make This My Store') {
        $clickable_mtmsbs[] = $mtmsb;
      }
    }

    $random_index = array_rand($clickable_mtmsbs);
    $mtmsb = $clickable_mtmsbs[$random_index];

    if (empty($mtmsb)) {
      throw new Exception('Unable to find button with .set_store input[type="submit"]" and value of "Make This My Store" selector');
    }

    $torn_pod_content_node = $mtmsb->getParent()
      ->getParent()
      ->getParent()
      ->getParent();
    $store_name_link = $torn_pod_content_node->find('css', '.views-field-title .field-content a');

    if (empty($store_name_link)) {
      throw new Exception("Unable to find store name link with css selector .views-field-title .field-content a");
    }
    $store_name = $store_name_link->getText();
    $this->getMainContext()->selected_store_name = $store_name_link->getText();

    $mtmsb->click();
  }

  /**
   * @Then /^I should see the selected store name in the "([^"]*)" region$/
   */
  public function iShouldSeeTheSelectedStoreNameInTheRegion($region) {
    $this->getMainContext()->spin(function ($context) use ($region) {
      $region_node = $context->getRegion($region);
      $store_in_header = $region_node->find('css', '#block-wfm-welcome-block-wfm-welcome-block .content .welcome-box .store-box a');

      if (empty($store_in_header)) {
        throw new Exception('Unable to locate store name container in the . "header" . region identified by "#block-wfm-welcome-block-wfm-welcome-block .content .welcome-box .store-box a"');
      }

      $store_name = $store_in_header->getText();
      if (stripos($store_name, $context->selected_store_name) === FALSE) {
        throw new Exception('Found store name container in the ' . $region . ' region but store names do not match. Found: ' . $store_name . ', expected ' . $context->selected_store_name);
      }
      return TRUE;
    });
  }

  /**
   * @Given /^there should be at least "([^"]*)" "([^"]*)" elements with display "([^"]*)" in the "([^"]*)" region$/
   */
  public function thereShouldBeAtLeastElementsWithDisplayInTheRegion($min, $selector, $block_state, $region) {
    // Check if the region exists
    $element = $this->getRegion($region);

    // Do any elements called $selector exist in our region
    $find = $element->findall('css', $selector);
    if (empty($find)) {
      throw new Exception("No elements of " . $selector . " were found on the page.");
    }

    // use jQuery to count display:$block_state elements
    $counter = $this->getSession()->getDriver()->evaluateScript(
      "return jQuery('.views-row').css('display','$block_state').size();"
    );

    if ($counter < $min) {
      throw new Exception("Expected " . $count . " elements, but found " . $counter);
    }
  }

  /**
   * @Then /^I should see at least "([^"]*)" "([^"]*)" elements in the "([^"]*)" container$/
   */
  public function thereShouldBeAtLeastElementsInTheContainer($min, $element_selector, $container_selector) {
    $container = $this->getSession()
      ->getPage()
      ->find('css', $container_selector);
    if (empty($container)) {
      throw new Exception("Did not find element with css matching " . $container_selector);
    }
    $find = $container->findall('css', $element_selector);
    if (empty($find)) {
      throw new Exception("No elements of " . $element_selector . " were found on the page.");
    }
    $counter = count($find);
    if ($counter < $min) {
      throw new Exception('Expected at least ' . $min . ' elements, but found ' . $counter);
    }
  }


  /**
   * @When /^I view each enabled store I should see at least "([^"]*)" sales items identified by "([^"]*)"$/
   */
  public function iViewEachEnabledStoreIShouldSeeAtLeastSalesItemsIdentifiedBy($min_num_sales_items, $selector) {
    if ($enabled_store_ids = $this->getEnabledStoreIds()) {
      echo 'Verify enabled stores have sales flyer items - STARTING' . "\n";
      echo 'Found ' . count($enabled_store_ids) . ' enabled store ids: ' . implode(', ', $enabled_store_ids) . "\n";
      foreach ($enabled_store_ids as $store_id) {
        if ((!empty($store_id) && $store_id != 'nid')) {
          $this->hasMinSalesItems($store_id, $min_num_sales_items, $selector);
        }
      }
      if (count($this->store_ids_no_sales_items) > 0) {
        $this->sendNoSalesFlyerReportEmail();
      }
      else {
        $this->sendAllStoresHaveSalesFlyersEmail();
      }
      echo 'Verify enabled stores have sales flyer items - COMPLETE' . "\n";
      return;
    }

    throw new Exception("Unable to load enabled store ids.");
  }

  private function getCurrentUrl() {
    $current_url = $this->getSession()->getCurrentUrl();

    //drop the querystring if it has one
    if (strpos($current_url, '?') !== FALSE) {
      $current_url = substr($current_url, 0, strpos($current_url, '?'));
    }
    return $current_url;
  }

  private function hasMinSalesItems($store_id, $min_num_sales_items, $selector) {
    if (is_numeric($store_id)) {
      $current_url = $this->getCurrentUrl();

      $this->getSession()->visit($current_url . '?store=' . $store_id);

      $page = $this->getSession()->getPage();
      $elements = $page->findAll('css', $selector);

      //page layout changed late Jan 2015, no longer using region
      //$region_node = $this->getRegion($region);
      //$elements = $region_node->findAll('css', $selector);

      $message = "Store id " . $store_id . " has sales items" . "\n";

      if (empty($elements) || count($elements) < $min_num_sales_items) {
        $this->store_ids_no_sales_items[] = $store_id;
        $message = "Store id " . $store_id . " DOES NOT have sales items" . "\n";
      }

      echo $message;
    }
  }

  private function sendAllStoresHaveSalesFlyersEmail() {
    $current_url = $this->getSession()->getCurrentUrl();
    $current_url = preg_replace('/[0-9]+/', '', $current_url);
    $to = "bobby.hawley@wholefoods.com,eileen.genevro@wholefoods.com";
    $from = "noreply@wholefoods.com";
    $headers = "From: " . $from . "\n";
    $subject = "No Stores Missing Sales Flyers :)";
    $message = "Hurray! All stores have sales flyers.";

    mail($to, $subject, $message, $headers);
  }

  private function sendNoSalesFlyerReportEmail() {
    $current_url = $this->getSession()->getCurrentUrl();
    $current_url = preg_replace('/[0-9]+/', '', $current_url);
    $to = "itim@wholefoods.com,bobby.hawley@wholefoods.com,eileen.genevro@wholefoods.com";
    $from = "noreply@wholefoods.com";
    $headers = "From: " . $from . "\n";
    $subject = "Store(s) Missing Sales Flyers";
    $message = "";

    if (count($this->store_ids_no_sales_items) > 0) {
      $message = "The following stores do not have any sales items: \n\n";
      foreach ($this->store_ids_no_sales_items as $store_id) {
        $message .= $current_url . $store_id . "\n";
      }
    }

    mail($to, $subject, $message, $headers);
  }

  private function getEnabledStoreIds() {
    $query = "\"SELECT nid FROM node AS n
                INNER JOIN field_data_field_opening_soon AS os ON n.nid = os.entity_id
                WHERE n.type='store'
                AND n.status = 1
                AND os.field_opening_soon_value = 0
                AND n.title NOT LIKE '%CLOSED%'
                ORDER BY n.nid\"";

    $enabled_store_ids = $this->getMainContext()
      ->getDriver()
      ->drush('sql-query', array($query));

    if (!empty($enabled_store_ids)) {
      $enabled_store_ids = explode("\n", $enabled_store_ids);
      $size = count($enabled_store_ids);
      for ($i = 0; $i < $size; $i++) {
        //clean off cruft returned from drush sql-query
        $store_id = trim($enabled_store_ids[$i]);
        if (!is_numeric($store_id)) {
          unset($enabled_store_ids[$i]);
        }
      }

      return $enabled_store_ids;
    }
    return FALSE;
  }

  /**
   * @Then /^I should see coupons in the "([^"]*)" region$/
   */
  public function iShouldSeeCouponsInTheRegion($region) {
    $region = $this->getRegion($region);
    $coupons = $region->findAll('css', '.view-id-coupons .views-row');
    if (count($coupons) <= 3) {
      throw new Exception('There are three or fewer coupons.');
    }
  }

  /**
   * @Given /^I should see sales items in the "([^"]*)" region$/
   */
  public function iShouldSeeSalesItemsInTheRegion($region_name) {
    $region = $this->getRegion($region_name);
    $sales = $region->findAll('css', '.view-id-sales_items .views-row');
    if (count($sales) <= 3) {
      throw new Exception('There are three or fewer sales items.');
    }
  }


  /**
   * @Then /^I press the escape key$/
   */
  public function iPressTheEscapeKey() {
    $key = '\e';
    $script = "jQuery.event.trigger({ type : 'keypress', which : '" . $key . "' });";
    $this->getSession()->evaluateScript($script);
  }

  /**
   * @Then /^I print the coupons$/
   */
  public function iPrintTheCoupons() {
    $button_selector = self::COUPONS_PRINT_BUTTON_SELECTOR;
    $button = $this->getSession()->getPage()->find('css', $button_selector);
    if (empty($button)) {
      throw new Exception("Unable to find text 'Print Selected' on page.");
    }
    print "\n BEFORE BUTTON CLICK \n";
    $button->click();
    print "\n AFTER BUTTON CLICK \n";
    $this->iPressTheEscapeKey();
  }

  /**
   * @Then /^Nuke the qualtrix popup$/
   *
   * This will close the qualtrix popup, which says “turn your phone into a money saving machine.”
   *
   */
  public function nukeTheQualtrixPopup() {
    $session = $this->getSession();
    $session->executeScript("$('#body > div.QSISlider.SI_diZeAbai6Cu1OoR_SliderContainer > div:nth-child(2) > div > img').click();");

  }

  /**
   * @Given /^I select store page (\d+)$/
   */
  public function iSelectStorePage($arg1) {
    $container = $this->getSession()->getPage()->find('css', '.pager-item');
    $text = "Go to page " . $arg1;

    //Grab all the pager items to try to find "Go to page x."
    $pages = $this->getSession()->getPage()->findAll('css', '.pager-item');
    if (count($pages) > 1) {
      foreach ($pages as $page) {
        $link = $page->findLink($text);
        if ($link) {
          $link->click();
          break;
        }
      }
    }
    $this->getMainContext()->iWaitForMilliseconds('3000');
  }

  /**
   * @Then /^the dropdown "([^"]*)" shows "([^"]*)"$/
   */
  public function theDropdownShows($container, $expected_contents) {
      $session = $this->getSession();
      $element = $session->getPage()->find('css', $container);
      if (empty($element)) {
        throw new Exception("Unable to find container: " . $container);
      }

      $script = "return jQuery('$container').val();";
      $contents = $session->evaluateScript($script);
      if ($contents !== $expected_contents) {
        if (empty($contents)) {
          $contents = "null";
        }
        throw new Exception("Expected: $expected_contents but got: $contents from container: " . $container);
      }
    }
}
// end of file.
