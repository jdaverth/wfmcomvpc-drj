<?php
use Behat\Behat\Context\ClosuredContextInterface;
use Behat\Behat\Context\TranslatedContextInterface;
use Behat\Behat\Context\BehatContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Drupal\Component\Utility\Random;
use Behat\Mink\Element\NodeElement;
use Behat\Behat\Context\Step\Then;
use WebDriver\Key;

// Require 3rd-party libraries here:
// require_once 'PHPUnit/Autoload.php';

/**
 * Features context.
 */
class FeatureContext extends Drupal\DrupalExtension\Context\DrupalContext {
  /* Associated with email:
   * wfmjanraintest@hotmail.com
   *PW: EATy0urVEGGIE$
   *Janrain user ID
   */
  const TEST_JANRAIN_UUID = '7c152da3-8e51-4b31-84db-9512f2751d40';
  const TEST_USER_NAME = 'WFM JanrainTest';
  const TEST_USER_PW = 'EATy0urVEGGIE$';
  const TEST_USER_EMAIL = 'wfmjanraintest@hotmail.com';
  const STATES = 'Alabama,Alaska,Arizona,Arkansas,California,Colorado,Connecticut,Delaware,District of Columbia,Florida,Georgia,Hawaii,Idaho,Illinois,Indiana,Iowa,Kansas,Kentucky,Louisiana,Maine,Maryland,Massachusetts,Michigan,Minnesota,Mississippi,Missouri,Montana,Nebraska,Nevada,New Hampshire,New Jersey,New Mexico,New York,North Carolina,North Dakota,Ohio,Oklahoma,Oregon,Pennsylvania,Rhode Island,South Carolina,South Dakota,Tennessee,Texas,Utah,Vermont,Virginia,Washington,West Virginia,Wisconsin';
  const CHECKED = "checked";
  const UNCHECKED = "unchecked";
  const LOGIN_URL = 'https://users.wholefoodsmarket.com/oauth/signin';

  // Used for maintaining state between lines of Gherkin tests.
  public $selected_url;

  public $selected_text;

  public $found_link_text = FALSE;

  public $return = FALSE;

  public $selected_store_name = FALSE;

  public $selected_region = FALSE;

  // Array used to store data for later verification between steps.
  public $memorized_data = [];

  /**
   * Initializes context.
   *
   * Every scenario gets its own context object.
   *
   * @param array $parameters
   *    Context parameters (set them up through behat.yml).
   */
  public function __construct(array $parameters) {
    // Initialize your context here.
    $this->useContext('mobile', new MobileSubContext(array()));
    $this->useContext('desktop', new DesktopSubContext(array()));
    $this->useContext('eStoreDesktop', new eStoreDesktopSubContext(array()));
    $this->useContext('BrandCampDesktop', new BrandCampDesktopSubContext(array()));
  }

  /*.*.*.*.*.*.*.*.*.*.*.*.*.*.* -- SPIN FUNCTIONS -- *.*.*.*.*.*.*.*.*.*.*.*.*/

  /**
   * Function that run the supplied lambda at most $wait times.
   *
   * @param string $lambda
   *    A function that throws exceptions or returns FALSE on failure.
   *    And returns some form of TRUE upon success.
   * @param int $wait
   *    The max number of lambda attempts to try.
   *
   * @return bool
   *    TRUE if successful.
   *
   * @throws Exception
   *    Forwards last exception thrown by lambda function.
   */
  public function spin($lambda, $wait = 20) {
    $exception_message = '';

    for ($i = 0; $i < $wait; $i++) {
      try {
        if ($lambda($this)) {
          return TRUE;
        }
      }
      catch (Exception $e) {
        $exception_message = $e->getMessage() . "\n";
      }
      sleep(1);
    }

    $backtrace = debug_backtrace();
    $exception_message .= "Timeout thrown by " . $backtrace[1]['class'] . "::" . $backtrace[1]['function'] . "()\n";
    if (array_key_exists('file', $backtrace[1])) {
      $exception_message .= $backtrace[1]['file'];
    }
    if (array_key_exists('line', $backtrace[1])) {
      $exception_message .= ", line " . $backtrace[1]['line'];
    }
    throw new Exception($exception_message);
  }

  public function spinGetRegion($region) {
    $this->spin(function($context) use ($region) {
      $context->selected_region = $context->getRegion($region);
      return TRUE;
    });
    return $this->selected_region;
  }

  /**
   * Replace Mink's setValue() with a spin version to see the control first.
   *
   * Note this currently only supports a CSS selector,
   * not the text on the control, etc.
   *
   * Example:
   *   $this->spinSetValue("#capture_signIn_traditionalSignIn_password", $this->user->pass);
   */
  public function spinSetValue($cssSelector, $value) {
    $this->spin(function ($context) use ($cssSelector, $value) {
      $page = $context->getSession()->getPage();
      $found = $page->find("css", $cssSelector);
      if (count($found) == 1) {
        $found->setValue($value);
        return TRUE;
      }
      else {
        return FALSE;
      }
    });
  }

  /**
   * Replace Mink's click() with a version that spins to get the control first.
   *
   * Note this currently only supports a CSS selector,
   * not the text on the control, etc.
   *
   * Example:
   *   $this->spinClick("#capture_signIn_traditionalSignIn_signInButton");
   */
  public function spinClick($cssSelector) {
    $this->spin(function ($context) use ($cssSelector) {
      $page = $context->getSession()->getPage();
      $found = $page->find("css", $cssSelector);
      if (count($found) == 1) {
        $found->click();
        return TRUE;
      }
      else {
        return FALSE;
      }
    });
  }

  /**
   * Waits for the specified CSS selector to be available on the current page.
   *
   * If you have timing issues you can pass in a CSS selector and this function
   * will wait for an element that matches the selector to be visible before
   * continuing.
   *
   * Examples:
   *   $this->waitForCssSelectorToBeAvailable("#my-button");
   *   $this->waitForCssSelectorToBeAvailable(".region-header #input_form");
   */
  public function waitForCssSelectorToBeAvailable($selector) {
    $this->spin(function ($context) use ($selector) {
      $page = $context->getSession()->getPage();
      $found = $page->find("css", $selector);
      return count($found) > 0;
    });
  }

  /*.*.*.*.*.*.*.*.*.*.*.*.*.*.* -- API FUNCTIONS -- *.*.*.*.*.*.*.*.*.*.*.*.*/

  /**
   * @Given /^That the WFM API is Dev$/
   */
  public function thatTheWfmApiIsDev() {
    $this->getDriver()->drush('vset wfm_variable__wfmapi_host api.wholefoodsmarket.com:1800 --yes');
  }

  /**
   * @Given /^That the WFM API is Prod$/
   */
  public function thatTheWfmApiIsProd() {
    $this->getDriver()->drush('vset wfm_variable__wfmapi_host api.wholefoodsmarket.com --yes');
  }

  /*.*.*.*.*.*.*.*.*.*.*.*.*.*.* -- HELPER/DEBUGGER FUNCTIONS -- *.*.*.*.*.*.*/

  /**
   * @Given /^I am not on production$/
   * @Given /^I am not on prod$/
   */
  public function iAmNotOnProduction() {
    $url = $this->getSession()->getCurrentUrl();
    if (strpos($url, "://www.")) {
      throw new Exception("Warning: url contains '://www.'. This is the production environment and we will not run transaction tests on the production environment.");
    }
  }

  /**
   * @Then /^I should output the current URL$/
   *
   * Helper method for test debugging.
   */
  public function iShouldOutputTheCurrentUrl() {
    $browser_url = $this->getSession()->getCurrentUrl();
    print("\n******* Current URL = $browser_url\n\n");
  }

  /**
   * @Given /^I wait for "([^"]*)" milliseconds$/
   *
   * This can only be called by scenarios with @javascript tag
   */
  public function iWaitForMilliseconds($millis) {
    $this->getSession()->wait($millis);
  }

  /**
   * @Given /^I wait for "([^"]*)" seconds$/
   *
   * This can be called by scenarios with @nojs tag
   */
  public function iWaitForSeconds($seconds) {
    if (is_numeric($seconds)) {
      if ($seconds > 0) {
        sleep($seconds);
      }
      else {
        throw new Exception("Error invalid number of seconds specified.");
      }
    }
  }

  /**
   * @Then /^I switch to window "([^"]*)"$/
   */
  public function iSwitchToWindow($window_name) {
    $this->getSession()->wait(3000);
    $this->getSession()->switchToWindow($window_name);
  }

  /**
   * @Then /^I close window$/
   */
  public function iCloseWindow() {
    $this->getSession()->executeScript('window.close();');
  }

  /**
   * @Then /^restart session$/
   */
  public function restartSession() {
    $this->getSession()->restart();
  }

  /**
   * @Then /^stop session$/
   */
  public function stopSession() {
    $this->getSession()->stop();
  }

  /**
   * Does the current URL match the given original URL?
   *
   * @param string $original_url
   *    The URL to compare against the current URL.
   *
   * @return bool
   *    TRUE if the current URL and original URL match, false otherwise.
   */
  public function areOnOriginalUrl($original_url) {
    // Check the original URL for validity.
    if (strpos($original_url, "http") === FALSE) {
      return FALSE;
    }

    // If the original url was "signin" then the login is considered true.
    if (strpos($original_url, "/signin") !== FALSE) {
      return TRUE;
    }

    // We have a valid original URL, let's compare it to the current URL.
    if ($original_url === $this->getSession()->getCurrentUrl()) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Returns '.' or "#".
   */
  private function idOrClassSeparator($id_or_class) {
    $separator = '#';
    if ($id_or_class == 'class') {
      $separator = '.';
    }
    return $separator;
  }

  /**
   * @Given /^I wait for ajax calls to finish$/
   */
  public function iWaitForAjaxCallsToFinish() {
    $this->getSession()->wait(5000, '(0 === jQuery.active)');
  }

  /**
   * @Then /^I should be on a page with "([^"]*)" in the url$/
   */
  public function iShouldBeOnAPageWithInTheUrl($segment) {
    $url = $this->getSession()->getCurrentUrl();
    $position = strpos($url, $segment);
    if ($position === FALSE) {
      throw new Exception($segment . " not found in " . $url);
    }
  }

  /*.*.*.*.*.*.*.*.*.*.* -- JAVASCRIPT/JQUERY FUNCTIONS -- *.*.*.*.*.*.*.*.*.*/

  /**
   * @Given /^via Javascript I enter "([^"]*)" for "([^"]*)"$/
   * This one is to get around "invisible" form elements that are indeed visible
   */
  public function viaJavascriptIEnterFor($arg1, $arg2) {
    $script = 'jQuery("' . $arg2 . '").attr("value", "' . $arg1 . '");';
    $this->getSession()->executeScript($script);
  }

  /**
   * @When /^I submit the "([^"]*)" form via Javascript$/
   * This one is to get around "invisible" form elements that are indeed visible
   */
  public function iSubmitTheFormViaJavascript($jquery_selector) {
    $script = 'jQuery("' . $jquery_selector . '").submit();';
    $this->getSession()->executeScript($script);
    $this->getSession()->wait(5000, '(0 === jQuery.active)');
  }

  /**
   * @When /^I click "([^"]*)" via Javascript$/
   */
  public function iClickViaJavascript($jquery_selector) {
    $script = 'jQuery("' . $jquery_selector . '").click();';
    $this->getSession()->executeScript($script);
    $this->getSession()->wait(5000, '(0 === jQuery.active)');
  }

  /**
   * @Then /^I show the "([^"]*)" container$/
   */
  public function iShowInTheContainer($container) {
    $session = $this->getSession();
    $element = $session->getPage()->find('css', $container);
    if (empty($element)) {
      throw new Exception("Unable to find container: " . $container);
    }
    $script = "jQuery('$container').css('height', '120px');";
    $this->getSession()->executeScript($script);
  }

  /*.*.*.*.*.*.*.*.*.*.*.*.*.*.* -- LOG IN FUNCTIONS -- *.*.*.*.*.*.*.*.*.*.*.*/

  /**
   * Authenticates a test user.
   *
   * @Given /^I am logged in as a test user$/
   */
  public function iAmLoggedInAsATestUser() {
    $this->getSession()->visit('user/janrain_user');
  }

  /**
   * @Then /^I login as a test user$/
   */
  public function iLoginAsATestUser() {
    // Create user (and project)
    $user = (object) array(
      'name' => self::TEST_USER_EMAIL,
      'mail' => self::TEST_USER_EMAIL,
      'pass' => self::TEST_USER_PW,
      'role' => 'authenticated user',
    );

    /* If we ever use the Drupal extension to create new users, etc. we need
     * to save the user in the $this->users array.  Not needed now.
     *$this->users[$user->name] = $user;
     */

    /* Remember the user.  Not currently used, but needed if we use WFMAPI for
     * some tests.  Also, we override the Drupal Extension login() method which
     * relies on this.  Not sure if it is better to just create our own
     * janrainLogin() method that doesn't rely on this or continue to stick with
     * the override.
     */
    $this->user = $user;

    // Do the login.
    $this->login();
  }

  /**
   * Uses a janrain login page rather than Drupal's default.
   *
   * Overrides DrupalContext's login method.
   * Login to the site.  Note we use the /signin page to do this instead of the
   * popup login method
   * (The popup login functionality should be tested elsewhere).
   *
   * Note this leaves you on the /signin page.
   *
   * @param array $new_user
   *    Optional array that has assignments for keys named "mail" and "pass".
   *        ex. ['mail'=>'me@aol.com", 'pass'=>'1234', 'stuff1'=> 'something',
   *             'stuff2'=> 'something else'].
   */
  public function login($new_user = NULL) {
    // If $new_user is passed in, use its login values.
    if ($new_user) {
      $this->user = (object) $new_user;
    }

    // Remember the current URL so we can return to it later.
    $original_url = $this->getSession()->getCurrentUrl();

    /* Go to the signin page.
     * Use locatePath() to get the proper protocol and URL for the base site.
     * as defined in the config files.
     */
    if (strpos($original_url, "/signin") === FALSE) {
      $this->getSession()->visit($this->locatePath("/signin"));
    }

    // Fill in and submit the login form.
    $this->spinSetValue("#capture_signIn_traditionalSignIn_password", $this->user->pass);
    $this->spinSetValue("#capture_signIn_traditionalSignIn_emailAddress", $this->user->mail);
    $this->spinClick("#capture_signIn_traditionalSignIn_signInButton");

    // Wait for the profile edits "Save" button to be visible.
    // This ensures the post-login page is fully loaded.
    $this->waitForCssSelectorToBeAvailable("#capture_editProfile_saveButton");

    /* If the current URL is valid and not the same as what we are on now after
     * the login, go back to that URL.  If the test had not specified a URL
     * prior to logging in then we just stay on the post-login screen.
     */
    if ($new_user == NULL) {
      if (!$this->areOnOriginalUrl($original_url)) {
        $this->getSession()->visit($original_url);
      }
    }
  }

  /**
   * @Then /^I should see test username in the "([^"]*)" region$/
   */
  public function iShouldSeeTestUsernameInTheRegion($region) {
    $this->loggedIn($region);
  }

  /**
   * Determine if the a user is already logged in.
   */
  public function loggedIn($region = 'header') {

    $is_logged_in = FALSE;

    if (!$this->getSession()->getPage()->find('region', $region)) {
      return $is_logged_in;
    }

    $region_element = $this->getRegion($region);
    $header_text = $region_element->getText();

    if (stripos($header_text, "Welcome, " . self::TEST_USER_NAME) !== FALSE) {
      $is_logged_in = TRUE;
    }

    return $is_logged_in;
  }

  /*.*.*.*.*.*.*.*.*.*.*.*.*.*.* -- STORE SELECT FUNCTIONS -- *.*.*.*.*.*.*.*.*/

  /**
   * @Given /^I have no store selected$/
   */
  public function iHaveNoStoreSelected() {
    // Delete cookie:
    $this->getSession()->setCookie('local_store', NULL);
    $this->getSession()->setCookie('local_store_id', NULL);
    $this->getSession()->setCookie('local_store_name', NULL);
    $this->getSession()->setCookie('local_store_url', NULL);
  }

  /**
   * Helper function to load a random state to fill in forms.
   */
  public function getRandomState() {
    $states = explode(',', self::STATES);
    $index = array_rand($states);
    return $states[$index];
  }

  /**
   * @Given /^"([^"]*)" is my home store$/
   *
   * Sets your home store based on the store name you provide.  Note the store
   * name is used to search on /stores/list and it will select the top hit based
   * on the store name.
   *
   * @param string $storeName
   *   The store to be selected.
   *
   * @throws Exception
   *    Store name could not be used.
   */
  public function isMyHomeStore($storeName) {
    // Remember the current URL so we can return to it later.
    $original_url = $this->getSession()->getCurrentUrl();

    // Go to /stores/list.
    $this->getSession()->visit($this->locatePath("/stores/list"));

    // Fill in search box with store name.
    $this->spin(function ($context) use ($storeName) {
      try {
        $context->fillField("address", $storeName);
      }
      catch (Exception $e) {
        throw new Exception("Could not fill field address with " . $storeName);
      }
      return TRUE;
    });

    // Click the search button to get search results.
    $this->spin(function ($context) {
      try {
        $context->pressButton("edit-submit-store-lookup-by-address");
      }
      catch (Exception $e) {
        throw new Exception("Could not press button edit-submit-store-lookup-by-address");
      }
      return TRUE;
    });

    /* Verify we get our chosen store as the top result
     * Without this we sometimes get the wrong store selected because the
     * next section clicks the first make this my store button but there
     * are already stores displayed by default.  So we need to wait for our
     * search to finish to ensure we select the correct store.
     */
    $this->spin(function ($context) use ($storeName) {
      $context->assertElementContainsText(".view-content h4 a", $storeName);
      return TRUE;
    });

    // Click "make this my store" link for top search result.
    $this->spin(function ($context) {
      $context->pressButton("Make This My Store");
      return TRUE;
    });

    /* Wait for the Ajax call to finish that actually sets the store.
     * Without this the next test step could leave the page before this call
     * finishes and the home store would not get set.
     */
    $this->getSession()->wait(5000, '(0 === jQuery.active)');

    /* If the current URL is valid and not the same as what we are on now after
     * setting the home store, go back to that URL.  If the test had not
     * specified a URL prior to logging in then we just stay where we are.
     */
    if (strpos($original_url, "http") !== FALSE && $original_url !== $this->getSession()->getCurrentUrl()) {
      $this->getSession()->visit($original_url);
    }
  }

  /**
   * @Given /^my selected store is "([^"]*)"$/
   */
  public function mySelectedStoreIs($store_name) {
    if (strlen($store_name) !== 3) {
      throw new Exception('Use the store three-letter code with this function!');
    }
    $store = $this->getStoreInfo($store_name);

    $this->getSession()->setCookie('local_store', $store->nid);
    $this->getSession()->setCookie('local_store_id', $store->tlc);
    $this->getSession()->setCookie('local_store_name', $store->storename);
    $this->getSession()->setCookie('local_store_url', '/stores/' . $store->path);
  }

  /**
   * Get store info from request to wholefoodsmarket.com .
   *
   * @param string $tlc
   *   Store three-letter-code.
   *
   * @return object
   *   json-decoded store object.
   *
   * @todo This isn't working.
   *       Inexplicably cannot access properties of $nid object in foreach(),
   *       even though returned json is valid in linters.
   */
  protected function getStoreInfo($tlc) {
    $url = "http://www.wholefoodsmarket.com/ajax/stores/" . $tlc;
    $response = file_get_contents($url);
    if (!$response) {
      throw new Exception("Something is wrong with the request to " . $url);
    }
    $storeobj = json_decode($response);
    foreach ($storeobj as $nid => $obj) {
      return $obj;
    }
  }

  /*.*.*.*.*.*.*.*.*.*.*.*.*.*.* -- IMAGE FUNCTIONS -- *.*.*.*.*.*.*.*.*.*.*.*/

  /**
   * @Then /^I should see an image in the "([^"]*)" container$/
   * @Then /^I should see an image in container "([^"]*)"$/
   * @Then /^I should see an image "([^"]*)"$/
   */
  public function iShouldSeeAnImageInContainer($container_node_or_name) {

    // If a container node is passed in, use it directly.
    $container = $container_node_or_name;

    // If a container selector string is supplied, look for the container node.
    if (is_string($container_node_or_name)) {
      $container = $this->getSession()->getPage()->find('css', $container_node_or_name);
      if (empty($container)) {
        throw new Exception("Unable to find container: " . $container_node_or_name);
      }
    }

    $img_node = $container->find('css', 'img');
    if (!$img_node->hasAttribute('src')) {
      throw new Exception("No image tag found in container: " . $container_node_or_name);
    }
    $img_src = $img_node->getAttribute('src');
    if (empty($img_src)) {
      throw new Exception("Unable to find src for image in: " . $container_node_or_name);
    }
  }

  /**
   * @Then /^I should see an image in container "([^"]*)" in the "([^"]*)" region$/
   * @Then /^I should see an image in the "([^"]*)" container in the "([^"]*)" region$/
   */
  public function iShouldSeeAnImageInContainerInTheRegion($selector, $region) {
    $container_node = $this->containerInRegion($selector, $region);
    $this->iShouldSeeAnImageInContainer($container_node);
  }

  /**
   * @Then /^I should see the image link with src "([^"]*)" with href "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeTheImageLinkWithSrcWithHrefInTheRegion($image_src, $link_href, $region) {
    $region_element = $this->getRegion($region);

    $link_elements = $region_element->findAll('css', 'a');
    $found_link_href = FALSE;
    $found_img_src = FALSE;
    foreach ($link_elements as $link_element) {
      if ($link_element->hasAttribute('href')) {
        $current_href = $link_element->getAttribute('href');
        if (strpos($current_href, $link_href) !== FALSE) {
          $found_link_href = TRUE;
        }

        $img_tag = $link_element->getHtml();
        if (strpos($img_tag, $image_src) !== FALSE) {
          $found_img_src = TRUE;
        }
      }
    }

    if (!$found_link_href) {
      throw new Exception('Unable to locate link with href fragment: ' . $link_href . ' in the ' . $region . ' region');
    }

    if (!$found_img_src) {
      throw new Exception('Unable to locate image with src fragment: ' . $image_src . ' in the ' . $region . ' region');
    }
  }

  /**
   * Validates that an image exists.
   *
   * This function breaks while pointing at prod.
   * Perhaps permissions is denied.
   */
  private function curl_image($img_src) {
    $handle = curl_init($img_src);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);
    /* Get the HTML or whatever is linked in $url. */
    $response = curl_exec($handle);
    /* Check for 404 (file not found). */
    $http_code = curl_getinfo($handle, CURLINFO_HTTP_CODE);
    if ($http_code != 200) {
      throw new Exception("image not found, http response " . $http_code);
    }
    curl_close($handle);
    return;
  }

  /**
   * @Then /^I should see an image in each "([^"]*)" element in the "([^"]*)" container$/
   */
  public function iShouldSeeAnImageInEachElementInTheContainer($listElement, $listContainer) {
    if (count($this->getSession()->getPage()->findAll('css', $listContainer)) > 1) {
      throw new Exception("Found more than one container named: " . $listContainer . ". Function should only receive id with no possible duplicates/copies on page.");
    }
    $container = $this->getSession()->getPage()->find('css', $listContainer);
    if (empty($container)) {
      throw new Exception("Unable to find container: " . $listContainer);
    }
    $list_nodes = $container->findAll('css', $listElement);
    if (count($list_nodes) == 0) {
      throw new Exception("Zero elements named: " . $listElement . " found in " . $listContainer);
    }
    for ($index = 0; $index < count($list_nodes); $index++) {
      $this->iShouldSeeAnImageInContainer($list_nodes[$index]);
    }
  }

  /*.*.*.*.*.*.*.*.*.*.*.* -- I SHOULD SEE THE TEXT/LINK/HREF -- *.*.*.*.*.*.*/

  /**
   * @Then /^I should see a link with href "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeALinkWithHrefInTheRegion($href, $region) {
    $region_node = $this->getRegion($region);
    $link_nodes = $region_node->findAll('css', 'a');
    $found_link_text = FALSE;
    foreach ($link_nodes as $link_node) {
      if (strpos($link_node->getAttribute('href'), $href) !== FALSE) {
        // Using strpos instead of '=='.
        // To ignore the potential presence of a querystring in the href.
        return;
      }
    }
    throw new Exception('Unable to locate link with href: ' . $href . ' in the ' . $region . ' region');
  }

  /**
   * @Then /^I should see the "([^"]*)" link with href "([^"]*)" in the "([^"]*)" container in the "([^"]*)" region$/
   */
  public function iShouldSeeTheLinkWithHrefInTheContainerInTheRegion($link_text, $href, $container_selector, $region) {
    $container = $this->containerInRegion($container_selector, $region);
    $this->iShouldSeeTheLinkWithHrefInTheRegion($link_text, $href, $container);
  }

  /**
   * @Then /^I should see the link "([^"]*)" with href "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeTheLinkWithHrefInTheRegion($link_text, $href, $region_name_or_container) {
    $parent_node = $region_name_or_container;
    if (is_string($region_name_or_container)) {
      $parent_node = $this->getRegion($region_name_or_container);
    }

    $link_nodes = $parent_node->findAll('css', 'a');
    $found_link_text = FALSE;
    foreach ($link_nodes as $link_node) {
      $text = $link_node->getText();
      if (empty($text)) {
        $text = trim(strip_tags($link_node->getHtml()));
      }

      /* Some spaces (c2a0) were not being interpreted as standard spaces.
       * Likely a cut and paste issue in content in Drupal for details see:
       * http://stackoverflow.com/questions/12837682/non-breaking-utf-8-0xc2a0-space-and-preg-replace-strange-behaviour
       * A non breaking space is U+00A0 (Unicode) but encoded as C2A0 in UTF-8.
       */
      $cleaned_link_text = preg_replace('/[\s\x{00a0}]+/u', '\s', $link_text);

      // Escape '/' with '\/'.
      $cleaned_link_text = preg_replace('~/~', '\\/', $cleaned_link_text);

      // Build regex used cleaned link.
      $regex = '/^' . $cleaned_link_text . '$/iu';

      // Case insensitive string comparison.
      if (strlen($text) > 0 && preg_match($regex, $text)) {
        $found_link_text = TRUE;
        break;
      }
      elseif (strpos($text, $link_text) !== FALSE) {
        $found_link_text = TRUE;
        break;
      }
    }
    if (!$found_link_text) {
      throw new Exception('Unable to locate link with text: ' . $link_text);
    }
    if (strpos($link_node->getAttribute('href'), $href) === FALSE) {
      // Doing strpos instead of "==".
      // To ignore the potential presence of a querystring in the href.
      throw new Exception('Unable to locate link with href: ' . $href);
    }
  }

  /**
   * @Then /^I should see the heading "([^"]*)" in the "([^"]*)" container$/
   */
  public function iShouldSeeTheHeadingInTheContainer($heading, $container_selector) {
    $container = $this->getSession()->getPage()->find('css', $container_selector);
    if (empty($container)) {
      throw new Exception("Unable to find container: " . $container_selector);
    }
    $heading_node = $container->find('css', '.view-footer h2');
    if (empty($heading_node)) {
      throw new Exception("Unable to locate an h2 heading in the " . $container_selector . " container.");
    }
    $heading_text = $heading_node->getText();
    if (strpos($heading_text, $heading) === FALSE) {
      throw new Exception("Header: " . $heading . " does not match header found: " . $heading_text);
    }
  }

  /**
   * @Then /^I should see the link "([^"]*)" in the "([^"]*)" container$/
   */
  public function iShouldSeeTheLinkInTheContainer($link_text, $container_selector) {
    $container = $this->getSession()->getPage()->find('css', $container_selector);
    if (empty($container)) {
      throw new Exception("Unable to find container: " . $container_selector);
    }
    $link_node = $container->findLink($link_text);
    if (empty($link_node)) {
      throw new Exception("Unable to find link with text: " . $link_text . " in container: " . $container_selector);
    }
  }

  /**
   * @Then /^I should see the link "([^"]*)" in the "([^"]*)" container with href "([^"]*)"$/
   */
  public function iShouldSeeTheLinkInTheContainerWithHref($link_text, $container_selector, $href) {
    $container = $this->getSession()->getPage()->find('css', $container_selector);
    if (empty($container)) {
      throw new Exception("Unable to find container: " . $container_selector);
    }
    $link_node = $container->findLink($link_text);
    if (empty($link_node)) {
      throw new Exception("Unable to find link with text: " . $link_text . " in container: " . $container_selector);
    }
    $address = $link_node->getAttribute('href');
    if (strpos($address, $href) === FALSE) {
      throw new Exception("Link address does not match href: " . $href);
    }
  }

  /**
   * @Then /^I should see the text "([^"]*)" in count "([^"]*)" of "([^"]*)" element in the "([^"]*)" container$/
   */
  public function iShouldSeeTheTextInCountOfElementInTheContainer($text, $elementCountInList, $listElement, $listContainer) {
    if (count($this->getSession()->getPage()->findAll('css', $listContainer)) > 1) {
      throw new Exception("Found more than one container named: " . $listContainer . ". Function should only receive id with no possible duplicates/copies on page.");
    }
    $container = $this->getSession()->getPage()->find('css', $listContainer);
    if (empty($container)) {
      throw new Exception("Unable to find container: " . $listContainer);
    }
    $list_nodes = $container->findAll('css', $listElement);
    if (count($list_nodes) == 0) {
      throw new Exception("Zero elements named: " . $listElement . " found in " . $listContainer);
    }
    $retrieved_text = $list_nodes[($elementCountInList - 1)]->getText();
    if (empty($retrieved_text)) {
      throw new Exception("Unable to find text: " . $text . " in count " . $elementCountInList . " of " . $listElement . " in the " . $listContainer . " container.");
    }
    $positive = strpos($retrieved_text, $text);
    if ($positive === FALSE) {
      throw new Exception("Text " . $text . " does not match text in container.");
    }
  }

  /**
   * @Then /^I should see the link "([^"]*)" with href "([^"]*)" in count "([^"]*)" of "([^"]*)" element in the "([^"]*)" container$/
   */
  public function iShouldSeeTheLinkWithHrefInCountOfElementInTheContainer($link_text, $href, $elementCountInList, $listElement, $listContainer) {
    if (count($this->getSession()->getPage()->findAll('css', $listContainer)) > 1) {
      throw new Exception("Found more than one container named: " . $listContainer . ". Function should only receive id with no possible duplicates/copies on page.");
    }
    $container = $this->getSession()->getPage()->find('css', $listContainer);
    if (empty($container)) {
      throw new Exception("Unable to find container: " . $listContainer);
    }
    $list_nodes = $container->findAll('css', $listElement);
    if (count($list_nodes) == 0) {
      throw new Exception("Zero elements named: " . $listElement . " found in " . $listContainer);
    }
    $link_node = $container->findLink($link_text);
    if (empty($link_node)) {
      throw new Exception("Unable to find link with text: " . $link_text . " in container: " . $listContainer);
    }
    $address = $link_node->getAttribute('href');
    $positive = strpos($address, $href);
    if ($positive === FALSE) {
      throw new Exception("Link address does not match href: " . $href);
    }
  }

  /**
   * @Then /^I should see the href "([^"]*)" in count "([^"]*)" of "([^"]*)" element in the "([^"]*)" container$/
   */
  public function iShouldSeeTheHrefInCountOfElementInTheContainer($href, $elementCountInList, $listElement, $listContainer) {
    if (count($this->getSession()->getPage()->findAll('css', $listContainer)) > 1) {
      throw new Exception("Found more than one container named: " . $listContainer . ". Function should only receive id with no possible duplicates/copies on page.");
    }
    $container = $this->getSession()->getPage()->find('css', $listContainer);
    if (empty($container)) {
      throw new Exception("Unable to find container: " . $listContainer);
    }
    $list_nodes = $container->findAll('css', $listElement);
    if (count($list_nodes) == 0) {
      throw new Exception("Zero elements named: " . $listElement . " found in " . $listContainer);
    }
    $count = $elementCountInList - 1;
    $element = $list_nodes[$count]->findAll('css', 'a');
    $index = 0;
    foreach ($element as $key => $item) {
      if ($item->getAttribute('href') == $href) {
        return;
      }
    }
    throw new Exception("Unable to find href " . $href);
  }

  /**
   * @Then /^I should see the "([^"]*)" link with href "([^"]*)" in the "([^"]*)" container in "([^"]*)"$/
   */
  public function iShouldSeeTheLinkWithHrefInTheContainerIn($link_text, $link_href, $podContainer, $blockRowContainer) {
    $region = $this->getSession()->getPage()->find('css', $blockRowContainer);
    if (empty($region)) {
      throw new Exception("Unable to find container: " . $blockRowContainer);
    }
    $block = $region->find('css', $podContainer);
    if (empty($block)) {
      throw new Exception("Unable to find container: " . $podContainer . " in container " . $blockRowContainer);
    }
    $link_node = $block->findLink($link_text);
    if (empty($link_node)) {
      throw new Exception("Unable to find link with text: " . $link_text . " in container " . $podContainer . " in container " . $blockRowContainer);
    }
    $link_address = $link_node->getAttribute('href');
    if (strpos($link_address, $link_href) === FALSE) {
      throw new Exception("Link address with text: " . $link_text . " does not match href: " . $link_href);
    }
  }

  /**
   * @Then /^I should see the link "([^"]*)" with href "([^"]*)"$/
   */
  public function iShouldSeeTheLinkWithHref($link_text, $link_href) {
    $link_node = $this->getSession()->getPage()->findLink($link_text);
    if (empty($link_node)) {
      throw new Exception("Unable to find link with text: " . $link_text);
    }
    $link_address = $link_node->getAttribute('href');
    if (strpos($link_address, $link_href) === FALSE) {
      throw new Exception("Link address $link_href expected, but found $link_address instead.");
    }
  }

  /**
   * @Then /^I should see the "([^"]*)" link with href "([^"]*)" in the "([^"]*)" container$/
   * @Then /^I should see the link "([^"]*)" with href "([^"]*)" in the "([^"]*)" container$/
   * @Then /^I should see a link "([^"]*)" with href "([^"]*)" in the "([^"]*)" container$/
   */
  public function iShouldSeeTheLinkWithHrefInTheContainer($link_text, $link_href, $container_selector) {
    $container = $this->getSession()->getPage()->find('css', $container_selector);
    if (empty($container)) {
      throw new Exception("Unable to find container: " . $container_selector);
    }
    $link_node = $container->findLink($link_text);
    if (empty($link_node)) {
      throw new Exception("Unable to find link with text: " . $link_text . " in container " . $container_selector);
    }
    $link_address = $link_node->getAttribute('href');
    if (strpos($link_address, $link_href) === FALSE) {
      throw new Exception("Link address, " . $link_text . ", does not match href: " . $link_href . " instead has href: " . $link_address);
    }
  }

  /**
   * @Then /^I should see a link with href "([^"]*)" in the "([^"]*)" container$/
   */
  public function iShouldSeeALinkWithHrefInTheContainer($href, $container_selector) {
    $container = $this->getSession()->getPage()->find('css', $container_selector);
    $link_nodes = $container->findAll('css', 'a');
    $found_link_text = FALSE;
    foreach ($link_nodes as $link_node) {
      if (strpos($link_node->getAttribute('href'), $href) !== FALSE) {
        // Doing strpos instead of "==" .
        // To ignore the potential presence of a querystring in the href.
        return;
      }
    }
  }

  /**
   * @Then /^I should see a link in the "([^"]*)" container in the "([^"]*)" region$/
   */
  public function iShouldSeeALinkInTheContainerInTheRegion($container_selector, $region) {
    $container = $this->containerInRegion($container_selector, $region);
    $link_node = $container->findLink('');
    if (empty($link_node)) {
      throw new Exception("Unable to find a link in container: " . $container_selector . ' in the ' . $region . ' region.');
    }
  }

  /**
   * @Then /^I should see the promo content in "([^"]*)" in "([^"]*)"$/
   */
  public function iShouldSeeThePromoContentInIn($promoContentContainerName, $blockName) {
    $block = $this->getSession()->getPage()->find('css', $blockName);
    if (empty($block)) {
      throw new Exception("Unable to find container: " . $blockName);
    }
    $container = $block->find('css', $promoContentContainerName);
    if (empty($container)) {
      throw new Exception("Unable to find promo content in : " . $promoContentContainerName . " in " . $blockName . "!!!");
    }
  }

  /**
   * @Then /^I should see the text "([^"]*)" in the "([^"]*)" container$/
   */
  public function iShouldSeeTheTextInTheContainer($text_to_find, $container_node_or_array_or_name) {
    $this->spin(function($context) use ($text_to_find, $container_node_or_array_or_name) {
      if (is_array($container_node_or_array_or_name)) {
        $found_text = FALSE;
        foreach ($container_node_or_array_or_name as $element) {
          $element_text = $element->getText();
          if (stripos($element_text, $text_to_find) !== FALSE) {
            $found_text = TRUE;
            break;
          }
        }
        if (!$found_text) {
          throw new Exception("Unable to find text in array: " . $text_to_find);
        }
        return TRUE;
      }

      $container = $container_node_or_array_or_name;

      // String selector to select a single element.
      if (is_string(($container_node_or_array_or_name))) {
        $container = $context->getSession()->getPage()->find('css', $container);
        if (empty($container)) {
          throw new Exception("Unable to find container: " . $container);
        }
      }

      $get_text = $container->getText();
      if (stripos($get_text, $text_to_find) === FALSE) {
        throw new Exception("Unable to find text: " . $text_to_find);
      }

      return TRUE;
    });
  }

  /**
   * @Then /^I should see the text "([^"]*)" in the "([^"]*)" container in the "([^"]*)" region$/
   */
  public function iShouldSeeTheTextInTheContainerInTheRegion($text, $container_selector, $region) {
    $container = $this->containerInRegion($container_selector, $region);
    $this->iShouldSeeTheTextInTheContainer($text, $container);
  }

  /**
   * @Then /^I should see a heading in the "([^"]*)" region$/
   */
  public function iShouldSeeAHeadingInTheRegion($region) {
    $region_node = $this->getRegion($region);
    $heading_node = $region_node->find('css', 'h2') ||
      $heading_node = $region_node->find('css', 'h3') ||
        $heading_node = $region_node->find('css', 'h4');
    if (empty($heading_node)) {
      throw new Exception("Unable to locate a heading (h2, h3, h4) in the " . $region . " region.");
    }
  }

  /*.*.*.*.*.*.*.*.*.*.*.*.*.*. -- AT LEAST FUNCTIONS -- *.*.*.*.*.*.*.*.*.*.*/

  /**
   * @Then /^the "([^"]*)" element should contain at least "([^"]*)" "([^"]*)" elements$/
   */
  public function theElementShouldContainAtLeastElements($parent_element_or_selector, $min_number_children, $child_elements_selector) {

    $container = $parent_element_or_selector;

    if (is_string($parent_element_or_selector)) {
      $element = $this->getSession()->getPage();
      $parent_containers = $element->findAll('css', $parent_element_or_selector);
      if (is_array($parent_containers) && count($parent_containers) > 1) {
        throw new Exception("More than one parent container found. Please supply an element selector that will return a single result.");
      }
      if (!($container = $element->find('css', $parent_element_or_selector))) {
        throw new Exception("Unable to find parent element " . $parent_element_or_selector);
      }
    }

    if (!($children = $container->findAll('css', $child_elements_selector))) {
      throw new Exception("Unable to find " . $child_elements_selector . " child elements");
    }
    $how_many_children = count($children);
    if ($how_many_children < $min_number_children) {
      throw new Exception("Insufficient child nodes found for parent element " . $parent_element_or_selector . ". " . $how_many_children . " were found, and " . $min_number_children . " are required.");
    }
  }

  /**
   * @Then /^the "([^"]*)" element should contain at least "([^"]*)" "([^"]*)" elements in the "([^"]*)" region$/
   */
  public function theElementShouldContainAtLeastElementsInTheRegion($parent_element, $min_number_children, $child_elements_selector, $region) {
    $this->getRegion($region);
    $this->theElementShouldContainAtLeastElements($parent_element, $min_number_children, $child_elements_selector);
  }

  /**
   * @Then /^I should see the text "([^"]*)" or at least "([^"]*)" elements identified by "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeTheTextOrAtLeastElementsIdentifiedByInTheRegion($text, $min_number_children, $child_elements_selector, $region) {
    $region_element = $this->getRegion($region);

    $region_text = $region_element->getText();

    if (stripos($region_text, $text) !== FALSE) {
      return;
    }

    $this->theElementShouldContainAtLeastElements($region_element, $min_number_children, $child_elements_selector);
  }

  /**
   * @Then /^I should see the text "([^"]*)" or at least "([^"]*)" elements identified by "([^"]*)" in the "([^"]*)" container$/
   */
  public function iShouldSeeTheTextOrAtLeastElementsIdentifiedByInTheContainer($text, $min_number_children, $child_elements_selector, $container_selector) {
    $container = $this->getSession()->getPage()->find('css', $container_selector);
    if (empty($container)) {
      throw new Exception("Unable to find container matching css " . $container_selector);
    }
    $container_text = $container->getText();
    if (stripos($container_text, $text) !== FALSE) {
      return;
    }
    $this->theElementShouldContainAtLeastElements($container, $min_number_children, $child_elements_selector);
  }

  /**
   * @Given /^I should see at least "([^"]*)" "([^"]*)"$/
   * @Then /^I should see at least "([^"]*)" "([^"]*)" elements$/
   */
  public function iShouldSeeAtLeast($count, $selector) {
    $this->spin(function($context) use ($count, $selector) {
      $actual = count($context->getSession()->getPage()->findAll('css', $selector));
      if ($actual < $count) {
        throw new Exception("Saw " . $actual . " elements and was expecting at least " . $count . " elements with selector : \'" . $selector . "\'");
      }
      return TRUE;
    });
  }

  /**
   * @Given /^I should see "([^"]*)" or more "([^"]*)" elements$/
   */
  public function iShouldSeeOrMoreElements($num, $element) {
    $container = $this->getSession()->getPage();
    $nodes = $container->findAll('css', $element);
    if (intval($num) > count($nodes)) {
      $message = sprintf('%d elements less than %s "%s" found on the page, but should be %d.', count($nodes), $element, $element, $num);
      throw new ExpectationException($message, $this->session);
    }
  }

  /**
   * @Then /^I should see the value "([^"]*)" times "([^"]*)" in the "([^"]*)" container$/
   */
  public function iShouldSeeTheValueTimesInTheContainer($quantity, $price, $container_selector) {
    $container = $this->getSession()->getPage()->find('css', $container_selector);
    if (empty($container)) {
      throw new Exception("Unable to find container " . $container_selector);
    }
    // I should probably grab the quantity and price.
    // Rather than passing them through the Gherkin...
    $calculated_subtotal = $quantity * $price;
    $pos = strpos($container->getText(), $calculated_subtotal);
    if (strpos($container->getText(), strval($calculated_subtotal)) === FALSE) {
      throw new Exception("Price incorrect. Expected price was " . $calculated_subtotal . ". But displayed price was " . $container->getText());
    }
  }

  /**
   * @Then /^there should be at least "([^"]*)" words in the "([^"]*)" container in the "([^"]*)" region$/
   */
  public function thereShouldBeAtLeastWordsInTheContainerInTheRegion($min_num_of_words, $selector, $region) {

    $container_element = $this->containerInRegion($selector, $region);
    $paragraph_text = $container_element->getText();

    $word_count = str_word_count($paragraph_text);

    if ($word_count < $min_num_of_words) {
      throw new Exception('Insuffient word count. At least ' . $min_num_of_words . ' expected. ' . $word_count . ' found. Current URL: ' . $this->getSession->getCurrentUrl());
    }
  }

  /*.*.*.*.*.*.*.*.*.*.*.*.*.*.* -- SEARCH FUNCTIONS -- *.*.*.*.*.*.*.*.*.*.*/

  /**
   * @Then /^I should see search results for "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeSearchResultsForInTheRegion($search_query, $region) {
    $region_node = $this->getRegion($region);
    $search_result_rows = $region_node->findAll('css', 'li.search-result');
    if (count($search_result_rows) == 0) {
      throw new Exception('Search results expected. No search results returned for search "' . $search_query . '"');
    }
  }

  /**
   * @Then /^I should see no search results for "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeNoSearchResultsForInTheRegion($search_query, $region) {
    $region_node = $this->getRegion($region);
    $search_result_rows = $region_node->findAll('css', '.search_result_row');
    $how_many_results = count($search_result_rows);
    if ($how_many_results > 0) {
      throw new Exception('No search results expected. ' . $how_many_results . ' search results returned for search "' . $search_query . '"');
    }
  }

  /**
   * @Then /^I should see Pluck Ratings$/
   */
  public function iShouldSeePluckRatings() {
    $element = $this->getSession()->getPage();
    if (!$element->find('css', '.plck-app-container-loaded')) {
      throw new Exception('Pluck recipe ratings not loaded');
    }
  }

  /**
   * @Then /^the url and querystring should match "([^"]*)"$/
   */
  public function theUrlAndQuerystringShouldMatch($url) {
    $browser_url = $this->getSession()->getCurrentUrl();
    if (strpos($browser_url, $url) === FALSE) {
      throw new Exception('Browser URL "' . $browser_url . '" does not match test URL "' . $url . '"');
    }
  }

  /**
   * @Then /^I should see "([^"]*)" search results identified by "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeSearchResultsIdentifiedByInTheRegion($number_visible_search_results, $selector, $region) {
    $region_element = $this->getRegion($region);
    $results = $region_element->findAll('css', $selector);

    if (count($results) == 0) {
      throw new Exception('Search results expected. No search results returned.');
    }

    if (count($results) > $number_visible_search_results) {
      throw new Exception($number_visible_search_results . ' search results expected. ' . count($results) . ' search results returned.');
    }

    if (count($results) > $number_visible_search_results) {
      throw new Exception($number_visible_search_results . ' search results expected. ' . count($results) . ' search results returned.');
    }
  }

  /*.*.*.*.*.*.*.*.*.*.*.*.*.*.* -- RECIPE BOX FUNCTIONS -- *.*.*.*.*.*.*.*.*/

  /**
   * Empty the Recipe Box.
   *
   * Only works if using the Drupal API driver which requires
   * that tests on the same server as the code base is on.
   *
   * @Given /^That My Recipe Box is Empty$/
   */
  public function thatMyRecipeBoxIsEmpty() {
    global $user;
    if ($lists = $this->getUserRecipes()) {
      foreach ($lists as $list) {
        WFMApiRecipeList::deleteItem($user->uid, $list['node']->nid);
      }
    }
  }

  /**
   * Assertion that the Recipe Box is empty.
   *
   * Only works if using the Drupal API driver which requires
   * that tests on the same server as the code base is on.
   *
   * @Then /^my recipe box should be empty$/
   */
  public function myRecipeBoxShouldBeEmpty() {
    if (!$this->getUserRecipes()) {
      throw new Exception("Recipe box is not empty");
    }
  }

  /**
   * Get all recipes for a given user account.
   *
   * Only works if using the Drupal API driver which requires
   * that tests on the same server as the code base is on.
   */
  private function getUserRecipes() {
    global $user;
    if (!($user = user_load_by_name($this->user->name))) {
      throw new Exception("Unable to load user with username: " . $this->user->name);
    }
    $lists = WFMApiRecipeList::getList($user->uid);
    if (is_array($lists) && count($lists) > 0) {
      return $lists;
    }
    return FALSE;
  }

  /*.*.*.*.*.*.*.*.*.*.*.*.*.*.* -- CLICK/SELECT FUNCTIONS -- .*.*.*.*.*.*.*.*/

  /**
   * @When /^I click the "(?P<link>[^"]*)" link in the "(?P<region>[^"]*)"(?:| region)$/
   */
  public function iClickTheLinkInTheRegion($link_text, $region) {

    $region_node = $this->spinGetRegion($region);

    $this->spin(function($context) use ($link_text, $region_node, $region) {
      // Find the link within the region.
      $link_node = $region_node->findLink($link_text);
      if (empty($link_node)) {
        throw new \Exception(sprintf('The link "%s" was not found in the region "%s" on the page %s', $link_text, $region, $context->getSession()->getCurrentUrl()));
      }
      $link_node->click();
      return TRUE;
    });
  }

  /**
   * @When /^I click the "([^"]*)" link in the "([^"]*)" container$/
   */
  public function iClickTheLinkInTheContainer($link_text, $container_selector) {
    $container = $this->getSession()->getPage()->find('css', $container_selector);
    if (empty($container)) {
      throw new Exception("Unable to find container: " . $container_selector);
    }
    $this->spin(function($context) use ($link_text, $container, $container_selector) {
      $link_node = $container->findLink($link_text);
      if (empty($link_node)) {
        throw new Exception("Could not find link " . $container_selector . " in the container " . $container_selector);
      }
      $link_node->click();
      return TRUE;
    });
  }

  /**
   * @When /^I click "([^"]*)" in the "([^"]*)" region to open a print window$/
   */
  public function iClickInTheRegionToOpenAPrintWindow($link_text, $region) {
    $region_element = $this->getRegion($region);

    $links = $region_element->findAll('css', 'a.print');

    $link_found = FALSE;
    $link_element = NULL;

    foreach ($links as $link_element) {
      if (strtolower($link_element->getText()) == strtolower($link_text)) {
        $link_found = TRUE;
        break;
      }
    }

    if (!$link_found) {
      throw new Exception('Unable to find link with text "' . $link_text . '" in the "' . $region . '" region.');
    }

    $suppress_print_dialog_function = '
      jQuery("a.print").click(
        function() {
          if (couponArray == null || (couponArray.length > 0)) {
            var cookieData = ( document.domain.indexOf("wholefoodsmarket.com") == -1 ) ? "&data="+couponArray.toString() : "";
            var features = "left=0,top=0,location=0,scrollbars=1,toolbar=0,status=0,menubar=0,resizable=0,width=750,height=550";
            window.open( "http://www2.wholefoodsmarket.com/coupons/print.php?suppressPrint=true&country=USA" + cookieData, "CouponPrint", features );
          }
          else{
            alert("Please select the coupons you would like to print, then click \'Print Selected\' again.");
          }
        }
      );
    ';

    $this->getSession()->executeScript($suppress_print_dialog_function);
    $print_selector = "#edit-form-submit";
    $click_print = "jQuery('$print_selector').click();";
    $this->getSession()->executeScript($click_print);
  }

  /**
   * @When /^I click the first link in the container "([^"]*)" "([^"]*)" in the "([^"]*)" region$/
   */
  public function iClickTheFirstLinkInTheContainerInTheRegion($id_or_class, $id_or_class_value, $region) {
    $region_node = $this->getRegion($region);
    if (!($container = $region_node->find('css', $this->idOrClassSeparator($id_or_class) . $id_or_class_value))) {
      throw new Exception('Unable to find container with ' . $id_or_class . ' ' . $id_or_class_value);
    }
    $first_link = $container->find('css', 'a');
    $url = $first_link->getAttribute("href");
    $this->getSession()->executeScript('window.open("' . $url . '", "testWindow")');
    $this->getSession()->switchToWindow('testWindow');
  }

  /**
   * @When /^I select radio button with value "([^"]*)"$/
   */
  public function iSelectRadioButtonWithValue($radio_button_value) {
    $script = 'jQuery(":radio[value=' . $radio_button_value . ']").attr("checked", true)';
    $this->getSession()->executeScript($script);
  }

  /**
   * @When /^I ammend radio button with value "([^"]*)" to be "([^"]*)" and then select it$/
   */
  public function iAmmendRadioButtonWithValueToBeAndThenSelectIt($radio_button_value, $new_radio_button_value) {
    $script = 'jQuery(":radio[value=' . $radio_button_value . ']").attr("value", "' . $new_radio_button_value . '").attr("checked", true)';
    $this->getSession()->executeScript($script);
  }

  /**
   * Click on the element with the provided xpath query.
   *
   * @When /^I click on the element with css selector "([^"]*)"$/
   */
  public function iClickOnTheElementWithCssSeletor($css) {
    $this->spin(function($context) use ($css) {
      $session = $context->getSession();
      $element = $session->getPage()->find('xpath', $session->getSelectorsHandler()->selectorToXpath('css', $css));
      if (NULL === $element) {
        throw new Exception('Could not find element ' . $css);
      }
      $element->click();
      return TRUE;
    });
  }

  /**
   * @Then /^I should see a submit button in the "([^"]*)" container with value "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeASubmitButtonInTheContainerWithValueInTheRegion($container_selector, $value, $region) {
    $container = $this->containerInRegion($container_selector, $region);
    $button = $container->findAll('css', 'input[type="submit"]');
    foreach ($button as &$submitbutton) {
      if (stripos($submitbutton->getAttribute('value'), $value) !== FALSE) {
        return;
      }
    }
    throw new Exception('Unable to find submit button with value ' . $value . ' in container: ' . $container_selector . ' in the ' . $region . ' region.');
  }

  /**
   * @Given /^I press the submit button with value "([^"]*)" in the "([^"]*)" container in the "([^"]*)" region$/
   */
  public function iPressTheSubmitButtonWithValueInTheContainerInTheRegion($submit_button_text, $selector, $region) {
    $container = $this->containerInRegion($selector, $region);

    $submit_buttons = $container->findAll('css', 'input[type="submit"]');
    foreach ($submit_buttons as $submit_button) {
      if ($submit_button->getAttribute('value') === $submit_button_text) {
        $submit_button->click();
        return;
      }
    }
    throw new Exception("Unable to find submit button with value '" . $submit_button_text . "' in container '" . $selector . "' in the region '" . $region . "'");
  }

  /**
   * @Given /^I press the submit button with value "([^"]*)" in the "([^"]*)" container$/
   */
  public function iPressTheSubmitButtonWithValueInTheContainer($submit_button_text, $selector) {
    $container = $this->getSession()->getPage()->find('css', $selector);
    if (empty($container)) {
      throw new Exception("Couldn't find selector " . $selector);
    }
    $submit_buttons = $container->findAll('css', 'input[type="submit"]');
    foreach ($submit_buttons as $submit_button) {
      if ($submit_button->getAttribute('value') === $submit_button_text) {
        $submit_button->click();
        return;
      }
    }
    throw new Exception("Unable to find submit button with value " . $submit_button_text . " in container " . $selector);
  }

  /**
   * @Given /^I manually press "([^"]*)"$/
   */
  public function iManuallyPress($key) {
    $script = "jQuery.event.trigger({ type : 'keypress', which : '" . $key . "' });";
    $this->getSession()->evaluateScript($script);
  }

  /**
   * @When /^I select "([^"]*)" from "([^"]*)" in the "([^"]*)" region$/
   *
   * This function is a wrapper for MinkContext's selectOption.
   */
  public function iSelectFromInTheRegion($option, $select, $region) {
    $this->spinGetRegion($region);
    $this->spin(function($context) use ($option, $select) {
      $context->selectOption($select, $option);
      return TRUE;
    });
  }

  /**
   * @When /^I additionally select "([^"]*)" from "([^"]*)" in the "([^"]*)" region$/
   */
  public function iAdditionallySelectFromInTheRegion($option, $select, $region) {
    $this->getRegion($region);
    $this->spin(function($context) use ($option, $select) {
      $context->selectOption($select, $option, TRUE);
      return TRUE;
    });
  }

  /**
   * @When /^I additionally select "([^"]*)" from "([^"]*)" in the "([^"]*)" container$/
   */
  public function iAdditionallySelectFromInTheContainer($option, $select, $container_selector) {
    $container = $this->getSession()->getPage()->find('css', $container_selector);
    $this->spin(function($context) use ($option, $select) {
      $context->selectOption($select, $option, TRUE);
      return TRUE;
    });
  }

  /**
   * @Then /^all checkboxes in container "([^"]*)" should be "([^"]*)" in the "([^"]*)" region$/
   */
  public function allCheckboxesInContainerShouldBeInTheRegion($selector, $checked_or_unchecked, $region) {
    $container_element = $this->containerInRegion($selector, $region);

    $checkboxes = $container_element->findAll('css', 'input[type=checkbox]');

    foreach ($checkboxes as $checkbox) {
      if ($checked_or_unchecked == self::CHECKED && !$checkbox->isChecked()) {
        throw new Exception('All checkboxes expected to be checked. At least one was not checked.');
      }

      if ($checked_or_unchecked == self::UNCHECKED && $checkbox->isChecked()) {
        throw new Exception('All checkboxes expected to be unchecked. At least one was checked.');
      }
    }
  }

  /**
   * @Then /^I click the "([^"]*)" button in the "([^"]*)" container$/
   */
  public function iClickTheButtonInTheContainer($button_id_name, $container_selector) {
    $container = $this->getSession()->getPage()->find('css', $container_selector);
    if (empty($container)) {
      throw new Exception("Unable to find container " . $container_selector);
    }
    $button_node = $container->find('css', $button_id_name);
    if (empty($button_node)) {
      throw new Exception("Unable to find button " . $button_id_name);
    }
    $button_node->click();
  }

  /**
   * @When /^I select "([^"]*)" from "([^"]*)" in the "([^"]*)" container$/
   */
  public function iSelectFromInTheContainer($option, $select, $container_selector) {
    $container = $this->getSession()->getPage()->find('css', $container_selector);
    if (empty($container)) {
      throw new Exception("Unable to find css selector " . $container_selector);
    }
    $this->spin(function($context) use ($option, $select) {
      $context->selectOption($select, $option);
      return TRUE;
    });
  }

  /* **************************** THE REST OF THE FUNCTIONS:********************** */

  /**
   * @Then /^I should see a "([^"]*)" element "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeAnEnabledOrDisabledElementInTheRegion($state, $selector, $region_name) {
    $region_node = $this->getRegion($region_name);
    if (empty($region_node)) {
      throw new Exception("Unable to find region " . $region_name);
    }
    $button_node = $region_node->find('css', $selector);
    if (empty($button_node)) {
      throw new Exception("Unable to find element with selector " . $selector);
    }

    $result = $this->getSession()->getDriver()->evaluateScript(
      "return jQuery('" . $selector . ":" . $state . "') ? true : false;"
    );
    if (!$result) {
      throw new Exception('Element ' . $selector . ' with state ' . $state . ' not found.');
    }
  }

  /**
   * @Then /^"([^"]*)" should not be visible$/
   */
  public function shouldNotBeVisible($selector) {
    $this->spin(function($context) use ($selector) {
      $element = $context->getSession()->getPage()->find('css', $selector);
      if (empty($element)) {
        throw new Exception("Element ({$selector}) not found.");
      }
      $style = preg_replace('/\s/', '', $element->getAttribute('style'));
      if (strpos($style, 'display:none') === FALSE) {
        throw new Exception("Element ({$selector}) is visible when it should not be.");
      }
      return TRUE;
    });
  }

  /**
   * @Then /^I should see list items with parent "([^"]*)" "([^"]*)"$/
   */
  public function iShouldSeeListItemsWithParent($id_or_class, $id_or_class_value) {
    $element = $this->getSession()->getPage();
    if (!($ul_node = $element->find('css', 'ul' . $this->idOrClassSeparator($id_or_class) . $id_or_class_value))) {
      throw new Exception('Unable to find ul with ' . $id_or_class . ': ' . $id_or_class_value);
    }
    $list_item_nodes = $ul_node->findAll('css', 'li');
    if (count($list_item_nodes) == 0) {
      throw new Exception('Zero ingredients loaded');
    }
  }

  /**
   * @Then /^the page should contain "([^"]*)" "([^"]*)" tag with ID or class "([^"]*)" "([^"]*)" in the "([^"]*)" region$/
   */
  public function thePageShouldContainTagWithIdOrClassInTheRegion($required_count, $tag, $id_or_class, $id_or_class_value, $region) {
    $selector = $tag . $this->idOrClassSeparator($id_or_class) . $id_or_class_value;
    $region_node = $this->getRegion($region);
    $div_nodes = $region_node->findAll('css', $selector);
    $div_node_count = count($div_nodes);
    if ($div_node_count == 0) {
      throw new Exception('Unable to locate any "' . $tag . '" tags with class name "' . $id_or_class_value . '" in the "' . $region . '" region ');
    }
    if ($div_node_count > $required_count) {
      throw new Exception('Found too many ' . $tag . ' tags with class name ' . $id_or_class_value . ' in the ' . $region . ' region. Found ' . $div_node_count . ', expected ' . $required_count . '.');
    }
  }

  /**
   * @Then /^"([^"]*)" should be visible$/
   */
  public function shouldBeVisible($selector) {
    $this->spin(function($context) use ($selector) {
      $element = $context->getSession()->getPage()->find('css', $selector);
      $style = '';
      if (empty($element)) {
        throw new Exception("Element $selector not found");
      }
      $style = preg_replace('/\s/', '', $element->getAttribute('style'));
      if (strpos($style, 'display:none') !== FALSE) {
        throw new Exception("Element $selector is not visible when it should be.");
      }
      return TRUE;
    });
  }

  /**
   * @When /^I hover over the element "([^"]*)"$/
   */
  public function iHoverOverTheElement($locator) {
    $element = $this->getSession()->getPage()->find('css', $locator);
    if (NULL === $element) {
      throw new Exception("Could not find container: " . $locator);
    }
    $element->mouseOver();
  }

  /**
   * @Then /^I should see "([^"]*)" "([^"]*)" elements in the "([^"]*)" container$/
   */
  public function iShouldSeeElementsInTheContainer($countOfListElementInstances, $listElement, $listContainer) {
    if (count($this->getSession()->getPage()->findAll('css', $listContainer)) > 1) {
      throw new Exception("Found more than one container named: " . $listContainer . ". Function should only receive id with no possible duplicates/copies on page.");
    }
    $container = $this->getSession()->getPage()->find('css', $listContainer);
    if (empty($container)) {
      throw new Exception("Unable to find container: " . $listContainer);
    }
    $list_nodes = $container->findAll('css', $listElement);
    if (count($list_nodes) == 0) {
      throw new Exception("Zero elements named: " . $listElement . " found in " . $listContainer);
    }
    if (count($list_nodes) != $countOfListElementInstances) {
      throw new Exception("Container " . $listContainer . " does not hold " . $countOfListElementInstances . " instances of " . $listElement . ".");
    }
  }

  /**
   * @Then /^I should see the regex "([^"]*)" identified by "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeTheRegexIdentifiedByInTheRegion($regex, $selector, $region) {
    $region_element = $this->getRegion($region);
    $element = $region_element->find('css', $selector);

    if (empty($element)) {
      throw new Exception('Unable to find element with selector ' . $selector);
    }

    $find = $element->getText();

    if (!preg_match($regex, $find)) {
      throw new Exception('Unable to find text with regex ' . $regex . ' in the ' . $region . ' region.');
    }
  }

  /**
   * @param string $selector
   *    CSS selector for the container.
   * @param string $region
   *    Defined region name.
   *
   * @return bool
   *    TRUE if succesful, otherwise exception.
   *
   * @throws Exception
   *    Unable to find container.
   */
  protected function containerInRegion($selector, $region) {
    $this->spin(function($context) use ($selector, $region) {
      $region_element = $context->getRegion($region);
      $container_elements = $region_element->findAll('css', $selector);

      if (empty($container_elements)) {
        throw new Exception('Unable to find container element with selector: "' . $selector . '" in the "' . $region . '" region.');
      }

      if (count($container_elements) == 1) {
        $context->return = $container_elements[0];
        return TRUE;
      }
      $context->return = $container_elements;
      return TRUE;
    });
    return $this->return;
  }

  /**
   * @Then /^the page element identified by "([^"]*)" has content of minimum length "([^"]*)" in the "([^"]*)" region$/
   */
  public function thePageElementIdentifiedByHasContentOfMinimumLengthInTheRegion($selector, $minimum_length, $region) {
    $container_element = $this->containerInRegion($selector, $region);
    $text = $container_element->getText();
    $text_length = strlen($text);
    if ($text_length < $minimum_length) {
      throw new Exception('Page element with selector "' . $selector . '" was found but element content is too short. Expected minimum "' . $minimum_length . '" . characters. Found "' . $text_length . '" characters.');
    }
  }

  /**
   * @Then /^the page element identified by "([^"]*)" has at least "([^"]*)" words in the "([^"]*)" region$/
   */
  public function thePageElementIdentifiedByHasAtLeastWordsInTheRegion($selector, $minimum_word_count, $region) {
    $container_element = $this->containerInRegion($selector, $region);
    $text = $container_element->getText();
    $word_count = str_word_count($text);
    if ($word_count < $minimum_word_count) {
      throw new Exception('Page element with selector "' . $selector . '" was found but element content is too short. Expected minimum "' . $minimum_word_count . '" . words. Found "' . $word_count . '" words.');
    }
  }

  /**
   * @Then /^I click a random element identified by "([^"]*)" in the "([^"]*)" region$/
   */
  public function iClickARandomElementIdentifiedByInTheRegion($selector, $region) {
    $region_element = $this->spinGetRegion($region);

    $links = $region_element->findAll('css', $selector);

    if (empty($links)) {
      throw new Exception('Unable to find links with selector "' . $selector . '"');
    }

    $random_link_element = $this->getRandomLinkElement($links);
    $link_text = $random_link_element->getText();
    $matches = array();
    if (preg_match('/(\w+|\s|-|,)+/', $link_text, $matches) == 1) {
      $this->found_link_text = TRUE;
      $this->selected_text = trim($matches[0]);
    }
    $this->selected_url = $random_link_element->getAttribute('href');
    $random_link_element->click();
  }

  /**
   * @Then /^I click a random element identified by "([^"]*)"$/
   */
  public function iClickARandomElementIdentifiedBy($container_selector) {
    $container = $this->getSession()->getPage()->findAll('css', $container_selector);
    if (empty($container)) {
      throw new Exception('Unable to find links with selector "' . $container_selector . '"');
    }
    $random_link_element = $this->getRandomLinkElement($container);
    if (empty($random_link_element)) {
      throw new Exception("Something went wrong with the getRandomLinkElement function");
    }
    $link_text = $random_link_element->getText();
    // Link text contains both recipe tile AND recipe rating.
    // So just grab the title to click on.
    $pos = strpos($link_text, " Recipe Rating:");
    $link_text = substr($link_text, 0, $pos);
    /* print "\n" .  $link_text . "\n"; */
    $this->found_link_text = TRUE;
    $this->selected_text = $link_text;
    $link_node = $this->getSession()->getPage()->findLink($link_text);
    if (empty($link_node)) {
      throw new Exception("Could not find link for random recipe selected.");
    }
    $link_node->click();
  } //@here

  /**
   * Get a random link from a list of elements.
   *
   * Sometimes the links founds aren't visible.
   * Try up to 10 random ones before failing.
   */
  private function getRandomLinkElement(&$links, $count = 0) {
    $index = array_rand($links);
    $random_link_element = $links[$index];
    if ($random_link_element->isVisible()) {
      return $random_link_element;
    }
    // 10 is an arbitrary number.
    // Hopefully a visible link is return by that many tries.
    elseif ($count < 10) {
      // Remove hidden links so they are selected next time around.
      unset($links[$index]);
      return $this->getRandomLinkElement($links, $count++);
    }
  }

  /**
   * @Then /^I should be on the selected page$/
   */
  public function iShouldBeOnTheSelectedPage() {
    $current_url = $this->getSession()->getCurrentUrl();

    if (strpos($current_url, $this->selected_url) === FALSE) {
      throw new Exception('Not on correct page. Expected: ' . $this->selected_url . ' but page is: ' . $current_url);
    }
  }

  /**
   * @Then /^I should be on the selected page with title identified by "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldBeOnTheSelectedPageWithTitleIdentifiedByInTheRegion($selector, $region) {
    $title_element = $this->containerInRegion($selector, $region);
    if (is_array($title_element)) {
      foreach ($title_element as $te) {
        $title_text = trim($te->getText());
        if ($this->found_link_text && (stripos($title_text, $this->selected_text) === 0)) {
          break;
        }
      }
    }
    else {
      $title_text = trim($title_element->getText());
    }
    if ($this->found_link_text && (stripos($title_text, $this->selected_text) === FALSE)) {
      throw new Exception('Page heading incorrect. Expected: ' . $this->selected_text . ' but page heading is: ' . $title_text);
      $this->iShouldBeOnTheSelectedPage();
    }
  }

  /**
   * @Then /^I should see the randomly added recipe identified by "([^"]*)"$/
   */
  public function iShouldSeeTheRandomlyAddedRecipeIdentifiedByInTheRegion($selector) {
    $saved_recipes = $this->getSession()->getPage()->findAll('css', $selector);
    $found_saved_recipe = FALSE;
    foreach ($saved_recipes as $save_recipe) {
      $link_text = $save_recipe->getText();
      $selected_link_text = $this->selected_text;
      if (strcasecmp($link_text, $selected_link_text) == 0) {
        $found_saved_recipe = TRUE;
        break;
      }
    } //@here

    if (!$found_saved_recipe) {
      throw new Exception("Unable to find randomly selected recipe in the recipe box with '$selector'");
    }
  }

  /**
   * @Then /^I should see an "([^"]*)" element in the "([^"]*)" region$/
   */
  public function iShouldSeeAnElementInTheRegion($selector, $region) {
    $region_node = $this->getRegion($region);

    $this->spin(function($context) use ($selector, $region_node) {
      $context->assertSession()->elementExists('css', $selector, $region_node);
      return TRUE;
    });
  }

  /**
   * @Given /^I enter "([^"]*)" into "([^"]*)" in the "([^"]*)" region$/
   */
  public function iEnterIntoInTheRegion($value, $field, $region) {

    $region_node = $this->spinGetRegion($region);

    $this->spin(function($context) use ($value, $field, $region_node) {
      $region_node->fillField($field, $value);
      return TRUE;
    });
  }

  /**
   * @Then /^I should see an "([^"]*)" element in the "([^"]*)" container$/
   */
  public function iShouldSeeAnElementInTheContainer($element, $selector) {
    $container = $this->getSession()->getPage()->find('css', $selector);
    if (empty($container)) {
      throw new Exception("Couldn't find selector '$selector'");
    }
    $this->spin(function($context) use ($element, $container) {
      $context->assertSession()->elementExists('css', $element, $container);
      return TRUE;
    });
  }

  /**
   * @Then /^I should see a disabled button with id "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeADisabledButtonWithIdInTheRegion($button_id_name, $region_name) {
    $region_node = $this->getRegion($region_name);
    if (empty($region_node)) {
      throw new Exception("Unable to find region " . $region_name);
    }
    $button_node = $region_node->findById($button_id_name);
    if (empty($button_node)) {
      throw new Exception("Unable to find button with id " . $button_id_name);
    }

    $this->getSession()->getDriver()->evaluateScript(
      "function(){ return jQuery('#footer-store-locator-submit:'); }()"
    );
    throw new Exception("Function not finished being written yet.");
  }

  /**
   * @Then /^I should see an enabled button with id "([^"]*)" in the "([^"]*)" region$/
   */
  public function iShouldSeeAnEnabledButtonWithIdInTheRegion($button_id_name, $region_name) {
    $region_node = $this->getRegion($region_name);
    if (empty($region_node)) {
      throw new Exception("Unable to find region " . $region_name);
    }
    $button_node = $region_node->findById($button_id_name);
    if (empty($button_node)) {
      throw new Exception("Unable to find button with id " . $button_id_name);
    }
    throw new Exception("Function not finished being written yet.");
  }

  /**
   * @Given /^each article should have a title, author, comment link and read more link$/
   */
  public function eachArticleShouldHaveATitleAuthorCommentLinkAndReadMoreLink() {
    // Find the articles for this page and check they have all the right parts.
    $articles = $this->getSession()->getPage()->findAll('css', ".view-id-blogs .views-row .torn-pod-content");
    $selectors = array(".blog-title a", ".blog-author", ".more-posts-by-link",
      ".views-more-link", ".post-comment-count"
    );
    foreach ($articles as $article) {
      foreach ($selectors as $selector) {
        $title = $article->find('css', $selector)->getText();
        if (!$title) {
          throw new Exception("Didn't find " . $selector . " element for " . $article);
        }
      }
    }
  }

  /**
   * @Then /^I should see the elements: "([^"]*)"$/
   */
  public function iShouldSeeTheElements($arg1) {
    if ($arg1) {
      $elements = explode(',', $arg1);
      if (is_array($elements)) {
        foreach ($elements as $element) {
          if (!$this->getSession()->getPage()->find('css', $element)->isVisible()) {
            throw new \WebDriver\Exception\ElementNotVisible($element . ' is not visible.');
          }
        }
      }
    }
  }

  /**
   * @Then /^I should see the element: "([^"]*)"$/
   */
  public function iShouldSeeTheElement($element) {

    $page = $this->getSession()->getPage();
    $found = $page->find('css', $element);
    if (empty($found)) {
      $str = "Unable to find element: " . $element;
      throw new Exception($str);
    }
    if (!$found->isVisible()) {
      $str = "This element is not visible: " . $element;
    }

//    throw new PendingException();
  }

  /**
   * @Then /^I should not see the elements: "([^"]*)"$/
   */
  public function iShouldNotSeeTheElements($arg1) {
    if ($arg1) {
      $elements = explode(',', $arg1);
      if (is_array($elements)) {
        foreach ($elements as $element) {
          if ($this->getSession()->getPage()->find('css', $element)->isVisible()) {
            throw new \WebDriver\Exception\ElementNotVisible($element . ' is visible.');
          }
        }
      }
    }
  }

  /**
   * @Given /^the "([^"]*)" input should have the value of "([^"]*)"$/
   */
  public function theInputShouldHaveTheValueOf($arg1, $arg2) {
    if ($arg1 && $arg2) {
      $hidden_input = $this->getSession()->getPage()->find('xpath', $arg1);
      if ($hidden_input) {
        if ($hidden_input->getValue() != $arg2) {
          throw new Exception($arg1 . ' should equal ' . $arg2);
        }
      }
      else {
        throw new Exception($arg1 . ' not present on the page.');
      }
    }
    else {
      throw new Exception('Both arguments should have values');
    }
  }

  /**
   * Utility to make and save a screenshot of the current page.
   *
   * @param string $text
   *   Optional short identifier added to the file name.
   * @param string $dir_path
   *    Folder where screenshot is placed. (Folder path must exist).
   *
   * @return string
   *    The full pathname to the screenshot
   *    or a msg as to why there was an error.
   */
  public function putScreenshot($text = 'putScreenshot', $dir_path = NULL) {
    if (NULL == $dir_path) {
      $dir_path = getcwd() . "/reports";
    }
    if (!is_dir($dir_path)) {
      return "No Screenshot: Folder does not exist '" . $dir_path . "'";
    }

    $driver = $this->getSession()->getDriver();
    if ($driver instanceof Behat\Mink\Driver\Selenium2Driver) {
      $screenshot = $driver->getScreenshot();
      $file_path = "$dir_path/" . date("Y-m-d_H.i.s") . "_$text.png";
      file_put_contents($file_path, $screenshot);
      return $file_path;
    }
    return "No Screenshot: Driver not supported ";
  }

  /**
   * Routine to return the selector element(s) if at least one is visible.
   *
   * @param string $el_selector
   *    The element selector.
   * @param string $container_selector
   *    Limits the element search to be within the specified container.
   * @param bool $multiple
   *    If FALSE selector must find one and only one element.
   * @param bool $quash_exception
   *    If true, will not throw exception but instead return FALSE.
   *
   * @return objects or bool
   *    Element(s) matching the selector
   *    or FALSE if there was a quashed exception.
   *
   * @throws Exception
   *    A screenshot of the current page.
   */
  public function resolveSelector($el_selector, $container_selector = NULL, $multiple = FALSE, $quash_exception = FALSE) {
    // If the field selector is a jQuery then special processing is required.
    if (stripos($el_selector, 'jQuery') !== FALSE) {
      throw new Exception("Please use function setElementValueJQuery for el_selector='$el_selector'");
    }

    // If container selector is specified, prepend it to the element selector.
    // Else just use the element selector.
    if ($container_selector !== NULL) {
      $selector = $container_selector . " " . $el_selector;
    }
    else {
      $selector = $el_selector;
    }

    // Make sure the element exists and is visible, then get all instances.
    try {
      $elements = $this->waitOnCssSelector($selector);
    }
    catch (Exception $exception_message) {
      if ($quash_exception) {
        // Quashed exceptions return FALSE.
        return FALSE;
      }
      $backtrace = debug_backtrace();
      $url = $this->getSession()->getCurrentUrl();
      $screenshot = $this->putScreenshot($backtrace[2]['function'] . "- resolveSelector");
      throw new Exception("Screenshot of '$url' is at location $screenshot\n" . $exception_message);
    }

    // Note: To get here, at least one element was found.
    $count = count($elements);

    // A single instance is always a good return.
    if ($count == 1 and !$multiple) {
      return $elements[0];
    }
    // Multiple instances are ok if expected.
    if ($multiple) {
      return $elements;
    }

    // Bad news, too many found so give a detailed output.
    $backtrace = debug_backtrace();
    $exception_message = $backtrace[1]['class'] . "::" . $backtrace[1]['function'] . " ";
    $exception_message .= "More than one element returned for selector='$selector'\n";
    if (array_key_exists('file', $backtrace[1])) {
      $exception_message .= $backtrace[1]['file'];
    }
    if (array_key_exists('line', $backtrace[1])) {
      $exception_message .= ", line " . $backtrace[1]['line'];
    }
    throw new Exception($exception_message);
  }

  /**
   * Utility routine to set an element referenced by a selector to a value.
   *
   * Supported elements (HTML tag names):
   * TEXTAREA tag: Set the element's value to $value
   * INPUT type=text tag: Set the element's value to $value
   * INPUT type=submit tag: Submits a form element
   * INPUT type=checkbox tag: Set checkbox value to CHECK/UNCHECK or click it
   * SELECT tag: If $value is an integer, set by matching the option value
   * "        ": if $value is a string, set by matching option's visible text
   * BUTTON tag: Press the button
   * <A> tag: Click the href link
   * DIV tag: Click the div.
   *
   * @param string $el_selector
   *    CSS selector.
   * @param string $value
   *    Optional value that the resolved element will receive.
   * @param string $container_selector
   *    Optional container selector.
   * @param int $index
   *    When el_selector returns multiple elements,
   *    the index of the element to act upon (1 based).
   *
   * @return bool
   *    TRUE if successful
   */
  public function setSelector($el_selector, $value = NULL, $container_selector = NULL, $index = FALSE) {
    // If the field selector is a jQuery then special process elsewhere.
    if (stripos($el_selector, 'jQuery') !== FALSE) {
      $this->setElementValueJQuery($el_selector, $value);
      return TRUE;
    }

    // Get the element by resolving its selectors and give it focus.
    if ($index == FALSE) {
      $element = $this->resolveSelector($el_selector, $container_selector);
    }
    else {
      $elements = $this->resolveSelector($el_selector, $container_selector, TRUE);
      $element = $elements[$index - 1];
    }

    $this->setElem($element, $value, $el_selector);
    return $element;
  }

  /**
   * Set the value of an element.
   *
   *  The "set" is performed based upon the HTML tag type.
   *  Elements that do not accept a value will be clicked or pressed.
   *
   * @param object $element
   *   The element to act upon.
   * @param string $value
   *   The value the element will receive (if applicable).
   * @param string $el_selector
   *    The selector used to find the element (just used for error reporting).
   *
   * @throws Exception
   *    Unhandled Type New code must be added to handle this HTML tag type.
   */
  public function setElem($element, $value = NULL, $el_selector = "unspecified") {
    // Use the element's tag type to perform the appropriate set action.
    $tag = $element->getTagName();
    switch ($tag) {
      case "a":
      case "div":
      case "li":
      case "button":
        // Press the button.
        try {
          $element->click();
        }
        catch (Exception $e) {
          $jquery_click = "jQuery('$el_selector').click()";
          $this->getSession()->executeScript($jquery_click);
        }
        break;

      case "textarea":
        // Set the text area's value.
        $element->setValue($value);
        break;

      case "select":
        // If the value is an integer then select using the value attribute.
        if (is_int($value)) {
          $this->getSession()->executeScript("jQuery('$el_selector').val($value)");
          $this->getSession()->executeScript("jQuery('$el_selector').change()");
          break;
        }
        // Otherwise, find and select the option(s) by the visible text.
        // But first, single selections are put into an array to simplify code.
        if (!is_array($value)) {
          $value = array($value);
        }
        // Get options text strings process them against the select value(s).
        $options_str = $element->getText();
        $multi = FALSE;
        foreach ($value as $item) {
          $hint = $this->getHint($options_str, $item);
          if (FALSE === $hint) {
            throw new Exception("Option '$item' not found in '$el_selector'");
          }
          // Use the hint to help find the option.
          $option = $this->findOptionByRegex($element, "/$item/", $hint);
          if ($option) {
            $element->selectOption($option->getValue(), $multi);
          }
          else {
            throw new Exception("Option '$item' not found in '$el_selector'");
          }

          // Additional looping will result in multiple selects.
          $multi = TRUE;
        }

        // Some selects need a "change" event to be fired in order for the
        // update to take effect.
        if ($this->js_enabled_driver) {
          $change_event = "jQuery('$el_selector').change()";
          $this->getSession()->executeScript($change_event);
        }
        break;

      // The input requires its own switch to handle its methods of input.
      case "input":
        // As new input types are encountered please add them to the switch.
        $input_type = $element->getAttribute("type");
        switch ($input_type) {

          case "text":
          case "email":
            $element->setValue($value);
            break;

          case "submit":
          case "radio":
            try {
              $element->press();
            }
            catch (Exception $e) {
              $jquery_click = "jQuery('$el_selector').click()";
              $this->getSession()->executeScript($jquery_click);
            }
            break;

          case "checkbox":
            // Only make checked if value is case insensitive CHECK.
            // Otherwise uncheck as default.
            switch (strtolower($value)) {
              case "click":
              case "press":
                // Sometimes a click is required to perform the action.
                $element->click();
                break;

              case "check":
                $element->check();
                break;

              default:
                $element->uncheck();
            }
            break;

          default:
            // Give the setValue function  a chance to work.
            // If it doesn't work then new code to handle that particular input
            // type will need to be added.
            $element->setValue($value);
        }
        // End input tag case.
        break;

      default:
        throw new Exception("UNHANDLED TYPE: '$tag'. Please update FeatureContext:SetElem");
    }
  }

  /**
   * Utility routine to to set an element's value via jQuery.
   *
   * Sometimes an input element will not respond to default Mink/Drupal methods.
   * Such as text input that requires validation or certain select options.
   * This function surrounds the value setting with focus, change, and blur
   * event triggers to simulate actual user behavior.
   *
   * @param string $field_id
   *   The element selector as a jQuery "jQuery('css_selector')".
   * @param string $value
   *   The value to use.
   *
   * @return bool
   *   TRUE when successful.
   *
   * @throws Exception
   *   Bad Format: field_id is not in the form of a jQuery.
   */
  private function setElementValueJQuery($field_id, $value) {
    // Verify the field ID selector is in the form of a jQuery.
    if (stripos($field_id, 'jQuery') === FALSE) {
      throw new Exception('BAD FORMAT: field_id is not in the form of a jQuery.' .
        'Given field_id="' . $field_id . '""');
    }

    // Remove the jQuery('selector') wrapper & verify the selector is visible.
    $selector = str_ireplace("jQuery('", "", $field_id);
    $selector = str_ireplace("')", "", $selector);
    $this->shouldBeVisible($selector);

    // Move the focus on to the element.
    $jquery = "$field_id.focus()";
    $this->getSession()->executeScript($jquery);

    // Input the element's new value.
    $jquery = "$field_id.val('$value')";
    $this->getSession()->executeScript($jquery);

    // Trigger a change event on the element.
    $jquery = "$field_id.change()";
    $this->getSession()->executeScript($jquery);

    // Move the focus away from the element.
    $jquery = "$field_id.blur()";
    $this->getSession()->executeScript($jquery);
    return TRUE;
  }

  /**
   * Utility function to allow option selection via a regular expression.
   *
   * This is needed to get around the default selectOption which
   * matches the text if it is the 1'st substring.
   *
   * @param string $el_select
   *    The CSS selector of the select element.
   * @param string $regex
   *    A regular expression designed for matching.
   *
   * @return string or bool
   *    The matching option's text, or FALSE if no match
   */
  private function selectOptionByRegex($el_select, $regex) {

    $option = $this->findOptionByRegex($el_select, $regex);
    if ($option) {
      $el_select->setValue($option->getValue());
      return $option->getText();
    }
    // No match no selection.
    return FALSE;
  }

  /**
   * Utility function to find a select option via a regular expression.
   *
   * This is needed to get around the default selectOption which matches text
   * even if it is only a substring. Ex "East Essex" will match before "Essex"
   * in an ordered list.
   * Some of the WFM select lists are quite long (> 100 options).
   * This search can use a supplied hint to calculate a likely starting index.
   * It will search alternately below and above the start until the option is
   * found or return FALSE if all elements do not result in a match.
   *
   * @param string $el_select
   *    The CSS selector of the select element.
   * @param string $regex
   *    A regular expression designed for matching.
   * @param float $hint
   *    Optional, the probable (0-1.0) location within the options list.
   *    Where 0 is the begining of the list and 1.0 is the end.
   *
   * @return object or bool
   *    The first matching option element, or FALSE if no match
   */
  private function findOptionByRegex($el_select, $regex, $hint = 0) {
    $options = $el_select->findAll("css", "option");
    $max_index  = count($options) - 1;
    $start = (int) ($max_index * $hint);
    $too_lo = FALSE;
    $too_hi = FALSE;
    for ($i = 0; TRUE; $i++) {
      // Move down from start index.
      if (!$too_lo) {
        $ilo = $start - $i;
        if (preg_match($regex, $options[$ilo]->getText())) {
          return $options[$ilo];
        }
        else {
          $too_lo = (0 > --$ilo);
        }
      }
      // Move up from start index.
      if (!$too_hi) {
        $ihi = $start + $i;
        if (preg_match($regex, $options[$ihi]->getText())) {
          return $options[$ihi];
        }
        else {
          $too_hi = ($max_index < ++$ihi);
        }
      }

      // Return FALSE if all options have been searched.
      if ($too_lo and $too_hi) {
        return FALSE;
      }
    }
  }

  // Indicates the scenario driver's support for javascript/jQuery calls.
  public $jsEnabledDriver = FALSE;

  /**
   * @BeforeScenario
   *    Determine if the driver's scenario supports javaScript/JQuery calls.
   */
  public function before($event) {
    $rc = new ReflectionClass(get_class($event));
    // Determine if this scenario's driver supports javaScript/jQuery execution.
    // If not, then implicit javascript functions will be avoided.
    if ($rc->hasMethod("getScenario")) {
      $this->js_enabled_driver = in_array("javascript", $event->getScenario()->getTags());
    }
    elseif ($rc->hasMethod("getOutline")) {
      $this->js_enabled_driver = in_array("javascript", $event->getOutline()->getTags());
    }
  }

  /**
   * Utility routine to fill in forms from a data array.
   *
   * @param string $form_selector
   *    CSS form selector.
   * @param string $form_data
   *    Array of [element selector, value] pairs to load into the form.
   */
  public function formLoader($form_selector, $form_data) {
    // When a form selector is specified wait for it to exist.
    if ($form_selector) {
      $this->resolveSelector($form_selector);
    }
    // Fill in the form with the form data.
    foreach ($form_data as $field_id => $value) {
      $this->setSelector($field_id, $value, $form_selector);
    }
  }

  /**
   * Get the visible text, currently selected, in a drop down list.
   *
   * @param string $select_selector
   *    A CSS selector.
   *
   * @return string
   *    The visible text string.
   *
   * @throws Exception
   *    Bad selector.
   */
  public function getSelectedOption($select_selector) {
    // If the selector is a string resolve it.
    // Otherwise assume it already an element node.
    if (is_string($select_selector)) {
      $el_select = $this->resolveSelector($select_selector);
    }
    else {
      $el_select = $select_selector;
    }

    // Make sure this is a select element.
    if ($el_select->getTagName() != "select") {
      throw new Exception('Bad selector: Not a SELECT tag');
    }

    $selected_val = $el_select->getValue();
    $selected_text = $el_select->find("css", "option[value='$selected_val']")->getText();
    return $selected_text;
  }

  /**
   * Utility function to allow option selection via an index.
   *
   * @param string $el_select
   *   A CSS selector.
   * @param int $index
   *   The zero based index of the option.
   */
  private function selectOptionByIndex($el_select, $index) {
    $options = $el_select->findAll("css", "option");
    $value = $options[$index]->getVaule();
    $el_select->setVaule($value);
  }

  /**
   * Utility function to memorize data in a formalized way.
   *
   * Use this function if values in one step need to be accessed in another
   * step.
   *
   * @param mixed $key
   *    A unique name to identify the data.
   * @param mixed $value
   *    The data to store.
   */
  public function memorizeData($key, $value) {
    if (array_key_exists($key, $this->memorized_data)) {
      // A message to log that existing data is being over written.
      // $this->log(....);.
      NULL;
    }
    $this->memorized_data[$key] = $value;
  }

  /**
   * Utility function to recall data in a formalized way.
   *
   * @param string $key
   *    The unique name used to identify the data.
   *
   * @return mixed
   *    The data associated with the key.
   *
   * @throws Exception
   *    Key not found.
   */
  public function recallData($key) {
    if (array_key_exists($key, $this->memorized_data)) {
      return $this->memorized_data[$key];
    }
    throw new Exception("Key not found '$key'. Check spelling and/or feature/context logic.");
  }

  /**
   * Utility function to determine if data has been set under a specific name.
   *
   * @param string $key
   *    The unique name used to identify the data.
   *
   * @return bool
   *    TRUE if key is found, otherwise FALSE
   */
  public function memorizedDataExists($key) {
    return array_key_exists($key, $this->memorized_data);
  }

  /**
   * Waits on the current page for the CSS selector to be available.
   *
   * If you have timing issues you can pass in a CSS selector and this function
   * will wait for an element that matches the selector to be visible before
   * continuing.
   * Examples:
   *   $this->waitOnCssSelector("#my-button");
   *   $this->waitOnCssSelector(".region-header .capture_end_session");.
   *
   * @param string $selector
   *    The CSS selector to wait for.
   * @param int $timeout
   *    Number of seconds to wait before throwing a timeout exception.
   *
   * @return array
   *    Element(s) that match the CSS selector.
   *
   * @throws Exception
   *    Time out Selector not found (via spinning lambda function).
   */
  public function waitOnCssSelector($selector, $timeout = 20) {
    $this->spinWithReturn(function ($context) use ($selector) {
      $elements = $context->getSession()->getPage()->findAll("css", $selector);
      if (NULL == $elements) {
        throw new Exception("Element '$selector' not found");
      }
      $context->return = $elements;
    }, $timeout);
    return $this->return;
  }

  /**
   * Spin function that returns a value from the lambda function.
   *
   * @param string $lambda
   *   Function that throws exception for error condition, else returns a value.
   * @param int $wait
   *   Max time in seconds to wait for a good return.
   *
   * @return mixed
   *   Whatever the lambda function returns.
   *
   * @throws Exception
   *   When wait time out is exceeded.
   */
  public function spinWithReturn($lambda, $wait = 20) {
    $exception_message = '';

    for ($i = 0; $i < $wait; $i++) {
      try {
        $result = $lambda($this);
        return $result;
      }
      catch (Exception $e) {
        $exception_message = $e->getMessage() . "\n";
      }
      sleep(1);
    }

    $backtrace = debug_backtrace();
    $exception_message .= "Timeout thrown by " . $backtrace[1]['class'] . "::" . $backtrace[1]['function'] . "()\n";
    if (array_key_exists('file', $backtrace[1])) {
      $exception_message .= $backtrace[1]['file'];
    }
    if (array_key_exists('line', $backtrace[1])) {
      $exception_message .= ", line " . $backtrace[1]['line'];
    }
    throw new Exception($exception_message);
  }

  /**
   * Make a hint for the position of the value within the options list.
   *
   * @param string $options_str
   *    The various options as one long string.
   * @param string $value
   *    The option value to find and make a hint.
   *
   * @return bool|float
   *    FALSE if not found; A value between 0.0 and 1.0 if found.
   */
  private function getHint($options_str, $value) {
    // Clean regex symbols from the value so that strpos will work.
    $cleaned = stripslashes($value);
    $cleaned = str_replace("$", "", $cleaned);
    $cleaned = str_replace("^", "", $cleaned);
    // Find the position within the long string that matches the item string.
    $option_pos = strpos($options_str, $cleaned);
    if (FALSE === $option_pos) {
      return FALSE;
    }
    // Calculate the probable position as a hint between 0.0 and 1.0.
    return $option_pos / strlen($options_str);
  }

} // FeatureContext
