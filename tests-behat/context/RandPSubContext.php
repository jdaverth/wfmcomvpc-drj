<?php
//require_once 'FeatureContext.php';.

use Drupal\DrupalExtension\Context\DrupalSubContextInterface;
use Behat\Behat\Context\BehatContext;
use Behat\Gherkin\Node\TableNode;
use \Behat\Gherkin\Node\PyStringNode;

class RandPSubContext extends BehatContext implements DrupalSubContextInterface {

  /**
   * RandPSubContext constructor.
   *
   * @param array|null $parameters
   *    Not used at this time.
   */
  public function __construct(array $parameters = NULL) {
    // Do subcontext initialization.
  }

  /**
   * Required subcontext alias.
   *
   * @return string
   *    The subcontext's alias.
   */
  public static function getAlias() {
    return 'RandP';
  }

  // Convenience constants to improve readability.
  const MULTIPLE_RESULTS = TRUE;
  const QUASH_EXCEPTION  = TRUE;

  /*
   * Constants and functions to define and setup the Admin users.
   * The values are used for initializing the user profiles and for login.
   * Usernames/passwords must be registered in runtime environment before use!
   */
  // One Store Admin.
  const LOCAL_ADMIN_ROLE  = 'local admin';
  const LOCAL_NAME        = 'TestLocal1';
  const LOCAL_PW          = 'testM3.';
  const LOCAL_EMAIL       = 'testlocal1@aut.com';
  const LOCAL_PERMISSION  = 'LMR (Lamar)';
  const LOCAL_STORE       = 'LMR (Lamar)';

  // Two Store Admin.
  const BILOCAL_ADMIN_ROLE  = 'two store admin';
  const BILOCAL_NAME        = 'TestLocalMulti';
  const BILOCAL_PW          = 'testM3.';
  const BILOCAL_EMAIL       = 'TestLocalMulti@aut.com';
  const BILOCAL_PERMISSION  = 'ABT (Arbor Trails), LMR (Lamar)';
  const BILOCAL_STORE       = 'ABT (Arbor Trails)';

  // A metro is a group of stores within an area like a city.
  const METRO_ADMIN_ROLE  = 'metro admin';
  const METRO_NAME        = 'TestLocalMetro';
  const METRO_PW          = 'testM3.';
  const METRO_EMAIL       = 'TestLocalMetro@aut.com';
  const METRO_PERMISSION  = 'Austin';
  const METRO_STORE       = 'GWY (Gateway)';

  // A region is a geographical area made up of individual stores and metros.
  const REGIONAL_ADMIN_ROLE  = 'regional admin';
  const REGIONAL_NAME        = 'TestRegionalAdmin';
  const REGIONAL_PW          = 'testM3.';
  const REGIONAL_EMAIL       = 'TestRegionalAdmin@aut.com';
  const REGIONAL_PERMISSION  = 'SW (Southwest)';
  const REGIONAL_STORE       = 'BEE (Bee Cave)';

  // Global admin.
  const GLOBAL_ADMIN_ROLE  = 'global admin';
  const GLOBAL_NAME        = 'TestGlobalAdmin';
  const GLOBAL_PW          = 'testM3.';
  const GLOBAL_EMAIL       = 'TestGlobalAdmin@aut.com';

  // Blog Admin.
  const BLOG_ADMIN_ROLE  = 'blog admin';
  const BLOG_ADMIN_NAME  = 'BlogAdminTest';
  const BLOG_ADMIN_PW    = 'testM3.';
  const BLOG_ADMIN_EMAIL = 'BlogAdminTest@aut.com';

  // Blog User.
  const BLOG_AUTH_ROLE  = 'blog author';
  const BLOG_AUTH_NAME  = 'BlogAuthorTest';
  const BLOG_AUTH_PW    = 'testM3.';
  const BLOG_AUTH_EMAIL = 'BlogAuthorTest@aut.com';

  // Drupal Admin.
  const DRUPAL_ADMIN_ROLE  = 'drupal admin';
  const DRUPAL_ADMIN_NAME  = 'DrupalAdmin';
  const DRUPAL_ADMIN_PW    = 'a valid password';
  const DRUPAL_ADMIN_EMAIL = "the drupal admin's email address";

  // Each Admin user is represented by a dictionary.
  static private $LOCAL_ADMIN = array(
    'name' => self::LOCAL_NAME,
    'mail' => self::LOCAL_EMAIL,
    'pass' => self::LOCAL_PW,
    'perm' => self::LOCAL_PERMISSION,
    'home' => self::LOCAL_STORE,
    'role' => self::LOCAL_ADMIN_ROLE,
  );

  static private $BILOCAL_ADMIN = array(
    'name' => self::BILOCAL_NAME,
    'mail' => self::BILOCAL_EMAIL,
    'pass' => self::BILOCAL_PW,
    'perm' => self::BILOCAL_PERMISSION,
    'home' => self::BILOCAL_STORE,
    'role' => self::BILOCAL_ADMIN_ROLE,
  );

  static private $METRO_ADMIN = array(
    'name' => self::METRO_NAME,
    'mail' => self::METRO_EMAIL,
    'pass' => self::METRO_PW,
    'perm' => self::METRO_PERMISSION,
    'home' => self::METRO_STORE,
    'role' => self::METRO_ADMIN_ROLE,
  );

  static private $REGIONAL_ADMIN = array(
    'name' => self::REGIONAL_NAME,
    'mail' => self::REGIONAL_EMAIL,
    'pass' => self::REGIONAL_PW,
    'perm' => self::REGIONAL_PERMISSION,
    'home' => self::REGIONAL_STORE,
    'role' => self::REGIONAL_ADMIN_ROLE,
  );

  static private $GLOBAL_ADMIN = array(
    'name' => self::GLOBAL_NAME,
    'mail' => self::GLOBAL_EMAIL,
    'pass' => self::GLOBAL_PW,
    'perm' => "",
    'home' => "",
    'role' => self::GLOBAL_ADMIN_ROLE,
  );

  static private $BLOG_ADMIN = array(
    'name' => self::BLOG_ADMIN_NAME,
    'mail' => self::BLOG_ADMIN_EMAIL,
    'pass' => self::BLOG_ADMIN_PW,
    'perm' => "",
    'home' => "",
    'role' => self::BLOG_ADMIN_ROLE,
  );

  static private $BLOG_AUTH = array(
    'name' => self::BLOG_AUTH_NAME,
    'mail' => self::BLOG_AUTH_EMAIL,
    'pass' => self::BLOG_AUTH_PW ,
    'perm' => "",
    'home' => "",
    'role' => self::BLOG_AUTH_ROLE,
  );

  static private $DRUPAL_ADMIN = array(
    'name' => self::DRUPAL_ADMIN_NAME,
    'mail' => self::DRUPAL_ADMIN_EMAIL ,
    'pass' => self::DRUPAL_ADMIN_PW,
    'role' => self::DRUPAL_ADMIN_ROLE,
  );

  /* THIS CONSTRUCT IS INVALID IN PHP 5.5.xx  but is VALID in PHP 5.6.xx
   * Using this type of data structure can be used to remove switch statements.
  // User logins indexed by role
    static $LOGINS_BY_ROLE = [
    self::LOCAL_ADMIN_ROLE    => self::$LOCAL_ADMIN      ,
    self::BILOCAL_ADMIN_ROLE  => self::$BILOCAL_ADMIN    ,
    self::METRO_ADMIN_ROLE    => self::$METRO_ADMIN      ,
    self::REGIONAL_ADMIN_ROLE => self::$REGIONAL_ADMIN   ,
    self::GLOBAL_ADMIN_ROLE   => self::$GLOBAL_ADMIN_ROLE,
    self::BLOG_ADMIN_ROLE     => self::$BLOG_ADMIN       ,
    self::BLOG_AUTH_ROLE      => self::$BLOG_AUTH        ,
    self::DRUPAL_ADMIN_ROLE   => self::$DRUPAL_ADMIN     ,
  ];
   */

  /*###########################################################################
  # Step Functions and support functions for initializing the different
  # admin types after a new DB has been loaded.
  ###########################################################################*/

  /**
   * Initialize the attributes of a test role.
   *
   * @Given /^I initialize test role "([^"]*)"$/
   *
   * @param string $role
   *    One of the defined roles Ex. "local admin" or "metro admin" ...etc.
   *
   * @throws Exception
   *    TEST CASE ERROR Unrecognized role.
   */
  public function iInitializeTestRole($role) {
    // Refuse to run in the production environment.
    $this->EndIfProductionURL();

    // Make a user dictionary that matches the requested role.
    switch (strtolower($role)) {
      case self::LOCAL_ADMIN_ROLE:
        $user = self::$LOCAL_ADMIN;
        break;

      case self::BILOCAL_ADMIN_ROLE:
        $user = self::$BILOCAL_ADMIN;
        break;

      case self::METRO_ADMIN_ROLE:
        $user = self::$METRO_ADMIN;
        break;

      case self::REGIONAL_ADMIN_ROLE:
        $user = self::$REGIONAL_ADMIN;
        break;

      case self::GLOBAL_ADMIN_ROLE:
        $user = self::$GLOBAL_ADMIN;
        break;

      case self::BLOG_ADMIN_ROLE:
        $user = self::$BLOG_ADMIN;
        break;

      case self::BLOG_AUTH_ROLE:
        $user = self::$BLOG_AUTH;
        break;

      case self::DRUPAL_ADMIN_ROLE:
        $user = self::$DRUPAL_ADMIN;
        break;

      default:
        throw new Exception("Test Case Error: Unrecognized role '$role'");
    }
    // Initialize the admin user that matches the requested role.
    $this->InitializeProfile($user);
  }

  /*
   * People/Profile  Constants
   */
  const PEOPLE_FRAME        = "iframe[title*=\"People \"]";
  const PEOPLE_FORM         = "#views-exposed-form-admin-people-page";
  const PEOPLE_APPLY        = "#edit-submit-admin-people";
  const EMAIL_ADDR          = "#edit-mail";
  const RESULTS_FIELD_EMAIL = "td.views-field-mail a";

  /**
   * Initialize a person's profile.
   *
   * Use the People form to find the specified person,
   * then initialize the person's profile.
   *
   * @param array $person_data
   *   A person's profile values.
   *
   * @throws exception
   *   EMAIL ADDRESS NOT FOUND.
   */
  private function InitializeProfile($person_data) {
    $count = count($this->getMainContext()->resolveSelector(self::TABLE_ROWS, NULL, self::MULTIPLE_RESULTS));

    // Perform a person search using email address.
    $email_addr = $person_data["mail"];
    $search_cmd = [self::EMAIL_ADDR => $email_addr, self::PEOPLE_APPLY => "click"];
    $this->getMainContext()->formLoader(self::PEOPLE_FORM, $search_cmd);

    // The URL will not change, so wait for results table to update.
    for ($i = 0; $i < 20; $i++) {
      sleep(1);
      $results = $this->getMainContext()->resolveSelector(self::TABLE_ROWS, NULL, self::MULTIPLE_RESULTS);
      if ($count !== count($results)) {
        break;
      }
    }

    // Search the results table to find the row with person's email address.
    $result_row = $this->findResultsRowInColumn($email_addr, self::RESULTS_FIELD_EMAIL);
    if ($result_row == FALSE) {
      throw new exception("EMAIL ADDRESS NOT FOUND in People Search Table: '$email_addr'");
    }

    // Open the Profile Editor.
    $open_editor = self::TABLE_ROWS . self::RESULTS_FIELD_EDIT_PERSON;
    $this->getMainContext()->setSelector($open_editor);
  }

  /*
   * Edit People Profile Constants
   */
  const PROFILE_FRAME         = "iframe[title*=\"People \"]";
  const PROFILE_FORM          = "#user-profile-form";
  const PROFILE_USERNAME      = "#edit-name";
  const PROFILE_GIVEN_NAME    = "#edit-field-given-name-und-0-value";
  const PROFILE_ROLES         = "[id^='edit-roles-']";
  const PROFILE_TIMEZONE      = "#edit-mail";
  const PROFILE_PERMISSIONS   = "jQuery('#edit-locations-acl-user-und')";
  const PROFILE_HOME          = "jQuery('#edit-locations-acl-und')";
  const PROFILE_LOCAL_ADMIN   = "#edit-roles-7";
  const PROFILE_GLOBAL_ADMIN  = "#edit-roles-6";
  const PROFILE_ADMINISTRATOR = "#edit-roles-3";
  const PROFILE_BLOG_ADMIN    = "#edit-roles-4";
  const PROFILE_BLOG_AUTHOR   = "#edit-roles-5";

  /**
   * EditProfile: Set values in the person's profile page.
   *
   * @param array $profile_data
   *   A dictionary of key => value pairs that describe a profile.
   *
   */
  private function EditProfile($profile_data) {
    // Build a list of actions that set specific profile values.
    $profile = [
      self::PROFILE_USERNAME     => $profile_data["name"],
      self::PROFILE_GIVEN_NAME   => $profile_data["name"],
      self::PROFILE_PERMISSIONS  => $profile_data["perm"],
      self::PROFILE_HOME         => $profile_data["home"],
    ];

    // If present, add role data to the profile.
    // (a user may have multiple roles separated by '&')
    if (array_key_exists("role", $profile_data)) {
      $roles = explode("&", $profile_data["role"]);
      foreach ($roles as $role) {
        switch (strtolower(trim($role))) {
          // Bi-Local Metro and Regional roles are all local content admins.
          case self::LOCAL_ADMIN_ROLE:
          case self::BILOCAL_ADMIN_ROLE:
          case self::METRO_ADMIN_ROLE:
          case self::REGIONAL_ADMIN_ROLE:
            $profile[self::PROFILE_LOCAL_ADMIN] = "check";
            break;

          case self::DRUPAL_ADMIN_ROLE:
            $profile[self::PROFILE_ADMINISTRATOR] = "check";
            break;

          case self::GLOBAL_ADMIN_ROLE:
            $profile[self::PROFILE_GLOBAL_ADMIN] = "check";
            break;

          case self::BLOG_ADMIN_ROLE:
            $profile[self::PROFILE_BLOG_ADMIN] = "check";
            break;

          case self::BLOG_AUTH_ROLE:
            $profile[self::PROFILE_BLOG_AUTHOR] = "check";
            break;
        }
      }
    }
    // Append the Save command to the profile commands list then load the form.
    $profile[self::SAVE] = "click";
    $this->getMainContext()->formLoader(self::PROFILE_FORM, $profile);
  }

  /**
   * Login as a specific type of role.
   *
   * @Given /^I am logged in as a "([^"]*)"$/
   *
   * @param string $role
   *    One of the defined roles Ex. "local admin" or "metro admin" ...etc.
   *
   * @throws Exception
   */
  public function iAmLoggedInAsA($role) {
    // Refuse to run in the production environment.
    $this->EndIfProductionURL();

    // Select the user to login based upon the role.
    switch (strtolower($role)) {
      case self::LOCAL_ADMIN_ROLE:
        $user = self::$LOCAL_ADMIN;
        break;

      case self::BILOCAL_ADMIN_ROLE:
        $user = self::$BILOCAL_ADMIN;
        break;

      case self::METRO_ADMIN_ROLE:
        $user = self::$METRO_ADMIN;
        break;

      case self::REGIONAL_ADMIN_ROLE:
        $user = self::$REGIONAL_ADMIN;
        break;

      case self::GLOBAL_ADMIN_ROLE:
        $user = self::$GLOBAL_ADMIN;
        break;

      case self::BLOG_ADMIN_ROLE:
        $user = self::$BLOG_ADMIN;
        break;

      case self::BLOG_AUTH_ROLE:
        $user = self::$BLOG_AUTH;
        break;

      case self::DRUPAL_ADMIN_ROLE:
        $user = self::$DRUPAL_ADMIN;
        break;

      default:
        throw new Exception("Test Case Error: Unrecognized role '$role'");
    }

    // If user is already logged in, then simply return.
    $page_text = $this->getMainContext()->getSession()->getPage()->getText();
    if (FALSE !== stristr($page_text, "Hello " . $user['name'])) {
      return;
    }

    // If a different user is logged in,then click their sign out link.
    $sign_out = $this->getMainContext()->getSession()->getPage()->find('css', "[href='/user/logout']");
    if ($sign_out) {
      $sign_out->click();
    }

    // Go to the sign in page and login the specified user.
    $this->getMainContext()->login($user);
  }

  /*
   * CONSTANTS, STEPS and FUNCTIONS associated with Admin toolbar(s) or
   * elements relatively common to all admin pages.
   */

  // Admin Menu Toolbar items.
  const ADMIN_TOOLBAR       = "#toolbar";
  const ADMIN_HOME          = "#toolbar-home";
  const ADMIN_USER          = "#toolbar-user";
  const ADMIN_TOOLBAR_MENU  = "#toolbar-menu";
  const ADMIN_DASHBOARD     = "#toolbar-link-admin-dashboard";
  const ADMIN_CONTENT       = "#toolbar-link-admin-content";
  const ADMIN_STRUCTURE     = "#toolbar-link-admin-structure";
  const ADMIN_APPEARANCE    = "#toolbar-link-admin-appearance";
  const ADMIN_PEOPLE        = "#toolbar-link-admin-people";
  const ADMIN_MODULES       = "#toolbar-link-admin-modules";
  const ADMIN_CONFIGURATION = "#toolbar-link-admin-config";
  const ADMIN_REPORTS       = "#toolbar-link-admin-reports";
  const ADMIN_ADVANCED_HELP = "#toolbar-link-admin-advanced_help";
  const ADMIN_HELP          = "#toolbar-link-admin-help";
  const ADMIN_DRAWER        = "div .toolbar-drawer";

  const ERROR_MESSAGE = ".messages.error";
  const OVERLAY_CLOSE = "#overlay-close";
  const ACTIVE_FRAME  = "iframe[class~=overlay-active]";

  /**
   * Wait for a named constant (representing a CSS selector) to exist.
   *
   * @When /^I wait for admin element "([^"]*)"$/
   *
   * @param string $constant
   *   A constant defined within this class, that represents a CSS selector.
   *
   * @throws exception
   *   A bad constant name results in an Undefined class constant exception.
   */
  public function iWaitForAdminElement($constant) {
    // Refuse to run in the production environment.
    $this->EndIfProductionURL();

    // Convert the element name into its local selector constant.
    // Then resolveSelector to wait for it to come into existence or timeout.
    $selector = eval("return self::$constant;");
    $this->getMainContext()->resolveSelector($selector);
  }

  /**
   * Verify a string appears on an admin page.
   *
   * @When /^I should see admin message "([^"]*)"$/
   *
   * @param string $message
   *   The string to verify that it appears on the web page.
   *
   * @throws exception
   *    MESSAGE NOT FOUND.
   */
  public function iShouldSeeAdminMessage($message) {
    // Refuse to run in the production environment.
    $this->EndIfProductionURL();

    // Check for message anywhere within the page.
    // Take screenshot and raise error if not found.
    $page_text = $this->getMainContext()->getSession()->getPage()->getText();
    if (FALSE === strpos($page_text, $message)) {
      $screenShot = $this->getMainContext()->putScreenshot("Message not found");
      throw new Exception("MESSAGE NOT FOUND: '$message' see image at $screenShot");
    }
  }

  /**
   * Wait for the named constant (representing a CSS selector) to exist.
   *
   * @When /^I set admin element "([^"]*)" to "([^"]*)"$/
   * @When /^I fill admin element "([^"]*)" with "([^"]*)"$/
   * @When /^I select admin element "([^"]*)"$/
   *
   * @param string $constant
   *   A constant defined within this class, that represents a CSS selector.
   * @param string $value
   *   The value or action use (actions are like 'click', 'check', 'uncheck').
   *
   * @throws exception
   *   A bad constant name results in an Undefined class constant exception.
   */
  public function iSetAdminElementTo($constant, $value = NULL) {
    // Refuse to run in the production environment.
    $this->EndIfProductionURL();

    // Convert the element name into its local selector constant.
    // Then set the selector.
    $cmd = "return self::$constant;";
    $selector = eval($cmd);
    $this->getMainContext()->setSelector($selector, $value);
  }

  /**
   * Verify that the given text appears in a given element constant.
   *
   * @Then /^I should see admin text "([^"]*)" in element "([^"]*)"$/
   * @Then /^I should see admin text "([^"]*)"$/
   *
   * @param string $text
   *   The expected text to find.
   * @param string $el_constant
   *    A defined constant within this class, representing a CSS selector.
   *
   * @throws exception
   *   A bad constant name results in an Undefined class constant exception.
   */
  public function iShouldSeeAdminTextInElement($text, $el_constant = NULL) {
    // Refuse to run in the production environment.
    $this->EndIfProductionURL();

    // If no element constant specified, then then use 'body' as the selector.
    // Otherwise convert the element constant into its local selector value.
    if (NULL == $el_constant) {
      $selector = 'body';
    }
    else {
      $selector = eval("return self::$el_constant;");
    }
    $this->getMainContext()->iShouldSeeTheTextInTheContainer($text, $selector);
  }

  /*
   * ADD CONTENT: CONSTANTS, STEP AND SUPPORT FUNCTIONS
   */
  const ADD_CONTENT_FRAME  = "iframe[title*=\"Add content\"]";
  const OVERLAY_CONTENT    = "#overlay-content";
  const ADD_CONTENT        = "#content a";
  const ADD_CONTENT_PAGE   = "/node/add";
  const ADMIN_CONTENT_PAGE = "/admin/content";

  /**
   * Go to the given action item in the in the given action list.
   *
   * @Given /^I follow admin toolbar item "([^"]*)"$/
   *
   * @param string $toolbar_item
   *    The Admin toolbar menu item that opens the action list.
   * @param string $action
   *    The specific action to pick in the action menu.
   *
   * @throws Exception
   *    TEST CASE ERROR Unknown admin toolbar item.
   */
  public function iFolowAdminToolbarItem($toolbar_item, $action = NULL) {
    // Refuse to run in the production environment.
    $this->EndIfProductionURL();

    // Determine which action menu is to be used.
    switch (strtoupper($toolbar_item)) {
      case "CONTENT":
        $path = $this->getMainContext()->locatePath(self::ADMIN_CONTENT_PAGE);
        $this->getMainContext()->getSession()->visit($path);
        // Ensure Content form is accessible before leaving.
        $this->getMainContext()->resolveSelector(self::CONTENT_FORM);
        break;

      default:
        throw new Exception("TEST CASE ERROR: Unknown admin toolbar item '$toolbar_item'");
    }
  }

  // Direct links to Create/Edit Content pages.
  private static $CREATE_CONTENT_ACTIONS = array(
    "BLOG" => "node/add/blog",
    "BLOG POST" => "node/add/blog-post",
    "BRAND CAMP ARTICLE" => "node/add/bc-article",
    "BRAND CAMP COUPON" => "node/add/bc-coupon",
    "BRAND CAMP IMAGE" => "node/add/bc-image",
    "BRAND CAMP SPECIAL" => "node/add/bc-connect",
    "BRAND CAMP VIDEO" => "node/add/bc-video",
    "COUPON" => "node/add/coupon",
    "DEPARTMENT" => "node/add/department",
    "DEPARTMENT ARTICLE" => "node/add/department-article",
    "EVENT" => "node/add/event",
    "JOB POSITION" => "node/add/job",
    "LOCAL VENDOR" => "node/add/local-vendor",
    "MARQUEE" => "node/add/marquee",
    "METRO" => "node/add/metro",
    "NATIONAL OFFICES" => "node/add/national-offices",
    "NEWSLETTER" => "node/add/newsletter",
    "PAGE" => "node/add/page",
    "PERSON" => "node/add/person",
    "PRODUCT" => "node/add/product",
    "PRODUCT CERTIFICATION" => "node/add/product-certification",
    "PRODUCT LINE" => "node/add/product-line",
    "PROMO" => "node/add/promo",
    "RECIPE" => "node/add/recipe",
    "REGION" => "node/add/region",
    "SERVICE" => "node/add/service",
    "SPECIAL DIET" => "node/add/special-diet",
    "STORE" => "node/add/store",
    "VIDEO" => "node/add/video",
    "WEBFORM" => "node/add/webform",
  );

  /**
   * Go to the given action item in the in the given action list.
   *
   * @Given /^I follow "([^"]*)" in the "([^"]*)" action list$/
   * @Given /^I follow "([^"]*)" in the  Admin toolbar$/
   *
   * @param string $action
   *    The specific action to pick in the action menu.
   * @param string $action_menu
   *    The Admin toolbar menu item that opens the action list.
   *
   * @throws Exception
   *    TEST CASE ERROR..
   */
  public function iFolowInTheActionList($action, $action_menu = NULL) {
    // Refuse to run in the production environment.
    $this->EndIfProductionURL();

    // Determine which action menu is to be used.
    switch (strtoupper($action_menu)) {
      case "ADD CONTENT":
        $path = $this->getMainContext()->locatePath('/');
        $url = self::$CREATE_CONTENT_ACTIONS[strtoupper($action)];
        $this->getMainContext()->getSession()->visit($path . $url);
        return;

      default:
        throw new Exception("TEST CASE ERROR: Unknown action menu '$action_menu'");
    }
  }

  /**
   * Perform the given list of actions on the open form.
   *
   * @Given /^I process "([^"]*)" with actions:$/
   *
   * @param string $open_form
   *   The currently open form.
   * @param PyStringNode $actions
   *   The list of (action => value,) pairs to execute.
   *
   * @throws Exception
   *   TEST CASE ERROR.
   */
  public function iProcessWithActions($open_form, PyStringNode $actions) {
    // Refuse to run in the production environment.
    $this->EndIfProductionURL();

    $in_frame = FALSE;
    switch (strtoupper($open_form)) {
      case "MANAGE CONTENT":
        $this->ManageContent($actions);
        break;

      case "CREATE EVENT":
      case "EDIT EVENT":
        $this->EditEvent($actions);
        break;

      case "CREATE SERVICE":
      case "EDIT SERVICE":
        $this->EditService($actions);
        break;

      case "CREATE PERSON":
      case "EDIT PERSON":
        $this->EditSimpleForm(self::PERSON_FORM, $actions, self::CREATE_PERSON_FRAME);
        break;

      case "CREATE BLOG":
      case "EDIT BLOG":
        $this->EditSimpleForm(self::BLOG_FORM, $actions, self::CREATE_BLOG_FRAME);
        break;

      case "CREATE BLOG POST":
      case "EDIT BLOG POST":
        $this->EditSimpleForm(self::BLOG_POST_FORM, $actions, self::CREATE_BLOG_POST_FRAME);
        break;

      case "CREATE BRAND CAMP ARTICLE":
      case "EDIT BRAND CAMP ARTICLE":
        $this->EditSimpleForm(self::BRAND_CAMP_ARTICLE_FORM, $actions, self::CREATE_BRAND_CAMP_ARTICLE_FRAME);
        break;

      case "CREATE BRAND CAMP COUPON":
      case "EDIT BRAND CAMP COUPON":
        $this->EditSimpleForm(self::BRAND_CAMP_COUPON_FORM, $actions, self::CREATE_BRAND_CAMP_COUPON_FRAME);
        break;

      case "CREATE BRAND CAMP IMAGE":
      case "EDIT BRAND CAMP IMAGE":
        $this->EditSimpleForm(self::BRAND_CAMP_IMAGE_FORM, $actions, self::CREATE_BRAND_CAMP_IMAGE_FRAME);
        break;

      case "CREATE BRAND CAMP SPECIAL":
      case "EDIT BRAND CAMP SPECIAL":
        $this->EditSimpleForm(self::BRAND_CAMP_SPECIAL_FORM, $actions, self::BRAND_CAMP_SPECIAL_FORM);
        break;

      case "CREATE BRAND CAMP VIDEO":
      case "EDIT BRAND CAMP VIDEO":
        $this->EditSimpleForm(self::BRAND_CAMP_VIDEO_FORM, $actions, self::CREATE_BRAND_CAMP_VIDEO_FRAME);
        break;

      case "CREATE DEPARTMENT ARTICLE":
      case "EDIT DEPARTMENT ARTICLE":
        $this->EditSimpleForm(self::DEPARTMENT_ARTICLE_FORM, $actions, self::CREATE_DEPARTMENT_ARTICLE_FRAME);
        break;

      case "CREATE DEPARTMENT":
      case "EDIT DEPARTMENT":
        $this->EditSimpleForm(self::DEPARTMENT_FORM, $actions, self::CREATE_DEPARTMENT_FRAME);
        break;

      case "CREATE JOB POSITION":
      case "EDIT JOB POSITION":
        $this->EditSimpleForm(self::JOB_POSITION_FORM, $actions, self::CREATE_JOB_POSITION_FRAME);
        break;

      case "CREATE LOCAL VENDOR":
      case "EDIT LOCAL VENDOR":
        $this->EditSimpleForm(self::LOCAL_VENDOR_FORM, $actions, self::CREATE_LOCAL_VENDOR_FRAME);
        break;

      case "CREATE MARQUEE":
      case "EDIT MARQUEE":
        $this->EditMarquee(self::MARQUEE_FORM, $actions);
        break;

      case "CREATE METRO":
      case "EDIT METRO":
        $this->EditSimpleForm(self::METRO_FORM, $actions, self::CREATE_METRO_FRAME);
        break;

      case "CREATE NATIONAL OFFICES":
      case "EDIT NATIONAL OFFICES":
        $this->EditSimpleForm(self::NATIONAL_OFFICES_FORM, $actions, self::CREATE_NATIONAL_OFFICES_FRAME);
        break;

      case "CREATE NEWSLETTER":
      case "EDIT NEWSLETTER":
        $this->EditSimpleForm(self::NEWSLETTER_FORM, $actions, self::CREATE_NEWSLETTER_FRAME);
        break;

      case "CREATE PAGE":
      case "EDIT PAGE":
        $this->EditSimpleForm(self::PAGE_FORM, $actions, self::CREATE_PAGE_FRAME);
        break;

      case "CREATE PRODUCT":
      case "EDIT PRODUCT":
        $this->EditSimpleForm(self::PRODUCT_FORM, $actions, self::CREATE_PRODUCT_FRAME);
        break;

      case "CREATE PRODUCT CERTIFICATION":
      case "EDIT PRODUCT CERTIFICATION":
        $this->EditSimpleForm(self::PRODUCT_CERTIFICATION_FORM, $actions, self::CREATE_PRODUCT_CERTIFICATION_FRAME);
        break;

      case "CREATE PRODUCT LINE":
      case "EDIT PRODUCT LINE":
        $this->EditSimpleForm(self::PRODUCT_LINE_FORM, $actions, self::CREATE_PRODUCT_LINE_FRAME);
        break;

      case "CREATE PROMO":
      case "EDIT PROMO":
        $this->EditSimpleForm(self::PROMO_FORM, $actions, self::CREATE_PROMO_FRAME);
        break;

      case "CREATE RECIPE":
      case "EDIT RECIPE":
        $this->EditSimpleForm(self::RECIPE_FORM, $actions, self::CREATE_RECIPE_FRAME);
        break;

      case "CREATE REGION":
      case "EDIT REGION":
        $this->EditSimpleForm(self::REGION_FORM, $actions, self::CREATE_REGION_FRAME);
        break;

      case "CREATE SPECIAL DIET":
      case "EDIT SPECIAL DIET":
        $this->EditSimpleForm(self::SPECIAL_DIET_FORM, $actions, self::CREATE_SPECIAL_DIET_FRAME);
        break;

      case "CREATE STORE":
      case "EDIT STORE":
        $this->EditSimpleForm(self::STORE_FORM, $actions, self::CREATE_STORE_FRAME);
        break;

      case "CREATE VIDEO":
      case "EDIT VIDEO":
        $this->EditSimpleForm(self::VIDEO_FORM, $actions, self::CREATE_VIDEO_FRAME);
        break;

      case "CREATE WEBFORM":
      case "EDIT WEBFORM":
        $this->EditSimpleForm(self::WEBFORM_FORM, $actions, self::CREATE_WEBFORM_FRAME);
        break;

      default:
        throw new Exception("TEST CASE ERROR: Unknown form '$open_form'");
    }
  }

  /*
   * Search Results Table Constants
   */
  const RESULT_FIELD_NID  = "td.views-field-nid";
  const RESULT_SELECT_ALL = ".vbo-table-select-all.form-checkbox";
  const RESULTS_FIELD_TITLE = ".views-field-title a";
  const RESULTS_FIELD_EDIT = " a[href^='/node/']";
  const RESULTS_FIELD_EDIT_PERSON = " a[href*='edit?']";
  const FIRST_RESULT = 1;

  /**
   * Find content type by title and perform an operation on the first result.
   *
   * @When /^I find content "([^"]*)" titled "([^"]*)" to "([^"]*)"$/
   *
   * @param string $content_type
   *   The content type selected for this search.
   * @param string $title
   *   The title of the content.
   * @param string $action
   *   The action/operation to be performed on the result.
   */
  public function iFindContentTitledTo($content_type, $title, $action) {
    // Do not run in production website.
    $this->EndIfProductionURL();

    // Manipulate the Content Search control to find the given content title.
    // which will be returned in the results table control.
    $search_cmd = "CONTENT_TITLE => \"$title\"         ,
                   CONTENT_TYPE  => \"^$content_type$\",
                   APPLY         => \"click\"          ";

    $this->iSearchContentForMatchedTo(new \Behat\Gherkin\Node\PyStringNode($search_cmd));

    $this->getMainContext()->resolveSelector(self::CONTENT_FORM);

    // The search results table is now searched to see if it contains $title.
    $result_row = $this->findResultsRowInColumn($title, self::RESULTS_FIELD_TITLE);
    if ($result_row == FALSE) {
      throw new exception("ITEM NOT FOUND in Content table: $content_type '$title'");
    }

    // Edit actions open a content editor and then return.
    if (strcasecmp("edit", $action) == 0) {
      $open_editor = self::TABLE_ROWS . self::RESULTS_FIELD_EDIT;
      $this->getMainContext()->setSelector($open_editor, NULL, NULL, self::FIRST_RESULT);
      return;
    }

    // Mark the row, perform operation and confirm.
    $this->getMainContext()->setSelector(self::OPERATIONS, $action);
    $this->getMainContext()->setSelector(self::ROW_CHECKBOX, "check", NULL, self::FIRST_RESULT);
    $this->getMainContext()->setSelector(self::EXECUTE);
    $this->getMainContext()->setSelector(self::CONFIRM);
  }

  /**
   * Perform an operation on the search results.
   *
   * @When /^I apply operation "([^"]*)" to "([^"]*)" search results?$/
   *
   * @param string $operation_type
   *   A matching text for one of the valid operations or "COUNT".
   * @param string $items
   *   "ALL" to apply to all visible results.
   *
   * @return int|void
   *   If the operation is "COUNT",
   *   then the visible results row count is returned.
   *   Otherwise nothing is returned.
   */
  public function iApplyOperationToSearchResult($operation_type, $items) {
    // Do not run in production website.
    $this->EndIfProductionURL();

    $this->getMainContext()->resolveSelector(self::CONTENT_FORM);

    // The search results table is now searched to see if it contains $title.
    $results = $this->getMainContext()->resolveSelector(self::RESULT_FIELD_NID, NULL, self::MULTIPLE_RESULTS, self::QUASH_EXCEPTION);
    if ($results === FALSE) {
      // No results, so nothing to do.
      return 0;
    }

    // The count is all that is required then return it.
    $result_count = count($results);
    if (stristr($operation_type, "COUNT") !== FALSE) {
      return $result_count;
    }

    // If editing is being done then edit only the first row.
    if (strcasecmp("edit", $operation_type) == 0) {
      $open_editor = self::TABLE_ROWS . self::RESULTS_FIELD_EDIT;
      $this->getMainContext()->setSelector($open_editor);
      return;
    }

    // Select the result items to be operated upon.
    if (stristr($items, "ALL") !== FALSE) {
      $this->getMainContext()->setSelector(self::RESULT_SELECT_ALL, "click");
    }
    else {
      // @todo add code to select specific individual rows.
      NULL;
    }

    // Select the operation, execute, and confirm it.
    $this->getMainContext()->setSelector(self::OPERATIONS, $operation_type);
    $this->getMainContext()->setSelector(self::EXECUTE);
    $this->getMainContext()->setSelector(self::CONFIRM);
  }

  /**
   * Wait for up to one minute for a Content manager action to complete.
   *
   * @When /^I wait for action to finish$/
   */
  function iWaitForActionToFinish() {
    $processing = TRUE;
    $timeout = 60;
    while ($processing and $timeout--) {
      sleep(1);
      $url = $this->getMainContext()->getSession()->getCurrentUrl();
      $processing = (0 < stripos($url, "batch"));
    }
  }

  /**
   * Perform a content search.
   *
   * @When /^I search content for matches to :$/
   *
   * @param string $matches
   *   The pattern to match.
   */
  public function iSearchContentForMatchedTo($matches) {
    // Do not run in production website.
    $this->EndIfProductionURL();

    // Convert the string data into an array of "selector=>value".
    $data_array = $this->eval_data($matches);

    // Count is being used to indicate that the search process has completed.
    // So get an initial count to compare against later.
    $count = count($this->getMainContext()->resolveSelector(self::TABLE_ROWS, NULL, self::MULTIPLE_RESULTS));

    // Execute the Content search actions.
    $this->getMainContext()->formLoader(self::CONTENT_FORM, $data_array);

    // The URL may not change, so wait for results table to update.
    for ($i = 0; $i < 20; $i++) {
      sleep(1);
      if ($count !== count($this->getMainContext()->resolveSelector(self::TABLE_ROWS, NULL, self::MULTIPLE_RESULTS))) {
        break;
      }
    }
  }

  /**
   * Select a file in the media browser.
   *
   * @When /^I select media from tab "([^"]*)" file "([^"]*)"$/
   *
   * @param string $tab
   *    Name of the media browser tab to select.
   * @param string $file
   *    Name of the image file to select.
   *    Name can be "ANY" to select a non-specific file.
   */
  public function iSelectMediaFromTabFile($tab, $file) {
    // Refuse to run in production.
    $this->EndIfProductionURL();
    $this->selectImage($file, $tab);
  }

  /*
   *  Vertical Tab Constants
   */
  const SERVICE_TABS  = ".vertical-tabs-list a";

  /**
   * Select a specific vertical tab by name.
   *
   * @When /^I select vertical tab "([^"]*)"$/
   *
   * Some admin screens have vertical tabs along their left edge.
   * Allow selection of vertical tab by text visible to user.
   *
   * @param string $tab_text
   *    Name of tab (text seen by the admin user).
   *
   * @throws Exception
   *    TEST CASE ERROR unknown tab.
   */
  public function iSelectVerticalTab($tab_text) {
    // Refuse to run in production.
    $this->EndIfProductionURL();

    $in_active_frame = FALSE;

    $tabs = $this->getMainContext()->resolveSelector(self::SERVICE_TABS, NULL, self::MULTIPLE_RESULTS);
    $i = 0;
    foreach ($tabs as $tab) {
      $i++;
      if (strcasecmp($tab->getText(), $tab_text) == 0) {
        if ($in_active_frame) {
          $this->getMainContext()->setElem($tab, NULL, self::SERVICE_TABS);
        }
        else {
          $jquery_click = "jQuery('.vertical-tabs-list li:nth-child($i) a').click();";
          $this->getMainContext()->getSession()->executeScript($jquery_click);
        }
        return;
      }
    }
    throw new Exception("TEST CASE ERROR: Tab '$tab_text' Not found.");
  }

  /*
   * ADMIN CONTENT Constant
   */
  const ADMIN_CONTENT_URL = "admin/content";

  /**
   * Search for a Content type at some location(s).
   *
   * @Given /^I search for content "([^"]*)" at location "([^"]*)"$/
   *
   * @param string $content_type
   *    A valid content type name.
   * @param string $location
   *    Name(s) of valid location(s) "name1, name2, ...".
   */
  public function iSearchForContentAtLocation($content_type, $location) {
    // Do not run in production website.
    $this->EndIfProductionURL();

    // Wait for the Content management url.
    for ($i = 0; $i < 20; $i++) {
      $url = $this->getMainContext()->getSession()->getCurrentUrl();
      if (stristr($url, self::ADMIN_CONTENT_URL)) {
        break;
      }
      sleep(1);
    }

    // Wait for the Content form to exist.
    $this->getMainContext()->resolveSelector(self::CONTENT_FORM);

    // If the location is empty,
    // Then a Reset action is applied to clear out any existing location(s).
    if ($location == "") {
      $this->getMainContext()->setSelector(self::RESET, "click");
      sleep(1);
      // Build a command packet to manipulate the Content Search control.
      $search_cmd = "CONTENT_TYPE => \"$content_type\",
                     APPLY        => \"click\"        ";
    }
    else {
      // Build a command packet with any specified location(s)
      // to manipulate the Content Search control.
      $locations_array = explode(',', preg_quote($location));
      $locations_array = array_map("trim", $locations_array);
      $location_str = '"' . implode('", "', $locations_array) . '"';
      $search_cmd = "CONTENT_TYPE     => \"$content_type\",
                     CONTENT_LOCATION => [$location_str]  ,
                     APPLY            => \"click\"        ";
    }

    $this->iSearchContentForMatchedTo(new \Behat\Gherkin\Node\PyStringNode($search_cmd));
  }

  /**
   * Assert the presents/absence of 'edit' tags in the search results.
   *
   * Note: 'disabled' permissions are an automatic PASS
   *  and no results returned are treated as a PASS.
   *
   * @Then /^I should see "([^"]*)" permitted edits$/
   *
   * @param string $permitted
   *   Either 'allowed', 'no' or 'disabled'.
   *
   * @throws Exception
   *   TEST CASE ERROR unknown permission
   *   UNEXPECTED RESULT: wrong permission.
   */
  public function iShouldSeePermittedEdits($permitted) {
    // Do not run in production website.
    $this->EndIfProductionURL();

    // Search results table is searched to see if it contains "edit" tags.
    $edits_selector = self::TABLE_ROWS . self::RESULTS_FIELD_EDIT;
    $edit_count = count($this->getMainContext()->getSession()->getPage()->findall("css", $edits_selector));

    // Based upon the edit counts and the given permission,
    // evaluate the passing conditions.
    switch (strtolower($permitted)) {
      case "no":
        if ($edit_count == 0) {
          // Pass.
          return;
        }
        break;

      case "allow":
        if ($edit_count != 0) {
          // Pass.
          return;
        }
        break;

      case "disabled":
        // Disabled test, automatic pass.
        return;

      default:
        throw new Exception("TESTCASE ERROR: Invalid permission '$permitted'");
    }

    // If no result rows were returned by the search.
    // Then treat it as a PASS, rather than a false fail.
    $select_all_rows = self::TABLE_ROWS . ' ' . self::RESULTS_FIELD_TITLE;
    $row_count = count($this->getMainContext()->getSession()->getPage()->findall("css", $select_all_rows));
    if (($row_count == 0)) {
      return;
    }

    // The test failed if it makes it to here.
    throw new Exception("UNEXPECTED RESULT: There are supposed to be '$permitted edits'.");
  }

  /**
   * Get a valid nodeID for the permission, ALWAYS throws an exception.
   *
   * Note: This is used only when the old node(s) fails.
   *  It is used by iCheckContentTypeAtLocationToDeterminePermission which
   *  produces the result by catching multiple Exceptionwhich will appear in the
   *  output. From there the NodeIDs can be copied into the test script.
   *
   * @param string $permitted
   *    Either 'allow' or 'no'.
   *
   * @throws Exception
   *    ALWAYS throws an exception with Node ID.
   *    The exception is to be caught and used by calling routine.
   */
  private function getNodeID($permitted) {
    $NID = -1;
    $page = $this->getMainContext()->getSession()->getPage();
    $row_count = count($page->findall("css", self::TABLE_ROWS . ' ' . self::RESULTS_FIELD_TITLE));
    if ($row_count > 0) {
      if (strtolower($permitted) == "allow") {
        $NID_row = $page->find("css", self::TABLE_ROWS . self::RESULTS_FIELD_EDIT)->getParent()->getParent();
        $NID = $NID_row->find('css', '.views-field-nid')->getText();
      }
      else {
        // Permitted is "no".
        $NID = $page->find("css", '.views-row-first .views-field-nid')->getText();
      }
    }
    throw new Exception("NodeID = $NID ");
  }

  /**
   * Verify permission of Content by searching its Content type/location.
   *
   * Note: This routine can also be used to produce valid Node IDs for
   * content so that runtime can be reduce when using the "sister" function
   *  iCheckContentByNodeIDToDeterminePermission.
   *
   * @Then /^I check content type at location to determine permission:$/
   *
   * @param TableNode $table
   *   Table of Content Types, Locations, and Permission.
   *
   * @throws Exception
   *   UNEXPECTED RESULT
   */
  public function iCheckContentTypeAtLocationToDeterminePermission(TableNode $table) {
    $exceptions = "";
    $rows = $table->getHash();
    foreach ($rows as $row) {
      // If the permission is disabled ignore the row.
      if (FALSE !== stristr($row["permitted"], "disabled")) {
        // The row was disabled.
        continue;
      }
      // Test the content type for the expected permission.
      try {
        $this->iSearchForContentAtLocation($row["content type"], $row["location"]);

        $this->iShouldSeePermittedEdits(($row["permitted"]));
        /* If Node IDs become invalid then ---
         * Temporarily comment out line above and uncomment line below,
         * to generate a list of new valid nodeIDs in the failed output.
         */
        // $this->getNodeID($row["permitted"]);
      }
      catch (Exception $e) {
        //Group exceptions together so that one does not end the entire process.
        $msg = $e->getMessage() . " for " . $row["content type"] . " at location '" . $row["location"] . "' See: ";
        $exceptions .= $msg . $this->getMainContext()->putScreenshot($row["content type"]) . "\n";
      }
    }
    if ($exceptions !== "") {
      // A compilation of all UNEXPECTED RESULT exceptions from the table.
      throw new Exception($exceptions);
    }
  }

  /*
   * Node ID Constants
   */
  const NODE_EDIT_TAB   = "a.active";
  const NODE_PAGE_TITLE = ".page-title";

  /**
   * Verify expected permission of Content by its Node ID.
   *
   * @Then /^I check content by node ID to determine permission:$/
   *
   * @param TableNode $table
   *   Table of content types, locations and expected permissions.
   *
   * @throws Exception
   *   TEST CASE ERROR invalid expectred permission.
   *   UNEXPECTED RESULT: wrong permission.
   */
  public function iCheckContentByNodeIDToDeterminePermission(TableNode $table) {
    // Do not run in production website.
    $this->EndIfProductionURL();

    $exceptions = "";
    $base_URL = $this->getMainContext()->locatePath('/');
    $rows = $table->getHash();
    foreach ($rows as $row) {
      // If the permission is disabled or the nodeID is -1 then ignore row.
      if (FALSE !== stristr($row["permitted"], "disabled")) {
        continue;
      }
      if (FALSE !== stristr($row["nodeID"], "-1")) {
        continue;
      }

      // Test the content type for the expected permission.
      try {
        $this->getMainContext()->getSession()->visit($base_URL . "node/" . $row["nodeID"] . "/edit");
        $permitted = strtolower($row["permitted"]);
        $page = $this->getMainContext()->getSession()->getPage();
        $page_title = $page->find('css', self::NODE_PAGE_TITLE)->getText();
        $edit_allowed = ("Access denied" <> $page_title);

        switch ($permitted) {
          case "allow":
            if ($edit_allowed) {
              // Pass and continue 'foreach' loop.
              continue 2;
            }
            break;

          case "no":
            if (!$edit_allowed) {
              // Pass and continue 'foreach' loop.
              continue 2;
            }
            break;

          default:
            throw new Exception("TESTCASE ERROR: Invalid permission '$permitted'. Try 'no','allow' or 'disabled' instead");
        }
        // Can only get here if test failed.
        throw new Exception("UNEXPECTED RESULT: There are supposed to be '$permitted edits'.");
      }
      catch (Exception $e) {
        $msg = $e->getMessage() . " for " . $row["content type"] . " with node ID '" . $row["nodeID"] . "' ";
        $exceptions .= $msg . $this->getMainContext()->putScreenshot($row["content type"]) . "\n";
      }
    }
    if ($exceptions !== "") {
      throw new Exception($exceptions);
    }
  }

  /*       #########################################
   *            S u p p o r t   f u n c t i o n s
   *       #########################################
   */

  /*
   * EDIT SERVICE Constants
   */
  const CREATE_SERVICE_FRAME = "iframe[title*=\"Create Service\"]";
  const SERVICE_FORM = "#service-node-form";
  const BASICS_TL_CODE = "#edit-field-service-tlc-und-0-value";
//  const TEASER_SELECT_MEDIA = "#edit-field-service-teaser-image-und-0-select";
  const MARQUEE_SELECT_MEDIA = "#edit-field-heading-background-und-0-select";
  const TEASER_DESCRIPTION = "#edit-field-service-teaser-body-und-0-value";

  /**
   * Fill in and process Service form.
   *
   * @param string $data
   *    The data in '["name" => "value", ...]' format.
   *    "name" corresponds to a defined constant representing a CSS selector.
   */
  private function EditService($data) {
    // Ensure the Event form is accessible.
    // Convert the string data into an array of "selector=>value".
    // Use form loader to fill in values and perform actions.
    $this->getMainContext()->resolveSelector(self::SERVICE_FORM);
    $data_array = $this->eval_data($data);
    $this->getMainContext()->formLoader(self::SERVICE_FORM, $data_array);
  }

  /*
   * EDIT PERSON Constants
   */
  const CREATE_PERSON_FRAME = "iframe[title*=\" Person \"]";
  const PERSON_FORM = "#person-node-form";
  const FULL_NAME = "#edit-title";
  const JOB_TITLE = "#edit-field-person-job-und-0-nid";

  /**
   * @param string $data
   *    The data in '["name" => "value", ...]' format.
   *    "name" corresponds to a defined constant representing a CSS selector.
   */
  private function EditPerson($data) {
    // Ensure the Event form is accessible.
    // Convert the string data into an array of "selector=>value".
    // Use form loader to fill in values and perform actions.
    $this->getMainContext()->resolveSelector(self::PERSON_FORM);
    $data_array = $this->eval_data($data);
    $this->getMainContext()->formLoader(self::PERSON_FORM, $data_array);
  }

  /*
   * EDIT EVENT Constants
   */
  const CREATE_EVENT_FRAME = "iframe[title*=\"Create Event\"]";
  const EVENT_FORM = "#event-node-form";
  const LOCATION_NAME = "#edit-field-location-name-und-0-value";
  const SELECT_EVENT_MEDIA = "#edit-field-event-image-und-0-select";
  const TITLE = "#edit-title";
  const STORE = "jQuery('#edit-field-locations-und')";
  const PERMISSIONS = "jQuery('#edit-locations-acl-und')";
  const PREVIEW = "#edit-preview";
  const SAVE = "#edit-submit";

  /**
   * Fill in and process Event content.
   *
   * @param string $data
   *    The data in '["name" => "value", ...]' format.
   *    "name" corresponds to a defined constant representing a CSS selector.
   */
  private function EditEvent($data) {
    // Ensure the Event form is accessible.
    // Convert the string data into an array of "selector=>value".
    // Use form loader to fill in values and perform actions.
    $this->getMainContext()->resolveSelector(self::EVENT_FORM);
    $data_array = $this->eval_data($data);
    $this->getMainContext()->formLoader(self::EVENT_FORM, $data_array);
  }

  /*
   * SELECT IMAGE Constants
   */
  const MEDIA_BROWSER_FRAME = "#mediaBrowser";
  const MEDIA_UPLOAD = "a[href='#media-tab-upload']";
  const MEDIA_LIBRARY = "a[href='#media-tab-library']";
  const MEDIA_LIBRARY_MBP = "a[href='#media-tab-media_browser_plus--media_browser_thumbnails']";
  const MEDIA_VIEW_LIBRARY = "a[href='#media-tab-media_default--media_browser_1']";
  const MEDIA_MY_FILES = "a[href='#media-tab-media_default--media_browser_my_files']";
  const MEDIA_ITEM = "[id^='media-item-'] a div";
  const MEDIA_SUBMIT = "a.button-yes.media_default--media_browser_1";
  const MEDIA_CANCEL = "a.button-no.media_default--media_browser_1";

  /**
   * Fill in and execute the image browser web-dialog.
   *
   * @param string|int $name_or_index
   *    The name or index of the image to select.
   * @param string $tab
   *    The name of the image browser tab to select.
   * @param string $filetype
   *    Name of a type '-Any-', 'Image', 'Video',...etc.
   * @param string $sort_by
   *    One of 'Upload date' or 'Use count'.
   * @param string $order
   *    One of 'Asc' or 'Desc'.
   * @param bool|TRUE $auto_close
   *    If TRUE submit the dialog, else leave dialog open.
   *
   * @throws Exception
   *    TEST CASE ERROR Unknown tab.
   */
  private function selectImage($name_or_index, $tab = "View Library", $filetype = 'Image',
                               $sort_by = "Upload date", $order = "Desc", $auto_close = TRUE) {

    // The media browser is  in its own frame.
    $this->getMainContext()->resolveSelector(self::MEDIA_BROWSER_FRAME);
    $cmd = "return jQuery('" . self::MEDIA_BROWSER_FRAME . "').attr('name','media-browser')";
    $this->getMainContext()->getSession()->evaluateScript($cmd);
    $this->getMainContext()->getSession()->switchToIframe('media-browser');

    // Select the tab.
    switch (strtoupper($tab)) {
      case "UPLOAD":
        $select_tab = self::MEDIA_UPLOAD;
        break;

      case "LIBRARY":
        $select_tab = self::MEDIA_LIBRARY;
        break;

      case "LIBRARY (MBP)":
        $select_tab = self::MEDIA_LIBRARY_MBP;
        break;

      case "VIEW LIBRARY":
        $select_tab = self::MEDIA_VIEW_LIBRARY;
        break;

      case "MY FILES":
        $select_tab = self::MEDIA_MY_FILES;
        break;

      default:
        throw new Exception("TEST CASE ERROR: Unknown tab name '$tab'");
    }
    $tab = $this->getMainContext()->resolveSelector($select_tab);
    $tab->click();

    // Select the image.
    $media_elem = $this->getMainContext()->getSession()->getPage()->find('css', self::MEDIA_ITEM);
    $this->getMainContext()->setElem($media_elem, NULL, self::MEDIA_ITEM);

    // Close the dialog and step out of the frame(s).
    if ($auto_close) {
      $this->getMainContext()->setSelector(self::MEDIA_SUBMIT);
      $this->getMainContext()->getSession()->switchToIframe();
    }
  }

  /*
   * MANAGE CONTENT Constants
   */
  const CONTENT = "#toolbar-link-admin-content";
  const CONTENT_FRAME = "iframe[title*=\"Content dialog\"]";
  const CONTENT_FORM = "#views-exposed-form-wfm-content-page";
  const CONTENT_TITLE = "#edit-title";
  const CONTENT_TYPE = "#edit-type";
  const PROMOTED = "#edit-promote";
  const PUBLISHED = "#edit-status";
  const CONTENT_AUTHOR = "#edit-field-computed-name-value";
  const CONTENT_LOCATION = "#edit-locations-acl-tid";
  const APPLY = "#edit-submit-wfm-content";
  const RESET = "#edit-reset";

  /**
   * Fill in Content search form.
   *
   * @param string $data
   *    The data in '["name" => "value", ...]' format.
   *    "name" corresponds to a defined constant representing a CSS selector.
   */
  private function ManageContent($data) {
    // Wait for the Content form to exist.
    $this->getMainContext()->resolveSelector(self::CONTENT_FORM);

    // Load form and possible perform actions based upon the data.
    $data_array = $this->eval_data($data);
    $this->getMainContext()->formLoader(self::CONTENT_FORM, $data_array);
  }

  /*
   * FIND SEARCH RESULT ROW IN TABLE COLUMN
   */
  const OPERATIONS = "#edit-operation";
  const EXECUTE = "#edit-submit--2";
  const CONFIRM = "#edit-submit";
  const TABLE_ROWS = "table.views-table tbody tr";
  const ROW_CHECKBOX = ".vbo-select";

  /**
   * Search the results table for a matching pattern in a specified column.
   *
   * @param string $pattern
   *    The pattern to match (can be a regex).
   * @param string $column_sel
   *    CSS selector that uniquely identifies a table column.
   *
   * @return bool
   *    If not found, FALSE.
   *    If success, the row element that contains the first match to pattern.
   */
  private function findResultsRowInColumn($pattern, $column_sel) {
    $rows = $this->getMainContext()->resolveSelector(self::TABLE_ROWS, NULL, self::MULTIPLE_RESULTS);

    foreach ($rows as $row) {
      $text = $row->find('css', $column_sel)->getText();
      $found = preg_match("/$pattern/", $text);
      if ($found) {
        return $row;
      }
    }
    return FALSE;
  }

  /*
   * Blog Constants
   */
  const CREATE_BLOG_FRAME = "iframe[title*=\"Create Blog\"]";
  const BLOG_FORM = "#blog-node-form";

  /*
   * Blog Post Constants
   */
  const CREATE_BLOG_POST_FRAME = "iframe[title*=\"Create Blog Post\"]";
  const BLOG_POST_FORM = "#blog-post-node-form";
  const BLOG  = "jQuery('#edit-field-blog-und-0-nid')";
  const CATEGORIES = "jQuery('#edit-field-blog-taxonomy-und')";
  const AUTHOR = "jQuery('#edit-field-blog-author-und-0-nid')";

  /*
   * Brand Camp Article Constants
   */
  const CREATE_BRAND_CAMP_ARTICLE_FRAME = "iframe[title*=\"Create Brand Camp Article\"]";
  const BRAND_CAMP_ARTICLE_FORM  = "#bc-article-node-form";
  const BC_TITLE = "#edit-field-bc-title-und-0-value";
  const SUB_TITLE  = "#edit-field-bc-sub-title-und-0-value";
  const SUMMARY = "#edit-field-bc-summary-und-0-value";
  const SHOW_ME_FOOD = "#edit-field-bc-showme-und-1";
  const SHOW_ME_PEOPLE = "#edit-field-bc-showme-und-2";
  const SHOW_ME_PLANET = "#edit-field-bc-showme-und-3";
  const I_WANT_TO_LEARN = "#edit-field-bc-iwantto-und-1";
  const I_WANT_TO_DO = "#edit-field-bc-iwantto-und-2";

  /*
   * Brand Camp Coupon Constants
   */
  const CREATE_BRAND_CAMP_COUPON_FRAME = "iframe[title*=\"Create Brand Camp Coupon\"]";
  const BRAND_CAMP_COUPON_FORM = "#bc-coupon-node-form";

  /*
   * Brand Camp Image Constants
   */
  const CREATE_BRAND_CAMP_IMAGE_FRAME = "iframe[title*=\"Create Brand Camp Image\"]";
  const BRAND_CAMP_IMAGE_FORM = "#bc-image-node-form";

  /*
   * Brand Camp Special Constants
   */
  const CREATE_BRAND_CAMP_SPECIAL_FRAME = "iframe[title*=\"Create Brand Camp Special\"]";
  const BRAND_CAMP_SPECIAL_FORM = "#bc-connect-node-form";

  /*
   * Brand Camp Article Constants
   */
  const CREATE_BRAND_CAMP_VIDEO_FRAME = "iframe[title*=\"Create Brand Camp Video\"]";
  const BRAND_CAMP_VIDEO_FORM = "#bc-video-node-form";

  /*
   * Department Constants
   */
  const CREATE_DEPARTMENT_FRAME = "iframe[title*=\"Create Department\"]";
  const DEPARTMENT_FORM = "#department-node-form";
  const DEPT_TEASER_DESCRIPTION  = "#edit-field-department-teaser-body-und-0-value";
  const DEPT_TEASER_SELECT_MEDIA = "#edit-field-department-thumbnail-und-0-select";

  /*
   * Department Article Constants
   */
  const CREATE_DEPARTMENT_ARTICLE_FRAME = "iframe[title*=\"Create Department Article\"]";
  const DEPARTMENT_ARTICLE_FORM = "#department-article-node-form";
  const DEPARTMENT_SERVICE = "jQuery('#edit-field-department-und-0-nid')";

  /*
   * Job Position Constants
   */
  const CREATE_JOB_POSITION_FRAME = "iframe[title*=\"Create Job Position\"]";
  const JOB_POSITION_FORM = "#job-node-form";

  /*
   * Local Vendor Constants
   */
  const CREATE_LOCAL_VENDOR_FRAME = "iframe[title*=\"Create Local Vendor\"]";
  const LOCAL_VENDOR_FORM = "#local-vendor-node-form";
  const CITY = "#edit-field-postal-address-und-0-locality";
  const ZIP = "#edit-field-postal-address-und-0-postal-code";
  const STATE = "#edit-field-postal-address-und-0-administrative-area";

  /*
   * Marquee Constants
   */
  const CREATE_MARQUEE_FRAME = "iframe[title*=\"Create Marquee\"]";
  const MARQUEE_FORM = "#marquee-node-form";
  const MARQUEE_HEADING_COPY = "#edit-field-heading-copy-und-0-value";
  const MARQUEE_FIELD_TOGGLE = "#wysiwyg-toggle-edit-field-heading-copy-und-0-value";

  /**
   * Marquee requires special processing to deactivate Rich Text Mode.
   *
   * @param string $form_selector
   *    CSS selector to the form (to check for accessibility).
   * @param string $data
   *    The data in '["name" => "value", ...]' format.
   *    "name" corresponds to a defined constant representing a CSS selector.
   */
  private function EditMarquee($form_selector, $data) {
    // Ensure the form is accessible.
    $this->getMainContext()->resolveSelector($form_selector);

    // Convert the string data into an array of "selector=>value".
    $data_array = $this->eval_data($data);

    // If the Heading Copy is going to be set:
    // Then make sure that its editor is NOT in Rich Text mode.
    if (array_key_exists(self::MARQUEE_HEADING_COPY, $data_array)) {
      $toggle = $this->getMainContext()->getSession()->getPage()->find('css', self::MARQUEE_FIELD_TOGGLE);
      if ("Disable rich-text" == $toggle->gettext()) {
        $toggle->click();
      }
    }

    // Use form loader to fill in values and perform actions.
    $this->getMainContext()->formLoader($form_selector, $data_array);
  }

  /*
   * Metro Constants.
   */
  const CREATE_METRO_FRAME = "iframe[title*=\"Create Metro\"]";
  const METRO_FORM = "#metro-node-form";
  const NAME = "#edit-title";
  const ADDR1 = "#edit-field-postal-address-und-0-thoroughfare";

  /*
   * National Offices Constants
   */
  const CREATE_NATIONAL_OFFICES_FRAME = "iframe[title*=\"Create National Offices\"]";
  const NATIONAL_OFFICES_FORM = "#national-offices-node-form";
  const NATIONAL_OFFICES_ADDR1 = "#edit-field-office-address-und-0-thoroughfare";
  const NATIONAL_OFFICES_CITY = "#edit-field-office-address-und-0-locality";
  const NATIONAL_OFFICES_STATE = "#edit-field-office-address-und-0-administrative-area";
  const NATIONAL_OFFICES_ZIP = "#edit-field-office-address-und-0-postal-code";

  /*
   * Newsletter Constants
   */
  const CREATE_NEWSLETTER_FRAME = "iframe[title*=\"Create Newsletter\"]";
  const NEWSLETTER_FORM = "#newsletter-node-form";
  const NEWSLETTER_CATEGORY = "#edit-field-newsletter-category-und";

  /*
   * Product Constants
   */
  const CREATE_PRODUCT_FRAME = "iframe[title*=\"Create Product\"]";
  const PRODUCT_FORM = "#product-node-form";

  /*
   * Product Certification Constants
   */
  const CREATE_PRODUCT_CERTIFICATION_FRAME = "iframe[title*=\"Create Product Certification\"]";
  const PRODUCT_CERTIFICATION_FORM = "#product-certification-node-form";

  /*
   * Product Line Constants
   */
  const CREATE_PRODUCT_LINE_FRAME = "iframe[title*=\"Create Product Line\"]";
  const PRODUCT_LINE_FORM = "#product-line-node-form";

  /*
   * Promo  Constants
   */
  const CREATE_PROMO_FRAME = "iframe[title*=\"Create Promo\"]";
  const PROMO_FORM = "#promo-node-form";

  /*
   * Recipe  Constants
   */
  const CREATE_RECIPE_FRAME = "iframe[title*=\"Create Recipe\"]";
  const RECIPE_FORM = "#recipe-node-form";

  /*
   * Region  Constants
   */
  const CREATE_REGION_FRAME = "iframe[title*=\"Create Region\"]";
  const REGION_FORM = "#region-node-form";

  /*
   * Special Diet Constants
   */
  const CREATE_SPECIAL_DIET_FRAME = "iframe[title*=\"Create Special Diet\"]";
  const SPECIAL_DIET_FORM = "#special-diet-node-form";

  /*
   * Store Constants
   */
  const CREATE_STORE_FRAME = "iframe[title*=\"Create Store\"]";
  const STORE_FORM  = "#store-node-form";
  const STORE_NAME = "#edit-title";
  const STORE_NICKNAME = "#edit-field-store-name-und-0-value";
    const BUSINESS_UNIT = "#edit-field-business-unit-und-0-value";

  /*
   * Video Constants
   */
  const CREATE_VIDEO_FRAME = "iframe[title*=\"Create Video\"]";
  const VIDEO_FORM  = "#video-node-form";

  /*...........................................................................
   * Webform Constants
   * .........................................................................*/
  const CREATE_WEBFORM_FRAME = "iframe[title*=\"Create Webform\"]";
  const WEBFORM_FORM = "#webform-node-form";
  const WEBFORM_CONSOLE = "div#console";

  /*...........................................................................
   * Edit Simple Form Constants
   * .........................................................................*/
  const CREATE_PAGE_FRAME = "iframe[title*=\"Create Page\"]";
  const PAGE_FORM = "#page-node-form";

  /**
   * Edit Simple Form (generic form, no sub-forms requiring a separate close).
   *
   * @param string $form_selector
   *    CSS selector to the form (to check for accessibility).
   * @param string $data
   *    The data in '["name" => "value", ...]' format.
   *    "name" corresponds to a defined constant representing a CSS selector.
   * @param string $frame_selector
   *    The name to be given to the enclosing iFrame (if needed).
   */
  private function EditSimpleForm($form_selector, $data, $frame_selector) {
    // Ensure the form is accessible.
    $this->getMainContext()->resolveSelector($form_selector);

    // Convert the string data into an array of "selector=>value".
    $data_array = $this->eval_data($data);

    // Use form loader to fill in values and perform actions.
    $this->getMainContext()->formLoader($form_selector, $data_array);
  }

  /**
   * Prevent administrative actions when current URL is a production URL.
   *
   * @throws exception
   *    Test Configuration Error.
   */
  private function EndIfProductionURL() {
    $url = $this->getMainContext()->getSession()->getCurrentUrl();
    $is_prod = (stristr($url, "://www.wholefoodsmarket.com") ||
      (stristr($url, "://vpcprod.wholefoodsmarket.com")));
    if ($is_prod) {
      // The URL is in the production space throw an exception.
      throw new exception("Test Configuration Error: This test must NOT run in the production environment $url");
    }
  }

  /**
   * Is an iFrame being used?
   *
   * @return bool
   *    TRUE if accessible elements are in an iFrame, Otherwise FALSE.
   */
  private function frameExists() {
    // Jump into iFrame only if URL indicates that an overlay is being used.
    $url = $this->getMainContext()->getSession()->getCurrentUrl();
    return (FALSE !== stristr($url, "#overlay="));
  }

  /**
   * Switch into an nameless iFrame by giving it a name.
   *
   * @param string $frame_selector
   *   CSS selector that uniquely identifies the iFrame.
   * @param string $frame_name
   *   The name to apply to the iFrame.
   *
   * @return string|bool
   *   False if failure, the fame name if successful.
   */
  private function moveToFrame($frame_selector, $frame_name) {
    // Jump into iFrame only if URL indicated that an overlay is being used.
    if ($this->frameExists()) {
      /* Wait for $frame iframe to exist then add the 'name' arttribute with
       * value $frame_name to it.
       * Using the newly added "name" the switch from 'page' to iframe overlay
       * can be accomplished.
       */
      $this->getMainContext()->resolveSelector($frame_selector);
      $this->getMainContext()->getSession()->evaluateScript("return jQuery('$frame_selector').attr('name','$frame_name')");
      $this->getMainContext()->getSession()->switchToIframe($frame_name);
      return $frame_name;
    }
    // No iFrame, the "page" is the frame.
    return FALSE;
  }

  /**
   * Convert PyStringNode data in to an arrayof (selector,value) pairs.
   *
   * @param string $data
   *    In the format constname1 => "value1, constname2 => "value2, ...
   *    Data example:
   *   (triple quotes denote PyStringNode input in Behat feature file)
   *     """
   *      LOCATION_NAME      => "Zilker Park Rock Island",
   *      TITLE              => "Zilker Treasure Island Getaway",
   *      STORE              => "LMR (Lamar)",
   *      PERMISSIONS        => "LMR (Lamar)",
   *     """.
   *
   * @return array
   *    An array of selector,value pairs
   */
  function eval_data(\Behat\Gherkin\Node\PyStringNode $data) {
    // Use regular expression substitution to to add.
    $re = "/\\s*(\\S+)\\s*(=>).*(\\S.*\")/Us";
    $subst = "self::$1 $2 $3";
    $php_data = preg_replace($re, $subst, (string) $data);
    $data_array = eval('return [' . $php_data . '];');
    return $data_array;
  }

  /**
   * Utility step that allows a breakpoint to be set at the Feature file level.
   *
   * Use mostly to hold the browser open at some point in the test,
   * so that it can be inspected.
   *
   * @When /^I break here$/
   */
  public function iBreakHere() {
    if (TRUE) {
      // Set breakpoint here.
      return;
    }
  }

    /**
     * @Given /^I sign out of drupal$/
     */
    public function iSignOutOfDrupal() {
        $path = $this->getMainContext()->locatePath('/');
        $this->getMainContext()->getSession()->visit($path . '/user/logout');
    }

}
