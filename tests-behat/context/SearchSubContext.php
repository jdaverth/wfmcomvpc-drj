<?php
require_once 'FeatureContext.php';
use Behat\Behat\Context\BehatContext;
use Drupal\DrupalExtension\Context\DrupalSubContextInterface;


class SearchSubContext extends BehatContext implements DrupalSubContextInterface
{
  //SEARCH RESULTS & PAGINATION
  const SEARCH_RESULT_SELECTOR = "li.search-result";
  const STORE_SEARCH_RESULT_SELECTOR = ".views-row";
  const RESULTS_PAGER_CONTAINER = ".pager";//".item-list";
  const RESULTS_PAGER_LINKS_ITEMS = ".pager-item";
  const RESULTS_PAGER_LAST = ".pager-last";
  const RESULTS_PAGER_CURRENT = ".pager-current";
  const RESULTS_PAGER_FIRST = ".pager-first";
  const RESULTS_SEARCH_DESCRIPTION = ".search_description";
  const RECIPE_RESULTS = ".views-row";
  const BLANK_SEARCH_ERROR_MESSAGE = "Please enter at least one keyword.";

  //GLOBAL SEARCH
  const GLOBAL_SEARCH_FIELD_SELECTOR = "#edit-keys-56";
  const GLOBAL_SEARCH_FIELD_MENU_SELECTOR = ".form-item-keys-56";
  const GLOBAL_SEARCH_FIELD_NAME = "keys_56";
  const GLOBAL_SEARCH_SUBMIT_SELECTOR = "#edit-submit-56"; //or "op"

  //PRODUCTS SEARCH
  const PRODUCTS_SEARCH_FIELD_SELECTOR = "#edit-keys-61";
  const PRODUCTS_SEARCH_FIELD_NAME = "keys_61";
  const PRODUCTS_SEARCH_FIELD_MENU_SELECTOR = ".form-item-keys-61";
  const PRODUCTS_SEARCH_SUBMIT_SELECTOR = "[id^='edit-submit-61']";

  //STORE SEARCH
  const STORE_SEARCH_FIELD_SELECTOR = "#edit-field-geo-data-latlon-address";
  const STORE_SEARCH_FIELD_MENU_SELECTOR = ".form-item-address";
  const STORE_SEARCH_FIELD_NAME = "address";
  const STORE_SEARCH_SUBMIT_SELECTOR = "#edit-submit-store-lookup-by-address"; //or "Apply"

  //RECIPE SEARCH
  const RECIPE_SEARCH_FIELD_SELECTOR = "#edit-search-box";
  const RECIPE_SEARCH_FIELD_MENU_SELECTOR = "#healthy-eating-recipe-search";
  const RECIPE_SEARCH_FIELD_NAME = "search_box";
  // Don't use the id  "#edit-submit" or "#edit-submit--3"
  // Drupal seems to change it spontaneously.
  const RECIPE_SEARCH_SUBMIT_SELECTOR = "#healthy-eating-recipe-search input[type='submit']";

  const RECIPE_ADVANCED_SEARCH_LINK_SELECTOR = "#edit-advanced-link"; //or value "Advanced Search"
  //RECIPE ADVANCED SEARCH CHECKBOXES:
  const ADVANCED_RECIPE_MENU_SELECTOR = "#edit-advanced";
  //RECIPE SIDE BAR SEARCH ON INDIVIDUAL RECIPES:
  const RECIPE_SIDE_SEARCH_MENU_SELECTOR = "#block-healthy-eating-recipe-search";
  const RECIPE_SIDE_SEARCH_FIELD_NAME = "search_box";
  const RECIPE_SIDE_SEARCH_SUBMIT_SELECTOR = "#edit-submit";
  const RECIPE_SIDE_SEARCH_SUBMIT_SELECTOR_ALT = "#edit-submit--2";


  //BLOG SEARCH
  const BLOG_SEARCH_FIELD_SELECTOR = "#edit-search-box";
  const BLOG_SEARCH_FIELD_MENU_SELECTOR = "#blogs-search";
  const BLOG_SEARCH_FIELD_NAME = "search_box";
  const BLOG_SEARCH_SUBMIT_SELECTOR = "#edit-submit"; //or "op"
  const BLOG_SEARCH_SUBMIT_SELECTOR_ALT = "#edit-submit--2";


  public function __construct(array $parameters = null) {
    // do subcontext initialization
  }

  public static function getAlias() {
    return 'search';
  }

  public function getSession() {
    return $this->getMainContext()->getSession();
  }

  public function getRegion($region) {
    return $this->getMainContext()->getRegion($region);
  }

  public function spin($lambda, $wait = 12) {
    $exception_message = '';
    for ($i = 0; $i < $wait; $i++) {
      try {
        if ($lambda($this)) {
          return TRUE;
        }
      } catch (Exception $e) {
        $exception_message = $e->getMessage() . "\n";
      }
      sleep(1);
    }
    $backtrace = debug_backtrace();
    $exception_message .= "Timeout thrown by " . $backtrace[1]['class'] . "::" . $backtrace[1]['function'] . "()\n";
    if (array_key_exists('file', $backtrace[1])) {
      $exception_message .= $backtrace[1]['file'];
    }
    if (array_key_exists('line', $backtrace[1])) {
      $exception_message .= ", line " . $backtrace[1]['line'];
    }
    throw new Exception($exception_message);
  }

//*.*.*.*.*.*.*.*.*.*.*.*.*.*.*-- SEARCH FUNCTIONS --*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*

  /**
   * @Then /^I perform a "([^"]*)" "([^"]*)" search for "([^"]*)"$/
   *
   * REQUIREMENTS: You must be on a page that contains the $search_type
   * This function is expecting multiple results and multiple pages of results. It will fail for no results found.
   * Complex query strings will not work as well with this function. Better to use single word search terms.
   * Requires that there are 10+ pages of results
   * First variable passed in must be "paginated" or "non-paginated"
   */
  public function iPerformASearchFor($paginated, $search_type, $query_string) {
    if ($search_type == "global") {
      $container_selector = self::GLOBAL_SEARCH_FIELD_MENU_SELECTOR;
      $field_selector = self::GLOBAL_SEARCH_FIELD_NAME;
      $button_selector = self::GLOBAL_SEARCH_SUBMIT_SELECTOR;
    }
    elseif ($search_type == "product") {
      $container_selector = self::PRODUCTS_SEARCH_FIELD_MENU_SELECTOR;
      $field_selector = self::PRODUCTS_SEARCH_FIELD_NAME;
      $button_selector = self::PRODUCTS_SEARCH_SUBMIT_SELECTOR;
    }
    elseif ($search_type == "recipe") {
      $container_selector = self::RECIPE_SEARCH_FIELD_MENU_SELECTOR;
      $field_selector = self::RECIPE_SEARCH_FIELD_NAME;
      $button_selector = self::RECIPE_SEARCH_SUBMIT_SELECTOR;
    }
    elseif ($search_type == "recipe side") {
      $container_selector = self::RECIPE_SIDE_SEARCH_MENU_SELECTOR;
      $field_selector = self::RECIPE_SIDE_SEARCH_FIELD_NAME;
      $button_selector = self::RECIPE_SIDE_SEARCH_SUBMIT_SELECTOR;
      $button_selector_alt = self::RECIPE_SIDE_SEARCH_SUBMIT_SELECTOR_ALT;
    }
    elseif ($search_type == "blog") {
      $container_selector = self::BLOG_SEARCH_FIELD_MENU_SELECTOR;
      $field_selector = self::BLOG_SEARCH_FIELD_NAME;
      $button_selector = self::BLOG_SEARCH_SUBMIT_SELECTOR;
      $button_selector_alt = self::BLOG_SEARCH_SUBMIT_SELECTOR_ALT;
    }
    elseif ($search_type == "store") {
      $container_selector = self::STORE_SEARCH_FIELD_MENU_SELECTOR;
      $field_selector = self::STORE_SEARCH_FIELD_NAME;
      $button_selector = self::STORE_SEARCH_SUBMIT_SELECTOR;
    }
    else {
      throw new Exception("This function only performs 'global', 'product', 'recipe', 'blog', and 'store' searches.");
    }
    //find search container
    $container = $this->getSession()->getPage()->find('css', $container_selector);
    if (empty($container)) {
      throw new Exception("Couldn't find " . $search_type . " field selector " . $container_selector);
    }
    //Fill search field with text and press search
    $container->fillField($field_selector, $query_string);
    $button_node = $this->getSession()->getPage()->find('css', $button_selector);
    if (empty($button_node) && isset($button_selector_alt)) {
      $button_selector = $button_selector_alt; //Now the error message makes more sense.
      $button_node = $this->getSession()->getPage()->find('css', $button_selector_alt);
    }
    if (empty($button_node)) {
      throw new Exception("Unable to find button with selector " . $button_selector);
    }

    $button_node->click();
    if ($search_type == "store") {
      return;
    }
    sleep(3);
    //Checks that it's not the "no results found" page
    if (stripos($this->getSession()->getPage()->getText(), "Sorry, no results were found.")) {
      throw new Exception("Expected results. Got 'Sorry, no results were found.' instead.");
    }
    //Checks that the url contains the search term
    $url = $this->getSession()->getCurrentUrl();
    $query_string_reg = str_replace(' ', '%20', $query_string);
    $query_string_reg = str_replace(':', '%3A', $query_string_reg);
    if (!stripos($url, $query_string_reg) && $query_string != "") {
      throw new Exception("Page url ". $url . " doesn't contain search query '" . $query_string . "'.");
      //print "Page url ". $url . " doesn't contain search query '" . $query_string . "'.\n";
    }
    //Checks that search query is reiterated to the user in the search_description field
    $search_description = $this->getSession()->getPage()->find('css', self::RESULTS_SEARCH_DESCRIPTION);
    if (empty($search_description)) {
      throw new Exception("Unable to find selector " . self::RESULTS_SEARCH_DESCRIPTION);
    }
    if (!stripos($search_description->getText(), $query_string) && $query_string != "") {
      throw new Exception("Unable to find query '" . $query_string . "' in selector " . self::RESULTS_SEARCH_DESCRIPTION);
    }
    //Checks that there are multiple results.
    $results = $this->getSession()->getPage()->findAll('css', self::SEARCH_RESULT_SELECTOR);
    if (count($results) == 0) {
      throw new Exception("Search results expected. No search results returned for search '" . $query_string . "'.");
    }
    //The search results found should be relevant
    //BUT HOW RELEVANT?
    //If I find >6 recipes that don't have my keyword in them, throw an exception
    $count_not_relevant = 0;
    foreach ($results as $result) {
      $txt = $result->getText();
      //If I don't find this string
      $found = stripos($txt,$query_string);
      if ($found === FALSE) {
        $count_not_relevant = $count_not_relevant + 1;
        if ($count_not_relevant > 6 && $query_string != "") {
          throw new Exception("Search results were not relevant to search query '" . $query_string . "'.");
        }
      }
    }
    if ($paginated != "paginated" && $paginated != "non-paginated") {
      throw new Exception("First variable must be 'paginated' or 'non-paginated'.");
    }
    if ($paginated == "paginated") {
      $this->checkPaginatedLinks();
    }
  }


  /**
   * @Then /^I perform a negative "([^"]*)" search for "([^"]*)"$/
   */
  public function iPerformANegativeSearchFor($search_type, $query_string) {
      if ($search_type == "global") {
      $container_selector = self::GLOBAL_SEARCH_FIELD_MENU_SELECTOR;
      $field_selector = self::GLOBAL_SEARCH_FIELD_NAME;
      $button_selector = self::GLOBAL_SEARCH_SUBMIT_SELECTOR;
    }
    elseif ($search_type == "product") {
      $container_selector = self::PRODUCTS_SEARCH_FIELD_MENU_SELECTOR;
      $field_selector = self::PRODUCTS_SEARCH_FIELD_NAME;
      $button_selector = self::PRODUCTS_SEARCH_SUBMIT_SELECTOR;
    }
    elseif ($search_type == "recipe") {
      $container_selector = self::RECIPE_SEARCH_FIELD_MENU_SELECTOR;
      $field_selector = self::RECIPE_SEARCH_FIELD_NAME;
      $button_selector = self::RECIPE_SEARCH_SUBMIT_SELECTOR;
    }
    elseif ($search_type == "recipe side") {
      $container_selector = self::RECIPE_SIDE_SEARCH_FIELD_MENU_SELECTOR;
      $field_selector = self::RECIPE_SIDE_SEARCH_FIELD_NAME;
      $button_selector = self::RECIPE_SIDE_SEARCH_SUBMIT_SELECTOR;
    }
    elseif ($search_type == "blog") {
      $container_selector = self::BLOG_SEARCH_FIELD_MENU_SELECTOR;
      $field_selector = self::BLOG_SEARCH_FIELD_NAME;
      $button_selector = self::BLOG_SEARCH_SUBMIT_SELECTOR;
    }
    elseif ($search_type == "store") {
      $container_selector = self::STORE_SEARCH_FIELD_MENU_SELECTOR;
      $field_selector = self::STORE_SEARCH_FIELD_NAME;
      $button_selector = self::STORE_SEARCH_SUBMIT_SELECTOR;
      //stuff here should call a different function, most likely
    }
    else {
      throw new Exception("This function only performs 'global', 'product', 'recipe', 'blog', and 'store' negative searches.");
    }
    //find search container
    $container = $this->getSession()->getPage()->find('css', $container_selector);
    if (empty($container)) {
      throw new Exception("Couldn't find " . $search_type . " field selector " . $container_selector);
    }
    //Fill search field with text and press search
    $container->fillField($field_selector, $query_string);
    $button_node = $this->getSession()->getPage()->find('css', $button_selector);
    if (empty($button_node)) {
      throw new Exception("Unable to find button with selector " . $button_selector);
    }
    $button_node->click();
    if ($search_type == "store") {
      return;
    }
    if ($query_string == "") {
      $pagetext = $this->getSession()->getPage()->getText();
      if (!stripos($pagetext, self::BLANK_SEARCH_ERROR_MESSAGE)) {
        throw new Exception("Unable to find error message '" . self::BLANK_SEARCH_ERROR_MESSAGE . "'.");
      }
    }
    //Check for "Sorry, no results were found." page
    else {
      //Checks that it is the "no results found" page
      if (!stripos($this->getSession()->getPage()->getText(), "Sorry, no results were found.")) {
        throw new Exception("No results expected; missing 'Sorry, no results were found.' message.");
      }
      //Checks that the url contains the search term
      $url = $this->getSession()->getCurrentUrl();
      if (!stripos($url, $query_string)) {
        throw new Exception("Page url ". $url . " doesn't contain search query '" . $query_string . "'.");
      }
      //Checks that there are no results.
      $results = $this->getSession()->getPage()->findAll('css', self::SEARCH_RESULT_SELECTOR);
      if (count($results) > 0) {
        throw new Exception("No search results expected for '" . $query_string . "'.");
      }
    }
  }



//*.*.*.*.*.*.*.*.*.*.*.*.*.*.*-- RESULTS --*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*
  /**
   * @Then /^I should see "([^"]*)" in the store results$/
   * Looking for a particular text (needle) in the results (haystack)
   * This is specifically looking for store search results right now
   */
  public function iShouldSeeInTheStoreResults($needle) {
    $results = $this->getSession()->getPage()->findAll('css', self::STORE_SEARCH_RESULT_SELECTOR);// getText();
    if (empty($results)) {
      throw new Exception("Unable to find results with selector " . self::STORE_SEARCH_RESULT_SELECTOR);
    }
    $found = FALSE;
    foreach($results as $haystack) {
      //print "HAYSTACK: " .$haystack->getText() . "\n";
      sleep(1); //They must not have been loading quick enough; this fixed the issue
      if (stripos($haystack->getText(), $needle)) {
        $found = TRUE;
      }
    }
    if ($found == FALSE) {
      throw new Exception("Unable to find result '" . $needle . "' in the results.");
    }

  }

//*.*.*.*.*.*.*.*.*.*.*.*.*.*.*-- PAGINATION --*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*

  /**
   * @Then /^I check the paginated links$/
   * function to check paginated links
   * Must be on a page that has the normal pagination that should work consistently across the site.
   */
  public function checkPaginatedLinks() {
    //print "\nPAGINATION TIME\n";
    $url = $this->getSession()->getCurrentUrl();
    //Pagination on TOP RATED and NEWEST recipes require a little more love, so they get an if statement
    if ((strpos($url, '/recipes')) && !(strpos($url, '/recipes/search'))) { //so we're on recipes AND we're NOT on recipe/search
      $results = self::RECIPE_RESULTS;
      $activetab_selector = ".quicktabs-tabs .active .active";
      $activetab = $this->getSession()->getPage()->find('css', $activetab_selector);
      if ($activetab->getText() == "TOP RATED") {
        $container = $this->getSession()->getPage()->findAll('css', self::RESULTS_PAGER_CONTAINER);
        $container = $container[0];
      }
      if ($activetab->getText() == "NEWEST") {
        $container = $this->getSession()->getPage()->findAll('css', self::RESULTS_PAGER_CONTAINER);
        $container = $container[1];
      }
    }
    //Pagination from searches should fall into here:
    else {
      $results = self::SEARCH_RESULT_SELECTOR;
      $container = $this->getSession()->getPage()->find('css', self::RESULTS_PAGER_CONTAINER);
    }
    if (empty($container)) {
      throw new Exception("Unable to find pagination or container " . self::RESULTS_PAGER_CONTAINER);
    }
    $paginationlinks = $container->findAll('css', self::RESULTS_PAGER_LINKS_ITEMS);
    if (empty($paginationlinks)) {
      throw new Exception("Unable to find pagination links with css selector " . self::RESULTS_PAGER_LINKS_ITEMS . " in container " . self::RESULTS_PAGER_CONTAINER);
    }
    $last_page_num_container = $container->find('css', self::RESULTS_PAGER_LAST);
    if (empty($last_page_num_container)) {
      throw new Exception("Unable to find pager-last.");
    }
    $last_page_num = $last_page_num_container->getText();
    //print "\nLAST PAGE NUM: " . $last_page_num . "\n";
    for ($i = 2; $i < count($paginationlinks); $i++) {
      $link = $container->findLink("Go to page $i");
      if (is_numeric($link->getText()) || $link->getAttribute('title') == ('Go to page ' . $last_page_num)) {
        $link->click();
        sleep(3); //wait for the new content to be loaded
        //check for content
        if (!count($this->getSession()->getPage()->findAll('css', $results))) { //self::SEARCH_RESULT_SELECTOR))) {
          throw new Exception("Didn't find content for paginated page number " . $link->getText());
        }
      }
    }
  }


//*.*.*.*.*.*.*.*.*.*.*.*.*.*.*-- CHECKBOXES --*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*

  /**
   * @Given /^I check the "([^"]*)" checkbox$/
   *
   * REQUIREMENTS: you are on /recipes with advanced search open
   * The variable you pass in must match the HTML/source code (NOT the all uppercase name that you see on the page)
   * Ex: "Dairy Free" (NOT "DAIRY FREE")
   */
  public function iCheckTheCheckbox($checkbox_name) {
    $container = $this->getSession()->getPage()->find('css', self::ADVANCED_RECIPE_MENU_SELECTOR);
    if (empty($container)) {
      throw new Exception("Unable to find selector " . self::ADVANCED_RECIPE_MENU_SELECTOR);
    }
    $checkbox_node = $container->find('css', ".option:contains(" . $checkbox_name . ")");//'xpath', '//label[text()=" . $checkbox_name . "]');
    if(empty($checkbox_node)) {
      throw new Exception("Unable to find checkbox '" . $checkbox_name . "'.");
    }
    $checkbox_selector = $checkbox_node->getAttribute('for');
    //print "\n************ " . $checkbox_selector . " *************\n";
    $checkbox = $container->find('css', '#' . $checkbox_selector);
    $checkbox->check();
    //$this->getMainContext()->iWaitForMilliseconds('3000');
  }

}//End eStoreSubContext
