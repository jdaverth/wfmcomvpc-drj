<?php
//require_once 'FeatureContext.php';

use Behat\Behat\Context\BehatContext;
use Drupal\DrupalExtension\Context\DrupalSubContextInterface;
/*
 * Subcontext for Desktop eStore steps, utilities, and constants
 */
class eStoreDesktopSubContext extends BehatContext implements DrupalSubContextInterface
{

  # Array used to store data for later verification between steps
  public $memorized_data = [];

  public function __construct(array $parameters = null)
  {
    // do subcontext initialization
  }

  public static function getAlias()
  {
    return 'eStoreDesktop';
  }

  public function getSession()
  {
    return parent::getMainContext()->getSession();
  }

  public function shouldBeVisible($css_selector)
  {
    $this->getMainContext()->shouldBeVisible($css_selector);
  }

  public function iWaitForAjaxCallsToFinish()
  {
    $this->getMainContext()->iWaitForAjaxCallsToFinish();
  }


  public function setSelector($el_selector, $value = null, $container_selector = null)
  {
    return $this->getMainContext()->setSelector($el_selector, $value, $container_selector);
  }

  public function resolveSelector($el_selector, $container_selector = null, $multiple = FALSE, $quash_exception=FALSE)
  {
    return $this->getMainContext()->resolveSelector($el_selector, $container_selector, $multiple, $quash_exception);
  }

  function putScreenshot( $text='putScreenshot', $dir_path=null)
  {
      return $this->getMainContext()->putScreenshot( $text, $dir_path);
  }

  #State abbreviations use in Shipping confirmation
  static $STATE_ABBR = [
    "Alabama"=>"AL",       "Alaska"=>"AK",      "Arizona"=>"AZ",        "Arkansas"=>"AR",      "California"=>"CA",
    "Colorado"=>"CO",      "Connecticut"=>"CT", "Delaware"=>"DE",       "Florida"=>"FL",       "Georgia"=>"GA",
    "Hawaii"=>"HI",        "Idaho"=>"ID",       "Illinois"=>"IL",       "Indiana"=>"IN",       "Iowa"=>"IA",
    "Kansas"=>"KS",        "Kentucky"=>"KY",    "Louisiana"=>"LA",      "Maine"=>"ME",         "Maryland"=>"MD",
    "Massachusetts"=>"MA", "Michigan"=>"MI",    "Minnesota"=>"MN",      "Mississippi"=>"MS",   "Missouri"=>"MO",
    "Montana"=>"MT",       "Nebraska"=>"NE",    "Nevada"=>"NV",         "New Hampshire"=>"NH", "New Jersey"=>"NJ",
    "New Mexico"=>"NM",    "New York"=>"NY",    "North Carolina"=>"NC", "North Dakota"=>"ND",  "Ohio"=>"OH",
    "Oklahoma"=>"OK",      "Oregon"=>"OR",      "Pennsylvania"=>"PA",   "Rhode Island"=>"RI",  "South Carolina"=>"SC",
    "South Dakota"=>"SD",  "Tennessee"=>"TN",   "Texas"=>"TX",          "Utah"=>"UT",          "Vermont"=>"VT",
    "Virginia"=>"VA",      "Washington"=>"WA",  "West Virginia"=>"WV",  "Wisconsin"=>"WI",     "Wyoming"=>"WY"
  ];

  #eStore landing page constants
  const eSTORE_LANDING = 'online-ordering';
  const eSTORE_STORE_SELECT_FORM = '#online-ordering-header-form';
  const eStore_STORE_SELECT_STATE = '[name="state"]';
  const eStore_STORE_SELECT_STORE = '[name="store"]';
  const eSTORE_STORE_SELECT_CONTINUE = '[name="op"]';

  /**
   * @When /^I select the eStore at "([^"]*)" in "([^"]*)"$/
   * REQUIREMENTS: The current url is any page.
   * @param $address   Street address of the eStore
   * @param $location  Geographic region of the eStore
   */
  public function iSelectTheEstoreAtIn($address, $location)
  {
    $this->getMainContext()->visit(self::eSTORE_LANDING);
    #Wait for the eStore selection form
    $this->shouldBeVisible(self::eSTORE_STORE_SELECT_FORM);

    #Load the eStore selection parameters
    $eStore_data =[ self::eStore_STORE_SELECT_STATE    => $location,
                    self::eStore_STORE_SELECT_STORE    => $address ,
                    self::eSTORE_STORE_SELECT_CONTINUE => ""       ];

    try {
      $this->formLoader(self::eSTORE_STORE_SELECT_FORM, $eStore_data);
    } catch (exception $e) {
        throw new Exception('Could not select eStore at "' . $address . '" in "' . $location . '"' . "\n" . $e);
    }
  }

  #eStore .../shop/... page
  const eSTORE_SEARCH_READY = 'input.query';
  const eSTORE_NO_MATCH = '.search_msg p';
  const eSTORE_RESULT_READY = '.bigheader';
  const eSTORE_PRODUCT_LIST = '.product.list';
  const eSTORE_PRODUCT_LIST_ITEM = 'li[data-href]';
  const eSTORE_PRODUCT_LINK = " .name a";
  const eSTORE_CLOSED = '/closed';
  const eSTORE_SEARCH_BOX = '.navbar .search';
  const eSTORE_PROD_DESC_CONTAINER = ".product_description";
  /**
   * @Given /^eStore "([^"]*)" is Open$/
   * REQUIREMENTS: The current url is an eStore .../shop/??? page.
   * Verify the current eStore is open
   * @param $address
   * @throws Exception
   */
  public function EstoreIsOpen($address)
  {
    //Get the current URL
    $browser_url = $this->getSession()->getCurrentUrl();

    //Is the URL an eStore URL.
    if (strpos($browser_url, '/shop/') == FALSE) {
      throw new Exception('Current URL is not an eStore URL "' . $browser_url . '"');
    }

    //Closed eStore URLs end in "/closed", throw an exception in that case.
    if (strpos($browser_url, self::eSTORE_CLOSED) > 0) {
      throw new Exception('The eStore "' . $address . '" is not OPEN.');
    }
  }

  /**
   * @When /^I search for "([^"]*)"$/
   * REQUIREMENTS: An eStore has been selected and the current url is an eStore .../shop/??? page.
   * Search for an eStore item using the eStore NavBar search box.
   * @param $search_query The product(s)/item(s) to search for
   */
  public function iSearchFor($search_query)
  {
    //Wait until the search control is visible or times out.
    $this->shouldBeVisible(self::eSTORE_SEARCH_READY);

    //In case of multiple search boxes:
    // Limit the query's fill field and Search button press to just the eStore search box
    $search_box = $this->getSession()->getPage()->find("css", self::eSTORE_SEARCH_BOX);
    $search_box->fillField("search", $search_query);
    $search_box->pressButton("Search");
  }

  /**
   * @Then /^I should see at least one product result for "([^"]*)"$/
   * REQUIREMENTS: An eStore item search has completed and the current url is an eStore .../shop/???/search page.
   * Verify at least one item is in the product list
   * @param $search_query
   * @throws Exception
   */
  public function iShouldSeeAtLeastOneProductResult($search_query)
  {
    //The product list should appear
    $this->shouldBeVisible(self::eSTORE_RESULT_READY);
    //Find all of the listed products returned from the search
    $search_result_rows = $this->getSession()->getPage()->findAll('css', self::eSTORE_PRODUCT_LIST_ITEM);
    //If no products are listed this test failed
    if (count($search_result_rows) == 0) {
      throw new Exception('No products were returned for search query "' . $search_query . '"');
    }
  }

  /**
   * @Given /^Verify the product name or page contains the search "([^"]*)"$/
   * REQUIREMENTS: An eStore item search has completed and the current url is an eStore .../shop/???/search page.
   * This function depends upon iShouldSeeAtLeastOneProductResult
   * @param $search_query
   * @throws Exception
   */
  public function verifyTheProductNameOrPageContainsTheSearch($search_query)
  {
    $this->shouldBeVisible(self::eSTORE_PRODUCT_LIST);
    $page = $this->getSession()->getPage();
    //Find all of the listed products returned from the search
    $search_results = $page->findAll('css', self::eSTORE_PRODUCT_LIST_ITEM . self::eSTORE_PRODUCT_LINK);
    //Does the search query appear in the name of the last product
    $last_result_index = count($search_results) - 1;
    $last_product = $search_results[$last_result_index];
    $last_product_name = $last_product->getText();
    if (strripos($last_product_name, $search_query) !== FALSE) return; //If found then return

    //The search item did not appear in the product name
    //Click the link of the last item found
    $last_product->click();

    //Wait for the product page to load the product description
    $this->shouldBeVisible(self::eSTORE_PROD_DESC_CONTAINER);

    //Get all of the paragraphs in the product description
    $paragraphs = $page->findAll('css', self::eSTORE_PROD_DESC_CONTAINER . ' p');

    //Return if any product description paragraph contains the search query
    foreach ($paragraphs as $paragraph) {
      $text = $paragraph->getText();
      if (strripos($text, $search_query) !== FALSE) return; //If found then return
    }
    //FAIL: No product description paragraph contained the search item.
    throw new Exception('No reference to search query "' . $search_query . '" was found in the "' . $last_product_name . '" product description page.');
  }

  /**
   * @Then /^Verify the no product result fields for "([^"]*)"$/
   * REQUIREMENTS: An eStore item search has completed and the current url is an eStore .../shop/???/search page.
   * @param $search_query
   * @throws Exception
   */
  public function verifyTheNoProductResultFieldsFor($search_query)
  {
    $this->shouldBeVisible(self::eSTORE_RESULT_READY);
    $page = $this->getSession()->getPage();

    //Get the "Results for" message
    $header_text = $page->find("css", self::eSTORE_RESULT_READY)->getText();
    $expected_text = "Results for " . $search_query;
    //FAIL: if header RESULTS FOR <search query> is not present
    if (strcmp($header_text, $expected_text) <> 0) {
      throw new Exception('Header mismatch: Expected: "' . $expected_text . '" Actual: "' . $header_text . '"');
    }
    //Get search response text
    $response = $page->find('css', self::eSTORE_NO_MATCH)->getText();
    $expected_text = "No products match your search.";
    //FAIL: No product description paragraph contained the search item.
    if (strcmp($response, $expected_text) <> 0) {
      throw new Exception('Header mismatch: Expected: "' . $expected_text . '" Actual: "' . $response . '"');
    }
  }

  const eSTORE_ACCESS_CART = '.last a';
  /**
   * @When /^I go to my cart$/
   * REQUIREMENTS: The current url is an eStore .../shop/???/* page displaying a cart icon.
   * Go to the eStore cart from any .../shop/???/* page
   */
  public function iGoToMyCart()
  {
    $this->setSelector(self::eSTORE_ACCESS_CART);
  }


  const eSTORE_ITEM_OPTION =  ".itemOption";
  const eSTORE_ADD_TO_CART =  ".addToCart";
  const eSTORE_CHECKOUT_BTN = '.dialog button a';

  /**
   * @When /^I add the "([^"]*)" item to the cart\.$/
   * REQUIREMENTS: The current url is an eStore .../shop/???/* page displaying at least $ordinal count products.
   * @param $ordinal The i'th item out of n items (i <= n) that is to be selected.
   * @param bool $autoOption When true and the item selected has options, then options will be selected automatically
   * @throws Exception
   */
  public function iAddTheItemToTheCart($ordinal, $autoOption=true)
  {
    //Wait a visible product/item list.
    $this->shouldBeVisible(self::eSTORE_PRODUCT_LIST_ITEM);

    //Get the listed products on the eStore page
    $products = $this->resolveSelector( self::eSTORE_PRODUCT_LINK, self::eSTORE_PRODUCT_LIST_ITEM, true);

    $product_index = $ordinal - 1;
    try { $ith_product = $products[$product_index]; }
    catch (Exception $e) {
      $msg = "ordinal index '$ordinal' out of range. Use a number <'" .count($products). "' \n";
      throw new Exception( $msg . $e);
    }

    //Click the link of the i'th item listed
    $ith_product->click();

    //Wait for Add To Cart button to become visible.
    $this->shouldBeVisible(self::eSTORE_ADD_TO_CART);

    //If there selections with options to be set, then automatically select the first option in each selection.
    $selections = $this->getSession()->getPage()->findAll("css", self::eSTORE_ITEM_OPTION);
    foreach ($selections as $select){
      $options = $select->findAll("css", "option");
      foreach ($options as $option) {
        $text = $option->getText();
        if (stristr($text,"Option") == false) break;
      }
      $select->selectOption($text);
    }

    //Add the item to the cart
    $this->setSelector(self::eSTORE_ADD_TO_CART);

    //Allow annoying OK-dialog to be automatically dimissed by waiting for the CHECKOUT button to become visible.
    $this->shouldBeVisible(self::eSTORE_CHECKOUT_BTN);
  }

  const eSTORE_CART_PROCEED_TO_CHECKOUT = '.proceed button';
  /**
   * @Given /^I proceed to eStore checkout\.$/
   * REQUIREMENTS: At least one item is in the cart and a CHECKOUT button on a dialog is visible
   */
  public function iProceedToEstoreCheckout()
  {
    //First go to the Checkout review tab (same as cart page)
    $this->iProceedToEstoreCheckoutReview();

    //Continue on to billing
    $this->iProceedToEstoreCheckoutBilling();
  }

  const eSTORE_CONTINUE_SHOPPING = '.dialog a[ng-href]';
  /**
   * @Given /^I continue eStore shoppping$/
   * REQUIREMENTS: An item was selected and there is a dialog to checkout or continue shopping appears
   */
  public function iContinueEstoreShopping()
  {
    //Wait for eStore dialog to become visible then press Continue Shopping
    $this->setSelector(self::eSTORE_CONTINUE_SHOPPING);
  }

  /**
   * @When /^I proceed to eStore checkout billing for "([^"]*)"$/
   * REQUIREMENTS: The current url is the cart (.../shop/???/cart)
   * @param null $checkout_choice The choice to make if the Delivery/Pickup(default) page appears
   */
  public function iProceedToEstoreCheckoutBillingFor($checkout_choice=null)
  {
    //Wait for eStore Cart PROCEED TO CHECKOUT button to become visible then press it
    $this->setSelector(self::eSTORE_CART_PROCEED_TO_CHECKOUT);

    #Wait for the CHECKOUT form to appear if it does not try making a Pickup/Delivery choice.
    #Note the resolveSelector quash_exception parameter is set to true so that only if the BILLING form
    #does not appear, then the pickup/delivery option will be attempted.
    #This allows for faster execution in most cases
    if( !$this->resolveSelector(self::eSTORE_CHECKOUT_FORM,null,FALSE,TRUE) ){
      switch ($checkout_choice){
        case "PICKUP":
          $this->setSelector(self::eSTORE_CHOOSE_PICKUP);
          break;
        case "DELIVERY":
          $this->setSelector(self::eSTORE_CHOOSE_DELIVERY);
          break;
        default:
          $this->setSelector(self::eSTORE_CHOOSE_PICKUP);
      }
      #The choice has been selected so submit the choice.
      $this->setSelector(self::eSTORE_CHOICE_SUBMIT);
    }
  }

  /**
   * @Given /^I proceed to eStore checkout review$/
   * REQUIREMENTS: At least one item is in the cart and a CHECKOUT button on a dialog is visible
   */
  public function iProceedToEstoreCheckoutReview()
  {
    //Wait for CHECKOUT button to become visible then press it
    $this->setSelector(self::eSTORE_CHECKOUT_BTN);

    //Be nice and wait for Ajax to finish before returning.
    $this->iWaitForAjaxCallsToFinish();
  }

  const eSTORE_CART_PAGE = '/cart';
  const eSTORE_CHECKOUT_ACTIVE_TAB = 'li.active';
  /**
   * @Then /^I should be on the checkout Review page$/
   * REQUIREMENTS: The current url is the cart (.../shop/???/cart)
   */
  public function iShouldBeOnTheCheckoutReviewPage()
  {
    //Get the current URL
    $browser_url = $this->getSession()->getCurrentUrl();

    //The eStore checkout Review page URLs end in "/cart", throw an exception in that case.
    if (strpos($browser_url, self::eSTORE_CART_PAGE) === FALSE) {
      throw new Exception('The eStore "' . $browser_url . '" is not is not the valid eStore CART CHECKOUT page.');
    }

    //Verify the eStore CHECKOUT review tab is active.
    $active_tab = $this->resolveSelector(self::eSTORE_CHECKOUT_ACTIVE_TAB);
    if (stripos($active_tab->getText(), "Review") === FALSE) {
      throw new Exception('The eStore "' . $browser_url . '" is not is not on the eStore Review tab.');
    }
  }

  /**
   * @Given /^I memorize the eStore cart for later$/
   * REQUIREMENTS: The current url is the cart (.../shop/???/cart)
   * Memorize the eStore cart contents for later comparison
   */
  public function iMemorizeTheEstoreCartForLater()
  {
    #Make sure this is the cart page then get it.
    $this->iShouldBeOnTheCheckoutReviewPage();
    $page = $this->getSession()->getPage();

    #Memorize the image links
    $images = $page->findAll('css', '.image');
    for ($i = 0; $i < count($images); $i++) {
      $this->memorizeData("CART_IMAGE_" . $i, $images[$i]->getHtml());
    }

    #Memorize the product names
    $names = $page->findAll('css', '.name');
    for ($i = 0; $i < count($names); $i++) {
      $this->memorizeData("CART_NAME_" . $i, $names[$i]->getText());
    }

    #Memorize individual product prices as well as product subtotals
    $prices = $page->findAll('css', '.cart_item .price');
    for ($i = 0; $i < count($prices); $i++) {
      $this->memorizeData("CART_PRICE_" . $i, $prices[$i]->getHtml());
    }

    #Memorize product quantities
    $qtys = $page->findAll('css', '.qty');
    for ($i = 0; $i < count($qtys); $i++) {
      $this->memorizeData("CART_QTY_" . $i, $qtys[$i]->getValue());
    }
    return;
}

  const eSTORE_ADDRESS_CONTAINER = ".store_image_color_overlay";
  /**
   * @Given /^I memorize the eStore address for later$/
   * REQUIREMENTS:  current page is a .../shop/???/... page that displays the eStore address
   * The key for the data is "AddressInfo"
   */
  public function iMemorizeTheEstoreAddressForLater()
  {
    #Memorize the physical location of the store
    $address = $this->resolveSelector(self::eSTORE_ADDRESS_CONTAINER)->getText();
    $this->memorizeData("AddressInfo", $address);
  }


  /*---------------------------------------------------------------------------
  | functions that verify the Confirm checkout page.
  ---------------------------------------------------------------------------*/
  const eSTORE_CONFIRMATION_CONTAINER = "div.contained.review";
  const eSTORE_BILLING_CONTAINERS = ".bill-info";
  const eSTORE_CONFIRMATION_PRICES =  'ul .ng-scope .price';
  const RE_STRIP_CURRENCY = "/CDN|£|\\\$/";
  const RE_CAN_USA_ADDRESS_STRIP = '/WHOLE FOODS MARKET(\s.*\w,\s\w+),( \w+)/'; #Separate address from zip and discard phone #
  const RE_UK_ADDRESS_STRIP = '/WHOLE FOODS MARKET(\s.*,\s)(.*),(\s\w+\s\w+)/';    #Separate address from zip and discard phone #
  const UK_VAT_PERCENT = 20;
  /**
   * @Given /^I verify the checkout Confirm page$/
   * REQUIREMENTS: Billing/Shipping/Pickup info has been successfully entered, current url is .../shop/???/CHECKOUT
   * and the Confirm tab is active
   * Dependencies: Billing data, Cart data, and perhaps
   */
  public function iVerifyTheCheckoutConfirmPage()
  {
    //Get the confirmation data from the Confirm page
    $confirmation_data = $this->resolveSelector(self::eSTORE_CONFIRMATION_CONTAINER)->getText();

    //Check the input billing data against the Confirm billing data.
    $billing_data = $this->recallData("BillingInfo");
    if (1 !== preg_match('/' . $billing_data . '/s', $confirmation_data)) {
      throw new Exception("Unable to match expected Billing info '" . $billing_data . "' ");
    }

    //If there is shipping info then check the input shipping data against the Confirm delivery data.
    if ($this->memorizedDataExists("DeliveryInfo")) {
        $delivery_data = $this->recallData("DeliveryInfo");
      if (1 !== preg_match('/' . $delivery_data . '/s', $confirmation_data)) {
        throw new Exception("Unable to match expected Delivery info '" . $delivery_data . "' ");
      }
    }

    //If there is eStore pickup info then check it against the Confirm data.
    if ($this->memorizedDataExists("PickupInfo")) {
      $pickup_data = $this->recallData("PickupInfo");
      $pickup_data = preg_replace('/\/0/',"/",$pickup_data); #remove leading '0' in day (mm/dd/yyyy)
      if (1 !== preg_match('/' .preg_quote($pickup_data, "/"). '/si', $confirmation_data)) {
        throw new Exception("Unable to match expected eStore Pickup info '" . $pickup_data . "'");
      }
      #Pickup info implies that Address info must also be checked against the confirmation data.
      #Separate the (Street, City, State) from the (zip code) and chop off the phone number
      $address_data = $this->recallData("AddressInfo");
      if( !preg_match(self::RE_CAN_USA_ADDRESS_STRIP, $address_data, $matches) ){
        preg_match(self::RE_UK_ADDRESS_STRIP, $address_data, $matches);
        $address_data = 'Pickup Location:'. $matches[1] . $matches[2] . $matches[3];
      }
      else{
        $address_data = 'Pickup Location:'. $matches[1] . $matches[2];
      }
      if (1 !== preg_match('/' . $address_data . '/si', $confirmation_data)) {
        throw new Exception("Unable to match expected eStore Address with the Pickup info '" . $address_data . "' ");
      }
    }

    #Begin verifying the itemized purchase list
    $page = $this->getSession()->getPage();
    $exceptions = [];

    #Verify the product images match the memorized cart images
    $images = $page->findAll('css', '.image');
    for ($i = 0; $i < count($images); $i++) {
      $cart_data = trim($this->recallData("CART_IMAGE_" . $i));
      $bill_data = trim($images[$i]->getHtml());
      if (0 <> strcasecmp($cart_data, $bill_data)) {
        $exceptions[] = "Bill image '" . $bill_data . "' does not match\nCart image '" . $cart_data . "'\n";
      }
    }

    #Verify the product names match the memorized cart names
    $names = $page->findAll('css', '.name');
    for ($i = 0; $i < count($names); $i++) {
      $cart_data = $this->recallData("CART_NAME_" . $i);
      $bill_data = $names[$i]->getText();
      if (0 <> strcasecmp($cart_data, $bill_data)) {
        $exceptions[] = "Bill name '" . $bill_data . "' does not match\nCart name '" . $cart_data . "'\n";
      }
    }

    #Verify the product quantities match the memorized cart quantities
    #Note: The cart quantity format is modified to match the Billing format
    $qtys = $page->findAll('css', '.quantity');
    for ($i = 0; $i < count($qtys); $i++) {
      $cart_data = 'x ' . $this->recallData("CART_QTY_" . $i);
      $bill_data = $qtys[$i]->getText();
      if (0 <> strcasecmp($cart_data, $bill_data)) {
        $exceptions[] = "Bill qty '" . $bill_data . "' does not match\nCart qty '" . $cart_data . "'\n";
      }
    }

    #Verify prices and totals separately due to code complexity
    #Merge in any detected exceptions.
    $pt_exceptions = $this->VerifyPricesAndTotals($page);
    $exceptions = array_merge($exceptions, $pt_exceptions);

    #Raise exception for Cart vs Billing differences (if any)
    if (count($exceptions) !== 0) {
      $e_list = "";
      foreach ($exceptions as $e) {
        $e_list = $e_list . $e . "\n"; #Add newlines to separate exceptions
      }
      throw new Exception($e_list);
    }
  }


  /**
   * Verify consistency and correctness of Prices and Totals appearing in the Cart (as memorized) vs
   * the checkout Confirm page.
   * For the UK the VAT rate is verified
   * @param $page the Checkout Confirm page.
   * @return array of detected differences
   */
  private function VerifyPricesAndTotals($page)
  {
    #Array to store mismatches
    $exceptions = [];

    #Verify the product prices match the memorized Cart prices
    #Note: There maybe more "prices" on the Confirm page due to Delivery or Shipping charges
    #      Therefore iterate Cart prices until empty
    $total_cart = 0.0;
    $prices = $page->findAll('css', self::eSTORE_CONFIRMATION_PRICES);
    for ($i = 0; $i < count($prices); $i++) {
      try {
        $cart_data = $this->recallData("CART_PRICE_" . $i);
      } catch (Exception $e) {
        #Leave this comparison section when there are no more cart prices to check
        break;
      }
      $cart_data = preg_replace('/<.*>/U', '', $cart_data);
      $bill_data = $prices[$i]->getHtml();
      if (0 <> strcasecmp($cart_data, $bill_data)) {
        $exceptions[] = "Bill price '" . $bill_data . "' does not match\nCart price '" . $cart_data . "'\n";
      }
      #Remove currency symbols and keep a runnin total of Cart prices that appeared in the Subtotal (blue) column
      if (FALSE !== stristr($prices[$i]->getAttribute('class'), 'blue')) {
        $total_cart += preg_replace(self::RE_STRIP_CURRENCY, "", $cart_data);
      }
    }

    #Verify the totals are valid in a mathematical sense
    $UK_Totals = false;
    $total_subtotal = 0.0;
    $total_tax = 0.0;
    $total_final = 0.0;
    $total_uk_wo_vat = 0.0;
    $total_uk_vat = 0.0;
    $total_shipping = 0.0;
    $total_delivery = 0.0;

    $totals_labels = $page->findAll('css', '.figure label');
    $totals_values = $page->findAll('css', '.figure .price');
    for ($i = 0; $i < count($totals_labels); $i++) {
      $total_label = $totals_labels[$i]->getText();
      $currency_value = preg_replace(self::RE_STRIP_CURRENCY, "", $totals_values[$i]->getText());
      #
      switch ($total_label) {
        case "Subtotal":
          $total_subtotal += $currency_value;
          break;
        case "Tax":
          $total_tax += $currency_value;
          break;
        case "Total":
        case "Estimated Total":
          $total_final += $currency_value;
          break;
        case "Total without VAT":
          $total_uk_wo_vat += $currency_value;
          $UK_Totals = true;
          break;
        case "VAT":
          $total_uk_vat += $currency_value;
          $UK_Totals = true;
          break;
        case "Shipping":
          $total_shipping += $currency_value;
          break;
        case "Delivery":
          $total_delivery += $currency_value;
          break;

        default:
          throw new Exception("Unknown Checkout Confirm tab total '" . $total_label . "'");

      }
      null;
    }

    #Determine how totals should be calculated based on country
    if ($UK_Totals){
      #UK totals are weird because the VAT is subtracted from the Subtotal and added back in later
      $total_expected = $total_uk_wo_vat + $total_uk_vat;
      if (round($total_cart, 2) != round($total_expected, 2)) {
        $exceptions[] = "Cart         total '" . $total_cart . "' does not match \n" .
                        "Checkout  Subtotal '" . $total_expected . "'";
      }

      $total_expected = $total_uk_wo_vat + $total_uk_vat + $total_delivery + $total_shipping;
      if (round($total_expected, 2) != round($total_final, 2)) {
        $exceptions[] = "Expected total '" . $total_expected . "' does not match \n" .
                        "Checkout total '" . $total_final . "'";
      }

      #Test is disabled because it is indeterminate which items do & don't have VAT applied
      #$vat_percentage = round(((100.0 * $total_uk_vat) / $total_uk_wo_vat), 0, PHP_ROUND_HALF_UP);
      #if (self::UK_VAT_PERCENT != $vat_percentage) {
      #  $exceptions[] = "Expected VAT% '" . self::UK_VAT_PERCENT . "' does not match \n" .
      #                  "Checkout VAT% '" . $vat_percentage . "'";
      #}
    }
    else {
      #Perform Canada and USA totals verifications.
      if (round($total_cart, 2) != round($total_subtotal, 2)) {
        $exceptions[] = "Cart         total '" . $total_cart . "' does not match \n" .
                        "Checkout  Subtotal '" . $total_subtotal . "'";
      }

      $total_expected = $total_subtotal + $total_delivery + $total_shipping + $total_tax;
      if (round($total_expected, 2) != round($total_final, 2)) {
        $exceptions[] = "Expected total '" . $total_expected . "' does not match \n" .
                        "Checkout total '" . $total_final . "'";
      }
    }
    return $exceptions;
  }

  /*---------------------------------------------------------------------------
  |Functions dealing with the checkout tabs Review / Billing / Confirm
  ---------------------------------------------------------------------------*/
  const eSTORE_CHECKOUT_PAGE = '/checkout';
  /**
   * @Given /^I should be on the checkout page$/
   * REQUIREMENTS: It is expected that current url is .../shop/???/CHECKOUT
   * Verify the current eStore is open
   * @throws Exception Not a checkout page
   */
  public function iShouldBeOnTheCheckoutPage()
  {
    //Get the current URL
    $browser_url = $this->getSession()->getCurrentUrl();

    //The eStore checkout page URLs end in "/checkout", throw an exception in that case.
    if (strpos($browser_url, self::eSTORE_CHECKOUT_PAGE) === FALSE) {
      throw new Exception('The eStore "' . $browser_url . '" is not is not a valid eStore CHECKOUT page.');
    }
  }

  /**
   * @Given /^I should be on the checkout Confirm page$/
   * REQUIREMENTS: The Billing/Shipping/Pickup info has been submitted and the current url is .../shop/???/CHECKOUT
   */
  public function iShouldBeOnTheCheckoutConfirmPage()
  {
    //Get the current URL
    $browser_url = $this->getSession()->getCurrentUrl();

    //The eStore checkout page URLs end in "/checkout", throw an exception if that is not the case.
    if (strpos($browser_url, self::eSTORE_CHECKOUT_PAGE) === FALSE) {
      throw new Exception('The eStore "' . $browser_url . '" is not is not a valid eStore CHECKOUT page.');
    }

    //Verify the eStore CHECKOUT review tab is active.
    $active_tab = $this->resolveSelector(self::eSTORE_CHECKOUT_ACTIVE_TAB);
    if (stripos($active_tab->getText(), "Confirm") === FALSE) {
      $filepath = $this->putScreenshot("iShouldBeOnTheCheckoutConfirmPage");
      throw new
         Exception("The eStore '$browser_url' is not is not on the eStore Confirm tab. \nSee screenshot at $filepath");
    }
  }

  /*---------------------------------------------------------------------------
  | Constants for Pickup/Delivery form
  ---------------------------------------------------------------------------*/
  const eSTORE_CHOOSE_PICKUP   = ".checkout #pickup";
  const eSTORE_CHOOSE_DELIVERY = ".checkout #delivery";
  const eSTORE_CHOICE_SUBMIT   = ".checkout button";
  /*---------------------------------------------------------------------------
  | Constants for locating elements on the Billing/Pickup/Shipping form
  ---------------------------------------------------------------------------*/
  //eStore Checkout page field ID constants
  const eSTORE_CHECKOUT_FORM = 'form[name="form"]';
  const eSTORE_CHKOUT_EMAIL = "[name='email']";
  const eSTORE_BILLING_CC_NUMBER = "[name='cc_num']";
  const eSTORE_BILLING_CC_MONTH = "[name='cc_exp_month']";
  const eSTORE_BILLING_CC_YEAR = "[name='cc_exp_year']";
  const eSTORE_BILLING_CC_CVV = "[name='cc_cvv']";
  const eSTORE_BILLING_FIRSTNAME = "[name='firstname']";
  const eSTORE_BILLING_LASTNAME = "[name='lastname']";
  const eSTORE_BILLING_STREET1 = "[name='street1']";
  const eSTORE_BILLING_STREET2 = "[name='street2']";
  const eSTORE_BILLING_CITY = "[name='city']";
  const eSTORE_BILLING_COUNTRY = ".country_field select";
  const eSTORE_BILLING_STATE = 'div[ng-if="billing.states"] select';
  const eSTORE_BILLING_ZIP = "jQuery('input[ng-model=\"billing.zip\"]')";
  const eSTORE_BILLING_AS_SHIPPING = "#clone";
  const eSTORE_CHKOUT_PHONE = "[name='phone1']";
  const eSTORE_CHKOUT_PHONE_EXT = "[name='phone1ext']";
  const eSTORE_SHIPPING_FIRSTNAME = "[name='shipping_firstname']";
  const eSTORE_SHIPPING_LASTNAME = "[name='shipping_lastname']";
  const eSTORE_SHIPPING_STREET1 = "[name='shipping_street1']";
  const eSTORE_SHIPPING_STREET2 = "[name='shipping_street2']";
  const eSTORE_SHIPPING_CITY = "[name='shipping_city']";
  const eSTORE_SHIPPING_COUNTRY = "select[ng-model=\"shipping.country\"]";
  const eSTORE_SHIPPING_STATE = "select[ng-model=\"shipping.state\"]";
  const eSTORE_SHIPPING_ZIP = "jQuery('input[ng-model=\"shipping.zip\"]')";
  const eSTORE_SHIPPING_METHOD = "jQuery('select[name=\"shipping_method\"]')";
  const eSTORE_COURIER_DELIVERY = "66";
  const eSTORE_PICKUP_DAY     = '[ng-model="pickup.day"]';
  const eSTORE_PICKUP_TIME    = '[ng-model="pickup.time"]';

  const eSTORE_CHKOUT_GIFT1 = "[name='GIFTMESSAGELINE1']";
  const eSTORE_CHKOUT_GIFT2 = "[name='GIFTMESSAGELINE2']";
  const eSTORE_CHKOUT_GIFT3 = "[name='GIFTMESSAGELINE3']";
  const eSTORE_SHIPPING_INSTRUCTIONS = "[name='delivery_details']";
  const eSTORE_PICKUP_DETAILS = '[name="pickup_details"]';

  /*---------------------------------------------------------------------------
  | Default Valid Credit Cards
  ---------------------------------------------------------------------------*/
  static $CC_MASTER_CARD_DEFAULT = [
    self::eSTORE_BILLING_CC_NUMBER => '5105105105105100',
    self::eSTORE_BILLING_CC_MONTH => '12',
    self::eSTORE_BILLING_CC_YEAR => '2016',
    self::eSTORE_BILLING_CC_CVV => '591',
    ];

  static $CC_VISA_CARD_DEFAULT = [
    self::eSTORE_BILLING_CC_NUMBER => '4222222222222006',
    self::eSTORE_BILLING_CC_MONTH => '01',
    self::eSTORE_BILLING_CC_YEAR => '2017',
    self::eSTORE_BILLING_CC_CVV => '245',
  ];

  static $CC_DISCOVER_CARD_DEFAULT = [
    self::eSTORE_BILLING_CC_NUMBER => '6011111111111117',
    self::eSTORE_BILLING_CC_MONTH => '05',
    self::eSTORE_BILLING_CC_YEAR => '2017',
    self::eSTORE_BILLING_CC_CVV => '524',
  ];

  static $CC_AMEX_CORPRATE_CARD_DEFAULT = [
    self::eSTORE_BILLING_CC_NUMBER => '378734493671000',
    self::eSTORE_BILLING_CC_MONTH => '03',
    self::eSTORE_BILLING_CC_YEAR => '2017',
    self::eSTORE_BILLING_CC_CVV => '459',
  ];

  static $CC_AMEX_CARD_DEFAULT = [
    self::eSTORE_BILLING_CC_NUMBER => '378282246310005',
    self::eSTORE_BILLING_CC_MONTH => '06',
    self::eSTORE_BILLING_CC_YEAR => '2017',
    self::eSTORE_BILLING_CC_CVV => '512',
  ];

  static $CC_JCB_CARD_DEFAULT = [
    self::eSTORE_BILLING_CC_NUMBER => '3566002020360505',
    self::eSTORE_BILLING_CC_MONTH => '04',
    self::eSTORE_BILLING_CC_YEAR => '2017',
    self::eSTORE_BILLING_CC_CVV => '351',
  ];

  /*---------------------------------------------------------------------------
  | Default Billing per geographical region
  ---------------------------------------------------------------------------*/
  static $CANADA_BILLING_DEFAULT = [
    self::eSTORE_CHKOUT_EMAIL => 'Canada_Test@email.com',
    self::eSTORE_BILLING_FIRSTNAME => 'Bob',
    self::eSTORE_BILLING_LASTNAME => 'Laublaw',
    self::eSTORE_BILLING_STREET1 => '7919 Prime Blvd',
    self::eSTORE_BILLING_STREET2 => 'Bung 7907',
    self::eSTORE_BILLING_CITY => 'Megaopolus',
    self::eSTORE_BILLING_COUNTRY => 'CANADA',
    self::eSTORE_BILLING_STATE => 'Ontario',
    self::eSTORE_BILLING_ZIP => 'L3R5M6',
    self::eSTORE_CHKOUT_PHONE => '(521)523-5939',
    self::eSTORE_CHKOUT_PHONE_EXT => '619'
    ];

  static $UK_BILLING_DEFAULT = [
    self::eSTORE_CHKOUT_EMAIL => 'UK_Test@email.com',
    self::eSTORE_BILLING_FIRSTNAME => 'John',
    self::eSTORE_BILLING_LASTNAME => 'Bull',
    self::eSTORE_BILLING_STREET1 => '102 Hillside Ln.',
    self::eSTORE_BILLING_STREET2 => '#7',
    self::eSTORE_BILLING_CITY => 'Worcester',
    self::eSTORE_BILLING_COUNTRY => 'UNITED KINGDOM',
    self::eSTORE_BILLING_STATE => 'Bedfordshire',
    self::eSTORE_BILLING_ZIP => 'W58SE ',
    self::eSTORE_CHKOUT_PHONE => '(020)50073684',
    self::eSTORE_CHKOUT_PHONE_EXT => '111',
    ];

  static $USA_BILLING_DEFAULT = [
    self::eSTORE_CHKOUT_EMAIL => 'USA_Test@email.com',
    self::eSTORE_BILLING_FIRSTNAME => 'Usa',
    self::eSTORE_BILLING_LASTNAME => 'Americo',
    self::eSTORE_BILLING_STREET1 => '919 Ross St.',
    self::eSTORE_BILLING_STREET2 => '#1',
    self::eSTORE_BILLING_CITY => 'Independence',
    self::eSTORE_BILLING_COUNTRY => 'USA',
    self::eSTORE_BILLING_STATE => 'California',
    self::eSTORE_BILLING_ZIP => '94115',
    self::eSTORE_CHKOUT_PHONE => '(415)327.0740',
    self::eSTORE_CHKOUT_PHONE_EXT => '1372'
    ];

  /*---------------------------------------------------------------------------
  | Default Pickup
  ---------------------------------------------------------------------------*/
  static $PICKUP_DEFAULT = [
    self::eSTORE_PICKUP_DAY => 1,                                       #Default day + 1
    self::eSTORE_PICKUP_TIME => 2,                                      #Default time + 2
    #self::eSTORE_PICKUP_DETAILS => "Write YOU ARE MY HERO on the bag", #Deprecated
    ];

  /*---------------------------------------------------------------------------
  | Default Delivery per geographical region
  ---------------------------------------------------------------------------*/
  static $CANADA_DELIVERY_DEFAULT = [
    self::eSTORE_SHIPPING_FIRSTNAME => 'Aurora',
    self::eSTORE_SHIPPING_LASTNAME => 'Borealis',
    self::eSTORE_SHIPPING_STREET1 => 'Waynorth Rd.',
    self::eSTORE_SHIPPING_STREET2 => 'Upperfloor',
    self::eSTORE_SHIPPING_CITY => 'Icefloe',
    self::eSTORE_SHIPPING_COUNTRY => 'CANADA',
    self::eSTORE_SHIPPING_STATE => 'ALBERTA',
    self::eSTORE_SHIPPING_ZIP => 'E94NW ',
    self::eSTORE_BILLING_AS_SHIPPING => 'uncheck'
  ];

  static $UK_DELIVERY_DEFAULT = [
    self::eSTORE_SHIPPING_FIRSTNAME => 'Jane',
    self::eSTORE_SHIPPING_LASTNAME => 'Bull',
    self::eSTORE_SHIPPING_STREET1 => '102 Clapham Rd.',
    self::eSTORE_SHIPPING_STREET2 => '#1',
    self::eSTORE_SHIPPING_CITY => 'Sheffield',
    self::eSTORE_SHIPPING_COUNTRY => 'UNITED KINGDOM',
    self::eSTORE_SHIPPING_STATE => 'Yorkshire',
    self::eSTORE_SHIPPING_ZIP => 'E85NE ',
    self::eSTORE_BILLING_AS_SHIPPING => 'uncheck'
  ];

  static $USA_DELIVERY_DEFAULT = [
    self::eSTORE_SHIPPING_FIRSTNAME => 'Aubrey',
    self::eSTORE_SHIPPING_LASTNAME => 'Piedmont',
    self::eSTORE_SHIPPING_STREET1 => 'Lost Ridge Rd.',
    self::eSTORE_SHIPPING_STREET2 => 'Breezeport #45',
    self::eSTORE_SHIPPING_CITY => 'Horizonville',
    self::eSTORE_SHIPPING_COUNTRY => 'USA',
    self::eSTORE_SHIPPING_STATE => 'Maine',
    self::eSTORE_SHIPPING_ZIP => '03901',
    self::eSTORE_BILLING_AS_SHIPPING => 'uncheck'
  ];


  /*---------------------------------------------------------------------------
  | Default Gift Message (deprecated)
  ---------------------------------------------------------------------------*/
  static $GIFT_MESSAGE_DEFAULT = [
    self::eSTORE_CHKOUT_GIFT1 => '123456789T123456789W123456789R123456789F', #deprecated
    self::eSTORE_CHKOUT_GIFT2 => '123456789I123456789S123456789V123456789E', #deprecated
    self::eSTORE_CHKOUT_GIFT3 => '123456789N123456789H123456789T123456789W', #deprecated
    ];

  /*---------------------------------------------------------------------------
  | Default Shipping Method
  ---------------------------------------------------------------------------*/
  static $SHIPPING_METHOD_DEFAULT = [
    self::eSTORE_SHIPPING_METHOD => self::eSTORE_COURIER_DELIVERY,
    #self::eSTORE_SHIPPING_INSTRUCTIONS => "House key under the mat",         #deprecated
  ];
  /*---------------------------------------------------------------------------
  | Default Delivery/Pickup/Billing per geographical region
  ---------------------------------------------------------------------------*/

  /**
   * @When /^I enter valid Canada checkout inputs$/
   * REQUIREMENTS: The current url is .../shop/???/CHECKOUT with Pickup/Billing tab active.
   * Fill the Billing/Shipping/Pickup form with valid data
   * Also memorize specific values for later comparison
   */
  public function iEnterValidCanadaCheckoutInputs()
  {
    #Define the Canada data to use in the Billing/Pickup form
    #Define the Canada data to use in the Billing/Shipping form
    #Wait for the eStore Pickup/Delivery/Billing form to appear
    $section_titles = $this->resolveSelector(" h3", self::eSTORE_CHECKOUT_FORM, TRUE);

    #Use the Billing form's section titles to build the valid CANADA input data
    $data = [];
    foreach ($section_titles as $section_title) {
      $title_text = strtoupper($section_title->getText());
      switch ( $title_text ){
        case "SCHEDULE PICKUP":
          $data = array_merge($data, self::$PICKUP_DEFAULT);
          break;
        case "BILLING INFORMATION":
          $data = array_merge($data, self::$CC_AMEX_CORPRATE_CARD_DEFAULT);
          break;
        case "BILLING ADDRESS":
          $data = array_merge($data, self::$CANADA_BILLING_DEFAULT);
          break;
        case "SHIPPING METHOD":
          $data = array_merge($data, self::$SHIPPING_METHOD_DEFAULT);
          break;
        case "SHIPPING ADDRESS":
          $data = array_merge($data, self::$CANADA_DELIVERY_DEFAULT);
          break;
      }
    }

    #Load the defined Canada data into the Billing/Pickup form
    $this->formLoader(self::eSTORE_CHECKOUT_FORM, $data);

    #Memorize the Billing data
    $this->memorizeData("BillingInfo", "Billing" .' '.
        $data[self::eSTORE_BILLING_FIRSTNAME]    .' '. $data[self::eSTORE_BILLING_LASTNAME] .' '.
        $data[self::eSTORE_BILLING_STREET1]      .' '. $data[self::eSTORE_BILLING_CITY]     .', '.
        'ON'                                     .' '. $data[self::eSTORE_BILLING_ZIP]             );

    #If Pickup data is present then pull the values from the form and memorize it
    if (array_key_exists(self::eSTORE_PICKUP_DAY,$data)) {
      $pickup_day = $this->getSelectedOption(self::eSTORE_PICKUP_DAY);
      $pickup_time = $this->getSelectedOption(self::eSTORE_PICKUP_TIME);
      $this->memorizeData("PickupInfo", "Time: $pickup_day $pickup_time");
      #"Pickup note:" . ' ' . $data[self::eSTORE_PICKUP_DETAILS]       #deprecated
    }
    #If Shipping data is present then memorize it
    if (array_key_exists(self::eSTORE_SHIPPING_FIRSTNAME,$data)){
      $this->memorizeData("DeliveryInfo", "Delivery"  .' '.
        $data[self::eSTORE_SHIPPING_FIRSTNAME]        .' '. $data[self::eSTORE_SHIPPING_LASTNAME]   .' ' .
        $data[self::eSTORE_SHIPPING_STREET1]          .' '. $data[self::eSTORE_SHIPPING_CITY]       .', '.
        $data[self::eSTORE_SHIPPING_STATE]            .' '. $data[self::eSTORE_SHIPPING_ZIP]             );
      #'Delivery note:' .' '. $data[self::eSTORE_SHIPPING_INSTRUCTIONS]); #deprecated
    }
  }

  /**
   * @When /^I enter valid USA checkout inputs$/
   * REQUIREMENTS: The current url is .../shop/???/CHECKOUT with Pickup/Billing tab active.
   * Fill the Billing/Shipping/Pickup form with valid data
   * Also memorize specific values for later comparison
   */
  public function iEnterValidUsaCheckoutInputs()
  {
    #Define the USA data to use in the Billing/Shipping form
    #Wait for the eStore Pickup/Delivery/Billing form to appear
    $section_titles = $this->resolveSelector(" h3", self::eSTORE_CHECKOUT_FORM, TRUE);

    #Use the Billing form's section titles to build the valid USA input data
    $data = [];
    foreach ($section_titles as $section_title) {
      $title_text = strtoupper($section_title->getText());
      switch ( $title_text ){
        case "SCHEDULE PICKUP":
          $data = array_merge($data, self::$PICKUP_DEFAULT);
          break;
        case "BILLING INFORMATION":
          $data = array_merge($data, self::$CC_JCB_CARD_DEFAULT);
          break;
        case "BILLING ADDRESS":
          $data = array_merge($data, self::$USA_BILLING_DEFAULT);
          break;
        case "SHIPPING METHOD":
          $data = array_merge($data, self::$SHIPPING_METHOD_DEFAULT);
          break;
        case "SHIPPING ADDRESS":
          $data = array_merge($data, self::$USA_DELIVERY_DEFAULT);
          break;
      }
    }

    #Load the defined USA data into the Billing/Pickup form
    $this->formLoader(self::eSTORE_CHECKOUT_FORM, $data);

    #Memorize the Billing data
    $state = $data[self::eSTORE_BILLING_STATE];
    $state_abbrev = self::$STATE_ABBR[$state];
    $this->memorizeData("BillingInfo", "Billing"  .' '.
        $data[self::eSTORE_BILLING_FIRSTNAME]     .' '. $data[self::eSTORE_BILLING_LASTNAME] .' '.
        $data[self::eSTORE_BILLING_STREET1]       .' '. $data[self::eSTORE_BILLING_CITY]     .', '.
        $state_abbrev                             .' '. $data[self::eSTORE_BILLING_ZIP]            );

    #If Pickup data is present then pull the values from the form and memorize it
    if (array_key_exists(self::eSTORE_PICKUP_DAY,$data)) {
      $pickup_day = $this->getSelectedOption(self::eSTORE_PICKUP_DAY);
      $pickup_time = $this->getSelectedOption(self::eSTORE_PICKUP_TIME);
      $this->memorizeData("PickupInfo", "Time:" . ' ' .
        $pickup_day . ' ' . $pickup_time);
      # "Pickup note:" . ' ' . $data[self::eSTORE_PICKUP_DETAILS] ); #deprecated
    }
    #If Shipping data is present then memorize it
    if (array_key_exists(self::eSTORE_SHIPPING_FIRSTNAME,$data)){
      $state = $data[self::eSTORE_SHIPPING_STATE];
      $state_abbrev = self::$STATE_ABBR[ $state ];
      $this->memorizeData("DeliveryInfo", "Delivery "                                              .
        $data[self::eSTORE_SHIPPING_FIRSTNAME]  .' '. $data[self::eSTORE_SHIPPING_LASTNAME]   .' ' .
        $data[self::eSTORE_SHIPPING_STREET1]    .' '. $data[self::eSTORE_SHIPPING_CITY]       .', '.
        $state_abbrev                           .' '. $data[self::eSTORE_SHIPPING_ZIP]             );
      #'Delivery note:' .' '. $data[self::eSTORE_SHIPPING_INSTRUCTIONS]); #deprecated
    }
  }

  /**
   * @When /^I enter valid UK checkout inputs$/
   * REQUIREMENTS: The current url is .../shop/???/CHECKOUT with Pickup/Billing tab active.
   * Fill the Billing/Shipping/Pickup form with valid data
   * Also memorize specific values for later comparison
   */
  public function iEnterValidUkCheckoutInputs()
  {
    #Define the UK data to use in the Billing/Shipping form
    #Wait for the eStore Pickup/Delivery/Billing form to appear
    $section_titles = $this->resolveSelector(" h3", self::eSTORE_CHECKOUT_FORM, TRUE);

    #Use the Billing form's section titles to build the valid UK input data
    $data = [];
    foreach ($section_titles as $section_title) {
      $title_text = strtoupper($section_title->getText());
      switch ( $title_text ){
        case "SCHEDULE PICKUP":
          $data = array_merge($data, self::$PICKUP_DEFAULT);
          break;
        case "BILLING INFORMATION":
          $data = array_merge($data, self::$CC_VISA_CARD_DEFAULT);
          break;
        case "BILLING ADDRESS":
          $data = array_merge($data, self::$UK_BILLING_DEFAULT);
          break;
        case "SHIPPING METHOD":
          $data = array_merge($data, self::$SHIPPING_METHOD_DEFAULT);
          break;
        case "SHIPPING ADDRESS":
          $data = array_merge($data, self::$UK_DELIVERY_DEFAULT);
          break;
      }
    }


    #Load the defined UK data into the Billing/Shipping form
    $this->formLoader(self::eSTORE_CHECKOUT_FORM, $data);

    //Memorize the billing data as strings for later comparison
    $this->memorizeData("BillingInfo", "Billing"  .' '.
      $data[self::eSTORE_BILLING_FIRSTNAME]       .' '. $data[self::eSTORE_BILLING_LASTNAME]  .' ' .
      $data[self::eSTORE_BILLING_STREET1]         .' '. $data[self::eSTORE_BILLING_CITY]      .', '.
      $data[self::eSTORE_BILLING_STATE]           .' '. $data[self::eSTORE_BILLING_ZIP]            );

    #If Pickup data is present then pull the values from the form and memorize it
    if (array_key_exists(self::eSTORE_PICKUP_DAY,$data)) {
      $pickup_day = $this->getSelectedOption(self::eSTORE_PICKUP_DAY);
      $pickup_time = $this->getSelectedOption(self::eSTORE_PICKUP_TIME);
      $this->memorizeData("PickupInfo", "Time: $pickup_day $pickup_time");
      # "Pickup note:" . ' ' . $data[self::eSTORE_PICKUP_DETAILS] ); #deprecated
    }
    #If Shipping data is present then memorize it
    if (array_key_exists(self::eSTORE_SHIPPING_FIRSTNAME,$data)){
      $this->memorizeData("DeliveryInfo", "Delivery "                                              .
        $data[self::eSTORE_SHIPPING_FIRSTNAME]  .' '. $data[self::eSTORE_SHIPPING_LASTNAME]   .' ' .
        $data[self::eSTORE_SHIPPING_STREET1]    .' '. $data[self::eSTORE_SHIPPING_CITY]       .', '.
        $data[self::eSTORE_SHIPPING_STATE]      .' '. $data[self::eSTORE_SHIPPING_ZIP]             );
      #'Delivery note:' .' '. $data[self::eSTORE_SHIPPING_INSTRUCTIONS]); #deprecated
    }
  }

  const eSTORE_CHECKOUT_REVIEW_ORDER = 'input.secure-btn';
  /**
   * @When /^I submit the checkout data as OK$/
   * REQUIREMENTS: The current url is .../shop/???/CHECKOUT with Pickup/Billing tab active.
   * Click REVIEW ORDER and wait for Confirm tab to load
   */
  public function iSubmitTheCheckoutDataAsOk()
  {
    #Leave the Pickup/Billing tab and goto the
    $this->setSelector(self::eSTORE_CHECKOUT_REVIEW_ORDER);

    #This is a special case because URL does not change and the active tab will already be present
    # Wait up to 10 seconds for the active tab to change to Billing
    for ($i = 0; $i < 10; $i++) {
      $active_tab = $this->resolveSelector(self::eSTORE_CHECKOUT_ACTIVE_TAB)->getText();
      if (stripos($active_tab, "Confirm") !== FALSE) return;
      sleep(1);
    }

  }

  /**
   * Utility routine to fill in forms from a data array
   * @param $form_selector: CSS form selector unique to the form element to load
   * @param $form_data: Array of [element selector, value] pairs to load into the form
   * @throws Exception
   */
  public function formLoader($form_selector, $form_data)
  {
    //Fill in the form with the form data
    foreach ($form_data as $field_id => $value) $this->setSelector($field_id, $value, $form_selector);
  }

  /**
   * Return the visible text, currently selected, in the specified drop down list
   * @param $select_selector
   * @return visible text string
   * @throws Exception Bad selector
   */
  private function getSelectedOption( $select_selector){
    #If the selector is a string resolve it otherwise assume it already an element node.
    if (is_string($select_selector)) $el_select = $this->resolveSelector($select_selector);
    else                             $el_select = $select_selector;

    #Make sure this is a select element
    if( $el_select->getTagName() != "select" ) throw new Exception('Bad selector: Not a SELECT tag');

    $selected_val = $el_select->getValue();
    $selected_text = $el_select->find("css", "option[value='" .$selected_val. "']")->getText();
    return $selected_text;
  }

  /**
   * Utility function to allow option selection via an index.
   */
  private function selectOptionByIndex($el_select, $index)
  {
    $options = $el_select->findAll("css", "option");
    $value = $options[$index]->getVaule();
    $el_select->setVaule($value);
  }

  /**
   * Utility function to memorize data in a formalized way.
   * @param $key A unique name to identify the data.
   * @param $value The data to store.
   */
  public function memorizeData($key, $value)
  {
    if (array_key_exists($key, $this->memorized_data)) {
      //A message to log that data is being over written should go here
      //$this->log(....);
      null;
    }
    $this->memorized_data[$key] = $value;
  }

  /**
   * Utility function to recall data in a formalized way.
   * @param $key the unique name used to identify the data
   * @return the data associated with the key
   * @throws Exception Key not found
   */
  public function recallData($key)
  {
    if (array_key_exists($key, $this->memorized_data)) return $this->memorized_data[$key];
    throw new Exception("Key not found '" . $key . "'. Please check the spelling and/or feature/context logic.");
  }

  /**
   * Utility function to determine if data has been set under a specific name.
   * @param $key the unique name used to identify the data
   * @return bool TRUE if key is found, otherwise FALSE
   */
  public function memorizedDataExists($key)
  {
    return array_key_exists($key, $this->memorized_data);
  }
    /**
     * @Then /^I switch to iframe$/
     */
    public function iSwitchToIframe() {
        $iframe=$this->getSession ()->getPage ()->find ( 'xpath', '//div[contains(@id,"payframe")]/iframe[contains(@id,"vantiv-payframe")]');
        $iframeId = $iframe->getAttribute('id');
        $this->getSession()->switchToIframe("$iframeId");
    }

} #eStoreDesktopSubcCntext