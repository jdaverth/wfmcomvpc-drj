<?php
require_once 'FeatureContext.php';
use Behat\Behat\Context\BehatContext;
use Drupal\DrupalExtension\Context\DrupalSubContextInterface;


class eStoreSubContext extends BehatContext implements DrupalSubContextInterface
{
  //GENERAL SELECTORS CONSTANTS
  const FIRST_ITEM_IN_STORE_SELECTOR = "ul.product.list:first-of-type li:first-of-type";
  const SECOND_ITEM_IN_STORE_SELECTOR = "ul.product.list:first-of-type li:nth-of-type(2)";
  const CART_ITEM_PRICE_SELECTOR = ".cart_item .price_wrapper .price";
  const CART_ITEM_PRICE_SUBTOTAL_SELECTOR = ".cart_item .blue.price";

  //ADD TO CART SELECTORS CONSTANTS
  const ADD_TO_CART_BUTTON_SELECTOR = ".addToCart";
  const ADD_TO_CART_POP_UP_SELECTOR = ".ng-scope";
  const ADD_TO_CART_POP_UP_X_SELECTOR = ".right";
  const ADD_TO_CART_POP_UP_X_LINK_TEXT = "x";
  const CONTINUE_SHOPPING_LINK_TEXT = "Continue shopping";
  const ADD_TO_CART_CHECKOUT_BUTTON_TEXT = "Checkout";
  const eSTORE_ITEM_OPTION =  ".itemOption";

  //ITEM QUANTITY MANIPULATION CONSTANTS
  const ITEM_PAGE_QUANTITY_SELECTOR = ".quantity";
  const ITEM_PAGE_QUANTITY_FIELD_SELECTOR = "qty";
  const CART_PAGE_QUANTITY_SELECTOR = ".cart_item .quantity";
  const CART_PAGE_QUANTITY_FIELD_SELECTOR = ".qty";
  const INCREASE_QUANTITY_BUTTON_SELECTOR = ".increase_qty";
  const DECREASE_QUANTITY_BUTTON_SELECTOR = ".decrease_qty";
  const ITEM_QUANTITY_MAX_VALUE = 1000;
  const ITEM_QUANTITY_MIN_VALUE = 1;

  //ORDER CONFIRMATION PAGE
  const CONFIRMATION_PAGE_SELECTOR = ".rec-conf";
  const CONFIRMATION_BREADCRUMBS = ".crumbs";
  const VERIFY_YOUR_IDENTITY = "email";
  const EMAIL_ADDRESS = "wfm.estore.test@gmail.com";
  const CONFIRMATION_VERIFICATION_PAGE_SELECTOR = ".body_inner";

  //CART
  const CART_ITEM_SELECTOR = ".cart_item";

  //BILLING INFO FIELDS
  const BILLING_INFO_CC_NUM = "cc_num";//"Credit Card *";//
  const BILLING_INFO_CC_EXP_MONTH = "cc_exp_month";
  const BILLING_INFO_CC_EXP_YEAR = "cc_exp_year";
  const BILLING_INFO_CC_CVV = "cc_cvv";

  public function __construct(array $parameters = null) {
    // do subcontext initialization
  }

  public static function getAlias() {
    return 'estore';
  }

  public function getSession() {
    return $this->getMainContext()->getSession();
  }

  public function getRegion($region) {
    return $this->getMainContext()->getRegion($region);
  }

  public function spin($lambda, $wait = 12) {
    $exception_message = '';
    for ($i = 0; $i < $wait; $i++) {
      try {
        if ($lambda($this)) {
          return TRUE;
        }
      } catch (Exception $e) {
        $exception_message = $e->getMessage() . "\n";
      }
      sleep(1);
    }
    $backtrace = debug_backtrace();
    $exception_message .= "Timeout thrown by " . $backtrace[1]['class'] . "::" . $backtrace[1]['function'] . "()\n";
    if (array_key_exists('file', $backtrace[1])) {
      $exception_message .= $backtrace[1]['file'];
    }
    if (array_key_exists('line', $backtrace[1])) {
      $exception_message .= ", line " . $backtrace[1]['line'];
    }
    throw new Exception($exception_message);
  }

//*.*.*.*.*.*.*.*.*.*.*.*.*.*.*-- ITEM SELECTION FUNCTIONS --*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*

  /**
   * @When /^I select the first item in the store$/
   *
   * REQUIREMENTS: You are on a valid and open store page.
   * This function will fail with a specific message if the store is closed.
   */
  public function iSelectTheFirstItemInTheStore() {
    $url = $this->getSession()->getCurrentUrl();
    if (strpos($url, "/closed")) {
      throw new Exception("This store is closed.");
    }
    $firstitem = $this->getSession()->getPage()->find('css', self::FIRST_ITEM_IN_STORE_SELECTOR);
    if (empty($firstitem)) {
      throw new Exception("Unable to find container " . self::FIRST_ITEM_IN_STORE_SELECTOR);
    }
    $firstitem->click();
  }
  /**
   * @When /^I select the second item in the store$/
   */
  public function iSelectTheSecondItemInTheStore() {
    //throw new PendingException();
    $url = $this->getSession()->getCurrentUrl();
    if (strpos($url, "/closed")) {
      throw new Exception("This store is closed.");
    }
    $firstitem = $this->getSession()->getPage()->find('css', self::SECOND_ITEM_IN_STORE_SELECTOR);
    if (empty($firstitem)) {
      throw new Exception("Unable to find container " . self::SECOND_ITEM_IN_STORE_SELECTOR);
    }
    $firstitem->click();

  }

//*.*.*.*.*.*.*.*.*.*.*.*.*.*.*-- ADD TO CART FUNCTIONS --*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*

  /**
   * @Then /^I add the item to the cart$/
   *
   * REQUIREMENTS: This function requires that you are currently on a valid item page with an enabled add to cart button
   * It also requires that the item you are viewing has NO drop down menus that would prevent adding to cart
   * First, this function, clicks the "ADD TO CART" button,
   * Then it waits for the mini-window to appear
   * Then it checks that the mini-window contains all the necessary options (with the correct urls):
   * x (stays on current page), continue shopping (takes user to the item's category page within the store), CHECKOUT (takes user to cart)
   */
  public function iAddTheItemToTheCart() {
    //CLICKS THE BUTTON:
    $button_selector = self::ADD_TO_CART_BUTTON_SELECTOR;
    $selections = $this->getSession()->getPage()->findAll("css", self::eSTORE_ITEM_OPTION);
    $this->spin(function($context) use ($button_selector, $selections) {
      $session = $context->getSession();
      $button_node = $session->getPage()->find('xpath', $session->getSelectorsHandler()->selectorToXpath('css', $button_selector));
      if (null === $button_node) {
        throw new Exception("Could not find container " . $button_selector);
      }
      //if the item has options to select from (and you can't add to cart without selecting), this select options randomly
      foreach ($selections as $select){
      $options = $select->findAll("css", "option");
      foreach ($options as $option) {
        $text = $option->getText();
        if (stristr($text,"Option") == false) break;
      }
        $select->selectOption($text);
      }
      $button_node->click();
      return TRUE;
    });

    //The little confirmation pop up takes a while to load and we can't seem to get a better work around. Spin doesn't allow us full usability here.
    $this->getMainContext()->iWaitForMilliseconds('12000');

    //CHECKS FOR LINKS (x, continue shopping, & checkout) IN THE POP UP AFTER CLICKING THE BUTTON:
    $container_selector = self::ADD_TO_CART_POP_UP_SELECTOR;
    $url = $this->getSession()->getCurrentUrl();
    $container = $this->getSession()->getPage()->find('css', $container_selector);
    if (empty($container)) {
      throw new Exception("Unable to find container " . $container_selector);
    }

    //FINDS THE CONTINUE SHOPPING LINK
    $continue_shopping_link_text = self::CONTINUE_SHOPPING_LINK_TEXT;
    $continue_shopping_link_node = $container->findLink($continue_shopping_link_text);
    if (empty($continue_shopping_link_node)) {
      throw new Exception("Unable to find link with text: " . $continue_shopping_link_text . " in container: " . $container_selector);
    }
    //finds the address attached to that link
    $url = $this->getSession()->getCurrentUrl();
    $address = $continue_shopping_link_node->getAttribute('href');
    if (strpos($url, $address) === false) {
      throw new Exception("Continue Shopping link address is " . $address . ", and does not match path to selected item: " . $url);
    }

    //FINDS THE X LINK
    $x_container_selector = self::ADD_TO_CART_POP_UP_X_SELECTOR;
    $x_link_text = self::ADD_TO_CART_POP_UP_X_LINK_TEXT;
    $x_container = $container->find('css', $x_container_selector);
    if (empty($x_container)) {
      throw new Exception("Unable to find selector " . $container_selector . " " . $x_container_selector);
    }
    if($x_container->getText() != $x_link_text) {
      throw new Exception("Unable to find text " . $x_link_text . " in container " . $x_container_selector . ". " . $x_container->getText() . " found instead.");
    }

    //FINDS THE "CHECKOUT" BUTTON/LINK
    $checkout_link_text = self::ADD_TO_CART_CHECKOUT_BUTTON_TEXT;
    $checkout_link_node = $container->findLink($checkout_link_text);
    if(empty($checkout_link_node)) {
      throw new Exception("Unable to find link with text " . $checkout_link_text . " in container " . $container_selector);
    }
    //calculates what url the Checkout button should be pointing to by grabbing current url, stripping category & item ids and then appending /cart to the end
    $pos = strpos($url, "/shop");
    $storepageurl = substr($url, 0, $pos + 9); // +9 to include the /shop/TLC part (and not cat&item ids)
    $cart_url_should_be = $storepageurl . "/cart";
    $cart_address = $checkout_link_node->getAttribute('href'); //grabs what the Checkout button actually points to
    if(strpos($cart_url_should_be, $cart_address) === false) {
      throw new Exception("Checkout link address is " . $cart_address . ", and does not match path to selected store cart: " . $cart_url_should_be);
    }
  }

//*.*.*.*.*.*.*.*.*.*.*.*.*.*.*-- ITEM QUANTITY (&PRICE) FUNCTIONS --*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*
//FOR INCREASING/DECREASING/SETTING ITEM QUANTITY FUNCTIONS, on /cart, we have to use ->find('css', ...) and on item pages, we have to use ->findById(...)

  /**
   * @Then /^I should see "([^"]*)" in the item quantity$/
   *
   * REQUIREMENTS: You are on a valid item page or on /cart with an item in the cart
   * in eStore with a quantity field that contains a value.
   */
  public function iShouldSeeInTheItemQuantity($value_should_be) {
    $url = $this->getSession()->getCurrentUrl();
    if (strpos($url, "/cart")) {
      $container_selector = self::CART_PAGE_QUANTITY_SELECTOR;
      $field_id = self::CART_PAGE_QUANTITY_FIELD_SELECTOR;
      $container = $this->getSession()->getPage()->find('css', $container_selector);
      if (empty($container)) {
        throw new Exception("Unable to find container " . $container_selector);
      }
      $field_node = $container->find('css', $field_id);
    }
    else {
      $container_selector = self::ITEM_PAGE_QUANTITY_SELECTOR;
      $field_id = self::ITEM_PAGE_QUANTITY_FIELD_SELECTOR;
      $container = $this->getSession()->getPage()->find('css', $container_selector);
      if (empty($container)) {
        throw new Exception("Unable to find container " . $container_selector);
      }
      $field_node = $container->findById($field_id);
    }
    if (empty($field_node)) {
      throw new Exception("Unable to find field by id " . $field_id);
    }
    // Grab the value from the field
    $field_actual_value = $field_node->getValue();

    // Handle empty string.
    if (strlen(trim($value_should_be) == 0)) {
      $intval = (int) $field_actual_value;
      if ($intval) {
        throw new Exception("Item quantity seen: " . $field_actual_value . ". Does not match value expected: blank" );
      }
    } else {
      if ($field_actual_value != $value_should_be) {
        throw new Exception("Item quantity seen: " . $field_actual_value . ". Does not match value expected: " . $value_should_be);
      }
    }
  }


 /**
  * @Then /^I increase the item quantity$/
  *
  * REQUIREMENTS: You are on a valid item page or on /cart with an item in the cart
  * in eStore with item increase/decrease quantity feature
  * VALUES: $container_selector should be the css selector for the +/- buttons and entry field
  * $field_id should be the css selector for the actual field with the value
  * $button_id_name should be the css selector for the increase or decrease button
  */
  public function iIncreaseTheItemQuantity() {
    $url = $this->getSession()->getCurrentUrl();
    if (strpos($url, "/cart")) {
      $container_selector = self::CART_PAGE_QUANTITY_SELECTOR;
      $field_id = self::CART_PAGE_QUANTITY_FIELD_SELECTOR;
      $container = $this->getSession()->getPage()->find('css', $container_selector);
      if (empty($container)) {
        throw new Exception("Unable to find container " . $container_selector);
      }
      $field_node = $container->find('css', $field_id);
    }
    else {
      $container_selector = self::ITEM_PAGE_QUANTITY_SELECTOR;
      $field_id = self::ITEM_PAGE_QUANTITY_FIELD_SELECTOR;
      $container = $this->getSession()->getPage()->find('css', $container_selector);
      if (empty($container)) {
        throw new Exception("Unable to find container " . $container_selector);
      }
      $field_node = $container->findById($field_id);
    }
    $button_id_name = self::INCREASE_QUANTITY_BUTTON_SELECTOR;

    //gets current value to check if new value is correctly updated
    $field_previous_value = $field_node->getValue();
    //now clicks the increase quantity button to increase the quantity
    $button_node = $container->find('css', $button_id_name);
    if (empty($button_node)) {
      throw new Exception("Unable to find button " . $button_id_name);
    }
    $button_node->click();
    //gets new value of quantity field and then compares
    $field_new_value = $field_node->getValue();
    if ($field_new_value != ($field_previous_value + 1) && $field_previous_value != self::ITEM_QUANTITY_MAX_VALUE){
      throw new Exception("New quantity field value (" . $field_new_value . ") is not an increment of previous quantity field value (" . $field_previous_value . ").");
    }
  }

 /**
  * @Then /^I decrease the item quantity$/
  *
  * REQUIREMENTS: You are on a valid item page or on /cart with an item in the cart
  * in eStore with item increase/decrease quantity feature
  * VALUES: $container_selector should be the css selector for the +/- buttons and entry field
  * $field_id should be the css selector for the actual field with the value
  * $button_id_name should be the css selector for the increase or decrease button
  */
  public function iDecreaseTheItemQuantity() {
    $url = $this->getSession()->getCurrentUrl();
    if (strpos($url, "/cart")) {
      $container_selector = self::CART_PAGE_QUANTITY_SELECTOR;
      $field_id = self::CART_PAGE_QUANTITY_FIELD_SELECTOR;
      $container = $this->getSession()->getPage()->find('css', $container_selector);
      if (empty($container)) {
        throw new Exception("Unable to find container " . $container_selector);
      }
      $field_node = $container->find('css', $field_id);
    }
    else {
      $container_selector = self::ITEM_PAGE_QUANTITY_SELECTOR;
      $field_id = self::ITEM_PAGE_QUANTITY_FIELD_SELECTOR;
      $container = $this->getSession()->getPage()->find('css', $container_selector);
      if (empty($container)) {
        throw new Exception("Unable to find container " . $container_selector);
      }
      $field_node = $container->findById($field_id);
    }
    $button_id_name = self::DECREASE_QUANTITY_BUTTON_SELECTOR;

    if (empty($field_node)) {
      throw new Exception("Unable to find field by id " . $field_id);
    }
    //gets current value to check if new value is correctly updated
    $field_previous_value = $field_node->getValue();
    //now clicks the decrease quantity button to increase the quantity
    $button_node = $container->find('css', $button_id_name);
    if (empty($button_node)) {
      throw new Exception("Unable to find button " . $button_id_name);
    }
    $button_node->click();
    //gets new value of quantity field and then compares
    $field_new_value = $field_node->getValue();
    if ($field_new_value != ($field_previous_value - 1) && $field_previous_value != self::ITEM_QUANTITY_MIN_VALUE){
      throw new Exception("New quantity field value (" . $field_new_value . ") is not an decrement of previous quantity field value (" . $field_previous_value . ").");
    }
  }

 /**
  * @Given /^I set the item quantity to "([^"]*)"$/
  *
  * REQUIREMENTS: You are on a valid item page or on /cart with an item in the cart
  * in eStore with item increase/decrease quantity feature
  * VALUES: $container_selector should be the css selector for the +/- buttons and entry field
  * $field_id should be the css selector for the actual field with the value
  */
  public function iSetTheItemQuantityTo($value_to_set) {
    //You can pass in value "random" to grab a random value between 50 and 900. This is to make sure purchases are over $40 and randomized so that credit cards don't bounce back thinking it's a duplicate submission of the same order.
    if ($value_to_set == "random") {
      $value_to_set = rand(50, 900);
    }
    $url = $this->getSession()->getCurrentUrl();
    if (strpos($url, "/cart")) {
      $container_selector = self::CART_PAGE_QUANTITY_SELECTOR;
      $field_id = self::CART_PAGE_QUANTITY_FIELD_SELECTOR;
      $container = $this->getSession()->getPage()->find('css', $container_selector);
      if (empty($container)) {
        throw new Exception("Unable to find container " . $container_selector);
      }
      $field_node = $container->find('css', $field_id);
    }
    else {
      $container_selector = self::ITEM_PAGE_QUANTITY_SELECTOR;
      $field_id = self::ITEM_PAGE_QUANTITY_FIELD_SELECTOR;
      $container = $this->getSession()->getPage()->find('css', $container_selector);
      if (empty($container)) {
        throw new Exception("Unable to find container " . $container_selector);
      }
      $field_node = $container->findById($field_id);
    }

    if (empty($field_node)) {
      throw new Exception("Unable to find field by id " . $field_id);
    }

    //sets the field to value provided
    $container->fillField($field_id, $value_to_set);

    //gets new value of quantity field
    $field_new_value = $field_node->getValue();

    //value must be an integer and between 1 and 1000 or it gets set to 1.
    try {
      $num_expected = intval($value_to_set);
    }
    catch(Exception $e) {
      $num_expected = self::ITEM_QUANTITY_MIN_VALUE;
    }
    //eStore handles negative numbers weird:
    //the "-" is immediately changed into a "1"
    //and then the rest of the number is inputted
    //so inputting "-2" will change the "-" to "1" and then add the "2"
    //resulting in "12" rather than just changing the whole thing to "1"
    //The if statement below reads this oddity as a valid return
    if (0 > $num_expected) {
      $string = strval($num_expected);
      $string = substr($string, 1);
      $string = "1" . $string;
      $num_expected = intval($string);
    }

    if (1 > $num_expected || $num_expected > 1000){
      $num_expected = self::ITEM_QUANTITY_MIN_VALUE;
    }

    if ($field_new_value != $num_expected) {
      throw new Exception("Quantity should be set to " . self::ITEM_QUANTITY_MIN_VALUE . " when value inputted not an int or is out of bounds.\nValue inputted: " . $value_to_set . "\nValue Expected: " . $num_expected . "\nValue Seen: " . $field_new_value);
    }
    /*print "\nValue Inputted: " . $value_to_set;
    print "\nValue Expected: " . $num_expected;
    print "\nValue Seen: " . $field_new_value;
    print "\n";*/
  }

  /**
   * @Then /^I check the item subtotal$/
   * REQUIREMENTS: You are on /cart with at least one valid item in the cart
   * The item subtotal must be equal to the price x quantity
   */
  public function iCheckTheItemSubtotal() {
    $item_price_selector = self::CART_ITEM_PRICE_SELECTOR;
    $item_subtotal_selector = self::CART_ITEM_PRICE_SUBTOTAL_SELECTOR;

    $quantity_container_selector = self::CART_PAGE_QUANTITY_SELECTOR;
    $field_id = self::CART_PAGE_QUANTITY_FIELD_SELECTOR;

    //any increase/decrease/set change to the quantity field will take a minute to update the subtotal.
    $this->getMainContext()->iWaitForMilliseconds('3000');

    $quantity_container = $this->getSession()->getPage()->find('css', $quantity_container_selector);
    if (empty($quantity_container)) {
      throw new Exception("Unable to find container " . $quantity_container_selector);
    }
    $field_node = $quantity_container->find('css', $field_id);
    if(empty($field_node)) {
      throw new Exception("Unable to find fieldId " . $field_id);
    }
    $quantity_value = $field_node->getValue();

    $item_price_container = $this->getSession()->getPage()->find('css', $item_price_selector);
    if (empty($item_price_container)) {
      throw new Exception("Unable to find container " . $quantity_container_selector);
    }
    $item_price = $item_price_container->getText();

    $item_subtotal_container = $this->getSession()->getPage()->find('css', $item_subtotal_selector);
    if (empty($item_subtotal_container)) {
      throw new Exception("Unable to find container " . $quantity_container_selector);
    }
    $item_subtotal = $item_subtotal_container->getText();

    //print "\nITEM QUANTITY: ". $quantity_value ."\n";
    //print "\nITEM PRICE: ". $item_price ."\n";
    //print "\nITEM SUBTOTAL: ". $item_subtotal ."\n";

    //They contain $ at the beginning, so just removing those here.
    $item_price = substr($item_price, 1);
    $item_subtotal = substr($item_subtotal, 1);

    //converting from strings to numerics.
    $item_quantity = intval($quantity_value);
    $item_price = floatval($item_price);
    $item_subtotal = floatval($item_subtotal);
    $precision = .001;
    //$stopfailing = abs(floatval($item_subtotal) - floatval($item_price * $item_quantity));
    //print "\nHERE: " . $stopfailing . "\n";
    if(abs(floatval($item_subtotal) - floatval($item_price * $item_quantity)) > $precision) {
      throw new Exception("Item Subtotal (" . $item_subtotal . ") does not match item price (" . $item_price . ") times item quantity (" . $item_quantity . ") which is (" . floatval($item_quantity * $item_price) . ").");
    }
  }


  /**
   * @Then /^I check the item price$/
   */
  public function iCheckTheItemPrice() {
    return;
    #throw new PendingException();
  }


//*.*.*.*.*.*.*.*.*.*.*.*.*.*.*-- CONFIRMATION PAGE --*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*


  /**
   * @Then /^I check the order confirmation number$/
   *
   * This function parses for the order confirmation number on the page.
   * It then makes sure the "View Order" button and the order confirmation number link both direct users to the correct url
   * Requires that tests are on the order confirmation page.
   */
  public function iCheckTheOrderConfirmationNumber() {
    //Credit Card Zero Authorization: There was an error processing your request. Please try again. (ERROR - CM00002-External message system error.) --
    //Should check the entire order confirmation page
    //AND if errored, report so.

    $pageText = $this->getSession()->getPage()->getText();
    if (strstr($pageText, "Credit Card Zero Authorization:")) {
      throw new Exception("Credit Card Zero Authorization Error again.");
    }

    //check breadcrumbs
    $crumbs_selector = self::CONFIRMATION_BREADCRUMBS;
    $crumbs = $this->getSession()->getPage()->find('css', $crumbs_selector);
    if (empty($crumbs)) {
      throw new Exception("Unable to find container " . $crumbs_selector);
    }
    if(strstr($crumbs->getText(), "Order Confirmation") == FALSE) {
      throw new Exception("Unable to find 'Order Confirmation' in breadcrumbs");
    }

    //get the confirmation order number
    $container_selector = self::CONFIRMATION_PAGE_SELECTOR;
    $container = $this->getSession()->getPage()->find('css', $container_selector);
    if (empty($container)) {
      throw new Exception("Unable to find container " . $container_selector);
    }
    $text_to_parse = $container->getText();
    print "\nTEXT IN CONTAINER: \n" . $text_to_parse . "\n";
    $hash_pos = strpos($text_to_parse, "#");
    $order_number = substr($text_to_parse, ($hash_pos + 1), 5);
    print "\nORDER NUMBER: " . $order_number . "\n";

    //check the order confirmation number links to the url with said order confirmation number
    // /shop/xox/customers/orders/#####
    $link_text = "#" . $order_number;
    $link_node = $container->findLink($link_text);
    if (empty($link_node)) {
      throw new Exception("Unable to find link with text " . $link_text);
    }
    print "\nHREF FOUND: " . $link_node->getAttribute('href') . "\n";
    print "\nURL GENERATED: /shop/xox/customers/orders/" . $order_number . "\n";
    $order_url = "/shop/xox/customers/orders/" . $order_number;
    if (strstr(($link_node->getAttribute('href')), $order_url) == FALSE) {
      throw new Exception("Link with text '" . $link_text . "' does not have href '/shop/xox/customers/orders/" . $order_number . "' and instead has href '" . $link_node->getAttribute('href') . "'");
    }

    //THEN I PRESS "VIEW ORDER STATUS" and I should be on /shop/xox/customers/orders/#####
    $view_order_status_node = $container->findButton("View Order Status");
    if (empty($view_order_status_node)) {
      throw new Exception("Could not find button VIEW ORDER STATUS in container " . $container_selector);
    }
    $view_order_status_node->click();

    //check that we're on the correct page:
    $url = $this->getSession()->getCurrentUrl();
    print "\nURL: " . $url . "\n";
    if (strstr($url, $order_url) == FALSE) {
      throw new Exception("View Order Status button did not link to correct page, instead linked to " . $url);
    }

    //then we enter the email address used for this transaction
    $this->getSession()->getPage()->fillField(self::VERIFY_YOUR_IDENTITY, self::EMAIL_ADDRESS);
    $view_order = $this->getSession()->getPage()->findButton("View Order");
    $view_order->click();

    //then we check the new order confirmation page.
    $new_container_selector = self::CONFIRMATION_VERIFICATION_PAGE_SELECTOR;
    $new_container = $this->getSession()->getPage()->find('css', $new_container_selector);
    $new_text = $new_container->getText();
    print "\n NEW TEXT: \n" . $new_text . "\n";
    if (strstr($new_text, ("ORDER #" . $order_number)) == FALSE) {
      throw new Exception("Could not find 'Order #" . $order_number . "' on the page.");
    }

    if (strstr($new_text, "Status:") == FALSE) {
      throw new Exception("Could not find Status on the page.");
    }
  }


//*.*.*.*.*.*.*.*.*.*.*.*.*.*.*-- CHECKOUT PAGE --*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*


  /**
   * @Then /^I should not see the billing information fields$/
   */
  public function iShouldNotSeeTheBillingInformationFields() {
    $billing_info_fields = [self::BILLING_INFO_CC_NUM, self::BILLING_INFO_CC_EXP_MONTH, self::BILLING_INFO_CC_EXP_YEAR, self::BILLING_INFO_CC_CVV];
    $checkoutfield = $this->getSession()->getPage()->find('css', '.checkout');
    if (empty($checkoutfield)) {
      throw new Exception("Could not find checkout field.");
    }
    $allfields = $checkoutfield->findAll('css', 'input');
    foreach ($allfields as $input) {
      foreach($billing_info_fields as $field) {
        if ($input->getAttribute('name') == $field) {
          #print "\nFOUND: " . $field . "\n";
          throw new Exception("Found billing field " . $field . " on pay in store item checkout where it should not be.");
        }
      }
    }
  }


//*.*.*.*.*.*.*.*.*.*.*.*.*.*.*-- CART FUNCTIONS --*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*

  /**
   * @Then /^I remove the "([^"]*)" item from the cart$/
   *
   * REQUIREMENTS: Must be on cart page and item must be in the cart
   * Only removes the FIRST instance of that item in the cart
   */
  public function iRemoveTheItemFromTheCart($item_to_remove) {
    $cart_selector = self::CART_ITEM_SELECTOR;
    $cart_items = $this->getSession()->getPage()->findAll('css', $cart_selector);
    if (empty($cart_items)) {
      throw new Exception("Unable to find cart items.");
    }
    foreach($cart_items as $cart_item) {
      if (stristr($cart_item->getText(), $item_to_remove)) {
        //print "\n FOUND THE ITEM \n";
        $remove_button = $cart_item->findButton("Remove");
        if (empty($remove_button)) {
          throw new Exception("Cannot find remove button for '" . $item_to_remove . "'");
        }
        $remove_button->click();
        return;
      }
    }
    throw new Exception("Did not find item '" . $item_to_remove . "' in cart.");
  }



}//End eStoreSubContext
