Feature: Basic Site Load
  In order to assure basic functionality
  As an unauthenticated user
  I need to visit top-level pages
  And be certain that they load

@basic @unauth @javascript @quick
Scenario: Header loads correctly
  Given That the Window is Full Screen
  Given I am on "/healthy-eating"
  Then I should see the link "find a store" with href "/stores/list" in the "header" region
  Then I should see the link "SIGN IN / REGISTER" with href "/signin" in the "header" region

@basic @unauth @nojs @quick
Scenario: Home Page Loads
  Given I am on "/healthy-eating"
  Then I should see the text "Welcome" in the "header" region

@basic @unauth @nojs @quick
Scenario: Customer Service Page Loads
  Given I am on "/customer-service"
  Then I should see the heading "Customer Service" in the "content" region

@basic @unauth @nojs @quick
Scenario: Recipe Page Loads
  Given I am on "/recipes"
  Then the "edit-search-box" field should contain "Search our"
  Then I should see the link "Featured" in the "content" region
  Then I should see the link "Newest" in the "content" region
  Then I should see at least "9" "#quicktabs-tabpage-recipes-0 .views-row" elements

#@basic @unauth @nojs @quick
#Scenario: Healthy Eating Page Loads
#  Given I am on "/healthy-eating"
#  Then I should see the text "HEALTHY EATING" in the "marquee" region

@basic @unauth @nojs @quick
Scenario: Products Page Loads
  Given I am on "/about-our-products"
  Then I should see the text "About Our Products" in the "marquee" region
  Then I should see the text "Product Search" in the "contentTop" region
  Then the "div#block-views-generic-page-items-list-copy div.content div.view-generic-page-items div.view-content" element should contain at least "5" ".list-row" elements in the "content" region
  Then I should see the text "Organic Food FAQ" in the "sidebar" region
  Then the "div.view-content div.item-list" element should contain at least "2" ".views-row" elements in the "sidebar" region

@basic @unauth @nojs @quick
Scenario: Online Ordering Page Loads
  Given I am on "/online-ordering"
  Then I should see the text "Shop Online"
  Then I should see the link "Shop Now" with href "https://delivery.wholefoodsmarket.com"
  Then I should see the link "Select your Store" with href "/shop/choose"
  Then I should see the link "Buy Gift Cards Now" with href "//wholefoods.buyatab.com/custom/wholefoods/"

@basic @unauth @nojs @quick
Scenario: Mission & Values Page Loads
  Given I am on "/mission-values"
  Then I should see the text "Mission & Values" in the "content" region
  Then the "div#mission-values-wrapper" element should contain at least "3" ".slides" elements in the "content" region

@basic @unauth @nojs @quick
Scenario: Blog Page Loads
  Given I am on "/blog/whole-story"
  Then I should see the text "Whole Story" in the "contentTop" region
  Then the "div#block-views-blogs-blog-pages div.view-blogs div.view-content" element should contain at least "5" ".views-row" elements in the "content" region

@basic @unauth @nojs @quick
Scenario: Store Departments Page Loads
  Given I am on "/store-departments"
  Then I should see the text "Store Departments" in the "marquee" region
  Then I should see the text "Store Departments" in the "breadcrumb" region
  Then I should see the link "Bakery" in the "contentBottom" region
  Then I should see the link "Beer" in the "contentBottom" region
  Then I should see the link "Bulk" in the "contentBottom" region
  Then I should see the link "Cheese" in the "contentBottom" region
  Then I should see the link "Coffee & Tea" in the "contentBottom" region
  Then I should see the link "Floral" in the "contentBottom" region
  Then I should see the link "Grocery" in the "contentBottom" region
  Then I should see the link "Meat & Poultry" in the "contentBottom" region
  Then I should see the link "Prepared Foods" in the "contentBottom" region
  Then I should see the link "Produce" in the "contentBottom" region
  Then I should see the link "Seafood" in the "contentBottom" region
  Then I should see the link "Wine" in the "contentBottom" region
  Then I should see the link "Whole Body" in the "contentBottom" region
  Then I should see the link "Pets" in the "contentBottom" region

@basic @search @nojs @quick
Scenario: View search results
  Given I am on the homepage
  Then I perform a "paginated" "global" search for "cheese"

