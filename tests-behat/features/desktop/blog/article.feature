Feature: Blog landing page
  In order to assure basic functionality of the blog articles
  As an unauthenticated user
  I need to be able to go the blog page and view the articles on individual article pages

Background:
  Given I am on "blog/our-kosher-passover-picks"

@blog @nojs @quick
Scenario: All blog page elements are visible
  And I should see the link "More posts by Liz Burkhart"
  And I should see the text "Comment"
  And I should see an "#sidebar-second" element
  And I should see the heading "Related Posts"
  And I should see the heading "Recent Posts"
  And I should see the heading "Blog Roll"
  And I should see the heading "Blog Archives"
  And I should see the heading "Posts by Category"
  And I should see the heading "Search The Blog"
  And I should see the heading "About the Blog"
  And I should see at least "10" ".blogpost-title"
  And I should see at least "10" ".blog-roll-link"
  And I should see at least "30" ".view-id-blog_category_terms .field-content a"
  And I should see at least "30" ".view-id-blogs_archive .views-summary li a"

# Check a blog post and make sure there are some comments.
@blog @nojs @comments @quick
Scenario: Check a blog that has comments and make sure they are being displayed
  And I should see the heading "Comments"
  And I should see an ".comment-wrapper" element
  # on prod, there are 12 comments on this blog post at this time.
  And I should see at least "10" ".native-comment"
  And I should see at least "5" ".field-name-comment-body"
