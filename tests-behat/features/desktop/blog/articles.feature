Feature: Blog landing page   
  In order to assure basic functionality of the blog
  As an unauthenticated user 
  I need to assure all page blocks are displayed correctly 
  
Background:
  Given I am on "blog/whole-story"
  
@blog @nojs @quick
Scenario: Check basic blocks of the page are loading 
  Then I should see an ".blog-maintitle" element
  And I should see the link "Whole Story" in the "contentTop" region 
  And I should see "The Official Whole Foods Market® Blog" in the ".blog-maindesc" element
  And I should see at least "4" ".view-id-blogs .views-row"
  And I should see an ".pager" element 
  And I should see an "#sidebar-second" element
  And I should see the heading "Recent Posts"
  And I should see the heading "Blog Roll"
  And I should see the heading "Posts by Category"
  And I should see the heading "Search The Blog"
  And I should see the heading "Links"
  And I should see the heading "About the Blog"
  And I should see at least "10" ".blogpost-title"
  And I should see at least "10" ".blog-roll-link"
  And I should see at least "30" ".view-display-id-blog_category .field-content a"
  
@blog @javascript @refactor
Scenario: Check that side bar tabs are expanding and collapsing
  When I click on the element with css selector "#block-views-blogs-blog-roll h2"
  Then "#block-views-blogs-blog-roll .content" should be visible
  When I click on the element with css selector "#block-views-blogs-blog-roll h2"
  Then "#block-views-blogs-blog-roll .content" should not be visible
    
@blog @javascript @search
Scenario: Blog Search with query
  Then I perform a "paginated" "blog" search for "garden"
  
@blog @javascript @search
Scenario: Blog Search with query and no results
  Then I perform a "paginated" "blog" search for "bread"

@blog @nojs @search @refactor
Scenario: Blog pagination - checks three pages deep
  Then I click "» next"
  Then the url and querystring should match "/blog/whole-story?page=1"
  And each article should have a title, author, comment link and read more link
  Then I click "» next"
  Then the url and querystring should match "/blog/whole-story?page=2"
  And each article should have a title, author, comment link and read more link
  Then I click "» next"
  Then the url and querystring should match "/blog/whole-story?page=3"
  And each article should have a title, author, comment link and read more link
  
@blog @javascript @search
Scenario: Search for article and follow link to page 
  Then I perform a "non-paginated" "blog" search for "Make it Natural: Italian-Style Spaghetti Squash with Tempeh"
  Then I should be on "/blog/search/Make%20it%20Natural%3A%20Italian-Style%20Spaghetti%20Squash%20with%20Tempeh?f%5B0%5D=field_blog%3A6860"
  And I follow "Make it Natural: Italian-Style Spaghetti Squash with Tempeh"
  And I wait for "1000" milliseconds
  Then I should be on "/blog/whole-story/make-it-natural-italian-style-spaghetti-squash-tempeh"