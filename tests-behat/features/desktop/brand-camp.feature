Feature: Brand Camp functionality  (DOTS-nnn)
  As a Whole Foods Market website user
  I want to verify the Brand Camp filters work
  To verify the found products are relevant to the search query.
  To verify the customers receive a appropriate message when no products match the search query.

  Create behat tests for brand camp:
  Homepage:
    "Special Tiles" (like FIND A STORE)
    OK Are You On The List?
    OK Find A Store
    OK  Order Online Pick In Store
    OK  Join The Conversation
    OK  Download Our App
    OK  Connect with us
    OK  Send Virtual gift card
    OK  Calling All YAY Sayers

    Brand Camp articles and videos
  OK  LOAD MORE
  OK  Top Menu
  OK Filters
  OK Your Stories
  OK Curated Tiles


  @homepage @unauth @brandcamp
  Scenario: Brand Camp verify upper menu bar
    Given I am on homepage
    Then I verify upper menu bar

  @homepage @unauth @javascript @brandcamp
  Scenario Outline: Brand Camp Show Me filters
    Given I am on homepage
    And That the Window is Full Screen
    Then I set the Brand Camp SHOW ME filter to "<Filter>"
    Then Verify Brand Camp SHOW ME "<Filter>" is active
    And Verify no other Show Me tile count exceeds "<Filter>" tile count
    Examples:
     | Filter |
     | FOOD   |
     | PEOPLE |
     | PLANET |

  @homepage @unauth @javascript @brandcamp
  Scenario Outline: Brand Camp I want to filters
    #First Suggested Store Pod:
    Given I am on homepage
      And That the Window is Full Screen
    Then Verify Brand Camp I WANT TO "<Filter1>" differs from "<Filter2>"
     And Verify Brand Camp I WANT TO "<Filter2>" is active
    Examples:
    |Filter1|Filter2|
    | DO    | BOTH  |
    | LEARN | DO    |
    | BOTH  | LEARN |


  @homepage @unauth @javascript @brandcamp
  Scenario Outline: Brand Camp VIEW "YOUR STORIES" filter
    #First Suggested Store Pod:
    Given I am on homepage
    Then Verify Brand Camp VIEW "<Filter>" filter
    Examples:
      |    Filter    |
      | YOUR STORIES |


  @homepage @unauth @javascript @brandcamp
  Scenario Outline: Brand Camp VIEW "OUR STORIES" filter
    #First Suggested Store Pod:
    Given I am on "values-matter/your-stories"
    Then Verify Brand Camp VIEW "<Filter>" filter
    Examples:
      |    Filter    |
      | OUR STORIES  |

  #Verify that the LOAD MORE button adds 12 more tiles per press
  @homepage @unauth @javascript @brandcamp @slow
  Scenario: Brand Camp Load More
    Given I am on homepage
    And That the Window is Full Screen
    Then Verify Brand Camp tile count is "12"
    When Load More is Pressed "1" time(s)
    Then Verify Brand Camp tile count is "24"
    When Load More is Pressed "2" time(s)
    Then Verify Brand Camp tile count is "41"

  #Verify that the Scroll Top button appears and moves to top when pressed
  @homepage @unauth @javascript @brandcamp @slow
  Scenario: Brand Camp Load More
    Given I am on homepage
     And That the Window is Full Screen
     When Load More is Pressed "1" time(s)
     Given I wait for "2000" milliseconds
     Then I should see the elements: "#scrollTop"
     When Scroll Top is pressed
     Then I should not see the elements: "#scrollTop"
     #Then I break here

  @homepage @unauth @javascript @brandcamp @slow
  Scenario: Brand Camp Special Tile layout and content
    Given I am on homepage
    And That the Window is Full Screen
    When LOAD MORE Brand Camp is exhausted
    Then Verify Brand Camp Special Tiles are in place

  @homepage @unauth @javascript @brandcamp @slow
  Scenario: All special tiles are represented
    Given I am on homepage
    And That the Window is Full Screen
    When Load More is Pressed "3" time(s)
    And I wait for "2000" milliseconds
    Given Verify Brand Camp tile count is "41"
    Then Verify all Special tiles are present
    And Verify all Special tiles are redirected
    #Then I break here

  @homepage @unauth @javascript @brandcamp @slow
  Scenario: Brand Camp UGC Tile layout and content
    Given I am on homepage
    And I wait for "1000" milliseconds
    And That the Window is Full Screen
    And I wait for "1000" milliseconds
    When LOAD MORE Brand Camp is exhausted
    And I wait for "2000" milliseconds
    Then Verify Brand Camp UGC Tiles are in place

  @homepage @unauth @javascript @brandcamp @slow
  Scenario: Brand Camp Article,Video,Image Tile layout and content
    Given I am on homepage
    And That the Window is Full Screen
    #When LOAD MORE Brand Camp is exhausted
    Then Verify Brand Camp Article Tiles are in place

  @homepage @unauth @javascript @brandcamp
  Scenario: Brand Camp upper Menu Link operation
    Given I am on homepage
    And That the Window is Full Screen
    When I press the Brand Camp Menu "Link"
    Then Sidebar is open
    When I press the Brand Camp Menu "Link"
    Then Sidebar is closed

  @homepage @unauth @javascript @brandcamp
  Scenario: Brand Camp Menu Sidebar operation
    Given I am on homepage
    And That the Window is Full Screen
    When I press the Brand Camp Menu "SideBar"
    Then Sidebar is open
    When I press the Brand Camp Menu "SideBar"
    Then Sidebar is closed

  @homepage @unauth @javascript @brandcamp
  Scenario: Brand Camp Curated Tiles
    Given I am on homepage
    And That the Window is Full Screen
    And I wait for "1000" milliseconds
    When I filter Brand Camp by "YOUR STORIES"
    And I wait for "1000" milliseconds
    # Why search for 0 tiles?
    And I Search curated tiles for "Twinkies4ever"
    Then I should see "Twinkies4ever" has "0" curated tiles
    #When I Search curated tiles for "C"
    #Then I should see "C" has "multiple" curated tiles
    When I Search curated tiles for "X"
    Then I should see "X" has "4" curated tile
    When I Search curated tiles for "LI"
    Then I should see "LI" has "at least 1" curated tile
