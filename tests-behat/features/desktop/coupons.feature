Feature: Coupons
  To ensure coupons are working
  As an unauthenticated user

@coupons @unauth @nojs @quick
Scenario: Coupons Page Properly Loads
  Given I am on "/coupons"
  Then I should see a heading in the "salesAndCouponsHeaderBlock" region
  And I should not see an "#quicktabs-container-custom_quicktab_sales_coupons" element

@coupons @unauth @nojs @quick
Scenario: Coupons Display Based On Store In URL
  Given I am on "/coupons/lamar"
  Then I should see a heading in the "salesAndCouponsHeaderBlock" region
  And I should see coupons in the "salesAndCouponsQuicktab" region
  And I should see the text "Lamar" in the "breadcrumb" region

@coupons @unauth @javascript
Scenario: Store Select on /coupons page works
  Given I am on "/coupons"
  When I select "Texas" from "edit-state" in the "salesAndCouponsHeaderBlock" region
  When I additionally select "Lamar - 525 N Lamar Blvd." from "edit-store" in the "salesAndCouponsHeaderBlock" region
  And I press the submit button with value "View Sales & Coupons" in the "#block-wfm-blocks-wfm-blocks-coupon-filters" container in the "salesAndCouponsHeaderBlock" region
  Then I should be on "/sales-flyer/lamar"

@coupons @unauth @javascript
Scenario: Toggling between sales and coupons works
  Given I am on "/coupons/lamar"
  When I click "Sales"
  Then I should see sales items in the "salesAndCouponsQuicktab" region

@coupons @unauth @javascript @print
Scenario: Print coupon page has coupons
  Given I am on "/coupons/lamar"
  When I check the box "Select all coupons"
# Temporarily disabled while we figure out a solution.
#  Then I press "Print Selected"
#  Then I should be on a page with "print/mycoupons" in the url
#  And I should see "1" or more ".view-id-coupons .views-row" elements
