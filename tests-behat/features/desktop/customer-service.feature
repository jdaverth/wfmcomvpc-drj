Feature: Customer Service
  To verify the customer service page is working
  As an unauthenticated user
  With and without a store chosen

Background:
  Given I am on "/customer-service"
  
@customer-service @unauth @nojs @quick
Scenario: General Customer Service Page Content is present
  Then I should see "Customer Service" in the "breadcrumb" region
  Then I should see the "Customer Service" heading in the "content" region
  Then I should see the text "How Can We Help You?"
  Then I should see the link "Contact your local store" with href "/stores/list" in the "content" region
#  Then I should see the link "Contact our Gift Card department" with href "http://wholefoods.buyatab.com/custom/wholefoods/?_ga=1.79452180.1836112456.1482340200" in the "content" region
  Then I should see the link "Learn how to start the new vendor process" with href "/company-info/information-potential-suppliers" in the "content" region
  Then I should see the link "Careers" with href "/careers/find-and-apply-jobs" in the "content" region
  Then I should see the link "US National offices" with href "/company-info/us-national-offices" in the "content" region
  Then I should see the link "Regional Offices" with href "/company-info/regional-offices" in the "content" region
  Then I should see the link "Contact 365 by Whole Foods Market stores" with href "//365bywfm.com/contact" in the "content" region
  Then I should see the link "Request a Donation" with href "/donate" in the "content" region
  Then I should see the text "Send Us a Message"
  Then I should see the link "Find a Store" with href "/stores/list" in the "footer" region
  Then I should see the link "365 by Whole Foods Market™" with href "www.365bywfm.com" in the "footer" region




@customer-service @unauth @javascript
Scenario: Test that the form elements are present
   #check for form elements
  Then I should see the elements: "#edit-submitted-name"
  Then I should see the elements: "#edit-submitted-email"
  Then I should see the elements: "#edit-submitted-phone"
  Then I should see the elements: "#edit-submitted-step1-choose-topic"


# Numbers are store node ids.
@customer-service @unauth @javascript
Scenario: Try store feedback options
  Given I select "Store Feedback" from "edit-submitted-step1-choose-topic" in the "content" region

  # Roaring Fork
  And I select "Colorado" from "edit-submitted-step2-store-wrap-state-select" in the "content" region
  And I select "43216" from "edit-submitted-step2-store-wrap-store-select" in the "content" region
  Then I should see the element: "#phone-number-box-wrapper"
  Then I should see the element: "#phone-number-box"
  Then I should see the element: ".phone-number"
  Then I should see the text "For the fastest response, call this store directly"
  Then I should see the text "\(970\) 927-1500"
  Then I should see the element: "#edit-submitted-step3-body"
  Then I should see the text "Message"
  And I enter "Selwyn is a lovely chap" for "edit-submitted-step3-body"

  # Berkeley
  And I select "California" from "edit-submitted-step2-store-wrap-state-select" in the "content" region
  And I select "6484" from "edit-submitted-step2-store-wrap-store-select" in the "content" region
  Then I should see the text "510.649.1333"

  # Baton Rouge
  And I select "Louisiana" from "edit-submitted-step2-store-wrap-state-select" in the "content" region
  And I select "6488" from "edit-submitted-step2-store-wrap-store-select" in the "content" region
  Then I should see the text "\(225\) 218-0452"

  # Cambridge - River Street
  And I select "Massachusetts" from "edit-submitted-step2-store-wrap-state-select" in the "content" region
  And I select "6701" from "edit-submitted-step2-store-wrap-store-select" in the "content" region
  Then I should see the text "617.876.6990"

  # Seattle - Roosevelt Square
  And I select "Washington" from "edit-submitted-step2-store-wrap-state-select" in the "content" region
  And I select "6699" from "edit-submitted-step2-store-wrap-store-select" in the "content" region
  Then I should see the text "206.985.1500"

  # Canada North Burnaby
  And I select "British Columbia" from "edit-submitted-step2-store-wrap-state-select" in the "content" region
  And I select "3969481" from "edit-submitted-step2-store-wrap-store-select" in the "content" region
  Then I should see the text "604 205 5032"

  # UK - Giffnock
  And I select "United Kingdom" from "edit-submitted-step2-store-wrap-state-select" in the "content" region
  And I select "6552" from "edit-submitted-step2-store-wrap-store-select" in the "content" region
  Then I should see the text "0141 621 2700"

  And I press the "Send comment" button
  #test captcha
  Then I should see the text "The answer you entered for the CAPTCHA was not correct."
  # test required name & email fields
  And I should see the text "Name field is required."
  And I should see the text "Email address field is required."


#Online Ordering
@customer-service @unauth @javascript
Scenario: Test online ordering options
  Given I select "Online Ordering" from "edit-submitted-step1-choose-topic" in the "content" region
  And I should see the text "All online catering and holiday meals are handled by your local store."
  And I should see the text "For questions about gift cards call 1-844-WFM-CARD"

  # Berkeley
  And I select "California" from "edit-submitted-step2-store-wrap-state-select" in the "content" region
  And I select "6484" from "edit-submitted-step2-store-wrap-store-select" in the "content" region
  Then I should see the text "510.649.1333"

# Product Request
@customer-service @unauth @javascript
Scenario: Test Product request
    Given I select "Product Request" from "edit-submitted-step1-choose-topic" in the "content" region
    And I should see the text "Individual stores are responsible for their own product sourcing"
    And I should see the text "Select your store below to send your product request along to your local store team."

    # Berkeley
    And I select "California" from "edit-submitted-step2-store-wrap-state-select" in the "content" region
    And I select "6484" from "edit-submitted-step2-store-wrap-store-select" in the "content" region

# 365 Stores
@customer-service @unauth @javascript
Scenario: Test 365 stores
    Given I select "365 Stores" from "edit-submitted-step1-choose-topic" in the "content" region

    # Los Angeles - Silver Lake 365 store.
    And I select "California" from "edit-submitted-step2-store-wrap-365-state-select" in the "content" region
    And I select "893491" from "edit-submitted-step2-store-wrap-365-store-select" in the "content" region
#    And I select "365 Stores" from "edit-submitted-step1-choose-topic" in the "content" region
    Then I should see the text "Message"
    Then I should see the element: "#edit-submitted-step3-body"
    And I enter "Selwyn is a lovely chap" for "edit-submitted-step3-body"


# Store Location Request
@customer-service @unauth @javascript
Scenario: Store Location Request
    Given I select "Store Location Request" from "edit-submitted-step1-choose-topic" in the "content" region
    Then I should see the text "What ZIP code \(postal code\) do you think would be good for a new store location?"
    And I enter "78703" for "edit-submitted-step2-store-zip-code"
    Then I should see the text "Message"
    And I enter "Selwyn is a lovely chap" for "edit-submitted-step3-body"
    Then I should see the text "CAPTCHA"
    And I should see the text "This question is for testing whether or not you are a human visitor and to prevent automated spam submissions."

# Company Feedback
@customer-service @unauth @javascript
Scenario: Test Company Feedback
    Given I select "Company Feedback" from "edit-submitted-step1-choose-topic" in the "content" region
    And I should see the text "Although company feedback is handled by our corporate team, it helps us resolve your need faster if we know which store you visit most regularly."
    And I should see the text "Choose your state:"

    # Berkeley
    And I select "California" from "edit-submitted-step2-store-wrap-state-select" in the "content" region
    And I select "6484" from "edit-submitted-step2-store-wrap-store-select" in the "content" region
    Then I should see the text "510.649.1333"
    And I enter "Selwyn is a lovely chap" for "edit-submitted-step3-body"
    Then I should see the text "CAPTCHA"



# Test 365 Everyday Products
@customer-service @unauth @javascript
Scenario: Test 365 Everyday Products
    Given I select "365 Everyday Products" from "edit-submitted-step1-choose-topic" in the "content" region

    # Berkeley
    And I select "California" from "edit-submitted-step2-store-wrap-state-select" in the "content" region
    And I select "6484" from "edit-submitted-step2-store-wrap-store-select" in the "content" region
    Then I should see the text "Fill in the last six digits from your product label. If you don’t have the label, please be as specific as possible about the product name in the comments."
    And I enter "123456" for "edit-submitted-step3-product-upc"
    And I enter "Selwyn is a lovely chap" for "edit-submitted-step3-body"
    Then I should see the text "CAPTCHA"


# Test Product Standards
@customer-service @unauth @javascript
Scenario: Test Product Standards
    Given I select "Product Standards" from "edit-submitted-step1-choose-topic" in the "content" region

    Then I should see the text "Product standards include animal welfare, unacceptable ingredients, organic certification, and other similar topics. Feedback about store-prepared products and holiday meals should be sent to your local store. Select Store Feedback in the menu above to contact them."
    And I should see the text "If your feedback is about a specific product, please provide the name or the product category:"
    And I enter "Tasty Raw Kale" for "edit-submitted-step2-product-category"
    And I enter "Kale is the yummiest type of food ever and Selwyn is a lovely chap" for "edit-submitted-step3-body"
    Then I should see the text "CAPTCHA"

# Website Issues
@customer-service @unauth @javascript
Scenario: Test Website Issues
    Given I select "Website Issues" from "edit-submitted-step1-choose-topic" in the "content" region

    Then I should see the text "Please include a link to the page on our site about which you have a question or comment in the Message section. For questions about store sales and flyers please contact your local store. Select Store Feedback in the menu above to contact them."
    And I should see the text "URL of the page you're having trouble with:"
    And I enter "www.wholefoodsmarket.com/tasty-raw-kale" for "edit-submitted-step2-trouble-url"
    And I enter "Selwyn is a lovely chap" for "edit-submitted-step3-body"
    Then I should see the text "CAPTCHA"

# App Issues
@customer-service @unauth @javascript
Scenario: Test App Issues
  Given I select "App Issues" from "edit-submitted-step1-choose-topic" in the "content" region
  Then I should see the text "What device are you using?"
  And I select radio button with value "iphone_or_ipod"
  And I select radio button with value "ipad"
  And I select radio button with value "android_phone"
  And I select radio button with value "android_tablet"
  And I select radio button with value "dont_use_app"
  And I select radio button with value "iphone_or_ipod"
  Then I should see the text "Message"
  # Radio button select doesn't trigger the js event so I have to reselect app issues
  # to make the message box appear.
  And I select "App Issues" from "edit-submitted-step1-choose-topic" in the "content" region
  And I enter "That is a darned fine app you have there" for "edit-submitted-step3-body"
  Then I should see the text "CAPTCHA"

# Account Issues
@customer-service @unauth @javascript
Scenario: Test Account Issues
  Given I select "Account Issues" from "edit-submitted-step1-choose-topic" in the "content" region
  Then I should see the text "How do you sign in to wholefoodsmarket.com or the Whole Foods Market app?"
  And I select radio button with value "email"
  And I select radio button with value "yahoo"
  Then I should see the text "Message"
  And I enter "Selwyn is a lovely chap" for "edit-submitted-step3-body"
  Then I should see the text "CAPTCHA"


# Newsletter
@customer-service @unauth @javascript
Scenario: Test Newsletter
  Given I select "Newsletter" from "edit-submitted-step1-choose-topic" in the "content" region
  Then I should see the text "Please enter the email address you use to subscribe to the newsletter \(may differ from login email\)"
  And I enter "user_example.com" for "edit-submitted-step2-user-email"
  Then I should see the text "Message"
  And I enter "Selwyn is a lovely chap" for "edit-submitted-step3-body"
  Then I should see the text "CAPTCHA"
  And I press the "Send comment" button
  # test captcha
  Then I should see the text "The answer you entered for the CAPTCHA was not correct."
  # test required name & email fields
  And I should see the text "is not a valid email address."


@customer-service @unauth @javascript
Scenario: Test if store correct state/store shows up after selecting state/store in page footer
  Given I enter "CA" for "edit-state"
  #Berkeley store.
  And I enter "6484" for "edit-store"
  And I press the "Continue" button
  And I wait for "1000" milliseconds
  Then I should see the element: ".store-box"
  And I should see the text "3000 Telegraph Ave"
  And I should see the text "Your Store Is Berkeley"
  Then I should see the link "Berkeley" with href "/stores/berkeley" in the "footer" region
  And I enter "Ludwig von Beethoven" for "edit-submitted-name"
  And I enter "Ludwig@Beethoven.com" for "edit-submitted-email"
  And I enter "111-222-3333" for "edit-submitted-phone"
  And I select "Store Feedback" from "edit-submitted-step1-choose-topic" in the "content" region
  # Now the State should be California
  Then the dropdown "#edit-submitted-step2-store-wrap-state-select" shows "CA"
  # the store should be Berkeley - 3000 Telegraph Ave
  Then the dropdown "#edit-submitted-step2-store-wrap-store-select" shows "6484"
  And I enter "Ramesh is a lovely chap" for "edit-submitted-step3-body"



#Note this test will need to be modified  once we figure out a workaround for the CAPTCHA test
@customer-service @unauth @javascript
Scenario: Customer service form submits correctly for store feedback
  Given I enter "Johann Sebastian Bach" for "edit-submitted-name"
  And I enter "johann@bach.com" for "edit-submitted-email"
  And I enter "111-222-3333" for "edit-submitted-phone"
  And I select "Store Feedback" from "edit-submitted-step1-choose-topic" in the "content" region
  # Roaring Fork
  And I select "Colorado" from "edit-submitted-step2-store-wrap-state-select" in the "content" region
  And I select "43216" from "edit-submitted-step2-store-wrap-store-select" in the "content" region
  And I enter "Test message from automated testing, please disregard" for "edit-submitted-step3-body"
  Then I should see the text "CAPTCHA"
  And I should see the text "This question is for testing whether or not you are a human visitor and to prevent automated spam submissions."
  And I press the "Send comment" button
  And I wait for "1000" milliseconds
  # It will fail captcha
  Then I should see the text "The answer you entered for the CAPTCHA was not correct."


#Note this test will need to be modified  once we figure out a workaround for the CAPTCHA test
@customer-service @unauth @javascript
Scenario: Customer service form submits correctly for Company feedback
  Given I enter "Johann Sebastian Bach" for "edit-submitted-name"
  And I enter "johann@bach.com" for "edit-submitted-email"
  And I enter "111-222-3333" for "edit-submitted-phone"
  And I select "Company Feedback" from "edit-submitted-step1-choose-topic" in the "content" region
  # Roaring Fork
  And I select "Colorado" from "edit-submitted-step2-store-wrap-state-select" in the "content" region
  And I select "43216" from "edit-submitted-step2-store-wrap-store-select" in the "content" region
  And I enter "Test message from automated testing, please disregard" for "edit-submitted-step3-body"
  Then I should see the text "CAPTCHA"
  And I should see the text "This question is for testing whether or not you are a human visitor and to prevent automated spam submissions."
  And I press the "Send comment" button
  And I wait for "1000" milliseconds
  # It will fail captcha
  Then I should see the text "The answer you entered for the CAPTCHA was not correct."
  #  Then I should see "Thank you, your submission has been received."