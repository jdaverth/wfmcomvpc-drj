Feature: eStore Special Item/Item Combo Checkout
Weighted items (require in-store payment)
Items that have drop down menus that would change the price of the item when selected
Multiple items with (1, 2, 3, or 4) drop down menus/items with checkboxes
Minimum Quantity Order Item 
Items with constraints to the pick up day/time window (including those that would require separate carts)
Items with discounts when in certain combinations (like the buy six wines get 10% off or something like that)
Alcoholic items (require in-store payment)


Background:
  Given I am on "/shop/xox"
  Given That the Window is Full Screen
  Given I am not on production

@estore @javascript @checkout
Scenario: Negative check out with a standard item in the cart
  Then I click "VW-1"
  #Quantity test -- doesn't go below one
  Then I should see "1" in the item quantity
  Then I increase the item quantity
  Then I should see "2" in the item quantity
  Then I increase the item quantity
  Then I should see "3" in the item quantity
  Then I decrease the item quantity
  Then I should see "2" in the item quantity
  Then I decrease the item quantity
  Then I should see "1" in the item quantity
  Then I decrease the item quantity
  Then I should see "1" in the item quantity
  Then I decrease the item quantity
  Then I should see "1" in the item quantity
  #Quantity test -- manual input
  Given I set the item quantity to "10"
  Then I should see "10" in the item quantity
  Given I set the item quantity to "1000"
  Then I should see "1000" in the item quantity
  Given I set the item quantity to "-2"
  Then I should see "12" in the item quantity
  Given I set the item quantity to "0"
  Then I should see "1" in the item quantity
  Then I should see "1" in the item quantity
  Given I set the item quantity to "-3000000"
  Then I should see "1" in the item quantity
  Given I set the item quantity to "100000000"
  Then I should see "1" in the item quantity
  Then I set the item quantity to "3000"
  Then I should see "1" in the item quantity
  Given I set the item quantity to "999"
  Then I should see "999" in the item quantity
  Then I decrease the item quantity
  Then I should see "998" in the item quantity
  Then I increase the item quantity
  Then I should see "999" in the item quantity
  Then I increase the item quantity
  Then I should see "1000" in the item quantity
  Then I increase the item quantity
  Then I should see "1000" in the item quantity
  Then I increase the item quantity
  Then I should see "1000" in the item quantity

  Then I set the item quantity to "10"
  Then I add the item to the cart
  Then I click "Checkout"
  Then I should be on "/shop/xox/cart"
  #Then I should see "that random number that was generated in the random function" in the item quantity
  #price x quantity = subtotal
  Then I check the item subtotal
  Then I increase the item quantity
  Then I check the item subtotal
  Then I increase the item quantity
  Then I check the item subtotal
  #Decrease in /cart is not currently working.
  #Input/set item quantity in /cart is not currently working.
  #So here, again, we'd want to test the minimum and maximum and non-valid entries (like on the item page)

  Then I click "Proceed to Checkout"
  Then I should be on the checkout page
  Then I press "REVIEW ORDER"
  Then I should see the text "Uh oh, looks like we're missing some information."
  Then I press "OK"

  Then I fill in "email" with "123"
  Then I should see the text "This is not a valid email."
  Then I fill in "email" with "wfm.estore.test@gmail.com"

  #NOW FUNCTIONAL
  Then I fill in "firstname" with "Fname"
  Then I fill in "lastname" with "Lname"
  Then I fill in "street1" with "123 Main St."
  Then I fill in "city" with "Cincinnati"
  Then I select "Ohio" from "state"
  #The next two steps are intentionally duplicated (because masking often ends up overwriting the inputed value)
  Then I fill in "zip" with "45209"
  Then I fill in "phone1" with "5120000000"
  #Given I wait for "3000" milliseconds
  Then I press "REVIEW ORDER"
  Given I wait for "50000" milliseconds
  Then I press "PLACE ORDER"
  Given I wait for "10000" milliseconds

  #On Order Confirmation Page:
  Then I check the order confirmation number


@estore @javascript @checkout @nownow
Scenario: Checking out with a single weighted item in the cart and then weighted and standard
#Purpose: Does not request billing information (pay in store)
  Then I click "Catalog A"
  Then I click "VW-1"
  Then I set the item quantity to "12"
  Then I add the item to the cart
  Then I click "Checkout"
  Then I should be on "/shop/xox/cart"
  Then I check the item subtotal
  Then I click "Proceed to Checkout"
  #Then I click "CONTINUE"
  Then I should be on the checkout page
  Then I should not see the billing information fields
  Then I fill in "email" with "wfm.estore.test@gmail.com"
  Then I fill in "firstname" with "Fname"
  Then I fill in "lastname" with "Lname"
  Then I fill in "street1" with "123 Main St."
  Then I fill in "city" with "Cincinnati"
  Then I select "Ohio" from "state"
  #The next two steps are intentionally duplicated (because masking often ends up overwriting the inputed value)
  Then I fill in "zip" with "45209"
  Then I fill in "phone1" with "5120000000"
  #Now add standard item to cart and make sure it's still pay in store
  Given I am on "/shop/xox"
  Then I click "MF-1"
  Then I set the item quantity to "10"
  Then I add the item to the cart
  Then I click "Checkout"
  Then I should be on "/shop/xox/cart"
  Then I click "Proceed to Checkout"
  Then I should be on the checkout page
  Then I should not see the billing information fields
  Then I fill in "email" with "wfm.estore.test@gmail.com"
  Then I fill in "firstname" with "Fname"
  Then I fill in "lastname" with "Lname"
  Then I fill in "street1" with "123 Main St."
  Then I fill in "city" with "Cincinnati"
  Then I select "Ohio" from "state"
  #The next two steps are intentionally duplicated (because masking often ends up overwriting the inputed value)
  Then I fill in "zip" with "45209"
  Then I fill in "phone1" with "5120000000"
  Given I am on "shop/xox/cart"
  Then I wait for "2000" milliseconds
  #Now remove weighted item from cart and make sure online billing offered
  And I click on the element with css selector ".remove"
  Given I wait for "5000" milliseconds
  Then I click "Proceed to Checkout"
 # Then I click "CONTINUE"
  Then I should be on the checkout page
  Then I fill in "email" with "wfm.estore.test@gmail.com"
  Then I switch to iframe
  Then I fill in "accountNumber" with "5444009999222205"
  Then I fill in "expMonth" with "12"
  Then I fill in "expYear" with "2049"
  Then I fill in "cvv" with "222"
  Then I switch to window ""
  Then I wait for "5" seconds
  Then I fill in "billing_firstname" with "Fname"
  Then I fill in "lastname" with "Lname"
  Then I fill in "street1" with "123 Main St."
  Then I fill in "city" with "Cincinnati"
  Then I select "Ohio" from "state"
  #The next two steps are intentionally duplicated (because masking often ends up overwriting the inputed value)
  Then I fill in "zip" with "45209"
  Then I fill in "zip" with "45209"
  Then I fill in "phone1" with "5120000000"


@estore @javascript @checkout @nownownow
Scenario: Checking out with an item with drop down menu options that change the price & Minimum Quantity Ordering
#Purpose: Check that selecting a price changing option updates the item price on the item page and in the cart (also that subtotal scales with quantity and new price, not standard price)
  Then I click "MF-1"
  Then I check the item price
  Then I set the item quantity to "12"

@estore @javascript @checkout
Scenario: Checking out with an item that requires a minimum quantity order
#Purpose: Item will either not let you add item to cart without minimum quantity or cart will not let you check out without minimum quantity?

@estore @javascript @checkout
Scenario: Checking out with an item that has a discount
#Purpose: Item discount will be calculated in cart

@estore @javascript @checkout
Scenario: Checking out with any item(s) and then entering a discount code
#Purpose: All items included in the discount will be discounted accordingly

@estore @javascript @checkout
Scenario: Checking out with a sale item in the cart
#Purpose: Make sure the sale item continuously displays and calculates the correct sale price throughout the transaction

@estore @javascript @checkout
Scenario: Checking out with a standard item in the cart and selecting delivery
#Purpose: When an item has options between delivery and pick up, users can select one or the other.

@estore @javascript @checkout
Scenario: Checking out with a standard item in the cart and selecting pick up
#Purpose: When an item has optioins between delivery and pick up, users can select one or the other.

@estore @javascript @checkout
Scenario: Applying a discount to items that accept that discount
#Purpose: Make sure discounts are working for items that are valid for that discount.

@estore @javascript @checkout
Scenario: Applying a discount to items that are not valid for that discount 
#Purpose: Make sure the cart will reject that code

