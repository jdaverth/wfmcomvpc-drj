Feature: eStore Store Selection
To verify that a user without any store selected can pick a store location & address and clicks continue, can navigate to the stores TLC shopping page
To verify that a user with a store selected, can navigate to the store selected
To verify that a user with a store selected, can go to store selected from drop down menus where the data was prefilled with the store selected as their "home" store

Background:
  Given That the Window is Full Screen

@estore @unauth @javascript
Scenario: With no store selected, I navigate to /online-ordering
  #Kitsilano
  Given I have no store selected
  Given I am on "/online-ordering"
  Then I click "Select your Store"
  Then I wait for "1000" milliseconds
  Then I should be on "/shop/choose"
  Then I select "British Columbia" from "state"
  Then I select "Kitsilano - 2285 West 4th Avenue" from "store"
  Then I press "Continue"
  Then I wait for "1000" milliseconds
  Then I should be on "/shop/KIT"
  #Stoke Newington
  Given I have no store selected
  Given I am on "/online-ordering"
  Then I click "Select your Store"
  Then I wait for "1000" milliseconds
  Then I should be on "/shop/choose"
  Then I select "United Kingdom" from "state"
  Then I select "Stoke Newington - 32-40 Stoke Newington Church St" from "store"
  Then I press "Continue"
  Then I wait for "1000" milliseconds
  Then I should be on "/shop/STK/closed"
  #Beverly Hills (currently open)
  Given I have no store selected
  Given I am on "/online-ordering"
  Then I click "Select your Store"
  Then I wait for "1000" milliseconds
  Then I should be on "/shop/choose"
  Then I select "California" from "state"
  Then I select "Beverly Hills - 239 North Crescent Dr" from "store"
  Then I press "Continue"
  Then I wait for "1000" milliseconds
  Then I should be on "/shop/BVH"
  #Ocean Avenue
  Given I have no store selected
  Given I am on "/online-ordering"
  Then I click "Select your Store"
  Then I should be on "/shop/choose"
  Then I select "California" from "state"
  Then I select "Ocean - 1150 Ocean Ave" from "store"
  Then I press "Continue"
  Then I wait for "1000" milliseconds
  Then I should be on "/shop/OCN"

@estore @unauth @javascript
Scenario: With a store selected, go to the /online-ordering page
  #Kitsilano
  Given I have no store selected
  Given I am on "/online-ordering"
  Given "Kitsilano" is my home store
  Then I click "Shop Your Store"
  Then I wait for "1000" milliseconds
  Then I should be on "/shop/KIT"
  #Stoke Newington
  Given I have no store selected
  Given I am on "/online-ordering"
  Given "Stoke Newington" is my home store
  Then I click "Shop Your Store"
  Then I wait for "1000" milliseconds
  Then I should be on "/shop/STK/closed"
  #Beverly Hills (currently open)
  Given I have no store selected
  Given I am on "/online-ordering"
  Given "Beverly Hills" is my home store
  Then I click "Shop Your Store"
  Then I wait for "1000" milliseconds
  Then I should be on "/shop/BVH"
  #Berkeley
  Given I have no store selected
  Given I am on "/online-ordering"
  Given "Berkeley" is my home store
  Then I click "Shop Your Store"
  Then I wait for "1000" milliseconds
  Then I should be on "/shop/BRK"
