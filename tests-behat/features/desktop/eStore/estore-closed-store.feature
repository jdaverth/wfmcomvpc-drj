Feature: eStore Closed Store
To verify that a closed store page loads correctly
To verify that the footer loads correctly
To verify that the side menu links load correctly
As an unauthenticated user

@estore @unauth @javascript
Scenario: Check left side menu of an eStore Closed page
  Given I am on "/shop/MIA/closed"
  Then I should see the text "Whole Foods Market Downtown Miami" in the "#store_header" container
  Then I should see at least "2" ".panel-pane" elements in the ".nav.tablet_only" container
  Then I should see the link "Allegro Coffee" with href "https://www.allegrocoffee.com" in the ".nav.tablet_only" container
  Then I should see the link "Gift Cards" with href "/buy-gift-cards" in the ".nav.tablet_only" container

@estore @unauth @javascript @estore-footer @refactor
Scenario: Check that the links in the footer all point to the right URLs -- Social Media Links
  Given I am on "/shop/MIA/closed"
  Then I should see a link with href "http://www.wholefoodsmarket.com/" in the "#region-footer" container
  Then I should see a link with href "https://www.facebook.com/wholefoods" in the "#region-footer" container
  Then I should see a link with href "https://plus.google.com/+wholefoods/posts" in the "#region-footer" container
  Then I should see a link with href "https://twitter.com/wholefoods" in the "#region-footer" container
  Then I should see a link with href "https://www.youtube.com/wholefoods" in the "#region-footer" container
  Then I should see a link with href "http://www.pinterest.com/wholefoods/" in the "#region-footer" container
  Then I should see a link with href "http://instagram.com/wholefoodsmarket" in the "#region-footer" container

@estore @unauth @javascript @estore-footer @refactor
Scenario: Check that the links in the footer all point to the right URLs -- Nav Links
  Given I am on "/shop/MIA/closed"
  Then I should see a link "Healthy Eating" with href "/healthy-eating" in the "#region-footer" container
  Then I should see a link "About our Products" with href "/about-our-products" in the "#region-footer" container
  Then I should see a link "Missions & Values" with href "/mission-values" in the "#region-footer" container
  Then I should see a link "Recipes" with href "/recipes" in the "#region-footer" container
  Then I should see a link "Blog" with href "/blog/whole-story" in the "#region-footer" container
  Then I should see a link "Store Locations" with href "/stores/list" in the "#region-footer" container
  Then I should see a link "Customer Service" with href "/customer-service" in the "#region-footer" container
  Then I should see a link "Careers" with href "/careers" in the "#region-footer" container
  Then I should see a link "Store Departments" with href "/store-departments" in the "#region-footer" container

@estore @unauth @javascript @estore-footer @refactor
Scenario: Check that the links in the footer all point to the right URLs -- Disclosure Links
  Given I am on "/shop/MIA/closed"
  Then I should see a link "PDF" with href "/sites/default/files/media/California_Transparency_in_Supply_Chains_Act_Disclosure.pdf" in the ".disclosure" container
  Then I should see a link "Terms of Use" with href "/terms-use" in the ".disclosure" container
  Then I should see a link "Privacy Policy" with href "/privacy-policy" in the ".disclosure" container
  Then I should see a link "Site Map" with href "/site-map" in the ".disclosure" container
