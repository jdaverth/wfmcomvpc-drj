Feature: eStore Open Store
To verify that an open store page loads correctly
To verify that there are items in the store
To verify that the side menu category links load correctly and with items
As an unauthenticated user

@estore @unauth @javascript
Scenario: Check left side menu of an eStore Open page
  Given I am on "/shop/OCN"
  Then I should see the text "Whole Foods Market Ocean" in the "#store_header" container
  Then I should see at least "3" ".panel-pane" elements in the ".nav" container
  Then I should see the link "Parties & Catering" with href "/shop/OCN/1989" in the ".nav" container
  Then I should see the link "Allegro Coffee" with href "https://www.allegrocoffee.com" in the ".nav" container
  Then I should see the link "Gift Cards" with href "/buy-gift-cards" in the ".nav" container

@estore @unauth @javascript @item
Scenario: I click a given item on the page and add it to my cart
  Given I am on "/shop/OCN"
  When I click the "Parties & Catering" link in the ".nav" container
  Then I should be on "/shop/OCN/1989"
  When I click the "365 Every Day Value Sport Cap Spring Water (Case)" link in the ".body_inner" container
  Then I should be on "/shop/OCN/1989/18442"
  When I click on the element with css selector ".addToCart"
  Given I wait for "5000" milliseconds
  Then I should see the link "Continue shopping" with href "/shop/OCN/1989" in the ".ng-scope" container
  Then I should see the link "Checkout" with href "/shop/OCN/cart" in the ".ng-scope" container
  Then I click the "Checkout" link in the ".ng-scope" container
  Then I should be on "/shop/OCN/cart"

#This requires a standard catalog, will complete when eStore team has set up /xox store with a standard catalog
@estore @unauth @javascript @cart @disabled
Scenario: Then I should see that given item in my cart -- Should not run without above scenario running first
  Given I am on "/shop/OCN/cart"
  Given I wait for "2000" milliseconds
  #Then the ".ng-scope" element should contain at least "1" ".cart_item" elements
  #Then I should see the link "Bloom Candle Tin" with href "/shop/OCN/2543/17322" in the ".ng-scope" container


@estore @unauth @javascript @estore-footer @refactor
Scenario: Check that the links in the footer all point to the right URLs -- Social Media Links
  Then I should see a link with href "http://www.wholefoodsmarket.com/" in the "#region-footer" container
  Then I should see a link with href "https://www.facebook.com/wholefoods" in the "#region-footer" container
  Then I should see a link with href "https://plus.google.com/+wholefoods/posts" in the "#region-footer" container
  Then I should see a link with href "https://twitter.com/wholefoods" in the "#region-footer" container
  Then I should see a link with href "https://www.youtube.com/wholefoods" in the "#region-footer" container
  Then I should see a link with href "http://www.pinterest.com/wholefoods/" in the "#region-footer" container
  Then I should see a link with href "http://instagram.com/wholefoodsmarket" in the "#region-footer" container

@estore @unauth @javascript @estore-footer @refactor
Scenario: Check that the links in the footer all point to the right URLs -- Nav Links
  Then I should see a link "Healthy Eating" with href "/healthy-eating" in the "#region-footer" container
  Then I should see a link "About our Products" with href "/about-our-products" in the "#region-footer" container
  Then I should see a link "Missions & Values" with href "/mission-values" in the "#region-footer" container
  Then I should see a link "Recipes" with href "/recipes" in the "#region-footer" container
  Then I should see a link "Blog" with href "/blog/whole-story" in the "#region-footer" container
  Then I should see a link "Store Locations" with href "/stores/list" in the "#region-footer" container
  Then I should see a link "Customer Service" with href "/customer-service" in the "#region-footer" container
  Then I should see a link "Careers" with href "/careers" in the "#region-footer" container
  Then I should see a link "Store Departments" with href "/store-departments" in the "#region-footer" container

@estore @unauth @javascript @estore-footer @refactor
Scenario: Check that the links in the footer all point to the right URLs -- Disclosure Links
  Then I should see a link "PDF" with href "/sites/default/files/media/California_Transparency_in_Supply_Chains_Act_Disclosure.pdf" in the ".disclosure" container
  Then I should see a link "Terms of Use" with href "/terms-use" in the ".disclosure" container
  Then I should see a link "Privacy Policy" with href "/privacy-policy" in the ".disclosure" container
  Then I should see a link "Site Map" with href "/site-map" in the ".disclosure" container
