#Feature: eStore functional checkout
#  I am a WFM customer buying from my local eStore.
#  To verify that process from item selection to purchase confirmation works for USA
#  To verify that process from item selection to purchase confirmation works for Canada
#  To verify that process from item selection to purchase confirmation works for UK
#
#  Note: If the purchases have both checkout options (pickup or delivery) then both options can be executed.
#        However if no option exists, then the test will apply whichever checkout is available
#  ---------------------------------------------------------------------------------------------------------
#  #  DOTS-248 SCENARIO 1: Functional Test of USA eStore checkout
#  #  1. Navigate to the given open store.
#  #  2. Ensure it is an open store.
#  #  3. Add two products to the cart
#  #  4. Proceed to checkout billing
#  #  5. Submit valid billing information for whichever checkout option is present
#  #  6. On the Confirm page verify the Billing/Pickup/Delivery info as well as the cart items and totals
#  @estore @api @javascript
#  Scenario Outline: Verify USA checkout process up to the Confirm page
#    Given That the Window is Full Screen
#    And I am on "<eStore_USA>"
#    And eStore "<eStore_USA>" is Open
#    Then I memorize the eStore address for later
#    When I add the "<product_A_index>" item to the cart.
#    And I continue eStore shoppping
#    And I add the "<product_B_index>" item to the cart.
#    And I proceed to eStore checkout review
#    Then I should be on the checkout Review page
#    And I memorize the eStore cart for later
#    When I proceed to eStore checkout billing for "<option>"
#    Then I should be on the checkout page
#    When I enter valid USA checkout inputs
#    And I submit the checkout data as OK
#    Then I should be on the checkout Confirm page
#    And I verify the checkout Confirm page
#
#    Examples:
#      | eStore_USA | product_A_index | product_B_index | option   |
#      | /shop/LAJ  | 2               | 1               | DELIVERY |
#      | /shop/LAJ  | 2               | 1               | PICKUP   |
#
#
#  #  DOTS-248 SCENARIO 2: Functional Test of Canadaa eStore checkout
#  #  1. Navigate to the given open store.
#  #  2. Ensure it is an open store.
#  #  3. Add two products to the cart
#  #  4. Proceed to checkout billing
#  #  5. Submit valid billing information for whichever checkout option is present
#  #  6. On the Confirm page verify the Billing/Pickup/Delivery info as well as the cart items and totals
#  @estore @api @javascript
#  Scenario Outline: Verify Canada checkout process up to the Confirm page
#    Given That the Window is Full Screen
#     And I am on "<eStore_Canada>"
#     And eStore "<eStore_Canada>" is Open
#    Then I memorize the eStore address for later
#    When I add the "<product_A_index>" item to the cart.
#     And I continue eStore shoppping
#     And I add the "<product_B_index>" item to the cart.
#     And I proceed to eStore checkout review
#    Then I should be on the checkout Review page
#     And I memorize the eStore cart for later
#    When I proceed to eStore checkout billing for "<option>"
#    Then I should be on the checkout page
#    When I enter valid Canada checkout inputs
#     And I submit the checkout data as OK
#    Then I should be on the checkout Confirm page
#     And I verify the checkout Confirm page
#
#    Examples:
#      | eStore_Canada | product_A_index | product_B_index | option |
#      | /shop/MKM     | 2               | 1               | PICKUP |
#
#
#  #  DOTS-248 SCENARIO 3: Functional Test of UK eStore checkout
#  #  1. Navigate to the given open store.
#  #  2. Ensure it is an open store.
#  #  3. Add two products to the cart
#  #  4. Proceed to checkout billing
#  #  5. Submit valid billing information for whichever checkout option is present
#  #  6. On the Confirm page verify the Billing/Pickup/Delivery info as well as the cart items and totals
#  @estore @api @javascript
#  Scenario Outline: Verify UK checkout process up to the Confirm page
#    Given That the Window is Full Screen
#    And I am on "<eStore_UK>"
#    And eStore "<eStore_UK>" is Open
#    Then I memorize the eStore address for later
#    When I add the "<product_A_index>" item to the cart.
#    And I continue eStore shoppping
#    And I add the "<product_B_index>" item to the cart.
#    And I proceed to eStore checkout review
#    Then I should be on the checkout Review page
#    And I memorize the eStore cart for later
#    When I proceed to eStore checkout billing for "<option>"
#    Then I should be on the checkout page
#    When I enter valid UK checkout inputs
#    And I submit the checkout data as OK
#    Then I should be on the checkout Confirm page
#    #And I break here
#    And I verify the checkout Confirm page
#
#    Examples:
#      | eStore_UK  | product_A_index | product_B_index | option   |
#      | /shop/PCD  | 2               | 3               | PICKUP   |
#      | /shop/HSK  | 2               | 3               | DELIVERY |
#
