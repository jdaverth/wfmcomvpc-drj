Feature: eStore Item Quantity Check
To verify that item quantity increase and decrease work on item page.
To verify that item quantity direct field input works on item page.
To verify that item quantity transfers into cart when item is added to cart.
To verify that item quantity increase and decrease work in cart and update price.
To verify that item quantity direct field input works in cart and updates price.
As an unauthenticated user
Visiting a standard US store, using a standard item.

#####- ON ITEM PAGE -#####
@estore @unauth @javascript @item @quantity
Scenario: Item quantity increase/decrease (+/-) on item page - minimum
  Given I am on "/shop/LAJ"
  When I select the second item in the store
  Then I should see "1" in the item quantity
  Then I increase the item quantity
  Then I should see "2" in the item quantity
  Then I increase the item quantity
  Then I should see "3" in the item quantity
  Then I decrease the item quantity
  Then I should see "2" in the item quantity
  Then I decrease the item quantity
  Then I should see "1" in the item quantity
  Then I decrease the item quantity
  Then I should see "1" in the item quantity
  Then I decrease the item quantity
  Then I should see "1" in the item quantity

@estore @unauth @javascript @item @quantity
Scenario: Item quantity manual input - functional & negative testing - on item page
  Given I am on "/shop/LAJ"
  When I select the second item in the store

  #Note. don't change the order of these.
  Given I set the item quantity to "\\asdfasdf"
  And I wait for "1000" milliseconds
  Then I should see "1" in the item quantity

  Given I set the item quantity to "10"
  Then I should see "10" in the item quantity
  Given I set the item quantity to "1000"
  Then I should see "1000" in the item quantity
  Given I set the item quantity to "-2"
  Then I should see "12" in the item quantity
  Given I set the item quantity to "0"
  Then I should see "1" in the item quantity

  Given I set the item quantity to "-3000000"
  Then I should see "1" in the item quantity
  Given I set the item quantity to "100000000"
  Then I should see "1" in the item quantity
  Then I set the item quantity to "3000"
  Then I should see "1" in the item quantity

@estore @unauth @javascript @item @quantity
Scenario: Item quantity increase/decrease (+/-) on item page - maximum
  Given I am on "/shop/LAJ"
  When I select the second item in the store
  Given I set the item quantity to "999"
  Then I should see "999" in the item quantity
  Then I decrease the item quantity
  Then I should see "998" in the item quantity
  Then I increase the item quantity
  Then I should see "999" in the item quantity
  Then I increase the item quantity
  Then I should see "1000" in the item quantity
  Then I increase the item quantity
  Then I should see "1000" in the item quantity
  Then I increase the item quantity
  Then I should see "1000" in the item quantity

#####- IN CART -#####
@estore @unauth @javascript @item @quantity
Scenario: Item quantity increase/decrease (+/-) in cart
  Given I am on "/shop/LAJ"
  When I select the second item in the store
  Given I set the item quantity to "10"
  Then I should see "10" in the item quantity
#!!! This only works if there are no drop down menus! Will need a standard item without drop down menus to test this way
  Then I add the item to the cart
  Then I click "Checkout"
  Then I should be on "/shop/LAJ/cart"
  Then I should see "10" in the item quantity
  #price x quantity = subtotal
  Then I check the item subtotal
  Then I increase the item quantity
  Then I check the item subtotal
  Then I increase the item quantity
  Then I check the item subtotal
  #Decrease in /cart is not currently working.
  #Input/set item quantity in /cart is not currently working.
  #So here, again, we'd want to test the minimum and maximum and non-valid entries (like on the item page)
