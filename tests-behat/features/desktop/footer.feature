Feature: Footer
To verify the footer loads
As an unauthenticated user
I need to see the headings for each footer section
I need to see the links for each footer section
I need to see the link urls are correct

@footer @unauth @nojs @quick
Scenario: Footer Loads Correctly
  Given I am on homepage
  Then I should see "3" "div.footer-column" elements in the "#footer" container
  #Shopping
  Then I should see the text "Shopping" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Find a Store" with href "/stores/list" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Coupons" with href "/coupons" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Sales Flyers" with href "/sales-flyer" in count "1" of "div.footer-column" element in the "#footer" container
  #Then I should see the link "The Whole Deal®" with href "/about-our-products/whole-deal" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Catering & Online Ordering" with href "/online-ordering" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Gift Cards" with href "/buy-gift-cards" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Store Departments" with href "/store-departments" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Store Events" with href "/events" in count "1" of "div.footer-column" element in the "#footer" container
  #Company
  Then I should see the text "Company" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "About Whole Foods" with href "/company-info" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Careers" with href "/find-and-apply-jobs" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Mission & Values" with href "/mission-values" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Investor Relations" with href "/investor-relations" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Newsroom" with href "http://media.wholefoodsmarket.com" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Customer Service" with href "/customer-service" in count "1" of "div.footer-column" element in the "#footer" container
  #Eating & Cooking
  Then I should see the text "Eating & Cooking" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Healthy Eating" with href "/healthy-eating" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Recipe Collections" with href "/recipe-collections" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Special Diets" with href "/healthy-eating/special-diets" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Parents & Kids" with href "/healthy-eating/kid-friendly" in count "2" of "div.footer-column" element in the "#footer" container
  #About Our Products
  Then I should see the text "About Our Products" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Quality Standards" with href "/about-our-products/quality-standards" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Food Safety" with href "/about-our-products/food-safety" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Organic Farming" with href "/about-organic-farming" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "GMOs: Your Right to Know" with href "/gmo-your-right-know" in count "2" of "div.footer-column" element in the "#footer" container
  #More from whole Food Market
  Then I should see the text "More from Whole Foods Market" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Whole Planet Foundation" with href "http://www.wholeplanetfoundation.org" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Whole Kids Foundation" with href "http://www.wholekidsfoundation.org" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Whole Cities Foundation" with href "http://www.wholecitiesfoundation.org" in count "2" of "div.footer-column" element in the "#footer" container

  #Connect with Us
  Then I should see the text "Connect with Us" in count "3" of "div.footer-column" element in the "#footer" container
  Then I should see the href "https://www.facebook.com/wholefoods" in count "3" of "div.footer-column" element in the "#footer" container
  Then I should see the href "https://twitter.com/wholefoods/" in count "3" of "div.footer-column" element in the "#footer" container
  Then I should see the href "https://pinterest.com/wholefoods/" in count "3" of "div.footer-column" element in the "#footer" container
  Then I should see the href "https://plus.google.com/108900531664537360582/posts" in count "3" of "div.footer-column" element in the "#footer" container
  Then I should see the href "https://instagram.com/wholefoods" in count "3" of "div.footer-column" element in the "#footer" container
  Then I should see the href "https://www.youtube.com/wholefoods" in count "3" of "div.footer-column" element in the "#footer" container
  #Newsletter Subscription
  Then I should see the text "Newsletter Subscription" in count "3" of "div.footer-column" element in the "#footer" container

  @footer @unauth @javascript @quick
  Scenario: Select A Store Function in Footer Works
    #Select a Store
  Given I am on homepage
  Then I should see the text "Select a Store" in count "3" of "div.footer-column" element in the "#footer" container
  #Then I should see a "disabled" element "#store-select-form" in the "footer" region
  When I select "Texas" from "edit-state" in the "footer" region
  When I additionally select "Lamar" from "edit-store" in the "footer" region
  Then I should see a "enabled" element "#store-select-submit" in the "footer" region
