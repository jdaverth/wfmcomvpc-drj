Feature: Store Locator Page 
  In order to assure basic functionality of the locator page
  As an unauthenticated user 
  I need to be able to find stores based on postal code, or city and state/province
  And select a store as "my store"
  
Background:
  Given I am on "/stores/list"   

@locator @nojs @quick
Scenario: Check that the basic sections of the page are there 
  Then I should see the heading "Select a Store"
  And I should see an "#mapCanvas" element

@locator @nojs @quick
Scenario: List all stores in Canada 
  When I click "Canada Stores"
  And I wait for "1" seconds
  Then I should be on "/stores/list/canada"
  And I should see an "#block-views-store-locations-by-state-canada .view-content" element
  And I should see at least "6" "#block-views-store-locations-by-state-canada .views-row"

@locator @nojs @quick
Scenario: List all stores in UK
  When I click "United Kingdom Stores"
  And I wait for "1" seconds
  Then I should be on "/stores/list/uk"
  And I should see an "#block-views-store-locations-by-state-uk .view-content" element
  And I should see at least "5" "#block-views-store-locations-by-state-uk .views-row"

@locator @nojs @quick
Scenario: List all stores in development
  When I click "Stores In Development"
  And I wait for "1" seconds
  Then I should be on "/stores/list/development"
  And I should see an ".view-id-store_locations_by_state .view-content" element
  And I should see at least "1" ".view-id-store_locations_by_state .views-row"
  And I should see the text "Coming Soon"
  And I should not see the text "Make This My Store"

@locator @javascript @quick
Scenario: List stores by state
  When I click "Stores By US State"
  And I wait for "1" seconds
  Then I should be on "/stores/list/state"
  And I should see an ".view-id-store_locations_by_state .view-content" element
  And there should be at least "6" ".views-row" elements with display "block" in the "content" region
  And there should be at least "10" ".views-row" elements with display "none" in the "content" region
  When I click ".show_more_stores" via Javascript
  # this pager is always there - just hidden
  And I should see an ".view-id-store_locations_by_state .pager" element
  And I should see at least "10" ".views-field-field-storefront-image"
  #after they click the show more stores, all the view-row that were display:none will change to display:block
  And there should be at least "20" ".views-row" elements with display "block" in the "content" region

#365 by Whole Foods Market Stores
@locator @javascript @quick
  Scenario: List stores by state
    When I click "365 by Whole Foods Market™ Stores"
    And I wait for "1" seconds
    Then I should be on "/stores/list/365"
    And I should see an ".view-display-id-block_365_stores .view-content" element
    And there should be at least "3" ".views-row" elements with display "block" in the "content" region
