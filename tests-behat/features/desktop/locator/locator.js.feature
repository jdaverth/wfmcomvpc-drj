Feature: Store Locator Page 
  In order to assure basic functionality of the locator page
  As an unauthenticated user 
  I need to be able to find stores based on postal code, or city and state/province
  And select a store as "my store"
  
Background:
  Given That the Window is Full Screen
  Given I am on "/stores/list"

@locator @javascript @search
Scenario: Check the form submission button is visible 
  Then "#edit-submit-store-lookup-by-address" should be visible

@locator @javascript @search
Scenario: Locator search using postal code in united states 
  Given That the Window is Full Screen
  Given I am on "/stores/list"
  Then I perform a "non-paginated" "store" search for "78701"
  Then I should see "525 N Lamar Blvd." in the store results

@locator @javascript @search
Scenario: Locator search using city and state in United States
  Given That the Window is Full Screen
  Given I am on "/stores/list"
  Then I perform a "non-paginated" "store" search for "Austin, TX"
  Then I should see "525 N Lamar Blvd." in the store results
  Then I should see "4301 W. William Cannon" in the store results
  Then I should see "11920 Domain Drive" in the store results

@locator @javascript @search @disabled
Scenario: Locator search entering junk as query
  Given for "edit-field-geo-data-latlon-address" I enter "foobar"
  When I press "edit-submit-store-lookup-by-address"
  #And I should see the text "No results found. Please enter postal code, or city and state/province in the search box above." in the ".view-store-lookup-by-address .view-empty" container in the "content" region
  #NO ERROR MESSAGES FOR STORE SEARCH, it'll just show the last set of stores you looked up

@locator @javascript @search
Scenario: Locator search using a Canadian postal code
  Given That the Window is Full Screen
  Given I am on "/stores/list"
  Then I perform a "non-paginated" "store" search for "V5Z 1C5"
  Then I should see "510 West 8th Avenue" in the store results

@locator @javascript @search
Scenario: Locator search using a Canadian city and province
  Given That the Window is Full Screen
  Given I am on "/stores/list"
  Then I perform a "non-paginated" "store" search for "Vancouver BC"
  Then I should see "510 West 8th Avenue" in the store results
  Then I should see "2285 West 4th Avenue" in the store results
  Then I should see "1675 Robson Street" in the store results

@locator @javascript @search
Scenario: Locator search using postal code in UK
  Given That the Window is Full Screen
  Given I am on "/stores/list"
  Then I perform a "non-paginated" "store" search for "NW1 7PN"
  Then I should see "49 Parkway" in the store results
  Then I should see "20 Glasshouse Street" in the store results
  Then I should see "63-97 Kensington High Street" in the store results
  Then I should see "32-40 Stoke Newington Church St" in the store results
  Then I should see "2-6 Fulham Broadway" in the store results
  
@locator @javascript @search
Scenario: Locator search using city in Uk
  Given That the Window is Full Screen
  Given I am on "/stores/list"
  Then I perform a "non-paginated" "store" search for "London"
  Then I should see "49 Parkway" in the store results
  Then I should see "305-311 Lavender Hill" in the store results

@locator @javascript @search
Scenario: Check some of the pagination links of stores listed by state
  Given I am on "/stores/list"
  When I click "Stores By US State"
  Then I wait for "2" seconds
  Then I should be on "/stores/list/state"
  And I should see at least "7" ".view-id-store_locations_by_state .views-row"
  When I click "Go to page 2"
  And I wait for "1" seconds
  And I should see at least "7" ".view-id-store_locations_by_state .views-row"
  When I click "Go to page 7"
  And I wait for "1" seconds
  And I should see at least "7" ".view-id-store_locations_by_state .views-row"
  When I click "Go to page 21"
  And I wait for "5" seconds
  And I should see at least "1" ".view-id-store_locations_by_state .views-row"

@locator @javascript
Scenario: Select "Make This My Store" button, then try changing selection
  Given I select a random store
  Then I should see the selected store name in the "header" region
  Given I am on "/stores/list"
  Given I select a random store
  Then I should see the selected store name in the "header" region
 
@locator @javascript @disabled
Scenario: Go to paginated page and select store and check it stays set 
  #When I click "Load more stores"
  And I should see at least "10" ".view-store-lookup-by-address .view-content .views-row"
  Then I click a random element identified by ".torn-pod-content .views-field-title .field-content a" in the "content" region
  Then I should be on the selected page with title identified by ".torn-pod-content .store-title" in the "contentTop" region