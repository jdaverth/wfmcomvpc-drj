Feature: Store Page
  In order to assure basic functionality of the store page
  As an unauthenticated user
  I need to assure all page blocks are displayed correctly

Background:
  Given I am on "stores/lamar"

@nojs @store @quick
Scenario: I should see the basic elements of the page
  Then I should see the heading "Lamar"
  And I should see an ".store-address" element
  And I should see an ".locality-block" element
  And I should see an ".locality" element
  And I should see an ".state" element
  And I should see an ".postal-code" element
  And I should see an ".views-field-field-phone-number" element
  And I should see the link "Map & Directions"
  And I should see the link "Events"
  And I should see the link "See what's on sale"
  And I should see the link "Subscribe to our newsletter"
  And I should see the link "Facebook"
  And I should see the link "Follow us"
  And I should see the link "Flickr"
  And I should see the heading "Contact Your Local Store"
  And I should see an "#edit-submitted-subject" element
  And I should see the heading "Unique To This Store"
  And I should see the heading "Departments"
  And I should see the heading "Team Members"
  And I should see an ".view-display-id-wfm_store_teammembers" element
  And I should see an ".view-display-id-wfm_store_departments" element
  And I should see an "#store_twitter_widget" element
  And the "#store_twitter_widget" element should contain at least "3" ".tweet-content" elements
  And I should see the link "Follow @WholeFoodsATX"

@store @nojs
Scenario: Check sales flyer link
  Given I click "See what's on sale"
  And I wait for "1" seconds
  Then I should be on "/sales-flyer/lamar"

@store @nojs
Scenario: Check newsletter subscribe link
  When I click the "Subscribe to our newsletter" link in the "content" region
  And I wait for "1" seconds
  Then I should be on "/newsletters"

@store @javascript
Scenario: Check contact topic dropdown
  Given I select "Store Experience" from "edit-submitted-subject" in the "content" region
  Then I should see an "#webform-component-user-content" element
  And I should see an "#webform-component-user-content--name" element
  And I should see an "#webform-component-user-content--email" element
  And I should see an "#webform-component-user-content--address" element
  And I should see an "#webform-component-user-content--comments" element
  Given I select "Product Request" from "edit-submitted-subject"
  Then I should see an "#webform-component-user-content" element
  And I should see an "#webform-component-user-content--name" element
  And I should see an "#webform-component-user-content--email" element
  And I should see an "#webform-component-user-content--address" element
  And I should see an "#webform-component-user-content--comments" element
  Given I select "Sponsorship/Donation" from "edit-submitted-subject"
  Then I should see an "#webform-component-user-content" element
  And I should see an "#webform-component-user-content--name" element
  And I should see an "#webform-component-user-content--email" element
  And I should see an "#webform-component-user-content--address" element
  And I should see an "#webform-component-user-content--comments" element
  Given I select "Employment" from "edit-submitted-subject"
  Then I should see an "#webform-component-user-content" element
  And I should see an "#webform-component-user-content--name" element
  And I should see an "#webform-component-user-content--email" element
  And I should see an "#webform-component-user-content--address" element
  And I should see an "#webform-component-user-content--comments" element

@store @javascript
Scenario: Blank for submission
  Given I select "Store Experience" from "edit-submitted-subject" in the "content" region
  And I press "Send Comment"
  And I wait for "2" seconds
  Then I should be on "/stores/lamar"
  Then I should see the text "Name field is required"
  Then I should see the text "Email field is required"
  Then I should see the text "Your Comments field is required"

@store @javascript
Scenario: Form submission, fill in everything but comment
  Given I select "Store Experience" from "edit-submitted-subject" in the "content" region
  Given for "submitted[user_content][name]" I enter "Foo Bar"
  Given for "submitted[user_content][email]" I enter "foo@bar.com"
  And I press "Send Comment"
  Then I should see the text "Your Comments field is required"
