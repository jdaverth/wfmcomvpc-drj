Feature: Monster Navigation
  To ensure the monster navigation loads in entirety
  As an unauthenticated user
  I need to verify all menu elements load correctly

Background:
  Given I am on the homepage

##### - Healthy Eating Start - #####
@monster-nav @healthy-eating @nojs @unauth @quick @slow
Scenario Outline: Healthy Eating - Getting Started
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
  #  | 4 Ways to a Healthier You | /healthy-eating/four-pillars-healthy-eating | Getting Started | /healthy-eating/getting-started | monsterNavHealthyEating |
  #  | Health Starts Here | /health-starts-here | Getting Started | /healthy-eating/getting-started | monsterNavHealthyEating |
  #  | Simple Changes for Lifelong Health | /healthy-eating/simple-changes-lifelong-health | Getting Started | /healthy-eating/getting-started | monsterNavHealthyEating |
  #  | Resources | /healthy-eating/resources | Getting Started | /healthy-eating/getting-started | monsterNavHealthyEating |
    | Eat Real Food™ | /realfood | Getting Started | /healthy-eating/getting-started | monsterNavHealthyEating |

@monster-nav @healthy-eating @nojs @unauth @slow
Scenario Outline: Healthy Eating - What to Eat
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
  #  | Healthy Recipes | /healthy-eating/healthy-recipes | What to Eat | /healthy-eating/what-to-eat | monsterNavHealthyEating |
    | Weekly Meal Plans | /healthy-eating/meal-plans | What to Eat | /healthy-eating/what-to-eat | monsterNavHealthyEating |
    | The Engine 2® Diet | /healthy-eating/engine-2 | What to Eat | /healthy-eating/what-to-eat | monsterNavHealthyEating |

@monster-nav @healthy-eating @nojs @unauth @slow
Scenario Outline: Healthy Eating - Healthy Cooking
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Healthy Pantry | /healthy-eating/healthy-pantry | Healthy Cooking | /healthy-cooking | monsterNavHealthyEating |
    | Cooking Techniques | /healthy-eating/healthy-cooking-videos | Healthy Cooking | /healthy-cooking | monsterNavHealthyEating |
   # | Cooking with Whole Grains | /healthy-eating/cooking-whole-grains | Healthy Cooking | /healthy-cooking | monsterNavHealthyEating |
   # | Add Flavor, Naturally | /healthy-eating/add-flavor-naturally | Healthy Cooking | /healthy-cooking | monsterNavHealthyEating |
@monster-nav @healthy-eating @nojs @unauth @slow
Scenario Outline: Healthy Eating - Family and Special Diets
Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
Examples:
  | link_text | link_href | heading_text | heading_href | menu_region |
  | Cooking with Kids | /healthy-eating/healthy-eating-for-whole-family | Family and Special Diets | /healthy-eating/family-and-special-diets | monsterNavHealthyEating |
  | Diets & Lifestyle | /healthy-eating/special-diets | Family and Special Diets | /healthy-eating/family-and-special-diets | monsterNavHealthyEating |
  ### - Healthy Eating End - #####

##### - About Our Products Start - #####
@monster-nav @about-our-products @nojs @unauth @slow
Scenario Outline: About Our Products - Our Quality Standards
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Organic Food | /organic-food | Our Quality Standards | /quality-standards | monsterNavAboutOurProducts |
    | Food Ingredients | /quality-standards/food-ingredient | Our Quality Standards | /quality-standards | monsterNavAboutOurProducts |
    | Animal Welfare | /mission-values/animal-welfare/animal-welfare-basics | Our Quality Standards | /quality-standards | monsterNavAboutOurProducts |
    | Premium Body Care Standards | /premium-body-care-standards | Our Quality Standards | /quality-standards | monsterNavAboutOurProducts |
    | Fresh Produce and Flowers | /fresh-produce-and-flowers | Our Quality Standards | /quality-standards | monsterNavAboutOurProducts |
    | More | /quality-standards | Our Quality Standards | /quality-standards | monsterNavAboutOurProducts |

#@monster-nav @about-our-products @nojs @unauth @quick @slow
#Scenario Outline: About Our Products - Product Ingredient FAQs
#  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
#  Examples:
#    | link_text | link_href | heading_text | heading_href | menu_region |
#    | Product FAQs: GMOs | /about-our-products/product-faq/gmos | Product Ingredient FAQs | /product-ingredient-faq | monsterNavAboutOurProducts |
#    | Food Allergies | /about-our-products/product-faq/food-allergies | Product Ingredient FAQs | /product-ingredient-faq | monsterNavAboutOurProducts |
#    | Ingredients | /about-our-products/product-faq/ingredients | Product Ingredient FAQs | /product-ingredient-faq | monsterNavAboutOurProducts |
#    | Milk and Eggs | /about-our-products/product-faq/milk-and-eggs | Product Ingredient FAQs | /product-ingredient-faq | monsterNavAboutOurProducts |
#    | Olive Oil | /about-our-products/product-faq/olive-oil | Product Ingredient FAQs | /product-ingredient-faq | monsterNavAboutOurProducts |
#    | More | /product-ingredient-faq | Product Ingredient FAQs | /product-ingredient-faq | monsterNavAboutOurProducts |

@monster-nav @about-our-products @nojs @unauth @slow
Scenario Outline: About Our Products - Food Safety
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Cold Storage Chart | /about-our-products/food-safety/cold-storage-chart | Food Safety | /about-our-products/food-safety | monsterNavAboutOurProducts |
    | Handling Meat & Poultry | /about-our-products/food-safety/handling-meat-poultry-safely | Food Safety | /about-our-products/food-safety | monsterNavAboutOurProducts |
    | Handling Seafood | /about-our-products/food-safety/handling-seafood-safely | Food Safety | /about-our-products/food-safety | monsterNavAboutOurProducts |
    | Methyl-mercury in Seafood  | /about-our-products/food-safety/methylmercury-seafood | Food Safety | /about-our-products/food-safety | monsterNavAboutOurProducts |
    | Product Recalls  | /product-recalls | Food Safety | /about-our-products/food-safety | monsterNavAboutOurProducts |
    | More | /about-our-products/food-safety | Food Safety | /about-our-products/food-safety | monsterNavAboutOurProducts |

@monster-nav @about-our-products @nojs @unauth @slow
Scenario Outline: About Our Products -  GMOs: What You Should Know
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region 
  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | GMO Facts | /gmo-quick-facts-what-why-where | GMOs: What You Should Know | /gmo-your-right-know | monsterNavAboutOurProducts |


@monster-nav @about-our-products @nojs @slow
Scenario Outline: About Our Products -  Our Brands
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | 365 Everyday Value | /about-our-products/product-lines/365-everyday-value | Our Brands | /about-our-products/our-product-lines | monsterNavAboutOurProducts |
    | Engine 2® Plant-Strong® | /about-our-products/product-lines/engine-2 | Our Brands | /about-our-products/our-product-lines | monsterNavAboutOurProducts |
    | Whole Foods Market Brand | /about-our-products/product-lines/whole-foods-market-brand | Our Brands | /about-our-products/our-product-lines | monsterNavAboutOurProducts |
    | Whole Trade | /about-our-products/product-lines/whole-trade | Our Brands | /about-our-products/our-product-lines | monsterNavAboutOurProducts |

#@monster-nav @about-our-products @nojs @unauth @slow
#Scenario Outline: About Our Products -  The Whole Deal
#  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  #Examples:
  #  | link_text | link_href | heading_text | heading_href | menu_region |
  #  | Coupons | /coupons | The Whole Deal | /about-our-products/whole-deal | monsterNavAboutOurProducts |
  #  | Budget-Friendly Recipes | /about-our-products/whole-deal/budget-recipes | The Whole Deal | /about-our-products/whole-deal | monsterNavAboutOurProducts |
  #  | Money-Saving Tips | /about-our-products/whole-deal/money-saving-tips | The Whole Deal | /about-our-products/whole-deal | monsterNavAboutOurProducts |

@monster-nav @about-our-products @nojs @unauth @slow
Scenario Outline: About Our Products -  Locally Grown
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Local Growers Map | /locally-grown | Locally Grown | /local | monsterNavAboutOurProducts |
    | Local Vendor Profiles | /local-vendor-profiles | Locally Grown | /local | monsterNavAboutOurProducts |
##### - About Our Products End - #####

##### - Recipies Start - #####
@monster-nav @recipies @nojs @unauth @quick @slow
Scenario Outline: Recipies - Recipies
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Featured | /recipe-landing-featured | Recipes | /recipe-landing | monsterNavRecipies |
    | Newest | /recipe-landing-newest | Recipes | /recipe-landing | monsterNavRecipies |
    | Top Rated | /recipe-landing-toprated | Recipes | /recipe-landing | monsterNavRecipies |

@monster-nav @recipies @nojs @unauth @slow
Scenario Outline: Recipies - Cooking and Entertainment Guides
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Holiday Guide | /holidays | Cooking & Entertainment Guides | /cooking-entertainment-guides | monsterNavRecipies |
    | Summer Foods on the Go | /recipes/cooking-entertainment-guides/summer-foods-go | Cooking & Entertainment Guides | /cooking-entertainment-guides | monsterNavRecipies |
    | Complete Grilling Guide | /recipes/cooking-entertainment-guides/complete-grilling-guide | Cooking & Entertainment Guides | /cooking-entertainment-guides | monsterNavRecipies |
#These change seasonally

@monster-nav @recipies @nojs @unauth @quick @slow
Scenario Outline: Recipies - Food Guides
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Beans | /recipes/food-guides/beans | Food Guides | /recipes/food-guides | monsterNavRecipies |
    | Chocolate | /recipes/food-guides/chocolate | Food Guides | /recipes/food-guides | monsterNavRecipies |
    | Cooking Oils | /cooking-oils | Food Guides | /recipes/food-guides | monsterNavRecipies |
    | Dairy Products | /dairy-products | Food Guides | /recipes/food-guides | monsterNavRecipies |
    | Eggs | /recipes/food-guides/eggs | Food Guides | /recipes/food-guides | monsterNavRecipies |
    | More | /recipes/food-guides | Food Guides | /recipes/food-guides | monsterNavRecipies |

@monster-nav @recipies @nojs @unauth @quick @slow
Scenario Outline: Recipies - Recipe Collections
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Succulent Salmon Recipes | /recipes/salmon | Recipe Collections | /seasonal-recipe-collections | monsterNavRecipies |
    | Let's Go Green | /recipes/vegetable | Recipe Collections | /seasonal-recipe-collections | monsterNavRecipies |
    | Deviled Eggs | /recipes/deviled-egg | Recipe Collections | /seasonal-recipe-collections | monsterNavRecipies |
    | Our Recipe Picks for Ham| /recipes/ham | Recipe Collections | /seasonal-recipe-collections | monsterNavRecipies |
    | Our Favorite Cake Recipes | /recipes/cake | Recipe Collections | /seasonal-recipe-collections | monsterNavRecipies |
##### - Recipies End - #####

##### - Online Ordering Start - #####
#@monster-nav @online-ordering @javascript @api @unauth @no-store-selected @here
#Scenario Outline: Online Ordering - No Store
  #Given I have no store selected
  #When I reload the page
  #Given I wait for "1000" milliseconds
  #Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  #Examples:
  #  | link_text | link_href | heading_text | heading_href | menu_region |
  #  | Catering & Holiday Ordering | /online-ordering | Available At Your Store | /online-ordering-available-your-store | monsterNavOnlineOrdering |
  
@monster-nav @online-ordering @nojs @unauth @slow
Scenario Outline: Online Ordering - Nationwide
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Gift Cards | /buy-gift-cards | U.S. and Canada | /nationwide | monsterNavOnlineOrdering |
  #  | American Express Reload | /gift-cards/american-express-reload | nationwide | /nationwide | monsterNavOnlineOrdering |
    | Gift Card(s) Terms and Conditions | /gift-cards/gift-cards-terms-and-conditions | U.S. and Canada | /nationwide | monsterNavOnlineOrdering |
    | Gift Cards FAQ | /gift-cards/gift-cards-faq | U.S. and Canada | /nationwide | monsterNavOnlineOrdering |
#| Wine Club | /wine-club | Nationwide | /nationwide | monsterNavOnlineOrdering |
##### - Online Ordering End - #####

##### - Mission & Values Start - #####
@monster-nav @mission-values @nojs @unauth @slow
Scenario Outline: Mission and Values - Our Values
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    
    | Core Values | /core-values | Our Values | /our-values | monsterNavMissionValues |

#| Values Matter | /valuesmatter | Our Values | /our-values | monsterNavMissionValues |

@monster-nav @mission-values @nojs @unauth @slow
Scenario Outline: Mission and Values - Commitment to Society
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Community Giving | /mission-values/caring-communities/community-giving | Commitment to Society | /mission-values/caring-communities | monsterNavMissionValues |
    | Local Producer Loan Program | /mission-values/caring-communities/local-producer-loan-program | Commitment to Society | /mission-values/caring-communities | monsterNavMissionValues |
    | Whole Kids Foundation | /mission-values/caring-communities/whole-kids-foundation | Commitment to Society | /mission-values/caring-communities | monsterNavMissionValues |
    | Whole Planet Foundation | /mission-values/caring-communities/whole-planet-foundation | Commitment to Society | /mission-values/caring-communities | monsterNavMissionValues |
    | Whole Cities Foundation | /whole-cities-foundation | Commitment to Society | /mission-values/caring-communities | monsterNavMissionValues |

@monster-nav @mission-values @nojs @unauth @slow
Scenario Outline: Mission and Values - Environmental Stewardship
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | A Better Bag | /mission-values/environmental-stewardship/better-bag | Environmental Stewardship | /environmental-stewardship | monsterNavMissionValues |
    | Cloned Meat Q&A | /mission-values/environmental-stewardship/cloned-meat-qa | Environmental Stewardship | /environmental-stewardship | monsterNavMissionValues |
    | Doing The Green Thing | /mission-values/environmental-stewardship/doing-green-thing | Environmental Stewardship | /environmental-stewardship | monsterNavMissionValues |
    | Eco Scale | /eco-scale-our-commitment | Environmental Stewardship | /environmental-stewardship | monsterNavMissionValues |
    | Green Mission | /mission-values/environmental-stewardship/green-mission | Environmental Stewardship | /environmental-stewardship | monsterNavMissionValues |
#@monster-nav @mission-values @nojs @unauth @slow
#Scenario Outline: Mission and Values - Organic Farming
#  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
#  Examples:
#    | link_text | link_href | heading_text | heading_href | menu_region |
#    | Organic Industry Timeline | /mission-values/organic-farming/organic-industry-timeline | Organic Farming | /organic| monsterNavMissionValues |
#    | Growth of the Organics Industry | /mission-values/organic/growth-organics-industry | Organic Farming | /organic| monsterNavMissionValues |
#    | About Organic Farming | /mission-values/organic/about-organic-farming | Organic Farming | /organic| monsterNavMissionValues |
#    | Farming Organically | /mission-values/organic-farming/farming-organically | Organic Farming | /organic| monsterNavMissionValues |
#    | How Does Biodynamic Farming Fit In? | /mission-values/organic/how-does-biodynamic-farming-fit | Organic Farming | /organic| monsterNavMissionValues |
#    | More | /organic | Organic Farming | /organic | monsterNavMissionValues |

@monster-nav @mission-values @nojs @unauth @slow
Scenario Outline: Mission and Values - Seafood Sustainability
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Seafood Sustainability Basics | /seafood-sustainability-basics | Seafood Sustainability | /sustainable-seafood | monsterNavMissionValues |
    | Our Collaboration with the Marine Stewardship Council | /missions-values/seafood-sustainability/our-collaboration-marine-stewardship-council | Seafood Sustainability | /sustainable-seafood | monsterNavMissionValues |
    | Wild-Caught Seafood Sustainability Ratings | /sustainable-wild-caught-seafood | Seafood Sustainability | /sustainable-seafood | monsterNavMissionValues |
    | Aquaculture | /farm-raised-seafood | Seafood Sustainability | /sustainable-seafood | monsterNavMissionValues |
    | Seafood Sustainability FAQ | /seafood-sustainability-faq | Seafood Sustainability | /sustainable-seafood | monsterNavMissionValues |
    | More | /sustainable-seafood | Seafood Sustainability | /sustainable-seafood | monsterNavMissionValues |

@monster-nav @mission-values @nojs @unauth @slow
Scenario Outline: Mission and Values - Whole Trade
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Whole Trade Helps Real People | /whole-trade/whole-trade-helps-real-people | Whole Trade | /mission-values/whole-trade-program | monsterNavMissionValues |
    | Whole Trade Videos | /whole-trade/whole-trade-videos | Whole Trade | /mission-values/whole-trade-program | monsterNavMissionValues |
    | Whole Trade Products | /whole-trade/whole-trade-products | Whole Trade | /mission-values/whole-trade-program | monsterNavMissionValues |
    | Our Certifier Partners | /mission-values/whole-trade-program/certifier-partners | Whole Trade | /mission-values/whole-trade-program | monsterNavMissionValues |

@monster-nav @mission-values @nojs @unauth @slow
Scenario Outline: Mission and Values - Animal Welfare
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | 5-Step® Animal Welfare Rating | /mission-values/animal-welfare/5-step-animal-welfare-rating | Animal Welfare | /animal-welfare | monsterNavMissionValues |
    | Animal Welfare Basics | /mission-values/animal-welfare/animal-welfare-basics | Animal Welfare | /animal-welfare | monsterNavMissionValues |
    | Collaboration with Global Animal Partnership | /mission-values/animal-welfare/collaboration-global-animal-partnership | Animal Welfare | /animal-welfare | monsterNavMissionValues |
    | Meet the Farmers & Ranchers | /mission-values/animal-welfare/meet-farmers-ranchers | Animal Welfare | /animal-welfare | monsterNavMissionValues |

##### - Mission & Values End - #####

##### - Blog Start - #####
@monster-nav @blog @nojs @unauth @quick @slow
Scenario Outline: Blog - Whole Story
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Whole Story Blog | /blog/whole-story | Whole Story | /whole-story | monsterNavBlog |

@monster-nav @blog @nojs @unauth @slow
Scenario Outline: Blog - CEO Blogs
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | John Mackey's Blog | /blog/john-mackeys-blog | CEO Blogs | /ceo-blogs | monsterNavBlog |
##### - Blog End - #####

##### - Store Departments Start - #####
@monster-nav @store-departments @javascript @unauth @slow
Scenario Outline: Store Departments - Unique To Your Store
  And I wait for ajax calls to finish
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Events Calendar | /events | Unique To Your Store | /stores/list | monsterNavStoreDepartments |
    | Find Your Store | /stores/list | Unique To Your Store | /stores/list | monsterNavStoreDepartments |

@monster-nav @store-departments @javascript @unauth @slow
Scenario Outline: Store Departments - Store Departments
  And I wait for ajax calls to finish
  Then I should see the link "<link_text>" with href "<link_href>" under the "<heading_text>" heading link with href "<heading_href>" in the "<menu_region>" region
  Examples:
    | link_text | link_href | heading_text | heading_href | menu_region |
    | Bakery | /department/bakery | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Beer | /department/beer | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Bulk | /department/bulk | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Cheese | /department/cheese | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Coffee &amp; Tea | /department/coffee-tea | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Flowers and Floral Arrangements | /department/flowers-and-floral-arrangements | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Grocery | /department/grocery | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Meat &amp; Poultry | /department/meat-poultry | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Prepared Foods | /department/prepared-foods | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Produce | /department/produce | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Seafood | /department/seafood | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Wine | /department/wine | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Whole Body | /department/whole-body | Store Departments | /store-departments | monsterNavStoreDepartments |
    | Pets | /department/pets | Store Departments | /store-departments | monsterNavStoreDepartments |       
##### - Store Departments End - #####