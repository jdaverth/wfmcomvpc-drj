Feature: Preliminary Site Load & Functionality
  Used for CodeShip Builds
  A set of quick tests to determine build integrity
  And basic site functionality
  For any build

@preliminary @unauth @javascript
Scenario: Homepage Loads - Checks that the homepage and top menu load
  Given I am on the homepage
  Given That the Window is Full Screen
  Then I verify upper menu bar

@preliminary @auth @javascript
Scenario: Test I can log in
  Given I am on the homepage
  Then I click "Sign In / Register"
  And I wait for "1000" milliseconds
  Then I login as a test user

  @preliminary @unauth @javascript @sales-flyer
  Scenario: Check that Sales Flyer page and Sales Flyers load
    Given I have no store selected
    Given I am on "/sales-flyer"
    Then I should see the text "Sales and Coupons"
    Given I select "Texas" from "state"
    Given I select "Lamar - 525 N Lamar Blvd." from "store"
    Then I press "View Sales & Coupons"
    Then Nuke the qualtrix popup
    Then I press "View Sales & Coupons"
    Then I should see at least "16" ".view-sales-items .views-row" elements

@preliminary @unauth @javascript @locator @store-search @search
Scenario: Check that Store Lists Page Loads and can Search
  Given That the Window is Full Screen
  Given I am on "/stores/list"
  Then I perform a "non-paginated" "store" search for "Austin, TX"
  Then I should see at least "6" "#content .views-row" elements
  #Then I should see "525 N Lamar Blvd." in the store results #Only run this if we're going to have reindexing automatically completed, otherwise, it will most likely fail because Lamar store will not be indexed and won't be a result.

@preliminary @unauth @javascript @locator
Scenario: Select "Make This My Store" button, then try changing selection
  Given I am on "/stores/list"
  And I select store page 2
  Given I select a random store
  Then I should see the selected store name in the "header" region
  Given I am on "/stores/list"
  And I select store page 3
  Given I select a random store
  Then I should see the selected store name in the "header" region

@preliminary @unauth @nojs
Scenario: Customer Service Page Loads
  Given I am on "/customer-service"
  Then I should see the heading "Customer Service" in the "content" region

@preliminary @nojs @global-search @search
Scenario: View search results
  Given I am on the homepage
  Then I perform a "non-paginated" "global" search for "cheese"

@preliminary @unauth @nojs @healthy-eating
Scenario: Healthy Eating Page Loads
  Given I am on "/healthy-eating"
  Then I should see the text "Welcome" in the "header" region
  Then I should see the text "Healthy Eating"

@preliminary @javascript @products @product-search @search
Scenario: Product search
  Given I am on "/about-our-products"
  Then I should see the text "Welcome" in the "header" region
  Then I should see the text "About Our Products"
  Then I perform a "non-paginated" "product" search for "organic"

@preliminary @unauth @javascript @recipes @recipe-search @search
Scenario: Recipe Page Loads
  Given I am on "/recipes"
  Then I should see the text "Welcome" in the "header" region
  Then I should see the link "Featured" in the "content" region
  Then I should see the link "Newest" in the "content" region
  Then I should see the link "Top Rated" in the "content" region
  Then I should see at least "9" "#quicktabs-tabpage-recipes-0 .views-row" elements
  Then I perform a "non-paginated" "recipe" search for "bread"

@preliminary @javascript @online-ordering @estore
Scenario: Online Ordering page loads and eStore can be accessed
  Given I have no store selected
  Given I am on "/online-ordering"
  Then I should see the text "Welcome" in the "header" region
  Given That the Window is Full Screen
  Then I should see the text "SHOP ONLINE"
  Then I click "Select your Store"
  Then I should be on "/shop/choose"
  Given I select "California" from "state"
  Given I select "Ocean - 1150 Ocean Ave" from "store"
  Then I press "Continue"
  Then I should be on "/shop/OCN"

@preliminary @unauth @nojs @mission-values
Scenario: Mission & Values Page Loads
  Given I am on "/mission-values"
  Then I should see the text "Welcome" in the "header" region
  Then I should see the text "Mission & Values"

@preliminary @unauth @javascript @blog @blog-search @search
Scenario: Mission & Values Page Loads
  Given I am on "/blog/whole-story"
  Then I should see the text "Welcome" in the "header" region
  Then I should see the text "Whole Story"
  Then I should see at least "5" ".view-id-blogs .views-row"
  Then I perform a "non-paginated" "blog" search for "garden"

@preliminary @unauth @nojs @store-departments
Scenario: Mission & Values Page Loads
  Given I am on "/store-departments"
  Then I should see the text "Welcome" in the "header" region
  Then I should see the text "Store Departments"
