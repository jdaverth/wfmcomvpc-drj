Feature: Individual Recipe Page - No JS
  In order to ensure individual recipe pages load
  As an unauthenticated user
  I need to verify all page blocks are displayed correctly

Background:
  Given I am not logged in
  Given I am on "/recipe/sonoma-chicken-salad"

@recipe @unauth @nojs @quick
Scenario: Recipe Page Loads
  Then I should see the text "Sonoma Chicken Salad" in the "content" region

@recipe @unauth @nojs @quick
Scenario: Recipe Breadcrumbs are Functional
  Then I should see the link "Home" in the "breadcrumb" region
  And I should see the link "Recipes" in the "breadcrumb" region
  And I should see the text "Sonoma Chicken Salad" in the "breadcrumb" region
  When I click "Home" in the "breadcrumb" region
  Given I am on "/recipe/sonoma-chicken-salad"
  When I click "Recipes" in the "breadcrumb" region
  Then I should be on "/recipes"

@recipe @unauth @nojs @quick
Scenario: Recipe Picture Exists
  Then I should see an image in container ".recipe-image" in the "contentTop" region

@recipe @unauth @nojs @quick
Scenario: Recipe Serves
  Then I should see text matching "Serves \d+"
  
@recipe @unauth @nojs @quick
Scenario: Recipe Content Links
  Then I should see the link "Log in or create an account to save favorite recipes to your Recipe Box"
  Then I should see the link "print"
  
@recipe @unauth @nojs @quick
Scenario: Social Links Loaded
  Then I should see the Facebook Like Buttom
  Then I should see the Twitter Tweet Button
  Then I should see the Pin It Button

@recipe @unauth @nojs @quick
Scenario: Can I Email Recipe
  Then I should see the link "email" with href "mailto:" in the "contentTop" region

@recipe @unauth @nojs @quick
Scenario: Can I See Ingredients Heading
  Then I should see the text "Ingredients" in the "content" region

@recipe @unauth @nojs @quick
Scenario: Can I See Ingredients
  Then I should see list items with parent "class" "ingredient_ul"

@recipe @unauth @nojs @quick
Scenario: Can I See Login to Add Ingredients Text
  Then I should see the text "You must be signed in to use shopping lists" in the "content" region

@recipe @unauth @nojs @quick
Scenario: Sign In or Create Account Link
  When I click "Sign in or create account" in the "content" region
  Then I should see the text "Sign in or create an account"

@recipe @unauth @nojs @quick
Scenario: Is the Sidebar Visible
  Then I should see the text "Featured Recipes" in the "sidebar" region
  And I should see the text "Related Recipes" in the "sidebar" region

#@recipe @unauth @nojs @quick
#Scenario: Recipe Discussion Visible
#  Then I should see the text "Recipe Discussion" in the "contentBottom" region