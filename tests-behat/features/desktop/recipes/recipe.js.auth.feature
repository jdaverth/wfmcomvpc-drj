Feature: Individual Recipe Page - JS
  As an authenticated user
  I need to add and remove recipes from my recipe box
  so I can keep track of recipes that match my current tastes.

#dropped the Background even though there is some code duplication
#errors in Background are suppressed and hard to debug.

  @recipe @auth @javascript
  Scenario: Add a random recipe to the to Desktop Recipe Box and remove it
    Given That the Window is Full Screen
    Then I login as a test user
    Then I wait for "3" milliseconds
    Given That my desktop recipe box is empty
    Given I am on "recipes"
    Then I click a random element identified by ".view-display-id-featured .views-row"
    And I wait for "1000" milliseconds
    When I click "add to recipe box"
    And I wait for "1000" milliseconds
    #Try doing it a second time to see if the link text updates
#    When I click "add to recipe box"
#    And I wait for "1000" milliseconds

    # Uncomment the next two lines when QD-3837 is fixed.
    #Then I should see the link "remove from recipe box"
    #When I click "remove from recipe box"

    # This is a workaround for QD-3837.
    # Remove next six lines when QD-3837 is fixed.
    Given I am on "/user/recipe_box"
    And I wait for "3000" milliseconds
    Then I should see the link "Remove"
    When I click "Remove"
    And I wait for "1000" milliseconds
    Then I should not see an ".remove_link" element


  @recipe @auth @javascript @box
  Scenario: Add random recipe to Desktop Recipe Box, verify it shows up in my recipe box, and remove it
    Given That the Window is Full Screen
    Then I login as a test user
    Then I wait for "3" milliseconds
    Given That my desktop recipe box is empty
    Given I am on "recipes"
    Then I click a random element identified by ".view-display-id-featured .views-row"
    When I click "add to recipe box"
    When I click "view saved recipes"
    And I wait for "5000" milliseconds
    Then I should see the randomly added recipe identified by "#content ul li a"
    Given That my desktop recipe box is empty
    Then I should not see an "#block-users-user-recipe-self .content ul li a" element
