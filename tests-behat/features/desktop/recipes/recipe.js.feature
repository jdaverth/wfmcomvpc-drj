Feature: Individual Recipe Page - JS
  In order to ensure individual recipe pages load
  As an unauthenticated user
  I need to verify all page blocks are displayed correctly

Background:
  Given I am not logged in
  Given That the Window is Full Screen
  Given I am on "/recipe/sonoma-chicken-salad"

@recipe @javascript @unauth
Scenario: Can I Search in Sidebar With Results
  Then I perform a "paginated" "recipe side" search for "chicken"

@recipe @javascript @unauth
Scenario: Can I Search in Sidebar No Results
  Given I fill in "search_box" with "pebkac-pebkac-pebkac" in the "sidebar" region
  Given I press "Search" in the "sidebar" region
  Then I should be on "/recipe/search/pebkac-pebkac-pebkac" 
  Then I should see no search results for "pebkac-pebkac-pebkac" in the "content" region

@recipe @javascript @unauth
Scenario: Are Sidebar Links Correct
  When I click the first link in the container "id" "block-views-related-recipes-featured" in the "sidebar" region
  Then I should see the link "Recipes" in the "breadcrumb" region
  Then I should see an image in container ".recipe-image .field-type-image img" in the "contentTop" region  
  Then I should see the text "Ingredients" in the "content" region


@recipe @javascript @unauth @print
Scenario: Can I Print Full Page With Image
  When I click "print" in the "contentTop" region
  Then I wait for "1000" milliseconds
  Then I should see the text "Full Recipe (+ Image)" in the "#print-box .full_recipe_image" container
  When I select radio button with value "recipe_with_image"

@recipe @javascript @unauth @print
Scenario: Can I Print Full Page Without Image
  When I click "print" in the "contentTop" region
  Then I wait for "1000" milliseconds
  Then I should see the text "Full Recipe (No Image)" in the "#print-box .full_recipe" container
  When I select radio button with value "recipe_no_image"

@recipe @javascript @unauth @print
Scenario: Can I Print 3x5
  When I click "print" in the "contentTop" region
  Then I wait for "1000" milliseconds
  Then I should see the text "3 X 5 Recipe Card" in the "#print-box .recipe_card" container
  When I select radio button with value "recipe_3x5"
