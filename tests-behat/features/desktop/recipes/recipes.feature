#features/recipes.feature

Feature: Recipes Page
  In order to assure basic functionality of the recipes page
  As an unauthenticated user
  I need to be able to browse the recipes by the featured, top rated, recommend, and newest tabs 
  And view the results
  
Background: 
  Given That the Window is Full Screen
  Given I am on "/recipes"

@recipes @javascript @quick @disabled
Scenario: Recipes page loads basic content sections 
  Then I should see an "#healthy-eating-recipe-search" element
  And I should see tabs to filter recipes
  
@javascript @recipes @quick
Scenario: Checks for a recipe spotlight at the top of page  
  Then I should see an ".recipeSpotlight" element

@javascript @recipes
Scenario: Checks to make sure tabs are functional 
  When I click "Top Rated" in the "content" region
  Then "#quicktabs-tabpage-recipes-1" should be visible
  When I click "Newest" in the "content" region
  Then "#quicktabs-tabpage-recipes-2" should be visible
  When I click "Featured" in the "content" region
  Then "#quicktabs-tabpage-recipes-0" should be visible 

@javascript @recipes
Scenario: Checks that advanced search box can be expanded and collapsed 
  When I click "Advanced Search" in the "contentTop" region
  Then "#edit-advanced" should be visible
  When I click "Basic Search" in the "contentTop" region 
  Then "#edit-advanced" should not be visible

@javascript @recipes @recipe-search @quick @search
Scenario: Basic search with query works
  Then I perform a "paginated" "recipe" search for "bread"
  
@javascript @recipes @recipe-search @search
Scenario: Advanced search with nonempty query
  Given I am on "/recipes"
  Then I click "Advanced Search"
  Given I check the "Gluten Free" checkbox
  Given I check the "Vegetarian" checkbox
  Given I check the "Fruit" checkbox
  Then I perform a "non-paginated" "recipe" search for "pineapple"
  
@javascript @recipes @recipe-search @search @disabled
Scenario: Advanced search with empty query
  Then I click "Advanced Search"
  Given I check the box "field_recipe_main_ingredient[783]"
  When I press "op" in the "contentTop" region
  Then I should be on "/recipe/search/%20?f%5B0%5D=field_recipe_main_ingredient%253Aname%3ABeef"
  And I should see at least "4" ".search-result"
  
@javascript @recipes @recipe-search @search
Scenario: Advanced search with empty query and using an occasion filter 
  Given That the Window is Full Screen
  Given I am on "/recipes"
  Then I click "Advanced Search"
  Given I check the "Gluten Free" checkbox
  Given I check the "Fruit" checkbox
  Then I perform a "non-paginated" "recipe" search for ""
  
@javascript @recipes @recipe-search @search @disabled
Scenario: Advanced search with query and using an occasion filter 
  Then I click "Advanced Search" in the "contentTop" region
  Given I check the first checkbox under occasions
  Given for "search_box" I enter "cheese"  
  When I press "op" in the "contentTop" region
  Then I should be on a recipe search result page matching "/\/recipe\/search\/cheese\?f%5B0%5D=field_recipe_occasions%3A[0-9]{3}/"   
  And I should see relevant search results for "cheese" identified by "ol.search-results li.search-result h3.title a" in the "content" region
  
@javascript @recipes @recipe-search @search @disabled
Scenario: Advanced search with no query and using an occasion filter and one other filter (main ingredient)
  Then I click "Advanced Search" in the "contentTop" region
  Given I check the first checkbox under occasions 
  And I check the box "field_recipe_course[772]"
  When I press "op" in the "contentTop" region
  Then I should be on a recipe search result page matching "/\/recipe\/search\/%20\?f%5B0%5D=field_recipe_occasions%3A[0-9]{3}&f%5B1%5D=field_recipe_course%3A[0-9]{3}/"   
  And I should see at least "10" ".search-result"
  
@javascript @recipes @recipe-search @search @disabled
Scenario: Basic search with empty query 
  Given for "search_box" I enter ""
  When I press "Search" in the "contentTop" region
  Then I should be on "/recipe/search/%20"
  And I should see at least "10" ".search-result"
  
@javascript @recipes @pagination
Scenario: Pagination works in bottom of Top Rated tabs
  When I click "Top Rated" in the "content" region
  Then I should see an "#quicktabs-tabpage-recipes-1" element
  Then I check the paginated links

@javascript @recipes @pagination
Scenario: Pagination works in bottom of Newest tabs
  When I click "Newest" in the "content" region
  Then I should see an "#quicktabs-tabpage-recipes-2" element
  Then I check the paginated links