Feature: eStore Roles and Permission test cases for the LOCAL Admin, METRO Admin, REGIONAL Admin, GLOBAL Admin, the BLOG Admin and the Blog Author.
  As an Admin of various flavors
  I want to be able to verify that only allowed content can be created and edited by the various Admin types

#
#   DOTS-??? - Behat Tests for eStore - Roles and Permission Test Cases
#
# Scenario Outline: SETUP All ROLES & PERMISSIONS (Must have Drupal Admin permission)
# Scenario: TEST A LOCAL ADMIN can CREATE an EVENT, Person, and a Service.
# Scenario: TEST A LOCAL ADMIN cannot Create Events, People, or Services at an unassociated store.
# Scenario: TEST METRO ADMIN cannot Create Events, People, or Services at an unassociated store.
# Scenario: TEST REGIONAL ADMIN cannot Create Events, People, or Services at an unassociated store.
# Scenario: TEST LOCAL ADMIN MULTI can update LOCAL ADMIN's Service, Person and Event.
# Scenario: TEST METRO ADMIN can update LOCAL ADMIN's Event, Person and Service.
# Scenario: TEST REGIONAL ADMIN can update LOCAL ADMIN's Event, Person and Service.
# Scenario: TEST GLOBAL ADMIN create all valid content.
# Scenario: TEST BLOG AUTHOR create Blog Post.
# Scenario: TEST BLOG ADMIN create Blog Post.
# Scenario: TEST BLOG ADMIN create Blog.
# Scenario: TEST LOCAL ADMIN cannot access any existing content outside of its own store.
# Scenario: TEST LOCAL ADMIN can access permitted content in its own store.
# Scenario: TEST METRO ADMIN cannot access any existing content outside of its own Metro area.
# Scenario: TEST METRO ADMIN can access for editing, permitted content types within its Metro area.
# Scenario: TEST REGIONAL ADMIN cannot access existing content outside of its region.
# Scenario: TEST REGIONAL ADMIN can access for editing permitted content types within its region.
# Scenario: TEST GLOBAL ADMIN can access any existing content type.
#

#############################################################################
#                             I M P O R T A N T
# The scenario to set up the Roles and Permissions test is commented out
# because the admin users are going to be given permanent role assignments
# in prod. If that does not happen then this scenario should be uncommented
# and a Drupal level Admin login must be supplied to constants
# DRUPAL_ADMIN_EMAIL and DRUPAL_ADMIN_PW in the context file RandPSubContext.php
#############################################################################
# This test should be be run after a stage DB reload occurs so that
# the roles and permissions for the users defined in this test are set with
# the correct values.
#############################################################################
#@auth @javascript @demo @randp_setup @setup_admin
#Scenario Outline: SETUP ROLES & PERMISSIONS
#  Given That the Window is Full Screen
#    And I am on "/customer-service"
#    And I am logged in as a "<profile>"
#   When I am logged in as a "drupal admin"
#    And I follow "People"
#    And I initialize test role "<profile>"
#   Then I wait for admin element "PEOPLE_APPLY"
#    And I should see admin message "The changes have been saved."
#
#    Examples:
#      |     profile     |
#      | local admin     |
#      | two store admin |
#      | metro admin     |
#      | regional admin  |
#      | global admin    |
#      | blog admin      |
#      | blog author     |
#############################################################################


##########################################################################
#     This SCENARIO  requires that the "local admin" login exists.
##########################################################################
@auth @javascript @demo @randp @local_admin
Scenario: TEST A LOCAL ADMIN can CREATE an EVENT, Person, and a Service.
  Given That the Window is Full Screen
    And I am logged in as a "local admin"
  ###### CREATE EVENT ######
   When I follow "Event" in the "Add content" action list
    And I process "Create Event" with actions:
    """
      LOCATION_NAME      => "Zilker Park Rock Island",
      TITLE              => "Zilker Treasure Island Getaway",
      STORE              => "LMR (Lamar)",
      PERMISSIONS        => "LMR (Lamar)",
      SELECT_EVENT_MEDIA => ""
    """
    And I select media from tab "View Library" file "ANY"
    And I select admin element "SAVE"
   Then I wait for admin element "CONTENT"
    And I should see admin message "Zilker Treasure Island Getaway"
  ###### CREATE PERSON######
   When I am on "/customer-service"
    And I wait for admin element "ADMIN_DASHBOARD"
   When I follow "Person" in the "Add content" action list
    And I process "Create Person" with actions:
     """
       FULL_NAME   => "Wiffes Cousin",
       PERMISSIONS => "LMR (Lamar)",
       SAVE        => "click"
     """
   Then I wait for admin element "CONTENT"
    And I should see admin message "Wiffes Cousin"
  ###### CREATE SERVICE ######
  When I am on "/customer-service"
  And I wait for admin element "ADMIN_DASHBOARD"
  When I follow "Service" in the "Add content" action list
  And I process "Create Service" with actions:
        """
          PERMISSIONS    => "LMR (Lamar)",
          BASICS_TL_CODE => "LMR",
          TITLE          => "TestLocal1 Service",
        """
  And I select vertical tab "Marquee"
  And I select admin element "MARQUEE_SELECT_MEDIA"
  And I select media from tab "View Library" file "ANY"
  And I select admin element "SAVE"
  Then I wait for admin element "CONTENT"
  And I should see admin message "TestLocal1 Service"
  ###### REMOVE ALL CONTENT CREATED BY THIS ADMIN ######
   When I am on "/customer-service"
    And I wait for admin element "ADMIN_DASHBOARD"
   When I follow admin toolbar item "Content"
    And I search content for matches to :
       """
         CONTENT_LOCATION => "Lamar",
         CONTENT_AUTHOR   => "Test",
         APPLY            => "click"
       """
   Then I apply operation "Delete item" to "all" search results
    And I wait for action to finish
   Then I should see admin message "Performed Delete"
   And I sign out of drupal

##########################################################################
#     This SCENARIO  requires that the "local admin" login exists.
##########################################################################
@auth @javascript @demo @randp @local_admin
Scenario: TEST LOCAL ADMIN cannot Create Events, People, or Services at an unassociated store.
    Given That the Window is Full Screen
      And I am logged in as a "local admin"
    #----------------Create Service----------------------------
      And I follow "Service" in the "Add content" action list
      And I process "Create Service" with actions:
          """
            BASICS_TL_CODE => "LOC"               ,
            TITLE          => "TestLocal1 Service",
            PERMISSIONS    => "ABT (Arbor Trails)",
            SAVE           => "click"
          """
     Then I wait for admin element "ERROR_MESSAGE"
      And I should see admin message "Store / Region: You do not have permission to add"
    #----------------Create Person----------------------------
      And I follow "Person" in the "Add content" action list
      And I process "Create Person" with actions:
          """
            FULL_NAME   => "Wiffes Cousin",
            PERMISSIONS => "DOM (Domain)" ,
            SAVE        => "click"
          """
     Then I wait for admin element "ERROR_MESSAGE"
      And I should see admin message "Store / Region: You do not have permission to add"
    #----------------Create Event----------------------------
      And I follow "Event" in the "Add content" action list
      And I process "Create Event" with actions:
          """
            LOCATION_NAME => "Zilker Park Rock Island"       ,
            TITLE         => "Zilker Treasure Island Getaway",
            STORE         => "BEE (Bee Cave)"                ,
            PERMISSIONS   => "LMR (Lamar)"                   ,
            SAVE          => "click"
          """
     Then I wait for admin element "ERROR_MESSAGE"
      And I should see admin message "Store association: You do not have permission to add"
      And I sign out of drupal

##########################################################################
#     This SCENARIO  requires that the "metro admin" login exists.
##########################################################################
@auth @javascript @demo @randp @test_metro_admin
Scenario: TEST METRO ADMIN cannot Create Events, People, or Services at an unassociated store.
    Given That the Window is Full Screen
      And I am logged in as a "metro admin"
    #----------------Create Service----------------------------
      And I follow "Service" in the "Add content" action list
      And I process "Create Service" with actions:
          """
            BASICS_TL_CODE => "LOC"               ,
            TITLE          => "TestLocal1 Service",
            PERMISSIONS    => "CMD (Camden)",
            SAVE           => "click"
          """
     Then I wait for admin element "ERROR_MESSAGE"
      And I should see admin message "Store / Region: You do not have permission to add"
    #----------------Create Person----------------------------
     When I follow "Person" in the "Add content" action list
      And I process "Create Person" with actions:
          """
            FULL_NAME   => "Wiffes Cousin",
            PERMISSIONS => "PLN (Plano)" ,
            SAVE        => "click"
          """
     Then I wait for admin element "ERROR_MESSAGE"
      And I should see admin message "Store / Region: You do not have permission to add"
    #----------------Create Event----------------------------
     When I follow "Event" in the "Add content" action list
      And I process "Create Event" with actions:
          """
            LOCATION_NAME => "Zilker Park Rock Island"       ,
            TITLE         => "Zilker Treasure Island Getaway",
            STORE         => "VND (Vineyard)"                ,
            PERMISSIONS   => "LMR (Lamar)"                   ,
            SAVE          => "click"
          """
     Then I wait for admin element "ERROR_MESSAGE"
      And I should see admin message "Store association: You do not have permission to add"
     And I sign out of drupal

##########################################################################
#     This SCENARIO  requires that the "regional admin" login exists.
##########################################################################
@auth @javascript @demo @randp @test_metro_admin
Scenario: TEST REGIONAL ADMIN cannot Create Events, People, or Services at an unassociated store.
    Given That the Window is Full Screen
      And I am logged in as a "regional admin"
    #----------------Create Person----------------------------
     When I follow "Person" in the "Add content" action list
      And I process "Create Person" with actions:
          """
            FULL_NAME   => "Wiffes Cousin",
            PERMISSIONS => "RAL (Raleigh)" ,
            SAVE        => "click"
          """
     Then I wait for admin element "ERROR_MESSAGE"
      And I should see admin message "Store / Region: You do not have permission to add"
    #----------------Create Event----------------------------
     When I follow "Event" in the "Add content" action list
      And I process "Create Event" with actions:
          """
            LOCATION_NAME => "Zilker Park Rock Island"       ,
            TITLE         => "Zilker Treasure Island Getaway",
            STORE         => "CLO (Closter)"                ,
            PERMISSIONS   => "LMR (Lamar)"                   ,
            SAVE          => "click"
          """
     Then I wait for admin element "ERROR_MESSAGE"
      And I should see admin message "Store association: You do not have permission to add"
    #----------------Create Service----------------------------
     When I follow "Service" in the "Add content" action list
      And I process "Create Service" with actions:
              """
                BASICS_TL_CODE => "LOC"               ,
                TITLE          => "TestLocal1 Service",
                PERMISSIONS    => "CMD (Camden)",
                SAVE           => "click"
              """
     Then I wait for admin element "ERROR_MESSAGE"
      And I should see admin message "Store / Region: You do not have permission to add"
      And I sign out of drupal

##########################################################################
#     This SCENARIO  requires that the "local admin" login exists.
#          and also  requires that the "two store admin" login exists.
##########################################################################
 @auth @javascript @demo @randp @testlocalmulti
Scenario: TEST LOCAL ADMIN MULTI can update LOCAL ADMIN's Service.
  Given That the Window is Full Screen
    And I am logged in as a "local admin"
   When I follow "Service" in the "Add content" action list
    And I process "Create Service" with actions:
          """
             TITLE => "TestLocal1 Service to be updated",
             BASICS_TL_CODE => "LOC",
             PERMISSIONS    => "LMR (Lamar)"
          """
   And I select vertical tab "Marquee"
   And I select admin element "MARQUEE_SELECT_MEDIA"
    And I select media from tab "View Library" file "ANY"
    And I select admin element "SAVE"
   Then I wait for admin element "CONTENT"
    And I should see admin message "TestLocal1 Service to be updated"
   #
    And I follow "Event" in the "Add content" action list
    And I process "Create Event" with actions:
        """
        LOCATION_NAME      => "Zilker Park Rock Island",
        TITLE              => "Zilker Treasure Island Getaway",
        STORE              => "LMR (Lamar)",
        PERMISSIONS        => "LMR (Lamar)",
        SELECT_EVENT_MEDIA => ""
        """
    And I select media from tab "View Library" file "ANY"
    And I select admin element "SAVE"
   Then I wait for admin element "CONTENT"
    And I should see admin message "Zilker Treasure Island Getaway"
   #
   When I follow "Person" in the "Add content" action list
    And I process "Create Person" with actions:
        """
         FULL_NAME   => "TestLocalOnes Cousin"  ,
         PERMISSIONS => "LMR (Lamar)"           ,
         SAVE        => "click"
        """
    And I should see admin message "TestLocalOnes Cousin"
   #----------------Update newly added content--------------------------------------
    And I follow admin toolbar item "Content"
    And I find content "Service" titled "TestLocal1 Service to be updated" to "edit"
    And I process "Edit Service" with actions:
          """
             TITLE => "TestLocal1 Service updated by TestLocalMulti",
             SAVE => "click"
          """
   Then I should see admin message "TestLocal1 Service updated by TestLocalMulti"
   #
   When I follow admin toolbar item "Content"
    And I find content "Person" titled "TestLocalOnes Cousin" to "edit"
    And I process "Edit Person" with actions:
          """
            FULL_NAME => "Husbens Nefewe",
            SAVE => "click"
          """
   Then I should see admin message "Husbens Nefewe"
   #
   When I follow admin toolbar item "Content"
    And I find content "Event" titled "Zilker Treasure Island Getaway" to "edit"
    And I process "Edit Event" with actions:
          """
            TITLE => "TestLocal1 Event updated by TestLocalMulti",
            SAVE => "click"
          """
   Then I should see admin message "TestLocal1 Event updated by TestLocalMulti"
   # --------------------Delete newly added content--------------------------
   When I am logged in as a "local admin"
   And I wait for admin element "CONTENT"
   When I follow admin toolbar item "Content"
   And I search content for matches to :
         """
           CONTENT_LOCATION => "Lamar",
           CONTENT_AUTHOR   => "Test",
           APPLY            => "click"
         """
   Then I apply operation "Delete item" to "all" search results
   And I wait for action to finish
   Then I should see admin message "Performed Delete"
   And I sign out of drupal

##########################################################################
#     This SCENARIO  requires that the "local admin" login exists.
#          and also  requires that the "metro admin" login exists.
##########################################################################
@auth @javascript @demo @randp @testlocalmetro
Scenario: TEST METRO ADMIN can update LOCAL ADMIN's Event.
  Given That the Window is Full Screen
    And I am logged in as a "local admin"
    And I follow "Event" in the "Add content" action list
    And I process "Create Event" with actions:
      """
        LOCATION_NAME      => "Zilker Park Rock Island",
        TITLE              => "Zilker Treasure Island Getaway",
        STORE              => "LMR (Lamar)",
        PERMISSIONS        => "LMR (Lamar)",
        SELECT_EVENT_MEDIA => ""
      """
    And I select media from tab "View Library" file "ANY"
    And I select admin element "SAVE"
   Then I wait for admin element "CONTENT"
    And I should see admin message "Zilker Treasure Island Getaway"
   #
   When I follow "Person" in the "Add content" action list
    And I process "Create Person" with actions:
       """
         FULL_NAME   => "TestLocalOnes Cousin"  ,
         PERMISSIONS => "LMR (Lamar)"           ,
         SAVE        => "click"
       """
   Then I wait for admin element "CONTENT"
    And I should see admin message "TestLocalOnes Cousin"
   #
   When I follow "Service" in the "Add content" action list
    And I process "Create Service" with actions:
            """
              TITLE          => "TestLocal1 Service to be updated",
              BASICS_TL_CODE => "LOC",
              PERMISSIONS    => "LMR (Lamar)",
            """
    And I select vertical tab "Marquee"
    And I select admin element "MARQUEE_SELECT_MEDIA"
    And I select media from tab "View Library" file "ANY"
    And I select admin element "SAVE"
   Then I wait for admin element "CONTENT"
    And I should see admin message "TestLocal1 Service to be updated"
   # --------------------Update newly added content--------------------------
   When I am logged in as a "metro admin"
    And I wait for admin element "CONTENT"
    And I follow admin toolbar item "Content"
    And I find content "Event" titled "Zilker Treasure Island Getaway" to "edit"
    And I process "Edit Event" with actions:
          """
             TITLE => "TestLocal1 Event updated by TestLocalMetro",
             SAVE => "click"
          """
   Then I should see admin message "TestLocal1 Event updated by TestLocalMetro"
   #
   When I follow admin toolbar item "Content"
    And I find content "Person" titled "TestLocalOnes Cousin" to "edit"
    And I process "Edit Person" with actions:
            """
               FULL_NAME => "TestMetros Niece"           ,
               SAVE => "click"
            """
   Then I should see admin message "TestMetros Niece"
   #
   When I follow admin toolbar item "Content"
    And I find content "Service" titled "TestLocal1 Service to be updated" to "edit"
    And I process "Edit Service" with actions:
            """
              TITLE => "TestLocal1 Service updated by TestLocalMetro",
              SAVE => "click"
            """
   Then I should see admin message "TestLocal1 Service updated by TestLocalMetro"
   # --------------------Delete newly added content--------------------------
   When I am logged in as a "local admin"
    And I wait for admin element "CONTENT"
   When I follow admin toolbar item "Content"
    And I search content for matches to :
         """
           CONTENT_LOCATION => "Lamar",
           CONTENT_AUTHOR   => "Test",
           APPLY            => "click"
         """
   Then I apply operation "Delete item" to "all" search results
    And I wait for action to finish
   Then I should see admin message "Performed Delete"
  And I sign out of drupal

##########################################################################
#     This SCENARIO  requires that the "local admin" login exists.
#          and also  requires that the "regional admin" login exists.
##########################################################################
@auth @javascript @demo @randp @test_regional_admin
Scenario: TEST REGIONAL ADMIN can update LOCAL ADMIN's Event.
  Given That the Window is Full Screen
    And I am logged in as a "local admin"
   When I follow "Event" in the "Add content" action list
    And I process "Create Event" with actions:
        """
          LOCATION_NAME      => "Zilker Park Rock Island",
          TITLE              => "Zilker Treasure Island Getaway",
          STORE              => "LMR (Lamar)",
          PERMISSIONS        => "LMR (Lamar)",
          SELECT_EVENT_MEDIA => ""
        """
    And I select media from tab "View Library" file "ANY"
    And I select admin element "SAVE"
   Then I wait for admin element "CONTENT"
    And I should see admin message "Zilker Treasure Island Getaway"
   #
    When I follow "Person" in the "Add content" action list
     And I process "Create Person" with actions:
         """
           FULL_NAME   => "TestLocalOnes Cousin"  ,
           PERMISSIONS => "LMR (Lamar)"           ,
           SAVE        => "click"
         """
    Then I wait for admin element "CONTENT"
     And I should see admin message "TestLocalOnes Cousin"
     #
    When I follow "Service" in the "Add content" action list
     And I process "Create Service" with actions:
              """
                TITLE          => "TestLocal1 Service to be updated",
                BASICS_TL_CODE => "LOC",
                PERMISSIONS    => "LMR (Lamar)",
              """
     And I select vertical tab "Marquee"
     And I select admin element "MARQUEE_SELECT_MEDIA"
     And I select media from tab "View Library" file "ANY"
     And I select admin element "SAVE"
    Then I wait for admin element "CONTENT"
     And I should see admin message "TestLocal1 Service to be updated"
   # --------------------Update newly added content--------------------------
    When I am logged in as a "regional admin"
     And I wait for admin element "CONTENT"
     And I follow admin toolbar item "Content"
     And I find content "Event" titled "Zilker Treasure Island Getaway" to "edit"
     And I process "Edit Event" with actions:
              """
                 TITLE => "TestLocal1 Event updated by TestRegionalAdmin",
                 SAVE => "click"
              """
    Then I should see admin message "TestLocal1 Event updated by TestRegionalAdmin"
       #
    When I follow admin toolbar item "Content"
     And I find content "Person" titled "TestLocalOnes Cousin" to "edit"
     And I process "Edit Person" with actions:
                """
                   FULL_NAME => "TestRegionalAdmin Niece"           ,
                   SAVE => "click"
                """
    Then I should see admin message "TestRegionalAdmin Niece"
   #
    When I follow admin toolbar item "Content"
     And I find content "Service" titled "TestLocal1 Service to be updated" to "edit"
     And I process "Edit Service" with actions:
                """
                  TITLE => "TestLocal1 Service updated by TestRegionalAdmin",
                  SAVE => "click"
                """
    Then I should see admin message "TestLocal1 Service updated by TestRegionalAdmin"
       # --------------------Delete newly added content--------------------------
    When I am logged in as a "local admin"
     And I wait for admin element "CONTENT"
    When I follow admin toolbar item "Content"
     And I search content for matches to :
             """
               CONTENT_LOCATION => "Lamar",
               CONTENT_AUTHOR   => "Test",
               APPLY            => "click"
             """
    Then I apply operation "Delete item" to "all" search results
     And I wait for action to finish
    Then I should see admin message "Performed Delete"
    And I sign out of drupal

  #############################################################################
  # Global Admin CREATE: All valid content
  #############################################################################
    @auth @javascript @demo @randp @test_global_admin
    Scenario: TEST GLOBAL ADMIN create all valid content.
      Given That the Window is Full Screen
      And I am logged in as a "global admin"
  # Global Admin CREATE: Blog
      And I follow "Blog" in the "Add content" action list
      And I process "Create Blog" with actions:
          """
             TITLE       => "TestGlobalAdmin create Blog",
             PERMISSIONS => "ABT (Arbor Trails)"         ,
             SAVE        => "click"
          """
      Then I wait for admin element "CONTENT"
       And I should see admin message "TestGlobalAdmin create Blog"
      And I wait for admin element "ADMIN_DASHBOARD"
      Then I wait for "5" seconds
  # Global Admin CREATE: Blog Post
      And I follow "Blog Post" in the "Add content" action list
      And I process "Create Blog Post" with actions:
          """
             TITLE       => "TestGlobalAdmin create Blog Post",
             BLOG        => "Whole Story"                     ,
             CATEGORIES  => "#Bar"                            ,
             AUTHOR      => "(Hiring)"                        ,
             PERMISSIONS => "ABT (Arbor Trails)"              ,
             SAVE        => "click"
          """
      Then I wait for "5" seconds
      Then I wait for admin element "CONTENT"
       And I should see admin message "TestGlobalAdmin create Blog Post"
      And I wait for admin element "ADMIN_DASHBOARD"
  # Global Admin Create: Brand Camp article
    And I follow "Brand Camp Article" in the "Add content" action list
    And I process "Create Brand Camp Article" with actions:
        """
           BC_TITLE         => "TestGlobalAdmin create Brand Camp Article",
           SUB_TITLE        => "BCA Subtitle",
           SUMMARY          => "BCA Summary" ,
           SHOW_ME_FOOD     => "check"       ,
           SHOW_ME_PEOPLE   => "check"       ,
           SHOW_ME_PLANET   => "check"       ,
           I_WANT_TO_LEARN  => "check"       ,
           I_WANT_TO_DO     => "check"       ,
           PERMISSIONS      => "ABT (Arbor Trails)",
           SAVE             => "click"
        """
   Then I wait for admin element "CONTENT"
    And I should see admin message "TestGlobalAdmin create Brand Camp Article"
      And I wait for admin element "ADMIN_DASHBOARD"
  # Global Admin Create: Brand Camp Coupon
    And I follow "Brand Camp Coupon" in the "Add content" action list
    And I process "Create Brand Camp Coupon" with actions:
        """
           BC_TITLE    => "TestGlobalAdmin create Brand Camp Coupon",
           PERMISSIONS => "ABT (Arbor Trails)",
           SAVE        => "click"
        """
   Then I wait for admin element "CONTENT"
    And I should see admin message "TestGlobalAdmin create Brand Camp Coupon"
      And I wait for admin element "ADMIN_DASHBOARD"
  # Global Admin Create: Brand Camp Image
    And I follow "Brand Camp Image" in the "Add content" action list
    And I process "Create Brand Camp Image" with actions:
        """
           BC_TITLE    => "TestGlobalAdmin create Brand Camp Image",
           PERMISSIONS => "ABT (Arbor Trails)",
           SAVE        => "click"
        """
   Then I wait for admin element "CONTENT"
    And I should see admin message "TestGlobalAdmin create Brand Camp Image"
      And I wait for admin element "ADMIN_DASHBOARD"
  # Global Admin Create: Brand Camp Special
    And I follow "Brand Camp Special" in the "Add content" action list
    And I process "Create Brand Camp Special" with actions:
        """
           BC_TITLE    => "TestGlobalAdmin create Brand Camp Special",
           PERMISSIONS => "ABT (Arbor Trails)",
           SAVE        => "click"
        """
   Then I wait for admin element "CONTENT"
    And I should see admin message "TestGlobalAdmin create Brand Camp Special"
      And I wait for admin element "ADMIN_DASHBOARD"
  # Global Admin Create: Brand Camp video
    And I follow "Brand Camp Video" in the "Add content" action list
    And I process "Create Brand Camp Video" with actions:
        """
           BC_TITLE         => "TestGlobalAdmin create Brand Camp Video",
           SUB_TITLE        => "BCV Subtitle",
           SUMMARY          => "BCV Summary" ,
           SHOW_ME_FOOD     => "check"       ,
           SHOW_ME_PEOPLE   => "check"       ,
           SHOW_ME_PLANET   => "check"       ,
           I_WANT_TO_LEARN  => "check"       ,
           I_WANT_TO_DO     => "check"       ,
           PERMISSIONS      => "ABT (Arbor Trails)",
           SAVE             => "click"
        """
    Then I wait for admin element "CONTENT"
    And I should see admin message "TestGlobalAdmin create Brand Camp Video BCV Subtitle"
      And I wait for admin element "ADMIN_DASHBOARD"
  # Global Admin Create: Department
      And I follow "Department" in the "Add content" action list
      And I process "Create Department" with actions:
        """
           TITLE                    => "TestGlobalAdmin create Department"            ,
           PERMISSIONS              => "ABT (Arbor Trails)"                           ,
           DEPT_TEASER_DESCRIPTION  => "TestGlobalAdmin Department Teaser Description",
           DEPT_TEASER_SELECT_MEDIA => "click"
        """
      And I select media from tab "View Library" file "ANY"
      And I select admin element "SAVE"
      Then I wait for admin element "CONTENT"
      And I should see admin message "TestGlobalAdmin create Department"
      And I wait for admin element "ADMIN_DASHBOARD"
   # Global Admin Create Event with an image
      And I follow "Event" in the "Add content" action list
      And I process "Create Event" with actions:
        """
          LOCATION_NAME      => "Zilker Park Rock Island",
          TITLE              => "Zilker Treasure Island Getaway",
          STORE              => "LMR (Lamar)",
          PERMISSIONS        => "LMR (Lamar)",
          SELECT_EVENT_MEDIA => ""
        """
      And I select media from tab "View Library" file "ANY"
      And I select admin element "SAVE"
      Then I wait for admin element "CONTENT"
      And I should see admin message "Zilker Treasure Island Getaway"
      And I wait for admin element "ADMIN_DASHBOARD"
  # Global Admin Create: Job Position
    And I follow "Job Position" in the "Add content" action list
    And I process "Create Job Position" with actions:
      """
         TITLE              => "TestGlobalAdmin create Job Position",
         PERMISSIONS        => "ABT (Arbor Trails)"                       ,
         SAVE               => "click"
      """
   Then I wait for admin element "CONTENT"
    And I should see admin message "TestGlobalAdmin create Job Position"
      And I wait for admin element "ADMIN_DASHBOARD"
  # Global Admin Create: Local Vendor
    And I follow "Local Vendor" in the "Add content" action list
    And I process "Create Local Vendor" with actions:
      """
         TITLE       => "TestGlobalAdmin create Local Vendor",
         CITY        => "Smiley",
         STATE       => "South Dakota",
         ZIP         => "95787",
         PERMISSIONS => "ABT (Arbor Trails)"                       ,
         SAVE        => "click"
      """
   Then I wait for admin element "CONTENT"
    And I should see admin message "TestGlobalAdmin create Local Vendor"
      And I wait for admin element "ADMIN_DASHBOARD"
  # Global Admin Create: Marquee
    And I follow "Marquee" in the "Add content" action list
    And I process "Create Marquee" with actions:
      """
         TITLE                => "TestGlobalAdmin create Marquee",
         MARQUEE_HEADING_COPY => "TestGlobalAdmin Header Copy"   ,
         PERMISSIONS          => "ABT (Arbor Trails)"            ,
         SAVE                 => "click"
      """
   Then I wait for admin element "CONTENT"
    And I should see admin message "TestGlobalAdmin create Marquee"
      And I wait for admin element "ADMIN_DASHBOARD"
  # Global Admin Create: Metro
    And I follow "Metro" in the "Add content" action list
    And I process "Create Metro" with actions:
      """
         NAME        => "TestGlobalAdmin create Metro",
         ADDR1       => "707 Lost Seas",
         CITY        => "Austin",
         STATE       => "Texas",
         ZIP         => "78759",
         PERMISSIONS => "ABT (Arbor Trails)"            ,
         SAVE        => "click"
      """
   Then I wait for admin element "CONTENT"
    And I should see admin message "TestGlobalAdmin create Metro"
      And I wait for admin element "ADMIN_DASHBOARD"
  # Global Admin Create: National Offices
    And I follow "National Offices" in the "Add content" action list
    And I process "Create National Offices" with actions:
      """
         NAME                   => "TestGlobalAdmin create National Offices",
         NATIONAL_OFFICES_ADDR1 => "707 Lost Seas",
         NATIONAL_OFFICES_CITY  => "Austin",
         NATIONAL_OFFICES_STATE => "Texas",
         NATIONAL_OFFICES_ZIP   => "78759",
         PERMISSIONS            => "ABT (Arbor Trails)",
         SAVE                   => "click"
      """
   Then I wait for admin element "CONTENT"
    And I should see admin message "TestGlobalAdmin create National Offices"
      And I wait for admin element "ADMIN_DASHBOARD"
  # Global Admin Create: Newsletter
    And I follow "Newsletter" in the "Add content" action list
    And I process "Create Newsletter" with actions:
      """
         TITLE               => "TestGlobalAdmin create Newsletter",
         NEWSLETTER_CATEGORY => "Our Foundations",
         PERMISSIONS         => "ABT (Arbor Trails)",
         SAVE                => "click"
      """
   Then I wait for admin element "CONTENT"
    And I should see admin message "TestGlobalAdmin create Newsletter"
      And I wait for admin element "ADMIN_DASHBOARD"
  # Global Admin Create: Page
      And I follow "Page" in the "Add content" action list
      And I process "Create Page" with actions:
       """
         TITLE              => "TestGlobalAdmin create Page",
         PERMISSIONS        => "ABT (Arbor Trails)"         ,
         SAVE               => "click"
       """
     Then I wait for admin element "CONTENT"
      And I should see admin message "TestGlobalAdmin create Page"
      And I wait for admin element "ADMIN_DASHBOARD"
  # Global Admin EDIT: Person
      And I follow "Person" in the "Add content" action list
      And I process "Create Person" with actions:
       """
         FULL_NAME   => "Bud Best",
         PERMISSIONS => "GWY (Gateway), LMR (Lamar)",
         SAVE        => "click"
       """
     Then I wait for admin element "CONTENT"
      And I should see admin message "Bud Best"
      And I wait for admin element "ADMIN_DASHBOARD"
  # Global Admin Create: Product
      And I follow "Product" in the "Add content" action list
      And I process "Create Product" with actions:
      """
         TITLE       => "TestGlobalAdmin create Product",
         PERMISSIONS => "ABT (Arbor Trails)",
         SAVE        => "click"
      """
      Then I wait for admin element "CONTENT"
      And I should see admin message "TestGlobalAdmin create Product"
      And I wait for admin element "ADMIN_DASHBOARD"
  # Global Admin Create: Product Certification
      And I follow "Product Certification" in the "Add content" action list
      And I process "Create Product Certification" with actions:
      """
         TITLE       => "TestGlobalAdmin create Product Certification",
         PERMISSIONS => "ABT (Arbor Trails)",
         SAVE        => "click"
      """
      Then I wait for admin element "CONTENT"
      And I should see admin message "TestGlobalAdmin create Product Certification"
      And I wait for admin element "ADMIN_DASHBOARD"
  # Global Admin Create: Product Line
      And I follow "Product Line" in the "Add content" action list
      And I process "Create Product Line" with actions:
      """
         TITLE       => "TestGlobalAdmin create Product Line",
         PERMISSIONS => "ABT (Arbor Trails)",
         SAVE        => "click"
      """
      Then I wait for admin element "CONTENT"
      And I should see admin message "TestGlobalAdmin create Product Line"
      And I wait for admin element "ADMIN_DASHBOARD"
  # Global Admin Create: Promo
      And I follow "Promo" in the "Add content" action list
      And I process "Create Promo" with actions:
      """
         TITLE       => "TestGlobalAdmin create Promo",
         PERMISSIONS => "ABT (Arbor Trails)",
         SAVE        => "click"
      """
      Then I wait for admin element "CONTENT"
      And I should see admin message "TestGlobalAdmin create Promo"
      And I wait for admin element "ADMIN_DASHBOARD"
  # Global Admin Create: Recipe
      And I follow "Recipe" in the "Add content" action list
     Then I wait for admin element "SAVE"
     Then I fill admin element "PERMISSIONS" with "ABT (Arbor Trails)"
      And I select vertical tab "Imported Data *"
      And I fill admin element "TITLE" with "TestGlobalAdmin create Recipe"
      And I select admin element "SAVE"
     Then I wait for admin element "CONTENT"
      And I should see admin message "TestGlobalAdmin create Recipe"
      And I wait for admin element "ADMIN_DASHBOARD"
  # Global Admin Create: Region
      And I follow "Region" in the "Add content" action list
      And I process "Create Region" with actions:
        """
           TITLE       => "TestGlobalAdmin create Region",
           ADDR1       => "707 Lost Seas",
           CITY        => "Austin",
           STATE       => "Texas",
           ZIP         => "78759",
           PERMISSIONS => "ABT (Arbor Trails)",
           SAVE        => "click"
        """
     Then I wait for admin element "CONTENT"
      And I should see admin message "TestGlobalAdmin create Region"
      And I wait for admin element "ADMIN_DASHBOARD"
  # Global Admin Create: Service
      And I follow "Service" in the "Add content" action list
      And I process "Create Service" with actions:
            """
              BASICS_TL_CODE => "LOC",
              TITLE          => "Global Admin Service",
              PERMISSIONS    => "LMR (Lamar), KTY (Katy)",
            """
      And I select vertical tab "Marquee"
      And I select admin element "MARQUEE_SELECT_MEDIA"
      And I select media from tab "View Library" file "ANY"
      And I select admin element "SAVE"
     Then I wait for admin element "CONTENT"
      And I should see admin message "Global Admin Service"
      And I wait for admin element "ADMIN_DASHBOARD"
  # Global Admin Create: Special Diet
      And I follow "Special Diet" in the "Add content" action list
      And I process "Create Special Diet" with actions:
        """
           TITLE       => "TestGlobalAdmin create Special Diet",
           PERMISSIONS => "ABT (Arbor Trails)",
           SAVE        => "click"
        """
     Then I wait for admin element "CONTENT"
      And I should see admin message "TestGlobalAdmin create Special Diet"
      And I wait for admin element "ADMIN_DASHBOARD"
  # Global Admin Create: Store
      And I follow "Store" in the "Add content" action list
      And I process "Create Store" with actions:
         """
           STORE_NAME     => "Whole Foods Hi-City",
           STORE_NICKNAME => "WF_Hi-City1",
           BUSINESS_UNIT  => "10525"    ,
           ADDR1          => "0 Buut Sunshine",
           CITY           => "Smiley",
           STATE          => "South Dakota",
           ZIP            => "95787",
           PERMISSIONS    => "RM (Rocky Mountain)",
           SAVE           => "click"
         """
     Then I wait for admin element "CONTENT"
      And I should see admin message "Whole Foods Hi-City"
      And I wait for admin element "ADMIN_DASHBOARD"
  # Global Admin Create: Video
      And I follow "Video" in the "Add content" action list
      And I process "Create Video" with actions:
        """
           TITLE       => "TestGlobalAdmin create Video",
           PERMISSIONS => "ABT (Arbor Trails)",
           SAVE        => "click"
        """
     Then I wait for admin element "CONTENT"
      And I should see admin message "TestGlobalAdmin create Video"
      And I wait for admin element "ADMIN_DASHBOARD"
  # Global Admin Create: Webform
      And I follow "Webform" in the "Add content" action list
      And I process "Create Webform" with actions:
        """
           TITLE       => "TestGlobalAdmin create Webform",
           PERMISSIONS => "ABT (Arbor Trails)",
           SAVE        => "click"
        """
     Then I wait for admin element "WEBFORM_CONSOLE"
      And I should see admin text "TestGlobalAdmin create Webform"
      When I follow admin toolbar item "Content"
      And I search content for matches to :
        """
          CONTENT_AUTHOR   => "TestGlobalAdmin",
          APPLY            => "click"
        """
      Then I apply operation "Delete item" to "all" search results
      And I wait for action to finish
      Then I should see admin message "Performed Delete"
      #Do it again to remove stragglers
      When I follow admin toolbar item "Content"
      And I search content for matches to :
        """
          CONTENT_AUTHOR   => "TestGlobalAdmin",
          APPLY            => "click"
        """
      Then I apply operation "Delete item" to "all" search results
      And I wait for action to finish
      Then I should see admin message "Performed Delete"
      And I sign out of drupal

#############################################################################
# Blog Author CREATE: Blog Post
#############################################################################
  @auth @javascript @demo @randp @test_blog_author
  Scenario: TEST BLOG AUTHOR create Blog Post.
   Given That the Window is Full Screen
     And I am logged in as a "blog author"
    When I follow "Blog Post" in the "Add content" action list
     And I process "Create Blog Post" with actions:
        """
           TITLE       => "BlogAuthorTest create Blog Post",
           BLOG        => "Whole Story"                     ,
           CATEGORIES  => "#Bar"                            ,
           AUTHOR      => "(Hiring)"                        ,
           PERMISSIONS => "LMR (Lamar)"                     ,
           SAVE        => "click"
        """
    Then I wait for admin element "CONTENT"
    And I should see admin message "BlogAuthorTest create Blog Post"
    And I follow admin toolbar item "Content"
    And I find content "Blog Post" titled "BlogAuthorTest create Blog Post" to "Delete item"
    And I wait for action to finish
    Then I should see admin message "Performed Delete item on 1 item."
    And I sign out of drupal
    

#############################################################################
# Blog Admin CREATE: Blog Post
#############################################################################
  @auth @javascript @demo @randp @test_blog_admin
  Scenario: TEST BLOG ADMIN create Blog Post.
    Given That the Window is Full Screen
    And I am logged in as a "blog admin"
   When I follow "Blog Post" in the "Add content" action list
    And I process "Create Blog Post" with actions:
        """
           TITLE       => "BlogAdminTest create Blog Post",
           BLOG        => "Whole Story"                     ,
           CATEGORIES  => "#Bar"                            ,
           AUTHOR      => "(Hiring)"                        ,
           PERMISSIONS => "LMR (Lamar)"                     ,
           SAVE        => "click"
        """
    Then I wait for admin element "CONTENT"
    And I should see admin message "BlogAdminTest create Blog Post"
    And I follow admin toolbar item "Content"
    And I find content "Blog Post" titled "BlogAdminTest create Blog Post" to "Delete item"
    And I wait for action to finish
    Then I should see admin message "Performed Delete item on 1 item."
    And I sign out of drupal

#############################################################################
# Blog Admin CREATE: Blog
#############################################################################
  @auth @javascript @demo @randp @test_blog_admin
  Scenario: TEST BLOG ADMIN create Blog.
    Given That the Window is Full Screen
      And I am logged in as a "blog admin"
     When I follow "Blog" in the "Add content" action list
      And I process "Create Blog" with actions:
        """
           TITLE       => "BlogAdminTest create Blog",
           PERMISSIONS => "LMR (Lamar)"              ,
           SAVE        => "click"
        """
     Then I wait for admin element "CONTENT"
      And I should see admin message "BlogAdminTest create Blog"
      And I follow admin toolbar item "Content"
      And I find content "Blog" titled "BlogAdminTest create Blog" to "Delete item"
      And I wait for action to finish
     Then I should see admin message "Performed Delete item on 1 item."
    And I sign out of drupal

##########################################################################
#     This SCENARIO  requires that the "local admin" login exists.
##########################################################################
@auth @javascript @demo @randp @local_admin
Scenario: TEST LOCAL ADMIN cannot access any existing content outside of its own store.
  Given That the Window is Full Screen
    And I am logged in as a "local admin"
    And I wait for admin element "CONTENT"
   Then I check content by node ID to determine permission:
    | content type          | nodeID  | permitted |
    | Blog                  |    6860 |  no       |
    | Blog Post             | 5597176 |  no       |
    | Brand Camp Article    | 5421321 |  no       |
    | Brand Camp Coupon     |      -1 |  no       |
    | Brand Camp Image      | 2533491 |  no       |
    | Brand Camp Special    | 2552836 |  no       |
    | Brand Camp UGC image  | 3492201 |  no       |
    | Brand Camp Video      | 5596791 |  no       |
    | Chute URL             | 4013616 |  no       |
    | Coupon                |      -1 |  no       |
    | Department            | 3745161 |  no       |
    | Department Article    | 3745161 |  no       |
    | Event                 |  154006 |  no       |
    | Job Position          | 4061011 |  no       |
    | Local Vendor          | 3991301 |  no       |
    | Marquee               | 3273481 |  no       |
    | Metro                 |    7504 |  no       |
    | National Offices      |      95 |  no       |
    | Newsletter            |    7517 |  no       |
    | Page                  |      27 |  no       |
    | Person                | 5597941 |  no       |
    | Product               | 5595856 |  no       |
    | Product Certification |   83866 |  no       |
    | Product Line          |      31 |  no       |
    | Product Recall        | 5596441 |  no       |
    | Promo                 | 3180761 |  no       |
    | Recipe                | 1517981 |  no       |
    | Region                |      -1 |  no       |
#   | Sale Item             |      -1 |  no       |
    | Service               | 4002701 |  no       |
    | Special Diet          |      25 |  no       |
    | Store                 |       1 |  no       |
    | ^Video$               | 3919591 |  no       |
    | Webform               |    7477 |  no       |
   And I sign out of drupal

##########################################################################
#     This SCENARIO  requires that the "local admin" login exists.
##########################################################################
@auth @javascript @demo @randp @local_admin
Scenario: TEST LOCAL ADMIN can access permitted content in its own store.
  Given That the Window is Full Screen
    And I am logged in as a "local admin"
    And I wait for admin element "CONTENT"
  Then I check content by node ID to determine permission:
    | content type          | nodeID  | permitted |
    | Event                 | 5597076 |  allow    |
    | Person                |   54096 |  allow    |
    | Service               | 4260221 |  allow    |
    | Store                 |    6606 |  allow    |
    | Department Article    |   35756 |  allow    |
    | Marquee               |   35766 |  allow    |
    | Promo                 |   35681 |  allow    |
   And I sign out of drupal

##########################################################################
#     This SCENARIO  requires that the "metro admin" login exists.
##########################################################################
@auth @javascript @demo @randp @test_metro_admin
Scenario: TEST METRO ADMIN cannot access any existing content outside of its own Metro area.
  Given That the Window is Full Screen
    And I am logged in as a "metro admin"
    And I wait for admin element "CONTENT"
   Then I check content by node ID to determine permission:
    | content type          | nodeID  | permitted |
    | Blog                  |    6860 |  no       |
    | Blog Post             | 5597176 |  no       |
    | Brand Camp Article    | 5421321 |  no       |
    | Brand Camp Coupon     |      -1 |  no       |
    | Brand Camp Image      | 2533491 |  no       |
    | Brand Camp Special    | 2552836 |  no       |
    | Brand Camp UGC image  | 3948346 |  no       |
    | Brand Camp Video      | 5596791 |  no       |
    | Chute URL             | 4013616 |  no       |
    | Coupon                |      -1 |  no       |
    | Department            | 3745161 |  no       |
    | Department Article    | 3745161 |  no       |
    | Event                 | 5595381 |  no       |
    | Job Position          | 4061011 |  no       |
    | Local Vendor          |   24459 |  no       |
    | Marquee               |  396211 |  no       |
    | Metro                 |    7504 |  no       |
    | National Offices      |      95 |  no       |
    | Newsletter            |    7517 |  no       |
    | Page                  |      27 |  no       |
    | Person                |   39551 |  no       |
    | Product               | 5595856 |  no       |
    | Product Certification |   83866 |  no       |
    | Product Line          |      31 |  no       |
    | Product Recall        | 5596441 |  no       |
    | Promo                 | 3180761 |  no       |
    | Recipe                | 1517981 |  no       |
    | Region                |    7498 |  no       |
    | Sale Item             |      -1 |  DISABLED |
    | Service               |   39506 |  no       |
    | Special Diet          |      25 |  no       |
    | Store                 |       1 |  no       |
    | ^Video$               | 3919591 |  no       |
    | Webform               |    7477 |  no       |
   And I sign out of drupal

##########################################################################
#     This SCENARIO  requires that the "metro admin" login exists.
##########################################################################
  @auth @javascript @demo @randp @test_metro_adminadmin
Scenario: TEST METRO ADMIN can access for editing, permitted content types within its Metro area.
  Given That the Window is Full Screen
    And I am logged in as a "metro admin"
    And I wait for admin element "CONTENT"
   Then I check content by node ID to determine permission:
        | content type          | nodeID  | permitted |
        | Event                 | 5596241 |  allow    |
        | Person                | 3959751 |  allow    |
        | Service               | 2735836 |  allow    |
        | Store                 |    6561 |  allow    |
        | Department Article    |   35756 |  allow    |
        | Marquee               |      71 |  allow    |
        | Promo                 |   35681 |  allow    |
    And I sign out of drupal

##########################################################################
#     This SCENARIO  requires that the "regional admin" login exists.
##########################################################################
@auth @javascript @demo @randp @test_regional_admin
Scenario: TEST REGIONAL ADMIN cannot access existing content outside of its region.
  Given That the Window is Full Screen
    And I am logged in as a "regional admin"
    And I wait for admin element "CONTENT"
   Then I check content by node ID to determine permission:
    | content type          | nodeID  | permitted |
    | Blog                  |    6860 |  no       |
    | Blog Post             | 5597176 |  no       |
    | Brand Camp Article    | 5421321 |  no       |
    | Brand Camp Coupon     |      -1 |  no       |
    | Brand Camp Image      | 2533491 |  no       |
    | Brand Camp Special    | 4862556 |  no       |
    | Brand Camp UGC image  | 3135511 |  no       |
    | Brand Camp Video      | 5596791 |  no       |
    | Chute URL             | 4013616 |  no       |
    | Coupon                |      -1 |  no       |
    | Department            | 3745161 |  no       |
    | Department Article    | 3745161 |  no       |
    | Event                 | 5089211 |  no       |
    | Job Position          | 4061011 |  no       |
    | Local Vendor          |  374231 |  no       |
    | Marquee               | 3273481 |  no       |
    | Metro                 |    7504 |  no       |
    | National Offices      |      95 |  no       |
    | Newsletter            |    7517 |  no       |
    | Page                  |      27 |  no       |
    | Person                |  134761 |  no       |
    | Product               | 5595856 |  no       |
    | Product Certification |   83866 |  no       |
    | Product Line          |      31 |  no       |
    | Product Recall        | 5596441 |  no       |
    | Promo                 | 3180761 |  no       |
    | Recipe                | 1517981 |  no       |
    | Region                |    7496 |  no       |
#   | Sale Item             |      -1 |  no       |
    | Service               | 1791256 |  no       |
    | Special Diet          |      25 |  no       |
    | Store                 |    6612 |  no       |
#    | ^Video$               | 3176536 |  no       |
    | Webform               |   7477  |  no       |
    And I sign out of drupal

##########################################################################
#     This SCENARIO  requires that the "regional admin" login exists.
##########################################################################
@auth @javascript @demo @randp @test_regional_admin
Scenario: TEST REGIONAL ADMIN can access for editing permitted content types within its region.
  Given That the Window is Full Screen
    And I am logged in as a "regional admin"
    And I wait for admin element "CONTENT"
   Then I check content by node ID to determine permission:
        | content type          | nodeID  | permitted |
        | Event                 | 5595381 |  allow    |
        | Person                |   34951 |  allow    |
        | Service               |   39506 |  allow    |
        | Store                 | 2460051 |  allow    |
        | Department Article    |  473036 |  allow    |
        | Marquee               |  287746 |  allow    |
        | Promo                 |  175781 |  allow    |
   And I sign out of drupal

  ##########################################################################
  #     This SCENARIO  requires that the "global admin" login exists.
  ##########################################################################
  @auth @javascript @demo @randp @test_global_admin
  Scenario: TEST GLOBAL ADMIN can access any existing content type.
    Given That the Window is Full Screen
      And I am logged in as a "global admin"
      And I wait for admin element "CONTENT"
     Then I check content by node ID to determine permission:
       | content type          | nodeID         | permitted |
       | Blog                  |    6860        |  allow    |
       | Blog Post             | 5597176        |  allow    |
       | Brand Camp Article    | 5421321        |  allow    |
       | Brand Camp Coupon     |      -1        |  allow    |
       | Brand Camp Image      | 2533491        |  allow    |
       | Brand Camp Special    | 4862556        |  allow    |
       | Brand Camp Video      | 5596791        |  allow    |
       | Brand Camp UGC image  | 3994931        |  DISABLED |
       | Chute URL             | 4013616        |  DISABLED |
       | Coupon                |      -1        |  allow    |
       | Department            | 3745161        |  allow    |
       | Department Article    | 3745161        |  allow    |
       | Event                 | 5556676        |  allow    |
       | Job Position          | 4061011        |  allow    |
       | Local Vendor          |  374231        |  allow    |
       | Marquee               | 3273481        |  allow    |
       | Metro                 |    7504        |  allow    |
       | National Offices      |      95        |  allow    |
       | Newsletter            |    7517        |  allow    |
       | Page                  |      27        |  allow    |
       | Person                | 5597941        |  allow    |
       | Product               | 5595856        |  allow    |
       | Product Certification |   83866        |  allow    |
       | Product Line          |      31        |  allow    |
       | Product Recall        |      -1        |  DISABLED |
       | Promo                 | 3180761        |  allow    |
       | Recipe                | 1517981        |  allow    |
       | Region                |    7496        |  allow    |
       | Sale Item             |      -1        |  DISABLED |
       | Service               | 4002701        |  allow    |
       | Special Diet          |      25        |  allow    |
       | Store                 |      -1        |  allow    |
       | ^Video$               | 3919591        |  allow    |
       | Webform               |    7477        |  allow    |
    And I sign out of drupal

##########################################################################
##########################################################################
##########################################################################
#                 D I S A B L E D - T E S T S
# The disabled tests perform actual content lookup instead of
# direct node access. They should be run if the node versions fail
# because the node IDs were altered by a database reload.
##########################################################################
##########################################################################
##########################################################################

##########################################################################
#     This SCENARIO  requires that the "global admin" login exists.
##########################################################################
  @auth @javascript @demo @randp @test_global_admin @disabled
  Scenario: TEST GLOBAL ADMIN can access any existing content type.
    Given That the Window is Full Screen
    And I am on "/customer-service"
    And I am logged in as a "global admin"
    And I wait for admin element "CONTENT"
    And I follow admin toolbar item "Content"
    Then I check content type at location to determine permission:
      | content type          | location       | permitted |
      | Blog                  | WFM            |  allow    |
      | Blog Post             | WFM            |  allow    |
      | Brand Camp Article    | WFM            |  allow    |
      | Brand Camp Coupon     |                |  allow    |
      | Brand Camp Image      | WFM            |  allow    |
      | Brand Camp Special    | WFM            |  allow    |
      | Brand Camp Video      | WFM            |  allow    |
      | Brand Camp UGC image  |                |  allow    |
      | Chute URL             |                |  allow    |
      | Coupon                |                |  allow    |
      | Department            | WFM            |  allow    |
      | Department Article    | WFM            |  allow    |
      | Event                 | ARR (Arroyo)   |  allow    |
      | Job Position          | WFM            |  allow    |
      | Local Vendor          | SO (South)     |  allow    |
      | Marquee               | WFM            |  allow    |
      | Metro                 | WFM            |  allow    |
      | National Offices      | WFM            |  allow    |
      | Newsletter            |                |  allow    |
      | Page                  | WFM            |  allow    |
      | Person                | WFM            |  allow    |
      | Product               | WFM            |  allow    |
      | Product Certification |                |  allow    |
      | Product Line          |                |  allow    |
      | Product Recall        | WFM            |  DISABLED |
      | Promo                 | WFM            |  allow    |
      | Recipe                | WFM            |  allow    |
      | Region                | SO (South)     |  allow    |
      | Sale Item             | WFM            |  DISABLED |
      | Service               | WFM            |  allow    |
      | Special Diet          |                |  allow    |
      | Store                 | WFM            |  allow    |
      | ^Video$               |                |  allow    |
      | Webform               |                |  allow    |

##########################################################################
#     This SCENARIO  requires that the "regional admin" login exists.
##########################################################################
  @auth @javascript @demo @randp @test_regional_admin @disabled
  Scenario: TEST REGIONAL ADMIN can access for editing permitted content types within its region.
    Given That the Window is Full Screen
    And I am on "/customer-service"
    And I am logged in as a "regional admin"
    And I wait for admin element "CONTENT"
    And I follow admin toolbar item "Content"
    Then I check content type at location to determine permission:
      | content type          | location    | permitted |
      | Event                 | PLN (Plano) |  allow    |
      | Person                | PLN (Plano) |  allow    |
      | Service               | PLN (Plano) |  allow    |
      | Store                 | VOS (Voss)  |  allow    |
      | Department Article    | TLS (Tulsa) |  allow    |
      | Marquee               | KIR (Kirby) |  allow    |
      | Promo                 | KTY (Katy)  |  allow    |


##########################################################################
#     This SCENARIO  requires that the "regional admin" login exists.
##########################################################################
  @auth @javascript @demo @randp @test_regional_admin @disabled
  Scenario: TEST REGIONAL ADMIN cannot access existing content outside of its region.
    Given That the Window is Full Screen
    And I am on "/customer-service"
    And I am logged in as a "regional admin"
    And I wait for admin element "CONTENT"
    And I follow admin toolbar item "Content"
    Then I check content type at location to determine permission:
      | content type          | location                 | permitted |
      | Blog                  | WFM                      |  no       |
      | Blog Post             | WFM                      |  no       |
      | Brand Camp Article    | WFM                      |  no       |
      | Brand Camp Coupon     |                          |  no       |
      | Brand Camp Image      | WFM                      |  no       |
      | Brand Camp Special    | WFM                      |  no       |
      | Brand Camp UGC image  | TMC (Tamarac)            |  no       |
      | Brand Camp Video      | WFM                      |  no       |
      | Chute URL             |                          |  no       |
      | Coupon                |                          |  no       |
      | Department            | WFM                      |  no       |
      | Department Article    | WFM                      |  no       |
      | Event                 | ARR (Arroyo), BRE (Brea) |  no       |
      | Job Position          | WFM                      |  no       |
      | Local Vendor          | SO (South)               |  no       |
      | Marquee               | WFM                      |  no       |
      | Metro                 | WFM                      |  no       |
      | National Offices      | WFM                      |  no       |
      | Newsletter            |                          |  no       |
      | Page                  | WFM                      |  no       |
      | Person                | MBK (Mountain Brook)     |  no       |
      | Product               | WFM                      |  no       |
      | Product Certification |                          |  no       |
      | Product Line          |                          |  no       |
      | Product Recall        | WFM                      |  no       |
      | Promo                 | WFM                      |  no       |
      | Recipe                | WFM                      |  no       |
      | Region                | SO (South)               |  no       |
  #   | Sale Item             | MBK (Mountain Brook)     |  no       |
      | Service               | MBK (Mountain Brook)     |  no       |
      | Special Diet          |                          |  no       |
      | Store                 | MBK (Mountain Brook)     |  no       |
      | Video                 |                          |  no       |
      | Webform               |                          |  no       |


##########################################################################
#     This SCENARIO  requires that the "metro admin" login exists.
##########################################################################
  @auth @javascript @demo @randp @test_metro_adminadmin @disabled
  Scenario: TEST METRO ADMIN can access for editing, permitted content types within its Metro area.
    Given That the Window is Full Screen
    And I am on "/customer-service"
    And I am logged in as a "metro admin"
    And I wait for admin element "CONTENT"
    And I follow admin toolbar item "Content"
    Then I check content type at location to determine permission:
      | content type          | location      | permitted |
      | Event                 | GWY (Gateway) |  allow    |
      | Person                | GWY (Gateway) |  allow    |
      | Service               | GWY (Gateway) |  allow    |
      | Store                 | GWY (Gateway) |  allow    |
      | Department Article    | LMR (Lamar)   |  allow    |
      | Marquee               | GWY (Gateway) |  allow    |
      | Promo                 | LMR (Lamar)   |  allow    |

##########################################################################
#     This SCENARIO  requires that the "metro admin" login exists.
##########################################################################
  @auth @javascript @demo @randp @test_metro_admin @disabled
  Scenario: TEST METRO ADMIN cannot access any existing content outside of its own Metro area.
    Given That the Window is Full Screen
    And I am on "/customer-service"
    And I am logged in as a "metro admin"
    And I wait for admin element "CONTENT"
    And I follow admin toolbar item "Content"
    Then I check content type at location to determine permission:
      | content type          | location                | permitted |
      | Blog                  | WFM                     |  no       |
      | Blog Post             | WFM                     |  no       |
      | Brand Camp Article    | WFM                     |  no       |
      | Brand Camp Coupon     |                         |  no       |
      | Brand Camp Image      | WFM                     |  no       |
      | Brand Camp Special    | WFM                     |  no       |
      | Brand Camp UGC image  | TMC (Tamarac)           |  no       |
      | Brand Camp Video      | WFM                     |  no       |
      | Chute URL             |                         |  no       |
      | Coupon                |                         |  no       |
      | Department            | WFM                     |  no       |
      | Department Article    | WFM                     |  no       |
      | Event                 | PLN (Plano), KTY (Katy) |  no       |
      | Job Position          | WFM                     |  no       |
      | Local Vendor          | SW (Southwest)          |  no       |
      | Marquee               | PLN (Plano)             |  no       |
      | Metro                 | WFM                     |  no       |
      | National Offices      | WFM                     |  no       |
      | Newsletter            |                         |  no       |
      | Page                  | WFM                     |  no       |
      | Person                | PLN (Plano)             |  no       |
      | Product               | WFM                     |  no       |
      | Product Certification |                         |  no       |
      | Product Line          |                         |  no       |
      | Product Recall        | WFM                     |  no       |
      | Promo                 | WFM                     |  no       |
      | Recipe                | WFM                     |  no       |
      | Region                | SW (Southwest)          |  no       |
      | Sale Item             | PLN (Plano)             |  DISABLED |
      | Service               | PLN (Plano)             |  no       |
      | Special Diet          |                         |  no       |
      | Store                 | PLN (Plano)             |  no       |
      | ^Video$               |                         |  no       |
      | Webform               |                         |  no       |

##########################################################################
#     This SCENARIO  requires that the "local admin" login exists.
##########################################################################
  @auth @javascript @demo @randp @local_admin @disabled
  Scenario: TEST LOCAL ADMIN can access permitted content in its own store.
    Given That the Window is Full Screen
    And I am on "/customer-service"
    And I am logged in as a "local admin"
    And I wait for admin element "CONTENT"
    And I follow admin toolbar item "Content"
    Then I check content type at location to determine permission:
      | content type          | location    | permitted |
      | Event                 | LMR (Lamar) |  allow    |
      | Person                | LMR (Lamar) |  allow    |
      | Service               | LMR (Lamar) |  allow    |
      | Store                 | LMR (Lamar) |  allow    |
      | Department Article    | LMR (Lamar) |  allow    |
      | Marquee               | LMR (Lamar) |  allow    |
      | Promo                 | LMR (Lamar) |  allow    |

##########################################################################
#     This SCENARIO  requires that the "local admin" login exists.
##########################################################################
  @auth @javascript @demo @randp @local_admin @disabled
  Scenario: TEST LOCAL ADMIN cannot access any existing content outside of its own store.
    Given That the Window is Full Screen
    And I am on "/customer-service"
    And I am logged in as a "local admin"
    And I wait for admin element "CONTENT"
    And I follow admin toolbar item "Content"
    Then I check content type at location to determine permission:
      | content type          | location    | permitted |
      | Blog                  | WFM         |  no       |
      | Blog Post             | WFM         |  no       |
      | Brand Camp Article    | WFM         |  no       |
      | Brand Camp Coupon     |             |  no       |
      | Brand Camp Image      | WFM         |  no       |
      | Brand Camp Special    | WFM         |  no       |
      | Brand Camp UGC image  | WFM         |  no       |
      | Brand Camp Video      | WFM         |  no       |
      | Chute URL             |             |  no       |
      | Coupon                |             |  no       |
      | Department            | WFM         |  no       |
      | Department Article    | WFM         |  no       |
      | Event                 | WFM         |  no       |
      | Job Position          | WFM         |  no       |
      | Local Vendor          | WFM         |  no       |
      | Marquee               | WFM         |  no       |
      | Metro                 | WFM         |  no       |
      | National Offices      | WFM         |  no       |
      | Newsletter            | WFM         |  no       |
      | Page                  | WFM         |  no       |
      | Person                | WFM         |  no       |
      | Product               | WFM         |  no       |
      | Product Certification |             |  no       |
      | Product Line          |             |  no       |
      | Product Recall        | WFM         |  no       |
      | Promo                 | WFM         |  no       |
      | Recipe                | WFM         |  no       |
      | Region                | WFM         |  no       |
  #   | Sale Item             | WFM         |  no       |
      | Service               | WFM         |  no       |
      | Special Diet          |             |  no       |
      | Store                 | PLN (Plano) |  no       |
      | ^Video$               |             |  no       |
      | Webform               |             |  no       |

