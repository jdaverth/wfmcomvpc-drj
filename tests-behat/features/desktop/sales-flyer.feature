Feature: Sales Flyer
  To ensure Sales Flyer is working
  As an unauthenticated user
  I need to verify that the state and store selectors load correctly

Background:
  Given I have no store selected

@sales-flyer @unauth @nojs @quick
Scenario: Sales Flyer Page Properly Loads
  Given I am on "/sales-flyer"
  Then I should see a heading in the "salesAndCouponsHeaderBlock" region
  And I should not see an "#quicktabs-container-custom_quicktab_sales_coupons" element

@sales-flyer @unauth @nojs @quick @disabled
Scenario: Whole Deal Subfooter Links
  Then I should see the link "Coupons" with href "/coupons" in the ".wholecontent" container
  Then I should see the link "Budget-Friendly Recipes" with href "/about-our-products/whole-deal/budget-recipes" in the ".wholecontent" container
  Then I should see the link "Money-Saving Tips" with href "/about-our-products/whole-deal/money-saving-tips" in the ".wholecontent" container

@sales-flyer @unauth @javascript @disabled
Scenario: State Select Loads Correct Stores
  When I select "Alabama" from "edit-state-selector"
  When I additionally select "Mountain Brook - 3100 Cahaba Village Plaza" from "edit-store-selector"
  When I select "California" from "edit-state-selector"
  When I additionally select "Brentwood - 11737 San Vicente Blvd" from "edit-store-selector"
  When I additionally select "Hillcrest - 711 University Ave" from "edit-store-selector"
  When I select "Hawaii" from "edit-state-selector"
  When I additionally select "Maui - 70 East Kaahumanu Avenue #B" from "edit-store-selector"
  When I select "Texas" from "edit-state-selector"
  When I additionally select "Arbor Trails - 4301 W. William Cannon" from "edit-store-selector"
  When I additionally select "Alamo Quarry - 255 E. Basse Rd, Ste. 130" from "edit-store-selector"
  When I select "British Columbia" from "edit-state-selector"
  When I additionally select "Cambie - 510 West 8th Avenue" from "edit-store-selector"
  When I select "United Kingdom" from "edit-state-selector"
  When I additionally select "Clapham Junction - 305-311 Lavender Hill" from "edit-store-selector"

@sales-flyer @unauth @javascript @disabled
Scenario Outline: Load Correct Sales Flyers
  When I select "<state>" from "edit-state-selector" in the "#block-products-products-flyers-selector" container
  When I additionally select "<store>" from "edit-store-selector" in the "#block-products-products-flyers-seletor" container
  Then I click "View Specials"
  Then I should see the text "<header>" in the "#block-products-products-flyers-selector" container
  Then I should see the text "There are no sales flyers for this store at the moment" or at least "3" elements identified by ".view-sales-items .view-content .views-row .views-field-field-flyer-product-name" in the "#block-products-sale-items" container
  Examples:
    | state | store | header |
    | British Columbia | Cambie - 510 West 8th Avenue | What's On Sale at Cambie |
    | Ontario | Oakville - 301 Cornwall Rd | What's On Sale at Oakville |
    | Texas | Lamar - 525 N Lamar Blvd. | What's On Sale at Lamar |
    | Alabama | Mountain Brook - 3100 Cahaba Village Plaza | What's On Sale at Mountain Brook |
    | California | Brentwood - 11737 San Vicente Blvd | What's On Sale at Brentwood |
    | United Kingdom | Clapham Junction - 305-311 Lavender Hill | What's On Sale at Clapham Junction |
    | United Kingdom | Giffnock - 124-134 Fenwick Road | What's On Sale at Giffnock |

@sales-flyer @unauth @nojs @drush @each-store-has-sales-items @disabled
Scenario: Verify Each Store has Sales Items
  When I view each enabled store I should see at least "3" sales items identified by ".view-sales-items .view-content .views-row .views-field-field-flyer-product-name"

@sales-flyer @unauth @nojs @quick @testsales
Scenario: Sales Flyer Display Based On Selected Store
  Given my selected store is "LMR"
  And I am on "/sales-flyer"
  And I wait for "3" seconds
  Then I should see a heading in the "salesAndCouponsHeaderBlock" region
  And I should see sales items in the "salesAndCouponsQuicktab" region
  # todo step below fails for idk why? Maybe it needs a spin?
  #And I should see the text "Lamar" in the "breadcrumb" region
  And I should see the link "Download Sales Flyer as PDF"

@sales-flyer @unauth @nojs @quick
Scenario: Sales Flyer Display Based On Store In URL
  Given I am on "/sales-flyer/lamar"
  Then I should see a heading in the "salesAndCouponsHeaderBlock" region
  And I should see sales items in the "salesAndCouponsQuicktab" region
  And I should see the text "Lamar" in the "breadcrumb" region
  And I should see the link "Download Sales Flyer as PDF"

@sales-flyer @unauth @javascript
  Scenario: Toggling between sales and coupons works
    Given I am on "/sales-flyer/lamar"
    When I click "quicktabs-tab-custom_quicktab_sales_coupons-1"
    Then I should see coupons in the "salesAndCouponsQuicktab" region
