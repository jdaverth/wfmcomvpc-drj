Feature: Shopping list
To verify the Shopping List application is working
As an authenticated user
I need to be able to create a shopping list
I need to be able to add items to a shopping list
I need to be able to remove items from a shopping list
I need to be able to edit items on a shopping list
I need to be able to edit a shopping list title
I need to be able to delete a shopping list

Background:
  Given That the Window is Full Screen 
  Given I am not logged in
  Given I am on "recipes"
  When I click "Sign In" in the "header" region
  Then I login as a test user
  And I am on "/user/shopping_lists"

@shopping-list @javascript @auth
Scenario: Create A List
  And I click "+ Create a new list"
  And I fill in "list_name" with "Exciting New Shopping List"
  And I press "Add Shopping List"
  Then I should see "Exciting New Shopping List"

@shopping-list @javascript @auth
Scenario: Rename a Shopping List
  And I am on "/user/shopping_lists"
  When I click "Exciting New Shopping List"
  And I wait for "3000" milliseconds
  And I click "Rename"
  And I wait for "3000" milliseconds
  And I fill in "rename_list" with "Fancy Renamed Shopping List"
  #And I click the "Save Rename" link
  And I click "Save Rename"
  And I wait for "3000" milliseconds
  Then I should see "Fancy Renamed Shopping List"

@shopping-list @javascript @auth
Scenario: Add an Item to a Shopping List
  When I click "Fancy Renamed Shopping List"
  And I click "+ Add item"
  And I fill in "item_name" with "Apples"
  And I fill in "item_desc" with "Green Apples"
  And I select "Fresh produce" from "item_category"
  And I press "Add Item to List"
  Then I should see "Apples"
  And I should see "Green Apples"
  And I should see "Fresh produce"

@shopping-list @javascript @auth
Scenario: Edit an item in a shopping list
  When I click "Fancy Renamed Shopping List"
  And I click "Apples"
  And I fill in "edit-item-name--2" with "Apples - Edited"
  And I fill in "edit-item-desc--2" with "Edited description"
  And I select "Beer and wine" from "edit-item-category--2"
  And I press "Update Item"
  Then I should see "Apples - Edited"
  And I should see "Edited description"
  And I should see "Beer and wine"

@shopping-list @javascript @auth
Scenario: Delete an item from a shopping list
  When I click "Fancy Renamed Shopping List"
  And I click "Apples - Edited"
  And I press "Remove Item From List"
  And I wait for "1" seconds
  Then I should see "Fancy Renamed Shopping List"
  When I click "Fancy Renamed Shopping List"
  Then I should not see the text "Apples - Edited"

@shopping-list @javascript @auth
Scenario: Delete a shopping list
  When I click "Fancy Renamed Shopping List"
  And I click "Delete"
  And I wait for "1" seconds
  Then I should not see the text "Fancy Renamed Shopping List"