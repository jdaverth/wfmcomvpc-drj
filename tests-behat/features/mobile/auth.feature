Feature: Mobile Authentication Works
  In order to verfiy authentication functionality
  As an unauthenticated user
  I need to sign in
  And be certain that Janrain authenticates correctly

Background:
  Given I am on the homepage
  Given I am not logged in

@mobile @auth @javascript
Scenario: Janrain Login
  Then I should see the link "Sign in or Create account" in the "header" region
  When I click "Sign in or Create account" in the "header" region
  #Then I should be on "https://users.wholefoodsmarket.com/oauth/signin"
  #Is a modal and the URL should not change
  
  #For whatever reason, the below are labled as undefined when run.
  Then I login as a test user
  Then I should see test username in the "header" region 

# todo QD-1900 is finished. Can disable tag be removed now?
# The janrain login page and process is currently under construction.
# When Darren finishes QD-1900, drop @disabled and see if the test works.
# The test may need a bit of re-work after QD-1900 is finished.
@mobile @auth @javascript @disabled
Scenario: Redirect After Login
  When I click "more"
  And I wait for "1000" milliseconds
  When I click "My Recipe Box"
  When I click "sign in or create an account"
  Then I should be on "user-register"
  Then I login as a test user
  Then I should be on "myrecipebox"