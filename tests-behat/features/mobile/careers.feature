Feature: Careers
  To ensure the mobile careers page loads in entirety
  As an unauthenticated user
  I need to verify all page elements load correctly

Background:
  Given I am on "/careers"

@mobile @unauth @nojs @quick @careers
Scenario: Careers Page is Complete
  Then I should see the heading "Careers" in the "content" region
  Then I should see the link "United States" with href "https://career4.successfactors.com/career?company=WFM" in the "content" region
  Then I should see the link "Canada" with href "https://career4.successfactors.com/career?company=WFM&rcm%5fsite%5flocale=en%5fGB&" in the "content" region
  Then I should see the link "United Kingdom" with href "https://career4.successfactors.com/career?company=WFM&rcm%5fsite%5flocale=en%5fGB&" in the "content" region  
  Then the page element identified by ".view-generic-page-items .view-content .views-row .views-field-field-body-mobile .field-content" has at least "100" words in the "content" region