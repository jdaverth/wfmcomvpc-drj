Feature: Mobile Customer Service Form
  To ensure the form function correctly
  As an unauthenticated user
  With and without a store chosen
  I need to test the shit out of all the options

  Background:
    Given I am on "/mobile/customer-service"

  @customer-service @unauth @nojs @quick @mobile
  Scenario: Page loads
    Then I should see the text "Contact Your Local Store"

  @customer-service @unauth @javascript @mobile
  Scenario: Correct form elements are present on page load.
    Then I should see the elements: "#edit-phone-number-state-list, #edit-phone-number-store-list"
    And I should not see the elements: "#edit-submitted-choose-topic"
    And I should not see the elements: "#edit-submitted-name"
    And I should not see the elements: "#edit-submitted-phone"
    And I should not see the elements: "#edit-submitted-email"
    And I should not see the elements: "#edit-submitted-body"
    And I should not see the elements: "#edit-submit"

  @customer-service @unauth @javascript @mobile
  Scenario: Check appropriate events happen when store is selected.
    Then I should see the text "Please select your store from the above menu to see phone number."
    When I select "Texas" from "edit-phone-number-state-list"
    And I select "Lamar" from "edit-phone-number-store-list"
    Then I should see "This store can be reached directly at (512) 542-2200."
    And I should see an "#webform-component-choose-topic" element
    # Yeah, xpath. Couldn't select this any other way.
    And the "//*[@id='webform-client-form-7477']/div/input[1]" input should have the value of "LMR"

  @customer-service @unauth @javascript @mobile @disabled
  Scenario: Form fields are not visible when no topic is selected.
    Given I select "Texas" from "edit-phone-number-state-list"
    And I select "Lamar" from "edit-phone-number-store-list"
    Then I should see an "#webform-component-choose-topic" element
    And I should not see an "#webform-component-name" element
    And I should not see an "#webform-component-phone" element
    And I should not see an "#webform-component-email" element
    And I should not see an "#webform-component-body" element
    And I should not see an "#webform-component-product-upc" element
    And I should not see an "#webform-component-store-zip-code" element

  @customer-service @unauth @javascript @mobile
  Scenario Outline: Topic Select Displays Correct Form Fields
    Given I select "Texas" from "edit-phone-number-state-list"
    And I select "Lamar" from "edit-phone-number-store-list"
    And I select "<topic>" from "edit-submitted-choose-topic"
    Then I should see the elements: "<visible-elements>"
    Then I should not see the elements: "<invisible-elements>"

    Examples:
      | topic | visible-elements | invisible-elements |
      | Online Ordering | #webform-component-name, #webform-component-phone, #webform-component-email, #webform-component-body | #webform-component-product-upc, #webform-component-store-zip-code |
      | Store Feedback | #webform-component-name, #webform-component-phone, #webform-component-email, #webform-component-body | #webform-component-product-upc, #webform-component-store-zip-code |
      | Product Request | #webform-component-name, #webform-component-phone, #webform-component-email, #webform-component-body | #webform-component-product-upc, #webform-component-store-zip-code |
      | Company Feedback | #webform-component-name, #webform-component-phone, #webform-component-email, #webform-component-body | #webform-component-product-upc, #webform-component-store-zip-code |
      | 365 Everyday Value | #webform-component-name, #webform-component-phone, #webform-component-email, #webform-component-body, #webform-component-product-upc | #webform-component-store-zip-code |
      | Product Standards | #webform-component-name, #webform-component-phone, #webform-component-email, #webform-component-body | #webform-component-product-upc, #webform-component-store-zip-code |
      | Website Issues | #webform-component-name, #webform-component-phone, #webform-component-email, #webform-component-body | #webform-component-product-upc, #webform-component-store-zip-code |
      | Potential Vendor | #webform-component-name, #webform-component-phone, #webform-component-email, #webform-component-body | #webform-component-product-upc, #webform-component-store-zip-code |
      | Store Location Request | #webform-component-name, #webform-component-phone, #webform-component-email, #webform-component-body, #webform-component-store-zip-code | #webform-component-product-upc |

  @customer-service @unauth @javascript @mobile
  Scenario: Customer service requires name, message, and email fields.
    Given I select "Texas" from "edit-phone-number-state-list"
    And I select "Lamar" from "edit-phone-number-store-list"
    And I select "Company Feedback" from "edit-submitted-choose-topic"
    When I press the "Send comment" button
    Then I should see "Name field is required."
    And I should see "Message field is required."
    And I should see "Email address field is required."

  @customer-service @unauth @javascript @mobile
  Scenario: Customer service form submits correctly.
    Given I select "Texas" from "edit-phone-number-state-list"
    And I select "Lamar" from "edit-phone-number-store-list"
    And I select "Company Feedback" from "edit-submitted-choose-topic"
    And I enter "WFM Test" for "edit-submitted-name"
    And I enter "noreply@wholefoods.com" for "edit-submitted-email"
    And I enter "Test message from automated testing, please disregard" for "edit-submitted-body"
    And I press the "Send comment" button
    Then I should see "Thank you, your submission has been received."