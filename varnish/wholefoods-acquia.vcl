# Default Varnish cache policy for Acquia Hosting

# The default backend is specified in another file and should not be declared here.
# All other backends may be declared here.

# Incoming requests: Decide whether to try cache or not
sub vcl_recv {
  # Pipe all requests for files whose Content-Length is >=10,000,000. See
  # comment in vcl_fetch.
  if (req.http.x-pipe && req.restarts > 0) {
    return(pipe);
  }

  # Pipe all websocket requests.
  if (req.http.Upgrade ~ "(?i)websocket") {
    return (pipe);
  }

  # Grace: Avoid thundering herd when an object expires by serving
  # expired stale object during the next N seconds while one request
  # is made to the backend for that object.
  set req.grace = 120s;

  # EXAMPLE: How to do an external redirect in Varnish
  # in case we need to redirect a site to another balancer.
  # if (req.http.host ~ "^(www.)?domain.tld$") {
  #  error 302;
  # }

  # Varnish doesn't support Range requests: needs to be piped
  if (req.http.Range) {
    return(pipe);
  }

  # PURGE method support: PURGE request must include
  # a X-Acquia-Purge header which is weak but better
  # than nothing or ip address whack-a-mole. Example:
  # curl -X PURGE -H "X-Acquia-Purge: sitename" http://site/file
  if (req.request == "PURGE") {
    if (!req.http.X-Acquia-Purge) {
      error 405 "Not allowed.";
    }
    return(lookup);
  }

  # Don't Cache executables or archives
  # This was put in place to ensure these objects are piped rather then passed to the backend.
  # We had a customer who had a 500+MB file *.msi that Varnish was choking on,
  # so we decided to pipe all archives and executables to keep them from choking Varnish.
  if (req.url ~ "\.(msi|exe|dmg|zip|tgz|gz)") {
    return(pipe);
  }

  # Don't check cache for POSTs and various other HTTP request types
  if (req.request != "GET" && req.request != "HEAD") {
    return(pass);
  }

  # Always cache the following file types for all users if not coming from the private file system.
  if (req.url ~ "(?i)/(modules|themes|files|libraries)/.*\.(png|gif|jpeg|jpg|ico|swf|css|js|flv|f4v|mov|mp3|mp4|pdf|doc|ttf|eot|ppt|ogv)(\?[a-z0-9]+)?$" && req.url !~ "/system/files") {
    unset req.http.Cookie;
    # Set header so we know to remove Set-Cookie later on.
    set req.http.X-static-asset = "True";
  }

  # Don't check cache for cron.php
  if (req.url ~ "^/cron.php") {
    return(pass);
  }

  # NOTE: xmlrpc.php requests are not cached because they're POSTs

  # Don't check cache for feedburner or feedvalidator for ise
  if ((req.http.host ~ "^(www\.|web\.)?ise") &&
     (req.http.User-Agent ~ "(?i)feed")) {
       return(pass);
   }

# All mobile devices except iPad need to use the mobile site. 
# Redirect desktops and mobile devices accordingly. 
 if (req.http.host ~ "^www\.wholefoodsmarket\.com") {
   # if UA is not desktop and not ipad
   if (
     ( req.http.User-agent !~ "iPad") && (
       ( req.http.User-agent ~ "iPhone|iPod" ) ||
       ( req.http.User-agent ~ "Mobile|mobile" ) ||
       ( req.http.User-agent ~ "Opera.Mobi|Windows.CE")
     )
    ) {
      set req.http.X-Acquia-Mobile = "MOBILE";
      if (req.http.X-Forwarded-Proto ~ "https") {
        set req.http.newhost = regsub(req.http.host, "(www.)?(.*)", "https://m.\2");
      }
      else {
       set req.http.newhost = regsub(req.http.host, "(www.)?(.*)", "http://m.\2");
      }
      error 750 req.http.newhost;
    }
  } 
  else if (req.http.host ~ "^m\.wholefoodsmarket\.com") {
    # if UA is ipad or desktop
    if (
      ( req.http.User-agent ~ "iPad") || (
        ( req.http.User-agent !~ "iPhone|iPod" ) &&
        ( req.http.User-agent !~ "Mobile|mobile" ) &&
        ( req.http.User-agent !~ "Opera.Mobi|Windows.CE")
      )
    ) {
      remove req.http.X-Acquia-Mobile;
      if (req.http.X-Forwarded-Proto ~ "https") {
        set req.http.newhost = regsub(req.http.host, "(m.)?(.*)", "https://www.\2");
      }
      else {
       set req.http.newhost = regsub(req.http.host, "(m.)?(.*)", "http://www.\2");
      }
      error 750 req.http.newhost;
    }
  }

  # Cookie Cache Bypass Drupal module (Pressflow): Don't check cache for
  # any user that just submitted a content form within the past 5 to 10
  # minutes (depending on Drupal's cache_lifetime setting).
  # Persistent login module support: http://drupal.org/node/1306214
  if (req.http.cookie ~ "(NO_CACHE|PERSISTENT_LOGIN_[a-zA-Z0-9]+)") {
    return(pass);
  }

  # This is part of Varnish's default behavior to pass through any request that
  # comes from an http auth'd user.
  if (req.http.Authorization) {
    return(pass);
  }

  # Don't check cache if the Drupal session cookie is set.
  # Pressflow pages don't send this cookie to anon users.
  if (req.http.cookie ~ "(^|;\s*)(S?SESS[a-zA-Z0-9]*)=") {
    return(pass);
  }

  # If a local store has been selected, capture the store ID for vary purposes.
  if (req.http.cookie ~ "local_store=") {
    # Set store id value to a header called X-Local-Store
    set req.http.X-Local-Store = regsub(req.http.cookie, ".*local_store=([^;]+);.*", "\1");
  }

  # Enforce no-cookie-vary: Hide the Cookie header prior
  # to vcl_hash, then restore Cookie if we get to vcl_miss.
  # BUG: Varnish is truncates the X-Acquia-Cookie var
  if (req.http.Cookie) {
    set req.http.X-Acquia-Cookie = req.http.cookie;
    unset req.http.Cookie;
  }

  # Pass requests from simpletest to drupal.
  if (req.http.User-Agent ~ "simpletest") {
    return(pipe);
  }

  # strip out certain querystring params that varnish should not vary cache by
  call normalize_req_url;

  # Default cache check
  return(lookup);
}

# Cache hit: the object was found in cache
sub vcl_hit {
  if (req.request == "PURGE") {
    set obj.ttl = 0s;
    error 200 "Purged.";
  }
}

# Cache miss: request is about to be sent to the backend
sub vcl_miss {
  # Restore the original incoming Cookie
  if (req.http.X-Acquia-Cookie) {
    set bereq.http.Cookie = req.http.X-Acquia-Cookie;
    unset bereq.http.X-Acquia-Cookie;
  }
  # PURGE method support
  if (req.request == "PURGE") {
    error 404 "Not in cache.";
  }
}

# Pass (including HitPass): request is about to be sent to the backend
# or about to be delivered
sub vcl_pass {
  # Restore the original incoming Cookie
  if (req.http.X-Acquia-Cookie) {
    set bereq.http.Cookie = req.http.X-Acquia-Cookie;
    unset bereq.http.X-Acquia-Cookie;
  }
}

# piped requests should not support keepalive because
# Varnish won't have chance to process or log the subrequests
sub vcl_pipe {
  if (req.http.upgrade) {
    set bereq.http.upgrade = req.http.upgrade;
  }
  else {
    set req.http.connection = "close";
  }
}

# Backend response: Determine whether to cache each backend response
sub vcl_fetch {
  # Pipe all requests for files whose Content-Length is >=10,000,000. See
  # comment in vcl_pipe.
  if ( beresp.http.Content-Length ~ "[0-9]{8,}" ) {
     set req.http.x-pipe = "1";
     return(restart);
  }

  # Avoid attempting to gzip an empty response body
  # https://www.varnish-cache.org/trac/ticket/1320
  if (beresp.http.Content-Encoding ~ "gzip" && beresp.http.Content-Length == "0") {
    unset beresp.http.Content-Encoding;
  }

  # Remove the Set-Cookie header from static assets
  # This is just for cleanliness and is also done in vcl_deliver
  if (req.http.X-static-asset) {
    unset beresp.http.Set-Cookie;
  }

  # 500-505 status should be shown the default error page
  if (beresp.status >= 500 && beresp.status <= 505) {
    error beresp.status beresp.response;
  }

  # Don't cache errors, temporary redirects (302), HEAD, and POST responses.
  if (beresp.status >= 302 || !(beresp.ttl > 0s) || req.request != "GET") {
    call ah_pass;
  }

  # Make sure we are caching 301s for at least 15 mins.
  if (beresp.status == 301) {
    if (beresp.ttl < 15m) {
      set beresp.ttl = 15m;
    }
  }

  # Respect explicit no-cache headers
  if (beresp.http.Pragma ~ "no-cache" ||
     beresp.http.Cache-Control ~ "no-cache" ||
     beresp.http.Cache-Control ~ "private") {
    call ah_pass;
  }

  # Don't cache cron.php
  if (req.url ~ "^/cron.php") {
    return(hit_for_pass);
  }

  # NOTE: xmlrpc.php requests are not cached because they're POSTs

  # Don't cache if Drupal session cookie is set
  # Note: Pressflow doesn't send SESS cookies to anon users
  if (beresp.http.Set-Cookie ~ "SESS") {
    call ah_pass;
  }

  # Vary on the X-Local-Store header whether or not it is present
  if (beresp.http.Vary) {
    set beresp.http.Vary = beresp.http.Vary + ", X-Local-Store";
  } else {
    set beresp.http.Vary = "X-Local-Store";
  }

  # Grace: Avoid thundering herd when an object expires by serving
  # expired stale object during the next N seconds while one request
  # is made to the backend for that object.
  set beresp.grace = 120s;

  # Cache anything else. Returning nothing here would fall-through
  # to Varnish's default cache store policies.
  return(deliver);
}

# Deliver the response to the client
sub vcl_deliver {
  # Add an X-Cache diagnostic header
  if (obj.hits > 0) {
    set resp.http.X-Cache = "HIT";
    set resp.http.X-Cache-Hits = obj.hits;
    # Don't echo cached Set-Cookie headers
    unset resp.http.Set-Cookie;
  } else {
    set resp.http.X-Cache = "MISS";
  }

  # Hide the existence of the X-Local-Store header from downstream
  if (resp.http.Vary ~ "X-Local-Store") {
    set resp.http.Vary = regsub(resp.http.Vary, ",? *X-Local-Store *", "");
    set resp.http.Vary = regsub(resp.http.Vary, "^, *", "");
    if (resp.http.Vary == "") {
      unset resp.http.Vary;
    }
  }

  # Strip the age header for Akamai requests
  if (req.http.Via ~ "akamai") {
    set resp.http.X-Age = resp.http.Age;
    unset resp.http.Age;
  }

  # Remove the Set-Cookie header from static assets
  if (req.http.X-static-asset) {
    unset resp.http.Set-Cookie;
  }

  # Force Safari to always check the server as it doesn't respect Vary: cookie.
  # See https://bugs.webkit.org/show_bug.cgi?id=71509
  # Static assets may be cached however as we already forcefully remove the
  # cookies for them.
  if (req.http.user-agent ~ "Safari" && !req.http.user-agent ~ "Chrome" && !req.http.X-static-asset) {
    set resp.http.cache-control = "max-age: 0";
  }
  # ELB health checks respect HTTP keep-alives, but require the connection to
  # remain open for 60 seconds. Varnish's default keep-alive idle timeout is
  # 5 seconds, which also happens to be the minimum ELB health check interval.
  # The result is a race condition in which Varnish can close an ELB health
  # check connection just before a health check arrives, causing that check to
  # fail. Solve the problem by not allowing HTTP keep-alive for ELB checks.
  if (req.http.user-agent ~ "ELB-HealthChecker") {
    set resp.http.Connection = "close";
  }
  return(deliver);
}


# Backend down: Error page returned when all backend servers are down
sub vcl_error {

  # EXAMPLE: How to do an external redirect in Varnish
  # in case we need to redirect a site to another balancer.
  # if (req.http.host ~ "^(www.)?domain.tld$") {
  #  if (obj.status == 302) {
  #    set obj.http.Location = "http://domain.tld.prod.acquia-sites.com" + req.url;
  #    deliver;
  #  }
  # }

  # mobile browsers redirect
  if (obj.status == 750) {
    set obj.http.Location = obj.response + req.url;
    set obj.status = 302;
    set obj.response = "Found";
    return(deliver);
  }


  # Default Varnish error (Nginx didn't reply)
  set obj.http.Content-Type = "text/html; charset=utf-8";

  synthetic {"<?xml version="1.0" encoding="utf-8"?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
      <head>
        <title>Whole Foods Market"} + " | " + obj.status + " " + obj.response + " | XID: " + req.xid + {"</title>
        <!-- Google Tag Manager -->
        <noscript>
          <iframe src="//www.googletagmanager.com/ns.html?id=GTM-2HH6" height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-2HH6');
        </script>
        <!-- End Google Tag Manager -->
        <script>
        //  Push error message to datalayer.
        (function wfm_gtm_error() {
          window.dataLayer = window.dataLayer || [];
          window.dataLayer.push({ event: 'errorPage', errorType: '"} + obj.status + {"' });
        })();
        </script>
        <style type="text/css" title="text/css" media="all">
          body { color: #000; margin: 0; padding: 0; font-family: 'lucida sans', calibri, san-serif; font-size: small; line-height: 150%; text-align: center; border-top: 20px solid #3c5423; }
          #container { width: 90%; text-align: left; margin: 0 auto; background: transparent url(http://assets.wholefoodsmarket.com/www/misc/fruit.jpg) right bottom no-repeat; }
          h1 { font-size: 2em; font-family: Calibri, 'Lucida Sans', sans-serif; padding: 0px 0 40px 110px; margin: 1.5em 0 1em; line-height: 120%; background: transparent url(http://assets.wholefoodsmarket.com/www/misc/wfm-logo-circle-342.png) left top no-repeat; }
          h2 { font-size: 1.7em; font-family: Calibri, 'Lucida Sans', sans-serif; margin: 1.5em 0 1em; }
          p { margin: 1em 0; font-size: medium;  }
          ul li { margin: 0 0 .5em; padding: 0; }
          a, a:hover { color: #c35400; }
          /* Smartphones ----------- */
          @media only screen
          and (min-device-width : 320px)
          and (max-device-width : 680px) {
          div#container { background-image: none;  }
          li { font-size: 120%; }
          }
        </style>
      </head>
      <body>
        <div id="container">
          <h1>Sorry, our website is down.<br />Please try again in a few minutes.</h1>
          <h2>Get info:</h2>
          <ul>
            <li><a href="http://maps.google.com/maps?q=whole%20foods%20market">Whole Foods Market store locations<br />on Google Maps</a></li>
            <li><a href="http://news.google.com/news?hl=en&amp;safe=active&amp;q=whole+foods+market&amp;um=1&amp;ie=UTF-8&amp;ei=l_zMSt2nA8-_twfB84jbAQ&amp;sa=X&amp;oi=news_group&amp;ct=title&amp;resnum=14">Whole Foods Market in the News</a></li>
          </ul>
          <h2>Connect:</h2>
          <ul>
            <li><a href="http://twitter.com/wholefoods">Twitter</a></li>
            <li><a href="http://facebook.com/wholefoods">Facebook</a></li>
            <li><a href="http://www.instagram.com/wholefoodsmarket">Instagram</a></li>
            <li><a href="http://www.pinterest.com/wholefoods/">Pinterest</a></li>
          </ul>
          <h2>Explore:</h2>
          <ul>
            <li><a href="http://www.wholeplanetfoundation.org">Whole Planet Foundation</a></li>
            <li><a href="http://www.wholekidsfoundation.org">Whole Kids Foundation</a></li>
            <li><a href="http://www.wholecitiesfoundation.org">Whole Cities Foundation</a></li>
            <li><a href="http://www.wholejourneys.com">Whole Journeys</a></li>
          </ul>
        </div>
      </body>
    </html>
  "};
  return(deliver);
}

# Separate pass subroutine to shorten the lifetime of beresp.ttl
# This will reduce the amount of "Cache Hits for Pass" for objects
sub ah_pass {
  set beresp.ttl = 10s;
  return(hit_for_pass);
}

sub normalize_req_url {
    # Strip out Google Analytics campaign variables. They are only needed
    # by the javascript running on the page
    # utm_source, utm_medium, utm_campaign, gclid, ...
    if(req.url ~ "(\?|&)(gclid|dclid|cx|ie|cof|siteurl|gclsrc|zanpid|os_ehash|origin|hConversionEventId|utm_[a-z]+|mr:[A-z]+)=") {
        set req.url = regsuball(req.url, "(gclid|dclid|cx|ie|cof|siteurl|gclsrc|zanpid|os_ehash|origin|hConversionEventId|utm_[a-z]+|mr:[A-z]+)=[A-z0-9%\._+-:]*&?", "");
    }
    set req.url = regsub(req.url, "(\??&?)$", "");
}
